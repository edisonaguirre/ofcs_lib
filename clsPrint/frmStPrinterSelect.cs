﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Printing
{


    public partial class frmStPrinterSelect : Form
    {

        public string spPRINTERNAME = ""; //            'プリンタ名
        public int spPRINTCOPIES = 1; //      '印刷部数
        public ulong spPRINThDC = 0; //   'プリンタhDC
        public bool STATUS = false;

        public frmStPrinterSelect()
        {
            InitializeComponent();
        }

        private void frmStPrinterSelect_Load(object sender, EventArgs e)
        {
            var printers = System.Drawing.Printing.PrinterSettings.InstalledPrinters;

            foreach (String s in printers)
            {
                cboPrinters.Items.Add(s);
            }
            if (spPRINTERNAME != "")
            {
                foreach (var i in cboPrinters.Items)
                {
                    if (i.ToString().Trim() == spPRINTERNAME)
                    {
                        cboPrinters.SelectedText = spPRINTERNAME;
                        
                        break;
                    }
                }
            }
            cmdOK.Focus();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            this.STATUS = true;
            spPRINTERNAME = this.cboPrinters.Text.Trim();
            this.Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
