﻿using System;

namespace Printing
{
    partial class frmPrintPreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pibView2 = new System.Windows.Forms.PictureBox();
            this.imgExcelView = new System.Windows.Forms.PictureBox();
            this.pibView1 = new System.Windows.Forms.PictureBox();
            this.cmdFunc_12 = new System.Windows.Forms.Button();
            this.cmdFunc_11 = new System.Windows.Forms.Button();
            this.cmdFunc_10 = new System.Windows.Forms.Button();
            this.cmdFunc_8 = new System.Windows.Forms.Button();
            this.cmdFunc_7 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgExcelView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pibView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.imgExcelView);
            this.groupBox1.Controls.Add(this.pibView1);
            this.groupBox1.Controls.Add(this.cmdFunc_12);
            this.groupBox1.Controls.Add(this.cmdFunc_11);
            this.groupBox1.Controls.Add(this.cmdFunc_10);
            this.groupBox1.Controls.Add(this.cmdFunc_8);
            this.groupBox1.Controls.Add(this.cmdFunc_7);
            this.groupBox1.Location = new System.Drawing.Point(4, -2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1102, 618);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.pibView2);
            this.panel1.Location = new System.Drawing.Point(6, 22);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1090, 540);
            this.panel1.TabIndex = 49;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pibView2
            // 
            this.pibView2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pibView2.Location = new System.Drawing.Point(0, 0);
            this.pibView2.Name = "pibView2";
            this.pibView2.Size = new System.Drawing.Size(944, 308);
            this.pibView2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pibView2.TabIndex = 49;
            this.pibView2.TabStop = false;
            // 
            // imgExcelView
            // 
            this.imgExcelView.Location = new System.Drawing.Point(6, 22);
            this.imgExcelView.Name = "imgExcelView";
            this.imgExcelView.Size = new System.Drawing.Size(948, 540);
            this.imgExcelView.TabIndex = 46;
            this.imgExcelView.TabStop = false;
            // 
            // pibView1
            // 
            this.pibView1.Location = new System.Drawing.Point(6, 22);
            this.pibView1.Name = "pibView1";
            this.pibView1.Size = new System.Drawing.Size(948, 540);
            this.pibView1.TabIndex = 44;
            this.pibView1.TabStop = false;
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cmdFunc_12.Location = new System.Drawing.Point(840, 577);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(105, 28);
            this.cmdFunc_12.TabIndex = 43;
            this.cmdFunc_12.Text = "終了(F12)";
            this.cmdFunc_12.UseVisualStyleBackColor = false;
            this.cmdFunc_12.Click += new System.EventHandler(this.cmdFunc_12_Click);
            // 
            // cmdFunc_11
            // 
            this.cmdFunc_11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cmdFunc_11.Location = new System.Drawing.Point(734, 577);
            this.cmdFunc_11.Name = "cmdFunc_11";
            this.cmdFunc_11.Size = new System.Drawing.Size(107, 28);
            this.cmdFunc_11.TabIndex = 42;
            this.cmdFunc_11.Text = "１頁印刷(F11)";
            this.cmdFunc_11.UseVisualStyleBackColor = false;
            this.cmdFunc_11.Click += new System.EventHandler(this.cmdFunc_11_Click);
            // 
            // cmdFunc_10
            // 
            this.cmdFunc_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cmdFunc_10.Location = new System.Drawing.Point(626, 577);
            this.cmdFunc_10.Name = "cmdFunc_10";
            this.cmdFunc_10.Size = new System.Drawing.Size(109, 28);
            this.cmdFunc_10.TabIndex = 41;
            this.cmdFunc_10.Text = "全頁印刷(F10)";
            this.cmdFunc_10.UseVisualStyleBackColor = false;
            this.cmdFunc_10.Click += new System.EventHandler(this.cmdFunc_10_Click);
            // 
            // cmdFunc_8
            // 
            this.cmdFunc_8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cmdFunc_8.Location = new System.Drawing.Point(110, 577);
            this.cmdFunc_8.Name = "cmdFunc_8";
            this.cmdFunc_8.Size = new System.Drawing.Size(86, 28);
            this.cmdFunc_8.TabIndex = 39;
            this.cmdFunc_8.Text = "次頁(F8)";
            this.cmdFunc_8.UseVisualStyleBackColor = false;
            this.cmdFunc_8.Click += new System.EventHandler(this.cmdFunc_8_Click);
            // 
            // cmdFunc_7
            // 
            this.cmdFunc_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cmdFunc_7.Location = new System.Drawing.Point(25, 577);
            this.cmdFunc_7.Name = "cmdFunc_7";
            this.cmdFunc_7.Size = new System.Drawing.Size(86, 28);
            this.cmdFunc_7.TabIndex = 38;
            this.cmdFunc_7.Text = "前頁(F7)";
            this.cmdFunc_7.UseVisualStyleBackColor = false;
            this.cmdFunc_7.Click += new System.EventHandler(this.cmdFunc_7_Click);
            // 
            // frmPrintPreview
            // 
            this.AccessibleDescription = "Invoicing";
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 617);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmPrintPreview";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Print Preview";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPrintPreview_FormClosing);
            this.Load += new System.EventHandler(this.frmPrintPreview_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPrintPreview_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgExcelView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pibView1)).EndInit();
            this.ResumeLayout(false);

        }



        private void datKanryoYMD_0_ValueChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void datKanryoYMD_1_ValueChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void cmdOnOffMei_Click(object sender, EventArgs e)
        {

        }

        private void cboTanto_SelectedIndexChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void datSeikyuYMD_ValueChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void cmdOnOffHed_Click(object sender, EventArgs e)
        {

        }


        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cmdFunc_12;
        private System.Windows.Forms.Button cmdFunc_11;
        private System.Windows.Forms.Button cmdFunc_10;
        private System.Windows.Forms.Button cmdFunc_8;
        private System.Windows.Forms.Button cmdFunc_7;
        private System.Windows.Forms.PictureBox pibView1;
        private System.Windows.Forms.PictureBox imgExcelView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pibView2;
    }
}

