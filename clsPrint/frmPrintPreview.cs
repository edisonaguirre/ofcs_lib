﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using SAHANBAI;
using VBlibrary;


namespace Printing
{
    public partial class frmPrintPreview : Form
    {
        public string srcmodule; //m

        private const string ppMSG001 = "全頁印刷を開始します。よろしいですか？";
        private const string ppMSG002 = "現在の頁を印刷します。よろしいですか？";
        public string ppvActCell;//       'セル番号文字列("A1:B2"形式)

        private long ppvCurPage;//         'カレントページ番号
        private long ppvMaxPage;//         '最大ページ番号
        string ReportDIR;

        Excel.Workbook xlWorkBook;

        //'Excel

        public Boolean tagSEIMR020_SDateUpdate = false;
        public Boolean tagJYUMR020_JyuHUpdate = false;
        public Boolean tagJYUMR030_JyuHUpdate = false;
        #region " VARIABLES "

        public clsGlobal gbl = new clsGlobal();
        public clsDB.DBService DB;
        public DataTable dtConMstInfo = new DataTable();

        public string stvVersionNumber = "1.0.0";

        public string stvVersionTitle;
        public string KS_WSNumber = "999";

        public DateTime SysDate;


        public SEIMR020Upd[] OrdUPDKey;

        public struct SEIMR020Upd
        {
            public string KeiyakuNo; //'契約№
            public string SeikyuNo;  //'請求№
        }

        #endregion
        public frmPrintPreview()
        {
            InitializeComponent();
        }




        private void frmPrintPreview_Load(object sender, EventArgs e)
        {
            DB= new clsDB.DBService();
            SysDate = DateTime.Today;
           
            KS_WSNumber = gbl.GetIniValue("WS NUMBER");
            ppvCurPage = 1;

            ReportDIR = gbl.GetIniValue("REPORT PATH");
            if (stePrevExcelOpen1(ppvCurPage, ppvActCell))
            {
                GetImageShow();
            }

            InitialForm();

            //'最大ページ番号を取得
            //'        ppvMaxPage = stePrevSheetCount                            
            ppvMaxPage = xlWorkBook.Worksheets.Count;


            //'ページボタンの設定
            SetPageButton(ppvCurPage);

            //'中央に表示
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

            //xlWorkBook.Close(SaveChanges: false);





        }

        public Boolean stePrevExcelOpen1(long vlPage, string vlRange)

        {
            Boolean stePrevExcelOpen1 = false;
            string exFname;
            Image vImage;
            int w, h;
            
            

            //'ファイルの存在チェック
            if (stePrevFileCheck() != "")
            {
                if (SJKExcelCom.xlApp == null)
                {
                    SJKExcelCom.xlApp = new Microsoft.Office.Interop.Excel.Application();
                }

                xlWorkBook = SJKExcelCom.xlApp.Workbooks.Open(steCreateWorkPrev(0));
                SJKExcelCom.xlWs = xlWorkBook.Worksheets[vlPage];
                SJKExcelCom.xlWs.Range[vlRange].Copy(System.Type.Missing);

            }


            //'コピー
            stePrevExcelOpen1 = true;
            return stePrevExcelOpen1;
        }

        private void GetImageShow()
        {
            Image vImage = Clipboard.GetImage();
            this.pibView2.Image = vImage;


            this.pibView1 = this.pibView2;
            this.pibView1.Height = imgExcelView.Height;
            this.pibView1.Width = imgExcelView.Width;
            this.pibView1.Top = imgExcelView.Top;
            this.pibView1.Left = imgExcelView.Left;

            //    'クリップボードのクリア
            Clipboard.Clear();
        }


        private void InitialForm()
        {
            imgExcelView.Visible = false;
            pibView1.Visible = true;
            pibView2.Visible = true;

        }

        private void SetPageButton(long vlPage)
        {
            if (ppvMaxPage == 1) // '１ページの場合
            {
                cmdFunc_7.Enabled = false;
                cmdFunc_8.Enabled = false;
            }
            else
            {
                if (vlPage <= 1) //'１ページ目
                {
                    cmdFunc_7.Enabled = false;
                    cmdFunc_8.Enabled = true;
                }
                else if (vlPage >= ppvMaxPage) //'１ページ目
                {
                    cmdFunc_7.Enabled = true;
                    cmdFunc_8.Enabled = false;
                }

                else  //'間のページ
                {
                    cmdFunc_7.Enabled = true;
                    cmdFunc_8.Enabled = true;
                }

            }
        }





        public string stePrevFileCheck()
        {
            string stePrevFileCheck = "";
            if (File.Exists(steCreateWorkPrev(0)))
            {
                stePrevFileCheck = steCreateWorkPrev(0);
            }
            return stePrevFileCheck;
        }

        public string steCreateWorkPrev(int viMode)
        {
            string steCreateWorkPrev = "";
            switch (viMode)
            {
                case 0:
                    steCreateWorkPrev = ReportDIR + Constant.gstStrSEPPATH +
                                        Constant.exXLSPrevWk + Constant.gstStrSEPFILE +
                                        this.KS_WSNumber + Constant.exXLSSuffix;
                    break;
                case 1:
                    steCreateWorkPrev = ReportDIR + Constant.gstStrSEPPATH +
                                        Constant.exXLSPrevWk + Constant.gstStrSEPFILE +
                                        this.KS_WSNumber + Constant.exXLSWildCd;
                    break;
            }

            return steCreateWorkPrev;
        }
        private void frmPrintPreview_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (gbl.ksExpMsgBox(this, "請求書発行処理を終了します。よろしいですか？", "Q") == DialogResult.Yes)
            {

                if (SJKExcelCom.xlApp.Workbooks.Count > 0)
                {
                    SJKExcelCom.xlApp.Workbooks.Close();
                }

                if (SJKExcelCom.xlApp != null)
                {
                    SJKExcelCom.xlApp.Quit();
                    SJKExcelCom.xlApp = null;

                }
                e.Cancel = false;

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void frmPrintPreview_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                    this.cmdFunc_7_Click(null, null);
                    break;
                case Keys.F8:
                    this.cmdFunc_8_Click(null, null);
                    break;

                case Keys.F10:
                    this.cmdFunc_10_Click(null, null);
                    break;

                case Keys.F12:
                    this.cmdFunc_12_Click(null, null);
                    break;
            }
        }

        private void cmdFunc_7_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            PrevPage();
            Cursor.Current = Cursors.Default;
        }



        private void cmdFunc_8_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            NextPage();
            Cursor.Current = Cursors.Default;
        }


        private void cmdFunc_10_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            AllPrint();
            Cursor.Current = Cursors.Default;
        }

        private void cmdFunc_11_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            OnePrint();
            Cursor.Current = Cursors.Default;
        }

        private void cmdFunc_12_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void NextPage()
        {
            //'ボタンが有効なとき
            if (cmdFunc_8.Enabled == true)
            {

                if (xlWorkBook != null)
                {
                    xlWorkBook.Close(SaveChanges: false);
                    xlWorkBook = null;
                }
            }

            //'前ページに設定
            ppvCurPage = ppvCurPage + 1;
            //'ワークブックを再オープン

            if (stePrevExcelOpen1(ppvCurPage, ppvActCell))
            {
                GetImageShow();
                SetPageButton(ppvCurPage);
            }
            //if (xlWorkBook != null)
            //{
            //    xlWorkBook.Close(SaveChanges: false);
            //    xlWorkBook = null;
            //}
            //if (xlApp != null)
            //{
            //    xlApp.Quit();
            //    xlApp = null;
            //}

        }

        private void PrevPage()
        {
            //'ボタンが有効なとき
            if (cmdFunc_7.Enabled == true)
            {

                if (xlWorkBook == null)
                {
                    xlWorkBook.Close(SaveChanges: false);
                    xlWorkBook = null;
                }
            }

            //'前ページに設定
            ppvCurPage = ppvCurPage - 1;
            //'ワークブックを再オープン

            if (stePrevExcelOpen1(ppvCurPage, ppvActCell))
            {
                GetImageShow();
                SetPageButton(ppvCurPage);
            }
            //if (xlWorkBook != null)
            //{
            //    xlWorkBook.Close(SaveChanges: false);
            //    xlWorkBook = null;
            //}
            //if (xlApp != null)
            //{
            //    xlApp.Quit();
            //    xlApp = null;
            //}
        }

        public bool gstGetPrinterInfoFunc(frmStPrinterSelect frm)
        {
            bool result = false;
            frm.spPRINTERNAME =modCommon.ActivePrinter.Printer;
            frm.spPRINTCOPIES =modCommon.ActivePrinter.Copies;
            frm.ShowDialog();
            if (frm.STATUS == true)
            {
               modCommon.ActivePrinter.Printer = frm.spPRINTERNAME;
               modCommon.ActivePrinter.Copies = frm.spPRINTCOPIES;
               modCommon.ActivePrinter.hDC = frm.spPRINThDC;
                result = true;
                return result;
            }
            return result;
        }

        private void AllPrint()
        {
            long llPgCnt;

            //'印刷確認
            if (gbl.ksExpMsgBox(this, ppMSG001, "Q") == DialogResult.Yes)
            {
                //'プリンタ選択
                if (gstGetPrinterInfoFunc(new frmStPrinterSelect()))
                {
                    //'ファイルの存在チェック
                    if (stePrevFileCheck() != "")
                    {
                        if (modCommon.ActivePrinter.Printer == null)
                        {
                            SJKExcelCom.xlApp.Worksheets.PrintOutEx(Copies: 1, Collate: true);
                        }
                        else
                        {
                            //xlWs.PrintOutEx(Copies: 1, Collate: true, modCommon.ActivePrinter: gbl.modCommon.ActivePrinter.Printer);
                            SJKExcelCom.xlApp.Worksheets.PrintOutEx(Copies: 1, Collate: true, ActivePrinter:modCommon.ActivePrinter.Printer);
                            //string fullPath = @"C:\test\test_all.pdf";
                            //xlApp.Worksheets.PrintOutEx(Copies: 1, Collate: true, modCommon.ActivePrinter: gbl.modCommon.ActivePrinter.Printer, PrintToFile: true, PrToFileName: fullPath);


                        }

                        //if (tagSEIMR020_SDateUpdate == true)
                        //{
                        //    //'各ページごとに印刷
                        //    for (llPgCnt = 1; llPgCnt <= ppvMaxPage; llPgCnt++)
                        //    {
                        //        // '請求書発行日を更新
                        //        SEIMR020_SDateUpdate(OrdUPDKey[llPgCnt].SeikyuNo, OrdUPDKey[llPgCnt].KeiyakuNo);
                        //    }
                        //}

                        //if (tagJYUMR020_JyuHUpdate == true)
                        //{
                        //    //'各ページごとに印刷
                        //    for (llPgCnt = 1; llPgCnt <= ppvMaxPage -1; llPgCnt++)
                        //    {
                        //        // '請求書発行日を更新
                        //        modJYUMR020.JYUMR020_JyuHUpdate(modJYUMR020.JyuHUPDKey[llPgCnt].KEIYAKUNO, modJYUMR020.JyuHUPDKey[llPgCnt].RefNo);
                            
                               
                        //    }
                        //}

                        //if (tagJYUMR030_JyuHUpdate == true)
                        //{
                        //    //'各ページごとに印刷
                        //    for (llPgCnt = 1; llPgCnt <= ppvMaxPage; llPgCnt++)
                        //    {
                        //        // '請求書発行日を更新
                        //        prtOrderConfirmation.JYUMR030_JyuHUpdate(prtOrderConfirmation.JyuHUPDKey[llPgCnt].KEIYAKUNO, prtOrderConfirmation.JyuHUPDKey[llPgCnt].RefNo);
                        //    }
                        //}


                  

                    }


                }


            }



        }
        private void OnePrint()
        {
            //'印刷確認
            if (gbl.ksExpMsgBox(this, ppMSG002, "Q") == DialogResult.Yes)
            {

                if (gstGetPrinterInfoFunc(new frmStPrinterSelect()))
                {
                    //steExcelPrint1(ref this.xlWorkBook, ppvCurPage, gbl.modCommon.ActivePrinter.Printer);
                    SJKExcelCom.steExcelPrint(ppvCurPage,modCommon.ActivePrinter);
                    MITMR020_OrdHUpdate(modMITMR010P.OrdHUPDKey[ppvCurPage].KEIYAKUNO, modMITMR010P.OrdHUPDKey[ppvCurPage].RefNo);
                }
            }

        }
        //private void steExcelPrint1(ref Excel.Workbook vlWorkBook, long vlSheet, object viPrinter, int viCopies = 1)
        //{
        //    Excel.Worksheet ws = new Excel.Worksheet();
        //    ws = vlWorkBook.Worksheets[vlSheet];
        //    if (viPrinter == null)
        //    {
        //        //public void PrintOut(object From, object To, object Copies, object Preview, object modCommon.ActivePrinter, object PrintToFile, object Collate, object PrToFileName);
        //        //vlWorkBook.Worksheets[vlSheet].PrintOut(Copies: viCopies, Collate: true); // Copies:= viCopies, Collate:= True
        //        ws.PrintOutEx(Copies: viCopies, Collate: true);
        //    }
        //    else
        //    {
        //        //string fullPath = @"C:\test\test.pdf";
        //        //ws.PrintOutEx(Copies: viCopies, Collate: true, modCommon.ActivePrinter: viPrinter, PrintToFile: true, PrToFileName: fullPath);
        //        //vlWorkBook.Worksheets[vlSheet].PrintOut(Copies: viCopies, Collate: true , modCommon.ActivePrinter: viPrinter);
        //        ws.PrintOutEx(Copies: viCopies, Collate: true, modCommon.ActivePrinter: viPrinter);

        //        //vlWorkBook.Worksheets[vlSheet].PrintOut Copies:=viCopies, Collate:=True, _
        //        //                                    modCommon.ActivePrinter:=viPrinter
        //    }
        //}

        private void MITMR020_OrdHUpdate(string vKey1, string vKey2)
        {
            string strWk = "";
            strWk = "UPDATE OrdH_Dat ";

            if(true==true)
            {
                strWk = strWk + " SET OrdH_0281 = '" + SysDate.ToString("yyyy/MM/dd") + "'";
            }
            else
            {
                strWk = strWk + " SET OrdH_028 = '" + SysDate.ToString("yyyy/MM/dd") + "'";
            }
            strWk = strWk + " WHERE OrdH_001 = '" + vKey1 + "'";
            strWk = strWk + "   AND OrdH_009 = '" + vKey2 + "'";

            string outmsg;
            DB.ExecuteSql(strWk,out outmsg);

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

