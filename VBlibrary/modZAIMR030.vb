﻿
Imports System.Windows.Forms
Imports clswinApi
Imports SAHANBAI.clsGlobal
Imports winApi = clswinApi.StaticWinApi
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports SAHANBAI.TaxComp

Public Module modZAIMR030

    Dim xlApp As Excel.Application
    Dim xlWs As Excel.Worksheet
    Dim xlRange As Excel.Range

    Dim DB As clsDB.DBService
    Private TaxComp As New TaxComp
    Private TaxCalcInfo As TaxComp.TaxCalcInfo

    '***************************************************************
    '**
    '**  機能           :  在庫一覧表(modZAIMR030)
    '**  作成日         :
    '**  更新日         :
    '**  備考           :
    '**
    '***************************************************************
    '各マスタ情報
    Public ConMst(0 To 0) As ConMstInfo     'コントロールマスタ情報

    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
        Excel = 2       'Excel出力
    End Enum

    '出荷台帳印刷条件
    Public NyDate As String   '日付
    Public SName As String   '得意先名称

    ''エクセル上のセル範囲
    Private pstLastRow As Integer   '最終行

    '== 出荷台帳のExcel定義 ==
    Public psttREPORT_NAME As String                                'Report名称
    Public psttREPORT_NAME2 As String                                'エクセル出力
    Public Const psttREPORT_NAME3 As String = "JYUMR080_在庫リスト"        'Report名称
    Public Const psttREPORT_NAME4 As String = "JYUMR080_在庫リスト2"       'エクセル出力
    Public Const psttREPORT_NAME5 As String = "JYUMR080_在庫リスト3"       'Report名称
    Public Const psttREPORT_NAME6 As String = "JYUMR080_在庫リスト4"       'エクセル出力


    Public pstPrintArea As String                                'シートクリア範囲
    Public Const pstPrintArea2 As String = "B6:G57"                     'シートクリア範囲
    Public Const pstPrintArea3 As String = "B6:K57"                     'シートクリア範囲

    Private Const pstPrevArea As String = "A1:AO58"                    'プレビュー表示範囲
    Private Const pstMaxRow As Integer = 52                          '最大行数
    Private Const pstStartRow As Integer = 6                           'ページ内の開始行

    '印刷共通定義
    Private mCurMode As Integer     'カレント印刷モード
    Private mCurPage As Long        'カレントページ番号
    Private mCurRows As Long        'カレント行
    Private mCurCols As Long        'カレント列
    Private brkTokNm As String
    'Excel出力用
    Private xlBookTem As Excel.Workbook
    Private xlBookNew As Excel.Workbook   'ワーク用ブック

    Private curQuantity As Double
    Private curExcludingTax As Double
    Private curTaxIncluded As Double     'INSERT 2017/03/14 AOKI
    Private curSzeiKei As Double     'INSERT 2017/03/14 AOKI

    Private frmZAIMR030_optSelect_0 As RadioButton


    '***************************************************************
    '**  名称    : Function stmSetFunction() As Boolean
    '**  機能    : ファンクションキーの使用不可を設定する。
    '**  戻り値  :
    '**  引数    : vcForm ; 設定フォーム
    '**  備考    :


    '***************************************************************
    '**  名称    : Function JYUMR080PrintProc() As Boolean
    '**  機能    : 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function JYUMR080PrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim PCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim RecMeisai As New DataTable  '明細データ

        On Error GoTo JYUMR080PrintProc_Err

        JYUMR080PrintProc = False

        mCurMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存
        pstLastRow = pstMaxRow + pstStartRow - 1

        RecMeisai = Nothing

        '===データの読込み用SQL文作成ルーチン
        strSQL = CreateSQLText()
        If DB Is Nothing Then DB = New clsDB.DBService
        RecMeisai = DB.GetDatafromDB(strSQL)
        '===データの読込み

        '===データ存在チェック
        If RecMeisai.Rows.Count <= 0 Then
            'データ無しメッセージ
            Call ksExpMsgBox("印刷するデータが存在しません。", "I")
            GoTo Exit_JYUMR080PrintProc
        End If


        xlApp = CreateObject("Excel.Application")

        '===Excelファイルのオープン
        If steExcelOpen(psttREPORT_NAME & exXLSSuffix) = False Then
            'Excelファイル無しメッセージ
            Call ksExpMsgBox(psttREPORT_NAME & "のレポートが存在しません。", "E")
            GoTo Exit_JYUMR080PrintProc
        End If

        If mCurMode = enmPrintmode.Preview Then        'プレビューモード
            Call stePrevWorkDelete()  '古いワークファイルを削除する
        End If

        '===進行状況表示フォーム設定=============================================
        'With frmStProgressBar
        '    .SetCaption = "在庫リスト"
        '    .SetTitle = "【在庫リスト】・・作成中・・"
        '    .SetPage = "ページ = " & 0
        '    .SetMin = 0
        '    .SetMax = RecMeisai.RecordCount
        '    .ShowStart
        'End With

        '===各変数初期化
        mCurPage = 1                  'ページ番号
        mCurRows = pstStartRow - 1    '明細印刷開始行
        DCount = 0                    'データカウンタ
        PCount = 0                    '印字カウンタ
        brkTokNm = ""

        curQuantity = 0
        curExcludingTax = 0
        curTaxIncluded = 0          'INSERT 2017/03/14 AOKI

        'シートのクリア
        Call loSheetClear()


        For Each dr As DataRow In RecMeisai.Rows


            '進行状況用カウンタ
            DCount = DCount + 1

            '進行状況表示フォーム設定

            PCount = PCount + 1

            'ヘッダー出力
            If PCount = 1 Then
                Call sttWriteHeader()
            End If

            '明細情報出力
            Call CheckPages()
            Call sttWriteMeisai(dr, xlApp.Worksheets(GetTargetSheet))

            '合計格納
            Call setTotal(RecMeisai)

        Next


        Call CheckPages()
        '合計表示
        Call writeTotal(xlApp.Worksheets(GetTargetSheet))

        'モードごとの処理
        Select Case viMode

            Case enmPrintmode.Preview      'プレビュー

                Call stePrevBookSave()       'Bookの保存
                Call steWorkbooksClose()     'Bookのクローズ

                'プレビュー画面の起動
                Dim frm As New SAHANBAI.frmPrintPreview
                With frm

                    .ppvActCell = pstPrevArea
                    .ShowDialog()
                End With


            Case enmPrintmode.PrintOut     '印刷
                mCurRows = pstLastRow
                Call CheckPages()
                Call steWorkbooksClose()     'Bookのクローズ
        End Select


        JYUMR080PrintProc = True

Exit_JYUMR080PrintProc:

        RecMeisai = Nothing
        Exit Function

JYUMR080PrintProc_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Call steWorkbooksClose()        'Bookのクローズ

        Resume Exit_JYUMR080PrintProc

    End Function

    '***************************************************************
    '**  名称    : Function CreateSQLText() As Long
    '**  機能    : 読み込み用SQLの作成
    '**  戻り値  : 作成されたSQL対象となるExcelシート番号
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function CreateSQLText() As String

        Dim strSQL As String

        If frmZAIMR030_optSelect_0.Checked = True Then
            '===データの読込み用SQL文作成ルーチン
            strSQL = "          SELECT  ZaiD_001 "
            strSQL = strSQL & "      ,  ZaiD_020 "
            strSQL = strSQL & "      ,  ZaiD_028 "
            strSQL = strSQL & "      ,  ZaiD_008 "
            'strSQL = strSQL & "      ,  SUM(ZaiD_024) AS ZaiD_024 "                            'DELETE 2012/12/07 AOKI
            strSQL = strSQL & "      ,  SUM((ZaiD_021 + ZaiD_032) * ZaiD_026) AS ZaiD_024 "     'INSERT 2012/12/07 AOKI
            strSQL = strSQL & "      ,  ZaiD_007 "                                              'INSERT 2017/03/14 AOKI
            strSQL = strSQL & " FROM    ZaiD_Dat "
            strSQL = strSQL & " WHERE   ISNULL(ZaiD_020,'') > '' "
            strSQL = strSQL & " AND     ZaiD_020 <= '" & NyDate & "'"

            'INSERT 2019/08/22 AOKI START -----------------------------------------------------------------------------------------
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%DELIVERY CHARGE%'"
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%D/C%'"
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%DISCOUNT%'"
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%CANCEL%'"
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%[%] ON'"
            strSQL = strSQL & " AND     ZaiD_005 <> '検査費'"
            strSQL = strSQL & " AND     ZaiD_021 + ZaiD_032 > 0"
            strSQL = strSQL & " AND     ZaiD_007 <> 343"
            strSQL = strSQL & " AND     ZaiD_026 <> 0"
            'INSERT 2019/08/22 AOKI E N D -----------------------------------------------------------------------------------------

            '仕入先名称であいまい検索
            If NCnvN(SName) <> "" Then
                strSQL = strSQL & " AND   ZaiD_008 = '" & modCommon.EditSQLAddSQuot(SName) & "'"
            End If

            strSQL = strSQL & " AND (NOT EXISTS "
            strSQL = strSQL & "                 (SELECT * "
            strSQL = strSQL & "                  FROM   SeiD_Dat "
            strSQL = strSQL & "                  WHERE  SeiD_003 = ZaiD_001k "
            strSQL = strSQL & "                  AND    SeiD_002 <= '" & NyDate & "'"
            strSQL = strSQL & "                 ) "
            strSQL = strSQL & "     ) "

            strSQL = strSQL & " GROUP BY ZaiD_001 "
            strSQL = strSQL & "        , ZaiD_020 "
            strSQL = strSQL & "        , ZaiD_028 "
            strSQL = strSQL & "        , ZaiD_008 "
            strSQL = strSQL & "        , ZaiD_007 "                                             'INSERT 2017/03/14 AOKI
            strSQL = strSQL & " ORDER BY ZaiD_001 "
            strSQL = strSQL & "        , ZaiD_020 "
            strSQL = strSQL & "        , ZaiD_028 "
            strSQL = strSQL & "        , ZaiD_008 "
            strSQL = strSQL & "        , ZaiD_007 "                                             'INSERT 2017/03/14 AOKI

        Else

            '===データの読込み用SQL文作成ルーチン
            strSQL = "          SELECT  ZaiD_001 "
            strSQL = strSQL & "      ,  ZaiD_008 "
            strSQL = strSQL & "      ,  ZaiD_020 "
            strSQL = strSQL & "      ,  ZaiD_028 "
            strSQL = strSQL & "      ,  ZaiD_005 "
            strSQL = strSQL & "      ,  ZaiD_006 "
            strSQL = strSQL & "      ,  SUM(ZaiD_021) AS ZaiD_021 "
            'strSQL = strSQL & "      ,  SUM(ZaiD_024) AS ZaiD_024 "                            'DELETE 2012/12/07 AOKI
            strSQL = strSQL & "      ,  SUM((ZaiD_021 + ZaiD_032) * ZaiD_026) AS ZaiD_024 "     'INSERT 2012/12/07 AOKI
            strSQL = strSQL & "      ,  ZaiD_026 "
            strSQL = strSQL & "      ,  SUM(ZaiD_031) AS ZaiD_031 "
            strSQL = strSQL & "      ,  SUM(ZaiD_032) AS ZaiD_032 "
            strSQL = strSQL & "      ,  SUM(ZaiD_033) AS ZaiD_033 "
            strSQL = strSQL & "      ,  ZaiD_007 "                                              'INSERT 2017/03/14 AOKI
            strSQL = strSQL & " FROM    ZaiD_Dat "
            strSQL = strSQL & " WHERE   ISNULL(ZaiD_020,'') > ''"
            strSQL = strSQL & " AND     ZaiD_020 <= '" & NyDate & "'"

            'INSERT 2019/08/22 AOKI START -----------------------------------------------------------------------------------------
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%DELIVERY CHARGE%'"
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%D/C%'"
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%DISCOUNT%'"
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%CANCEL%'"
            strSQL = strSQL & " AND     ZaiD_005 NOT LIKE '%[%] ON'"
            strSQL = strSQL & " AND     ZaiD_005 <> '検査費'"
            strSQL = strSQL & " AND     ZaiD_021 + ZaiD_032 > 0"
            strSQL = strSQL & " AND     ZaiD_007 <> 343"
            strSQL = strSQL & " AND     ZaiD_026 <> 0"
            'INSERT 2019/08/22 AOKI E N D -----------------------------------------------------------------------------------------

            '仕入先名称であいまい検索
            If NCnvN(SName) <> "" Then
                strSQL = strSQL & " AND ZaiD_008 = '" & modCommon.EditSQLAddSQuot(SName) & "'"
            End If

            strSQL = strSQL & " AND (NOT EXISTS "
            strSQL = strSQL & "                 (SELECT * "
            strSQL = strSQL & "                  FROM   SeiD_Dat "
            strSQL = strSQL & "                  WHERE  SeiD_003 = ZaiD_001k "
            strSQL = strSQL & "                  AND    SeiD_002 <= '" & NyDate & "'"
            strSQL = strSQL & "                 ) "
            strSQL = strSQL & "     ) "

            strSQL = strSQL & " GROUP BY ZaiD_001 "
            strSQL = strSQL & "        , ZaiD_002 "
            strSQL = strSQL & "        , ZaiD_003 "
            strSQL = strSQL & "        , ZaiD_005 "
            strSQL = strSQL & "        , ZaiD_006 "
            strSQL = strSQL & "        , ZaiD_020 "
            strSQL = strSQL & "        , ZaiD_026 "
            strSQL = strSQL & "        , ZaiD_028 "
            strSQL = strSQL & "        , ZaiD_008 "
            strSQL = strSQL & "        , ZaiD_007 "                                             'INSERT 2017/03/14 AOKI
            strSQL = strSQL & " HAVING  SUM(ZaiD_021) + SUM(ZaiD_032) <> 0 "            '2012/09/03 INSERT AOKI
            strSQL = strSQL & " ORDER BY ZaiD_001 "
            strSQL = strSQL & "        , ZaiD_008 "
            strSQL = strSQL & "        , ZaiD_007 "                                             'INSERT 2017/03/14 AOKI
            strSQL = strSQL & "        , ZaiD_028 "
            strSQL = strSQL & "        , ZaiD_002 "
            strSQL = strSQL & "        , ZaiD_003 "
        End If

        CreateSQLText = strSQL

    End Function

    Private Sub setTotal(objRow As Object)

        If frmZAIMR030_optSelect_0.Checked = False Then
            '在庫数量
            curQuantity = curQuantity + NCnvZ(objRow![ZaiD_021]) + NCnvZ(objRow![ZaiD_032])
            '        curQuantity = curQuantity + NCnvZ(objRow![ZaiD_033])
            '仕入金額(税抜)
            curExcludingTax = curExcludingTax + NCnvZ(objRow![ZaiD_024])
            curTaxIncluded = curTaxIncluded + curSzeiKei                        'INSERT 2017/03/14 AOKI
        End If

    End Sub

    Private Sub writeTotal(xlsSheet As Excel.Worksheet)

        With xlsSheet

            If frmZAIMR030_optSelect_0.Checked = False Then
                '合計
                .Cells(mCurRows, 2).Value = "合計"
                '在庫数量
                .Cells(mCurRows, 6).Value = curQuantity
                '仕入金額(税込)
                .Cells(mCurRows, 8).Value = curTaxIncluded + curExcludingTax    'INSERT 2017/04/07 AOKI
                '仕入金額(税抜)
                .Cells(mCurRows, 9).Value = curExcludingTax
            End If

        End With

    End Sub
    '20120117 ADD END Yoshimura

    '***************************************************************
    '**  名称    : Function GetTargetSheet() As Long
    '**  機能    : 出力モードとカレントページから対象シート番号を返す。
    '**  戻り値  : 対象となるExcelシート番号
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function SeiDDatCheck(vsKNo As String) As Boolean

        Dim strSQL As String
        Dim loRSet As New DataTable

        On Error GoTo SeiDDatCheck_Err

        SeiDDatCheck = True

        '請求データ存在チェック
        strSQL = "SELECT * FROM SeiD_Dat"
        strSQL = strSQL & " WHERE SeiD_003 = '" & vsKNo & "'"
        strSQL = strSQL & " ORDER BY SeiD_003"

        'データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(strSQL)

        '===データ存在チェック
        If loRSet.Rows.Count > 0 Then

            If IsDate(NCnvN(loRSet.Rows(0)("SeiD_022"))) = True And NCnvN(loRSet.Rows(0)("SeiD_022")) > "1900/01/01" Then
                SeiDDatCheck = False
            End If
        End If

SeiDDatCheck_Exit:

        loRSet = Nothing
        On Error GoTo 0
        Exit Function

SeiDDatCheck_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Resume SeiDDatCheck_Exit

    End Function

    '***************************************************************
    '**  名称    : Function GetTargetSheet() As Long
    '**  機能    : 出力モードとカレントページから対象シート番号を返す。
    '**  戻り値  : 対象となるExcelシート番号
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function GetTargetSheet() As Long

        'モードによりシート番号を設定
        Select Case mCurMode
            Case 0  'プレビュー
                GetTargetSheet = mCurPage
            Case 1  '印刷
                GetTargetSheet = 1
        End Select

    End Function
    '***************************************************************
    '**  名称    : Sub sttWriteHeader
    '**  機能    : ヘッダー情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeader()

        With xlApp.Worksheets(GetTargetSheet)

            If frmZAIMR030_optSelect_0.Checked = True Then
                .Cells(1, 7).Value = Format(Date.Now, "YYYY年MM年DD月") & "  PAGE - " & mCurPage
            Else
                .Cells(1, 11).Value = Format(Date.Now, "YYYY年MM年DD月") & "  PAGE - " & mCurPage
            End If

        End With

    End Sub

    '***************************************************************
    '**  名称    : Sub sttWriteMeisai
    '**  機能    : 明細情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    : voRec:見積明細データ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMeisai(voRec As DataRow, xlsSheet As Excel.Worksheet)

        Dim curWork As Double

        'INSERT 2017/03/14 AOKI START -----------------------------------------------------------------------------------------
        Dim loSirMst(0 To 0) As SirMstInfo

        '得意先マスタ情報取得
        Erase loSirMst
        curSzeiKei = 0

        loSirMst(0).Sir_001 = NCnvZ(voRec("ZaiD_007"))
        Call GetSirMstInfo(loSirMst(0), 0)

        With TaxCalcInfo
            .ZeiKbn = loSirMst(0).Sir_019

            If loSirMst(0).Sir_020 = 0 Then
                .ZeiKeisan = loSirMst(0).Sir_020
            Else
                .ZeiKeisan = loSirMst(0).Sir_020 - 1
            End If

            .Kingaku = NCnvZ(voRec("ZaiD_024"))
            .DDate = NCnvN(voRec("ZaiD_020"))

            Call TaxComp.pTaxCalcCom(TaxCalcInfo)


            curSzeiKei = curSzeiKei + .Shohizei
        End With
        'INSERT 2017/03/14 AOKI E N D -----------------------------------------------------------------------------------------


        With xlsSheet
            '契約№
            .Cells(mCurRows, 2).Value = NCnvN(voRec("ZaiD_001"))

            If frmZAIMR030_optSelect_0.Checked = True Then
                '''            '在庫数量
                '''            curWork = NCnvZ(voRec![ZaiD_021]) + NCnvZ(voRec![ZaiD_032]) - NCnvZ(voRec![ZaiD_031])

                '仕入日付
                If IsDate(NCnvN(voRec("ZaiD_020"))) = True Then
                    .Cells(mCurRows, 3).Value = Format(NCnvN(voRec("ZaiD_020")), "YYYY/MM/DD")
                Else
                    .Cells(mCurRows, 3).Value = ""
                End If
                'チェック日
                If IsDate(NCnvN(voRec("ZaiD_028"))) = True Then
                    .Cells(mCurRows, 4).Value = Format(NCnvN(voRec("ZaiD_028")), "YYYY/MM/DD")
                Else
                    .Cells(mCurRows, 4).Value = ""
                End If
                '仕入先名称
                .Cells(mCurRows, 5).Value = NCnvN(voRec("ZaiD_008"))
                .Cells(mCurRows, 6).Value = NCnvZ(voRec("ZaiD_024")) + curSzeiKei
                .Cells(mCurRows, 7).Value = NCnvZ(voRec("ZaiD_024"))  '仕入金額(税抜)

            Else


                .Cells(mCurRows, 3).Value = NCnvN(voRec("ZaiD_008")) '仕入先名称
                .Cells(mCurRows, 4).Value = NCnvN(voRec("ZaiD_005")) '品名
                .Cells(mCurRows, 5).Value = NCnvN(voRec("ZaiD_006")) '品番

                '在庫数量
                curWork = NCnvZ(voRec("ZaiD_021")) + NCnvZ(voRec("ZaiD_032"))
                .Cells(mCurRows, 6).Value = curWork

                '仕入単価NET
                .Cells(mCurRows, 7).Value = NCnvZ(voRec("ZaiD_026"))

                .Cells(mCurRows, 8).Value = NCnvZ(voRec("ZaiD_024")) + curSzeiKei            'INSERT 2017/04/07 AOKI
                'UPDATE 2017/03/14 AOKI E N D -----------------------------------------------------------------------------------------

                '仕入金額(税抜)
                .Cells(mCurRows, 9).Value = NCnvZ(voRec("ZaiD_024"))


                '仕入日付
                If IsDate(NCnvN(voRec("ZaiD_020"))) = True Then
                    .Cells(mCurRows, 10).Value = Format(NCnvN(voRec("ZaiD_020")), "YYYY/MM/DD")
                Else
                    .Cells(mCurRows, 10).Value = ""
                End If
                'チェック日
                If IsDate(NCnvN(voRec("ZaiD_028"))) = True Then
                    .Cells(mCurRows, 11).Value = Format(NCnvN(voRec("ZaiD_028")), "YYYY/MM/DD")
                Else
                    .Cells(mCurRows, 11).Value = ""
                End If
            End If

        End With

    End Sub

    '***************************************************************
    '**  名称    : Sub CheckPages
    '**  機能    : 出力ページと行のチェックと改頁処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub CheckPages()

        '行をインクリメント
        mCurRows = mCurRows + 1

        '行チェック
        If mCurRows > pstLastRow Then
            mCurPage = mCurPage + 1     'ページインクリメント
            '改頁処理
            Select Case mCurMode
                Case 0  'プレビュー
                    Call stePrevSheetCopy(mCurPage)              'プレビュー・印刷用Bookのコピー
                    Call loSheetClear()                            'Excelシートクリア
                Case 1  '印刷
                    Call steExcelPrint(1, gbl.ActivePrinter.Printer) 'Excel 印刷
                    Call steSheetClear(1, pstPrintArea)          'Excelシートクリア
            End Select

            mCurRows = pstStartRow    '行をリセット
            Call sttWriteHeader()       'ヘッダー情報の出力
            brkTokNm = ""
        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub loSheetClear
    '**  機能    : シートのクリア
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub loSheetClear()

        Call steSheetClear(GetTargetSheet, pstPrintArea)

    End Sub

    '***************************************************************
    '**  名称    : Function JYUMR080ExcelProc() As Boolean
    '**  機能    : エクセル出力実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function JYUMR080ExcelProc() As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim RecMeisai As New DataTable  '明細データ
        Dim strPath As String
        Dim strWk As String

        On Error GoTo JYUMR080ExcelProc_Err

        JYUMR080ExcelProc = False

        RecMeisai = Nothing

        '===データの読込み用SQL文作成ルーチン
        strSQL = CreateSQLText()

        '===データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        RecMeisai = DB.GetDatafromDB(strSQL)

        '===データ存在チェック
        If RecMeisai.Rows.Count <= 0 Then
            'データ無しメッセージ
            Call ksExpMsgBox("対象データが存在しません。", "I")
            GoTo Exit_JYUMR080ExcelProc
        End If

        '==========================================
        '   テンプレートExcelファイルのオープン
        '==========================================
        strPath = Trim$(clswinApi.StaticWinApi.WsReportDir) & "\" & psttREPORT_NAME2 & exXLSSuffix
        xlBookTem = xlApp.Workbooks.Open(strPath)
        '新規ブックとしてオープン
        xlBookTem.Activate()
        xlBookTem.Sheets(1).Select
        xlBookTem.Sheets(1).Copy
        xlBookNew = xlApp.ActiveWorkbook


        '===各変数初期化
        mCurPage = 1         'ページ番号
        mCurRows = 3         '明細印刷開始行
        DCount = 0           'データカウンタ

        curQuantity = 0
        curExcludingTax = 0
        curTaxIncluded = 0          'INSERT 2017/03/14 AOKI

        For Each dr As DataRow In RecMeisai.Rows

            '進行状況用カウンタ
            DCount = DCount + 1


            If mCurRows <> 3 Then
                '3行目以降の枠線
                Call WriteLine()
            End If

            Call sttWriteMeisai(dr, xlBookNew.Worksheets(mCurPage))   '明細情報出力
            mCurRows = mCurRows + 1

            '合計格納
            Call setTotal(RecMeisai)

        Next


        If frmZAIMR030_optSelect_0.Checked = False Then
            Call WriteLine()
        End If

        '合計表示
        Call writeTotal(xlApp.Worksheets(GetTargetSheet))

        xlWs = xlBookNew.Worksheets(mCurPage)
        With xlBookNew.Worksheets(mCurPage)
            '罫線
            If frmZAIMR030_optSelect_0.Checked = True Then
                strWk = "B" & mCurRows & ":G" & mCurRows
            Else
                strWk = "B" & mCurRows & ":K" & mCurRows
            End If

            xlRange = xlWs.Range(strWk)
            xlRange.Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = Excel.XlLineStyle.xlContinuous
            xlRange.Borders(Excel.XlBordersIndex.xlEdgeTop).Weight = Excel.XlBorderWeight.xlThin
        End With

        Call loWorkbooksClose()      'Bookのクローズ

        JYUMR080ExcelProc = True

        xlApp.Visible = True
        xlApp.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized
        xlBookNew.Worksheets(1).Select()
        xlApp.Range("A1").Select()


Exit_JYUMR080ExcelProc:

        RecMeisai = Nothing
        Exit Function

JYUMR080ExcelProc_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Call loWorkbooksClose()         'Bookのクローズ

        Resume Exit_JYUMR080ExcelProc

    End Function
    '***************************************************************
    '**  名称    : Sub loWorkbooksClose()
    '**  機能    : 各ワークブックのクローズ(解放)処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub loWorkbooksClose()

        On Error Resume Next
        'Bookのクローズ
        xlBookTem.Close(SaveChanges:=False)
        ' オブジェクトを解放します。
        xlBookTem = Nothing

        On Error GoTo 0

    End Sub
    '***************************************************************
    '**  名称    : Sub WriteLine()
    '**  機能    : 明細　罫線
    '***************************************************************
    Private Sub WriteLine()

        Dim strWk As String

        On Error Resume Next
        xlWs = xlBookNew.Worksheets(mCurPage)
        With xlBookNew.Worksheets(mCurPage)

            If frmZAIMR030_optSelect_0.Checked = True Then
                strWk = "B" & mCurRows & ":G" & mCurRows
            Else
                strWk = "B" & mCurRows & ":K" & mCurRows
            End If

            xlRange = xlWs.Range(strWk)
        End With

        With xlRange

            .Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlEdgeLeft).Weight = Excel.XlBorderWeight.xlThick
            .Borders(Excel.XlBordersIndex.xlEdgeLeft).ColorIndex = 15

            .Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlEdgeRight).Weight = Excel.XlBorderWeight.xlThick
            .Borders(Excel.XlBordersIndex.xlEdgeRight).ColorIndex = 15

            .Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlInsideVertical).Weight = Excel.XlBorderWeight.xlThin
            .Borders(Excel.XlBordersIndex.xlInsideVertical).ColorIndex = 15

            .Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlEdgeBottom).Weight = Excel.XlBorderWeight.xlThin
            .Borders(Excel.XlBordersIndex.xlEdgeBottom).ColorIndex = 15
        End With

        On Error GoTo 0

    End Sub

#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = winApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    winApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk + gstStrSEPFILE +
                     winApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next

            If xlApp IsNot Nothing Then
                xlApp.Quit()
                xlApp = Nothing
            End If
        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub

    Private Sub steSheetClear(ByVal vlSheet As Long, ByVal vsCell As String)
        xlApp.Worksheets(vlSheet).Range(vsCell).ClearContents()
    End Sub
#End Region
End Module
