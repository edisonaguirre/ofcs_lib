﻿Imports System.Windows.Forms
Imports globalcls.modHanbai
Imports clswinApi
Imports SAHANBAI.clsGlobal
'Imports Constant = SAHANBAI.Constant
Imports winApi = clswinApi.StaticWinApi
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
''all database transaction were put here , all control related
Public Module modJYUMR020

    '***************************************************************
    '**
    '**  機能           :  契約台帳発行(modJYUMR020)
    '**  作成日         :
    '**  更新日         :
    '**  備考           :
    '**
    '***************************************************************
    '各マスタ情報
    Public ConMst(0 To 0) As ConMstInfo     'コントロールマスタ情報
    Public TokMst(0 To 0) As TokMstInfo     '得意先マスタ


    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
    End Enum

    '契約台帳印刷条件
    Public ourRefNoP() As String   '契約番号指定
    Public strHakkoDate As String   '注文請書発行日付


    '契約台帳ヘッダ項目
    Private Structure JYUMR020Hed
        Public KEIYAKUNO As String      '契約№
        Public KEIYAKUDATE As String      '契約日
        Public Messrs As String      '客先名
        Public Attn As String      '客先担当者名
        Public YourRefNo As String      '客先依頼Ｎｏ
        Public YourOrderNo As String      '客先注文Ｎｏ
        Public VesselsName As String      '船名
        Public sDate As String      '日付
        Public TantoNm As String      '当社担当者
        Public OurEstimateNo As String  '見積Ｎｏ
        Public MITDATE As String      '見積日
        Public MitNouki As String      '見積時期納期
        Public Source As String      'ソース
        Public Tekiyo As String      '摘要
        Public NyukaYotei As String      '入荷予定
        Public NyukaBasho As String      '入荷場所
        Public Ukewatashi As String      '出荷先場所
        Public UriKingaku As Double    '売上金額
        Public SirKingaku As Double    '仕入金額
        Public Rieki As Double    '利益
    End Structure
    Private printHed(0 To 0) As JYUMR020Hed

    'プレビューからの見積発行日更新用キー'
    Private KeiyakuNoWk As String   '契約№
    Private RefNoWk As String   '見積№
    Public Structure JYUMR020Upd
        Public KEIYAKUNO As String         '契約№
        Public RefNo As String         '見積№
    End Structure
    Public JyuHUPDKey() As JYUMR020Upd

    'エクセル上のセル範囲
    Private pstLastRow As Integer     '最終行
    '== 契約台帳のExcel定義 ==
    Private Const psttREPORT_NAME As String = "JYUMR020_契約台帳"          'Report名称
    Private Const psttREPORT_NAME1 As String = "JYUMR020_契約台帳(鑑)"     'Report名称
    Private Const psttREPORT_NAME2 As String = "JYUMR020_契約台帳(明細)"   'Report名称
    Private Const pstPrintArea1 As String = "B21:BJ46"                  'シートクリア範囲(鑑)
    Private Const pstPrintArea1A As String = "B14:BJ17"                 'シートクリア範囲(鑑)
    Private Const pstPrintArea2 As String = "B7:BJ45"                   'シートクリア範囲(明細)
    Private Const pstPrevArea As String = "A1:BK46"                    'プレビュー表示範囲
    Private Const pstMaxRow1 As Integer = 25                           '最大行数
    Private Const pstMaxRow2 As Integer = 38                           '最大行数
    Private Const pstStartRow1 As Integer = 21                          'ページ内の開始行(鑑)
    Private Const pstStartRow2 As Integer = 7                           'ページ内の開始行(明細)
    Private Const pstPrintP As Integer = 1

    '印刷共通定義
    Private mCurMode As Integer     'カレント印刷モード
    Private mCurPage As Long        'カレントページ番号
    Private mCurRows As Long        'カレント行
    Private mCurCols As Long        'カレント列
    Private PageWk(1) As Integer     '見積№毎のページ印字用ワーク(シートのFROM-TO)
    Private BefSeq As Integer     'SEQブレークチェック用

    Private xlApp As Excel.Application
    Private xlBookTemp As Excel.Workbook   'テンプレートブック
    Private xlBookWork As Excel.Workbook   'ワーク用ブック
    Private DB As New clsDB.DBService

    '***************************************************************
    '**  名称    : Sub main()
    '**  機能    : 契約台帳発行メイン
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    'Sub Main()

    '    '初期情報の設定
    '    If InitialLoad = True Then
    '        'フォームのオープン
    '        frmJYUMR020.Load
    '    End If

    'End Sub

    '***************************************************************
    '**  名称    : Function JYUMR020PrintProc() As Boolean
    '**  機能    : 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function JYUMR020PrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim RecHed As New DataTable  '見積ヘッダデータ
        Dim RecMeisai As New DataTable  '見積明細データ
        Dim iCnt As Integer              'カウント

        On Error GoTo JYUMR020PrintProc_Err

        JYUMR020PrintProc = False

        mCurMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存

        RecHed = Nothing

        '===受注ヘッダデータの読込み用SQL文作成ルーチン
        strSQL = CreateSQLText(0)

        '===受注ヘッダデータの読込み
        DB = New clsDB.DBService
        RecHed = DB.GetDatafromDB(strSQL)

        '===データ存在チェック
        If RecHed.Rows.Count <= 0 Then
            'データ無しメッセージ
            Call ksExpMsgBox("印刷するデータが存在しません。", "I")
            GoTo Exit_JYUMR020PrintProc
        End If

        '===テンプレートExcelファイルのオープン
        xlApp = CreateObject("Excel.Application")
        xlBookTemp = xlApp.Workbooks.Open(Strings.Trim(winApi.WsReportDir) &
                                "\" & psttREPORT_NAME & modHanbai.exXLSSuffix)

        ''** 印刷モード時もプレビュー時と同様にワークファイルを作成して印刷を行う **
        ''シート1(鑑)のシートをコピー（ワークファイル用に新規ブックとしてオープン）
        'xlBookTemp.Sheets(1).Select
        'xlBookTemp.Sheets(1).Copy
        xlBookWork = xlApp.ActiveWorkbook

        If mCurMode = enmPrintmode.Preview Then        'プレビューモード
            Call stePrevWorkDelete()  '古いワークファイルを削除する
        End If

        '===各変数初期化
        mCurPage = 1                  'シート番号
        mCurRows = pstStartRow1 - 2   '印刷開始行
        pstLastRow = pstMaxRow1 + pstStartRow1 - 1   '最終行
        DCount = 0                    'データカウンタ
        PageWk(0) = mCurPage          '契約№毎のページ印字用ワーク(シートのFROM)

        'シートのクリア
        Call loSheetClear(pstPrintArea1)
        Call loSheetClear(pstPrintArea1A)

        '指定された契約番号を１件ずつ読む
        For iCnt = LBound(ourRefNoP) To UBound(ourRefNoP)

            '進行状況用カウンタ
            DCount = DCount + 1

            '受注ヘッダデータの読込み
            RecHed = Nothing
            'SQL作成
            strSQL = CreateSQLText(1, ourRefNoP(iCnt))
            RecHed = DB.GetDatafromDB(strSQL)


            'ヘッダー情報を変数にセット
            Call sttSetHeader(RecHed.Rows(0))

            '２件目以降　改ページさせる
            If DCount > 1 Then
                Call CheckPages(0)
                '契約№毎のページ印字用ワーク(シートのTO)
                PageWk(1) = mCurPage - 1
                'ヘッダー　ページを出力、印刷時は印刷する。
                Call sttWriteHeaderPage()
                PageWk(0) = mCurPage
            End If

            'プレビューからの注文請書発行日更新用キー
            KeiyakuNoWk = modHanbai.NCnvN(RecHed.Rows(0)("JyuH_001"))         '契約№
            RefNoWk = modHanbai.NCnvN(RecHed.Rows(0)("JyuH_009"))             '見積№
            '１ページ目キー
            ReDim Preserve JyuHUPDKey(mCurPage)
            JyuHUPDKey(mCurPage).KEIYAKUNO = KeiyakuNoWk   '契約№
            JyuHUPDKey(mCurPage).RefNo = RefNoWk           '見積№

            '受注明細データの読込み用SQL作成
            strSQL = CreateSQLText(2, "", RecHed.Rows(0))

            '受注明細データの読込み
            RecMeisai = Nothing
            RecMeisai = DB.GetDatafromDB(strSQL)

            'データ存在チェック
            If RecMeisai.Rows.Count > 0 Then

                'ヘッダー情報をExcelに出力
                Call sttWriteHeader(0)

                BefSeq = -9999      'SEQブレークチェック用

                '読込んだ明細データが無くなるまで繰返す
                For Each dr As DataRow In RecMeisai.Rows
                    Call sttWriteMeisai(dr)
                Next

            End If

            '＜印刷時＞注文請書発行日を更新
            If viMode = enmPrintmode.PrintOut Then
                Call JYUMR020_JyuHUpdate(RecHed.Rows(0)("JyuH_001"), RecHed.Rows(0)("JyuH_009"))
            End If

        Next iCnt




        '契約№毎のページ印字用ワーク(シートのTO)
        PageWk(1) = mCurPage
        'ヘッダー　契№毎のページを出力、印刷時は印刷する。
        Call sttWriteHeaderPage()

        'モードごとの処理
        Select Case viMode

            Case enmPrintmode.Preview      'プレビュー
                '保
                xlBookWork.SaveAs(Filename:=Trim(winApi.WsReportDir) & gstStrSEPPATH &
                                        "SPW" & gstStrSEPFILE &
                                        winApi.Wsnumber & modHanbai.exXLSSuffix,
                                        FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                        ReadOnlyRecommended:=False, CreateBackup:=False)
                Call loWorkbooksClose()         'Bookのクローズ



                'プレビュー画面の起動
                Dim frm As New SAHANBAI.frmPrintPreview
                With frm

                    .tagJYUMR020_JyuHUpdate = True
                    .ppvActCell = pstPrevArea
                    .ShowDialog()
                End With

            Case enmPrintmode.PrintOut     '印刷
                'Bookのクローズ
                Call loWorkbooksClose()

        End Select

        xlBookWork = Nothing
        xlBookTemp = Nothing

        JYUMR020PrintProc = True

Exit_JYUMR020PrintProc:

        RecHed = Nothing
        RecMeisai = Nothing
        DB = Nothing
        On Error GoTo 0
        Exit Function

JYUMR020PrintProc_Err:

        RecHed = Nothing
        RecMeisai = Nothing
        DB = Nothing
        Call ksExpMsgBox(Err.Number & " :  " & Err.Description, "E")
        Call loWorkbooksClose()         'Bookのクローズ


        Resume Exit_JYUMR020PrintProc

    End Function

    '********************************************************************
    '**  名称    : Function CreateSQLText() String
    '**  機能    : 受注データ読み込み用SQL作成処理
    '**  戻り値  : 作成されたSQL文字列
    '**  引数    : iMode 0:印刷データ確認 iMode 1:受注ヘッダ　2：受注明細
    '**          : vKey  受注ヘッダデータKEY(受注ヘッダ読込時使用）
    '**          : voRec 受注ヘッダデータ(明細データ読込時使用）
    '**  備考    :
    '********************************************************************
    Private Function CreateSQLText(iMode As Integer, Optional vKey As String = "",
                               Optional voRec As DataRow = Nothing) As String

        Dim strSQL As String
        Dim strWhere As String
        Dim iWk As Integer

        Select Case iMode
            Case 0  '印刷データ確認
                strSQL = strSQL & "SELECT * FROM JyuH_Dat"
                strSQL = strSQL & " WHERE 1 = 1 "                  'データ区分
                '受注番号
                strSQL = strSQL & " AND ("
                For iWk = LBound(ourRefNoP) To UBound(ourRefNoP)
                    If strWhere = "" Then
                        strWhere = " JyuH_001 ='" & ourRefNoP(iWk) & "'"
                    Else
                        strWhere = strWhere & " OR JyuH_001 ='" & ourRefNoP(iWk) & "'"
                    End If
                Next iWk
                strSQL = strSQL & strWhere
                strSQL = strSQL & ")"

            Case 1  'ヘッダデータ
                '受注番号指定
                strSQL = "SELECT TH.*,TD.* FROM JyuH_Dat TH"
                strSQL = strSQL & ",(SELECT JyuD_001,SUM(JyuD_0241) AS JyuD_0241,SUM(JyuD_0261) AS JyuD_0261"
                strSQL = strSQL & "    FROM JyuD_Dat WHERE JyuD_001 ='" & vKey & "'"
                strSQL = strSQL & "   GROUP BY JyuD_001) TD"
                strSQL = strSQL & " WHERE TH.JyuH_001 ='" & vKey & "'"
                strSQL = strSQL & "   AND TH.JyuH_001 = TD.JyuD_001 "

            Case 2  '見積明細データ
                strSQL = strSQL & "SELECT * FROM JyuM_Dat TM,JyuD_Dat TD"
                strSQL = strSQL & " WHERE TM.JyuM_001 = '" & voRec("JyuH_001") & "'"   '契約№
                strSQL = strSQL & "   AND TD.JyuD_001 = TM.JyuM_001"                  '契約№
                strSQL = strSQL & "   AND TD.JyuD_003 = TM.JyuM_003"                  'SEQ
                strSQL = strSQL & " ORDER BY TM.JyuM_001,TM.JyuM_003,TD.JyuD_004  "
        End Select

        CreateSQLText = strSQL

    End Function

    '***************************************************************
    '**  名称    : Sub sttSetHeader
    '**  機能    : ヘッダー情報を変数にセットする。
    '**  戻り値  :
    '**  引数    : voRec:見積ヘッダデータ
    '**  備考    :
    '***************************************************************
    Private Sub sttSetHeader(voRec As DataRow)

        Erase printHed
        ReDim Preserve printHed(0)
        With printHed(0)
            .KEIYAKUNO = modHanbai.NCnvN(voRec("JyuH_001"))      '契約№
            .KEIYAKUDATE = modHanbai.NCnvN(voRec("JyuH_002"))    '契約日
            .Messrs = modHanbai.NCnvN(voRec("JyuH_003"))         '客先名
            .Attn = modHanbai.NCnvN(voRec("JyuH_004"))           '客先担当者名
            .YourRefNo = modHanbai.NCnvN(voRec("JyuH_005"))      '客先依頼№
            .YourOrderNo = modHanbai.NCnvN(voRec("JyuH_017"))    '客先注文№
            .VesselsName = modHanbai.NCnvN(voRec("JyuH_006"))    '船名
            .sDate = modHanbai.NCnvN(voRec("JyuH_002"))           '日付
            .TantoNm = modHanbai.NCnvN(voRec("JyuH_008"))        '当社担当者
            .OurEstimateNo = modHanbai.NCnvN(voRec("JyuH_009"))  '見積№
            .MITDATE = modHanbai.NCnvN(voRec("JyuH_010"))        '見積日
            .MitNouki = modHanbai.NCnvN(voRec("JyuH_011"))       '見積時期納期
            .Source = modHanbai.NCnvN(voRec("JyuH_012"))         'ソース
            .Tekiyo = modHanbai.NCnvN(voRec("JyuH_014"))         '摘要
            .NyukaYotei = modHanbai.NCnvN(voRec("JyuH_018"))     '入荷予定
            .NyukaBasho = modHanbai.NCnvN(voRec("JyuH_019"))     '入荷場所
            .Ukewatashi = modHanbai.NCnvN(voRec("JyuH_020"))     '出荷先場所
            .UriKingaku = CDbl(modHanbai.NCnvZ(voRec("JyuD_0261"))) '売上金額
            .SirKingaku = CDbl(modHanbai.NCnvZ(voRec("JyuD_0241")))   '仕入金額
            .Rieki = .UriKingaku - .SirKingaku        '利益
        End With

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteHeader
    '**  機能    : ヘッダー情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    : viMode:0:鑑　1:明細ページ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeader(viMode As Integer)

        If viMode = 0 Then
            '鑑
            With xlBookWork.Worksheets(mCurPage)
                .Cells(5, 5).Value = printHed(0).Messrs          '客先名
                .Cells(6, 5).Value = printHed(0).VesselsName     '船名
                .Cells(7, 5).Value = printHed(0).Attn            '客先担当者名
                .Cells(8, 5).Value = printHed(0).Source          'ソース
                .Cells(9, 5).Value = printHed(0).Tekiyo          '摘要

                .Cells(2, 25).Value = printHed(0).YourOrderNo    '客先依頼№
                .Cells(3, 25).Value = printHed(0).KEIYAKUNO      '契約№
                .Cells(4, 25).Value = modCommon.CnvDate(printHed(0).KEIYAKUDATE, "yyyy/MM/dd")    '契約日

                .Cells(6, 25).Value = printHed(0).OurEstimateNo  '見積№
                .Cells(7, 25).Value = modCommon.CnvDate(printHed(0).MITDATE, "yyyy/MM/dd")        '見積日
                .Cells(8, 25).Value = printHed(0).MitNouki       '見積時期納期
                .Cells(9, 25).Value = modCommon.CnvDate(printHed(0).NyukaYotei, "yyyy/MM/dd")     '入荷予定
                .Cells(10, 25).Value = printHed(0).NyukaBasho    '入荷場所
                .Cells(11, 25).Value = printHed(0).Ukewatashi    '出荷先場所

                .Cells(2, 39).Value = Format(printHed(0).UriKingaku, "#,##0")  '売上金額
                .Cells(2, 44).Value = Format(printHed(0).SirKingaku, "#,##0")  '仕入金額
                .Cells(2, 49).Value = Format(printHed(0).Rieki, "#,##0")       '利益

                .Cells(2, 62).Value = "作成日：" & Format(DateTime.Now, "yyyy/MM/dd")
            End With

            '出荷データ印字
            Call Write_Shuka(printHed(0).KEIYAKUNO)
        Else
            '明細ページ
            With xlBookWork.Worksheets(mCurPage)
                .Cells(2, 21).Value = printHed(0).Messrs         '客先名
                .Cells(3, 21).Value = printHed(0).VesselsName    '船名
                .Cells(2, 41).Value = printHed(0).KEIYAKUNO      '契約№
                .Cells(3, 41).Value = modCommon.CnvDate(printHed(0).KEIYAKUDATE, "yyyy/MM/dd")    '契約日
                .Cells(2, 62).Value = "作成日：" & Format(DateTime.Now, "yyyy/MM/dd")
            End With
        End If

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteHeaderPage
    '**  機能    : ページを出力、印刷時は印刷する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeaderPage()

        Dim lWk As Long
        Dim lCnt As Long
        Dim AllPage As Long
        Dim SheetWk As Long

        AllPage = PageWk(1) - PageWk(0) + 1  '全ページ数
        SheetWk = PageWk(0)                  '開始シート

        For lWk = 1 To AllPage
            lCnt = lCnt + 1
            'ページ印字
            With xlBookWork.Worksheets(SheetWk)
                .Cells(1, 62).Value = "PAGE - " & lCnt
            End With

            '印刷時
            If mCurMode = enmPrintmode.PrintOut Then
                xlBookWork.Activate()
                Call steExcelPrint(SheetWk, gbl.ActivePrinter.Printer)
            End If

            SheetWk = SheetWk + 1
        Next lWk

    End Sub

    '***************************************************************
    '**  名称    : Sub sttWriteMeisai
    '**  機能    : 明細情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    : voRec:見積明細データ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMeisai(voRec As DataRow)

        Dim strWk As String
        Dim strSQL As String
        Dim loRec As New DataTable

        Dim DB As New clsDB.DBService
        Try
            '中ヘッダ
            If BefSeq <> voRec("JyuM_003") Then
                Call CheckPages(1)

                'UNIT
                If modHanbai.NCnvN(voRec("JyuM_004")) <> "" Then
                    Call CheckPages(1)
                    With xlBookWork.Worksheets(mCurPage)
                        '                .Cells(mCurRows, 2).ShrinkToFit = False   '全体を縮小しない
                        .Cells(mCurRows, 15).Value = "UNIT: " & modHanbai.NCnvN(voRec![JyuM_004])
                    End With
                End If
                'MAKER
                If modHanbai.NCnvN(voRec("JyuM_005")) <> "" Then
                    Call CheckPages(1)
                    With xlBookWork.Worksheets(mCurPage)
                        '                .Cells(mCurRows, 2).ShrinkToFit = False   '全体を縮小しない
                        .Cells(mCurRows, 15).Value = "MAKER:" & modHanbai.NCnvN(voRec![JyuM_005])
                    End With
                End If
                'TYPE
                If modHanbai.NCnvN(voRec("JyuM_006")) <> "" Then
                    Call CheckPages(1)
                    With xlBookWork.Worksheets(mCurPage)
                        '                .Cells(mCurRows, 2).ShrinkToFit = False   '全体を縮小しない
                        .Cells(mCurRows, 15).Value = "TYPE:" & modHanbai.NCnvN(voRec![JyuM_006])
                    End With
                End If
                'SER/NO
                If modHanbai.NCnvN(voRec("JyuM_006")) <> "" Then
                    Call CheckPages(1)
                    With xlBookWork.Worksheets(mCurPage)
                        '                .Cells(mCurRows, 2).ShrinkToFit = False   '全体を縮小しない
                        .Cells(mCurRows, 15).Value = "SER/NO:" & modHanbai.NCnvN(voRec![JyuM_007])
                    End With
                End If
                'DWG NO
                If modHanbai.NCnvN(voRec("JyuM_008")) <> "" Then
                    Call CheckPages(1)
                    With xlBookWork.Worksheets(mCurPage)
                        '                .Cells(mCurRows, 2).ShrinkToFit = False   '全体を縮小しない
                        .Cells(mCurRows, 15).Value = "DWG NO:" & modHanbai.NCnvN(voRec![JyuM_008])
                    End With
                End If
                'ADDITIONAL DETAILS
                If modHanbai.NCnvN(voRec("JyuM_025")) <> "" Then
                    Call CheckPages(1)
                    With xlBookWork.Worksheets(mCurPage)
                        '                .Cells(mCurRows, 2).ShrinkToFit = False   '全体を縮小しない
                        .Cells(mCurRows, 15).Value = "ADDITIONAL DETAILS:" & modHanbai.NCnvN(voRec![JyuM_025])
                    End With
                End If
            End If

            'SEQブレークチェック用
            BefSeq = voRec![JyuM_003]

            '改ページチェック
            Call CheckPages(1)

            With xlBookWork.Worksheets(mCurPage)

                strWk = modHanbai.NCnvN(voRec![JyuD_004])

                If Right(strWk, 1) = "0" Then
                    '品名のみ
                    .Cells(mCurRows, 15).Value = modHanbai.NCnvN(voRec![JyuD_005])
                Else
                    'SEQ
                    .Cells(mCurRows, 11).Value = modHanbai.NCnvN(voRec![JyuD_003])
                    '項目
                    strWk = Left(strWk, Len(strWk) - 1)
                    .Cells(mCurRows, 13).Value = strWk
                    '品名
                    .Cells(mCurRows, 15).Value = modHanbai.NCnvN(voRec![JyuD_005])
                    '品番
                    .Cells(mCurRows, 21).Value = modHanbai.NCnvN(voRec![JyuD_006])
                    '数量
                    .Cells(mCurRows, 25).Value = Format(CDbl(modHanbai.NCnvZ(voRec![JyuD_018])), "#,##0")
                    '単位
                    .Cells(mCurRows, 28).Value = modHanbai.NCnvN(voRec![JyuD_019])
                    '販売単価
                    .Cells(mCurRows, 30).Value = Format(CDbl(modHanbai.NCnvZ(voRec![JyuD_020])), "#,##0")
                    '販売金額
                    .Cells(mCurRows, 34).Value = Format(CDbl(modHanbai.NCnvZ(voRec![JyuD_021])), "#,##0")

                    '発注仕入データ読込み
                    '仕入単価、掛率、単価ＮＥＴ、仕入金額取得
                    strSQL = "SELECT HatD_001,HatD_002,HatD_003"
                    strSQL = strSQL & ",MAX(HatD_007) AS SIRNM,MAX(HatD_019) AS SIRTANKA"
                    strSQL = strSQL & ",MAX(HatD_020) AS KRITSU,MAX(HatD_021) AS SIRNET"
                    strSQL = strSQL & ",MAX(HatD_022) AS SIRKIN"
                    strSQL = strSQL & " FROM HatD_Dat"
                    strSQL = strSQL & " WHERE HatD_001 ='" & voRec("JyuD_001") & "'"   '契約№
                    strSQL = strSQL & "   AND HatD_002 = " & voRec("JyuD_003")       'SEQ
                    strSQL = strSQL & "   AND HatD_003 = " & voRec("JyuD_004")       '項目
                    strSQL = strSQL & " GROUP BY HatD_001,HatD_002,HatD_003"
                    loRec = DB.GetDatafromDB(strSQL)

                    If loRec.Rows.Count > 0 Then
                        '仕入先
                        .Cells(mCurRows, 2).ShrinkToFit = True       '全体を縮小する
                        .Cells(mCurRows, 2).Value = modHanbai.NCnvN(loRec.Rows(0)("SIRNM"))

                        '仕入単価
                        .Cells(mCurRows, 38).Value = Format(CDbl(modHanbai.NCnvZ(loRec.Rows(0)("SIRTANKA"))), "#,##0")
                        '掛率
                        .Cells(mCurRows, 42).Value = Format(CDbl(modHanbai.NCnvZ(loRec.Rows(0)("SIRTANKA"))), "##0")
                        '単価ＮＥＴ
                        .Cells(mCurRows, 44).Value = Format(CDbl(modHanbai.NCnvZ(loRec.Rows(0)("SIRNET"))), "#,##0")
                        '仕入金額
                        .Cells(mCurRows, 48).Value = Format(CDbl(modHanbai.NCnvZ(loRec.Rows(0)("SIRKIN"))), "#,##0")

                        loRec = Nothing
                    End If

                    '発注日付 mm/dd
                    If modHanbai.NCnvN(voRec("JyuD_023")) <> "" Then
                        .Cells(mCurRows, 52).Value = Strings.Right(voRec("JyuD_023"), 5)
                    End If
                    '仕入日付 mm/dd
                    If modHanbai.NCnvN(voRec("JyuD_025")) <> "" Then
                        .Cells(mCurRows, 54).Value = Strings.Right(voRec("JyuD_025"), 5)
                    End If
                    '売上日付 mm/dd
                    If modHanbai.NCnvN(voRec("JyuD_027")) <> "" Then
                        .Cells(mCurRows, 56).Value = Strings.Right(voRec("JyuD_027"), 5)
                    End If
                    '請求日付 mm/dd
                    If modHanbai.NCnvN(voRec("JyuD_029")) <> "" Then
                        .Cells(mCurRows, 58).Value = Strings.Right(voRec("JyuD_029"), 5)
                    End If

                End If
            End With
        Catch ex As Exception
            DB = Nothing
            MsgBox(ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error")
        Finally
            DB = Nothing
        End Try


    End Sub

    '***************************************************************
    '**  名称    : Sub CheckPages
    '**  機能    : 出力ページと行のチェックと改頁処理。
    '**  戻り値  :
    '**  引数    : viMode:0:ヘッダが変わった時　1:明細改ページ時
    '**  備考    :
    '***************************************************************
    Private Sub CheckPages(viMode As Integer)

        mCurRows = mCurRows + 1     'ページインクリメント

        Select Case viMode
            Case 0     'ヘッダが変わった時
                mCurPage = mCurPage + 1            'ページインクリメント
                '改頁処理
                Call loPrevSheetCopy(0)            'プレビュー・印刷用Bookのコピー
                'Excelシートクリア
                Call loSheetClear(pstPrintArea1A)
                Call loSheetClear(pstPrintArea1)


                'プレビューからの見積発行日更新用キー作成
                ReDim Preserve JyuHUPDKey(mCurPage)
                JyuHUPDKey(mCurPage).KEIYAKUNO = KeiyakuNoWk      '契約№
                JyuHUPDKey(mCurPage).RefNo = RefNoWk              '見積№

                mCurRows = pstStartRow1 - 2                  '行をリセット
                pstLastRow = pstMaxRow1 + pstStartRow1 - 1   '契約台帳（鑑）の最終行

                Call sttWriteHeader(0)                       'ヘッダー情報の出力

            Case 1      '明細改ページ時
                '行チェック
                If mCurRows > pstLastRow Then
                    mCurPage = mCurPage + 1            'ページインクリメント
                    '改頁処理
                    Call loPrevSheetCopy(1)            'プレビュー・印刷用Bookのコピー
                    Call loSheetClear(pstPrintArea2)   'Excelシートクリア

                    'プレビューからの見積発行日更新用キー作成
                    ReDim Preserve JyuHUPDKey(mCurPage)
                    JyuHUPDKey(mCurPage).KEIYAKUNO = KeiyakuNoWk      '契約№
                    JyuHUPDKey(mCurPage).RefNo = RefNoWk              '見積№

                    mCurRows = pstStartRow2    '行をリセット
                    pstLastRow = pstMaxRow2 + pstStartRow2 - 1   '契約台帳（明細）の最終行
                    Call sttWriteHeader(1)           'ヘッダー情報の出力
                End If
        End Select

    End Sub

    '***************************************************************
    '**  名称    : Sub JYUMR020_JyuHUpdate
    '**  機能    : 契約台帳発行日を更新
    '**  戻り値  :
    '**  引数    : vKey1: 契約№　vkey2:見積№
    '**  備考    :
    '***************************************************************
    Public Sub JYUMR020_JyuHUpdate(vKey1 As String, vKey2 As String)

        Dim strWk As String
        Try
            strWk = "UPDATE JyuH_Dat "
            strWk = strWk & " SET JyuH_030 = '" & strHakkoDate & "'"
            strWk = strWk & " WHERE JyuH_001 = '" & vKey1 & "'"
            strWk = strWk & "   AND JyuH_009 = '" & vKey2 & "'"
            Dim DB As New clsDB.DBService
            DB.ExecuteSql(strWk)
        Catch ex As Exception
            DB = Nothing
            MsgBox(ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error")
        Finally
            DB = Nothing
        End Try




    End Sub
    '***************************************************************
    '**  名称    : Sub loPrevSheetCopy()
    '**  機能    : 契約台帳　プレビュー用シートのコピー処理。
    '**  戻り値  :
    '**  引数    : viMode:0:ヘッダが変わった時　1:明細改ページ時
    '**  備考    :
    '***************************************************************
    Private Sub loPrevSheetCopy(viMode As Integer)

        Select Case viMode
            Case 0     'ヘッダが変わった時
                '契約台帳（鑑）をコピーする
                If mCurPage > 1 Then
                    xlBookTemp.Worksheets(1).Copy(After:=xlBookWork.Worksheets(mCurPage - 1))
                End If
            Case 1     '明細改ページ時
                '契約台帳（明細）をコピーする
                If mCurPage > 1 Then
                    xlBookTemp.Worksheets(2).Copy(After:=xlBookWork.Worksheets(mCurPage - 1))
                End If

        End Select

    End Sub
    '***************************************************************
    '**  名称    : Sub loSheetClear()
    '**  機能    : 指定シート番号の指定レンジをクリアする。
    '**  戻り値  :
    '**  引数    : vsCell  ; レンジをあらわすセル名("A1:B2")
    '**  備考    :
    '***************************************************************
    Private Sub loSheetClear(vsCell As String)

        '指定レンジをクリアする
        xlBookWork.Sheets(mCurPage).Range(vsCell).ClearContents

    End Sub
    '***************************************************************
    '**  名称    : Sub loWorkbooksClose()
    '**  機能    : 各ワークブックのクローズ(解放)処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub loWorkbooksClose()

        On Error Resume Next
        'Bookのクローズ
        xlBookWork.Close(SaveChanges:=False)
        xlBookTemp.Close(SaveChanges:=False)
        xlApp.Quit()
        ' オブジェクトを解放します。
        xlBookWork = Nothing
        xlBookTemp = Nothing
        xlApp = Nothing

        On Error GoTo 0

    End Sub
    '***************************************************************
    '**  名称    : Sub Write_Shuka()
    '**  機能    : 出荷データ印字
    '**  戻り値  :
    '**  引数    : vKey:契約№
    '**  備考    :
    '***************************************************************
    Private Sub Write_Shuka(vKey As String)

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim lRow As Long

        Dim DB As New clsDB.DBService

        On Error GoTo Write_Shuka_Err

        strSQL = "SELECT * "
        strSQL = strSQL & " FROM ShuD_Dat"
        strSQL = strSQL & " WHERE ShuD_012 = '" & vKey & "'"
        strSQL = strSQL & " ORDER BY ShuD_002"

        '出荷データの読込み
        loRec = DB.GetDatafromDB(strSQL)

        'データ存在チェック
        If loRec.Rows.Count <= 0 Then
            'データ無し
            GoTo Write_Shuka_Exit
        End If

        lRow = 14
        Do Until lRow > loRec.Rows.Count - 1

            With xlBookWork.Worksheets(mCurPage)
                .Cells(lRow, 2).Value = modHanbai.NCnvN(loRec.Rows(lRow)("ShuD_002"))      'ケース№
                .Cells(lRow, 8).Value = Format(CDbl(modHanbai.NCnvZ(loRec.Rows(lRow)("ShuD_005"))), "#,##0.000")     '重量
                .Cells(lRow, 13).Value = modHanbai.NCnvN(loRec.Rows(lRow)("ShuD_006"))      '置き場所
                '預け日
                If IsDate(modHanbai.NCnvN(loRec.Rows(lRow)("ShuD_007"))) Then
                    .Cells(lRow, 27).Value = Format(CDate(loRec.Rows(lRow)("ShuD_007")), "yyyy/MM/dd")
                Else
                    .Cells(lRow, 27).Value = ""
                End If
                .Cells(lRow, 32).Value = modHanbai.NCnvN(loRec.Rows(lRow)("ShuD_008"))     '出荷方法
                .Cells(lRow, 50).Value = modHanbai.NCnvN(loRec.Rows(lRow)("ShuD_009"))     '備考

            End With
            lRow = lRow + 1
            If lRow > 17 Then
                Exit Do
            End If
        Loop



Write_Shuka_Exit:

        loRec = Nothing
        On Error GoTo 0
        Exit Sub

Write_Shuka_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Resume Write_Shuka_Exit

    End Sub

#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = winApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    winApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk + gstStrSEPFILE +
                     winApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next

            If xlApp IsNot Nothing Then
                xlApp.Quit()
                xlApp = Nothing
            End If
        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub
#End Region
End Module
