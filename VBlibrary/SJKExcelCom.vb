﻿Imports System.Windows.Forms
Imports clswinApi
Imports globalcls.modHanbai
Imports Microsoft.Office.Interop.Excel
Imports Excel = Microsoft.Office.Interop.Excel
Public Module SJKExcelCom

    Public xlApp As Excel.Application
    Public xlWs As Excel.Worksheet
    Public xlRange As Excel.Range
    '***************************************************************
    '**
    '**  プログラムＩＤ :  印刷用共通モジュール
    '**  機能           :  Excel関連処理
    '**  作成日         :  2003/11/12
    '**  更新日         :
    '**  備考           :
    '**
    '***************************************************************

    '' Public Const exXLSSuffix As String = ".xls"      'エクセル拡張子
    Private Const exXLSWildCd As String = "*"         'ワークファイル用ワイルドカード
    Private Const exXLSPrevWk As String = "SPW"

    '***************************************************************
    '**  名称    : Function steCreatePath() As String
    '**  機能    : ファイル名を含むパス名を作成する(通常印刷用)。
    '**  戻り値  : 作成したパス名
    '**  引数    : fName ; パスを作成するファイル名
    '**  備考    :
    '***************************************************************
    Private Function steCreatePath(fname As String) As String

        'パスの作成
        If fname <> "" Then
            steCreatePath = Trim$(StaticWinApi.ReportDIR) & "\" & fname
        Else
            steCreatePath = fname
        End If

    End Function

    '***************************************************************
    '**  名称    : Function steCreatePathPrev() As String
    '**  機能    : ファイル名を含むパス名を作成する。
    '**  戻り値  : 作成したパス名
    '**  引数    : fName ; パスを作成するファイル名
    '**  備考    :
    '***************************************************************
    Private Function steCreatePathPrev(fname As String) As String

        'パスの作成
        If fname <> "" Then
            steCreatePathPrev = Trim(StaticWinApi.ReportDIR) & "\" &
                            exXLSPrevWk & modSac_Com.gstStrSEPFILE &
                            StaticWinApi.Wsnumber & modSac_Com.gstStrSEPFILE & fname
        Else
            steCreatePathPrev = fname
        End If

    End Function

    '***************************************************************
    '**  名称    : Function steCreateWorkPrev() As String
    '**  機能    : ファイル名を含むパス名を作成する(ワーク用)。
    '**  戻り値  : 作成したパス名
    '**  引数    : viMode ; ワークファイル名の作成モード
    '**                     0 = ".xls"を含むファイル名
    '**                     1 = ワイルドカードを含むファイル名
    '**  備考    :
    '***************************************************************
    '--- UPDATE 2011/11/11 品名品番改行 青木 START -------------------------------------------------------------
    'Private Function steCreateWorkPrev(viMode As Integer) As String
    Public Function steCreateWorkPrev(viMode As Integer) As String
        '--- UPDATE 2011/11/11 品名品番改行 青木 E N D -------------------------------------------------------------

        'パスの作成
        Select Case viMode
            Case 0      '".xls"ファイル指定
                steCreateWorkPrev = Trim(StaticWinApi.ReportDIR) & gstStrSEPPATH &
                                exXLSPrevWk & gstStrSEPFILE &
                                StaticWinApi.Wsnumber & exXLSSuffix
            Case 1      'ワイルドカード指定
                steCreateWorkPrev = Trim(StaticWinApi.ReportDIR) & gstStrSEPPATH &
                                exXLSPrevWk & gstStrSEPFILE &
                                StaticWinApi.Wsnumber & exXLSWildCd
        End Select

    End Function

    '***************************************************************
    '**  名称    : Function steExcelOpen() As Boolean
    '**  機能    : 指定Excelファイルのオープン処理。
    '**  戻り値  : True;正常終了, False;エラー
    '**  引数    : xlsName ; オープンするエクセルファイル名
    '**  備考    :
    '***************************************************************
    Public Function steExcelOpen(xlsName As String) As Boolean

        On Error GoTo steExcelOpen_Err

        'Excel WorkBook のオープン
        xlApp = New Excel.Application()

        xlApp.Workbooks.Open(Filename:=steCreatePath(xlsName))
        steExcelOpen = True
        Exit Function

steExcelOpen_Err:
        steExcelOpen = False

    End Function

    '***************************************************************
    '**  名称    : Sub steSheetClear()
    '**  機能    : 指定シート番号の指定レンジをクリアする。
    '**  戻り値  :
    '**  引数    : vlSheet ; WoksSheets内のシート番号
    '**            vsCell  ; レンジをあらわすセル名("A1:B2")
    '**  備考    :
    '***************************************************************
    Public Sub steSheetClear(vlSheet As Long, vsCell As String)

        '指定レンジをクリアする
        xlApp.Worksheets(vlSheet).Range(vsCell).ClearContents

    End Sub

    '***************************************************************
    '**  名称    : Sub steExcelPrint()
    '**  機能    : 指定ワークシート番号の内容を印刷する。
    '**  戻り値  :
    '**  引数    : vlSheet ; WoksSheets内のシート番号
    '**            viPrinter ; プリンタ名称
    '**            viCopies  ; 印刷部数(デフォルト:1)
    '**  備考    :
    '***************************************************************
    Public Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub

    '***************************************************************
    '**  名称    : Sub steWorkbooksClose()
    '**  機能    : 各ワークブックのクローズ(解放)処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Sub steWorkbooksClose()

        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next


        End If

    End Sub

    '***************************************************************
    '**  名称    : Function stePrevExcelOpen() As Boolean
    '**  機能    : プレビュー用ワークシートのオープン処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : vlPage ; ページ番号(ワークシート名)
    '**  備考    :
    '***************************************************************
    Public Function stePrevExcelOpen(vlPage As Long, vlRange As String) As Boolean

        Dim exFname As String

        stePrevExcelOpen = False

        'ファイルの存在チェック
        If stePrevFileCheck() <> "" Then
            exFname = steCreateWorkPrev(0)
            xlApp.Workbooks.Open(Filename:=exFname)        'ブックオープン
            xlApp.Worksheets(vlPage).Select               'シートの選択
            xlApp.Range(vlRange).Select()                   'レンジの選択
            xlApp.Selection.Copy                          'クリップボードにコピー

            stePrevExcelOpen = True
        End If

    End Function

    '***************************************************************
    '**  名称    : Sub stePrevSheetCopy()
    '**  機能    : プレビュー用シートのコピー処理。
    '**  戻り値  :
    '**  引数    : vlPage ; ページ番号(ワークシート名)
    '**  備考    :
    '***************************************************************
    Public Sub stePrevSheetCopy(vlPage As Long)

        'コピー
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(after:=xlApp.Sheets(vlPage - 1))
        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub stePrevBookSave()
    '**  機能    : カレントのワークシートをプレビュー用に保存する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Sub stePrevBookSave()

        '保存
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                          FileFormat:=XlFileFormat.xlWorkbookNormal,
                          ReadOnlyRecommended:=False, CreateBackup:=False)

    End Sub

    '***************************************************************
    '**  名称    : Sub stePrevWorkDelete()
    '**  機能    : プレビュー用ワークファイルの削除処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Sub stePrevWorkDelete()

        'ファイルパスの作成
        If stePrevFileCheck() <> "" Then
            Kill(steCreateWorkPrev(1))   'ワイルドカード指定で全て削除
            '        Kill "C:\新星マリン\Report\SPW_FMV001*"   'ワイルドカード指定で全て削除
        End If

    End Sub

    '***************************************************************
    '**  名称    : Function stePrevSheetCount() As Long
    '**  機能    : ワークシート上のシート数をカウントする。
    '**  戻り値  : シート数 , =0;無し
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function stePrevSheetCount() As Long

        'シート数を返す。
        stePrevSheetCount = xlApp.Worksheets.Count

    End Function

    '***************************************************************
    '**  名称    : Function stePrevFileCheck() As String
    '**  機能    : プレビュー用ファイルの存在チェック処理。
    '**  戻り値  : ファイル文字列(Dir()関数の返り値)
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function stePrevFileCheck() As String

        stePrevFileCheck = Dir(steCreateWorkPrev(0), vbNormal)

    End Function


End Module
