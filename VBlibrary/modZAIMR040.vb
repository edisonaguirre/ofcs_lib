﻿
Imports System.Windows.Forms
Imports clswinApi
Imports SAHANBAI.clsGlobal
Imports winApi = clswinApi.StaticWinApi
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports SAHANBAI.TaxComp
Imports System.Transactions

Public Module modZAIMR040


    Dim xlApp As Excel.Application
    Dim xlWs As Excel.Worksheet
    Dim xlRange As Excel.Range
    Dim DB As clsDB.DBService
    '***************************************************************
    '**
    '**  機能           :  在庫数調整画面フォーム(frmZAIMR040)
    '**  作成日         :  2012/04/11   真美子
    '**  更新日         :  2012/04/19   青木
    '**  備考           :
    '**
    '***************************************************************

    '各定数定義
    Public Const pkspPGID As String = "ZAIMR040"
    Public Const pkspPGTITLE As String = "在庫数調整画面"

    '各種メッセージ
    Public ConMst(0 To 0) As ConMstInfo     'コントロールマスタ情報
    Public SirMst(0 To 0) As SirMstInfo     '仕入先マスタ

    'スプレッド列数
    Public Const spdChuNo As Integer = 1
    Public Const spdSEQ1 As Integer = 2
    Public Const spdKouNo1 As Integer = 3
    Public Const spdRenNo1 As Integer = 4
    Public Const spdSirCode As Integer = 5
    Public Const spdSirName As Integer = 6
    Public Const spdHinName As Integer = 7
    Public Const spdHinNo As Integer = 8
    Public Const spdSirTanka As Integer = 9
    Public Const spdSir As Integer = 10
    Public Const spdTyousei As Integer = 11
    Public Const spdUriYotei As Integer = 12
    Public Const spdUri As Integer = 13
    Public Const spdZaiYotei As Integer = 14
    Public Const spdSirZai As Integer = 15
    Public Const spdSirDate As Integer = 16
    Public Const spdKeiNo As Integer = 17


    '== 出荷台帳のExcel定義 ==
    Private Const psttREPORT_NAME As String = "ZAIMR040_在庫表"       'エクセル出力
    Private Const pstPrintArea As String = "A2:I21"                 'シートクリア範囲

    'Excel出力用
    Private xlBookTem As Excel.Workbook
    Private xlBookNew As Excel.Workbook   'ワーク用ブック

    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
        Excel = 2       'Excel出力
    End Enum

    '印刷共通定義
    'Private mCurMode        As Integer     'カレント印刷モード
    Private mCurPage As Long        'カレントページ番号
    Private mCurRows As Long        'カレント行
    'Private mCurCols        As Long        'カレント列
    'Private brkTokNm        As String
    Private curQuantity As Double
    Private curExcludingTax As Double
    Private curSirNet As Double
    Private curSirCnt As Double

    Public ZAIMR040_datSirYMD_0 As DateTimePicker
    Public ZAIMR040_datSirYMD_1 As DateTimePicker
    Public ZAIMR040_txtChumonNo As TextBox
    Public ZAIMR040_txtShiireCd As NumericUpDown
    Public ZAIMR040_txtShiireNm As TextBox
    Public ZAIMR040_txtHinmei As TextBox
    Public ZAIMR040_txtHinban As TextBox
    Public ZAIMR040_spdMst As DataGridView
    Public ZAIMR040_numSuryo_0 As NumericUpDown
    Public ZAIMR040_numSuryo_1 As NumericUpDown
    Public ZAIMR040_numSuryo_2 As NumericUpDown
    Public ZAIMR040_numSuryo_3 As NumericUpDown
    Public ZAIMR040_numSuryo_4 As NumericUpDown
    Public ZAIMR040_numSuryo_5 As NumericUpDown

    '***************************************************************
    '**  名称    : Public Sub DataSpdSetProc()
    '**  機能    : Type情報のスプレッド表示
    '**  戻り値  :
    '**  引数    : voForm  フォーム
    '**          : viMode  0 : 全件
    '**          :         1 : 分類指定
    '**          :         2 : 区分名指定
    '**  備考    :
    '***************************************************************
    Public Sub DataSpdSetProc(voForm As Form, viMode As Integer)

        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim DCount As Long

        Dim liSir As Integer
        Dim liUriYotei As Integer
        Dim liUri As Integer
        Dim liZaiYotei As Integer
        Dim liSirZai As Integer
        Dim liTyousei As Integer

        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT "
        SQLtxt = SQLtxt & " ZaiD_001"
        SQLtxt = SQLtxt & ",ZaiD_002"
        SQLtxt = SQLtxt & ",ZaiD_003"
        SQLtxt = SQLtxt & ",ZaiD_004"
        SQLtxt = SQLtxt & ",ZaiD_005"
        SQLtxt = SQLtxt & ",ZaiD_006"
        SQLtxt = SQLtxt & ",ZaiD_007"
        SQLtxt = SQLtxt & ",ZaiD_008"
        SQLtxt = SQLtxt & ",ZaiD_020"
        SQLtxt = SQLtxt & ",ZaiD_026"
        SQLtxt = SQLtxt & ",ZaiD_021"
        SQLtxt = SQLtxt & ",ZaiD_030"
        SQLtxt = SQLtxt & ",ZaiD_031"
        SQLtxt = SQLtxt & ",ZaiD_032"
        SQLtxt = SQLtxt & ",ZaiD_001k"
        SQLtxt = SQLtxt & " FROM   ZaiD_Dat "
        'SQLtxt = SQLtxt & " WHERE ZaiD_021 > 0 "           'DELETE 2012/10/01 AOKI
        SQLtxt = SQLtxt & " WHERE  (ZaiD_021  > 0  "        'INSERT 2012/10/01 AOKI
        SQLtxt = SQLtxt & " OR      ZaiD_032 <> 0) "        'INSERT 2012/10/01 AOKI

        With voForm
            '仕入日付 FROM-TO
            If IsDate(ZAIMR040_datSirYMD_0.Value) Then
                SQLtxt = SQLtxt & " AND ZaiD_020 >= '" & ZAIMR040_datSirYMD_0.Value.ToShortDateString & "'"
                SQLtxt = SQLtxt & " AND ISNULL(ZaiD_020,'') > ''"
            End If
            If IsDate(ZAIMR040_datSirYMD_1.Value) Then
                SQLtxt = SQLtxt & " AND ZaiD_020 <= '" & ZAIMR040_datSirYMD_1.Value.ToShortDateString & "'"
                SQLtxt = SQLtxt & " AND ISNULL(ZaiD_020,'') > ''"
            End If
            '注文№
            If Trim(ZAIMR040_txtChumonNo.Text) <> "" Then
                SQLtxt = SQLtxt & " AND ZaiD_001 Like '%" & modCommon.EditSQLAddSQuot(Trim(ZAIMR040_txtChumonNo.Text)) & "%'"
            End If
            '仕入先コード
            If Trim(ZAIMR040_txtShiireCd.Text) <> "" Then
                SQLtxt = SQLtxt & " AND ZaiD_007 Like '%" & modCommon.EditSQLAddSQuot(Trim(ZAIMR040_txtShiireCd.Text)) & "%'"
            End If
            '仕入先名
            If Trim(ZAIMR040_txtShiireNm.Text) <> "" Then
                SQLtxt = SQLtxt & " AND ZaiD_008 Like '%" & modCommon.EditSQLAddSQuot(Trim(ZAIMR040_txtShiireNm.Text)) & "%'"
            End If
            '品名
            If Trim(ZAIMR040_txtHinmei.Text) <> "" Then
                SQLtxt = SQLtxt & " AND ZaiD_005 Like '%" & modCommon.EditSQLAddSQuot(Trim(ZAIMR040_txtHinmei.Text)) & "%'"
            End If
            '品番
            If Trim(ZAIMR040_txtHinban.Text) <> "" Then
                SQLtxt = SQLtxt & " AND ZaiD_006 Like '%" & modCommon.EditSQLAddSQuot(Trim(ZAIMR040_txtHinban.Text)) & "%'"
            End If
        End With

        SQLtxt = SQLtxt & " ORDER BY  ZaiD_001 "
        SQLtxt = SQLtxt & "        ,  ZaiD_002 "
        SQLtxt = SQLtxt & "        ,  ZaiD_003 "
        SQLtxt = SQLtxt & "        ,  ZaiD_004 "

        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)


        DCount = -1

        ZAIMR040_spdMst.Rows(ZAIMR040_spdMst.SelectedCells(0).RowIndex).Cells(spdTyousei).Selected = True
        'voForm.spdMst.SetActiveCell spdTyousei, voForm.spdMst.ActiveRow


        For Each dr As DataRow In loRSet.Rows

            DCount = DCount + 1
            With ZAIMR040_spdMst
                'データセット
                ZAIMR040_spdMst.Rows(DCount).Cells(spdChuNo - 1).Value = NCnvN(dr("ZaiD_001")) '注文No.
                ZAIMR040_spdMst.Rows(DCount).Cells(spdSEQ1 - 1).Value = NCnvZ(dr("ZaiD_002"))   'SEQ
                ZAIMR040_spdMst.Rows(DCount).Cells(spdKouNo1 - 1).Value = NCnvZ(dr("ZaiD_003")) '項目
                ZAIMR040_spdMst.Rows(DCount).Cells(spdRenNo1 - 1).Value = NCnvZ(dr("ZaiD_004")) '連番
                ZAIMR040_spdMst.Rows(DCount).Cells(spdSirCode - 1).Value = NCnvN(dr("ZaiD_007")) '仕入先コード
                ZAIMR040_spdMst.Rows(DCount).Cells(spdSirName - 1).Value = NCnvN(dr("ZaiD_008")) '仕入先名
                ZAIMR040_spdMst.Rows(DCount).Cells(spdHinName - 1).Value = NCnvN(dr("ZaiD_005"))    '品名
                ZAIMR040_spdMst.Rows(DCount).Cells(spdHinNo - 1).Value = NCnvN(dr("ZaiD_006"))      '品番
                ZAIMR040_spdMst.Rows(DCount).Cells(spdSirTanka - 1).Value = NCnvN(dr("ZaiD_026")) '仕入単価

                liSir = NCnvZ(dr("ZaiD_021"))
                ZAIMR040_spdMst.Rows(DCount).Cells(spdSir - 1).Value = NCnvZ(liSir)  '仕入

                liUriYotei = NCnvZ(dr("ZaiD_030"))
                ZAIMR040_spdMst.Rows(DCount).Cells(spdUriYotei - 1).Value = NCnvZ(liUriYotei) '売上予定

                liUri = NCnvZ(dr("ZaiD_031"))
                ZAIMR040_spdMst.Rows(DCount).Cells(spdUri - 1).Value = NCnvZ(liUri)         '売上

                liZaiYotei = NCnvZ(dr("ZaiD_021")) +
                         NCnvZ(dr("ZaiD_032")) -
                         NCnvZ(dr("ZaiD_031")) -
                         NCnvZ(dr("ZaiD_030"))
                ZAIMR040_spdMst.Rows(DCount).Cells(spdZaiYotei - 1).Value = NCnvZ(liZaiYotei)                              '在庫予定=仕入+調整-売上-売上予定

                liSirZai = NCnvZ(dr("ZaiD_021")) +
                       NCnvZ(dr("ZaiD_032")) -
                       NCnvZ(dr("ZaiD_031"))
                ZAIMR040_spdMst.Rows(DCount).Cells(spdSirZai - 1).Value = NCnvZ(liSirZai)     '仕入在庫=仕入+調整-売上

                liTyousei = NCnvZ(dr("ZaiD_032"))
                ZAIMR040_spdMst.Rows(DCount).Cells(spdTyousei - 1).Value = NCnvZ(liTyousei)                                '調整

                ZAIMR040_spdMst.Rows(DCount).Cells(spdSirDate - 1).Value = NCnvZ(dr("ZaiD_020"))        '仕入日
                ZAIMR040_spdMst.Rows(DCount).Cells(spdKeiNo - 1).Value = NCnvN(dr("ZaiD_001k"))     '契約No.


                If NCnvZ(dr("ZaiD_021")) + NCnvZ(dr("ZaiD_032")) - NCnvZ(dr("ZaiD_031")) = 0 Or
                    NCnvZ(dr("ZaiD_021")) + NCnvZ(dr("ZaiD_032")) - NCnvZ(dr("ZaiD_030")) = 0 Then
                    ZAIMR040_spdMst.Rows(DCount).Cells(spdTyousei).ReadOnly = True
                Else
                    ZAIMR040_spdMst.Rows(DCount).Cells(spdTyousei).ReadOnly = False
                End If

                ZAIMR040_numSuryo_0.Value = NCnvZ(ZAIMR040_numSuryo_0.Value) + liSir
                ZAIMR040_numSuryo_1.Value = NCnvZ(ZAIMR040_numSuryo_1.Value) + liTyousei
                ZAIMR040_numSuryo_2.Value = NCnvZ(ZAIMR040_numSuryo_2.Value) + liUriYotei
                ZAIMR040_numSuryo_3.Value = NCnvZ(ZAIMR040_numSuryo_3.Value) + liUri
                ZAIMR040_numSuryo_4.Value = NCnvZ(ZAIMR040_numSuryo_4.Value) + liZaiYotei
                ZAIMR040_numSuryo_5.Value = NCnvZ(ZAIMR040_numSuryo_5.Value) + liSirZai

            End With
        Next


        loRSet = Nothing

        ZAIMR040_spdMst.Select()


    End Sub
    'TODO:MIKE 
    '***************************************************************
    '**  名称    : Sub DataUpdateProc()
    '**  機能    : データアップデート処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    : 2006/05/13
    '***************************************************************
    Public Sub DataUpdateProc(voForm As Form)

        Dim strSQL As String  'SQL文作成用
        Dim strMsg As String  'エラーメッセージ作成用
        Dim lngErr As Long    'エラー番号
        Dim SQLZaiD_001 As Object
        Dim SQLZaiD_002 As Object
        Dim SQLZaiD_003 As Object
        Dim SQLZaiD_004 As Object
        Dim SQLZaiD_030 As Object                              'INSERT 2012/10/01 AOKI
        Dim SQLZaiD_031 As Object                              'INSERT 2012/10/01 AOKI
        Dim SQLZaiD_032 As Object
        Dim iCount As Integer
        Dim lFlg As Boolean                              'INSERT 2012/10/01 AOKI

        lFlg = True                                             'INSERT 2012/10/01 AOKI

        If ksExpMsgBox("在庫" & gstGUIDE_Q003, "Q") = vbYes Then
            If ZAIMR040_spdMst.Rows.Count = 0 Then
                Call ksExpMsgBox(gstGUIDE_E005, "W")
            End If
        Else
            Exit Sub
        End If

        'セッション開始
        Dim TransactionOptions As New TransactionOptions()
        TransactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
        TransactionOptions.Timeout = TimeSpan.MaxValue

        Using transcope As New TransactionScope(TransactionScopeOption.Required, TransactionOptions)

            Try



                For iCount = 0 To ZAIMR040_spdMst.Rows.Count - 1
                    SQLZaiD_001 = ""
                    SQLZaiD_002 = 0
                    SQLZaiD_003 = 0
                    SQLZaiD_004 = 0
                    SQLZaiD_030 = 0                                     'INSERT 2012/10/01 AOKI
                    SQLZaiD_031 = 0                                     'INSERT 2012/10/01 AOKI
                    SQLZaiD_032 = 0

                    With ZAIMR040_spdMst

                        SQLZaiD_032 = .Rows(iCount).Cells(spdTyousei - 1).Value
                        SQLZaiD_001 = .Rows(iCount).Cells(spdChuNo - 1).Value
                        SQLZaiD_002 = .Rows(iCount).Cells(spdSEQ1 - 1).Value
                        SQLZaiD_003 = .Rows(iCount).Cells(spdKouNo1 - 1).Value
                        SQLZaiD_004 = .Rows(iCount).Cells(spdRenNo1 - 1).Value
                        SQLZaiD_030 = .Rows(iCount).Cells(spdZaiYotei - 1).Value
                        SQLZaiD_031 = .Rows(iCount).Cells(spdSirZai - 1).Value

                    End With

                    If SQLZaiD_030 < 0 Or SQLZaiD_031 < 0 Then
                        lFlg = False
                        GoTo Error_DataUpdateProc
                    End If

                    '更新用SQL文作成
                    '=====================================================================
                    strSQL = ""
                    strSQL = " UPDATE ZaiD_Dat"
                    strSQL = strSQL & "   Set ZaiD_032    = " & SQLZaiD_032                                     'INSERT 2012/12/06 AOKI
                    strSQL = strSQL & "     , ZaiD_Update ='" & Now & "'"
                    strSQL = strSQL & "     , ZaiD_WsNo   ='" & clswinApi.StaticWinApi.Wsnumber & "'"
                    strSQL = strSQL & " WHERE ZaiD_001    ='" & SQLZaiD_001 & "'"
                    strSQL = strSQL & "   AND ZaiD_002    = " & SQLZaiD_002
                    strSQL = strSQL & "   AND ZaiD_003    = " & SQLZaiD_003
                    strSQL = strSQL & "   AND ZaiD_004    = " & SQLZaiD_004
                    '=====================================================================

                    'SQL文実行
                    If DB Is Nothing Then DB = New clsDB.DBService
                    If DB.ExecuteSql(strSQL, strMsg) = False Then
                        GoTo Error_DataUpdateProc
                    End If
                Next
                'コミット
                transcope.Complete()
                transcope.Dispose()
                Exit Sub
            Catch ex As Exception
                '処理を戻す（ロールバック）
                transcope.Dispose()
            End Try




        End Using
Error_DataUpdateProc:
        'エラーメッセージ表示
        If lFlg = False Then
            Call ksExpMsgBox("在庫、又は、倉庫在庫がマイナスになるような入力は行えません。", "W")
            ZAIMR040_spdMst.Rows(iCount).Cells(spdTyousei - 1).Selected = True
        Else
            Call ksExpMsgBox(gstGUIDE_E003, "E")
        End If

    End Sub



    '***************************************************************
    '**  名称    : Function ZAIMR040ExcelProc() As Boolean
    '**  機能    : エクセル出力実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function ZAIMR040ExcelProc() As Boolean

        Dim DCount As Long
        Dim strPath As String
        Dim strWk As String

        On Error GoTo ZAIMR040ExcelProc_Err

        ZAIMR040ExcelProc = False


        '==========================================
        '   テンプレートExcelファイルのオープン
        '==========================================
        strPath = Trim$(clswinApi.StaticWinApi.WsReportDir) & "\" & psttREPORT_NAME & exXLSSuffix
        xlBookTem = xlApp.Workbooks.Open(strPath)
        '新規ブックとしてオープン
        xlBookTem.Activate()
        xlBookTem.Sheets(1).Select
        xlBookTem.Sheets(1).Copy
        xlBookNew = xlApp.ActiveWorkbook


        '===各変数初期化
        mCurPage = 1         'ページ番号
        mCurRows = 2         '明細印刷開始行
        DCount = 0           'データカウンタ

        curQuantity = 0
        curExcludingTax = 0

        'シート名を仕入先名に変更
        xlApp.Worksheets(1).NAME = Format(Now, "yyyy年mm月末在庫表")

        'クリア
        Call loSheetClear()

        For DCount = 0 To ZAIMR040_spdMst.Rows.Count - 1

            '明細情報出力
            Call sttWriteMeisai(DCount, xlBookNew.Worksheets(mCurPage))
            '合計集計
            Call setTotal()
            '明細行設定
            Call WriteLine()

            mCurRows = mCurRows + 1

        Next

        '合計表示
        Call WriteTotal(xlApp.Worksheets(1))
        Call WriteLine()

        xlWs = xlBookNew.Worksheets(mCurPage)
        With xlWs
            '罫線
            strWk = "A" & mCurRows & ":I" & mCurRows
            xlRange = xlWs.Range(strWk)
        End With

        With xlRange
            .Font.Bold = True
            .Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlEdgeBottom).Weight = Excel.XlBorderWeight.xlMedium
            .Borders(Excel.XlBordersIndex.xlEdgeBottom).ColorIndex = 16
        End With

        Call loWorkbooksClose()      'Bookのクローズ

        ZAIMR040ExcelProc = True

        xlApp.Visible = True
        xlApp.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized
        xlBookNew.Worksheets(1).Select
        xlApp.Range("A1").Select()

Exit_ZAIMR040ExcelProc:

        Exit Function

ZAIMR040ExcelProc_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Call loWorkbooksClose()         'Bookのクローズ

        Resume Exit_ZAIMR040ExcelProc

    End Function

    '***************************************************************
    '**  名称    : Sub loWorkbooksClose()
    '**  機能    : 各ワークブックのクローズ(解放)処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub loWorkbooksClose()

        On Error Resume Next
        'Bookのクローズ
        xlBookTem.Close(SaveChanges:=False)
        ' オブジェクトを解放します。
        xlBookTem = Nothing

        On Error GoTo 0

    End Sub

    '***************************************************************
    '**  名称    : Sub WriteLine()
    '**  機能    : 明細行設定（罫線・高さ）
    '***************************************************************
    Private Sub WriteLine()

        Dim strWk As String
        Dim liWork As Integer
        Dim lcWork As Double

        On Error Resume Next
        xlWs = xlBookNew.Worksheets(mCurPage)
        With xlWs

            strWk = "A" & mCurRows & ":I" & mCurRows
            xlRange = xlWs.Range(strWk)


        End With

        With xlRange
            .Rows.RowHeight = 25

            .Font.ColorIndex = 1
            .Font.Size = 11
            .Font.Name = "ＭＳ Ｐゴシック"
            .Font.Bold = False

            .Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlEdgeLeft).Weight = Excel.XlBorderWeight.xlMedium
            .Borders(Excel.XlBordersIndex.xlEdgeLeft).ColorIndex = 16

            .Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlEdgeRight).Weight = Excel.XlBorderWeight.xlMedium
            .Borders(Excel.XlBordersIndex.xlEdgeRight).ColorIndex = 16

            .Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlInsideVertical).Weight = Excel.XlBorderWeight.xlThin
            .Borders(Excel.XlBordersIndex.xlInsideVertical).ColorIndex = 16

            .Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
            .Borders(Excel.XlBordersIndex.xlEdgeBottom).Weight = Excel.XlBorderWeight.xlThin
            .Borders(Excel.XlBordersIndex.xlEdgeBottom).ColorIndex = 16

            liWork = mCurRows / 21
            lcWork = mCurRows / 21

            If liWork = lcWork Then
                .Borders(Excel.XlBordersIndex.xlEdgeBottom).Weight = Excel.XlBorderWeight.xlMedium
            End If
        End With

        On Error GoTo 0

    End Sub


    '***************************************************************
    '**  名称    : Sub sttWriteMeisai
    '**  機能    : 明細情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    : voRec:見積明細データ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMeisai(DCount As Long, xlsSheet As Excel.Worksheet)

        Dim lvText As Object

        With xlsSheet
            '番号
            .Cells(mCurRows, 1).Value = DCount

            '注番
            lvText = ZAIMR040_spdMst.Rows(DCount).Cells(spdChuNo - 1).Value

            .Cells(mCurRows, 2).Value = NCnvN(lvText)

            '仕入日付
            lvText = ZAIMR040_spdMst.Rows(DCount).Cells(spdSirDate - 1).Value

            If IsDate(NCnvN(lvText)) = True Then
                .Cells(mCurRows, 3).Value = Format(NCnvN(lvText), "YYYY/MM/DD")
            Else
                .Cells(mCurRows, 3).Value = ""
            End If

            '仕入先名称
            lvText = ZAIMR040_spdMst.Rows(DCount).Cells(spdSirName - 1).Value
            .Cells(mCurRows, 4).Value = NCnvN(lvText)

            '品名
            lvText = ZAIMR040_spdMst.Rows(DCount).Cells(spdHinName - 1).Value
            .Cells(mCurRows, 5).Value = NCnvN(lvText)

            '品番
            lvText = ZAIMR040_spdMst.Rows(DCount).Cells(spdHinNo - 1).Value
            .Cells(mCurRows, 6).Value = NCnvN(lvText)

            '仕入単価NET
            lvText = ZAIMR040_spdMst.Rows(DCount).Cells(spdSirTanka - 1).Value
            curSirNet = NCnvZ(lvText)
            .Cells(mCurRows, 7).Value = curSirNet

            '数量
            lvText = ZAIMR040_spdMst.Rows(DCount).Cells(spdSirZai - 1).Value
            curSirCnt = NCnvZ(lvText)
            .Cells(mCurRows, 8).Value = curSirCnt

            '金額
            .Cells(mCurRows, 9).Value = curSirNet * curSirCnt


        End With

    End Sub

    Private Sub setTotal()

        '金額
        curQuantity = curSirNet * curSirCnt
        '合計集計
        curExcludingTax = curExcludingTax + curQuantity

    End Sub

    Private Sub WriteTotal(xlsSheet As Excel.Worksheet)

        With xlsSheet

            '合計
            .Cells(mCurRows, 8).Value = "合計"
            '仕入金額(税抜)
            .Cells(mCurRows, 9).Value = curExcludingTax

        End With

    End Sub

    '***************************************************************
    '**  名称    : Sub loSheetClear()
    '**  機能    : 指定シート番号の指定レンジをクリアする。
    '**  戻り値  :
    '**  引数    : vsCell  ; レンジをあらわすセル名("A1:B2")
    '**  備考    :
    '***************************************************************
    Private Sub loSheetClear()

        '罫線クリア
        xlWs = xlApp.Worksheets(1)
        xlRange = xlWs.Range(pstPrintArea)
        With xlRange
            .Select()
            .ClearContents()
            .Borders.LineStyle = Excel.XlLineStyle.xlLineStyleNone
        End With

    End Sub


End Module
