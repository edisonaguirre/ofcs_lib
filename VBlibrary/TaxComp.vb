﻿

Imports System.Globalization

Public Class TaxComp
    'Private gbl As clsGlobal = New clsGlobal()

    Public Structure TaxCalcInfo
        Public ZeiKbn As Integer
        Public ZeiKeisan As Integer
        Public Kingaku As Double
        Public DDate As String
        Public ZeinukiKingaku As Double
        Public Shohizei As Double
        Public ZeikomiKingaku As Double
        Public ConMst As DataTable
    End Structure

    Public Sub pTaxCalcCom(ByRef myTaxCalcInfo As TaxCalcInfo)
        Dim decWk As Double
        Dim decZei As Double
        myTaxCalcInfo.ZeinukiKingaku = 0
        myTaxCalcInfo.Shohizei = 0
        myTaxCalcInfo.ZeikomiKingaku = 0

        If myTaxCalcInfo.ZeiKbn = 1 Then
            decWk = Convert.ToDouble(myTaxCalcInfo.Kingaku / ((1 + GetZeiritsu(myTaxCalcInfo)) * 100) * (100 * GetZeiritsu(myTaxCalcInfo)))
            decZei = modCommon.CurRound2(decWk, 0, myTaxCalcInfo.ZeiKeisan)
            myTaxCalcInfo.ZeinukiKingaku = myTaxCalcInfo.Kingaku - decZei
            myTaxCalcInfo.Shohizei = decZei
            myTaxCalcInfo.ZeikomiKingaku = myTaxCalcInfo.Kingaku
        Else
            decWk = Convert.ToDouble(myTaxCalcInfo.Kingaku * GetZeiritsu(myTaxCalcInfo))
            decZei = modCommon.CurRound2(decWk, 0, myTaxCalcInfo.ZeiKeisan)
            myTaxCalcInfo.ZeinukiKingaku = myTaxCalcInfo.Kingaku
            myTaxCalcInfo.Shohizei = decZei
            myTaxCalcInfo.ZeikomiKingaku = myTaxCalcInfo.Kingaku + decZei
        End If
    End Sub

    Public Function GetZeiritsu(ByRef myTaxCalcInfo As TaxCalcInfo) As Double
        Dim SngLet As Double
        Dim dt1 As DateTime
        DateTime.TryParseExact(myTaxCalcInfo.DDate, "yy/MM/dd", CultureInfo.CurrentCulture, DateTimeStyles.None, dt1)

        If dt1 <= Convert.ToDateTime(myTaxCalcInfo.ConMst.Rows(0)("Con_038").ToString()) Then
            SngLet = Convert.ToDouble(myTaxCalcInfo.ConMst.Rows(0)("Con_033").ToString())
        Else
            SngLet = Convert.ToDouble(myTaxCalcInfo.ConMst.Rows(0)("Con_034").ToString())
        End If

        Return SngLet
    End Function
End Class

