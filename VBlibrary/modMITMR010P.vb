﻿Imports System.Windows.Forms
Imports globalcls.modHanbai
Imports clswinApi
Imports System
Imports Excel = Microsoft.Office.Interop.Excel
Imports Printing
Imports SAHANBAI
Public Module modMITMR010P

    '***************************************************************
    '**
    '**  機能      :  見積書発行(modMITMR010P)
    '**  作成日    :
    '**  更新日    :  2013/03/04　見積入力からの見積書プレビュー処理用
    '**  備考      :
    '***************************************************************
    '各マスタ情報
    Public TokMst(0 To 1) As TokMstInfo     '得意先マスタ
    Public numtok As String
    Public maker As String
    Public origin As String

    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
    End Enum

    '見積書印刷条件
    Public ourRefNoP() As String      '見積番号指定
    'Public strHakkoDate As String       '見積書発行日付        'DELETE 2014/06/12 AOKI

    '見積書ヘッダ項目
    Public Structure MITMR020Hed
        Public Country As String       '海外国内区分(0:国内 1:海外)
        Public Messrs As String       '客先名
        Public Attn As String       '客先担当者名
        Public YourRefNo As String       '客先依頼Ｎｏ
        Public YourOrderNo As String       '客先注文Ｎｏ           'INSERT 2014/06/12 AOKI
        Public VesselsName As String       '船名
        Public [Date] As String       '日付
        Public TantoNm As String       '当社担当者
        Public OurEstimateNo As String   '見積Ｎｏ
        Public TokCd As Integer      '得意先コード           'INSERT 2017/12/01 AOKI
    End Structure
    Private printHed(0 To 0) As MITMR020Hed

    'プレビューからの見積発行日更新用キー'
    Private KeiyakuNoWk As String      '契約№
    Private RefNoWk As String      '見積№
    Public Structure MITMR020Upd
        Public KEIYAKUNO As String      '契約№
        Public RefNo As String      '見積№
    End Structure
    Public OrdHUPDKey() As MITMR020Upd

    Private BefPrint As Integer     '直前に印字した行(0:明細行 1:中ヘッダー)

    'エクセル上のセル範囲
    Private pstLastRow As Integer              '最終行
    Private TotalAmount As Double             '合計金額

    '== 見積書のExcel定義 ==
    'Private Const psttREPORT_NAME  As String = "MITMR020_見積書(ESTIMATE)" 'Report名称                 'DELETE 2014/02/05 AOKI
    Private Const psttREPORT_NAME As String = "MITMR020" 'Report名称                                   'INSERT 2014/02/05 AOKI
    Private Const pstPrintArea As String = "A19:AN66"                  'シートクリア範囲
    Private Const pstPrevArea As String = "A1:BC67"                   'プレビュー表示範囲          'UPDATE 2017/09/27 AOKI
    Private Const pstMaxRow As Integer = 45                         '最大行数
    Private Const pstStartRow As Integer = 20                         'ページ内の開始行
    Private Const pstPrintP As Integer = 1

    '印刷共通定義
    Private mCurMode As Integer     'カレント印刷モード
    Private mCurPage As Long        'カレントページ番号
    Private mCurRows As Long        'カレント行
    Private mCurCols As Long        'カレント列
    Private PageWk(1) As Integer     '見積№毎のページ印字用ワーク(シートのFROM-TO)

    '###2010/07/23 PDF出力用Xls名
    Public PDFXlsNm As String
    'PDF出力用ファイル
    Public pstrPdfNm As String
    Private dbsvc As New clsDB.DBService()

    '***************************************************************
    '**  名称    : Function MITMR020PrintProc() As Boolean
    '**  機能    : 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function MITMR020PrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ

        Dim SeqBrk As Integer              'SEQブレークチェック用
        Dim iMHedCnt As Integer              '中ヘッダ印字カウント
        Dim iCnt As Integer              'カウント

        On Error GoTo MITMR020PrintProc_Err

        MITMR020PrintProc = False

        mCurMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存
        pstLastRow = pstMaxRow + pstStartRow - 1
        Dim RecHed As DataTable = New DataTable()
        Dim RecMeisai As DataTable = New DataTable()


        '===見積ヘッダデータの読込み用SQL文作成ルーチン
        strSQL = CreateSQLText(0)

        RecHed = dbsvc.GetDatafromDB(strSQL)
        '===見積ヘッダデータの読込み


        '===データ存在チェック
        If RecHed.Rows.Count <= 0 Then
            'データ無しメッセージ
            Call ksExpMsgBox("印刷するデータが存在しません。", "I")
            GoTo Exit_MITMR020PrintProc
        End If

        '===Excelファイルのオープン
        If steExcelOpen(psttREPORT_NAME & exXLSSuffix) = False Then
            'Excelファイル無しメッセージ
            Call ksExpMsgBox(psttREPORT_NAME & "のレポートが存在しません。", "E")
            GoTo Exit_MITMR020PrintProc
        End If

        If mCurMode = enmPrintmode.Preview Then        'プレビューモード
            Call SJKExcelCom.stePrevWorkDelete()  '古いワークファイルを削除する
        End If
        Dim frmStProgressBar As New frmstProgressBar
        '===進行状況表示フォーム設定=============================================
        With frmStProgressBar
            'UPDATE 2014/06/12 AOKI START -------------------------------------------------------------------------------------------
            '        .SetCaption = "見積書(ESTIMATE)"
            '        .SetTitle = "【 見積書(ESTIMATE) 】・・・作成中・・・"
            If FlgF6 = True Then
                .SetCaption = "PROFORMA INVOICE"
                .SetTitle = "【 PROFORMA INVOICE 】・・・作成中・・・"
            Else
                .SetCaption = "見積書(ESTIMATE)"
                .SetTitle = "【 見積書(ESTIMATE) 】・・・作成中・・・"
            End If
            'UPDATE 2014/06/12 AOKI E N D -------------------------------------------------------------------------------------------

            .SetPage = "ページ = " & 0
            .SetMin = 0
            .SetMax = RecHed.Rows.Count
            .ShowStart()
        End With

        '===各変数初期化
        mCurPage = 1                  'ページ番号
        mCurRows = pstStartRow - 1    '明細印刷開始行
        DCount = 0                    'データカウンタ
        PageWk(0) = mCurPage          '見積№毎のページ印字用ワーク(シートのFROM)
        '###2010/07/26 PDF出力用Xls名
        PDFXlsNm = ""

        'シートのクリア
        Call loSheetClear()

        '指定された見積番号を１件ずつ読む
        For iCnt = LBound(ourRefNoP) To UBound(ourRefNoP)

            '進行状況用カウンタ
            DCount = DCount + 1

            '見積ヘッダデータの読込み

            'SQL作成
            strSQL = CreateSQLText(1, ourRefNoP(iCnt))
            RecHed = dbsvc.GetDatafromDB(strSQL)

            'ヘッダー情報を変数にセット
            Call sttSetHeader(RecHed)

            '２件目以降　改ページさせる
            If DCount > 1 Then
                mCurRows = pstLastRow
                Call CheckPages()
                '見積№毎のページ印字用ワーク(シートのTO)
                PageWk(1) = mCurPage - 1
                'ヘッダー　見積№毎のページを出力、印刷時は印刷する。
                Call sttWriteHeaderPage()
                PageWk(0) = mCurPage
            End If

            'プレビューからの見積発行日更新用キー
            KeiyakuNoWk = NCnvN(RecHed.Rows(0)("OrdH_001"))       '契約№
            RefNoWk = NCnvN(RecHed.Rows(0)("OrdH_009"))           '見積№
            '１ページ目キー
            ReDim Preserve OrdHUPDKey(mCurPage)
            OrdHUPDKey(mCurPage).KEIYAKUNO = KeiyakuNoWk      '契約№
            OrdHUPDKey(mCurPage).RefNo = RefNoWk              '見積№

            '見積明細データの読込み用SQL作成
            strSQL = CreateSQLText(2, "", RecHed)

            '見積明細データの読込み

            RecMeisai = dbsvc.GetDatafromDB(strSQL)

            'クリア
            SeqBrk = -9999     'SEQブレークチェック用
            iMHedCnt = 0       '中ヘッダ印字カウント
            TotalAmount = 0    '金額合計

            'データ存在チェック
            If Not RecMeisai.Rows.Count <= 0 Then

                'ヘッダー情報をExcelに出力
                Call sttWriteHeader()

                '読込んだ明細データが無くなるまで繰返す

                For Each row As DataRow In RecMeisai.Rows
                    'strDetail = row("Detail")

                    '進行状況表示フォーム設定
                    With frmStProgressBar
                        .SetPage = "ページ = " & mCurPage
                        .SetVal = DCount
                        'キャンセルボタンのチェック
                        If .Cancel Then
                            Call steWorkbooksClose()      'Bookのクローズ
                            frmStProgressBar.ShowStop()   '進行状況表示フォーム終了
                            GoTo Exit_MITMR020PrintProc
                        End If
                    End With

                    'SEQブレーク
                    If SeqBrk <> row("OrdD_003") Then
                        '中ヘッダー出力
                        Call sttWriteMidHeader(row, iMHedCnt)
                        '明細情報出力
                        Call sttWriteMeisai(row)
                    Else
                        '明細情報出力
                        Call sttWriteMeisai(row)
                    End If

                    SeqBrk = row("OrdD_003")
                Next row
            End If

            'フッター出力
            Call sttWriteFooter(RecHed)

            '＜印刷時＞見積書発行日を更新
            If viMode = enmPrintmode.PrintOut Then
                Call MITMR020_OrdHUpdate(RecHed.Rows(0)("OrdH_001").ToString(), RecHed.Rows(0)("OrdH_009").ToString())
            End If

            '###2010/07/26 PDF出力用Xls名取得
            If PDFXlsNm = "" Then
                Dim stIraiNo As String
                'ファイル名に使えない文字を-置き換える
                stIraiNo = Replace(NCnvN(RecHed.Rows(0)("OrdH_005").ToString()), "/", "-")
                stIraiNo = Replace(stIraiNo, "\", "-")
                stIraiNo = Replace(stIraiNo, ":", "-")
                stIraiNo = Replace(stIraiNo, "*", "-")
                stIraiNo = Replace(stIraiNo, "?", "-")
                stIraiNo = Replace(stIraiNo, """", "-")
                stIraiNo = Replace(stIraiNo, "<", "-")
                stIraiNo = Replace(stIraiNo, ">", "-")
                stIraiNo = Replace(stIraiNo, "|", "-")

                'UPDATE 2015/09/25 AOKI START -----------------------------------------------------------------------------------------
                '            '"客先依頼№(YourRefNo) & "(" & "見積№(OurEstimateNo)" & ")"
                '            PDFXlsNm = stIraiNo & "(" & NCnvN(RecHed![OrdH_009]) & ")"

                Dim stVessel As String
                'ファイル名に使えない文字を-置き換える
                stVessel = Replace(modHanbai.NCnvN(RecHed.Rows(0)("OrdH_006").ToString()), "/", "-")
                stVessel = Replace(stVessel, "\", "-")
                stVessel = Replace(stVessel, ":", "-")
                stVessel = Replace(stVessel, "*", "-")
                stVessel = Replace(stVessel, "?", "-")
                stVessel = Replace(stVessel, """", "-")
                stVessel = Replace(stVessel, "<", "-")
                stVessel = Replace(stVessel, ">", "-")
                stVessel = Replace(stVessel, "|", "-")

                'UPDATE 2017/12/01 AOKI START -----------------------------------------------------------------------------------------
                '            '"船名(Vessel's Name)" & "客先依頼№(YourRefNo) & "(" & "見積№(OurEstimateNo)" & ")"
                '            PDFXlsNm = stVessel & Space(1) & stIraiNo & "(" & NCnvN(RecHed![OrdH_009]) & ")"

                If printHed(0).TokCd = 138 Then     'ISM Ship Management Pte Ltd
                    PDFXlsNm = stVessel & Space(1) & stIraiNo & "(" & NCnvN(RecHed.Rows(0)("OrdH_009")) & ")" & Space(1) & stIraiNo & Space(1) & "(Ones-Forte)"
                Else
                    PDFXlsNm = stVessel & Space(1) & stIraiNo & "(" & NCnvN(RecHed.Rows(0)("OrdH_009")) & ")"
                End If
                'UPDATE 2017/12/01 AOKI E N D -----------------------------------------------------------------------------------------
                'UPDATE 2015/09/25 AOKI E N D -----------------------------------------------------------------------------------------
            End If

        Next iCnt
        'クローズ

        '見積№毎のページ印字用ワーク(シートのTO)
        PageWk(1) = mCurPage
        'ヘッダー　見積№毎のページを出力、印刷時は印刷する。
        Call sttWriteHeaderPage()

        'モードごとの処理
        Select Case viMode

            Case enmPrintmode.Preview      'プレビュー

                Call stePrevBookSave()       'Bookの保存
                Call modSac_Com.TimeWait(1)
                Call steWorkbooksClose()     'Bookのクローズ
                Call TimeWait(1)

                '###2010/07/26 PDF出力用Xls作成================================================
                Dim strXlsNm As String
                Dim strPdfPath As String

                'PDF出力用ファイル保存用フォルダ
                strPdfPath = Trim(StaticWinApi.ReportDIR) & "\PDF\"
                If Dir(strPdfPath, vbDirectory) = "" Then
                    'Report\PDFフォルダが無ければ作成する
                    Call MkDir(strPdfPath)
                Else
                    'Report\PDFフォルダ下のファイルを削除
                    Call File_Delete(strPdfPath)
                End If

                strXlsNm = Trim(StaticWinApi.ReportDIR) & "\" & "SPW" & gstStrSEPFILE &
                       StaticWinApi.Wsnumber & exXLSSuffix
                pstrPdfNm = strPdfPath & PDFXlsNm & exXLSSuffix

                Call FileCopy(strXlsNm, pstrPdfNm)
                '###2010/07/26 PDF出力用Xls作成================================================

                Dim frmMITMR010P As Printing.frmPrintPreview = New Printing.frmPrintPreview()



                frmStProgressBar.ShowStop()  '進行状況表示フォーム終了

                'プレビュー画面の起動
                With frmMITMR010P

                    .ppvActCell = pstPrevArea
                End With
                frmMITMR010P.ShowDialog()

            Case enmPrintmode.PrintOut     '印刷
                Call steWorkbooksClose()     'Bookのクローズ
                frmStProgressBar.ShowStop()  '進行状況表示フォーム終了

        End Select

        MITMR020PrintProc = True

Exit_MITMR020PrintProc:

        RecHed.Dispose()
        RecHed.Dispose()
        Exit Function

MITMR020PrintProc_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Call steWorkbooksClose()        'Bookのクローズ
        frmStProgressBar.ShowStop()     '進行状況表示フォーム終了

        Resume Exit_MITMR020PrintProc

    End Function

    '********************************************************************
    '**  名称    : Function CreateSQLText() String
    '**  機能    : 見積データ読み込み用SQL作成処理
    '**  戻り値  : 作成されたSQL文字列
    '**  引数    : iMode 0:印刷データ確認　1:見積ヘッダ　2：見積明細
    '**          : vKey  見積ヘッダデータKEY(見積ヘッダ読込時使用）
    '**          : voRec 見積ヘッダデータ(明細データ読込時使用）
    '**  備考    :
    '********************************************************************
    Private Function CreateSQLText(iMode As Integer, Optional vKey As String = "", Optional voRec As DataTable = Nothing) As String

        Dim strSQL As String
        Dim strWhere As String
        Dim iWk As Integer

        Select Case iMode
            Case 0  '印刷データ確認
                strSQL = strSQL & "SELECT * FROM OrdH_Dat"
                strSQL = strSQL & " WHERE 1 = 1 "                  'データ区分
                '見積番号
                strSQL = strSQL & " AND ("
                For iWk = LBound(ourRefNoP) To UBound(ourRefNoP)
                    If strWhere = "" Then
                        strWhere = " OrdH_009 ='" & EditSQLAddSQuot(NCnvN(ourRefNoP(iWk))) & "'"
                    Else
                        strWhere = strWhere & " OR OrdH_009 ='" & EditSQLAddSQuot(NCnvN(ourRefNoP(iWk))) & "'"
                    End If
                Next iWk
                strSQL = strSQL & strWhere
                strSQL = strSQL & ")"

            Case 1  'ヘッダデータ
                '見積番号指定
                strSQL = strSQL & "SELECT * FROM OrdH_Dat"
                strSQL = strSQL & " WHERE OrdH_009 ='" & EditSQLAddSQuot(NCnvN(vKey)) & "'" '見積番号指定

            Case 2  '見積明細データ
                strSQL = strSQL & "SELECT * FROM OrdM_Dat TM,OrdD_Dat TD"
                strSQL = strSQL & " WHERE TM.OrdM_002 = '" & voRec.Rows(0)("OrdH_009") & "'"   '見積№
                strSQL = strSQL & "   AND TD.OrdD_002 = TM.OrdM_002"                  '見積№
                strSQL = strSQL & "   AND TD.OrdD_003 = TM.OrdM_003"                  'SEQ
                strSQL = strSQL & " ORDER BY TD.OrdD_002,TD.OrdD_003,TD.OrdD_004"
        End Select

        CreateSQLText = strSQL

    End Function

    '***************************************************************
    '**  名称    : Function GetTargetSheet() As Long
    '**  機能    : 出力モードとカレントページから対象シート番号を返す。
    '**  戻り値  : 対象となるExcelシート番号
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function GetTargetSheet() As Long

        'モードによりシート番号を設定
        GetTargetSheet = mCurPage
        '    Select Case mCurMode
        '        Case 0  'プレビュー
        '            GetTargetSheet = mCurPage
        '        Case 1  '印刷
        '            GetTargetSheet = pstPrintP
        '    End Select

    End Function
    '***************************************************************
    '**  名称    : Sub sttSetHeader
    '**  機能    : ヘッダー情報を変数にセットする。
    '**  戻り値  :
    '**  引数    : voRec:見積ヘッダデータ
    '**  備考    :
    '***************************************************************
    Private Sub sttSetHeader(voRec As DataTable)

        Dim lsManiTan As String               'INSERT 2016/02/05 AOKI



        Erase printHed
        ReDim printHed(voRec.Rows.Count - 1)
        With printHed(0)
            .TokCd = NCnvZ(voRec.Rows(0)("OrdH_0031"))          '得意先コード       'INSERT 2017/12/01 AOKI
            .Country = NCnvZ(voRec.Rows(0)("OrdH_007"))          '海外/国内区分
            'UPDATE 2015/08/11 AOKI START -----------------------------------------------------------------------------------------
            ''UPDATE 2014/10/02 AOKI START ----------------------------------------------------------------------------
            ''        .Messrs = NCnvN(voRec![OrdH_003])           '客先名
            ''        If NCnvN(voRec![OrdH_022]) <> "" And NCnvZ(voRec![OrdH_0031]) = 204 Then           'DELETE 2014/10/28 AOKI
            '        If NCnvZ(voRec![OrdH_0031]) = 204 Then      'Ost-West-Handel und Schiffahrt GmbH    'INSERT 2014/10/28 AOKI
            '            .Messrs = NCnvN(voRec![OrdH_022])       'オーナー名
            '        Else
            '            .Messrs = NCnvN(voRec![OrdH_003])       '得意先名
            '        End If
            ''UPDATE 2014/10/02 AOKI E N D ----------------------------------------------------------------------------
            Select Case CInt(voRec.Rows(0)("OrdH_0031"))
                Case 204                                    'Ost-West-Handel und Schiffahrt GmbH
                    .Messrs = NCnvN(voRec.Rows(0)("OrdH_0022"))        'オーナー名

                Case 117                                    'Five Stars Shipping Company Pvt. Ltd
                    If NCnvN(voRec.Rows(0)("OrdH_006")) = "KANAK PREM" Then
                        .Messrs = NCnvN(voRec.Rows(0)("OrdH_0022"))  'オーナー名
                    Else
                        .Messrs = NCnvN(voRec.Rows(0)("OrdH_003"))   '得意先名
                    End If

'INSERT 2016/12/02 AOKI START -----------------------------------------------------------------------------------------
                Case 138                                    'ISM Ship Management Pte Ltd
                    Select Case NCnvN(voRec.Rows(0)("OrdH_006"))
'                    Case "QUEEN SAPPHIRE", "AFRICAN SETO", "OCEAN DIAMOND", "HANJIN SHIKOKU", "CHORUS", "GLOBAL SYMPHONY", "MARITIME UNITY", "ANSAC WYOMING", "HL SHIKOKU"      'UPDATE 2016/12/12 AOKI    'DELETE 2017/01/24 AOKI
                        Case "QUEEN SAPPHIRE", "AFRICAN SETO", "OCEAN DIAMOND", "HL SHIKOKU", "CHORUS", "GLOBAL SYMPHONY", "MARITIME UNITY", "ANSAC WYOMING", "ANSAC MOON BEAR"     'INSERT 2017/01/24 AOKI
                            .Messrs = "ISM Pte Ltd"
                        Case "BULK GUATEMALA", "PACIFIC ENDURANCE", "AOM JULIA", "NAVIOS PROSPERITY", "IKAN SELIGI", "KING ISLAND", "JUBILANT DREAM"                                'INSERT 2017/01/24 AOKI
                            .Messrs = "KISM Pte Ltd"                                                                                                                                'INSERT 2017/01/24 AOKI
                        Case Else
                            .Messrs = NCnvN(voRec.Rows(0)("OrdH_003"))   '得意先名
                    End Select
'INSERT 2016/12/02 AOKI E N D -----------------------------------------------------------------------------------------

'INSERT 2019/02/07 AOKI START -----------------------------------------------------------------------------------------
'UPDATE 2019/03/15 AOKI START -----------------------------------------------------------------------------------------
'            Case 398
'                Select Case NCnvN(voRec![OrdH_006])
''                    Case "EVER GLORY 18"                                   'DELETE 2019/02/21 AOKI
'                    Case "EVER GLORY 18", "SUPER SKY", "SUPER LOTUS"        'INSERT 2019/02/21 AOKI
'                        'GlorySparkling Inc.
'                        .Messrs = NCnvN(voRec![OrdH_022])   'オーナー名
'                    Case Else
'                        .Messrs = NCnvN(voRec![OrdH_003])   '得意先名
'                End Select
                Case 398                                    'WINSON SHIPPING (TAIWAN) CO., LTD
                    If NCnvN(voRec.Rows(0)("OrdH_022")) = "" Then
                        .Messrs = NCnvN(voRec.Rows(0)("OrdH_003"))   '得意先名
                    Else
                        .Messrs = NCnvN(voRec.Rows(0)("OrdH_022"))   'オーナー名
                    End If
                    'UPDATE 2019/03/15 AOKI E N D -----------------------------------------------------------------------------------------
                    'INSERT 2019/02/07 AOKI E N D -----------------------------------------------------------------------------------------

                Case Else
                    .Messrs = NCnvN(voRec.Rows(0)("OrdH_003"))       '得意先名
            End Select
            'UPDATE 2015/08/11 AOKI E N D -----------------------------------------------------------------------------------------

            'UPDATE 2014/04/04 AOKI START ----------------------------------------------------------------------------
            '        .Attn = NCnvN(voRec![OrdH_004])             '客先担当者名
            If NCnvN(voRec.Rows(0)("OrdH_004")) = "" Then
                .Attn = "PERSON IN CHARGE"
            Else
                .Attn = NCnvN(voRec.Rows(0)("OrdH_004"))         '客先担当者名
            End If
            'UPDATE 2014/04/04 AOKI E N D ----------------------------------------------------------------------------

            .YourRefNo = NCnvN(voRec.Rows(0)("OrdH_005"))        '客先依頼№
            .YourOrderNo = NCnvN(voRec.Rows(0)("OrdH_017"))      '客先注文№     'INSERT 2014/06/12 AOKI
            .VesselsName = NCnvN(voRec.Rows(0)("OrdH_006"))      '船名
            .Date = NCnvN(voRec.Rows(0)("OrdH_010"))             '日付

            '--- UPDATE 2013/09/06 AOKI START ----------------------------------------------------------------------------
            '.TantoNm = NCnvN(voRec![OrdH_008])          '当社担当者

            Dim KbnMst(1) As KbnMstInfo

            KbnMst(1).Kbn_001 = 0
            KbnMst(1).Kbn_002 = "担当者"
            KbnMst(1).Kbn_003 = -1
            KbnMst(1).Kbn_004 = NCnvN(voRec.Rows(0)("OrdH_008"))
            KbnMst(1).Kbn_005 = ""

            'UPDATE 2016/02/05 AOKI START -----------------------------------------------------------------------------------------
            '        If GetKbnMstInfo2(KbnMst(1)) = True Then
            '            .TantoNm = NCnvN(KbnMst(1).Kbn_005)     '当社担当者
            '        Else
            '            .TantoNm = NCnvN(KbnMst(1).Kbn_004)
            '        End If

            Select Case NCnvN(voRec.Rows(0)("OrdH_0081"))
                Case "ELMER"
                    lsManiTan = "E"

                Case "AIZA"
                    lsManiTan = "Z"

                Case "HENIE"
                    lsManiTan = "H"

                Case "MIKE"
                    lsManiTan = "M"

                Case "ANNIE"
                    lsManiTan = "N"

                Case "EVA"                  'INSERT 2017/03/29 AOKI
                    lsManiTan = "V"         'INSERT 2017/03/29 AOKI

                Case "CHRISTINA"            'INSERT 2017/04/03 AOKI
                    lsManiTan = "C"         'INSERT 2017/04/03 AOKI

                Case "JING"                 'INSERT 2017/10/20 AOKI
                    lsManiTan = "J"         'INSERT 2017/10/20 AOKI

                Case "PHINE"                'INSERT 2018/01/11 AOKI
                    lsManiTan = "P"         'INSERT 2018/01/11 AOKI

                Case Else
                    lsManiTan = " "
            End Select

            'UPDATE 2019/05/08 AOKI START -----------------------------------------------------------------------------------------
            'lsManiTan = lsManiTan & Space(1) & Left(NCnvN(voRec![OrdH_0082]), 1)        'INSERT 2019/04/25 AOKI
            If NCnvN(voRec.Rows(0)("OrdH_0082")) = "" Then
                lsManiTan = lsManiTan & Space(2)
            Else
                lsManiTan = lsManiTan & Space(1) & Left(NCnvN(voRec.Rows(0)("OrdH_0082")), 1)
            End If
            'UPDATE 2019/05/08 AOKI E N D -----------------------------------------------------------------------------------------

            If GetKbnMstInfo2(KbnMst(1)) = True Then
                .TantoNm = NCnvN(KbnMst(1).Kbn_005) & "," & lsManiTan     '当社担当者
            Else
                .TantoNm = NCnvN(KbnMst(1).Kbn_004) & "," & lsManiTan
            End If
            'UPDATE 2016/02/05 AOKI E N D -----------------------------------------------------------------------------------------
            '--- UPDATE 2013/09/06 AOKI E N D ----------------------------------------------------------------------------

            .OurEstimateNo = NCnvN(voRec.Rows(0)("OrdH_009"))    '見積№
        End With

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteHeader
    '**  機能    : ヘッダー情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeader()

        With xlApp.Worksheets(GetTargetSheet)
            If printHed(0).Country = "0" Then
                '国内
                .Cells(1, 23).Value = "見積書"
            Else
                '海外
                .Cells(1, 23).Font.NAME = "Arial Black"

                'UPDATE 2014/06/12 AOKI START -------------------------------------------------------------------------------------------
                '            .Cells(1, 23).Value = "ESTIMATE"
                If FlgF6 = True Then
                    .Cells(1, 23).Value = "PROFORMA INVOICE"
                Else
                    .Cells(1, 23).Value = "ESTIMATE"
                End If
                'UPDATE 2014/06/12 AOKI E N D -------------------------------------------------------------------------------------------

            End If
            .Cells(8, 5).Value = printHed(0).Messrs         '客先名
            .Cells(10, 5).Value = printHed(0).Attn          '客先担当者名
            .Cells(14, 6).Value = printHed(0).YourRefNo     '客先依頼Ｎｏ

            If FlgF6 = True Then                                                    'INSERT 2014/06/12 AOKI
                .Cells(14, 15).Font.NAME = "Century"                                'INSERT 2014/06/12 AOKI
                .Cells(14, 15).Font.Size = 8                                        'INSERT 2014/06/12 AOKI
                If printHed(0).YourOrderNo <> "" Then                               'INSERT 2014/06/17 AOKI
                    .Cells(14, 15).Value = "Your Order No:"         'Your Order No  'INSERT 2014/06/12 AOKI
                    .Cells(14, 18).Value = printHed(0).YourOrderNo  '客先注文Ｎｏ   'INSERT 2014/06/12 AOKI
                End If                                                              'INSERT 2014/06/17 AOKI
            End If                                                                  'INSERT 2014/06/12 AOKI

            .Cells(16, 7).Value = printHed(0).VesselsName   '船名
            '日付
            If printHed(0).Country = "0" Then
                '国内
                .Cells(8, 31).Value = printHed(0).Date
            Else
                '海外
                '            .Cells(8, 34).Value = Format(printHed(0).Date, "DD/MM/YYYY")
                .Cells(8, 31).Value = Convert.ToDateTime(printHed(0).Date).ToString("dd/MMM/yyyy")
            End If
            '        .Cells(12, 31).Value = printHed(0).TantoNm          '当社担当者                                    'DELETE 2016/02/05 AOKI
            '        .Cells(12, 31).Value = Left(printHed(0).TantoNm, Len(printHed(0).TantoNm) - 2)      '当社担当者     'INSERT 2016/02/05 AOKI        'DELETE 2019/05/08 AOKI
            .Cells(12, 31).Value = Left(printHed(0).TantoNm, Len(printHed(0).TantoNm) - 4)      '当社担当者                                     'INSERT 2019/05/08 AOKI
            .Cells(16, 31).Value = printHed(0).OurEstimateNo    '見積Ｎｏ
            '        .Cells(16, 39).Value = Right(printHed(0).TantoNm, 1)                                'マニラ担当     'INSERT 2016/02/05 AOKI        'DELETE 2019/04/25 AOKI
            .Cells(16, 39).Value = Right(printHed(0).TantoNm, 3)                                'マニラ＆見積担当                               'INSERT 2019/04/25 AOKI
        End With

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteHeaderPage
    '**  機能    : ヘッダー　見積№毎のページを出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeaderPage()

        Dim glb As clsGlobal = New clsGlobal()

        Dim lWk As Long
        Dim lCnt As Long
        Dim AllPage As Long
        Dim SheeyWk As Long

        AllPage = PageWk(1) - PageWk(0) + 1  '全ページ数
        SheeyWk = PageWk(0)                  '開始シート

        For lWk = 1 To AllPage
            lCnt = lCnt + 1
            '「ページ/全ページ」印字
            With xlApp.Worksheets(SheeyWk)
                .Cells(5, 38).Value = lCnt & "/" & AllPage
            End With

            '印刷時
            If mCurMode = enmPrintmode.PrintOut Then
                Call steExcelPrint(SheeyWk, ActivePrinter.Printer) 'Excel 印刷
            End If

            SheeyWk = SheeyWk + 1
        Next lWk

    End Sub

    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteMidHeader
    '**  機能    : 中ヘッダー情報をExcelに出力する。
    '**  戻り値  : voRec:見積明細データ   viMHedCnt:中ヘッダ印字カウント
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMidHeader(voRec As DataRow, ByRef viMHedCnt As Integer)

        '中ヘッダ印字２回目以降は１行あける
        If viMHedCnt > 0 Then
            '改行
            Call CheckPages()
        End If

        'UNIT
        If NCnvN(voRec("OrdM_004")) <> "" Then
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = "UNIT:"
                .Cells(mCurRows, 7).Value = NCnvN(voRec("OrdM_004"))
            End With
        End If

        'MAKER
        If NCnvN(voRec("OrdM_005")) <> "" Then
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = "MAKER:"
                .Cells(mCurRows, 7).Value = NCnvN(voRec("OrdM_005"))
            End With
        End If

        'TYPE
        If NCnvN(voRec("OrdM_006")) <> "" Then

            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = "TYPE: "
                .Cells(mCurRows, 7).Value = NCnvN(voRec("OrdM_006"))
            End With
        End If
        'SER NO
        If NCnvN(voRec("OrdM_007")) <> "" Then
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = "SER/NO:"
                .Cells(mCurRows, 7).Value = NCnvN(voRec("OrdM_007"))
            End With
        End If
        'DWG NO
        If NCnvN(voRec("OrdM_008")) <> "" Then
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = "DWG NO:"
                .Cells(mCurRows, 7).Value = NCnvN(voRec("OrdM_008"))
            End With
        End If
        'ADDITIONAL DETAILS
        If NCnvN(voRec("OrdM_025")) <> "" Then
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = "ADDITIONAL DETAILS: " & NCnvN(voRec("OrdM_025"))
                '''            .Cells(mCurRows, 7).Value = NCnvN(voRec![OrdM_025])
            End With
        End If
        '改行
        Call CheckPages()
        '中ヘッダ印字カウント
        viMHedCnt = viMHedCnt + 1

        BefPrint = 1

    End Sub

    '***************************************************************
    '**  名称    : Sub sttWriteMeisai
    '**  機能    : 明細情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    : voRec:見積明細データ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMeisai(voRec As DataRow)

        Dim strWk As String
        Dim strHinNm As String
        Dim strHinNmBunkatu(9) As String
        Dim strHinban As String
        Dim strHinBanBunkatu(4) As String
        Dim Mojisu As Integer
        Dim lsWork As String
        Dim i As Integer

        Dim strHinNmWork() As String       'INSERT 2014/10/30 AOKI
        Dim j As Integer      'INSERT 2014/10/30 AOKI

        '改ページチェック
        Call CheckPages()

        strWk = NCnvN(voRec("OrdD_004"))

        If Right(strWk, 1) = "0" Then
            If BefPrint = 0 Then
                '直前に印字した行が明細行ならば、一行空ける
                Call CheckPages()
            End If

            '品名のみ
            Erase strHinNmBunkatu
            lsWork = NCnvN(voRec("OrdD_005"))

            If InStr(lsWork, "*") > 0 Then
                '"*"で改行
                i = 0
                For i = LBound(strHinNmBunkatu) To UBound(strHinNmBunkatu)
                    If InStr(lsWork, "*") > 0 Then
                        If InStr(lsWork, "*") > 35 Then
                            strHinNmBunkatu(i) = Mid(lsWork, 1, 34)
                            lsWork = Mid(lsWork, 35, Len(lsWork))
                        Else
                            strHinNmBunkatu(i) = Mid(lsWork, 1, InStr(lsWork, "*") - 1)
                            If InStr(lsWork, "*") + 1 <= Len(lsWork) Then
                                lsWork = Mid(lsWork, InStr(lsWork, "*") + 1, Len(lsWork))
                            Else
                                lsWork = ""
                            End If
                        End If
                    Else
                        strHinNmBunkatu(i) = lsWork
                        Exit For
                    End If
                Next i
            Else
                'UPDATE 2014/10/30 AOKI START -----------------------------------------------------------------------------------------
                '            '34文字ごとに分ける
                '            Mojisu = Len(lsWork)
                '            If Mojisu > 0 Then
                '                strHinNmBunkatu(0) = Trim(Left(lsWork, 34))
                '            End If
                '            If Mojisu > 34 Then
                '                strHinNmBunkatu(1) = Trim(Mid(lsWork, 35, 34))
                '            End If
                '            If Mojisu > 68 Then
                '                strHinNmBunkatu(2) = Trim(Mid(lsWork, 69, 34))
                '            End If
                '            If Mojisu > 102 Then
                '                strHinNmBunkatu(3) = Trim(Mid(lsWork, 103, 34))
                '            End If
                '            If Mojisu > 136 Then
                '                strHinNmBunkatu(4) = Trim(Mid(lsWork, 137, 34))
                '            End If
                '            If Mojisu > 170 Then
                '                strHinNmBunkatu(5) = Trim(Mid(lsWork, 171, 34))
                '            End If
                '            If Mojisu > 204 Then
                '                strHinNmBunkatu(6) = Trim(Mid(lsWork, 205, 34))
                '            End If
                '            If Mojisu > 238 Then
                '                strHinNmBunkatu(7) = Trim(Mid(lsWork, 239, 34))
                '            End If
                '            If Mojisu > 272 Then
                '                strHinNmBunkatu(8) = Trim(Mid(lsWork, 273, 28))
                '            End If

                j = 0
                strHinNmWork = Split(lsWork, " ")

                If Len(lsWork) > 34 Then
                    For i = 0 To UBound(strHinNmWork)
                        If Len(strHinNmBunkatu(j)) + Len(strHinNmWork(i)) > 34 Then
                            j = j + 1
                        End If

                        strHinNmBunkatu(j) = Trim(strHinNmBunkatu(j) & Space(1) & strHinNmWork(i))
                    Next
                Else
                    strHinNmBunkatu(0) = lsWork
                End If
                'UPDATE 2014/10/30 AOKI E N D -----------------------------------------------------------------------------------------
            End If

            '品名１
            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(0))
                .Cells(mCurRows, 4).Font.Bold = True
            End With

            '品名２
            If strHinNmBunkatu(1) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(1))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名３
            If strHinNmBunkatu(2) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(2))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名４
            If strHinNmBunkatu(3) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(3))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名５
            If strHinNmBunkatu(4) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(4))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名６
            If strHinNmBunkatu(5) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(5))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名７
            If strHinNmBunkatu(6) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(6))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名８
            If strHinNmBunkatu(7) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(7))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名９
            If strHinNmBunkatu(8) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(8))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名１０
            If strHinNmBunkatu(9) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(9))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If

        Else

            If NCnvN(voRec("OrdD_009")) = "" Then
                '改ページチェック
                Call CheckPages()
            End If
            With xlApp.Worksheets(GetTargetSheet)

                strWk = Left(strWk, Len(strWk) - 1)
                ''            '項目
                ''            .Cells(mCurRows, 2).Value = strWk
                If NCnvN(voRec("OrdD_009")) <> "" Then
                    '項目
                    .Cells(mCurRows, 2).Value = strWk
                End If
                '            '品名
                '            '30文字ごとに分ける
                '            Erase strHinNmBunkatu
                '            strHinNm = NCnvN(voRec![OrdD_005])
                '            Mojisu = Len(strHinNm)
                '            If Mojisu > 0 Then
                '                strHinNmBunkatu(0) = Trim(Left(strHinNm, 30))
                '            End If
                '            If Mojisu > 30 Then
                '                strHinNmBunkatu(1) = Trim(Mid(strHinNm, 31, 30))
                '            End If
                '            If Mojisu > 60 Then
                '                strHinNmBunkatu(2) = Trim(Mid(strHinNm, 61, 30))
                '            End If
                '            If Mojisu >= 90 Then
                '                strHinNmBunkatu(3) = Trim(Mid(strHinNm, 91, Len(strHinNm)))
                '            End If

                '品名
                Erase strHinNmBunkatu
                ReDim strHinNmBunkatu(9)
                lsWork = NCnvN(voRec("OrdD_005"))

                If InStr(lsWork, "*") > 0 Then
                    '"*"で改行
                    i = 0
                    For i = LBound(strHinNmBunkatu) To UBound(strHinNmBunkatu)
                        If InStr(lsWork, "*") > 0 Then
                            If InStr(lsWork, "*") > 35 Then
                                strHinNmBunkatu(i) = Mid(lsWork, 1, 34)
                                lsWork = Mid(lsWork, 35, Len(lsWork))
                            Else
                                strHinNmBunkatu(i) = Mid(lsWork, 1, InStr(lsWork, "*") - 1)
                                If InStr(lsWork, "*") + 1 <= Len(lsWork) Then
                                    lsWork = Mid(lsWork, InStr(lsWork, "*") + 1, Len(lsWork))
                                Else
                                    lsWork = ""
                                End If
                            End If
                        Else
                            strHinNmBunkatu(i) = lsWork
                            Exit For
                        End If
                    Next i

                Else
                    'UPDATE 2014/10/30 AOKI START -----------------------------------------------------------------------------------------
                    '                '30文字ごとに分ける
                    '                Mojisu = Len(lsWork)
                    '                If Mojisu > 0 Then
                    '                    strHinNmBunkatu(0) = Trim(Left(lsWork, 34))
                    '                End If
                    '                If Mojisu > 34 Then
                    '                    strHinNmBunkatu(1) = Trim(Mid(lsWork, 35, 34))
                    '                End If
                    '                If Mojisu > 68 Then
                    '                    strHinNmBunkatu(2) = Trim(Mid(lsWork, 69, 34))
                    '                End If
                    '                If Mojisu > 102 Then
                    '                    strHinNmBunkatu(3) = Trim(Mid(lsWork, 103, 34))
                    '                End If
                    '                If Mojisu > 136 Then
                    '                    strHinNmBunkatu(4) = Trim(Mid(lsWork, 137, 34))
                    '                End If
                    '                If Mojisu > 170 Then
                    '                    strHinNmBunkatu(5) = Trim(Mid(lsWork, 171, 34))
                    '                End If
                    '                If Mojisu > 204 Then
                    '                    strHinNmBunkatu(6) = Trim(Mid(lsWork, 205, 34))
                    '                End If
                    '                If Mojisu > 238 Then
                    '                    strHinNmBunkatu(7) = Trim(Mid(lsWork, 239, 34))
                    '                End If
                    '                If Mojisu > 272 Then
                    '                    strHinNmBunkatu(8) = Trim(Mid(lsWork, 273, 28))
                    '                End If

                    j = 0
                    strHinNmWork = Split(lsWork, " ")

                    If Len(lsWork) > 34 Then
                        For i = 0 To UBound(strHinNmWork)
                            If Len(strHinNmBunkatu(j)) + Len(strHinNmWork(i)) > 34 Then
                                j = j + 1
                            End If

                            strHinNmBunkatu(j) = Trim(strHinNmBunkatu(j) & Space(1) & strHinNmWork(i))
                        Next
                    Else
                        strHinNmBunkatu(0) = lsWork
                    End If
                    'UPDATE 2014/10/30 AOKI E N D -----------------------------------------------------------------------------------------
                End If

                Call ExcelSet(True)
                .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(0))

                '            '品番
                '            Erase strHinBanBunkatu
                '            '12文字を超えたら２行に分ける
                '            strHinban = NCnvN(voRec![OrdD_006])
                '            Mojisu = Len(strHinban)
                '            If Mojisu > 0 Then
                '                strHinBanBunkatu(0) = Trim(Left(strHinban, 12))
                '            End If
                '            If Mojisu > 12 Then
                '                strHinBanBunkatu(1) = Trim(Mid(strHinban, 13, Len(strHinban)))
                '            End If

                '品番
                Erase strHinBanBunkatu
                ReDim strHinBanBunkatu(4)
                lsWork = NCnvN(voRec("OrdD_006"))
                If InStr(lsWork, "*") > 0 Then
                    '"*"で改行
                    i = 0
                    For i = LBound(strHinBanBunkatu) To UBound(strHinBanBunkatu)
                        If InStr(lsWork, "*") > 0 Then
                            If InStr(lsWork, "*") > 17 Then
                                strHinBanBunkatu(i) = Mid(lsWork, 1, 16)
                                lsWork = Mid(lsWork, 17, Len(lsWork))
                            Else
                                strHinBanBunkatu(i) = Mid(lsWork, 1, InStr(lsWork, "*") - 1)
                                If InStr(lsWork, "*") + 1 <= Len(lsWork) Then
                                    lsWork = Mid(lsWork, InStr(lsWork, "*") + 1, Len(lsWork) - InStr(lsWork, "*"))
                                Else
                                    lsWork = ""
                                End If
                            End If
                        Else
                            strHinBanBunkatu(i) = lsWork
                            Exit For
                        End If
                    Next i

                Else
                    '12文字ごとに分ける
                    Mojisu = Len(lsWork)
                    If Mojisu > 0 Then
                        strHinBanBunkatu(0) = Trim(Left(lsWork, 16))
                    End If
                    If Mojisu > 16 Then
                        strHinBanBunkatu(1) = Trim(Mid(lsWork, 17, 16))
                    End If
                    If Mojisu > 32 Then
                        strHinBanBunkatu(2) = Trim(Mid(lsWork, 33, 16))
                    End If
                    If Mojisu > 48 Then
                        strHinBanBunkatu(3) = Trim(Mid(lsWork, 49, 12))
                    End If
                End If

                '品番１
                .Cells(mCurRows, 18).Value = NCnvN(strHinBanBunkatu(0))
                '見積数量
                .Cells(mCurRows, 25).Value = Format(voRec("OrdD_008"), "#,###")
                '単位
                .Cells(mCurRows, 27).Value = NCnvN(voRec("OrdD_009"))

                'UPDATE 2015/03/24 AOKI START -----------------------------------------------------------------------------------------
                '            '見積販売単価
                '            .Cells(mCurRows, 33).Value = Format(NCnvZ(voRec![OrdD_010]), "#,###")
                '            '見積販売金額
                '            .Cells(mCurRows, 39).Value = Format(NCnvZ(voRec![OrdD_011]), "#,###")

                xlApp.DisplayAlerts = False

                '見積販売単価
                .Range("AC" & mCurRows & ":AG" & mCurRows).Merge
                .Range("AC" & mCurRows & ":AG" & mCurRows).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
                .Cells(mCurRows, 29).Value = Format(voRec("OrdD_010"), "#,###")
                '見積販売金額
                .Range("AH" & mCurRows & ":AM" & mCurRows).Merge
                .Range("AH" & mCurRows & ":AM" & mCurRows).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
                .Cells(mCurRows, 34).Value = Format(voRec("OrdD_011"), "#,###")

                xlApp.DisplayAlerts = True
                'UPDATE 2015/03/24 AOKI START -----------------------------------------------------------------------------------------

                '金額合計
                TotalAmount = TotalAmount + NCnvZ(voRec("OrdD_011"))
            End With

            '品名２　OR 品番２
            If strHinNmBunkatu(1) <> "" Or strHinBanBunkatu(1) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名２
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(1))
                    '品番２
                    .Cells(mCurRows, 18).Value = NCnvN(strHinBanBunkatu(1))
                End With
            End If

            '品名３　OR 品番３
            If strHinNmBunkatu(2) <> "" Or strHinBanBunkatu(2) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名３
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(2))
                    '品番３
                    .Cells(mCurRows, 18).Value = NCnvN(strHinBanBunkatu(2))
                End With
            End If

            '品名４　OR 品番４
            If strHinNmBunkatu(3) <> "" Or strHinBanBunkatu(3) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名４
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(3))
                    '品番４
                    .Cells(mCurRows, 18).Value = NCnvN(strHinBanBunkatu(3))
                End With
            End If

            '品名５　OR 品番５
            If strHinNmBunkatu(4) <> "" Or strHinBanBunkatu(4) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名５
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(4))
                    '品番５
                    .Cells(mCurRows, 18).Value = NCnvN(strHinBanBunkatu(4))
                End With
            End If

            '品名６
            If strHinNmBunkatu(5) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名６
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(5))
                End With
            End If

            '品名７
            If strHinNmBunkatu(6) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名７
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(6))
                End With
            End If

            '品名８
            If strHinNmBunkatu(7) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名８
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(7))
                End With
            End If

            '品名９
            If strHinNmBunkatu(8) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名９
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(8))
                End With
            End If

            '品名１０
            If strHinNmBunkatu(9) <> "" Then
                '改ページチェック
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(True)
                    '品名１０
                    .Cells(mCurRows, 4).Value = NCnvN(strHinNmBunkatu(9))
                End With
            End If

        End If

        BefPrint = 0

    End Sub

    '***************************************************************
    '**  名称    : Sub ExcelSet
    '**  機能    : 行の設定
    '**  戻り値  :
    '**  引数    : vbKbn True: 列を結合し縮小して全体を表示にする
    '**          :       False:元に戻す
    '**  備考    :
    '***************************************************************
    Private Sub ExcelSet(vbKbn As Boolean)

        Dim lsWork As String


        If vbKbn = True Then

            lsWork = "D" & mCurRows & ": Q" & mCurRows

            '''        With Worksheets(GetTargetSheet).Range(lsWork)
            With xlApp.Worksheets(GetTargetSheet).Range(lsWork)
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .WrapText = False
                .Orientation = 0
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = True
                .MergeCells = True
            End With
            With xlApp.Worksheets(GetTargetSheet).Range(lsWork).Font
                .NAME = "Arial"
                .FontStyle = "標準"
                .Size = 9
                .Strikethrough = False
                .Superscript = False
                .Subscript = False
                .OutlineFont = False
                .Shadow = False
                .Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone
                .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
            End With

            lsWork = "R" & mCurRows & ":X" & mCurRows

            With xlApp.Worksheets(GetTargetSheet).Range(lsWork)
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .WrapText = False
                .Orientation = 0
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = True
                .MergeCells = True
            End With
            With xlApp.Worksheets(GetTargetSheet).Range(lsWork).Font
                .NAME = "Arial"
                .FontStyle = "標準"
                .Size = 9
                .Strikethrough = False
                .Superscript = False
                .Subscript = False
                .OutlineFont = False
                .Shadow = False
                .Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone
                .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
            End With

        Else

            lsWork = "D" & mCurRows & ":Q" & mCurRows
            With xlApp.Worksheets(GetTargetSheet).Range(lsWork)
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .WrapText = False
                .Orientation = 0
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
                .NumberFormatLocal = "@"                        'INSERT 2016/03/16 AOKI
            End With

            lsWork = "R" & mCurRows & ":X" & mCurRows
            With xlApp.Worksheets(GetTargetSheet).Range(lsWork)
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .WrapText = False
                .Orientation = 0
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
            End With

        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub CheckPages
    '**  機能    : 出力ページと行のチェックと改頁処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub CheckPages()

        '行をインクリメント
        mCurRows = mCurRows + 1

        '行チェック
        If mCurRows > pstLastRow Then
            mCurPage = mCurPage + 1     'ページインクリメント
            '改頁処理
            Call SJKExcelCom.stePrevSheetCopy(mCurPage)              'プレビュー・印刷用Bookのコピー
            Call loSheetClear()                            'Excelシートクリア

            'プレビューからの見積発行日更新用キー作成
            ReDim Preserve OrdHUPDKey(mCurPage)
            OrdHUPDKey(mCurPage).KEIYAKUNO = KeiyakuNoWk      '契約№
            OrdHUPDKey(mCurPage).RefNo = RefNoWk              '見積№

            mCurRows = pstStartRow - 1   '行をリセット
            Call sttWriteHeader()          'ヘッダー情報の出力
        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub sttWriteFooter
    '**  機能    : フッター情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :  voRec:見積ヘッダデータ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteFooter(voRec As DataTable)

        Dim strWk As String
        Dim TekiyoWk(3) As String
        '--- INSERT 2011/10/31 青木 START --------------------------------------------------------------------------
        '    Dim NoukiWk(3) As String           'DELETE 2016/04/14 AOKI
        Dim NoukiWk() As String             'INSERT 2016/04/14 AOKI
        Dim Mojisu As Integer
        '--- INSERT 2011/10/31 青木 E N D --------------------------------------------------------------------------
        Dim i As Integer

        Call CheckPages()
        Call CheckPages()
        '罫線
        strWk = "A" & mCurRows & ":" & "AN" & mCurRows
        xlWs = xlApp.Worksheets(GetTargetSheet)

        With xlWs.Range(strWk).Borders(Excel.XlBordersIndex.xlEdgeTop)
            .LineStyle = Excel.XlLineStyle.xlContinuous
            .Weight = Excel.XlBorderWeight.xlMedium
            .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
        End With



        '--- DELETE 2011/10/31 青木 START --------------------------------------------------------------------------
        '    Call CheckPages
        '--- DELETE 2011/10/31 青木 E N D --------------------------------------------------------------------------

        'UPDATE 2016/04/14 AOKI START -----------------------------------------------------------------------------------------
        ''--- UPDATE 2011/10/31 青木 START --------------------------------------------------------------------------
        ''    'Delivery Time:
        ''    With Worksheets(GetTargetSheet)
        ''        Call ExcelSet(False)
        ''        .Cells(mCurRows, 1).Value = "Delivery Time:"
        ''        .Cells(mCurRows, 7).Value = NCnvZ(voRec![OrdH_011])
        ''        .Rows(mCurRows).RowHeight = 18
        ''    End With
        '
        '    'Delivery Time:
        '    Erase NoukiWk
        '    strWk = NCnvN(voRec![OrdH_011])
        '    If InStr(strWk, "*") > 0 Then
        '        '"*"で改行
        '        i = 0
        '        For i = LBound(NoukiWk) To UBound(NoukiWk)
        '            If InStr(strWk, "*") > 0 Then
        '                If InStr(strWk, "*") > 52 Then
        '                    NoukiWk(i) = Mid(strWk, 1, 52)
        '                    strWk = Mid(strWk, 53, Len(strWk))
        '                Else
        '                    NoukiWk(i) = Mid(strWk, 1, InStr(strWk, "*") - 1)
        '                    If InStr(strWk, "*") + 1 < Len(strWk) Then
        '                        strWk = Mid(strWk, InStr(strWk, "*") + 1, Len(strWk) - InStr(strWk, "*"))
        '                    End If
        '                End If
        '            Else
        '                NoukiWk(i) = strWk
        '                Exit For
        '            End If
        '        Next i
        '    Else
        '        '52文字ごとに分ける
        '        Mojisu = Len(strWk)
        '        If Mojisu > 0 Then
        '            NoukiWk(0) = Trim(Left(strWk, 52))
        '        End If
        '        If Mojisu > 52 Then
        '            NoukiWk(1) = Trim(Mid(strWk, 53, 52))
        '        End If
        '        If Mojisu > 104 Then
        '            NoukiWk(2) = Trim(Mid(strWk, 105, 52))
        '        End If
        '        If Mojisu > 156 Then
        '            NoukiWk(3) = Trim(Mid(strWk, 157, 52))
        '        End If
        '    End If
        '
        '    If NoukiWk(0) <> "" Then
        '        '改ページチェック
        '        Call CheckPages
        '        With Worksheets(GetTargetSheet)
        '            Call ExcelSet(False)
        '            .Cells(mCurRows, 1).Value = "Delivery Time:"
        '            .Cells(mCurRows, 7).Value = NoukiWk(0)
        '            .Rows(mCurRows).RowHeight = 18                          'INSERT 2016/02/08 AOKI
        '        End With
        '    End If
        '
        '    If NoukiWk(1) <> "" Then
        '        '改ページチェック
        '        Call CheckPages
        '        With Worksheets(GetTargetSheet)
        '            Call ExcelSet(False)
        '            .Cells(mCurRows, 7).Value = NoukiWk(1)
        '            .Rows(mCurRows).RowHeight = 18                          'INSERT 2016/02/08 AOKI
        '        End With
        '    End If
        '
        '    If NoukiWk(2) <> "" Then
        '        '改ページチェック
        '        Call CheckPages
        '        With Worksheets(GetTargetSheet)
        '            Call ExcelSet(False)
        '            .Cells(mCurRows, 7).Value = NoukiWk(2)
        '            .Rows(mCurRows).RowHeight = 18                          'INSERT 2016/02/08 AOKI
        '        End With
        '    End If
        '
        '    If NoukiWk(3) <> "" Then
        '        '改ページチェック
        '        Call CheckPages
        '        With Worksheets(GetTargetSheet)
        '            Call ExcelSet(False)
        '            .Cells(mCurRows, 7).Value = NoukiWk(3)
        '            .Rows(mCurRows).RowHeight = 18                          'INSERT 2016/02/08 AOKI
        '        End With
        '    End If
        ''--- UPDATE 2011/10/31 青木 E N D --------------------------------------------------------------------------

        'INSERT 2016/06/23 AOKI START -----------------------------------------------------------------------------------------
        '改ページチェック
        Call CheckPages()

        '金額合計
        strWk = "X" & mCurRows & ":" & "AM" & mCurRows

        With xlWs.Range(strWk).Borders(Excel.XlBordersIndex.xlEdgeBottom)
            .LineStyle = Excel.XlLineStyle.xlDouble
            .Weight = Excel.XlBorderWeight.xlThick
            .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
        End With





        With xlApp.Worksheets(GetTargetSheet)
            .Cells(mCurRows, 24).Font.NAME = "Century"
            .Cells(mCurRows, 24).Font.Size = 8
            .Cells(mCurRows, 24).Value = "    Total Amount (J YEN)  EX WORKS  "
            .Range("AH" & mCurRows & ":AM" & mCurRows).Merge
            .Range("AH" & mCurRows & ":AM" & mCurRows).HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
            .Cells(mCurRows, 34).Value = Format(TotalAmount, "#,###")
        End With

        mCurRows = mCurRows - 1
        'INSERT 2016/06/23 AOKI E N D -----------------------------------------------------------------------------------------

        i = 0
        NoukiWk = Split(NCnvN(voRec.Rows(0)("OrdH_011")), "*")

        For i = LBound(NoukiWk) To UBound(NoukiWk)
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                If i = 0 Then
                    .Cells(mCurRows, 1).Value = "Delivery Time:"
                End If
                .Cells(mCurRows, 7).Value = NoukiWk(i)
                .Rows(mCurRows).RowHeight = 18
            End With
        Next
        'UPDATE 2016/04/14 AOKI E N D -----------------------------------------------------------------------------------------

        '--- UPDATE 2011/10/31 青木 START --------------------------------------------------------------------------
        '    'Origin:
        '    If NCnvN(voRec![OrdH_012]) <> "" Then
        '        Call CheckPages
        '        With Worksheets(GetTargetSheet)
        '            Call ExcelSet(False)
        '            .Cells(mCurRows, 1).Value = "Origin:"
        '            .Cells(mCurRows, 5).Value = NCnvN(voRec![OrdH_012])
        '            .Rows(mCurRows).RowHeight = 18
        '        End With
        '    End If

        'Origin:
        Erase NoukiWk
        ReDim NoukiWk(3)                    'INSERT 2016/04/14 AOKI
        strWk = NCnvN(voRec.Rows(0)("OrdH_012"))
        If InStr(strWk, "*") > 0 Then
            '"*"で改行
            i = 0
            For i = LBound(NoukiWk) To UBound(NoukiWk)
                If InStr(strWk, "*") > 0 Then
                    If InStr(strWk, "*") > 52 Then
                        NoukiWk(i) = Mid(strWk, 1, 52)
                        strWk = Mid(strWk, 53, Len(strWk))
                    Else
                        NoukiWk(i) = Mid(strWk, 1, InStr(strWk, "*") - 1)
                        If InStr(strWk, "*") + 1 < Len(strWk) Then
                            strWk = Mid(strWk, InStr(strWk, "*") + 1, Len(strWk) - InStr(strWk, "*"))
                        End If
                    End If
                Else
                    NoukiWk(i) = strWk
                    Exit For
                End If
            Next i
        Else
            '52文字ごとに分ける
            Mojisu = Len(strWk)
            If Mojisu > 0 Then
                NoukiWk(0) = Trim(Left(strWk, 52))
            End If
            If Mojisu > 52 Then
                NoukiWk(1) = Trim(Mid(strWk, 53, 52))
            End If
            If Mojisu > 104 Then
                NoukiWk(2) = Trim(Mid(strWk, 105, 52))
            End If
            If Mojisu > 156 Then
                NoukiWk(3) = Trim(Mid(strWk, 157, 52))
            End If
        End If

        If NoukiWk(0) <> "" Then
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 1).Value = "Origin:"
                .Cells(mCurRows, 5).Value = NoukiWk(0)
                .Rows(mCurRows).RowHeight = 18
            End With
        End If

        If NoukiWk(1) <> "" Then
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 5).Value = NoukiWk(1)
                .Rows(mCurRows).RowHeight = 18
            End With
        End If

        If NoukiWk(2) <> "" Then
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 5).Value = NoukiWk(2)
                .Rows(mCurRows).RowHeight = 18
            End With
        End If

        If NoukiWk(3) <> "" Then
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 5).Value = NoukiWk(3)
                .Rows(mCurRows).RowHeight = 18
            End With
        End If
        '--- UPDATE 2011/10/31 青木 E N D --------------------------------------------------------------------------

        'Country of Origin:
        If NCnvN(voRec.Rows(0)("OrdH_013")) <> "" Then
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 1).Value = "Country of Origin:"
                .Cells(mCurRows, 8).Value = NCnvN(voRec.Rows(0)("OrdH_013"))
                .Rows(mCurRows).RowHeight = 18
            End With
        End If

        'Payment terms:
        Call CheckPages()

        With xlApp.Worksheets(GetTargetSheet)
            Call ExcelSet(False)
            .Cells(mCurRows, 1).Value = "Payment terms:"

            'UPDATE 2014/06/12 AOKI START ----------------------------------------------------------------------------------
            ''UPDATE 2013/10/08 AOKI START ----------------------------------------------------------------------------------
            ''        .Cells(mCurRows, 7).Value = NCnvZ(voRec![OrdH_015]) & " DAYS FM INVOICE DATE"
            '        If NCnvZ(voRec![OrdH_015]) = 0 Then
            '            .Cells(mCurRows, 7).Value = "CASH IN ADVANCE"
            '        Else
            '            .Cells(mCurRows, 7).Value = NCnvZ(voRec![OrdH_015]) & " DAYS FM INVOICE DATE"
            '        End If
            ''UPDATE 2013/10/08 AOKI E N D ----------------------------------------------------------------------------------
            If FlgF6 = True Then
                .Cells(mCurRows, 7).Value = "100% ADVANCE"
            Else
                If NCnvZ(voRec.Rows(0)("OrdH_015")) = 0 Then
                    .Cells(mCurRows, 7).Value = "CASH IN ADVANCE"
                Else
                    .Cells(mCurRows, 7).Value = NCnvZ(voRec.Rows(0)("OrdH_015")) & " DAYS FM INVOICE DATE"
                End If
            End If
            'UPDATE 2014/06/12 AOKI E N D ----------------------------------------------------------------------------------

            .Rows(mCurRows).RowHeight = 18
        End With

        'Validity:
        Call CheckPages()

        With xlApp.Worksheets(GetTargetSheet)
            Call ExcelSet(False)
            .Cells(mCurRows, 1).Value = "Validity:"
            .Cells(mCurRows, 5).Value = "THIS OFFER IS FIRM FOR " & NCnvZ(voRec.Rows(0)("OrdH_016")) & " DAYS"
            '        '金額合計
            '        strWk = "X" & mCurRows & ":" & "AM" & mCurRows
            '        Range(strWk).Select
            '        With Selection.Borders(xlEdgeBottom)
            '            .LineStyle = xlDouble
            '            .Weight = xlThick
            '            .ColorIndex = xlAutomatic
            '        End With

            'UPDATE 2015/03/24 AOKI START -----------------------------------------------------------------------------------------
            '.Cells(mCurRows, 26).Value = "       Total Amount (J YEN)  EX WORKS  "
            '.Cells(mCurRows, 39).Value = Format(TotalAmount, "#,###")

            'DELETE 2016/06/23 AOKI START -----------------------------------------------------------------------------------------
            '        .Cells(mCurRows, 24).Font.NAME = "Century"
            '        .Cells(mCurRows, 24).Font.Size = 8
            '        .Cells(mCurRows, 24).Value = "    Total Amount (J YEN)  EX WORKS  "
            '        .Range("AH" & mCurRows & ":AM" & mCurRows).Merge
            '        .Range("AH" & mCurRows & ":AM" & mCurRows).HorizontalAlignment = xlHAlignRight
            '        .Cells(mCurRows, 34).Value = Format(TotalAmount, "#,###")
            'DELETE 2016/06/23 AOKI E N D -----------------------------------------------------------------------------------------
            'UPDATE 2015/03/24 AOKI E N D -----------------------------------------------------------------------------------------

            .Rows(mCurRows).RowHeight = 18
        End With

        '2行あける
        Call CheckPages()
        Call CheckPages()

        '罫線
        strWk = "A" & mCurRows & ":" & "AN" & mCurRows

        With xlWs.Range(strWk).Borders(Excel.XlBordersIndex.xlEdgeBottom)
            .LineStyle = Excel.XlLineStyle.xlDashDotDot
            .Weight = Excel.XlBorderWeight.xlMedium
            .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
        End With






        'Note:
        Erase TekiyoWk
        ReDim TekiyoWk(3)
        strWk = NCnvN(voRec.Rows(0)("OrdH_014"))
        If InStr(strWk, "*") > 0 Then
            '"*"で改行
            i = 0
            For i = LBound(TekiyoWk) To UBound(TekiyoWk)
                If InStr(strWk, "*") > 0 Then
                    If InStr(strWk, "*") > 60 Then
                        TekiyoWk(i) = Mid(strWk, 1, 60)
                        strWk = Mid(strWk, 61, Len(strWk))
                    Else
                        TekiyoWk(i) = Mid(strWk, 1, InStr(strWk, "*") - 1)
                        If InStr(strWk, "*") + 1 < Len(strWk) Then
                            strWk = Mid(strWk, InStr(strWk, "*") + 1, Len(strWk) - InStr(strWk, "*"))
                        End If
                    End If
                Else
                    TekiyoWk(i) = strWk
                    Exit For
                End If
            Next i
        Else
            '60文字ごとに分ける
            '--- DELETE 2011/10/31 青木 START --------------------------------------------------------------------------
            '        Dim Mojisu As Integer
            '--- DELETE 2011/10/31 青木 E N D --------------------------------------------------------------------------
            Mojisu = Len(strWk)
            If Mojisu > 0 Then
                TekiyoWk(0) = Trim(Left(strWk, 60))
            End If
            If Mojisu > 60 Then
                TekiyoWk(1) = Trim(Mid(strWk, 61, 60))
            End If
            If Mojisu > 120 Then
                TekiyoWk(2) = Trim(Mid(strWk, 121, 60))
            End If
            If Mojisu > 180 Then
                TekiyoWk(3) = Trim(Mid(strWk, 181, 60))
            End If
        End If

        If TekiyoWk(0) <> "" Then
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 1).Value = "Note:"
                .Cells(mCurRows, 4).Value = TekiyoWk(0)
            End With
        End If

        If TekiyoWk(1) <> "" Then
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = TekiyoWk(1)
            End With
        End If

        If TekiyoWk(2) <> "" Then
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = TekiyoWk(2)
            End With
        End If

        If TekiyoWk(3) <> "" Then
            '改ページチェック
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                Call ExcelSet(False)
                .Cells(mCurRows, 4).Value = TekiyoWk(3)
            End With
        End If

        '--- INSERT 2014/01/07 青木 START --------------------------------------------------------------------------
        Call CheckPages()
        Call CheckPages()

        With xlApp.Worksheets(GetTargetSheet)
            Call ExcelSet(False)
            .Range("A" & mCurRows).Font.Size = 9
            .Cells(mCurRows, 1).Value = "< all quoted items are ""ASBESTOS FREE"" >"
        End With
        '--- INSERT 2014/01/07 青木 E N D --------------------------------------------------------------------------

        'INSERT 2014/06/12 AOKI START ------------------------------------------------------------------------------
        If FlgF6 = True Then
            Call CheckPages()
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "<< N.B. >>"
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "We shall check the latest price and delivery time after receiving your 100% advance payment."
            Call CheckPages()
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "************Bank Details************"
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "BANK NAME: MIZUHO BANK,LTD."
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "BRANCH NAME: KANDA BRANCH"
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "BRANCH ADDRESS: 1-1 KANDA OGAWAMACHI CHIYODA-KU TOKYO JAPAN"
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "SWIFT BIC: MHCBJPJT"
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "ACCOUNT NUMBER: 1249905"
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "RECEIVER'S ACCOUNT NAME: ONE'S FORTE CO.,LTD"
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "RECEIVER'S ADDRESS: 1-1 KANDA OGAWAMACHI CHIYODA-KU TOKYO JAPAN"
            Call CheckPages()
            xlApp.Worksheets(GetTargetSheet).Cells(mCurRows, 1).Value = "************************************"
            'INSERT 2015/03/24 AOKI START -----------------------------------------------------------------------------------------
        Else
            '        If frmMITMR010.numTokCode.Value = 208 _
            '        And UCase(Left(frmMITMR010.txtMaker.Text, 6)) = "YANMAR" _
            '        And frmMITMR010.cboOrigin.Text = "1:GENUINE" Then                      'DELETE 2016/02/08 AOKI
            If numtok = 208 _
                And UCase(Left(maker, 6)) = "YANMAR" _
                And origin = "GENUINE" Then                         'INSERT 2016/02/08 AOKI
                Call CheckPages()
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    Call ExcelSet(False)
                    .Range("A" & mCurRows).Font.NAME = "Arial"
                    .Range("A" & mCurRows).Font.Size = 9
                    .Cells(mCurRows, 1).Value = "ITEMS ARE FROM YANMAR / MAKER CERTIFICATE AVAILABLE"
                End With
            End If
            'INSERT 2015/03/24 AOKI E N D -----------------------------------------------------------------------------------------
        End If
        'INSERT 2014/06/12 AOKI E N D ------------------------------------------------------------------------------


        'INSERT 2017/09/25 AOKI START -----------------------------------------------------------------------------------------
        Call CheckPages()
        Call CheckPages()
        Call CheckPages()
        strWk = "     *****Once your order is placed with us, thereafter the order cannot be cancelled in any case.*****"
        With xlApp.Worksheets(GetTargetSheet)
            .Cells(mCurRows, 1).Value = strWk
            .Range("A" & mCurRows).Font.Size = 9
        End With

        If mCurRows < 64 Then
            strWk = "－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－"
            strWk = strWk & vbCrLf & "3FL YAMASHIRO BLDG, 1-1 KANDAOGAWAMACHI, CHIYODA-KU, TOKYO 101-0052 JAPAN"
            strWk = strWk & vbCrLf & "Telephone: 81-3-5283-5252  Fax: 81-3-5283-5251"
            strWk = strWk & vbCrLf & "E-mail: sales@ones-forte.jp  Website: http://www.ones-forte.jp"
            With xlApp.Worksheets(GetTargetSheet).Range("A64:AN67")
                .Merge
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
                .Font.NAME = "Century Schoolbook"
                .Font.Size = 9
                .Value = strWk
            End With
        End If
        'INSERT 2017/09/25 AOKI E N D -----------------------------------------------------------------------------------------

    End Sub

    '***************************************************************
    '**  名称    : Sub loSheetClear
    '**  機能    : シートのクリア
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub loSheetClear()

        Call steSheetClear(GetTargetSheet, pstPrintArea)
        '罫線クリア

        With xlApp.Worksheets(GetTargetSheet).Range(pstPrintArea)
            .Select
            .ClearContents
            .Borders.LineStyle = Excel.XlLineStyle.xlLineStyleNone
        End With

    End Sub

    '***************************************************************
    '**  名称    : Sub MITMR020_OrdHUpdate
    '**  機能    : 見積書発行日を更新
    '**  戻り値  :
    '**  引数    : vKey1: 契約№　vkey2:見積№
    '**  備考    :
    '***************************************************************
    Public Sub MITMR020_OrdHUpdate(vKey1 As String, vKey2 As String)

        Dim strWk As String

        strWk = "UPDATE OrdH_Dat "

        'UPDATE 2014/06/12 AOKI START ------------------------------------------------------------------------------------------
        '    strWk = strWk & " SET OrdH_028 = '" & strHakkoDate & "'"
        If FlgF6 = True Then
            strWk = strWk & " SET OrdH_0281 = '" & DateTime.Now & "'"
        Else
            strWk = strWk & " SET OrdH_028 = '" & DateTime.Now & "'"
        End If
        'UPDATE 2014/06/12 AOKI E N D ------------------------------------------------------------------------------------------

        strWk = strWk & " WHERE OrdH_001 = '" & vKey1 & "'"
        strWk = strWk & "   AND OrdH_009 = '" & vKey2 & "'"
        Dim outmsg As String
        dbsvc.ExecuteSql(strWk, outmsg)


    End Sub

    '***************************************************************
    '**  名称    : Sub File_Delete
    '**  機能    : Report\PDFフォルダ下のファイルを削除
    '**  戻り値  :
    '**  引数    : vPath:フォルダパス
    '**  備考    :
    '***************************************************************
    Private Sub File_Delete(vPath As String)

        ' FileSystemObject (FSO) の新しいインスタンスを生成する

       
        FileSystem.Kill(vPath & "*.*")
        ' 不要になった時点で参照を解放する (Terminate イベントを早めに起こす)


    End Sub
    '***************************************************************
    '**  名称    : Function MITMR020_PrevExcelOpen() As Boolean
    '**  機能    : プレビュー用ワークシートのオープン処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : vlPage ; ページ番号(ワークシート名)
    '**  備考    :
    '***************************************************************
    Public Function MITMR020_PrevExcelOpen(vlPage As Long, vlRange As String) As Boolean

        Dim exFname As String

        MITMR020_PrevExcelOpen = False

        'ファイルの存在チェック
        If SJKExcelCom.stePrevFileCheck() <> "" Then
            xlApp.Workbooks.Open(Filename:=pstrPdfNm)      'ブックオープン
            xlApp.Worksheets(vlPage).Select               'シートの選択
            xlRange.Range(vlRange).Select()                   'レンジの選択
            xlRange.Selection.Copy                          'クリップボードにコピー

            MITMR020_PrevExcelOpen = True
        End If

    End Function


End Module
