﻿

Imports System.Windows.Forms
Imports clswinApi
Imports SAHANBAI.clsGlobal
Imports winApi = clswinApi.StaticWinApi
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports SAHANBAI.TaxComp
Imports System.Transactions


Public Module modZAIMR050

    Dim xlApp As Excel.Application
    Dim xlWs As Excel.Worksheet
    Dim xlRange As Excel.Range
    Public DB As clsDB.DBService
    '***************************************************************
    '**
    '**  機能           :  仕入先別在庫実績一覧表(modZAIMR050)
    '**  作成日         :
    '**  更新日         :　2013/03/14 在庫数に調整数は含めない
    '**  備考           :
    '**
    '***************************************************************
    ''各マスタ情報
    Public ConMst(0 To 0) As ConMstInfo     'コントロールマスタ情報

    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
    End Enum

    'データ抽出条件
    Public SirDate(1) As String    '仕入日付
    Public SelSirCd() As String    '仕入先コード指定
    Public chkYMD As Integer   '0:仕入日付 1:チェック日付

    'エクセル上のセル範囲
    Private pstLastRow As Integer              '最終行
    '== 請求一覧のExcel定義 ==
    Private Const psttREPORT_NAME As String = "ZAIMR050_仕入先別在庫実績一覧表"       'Report名称
    Private Const pstPrintArea As String = "B8:M73"                    'シートクリア範囲
    'Private Const pstPrevArea      As String = "A1:AM75"                   'プレビュー表示範囲         'DELETE 2014/07/18 AOKI
    Private Const pstPrevArea As String = "A1:BF75"                   'プレビュー表示範囲          'INSERT 2014/07/18 AOKI
    Private Const pstMaxRow As Integer = 66                         '最大行数
    Private Const pstStartRow As Integer = 8                          'ページ内の開始行

    '印刷共通定義
    Private mIntMode As Integer     'カレント印刷モード
    Private mCurPage As Long        'カレントページ番号
    Private mCurRows As Long        'カレント行
    Private mCurCols As Long        'カレント列




    '***************************************************************
    '**  名称    : Function JYUMR060PrintProc() As Boolean
    '**  機能    : 仕入先別実績一覧表(明細) 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function JYUMR060PrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim loRec As New DataTable  '明細データ
        Dim befSirCd As String
        Dim befKeiNo As String
        Dim befSDate As String
        Dim lsWork As String
        Dim curSubTotal As Double
        Dim curAllTotal As Double
        Dim SCnt As Long                 '仕入先毎の明細カウント

        On Error GoTo JYUMR060PrintProc_Err

        JYUMR060PrintProc = False

        mIntMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存
        pstLastRow = pstMaxRow + pstStartRow - 1

        loRec = Nothing

        '仕入データの読込み用SQL文作成ルーチン
        strSQL = CreateSQLText(0)

        'データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        '===データ存在チェック
        If loRec.Rows.Count <= 0 Then
            'データ無しメッセージ
            Call ksExpMsgBox("印刷するデータが存在しません。", "I")
            GoTo Exit_JYUMR060PrintProc
        End If

        '===Excelファイルのオープン
        If steExcelOpen(psttREPORT_NAME & exXLSSuffix) = False Then
            'Excelファイル無しメッセージ
            Call ksExpMsgBox(psttREPORT_NAME & "のレポートが存在しません。", "E")
            GoTo Exit_JYUMR060PrintProc
        End If

        If mIntMode = enmPrintmode.Preview Then        'プレビューモード
            Call stePrevWorkDelete()  '古いワークファイルを削除する
        End If

        '===進行状況表示フォーム設定=============================================

        '===各変数初期化
        mCurPage = 1                  'ページ番号
        mCurRows = pstStartRow - 1    '明細印刷開始行
        DCount = 0                    'データカウンタ
        curSubTotal = 0
        curAllTotal = 0
        SCnt = 0

        'シートのクリア
        Call loSheetClear()

        'ヘッダー印刷
        Call sttWriteHeader(NCnvN(loRec.Rows(0)("ZaiD_007")), NCnvN(loRec.Rows(0)("ZaiD_008")))

        befSirCd = NCnvN(loRec.Rows(0)("ZaiD_007"))


        For Each dr As DataRow In loRec.Rows


            '進行状況用カウンタ
            DCount = DCount + 1

            '進行状況表示フォーム設定


            If NCnvZ(dr("ZaiD_021")) + NCnvZ(dr("ZaiD_032")) > 0 Then                       'INSERT 2012/12/06 AOKI

                '仕入先コードブレーク
                If befSirCd <> NCnvN(dr("ZaiD_007")) Then
                    '小計出力
                    Call CheckPages()
                    Call sttWriteTotal(0, curSubTotal)
                    curSubTotal = 0
                    '合計出力(１行あける）
                    Call CheckPages()
                    Call CheckPages()
                    Call sttWriteTotal(1, curAllTotal)
                    curAllTotal = 0
                    '改ページ
                    mCurRows = pstLastRow + 1
                    Call CheckPages()
                    mCurRows = pstStartRow - 1   '行をリセット
                    'ヘッダー出力
                    Call sttWriteHeader(NCnvN(dr("ZaiD_007")), NCnvN(dr("ZaiD_008")))
                    befKeiNo = ""
                    befSDate = ""
                    befSirCd = NCnvN(dr("ZaiD_007"))
                    SCnt = 0
                End If

                '仕入日付か契約番号がブレークしたら小計印字
                If chkYMD = 0 Then
                    lsWork = Format(NCnvN(dr("ZaiD_020")), "YY/MM/DD")
                Else
                    lsWork = Format(NCnvN(dr("ZaiD_028")), "YY/MM/DD")
                End If
                If SCnt > 0 And (befSDate <> lsWork Or befKeiNo <> NCnvN(dr("ZaiD_001"))) Then
                    '小計出力
                    Call CheckPages()
                    Call sttWriteTotal(0, curSubTotal)
                    curSubTotal = 0
                End If

                Call CheckPages()

                If befSDate <> lsWork Then
                    With xlApp.Worksheets(GetTargetSheet)
                        .Cells(mCurRows, 2).Value = lsWork                                       '仕入日付
                        .Cells(mCurRows, 3).Value = NCnvN(dr("ZaiD_001"))                      '契約番号
                    End With
                    befSDate = lsWork
                    befKeiNo = NCnvN(dr("ZaiD_001"))
                ElseIf befKeiNo <> NCnvN(dr("ZaiD_001")) Then
                    With xlApp.Worksheets(GetTargetSheet)
                        .Cells(mCurRows, 3).Value = NCnvN(dr("ZaiD_001"))                      '契約番号
                    End With
                    befKeiNo = NCnvN(dr("ZaiD_001"))
                Else
                    With xlApp.Worksheets(GetTargetSheet)
                        .Cells(mCurRows, 2).Value = ""   '仕入日付
                        .Cells(mCurRows, 3).Value = ""   '契約番号
                    End With
                End If

                With xlApp.Worksheets(GetTargetSheet)
                    .Cells(mCurRows, 5).Value = NCnvZ(dr("ZaiD_002"))                     'SEQ
                    .Cells(mCurRows, 6).Value = NCnvZ(dr("ZaiD_003"))                     'Item
                    .Cells(mCurRows, 7).Value = NCnvN(dr("ZaiD_005"))                     '品名

                    .Cells(mCurRows, 8).Value = Format(NCnvZ(dr("ZaiD_021")))       '数量

                    .Cells(mCurRows, 9).Value = Format(NCnvZ(dr("ZaiD_026")), "#,##0")    '単価

                    .Cells(mCurRows, 10).Value = Format(NCnvZ(dr("ZaiD_021")) * NCnvZ(dr("ZaiD_026")), "#,##0")    '金額

                End With

                curSubTotal = curSubTotal + NCnvZ(dr("ZaiD_021")) * NCnvZ(dr("ZaiD_026"))
                curAllTotal = curAllTotal + NCnvZ(dr("ZaiD_021")) * NCnvZ(dr("ZaiD_026"))


                SCnt = SCnt + 1

            End If                                                                                          'INSERT 2012/12/06 AOKI

        Next

        '小計出力
        Call CheckPages()
        Call sttWriteTotal(0, curSubTotal)
        '合計出力(１行あける）
        Call CheckPages()
        Call CheckPages()
        Call sttWriteTotal(1, curAllTotal)


        'モードごとの処理
        Select Case viMode

            Case enmPrintmode.Preview      'プレビュー

                Call stePrevBookSave()       'Bookの保存
                Call steWorkbooksClose()     'Bookのクローズ

                'プレビュー画面の起動

                Dim frm As New SAHANBAI.frmPrintPreview
                With frm
                    .ppvActCell = pstPrevArea
                    .ShowDialog()
                End With


            Case enmPrintmode.PrintOut     '印刷
                mCurRows = pstLastRow
                Call CheckPages()
                Call steWorkbooksClose()     'Bookのクローズ
        End Select


        JYUMR060PrintProc = True

Exit_JYUMR060PrintProc:

        Exit Function

JYUMR060PrintProc_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Call steWorkbooksClose()        'Bookのクローズ

        Resume Exit_JYUMR060PrintProc

    End Function
    '***************************************************************
    '**  名称    : Function JYUMR060TotalPrintProc() As Boolean
    '**  機能    : 仕入先別実績一覧表(合計) 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function JYUMR060TotalPrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim loRec As New DataTable  '明細データ
        Dim SCnt As Long                 '仕入先毎の明細カウント
        Dim befSirCd As String
        Dim curAllTotal As Double             '仕入先合計

        On Error GoTo JYUMR060TotalPrintProc_Err

        JYUMR060TotalPrintProc = False

        mIntMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存
        pstLastRow = pstMaxRow + pstStartRow - 1

        loRec = Nothing

        '仕入データの読込み用SQL文作成ルーチン
        strSQL = CreateSQLText(1)

        'データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        '===データ存在チェック
        If loRec.Rows.Count <= 0 Then
            Call ksExpMsgBox("印刷するデータが存在しません。", "I")
            GoTo Exit_JYUMR060TotalPrintProc
        End If

        '===Excelファイルのオープン
        If steExcelOpen(psttREPORT_NAME & exXLSSuffix) = False Then
            'Excelファイル無しメッセージ
            Call ksExpMsgBox(psttREPORT_NAME & "のレポートが存在しません。", "E")
            GoTo Exit_JYUMR060TotalPrintProc
        End If

        If mIntMode = enmPrintmode.Preview Then        'プレビューモード
            Call stePrevWorkDelete()  '古いワークファイルを削除する
        End If

        '===進行状況表示フォーム設定=============================================

        '===各変数初期化
        mCurPage = 1                  'ページ番号
        mCurRows = pstStartRow - 1    '明細印刷開始行
        DCount = 0                    'データカウンタ
        curAllTotal = 0
        SCnt = 0

        'シートのクリア
        Call loSheetClear()

        For Each dr As DataRow In loRec.Rows


            '初回ヘッダー出力
            If DCount = 0 Then
                Call sttWriteHeader(NCnvN(dr("ZaiD_007")), NCnvN(dr("ZaiD_008")))
            End If

            '進行状況用カウンタ
            DCount = DCount + 1

            ''進行状況表示フォーム設定
            'With frmStProgressBar
            '    .SetPage = "ページ = " & mCurPage
            '    .SetVal = DCount
            '    'キャンセルボタンのチェック
            '    If .Cancel Then
            '        Call steWorkbooksClose()      'Bookのクローズ
            '        frmStProgressBar.ShowStop   '進行状況表示フォーム終了
            '        GoTo Exit_JYUMR060TotalPrintProc
            '    End If
            'End With

            '仕入先コードブレーク
            If SCnt > 0 And befSirCd <> NCnvN(dr("ZaiD_007")) Then
                '合計出力
                Call CheckPages()
                Call CheckPages()
                Call sttWriteTotal(1, curAllTotal)
                curAllTotal = 0
                '改ページ
                mCurRows = pstLastRow + 1
                Call CheckPages()
                mCurRows = pstStartRow - 1   '行をリセット
                'ヘッダー出力
                Call sttWriteHeader(NCnvN(dr("ZaiD_007")), NCnvN(dr("ZaiD_008")))
            End If

            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                If chkYMD = 0 Then
                    '仕入日付
                    .Cells(mCurRows, 2).Value = Format(NCnvN(dr("ZaiD_020")), "YY/MM/DD")
                Else
                    'チェック日付
                    .Cells(mCurRows, 2).Value = Format(NCnvN(dr("ZaiD_028")), "YY/MM/DD")
                End If
                '契約番号
                .Cells(mCurRows, 3).Value = NCnvN(dr("ZaiD_001"))
                '金額
                .Cells(mCurRows, 10).Value = Format(NCnvZ(dr("ZaiD_024")), "#,##0")
            End With

            '合計集計
            curAllTotal = curAllTotal + NCnvZ(dr("ZaiD_024"))

            befSirCd = NCnvN(dr("ZaiD_007"))

            SCnt = SCnt + 1

        Next

        '合計出力
        Call CheckPages()
        Call CheckPages()
        Call sttWriteTotal(1, curAllTotal)


        'モードごとの処理
        Select Case viMode

            Case enmPrintmode.Preview      'プレビュー
                Call stePrevBookSave()       'Bookの保存
                Call steWorkbooksClose()     'Bookのクローズ
                'frmStProgressBar.ShowStop  '進行状況表示フォーム終了

                'プレビュー画面の起動
                Dim frm As New SAHANBAI.frmPrintPreview
                With frm
                    .ppvActCell = pstPrevArea
                    .ShowDialog()
                End With

            Case enmPrintmode.PrintOut     '印刷
                mCurRows = pstLastRow
                Call CheckPages()
                Call steWorkbooksClose()     'Bookのクローズ
                'frmStProgressBar.ShowStop  '進行状況表示フォーム終了
        End Select


        JYUMR060TotalPrintProc = True

Exit_JYUMR060TotalPrintProc:

        Exit Function

JYUMR060TotalPrintProc_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Call steWorkbooksClose()        'Bookのクローズ
        'frmStProgressBar.ShowStop     '進行状況表示フォーム終了

        Resume Exit_JYUMR060TotalPrintProc

    End Function

    '********************************************************************
    '**  名称    : Function CreateSQLText() String
    '**  機能    : データ読み込み用SQL作成処理
    '**  戻り値  : 作成されたSQL文字列
    '**  引数    : iSel 0:明細 1:合計
    '**  備考    :
    '********************************************************************
    Private Function CreateSQLText(iSel As Integer) As String

        Dim strSQL As String
        Dim i As Integer

        If iSel = 0 Then
            '出荷データ読込(明細)
            If chkYMD = 0 Then
                strSQL = "SELECT ZaiD_007,ZaiD_008,ZaiD_020,ZaiD_001,ZaiD_002,ZaiD_003,ZaiD_005,"
                '            strSQL = strSQL & "SUM(ZaiD_024) AS ZaiD_024,SUM(ZaiD_021) AS ZaiD_021,ZaiD_026,SUM(ZaiD_032) AS ZaiD_032"                                 'DELETE 2012/12/07 AOKI
                '            strSQL = strSQL & "SUM((ZaiD_021 + ZaiD_032) * ZaiD_026) AS ZaiD_024,SUM(ZaiD_021) AS ZaiD_021,ZaiD_026,SUM(ZaiD_032) AS ZaiD_032"         'INSERT 2012/12/07 AOKI 'DELETE 2014/07/18 AOKI
                strSQL = strSQL & "SUM(ZaiD_021 * ZaiD_026) AS ZaiD_024,SUM(ZaiD_021) AS ZaiD_021,ZaiD_026,SUM(ZaiD_032) AS ZaiD_032"                       'INSERT 2014/07/18 AOKI
                strSQL = strSQL & "  FROM  ZaiD_Dat"
                strSQL = strSQL & " WHERE ZaiD_020 >='" & SirDate(0) & "'"
                strSQL = strSQL & "   AND ZaiD_020 <='" & SirDate(1) & "'"
            Else
                strSQL = "SELECT ZaiD_007,ZaiD_008,ZaiD_028,ZaiD_001,ZaiD_002,ZaiD_003,ZaiD_005,"
                '            strSQL = strSQL & "SUM(ZaiD_024) AS ZaiD_024,SUM(ZaiD_021) AS ZaiD_021,ZaiD_026,SUM(ZaiD_032) AS ZaiD_032"                                 'DELETE 2012/12/07 AOKI
                '            strSQL = strSQL & "SUM((ZaiD_021 + ZaiD_032) * ZaiD_026) AS ZaiD_024,SUM(ZaiD_021) AS ZaiD_021,ZaiD_026,SUM(ZaiD_032) AS ZaiD_032"         'INSERT 2012/12/07 AOKI 'DELETE 2014/07/18 AOKI
                strSQL = strSQL & "SUM(ZaiD_021 * ZaiD_026) AS ZaiD_024,SUM(ZaiD_021) AS ZaiD_021,ZaiD_026,SUM(ZaiD_032) AS ZaiD_032"                       'INSERT 2014/07/18 AOKI
                strSQL = strSQL & "  FROM  ZaiD_Dat"
                strSQL = strSQL & " WHERE ZaiD_028 >='" & SirDate(0) & "'"
                strSQL = strSQL & "   AND ZaiD_028 <='" & SirDate(1) & "'"
            End If
            strSQL = strSQL & "   AND ("
            '仕入先コード
            For i = LBound(SelSirCd) To UBound(SelSirCd)
                If i = LBound(SelSirCd) Then
                    strSQL = strSQL & " ZaiD_007 = " & SelSirCd(i)
                Else
                    strSQL = strSQL & " OR ZaiD_007 = " & SelSirCd(i)
                End If
            Next i
            strSQL = strSQL & ")"
            If chkYMD = 0 Then
                strSQL = strSQL & " GROUP BY ZaiD_007,ZaiD_008,ZaiD_020,ZaiD_001,ZaiD_002,ZaiD_003,ZaiD_005,ZaiD_026"
                strSQL = strSQL & " ORDER BY ZaiD_007,ZaiD_020,ZaiD_001,ZaiD_002,ZaiD_003"
            Else
                strSQL = strSQL & " GROUP BY ZaiD_007,ZaiD_008,ZaiD_028,ZaiD_001,ZaiD_002,ZaiD_003,ZaiD_005,ZaiD_026"
                strSQL = strSQL & " ORDER BY ZaiD_007,ZaiD_028,ZaiD_001,ZaiD_002,ZaiD_003"
            End If
        Else
            '出荷データ読込(合計)
            If chkYMD = 0 Then
                '            strSQL = "SELECT ZaiD_007,ZaiD_008,ZaiD_020,ZaiD_001,SUM(ZaiD_024) AS ZaiD_024,SUM(ZaiD_032) AS ZaiD_032"                                  'DELETE 2012/12/06 AOKI
                '            strSQL = "SELECT ZaiD_007,ZaiD_008,ZaiD_020,ZaiD_001,SUM((ZaiD_021 + ZaiD_032) * ZaiD_026) AS ZaiD_024,SUM(ZaiD_032) AS ZaiD_032"          'INSERT 2012/12/06 AOKI 'DELETE 2014/07/18 AOKI
                strSQL = "SELECT ZaiD_007,ZaiD_008,ZaiD_020,ZaiD_001,SUM(ZaiD_021 * ZaiD_026) AS ZaiD_024,SUM(ZaiD_032) AS ZaiD_032"                        'INSERT 2014/07/18 AOKI
                strSQL = strSQL & "  FROM  ZaiD_Dat"
                strSQL = strSQL & " WHERE ZaiD_020 >='" & SirDate(0) & "'"
                strSQL = strSQL & "   AND ZaiD_020 <='" & SirDate(1) & "'"
            Else
                '            strSQL = "SELECT ZaiD_007,ZaiD_008,ZaiD_028,ZaiD_001,SUM(ZaiD_024) AS ZaiD_024"                                                            'DELETE 2012/12/06 AOKI
                '            strSQL = "SELECT ZaiD_007,ZaiD_008,ZaiD_028,ZaiD_001,SUM((ZaiD_021 + ZaiD_032) * ZaiD_026) AS ZaiD_024"                                    'INSERT 2012/12/06 AOKI 'DELETE 2014/07/18 AOKI
                strSQL = "SELECT ZaiD_007,ZaiD_008,ZaiD_028,ZaiD_001,SUM(ZaiD_021 * ZaiD_026) AS ZaiD_024"                                                  'INSERT 2014/07/18 AOKI
                strSQL = strSQL & "  FROM  ZaiD_Dat"
                strSQL = strSQL & " WHERE ZaiD_028 >='" & SirDate(0) & "'"
                strSQL = strSQL & "   AND ZaiD_028 <='" & SirDate(1) & "'"
            End If
            strSQL = strSQL & "   AND ("
            '仕入先コード
            For i = LBound(SelSirCd) To UBound(SelSirCd)
                If i = LBound(SelSirCd) Then
                    strSQL = strSQL & " ZaiD_007 = " & SelSirCd(i)
                Else
                    strSQL = strSQL & " OR ZaiD_007 = " & SelSirCd(i)
                End If
            Next i
            strSQL = strSQL & ")"
            If chkYMD = 0 Then
                strSQL = strSQL & " GROUP BY ZaiD_007,ZaiD_008,ZaiD_020,ZaiD_001"
                strSQL = strSQL & " ORDER BY ZaiD_007,ZaiD_008,ZaiD_020,ZaiD_001"
            Else
                strSQL = strSQL & " GROUP BY ZaiD_007,ZaiD_008,ZaiD_028,ZaiD_001"
                strSQL = strSQL & " ORDER BY ZaiD_007,ZaiD_008,ZaiD_028,ZaiD_001"
            End If
        End If

        CreateSQLText = strSQL

    End Function
    '***************************************************************
    '**  名称    : Function GetTargetSheet() As Long
    '**  機能    : 出力モードとカレントページから対象シート番号を返す。
    '**  戻り値  : 対象となるExcelシート番号
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function GetTargetSheet() As Long

        'モードによりシート番号を設定
        Select Case mIntMode
            Case 0  'プレビュー
                GetTargetSheet = mCurPage
            Case 1  '印刷
                GetTargetSheet = 1
        End Select

    End Function
    '***************************************************************
    '**  名称    : Sub CheckPages
    '**  機能    : 出力ページと行のチェックと改頁処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub CheckPages()

        '行をインクリメント
        mCurRows = mCurRows + 1

        '行チェック
        If mCurRows > pstLastRow Then
            mCurPage = mCurPage + 1     'ページインクリメント
            '改頁処理
            Select Case mIntMode
                Case 0  'プレビュー
                    Call stePrevSheetCopy(mCurPage)              'プレビュー・印刷用Bookのコピー
                    Call loSheetClear()                            'Excelシートクリア
                Case 1  '印刷
                    Call steExcelPrint(1, ActivePrinter.Printer) 'Excel 印刷
                    Call steSheetClear(1, pstPrintArea)          'Excelシートクリア
            End Select

            '''        mCurRows = pstStartRow - 1   '行をリセット
            mCurRows = pstStartRow   '行をリセット
        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub loSheetClear
    '**  機能    : シートのクリア
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub loSheetClear()

        Call steSheetClear(GetTargetSheet, pstPrintArea)

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteHeader
    '**  機能    : ヘッダー情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeader(vSirCd As String, vSirNm As String)

        Dim strWk As String

        With xlApp.Worksheets(GetTargetSheet)
            .Cells(1, 13).Value = "PAGE - " & mCurPage
            .Cells(2, 13).Value = "作成日 :" & Format(Date.Now, "YYYY年MM年DD日")
            '年月日
            If chkYMD = 0 Then
                strWk = "仕入日付範囲："
                .Cells(7, 2).Value = "仕入日付"
            Else
                strWk = "チェック日付範囲："
                .Cells(7, 2).Value = "ﾁｪｯｸ日付"
            End If
            strWk = strWk & Format(CDate(SirDate(0)), "YYYY年MM年DD日") & "～"
            strWk = strWk & Format(CDate(SirDate(1)), "YYYY年MM年DD日")
            .Cells(4, 1).Value = strWk
            '仕入先情報
            .Cells(5, 1).Value = "仕入先　情報：" & vSirCd & Space(1) & vSirNm
        End With

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteTotal
    '**  機能    : 合計をExcelに出力する。
    '**  戻り値  :
    '**  引数    : viMode 0:小計 1:合計　curSubTotal：金額
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteTotal(viMode As Integer, vcKingaku As Double)

        With xlApp.Worksheets(GetTargetSheet)
            If viMode = 0 Then
                .Cells(mCurRows, 7).Value = "＊＊　小計　＊＊"
            Else
                .Cells(mCurRows, 7).Value = "＊＊　合計　＊＊"
            End If
            .Cells(mCurRows, 10).Value = Format(vcKingaku, "#,##0")
        End With

    End Sub


#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = winApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    winApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk + gstStrSEPFILE +
                     winApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next

            If xlApp IsNot Nothing Then
                xlApp.Quit()
                xlApp = Nothing
            End If
        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub

    Private Sub steSheetClear(ByVal vlSheet As Long, ByVal vsCell As String)
        xlApp.Worksheets(vlSheet).Range(vsCell).ClearContents()
    End Sub
#End Region

End Module
