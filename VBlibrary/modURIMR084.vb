﻿Imports System.Windows.Forms
Imports globalcls.modHanbai
Imports clswinApi
Imports SAHANBAI.clsGlobal
'Imports Constant = SAHANBAI.Constant
Imports winApi = clswinApi.StaticWinApi
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports SAHANBAI.TaxComp
Imports System.Drawing
''all database transaction were put here , all control related
Public Module modURIMR084

    Dim xlApp As Excel.Application
    Dim xlWs As Excel.Worksheet
    Dim xlRange As Excel.Range

    Dim DB As clsDB.DBService

    Public ConMst(0 To 0) As ConMstInfo     'コントロールマスタ情報
    Public TokMst(0 To 0) As TokMstInfo     '得意先マスタ

    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
    End Enum

    'オーダー状況印刷条件
    Public pTCode As Long     '得意先コード
    Public pTName As String   '得意先名称
    Public pVessel As String   '船名
    Public pSelect As Integer  '抽出区分
    Public BVessel As String   '船名(ブレークキー用)

    'オーダー状況ヘッダ項目
    Private Structure URIMR084Hed
        Public TName As String    '得意先名
    End Structure
    Private printHed(0 To 0) As URIMR084Hed

    '明細印字項目
    Private Structure URIMR084Meisai
        Public Vessel As String   '船名
        Public TokNm As String   '得意先名   'INSERT 2015/12/14 AOKI
        Public KDate As String   '契約日
        Public RefNo As String   '依頼№
        Public EstNo As String   '見積№
        Public OrdNo As String   '契約№
        Public TyuNo As String   '注文№
        Public Ready As String   '"RERDY","NOT READY","DELIVERED"
        Public SyuNo As Integer  '出荷№
        Public CaseNo As Integer  'ｹｰｽ№
        Public Weight As String   '重さ、サイズ等
        Public Remark As String   '梱包数等
        Public Remark2 As String   '同時梱包時の他の見積№
        Public WeightF As Integer  '重量印字フラグ
        Public SDate As String   '出荷日
        Public strBold As String   '最新の梱包日
        Public HSuryo As Integer  '発注数   
        Public SSuryo As Integer  '仕入数   
        Public NDate As String   '入荷予定日
        Public Size As String   'サイズ     
        Public Syuka As String   '出荷予定先
        Public Okiba As String   '置き場所    
    End Structure
    Private mMeisai() As URIMR084Meisai  '

    '印刷共通定義
    Private mCurRows As Long        'カレント行
    Public frmURIMR084_cboTanto As ComboBox
    Public frmURIMR084_chkToday As CheckBox
    Public frmURIMR084_datDateRange_1 As DateTimePicker
    Public frmURIMR084_datDateRange_0 As DateTimePicker
    Public frmURIMR084_spdMeisai As DataGridView

    '***************************************************************
    '**  名称    : Sub main()
    '**  機能    : オーダー状況発行メイン
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************


    '***************************************************************
    '**  名称    : Function stmSetFunction() As Boolean
    '**  機能    : ファンクションキーの使用不可を設定する。
    '**  戻り値  :
    '**  引数    : vcForm ; 設定フォーム
    '**  備考    :


    '***************************************************************
    '**  名称    : Function URIMR084PrintProc() As Boolean
    '**  機能    : 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function URIMR084PrintProc() As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim RecMeisai As New DataTable  '見積明細データ
        Dim PrFlg As Boolean              'True:印刷あり

        On Error GoTo URIMR084PrintProc_Err

        URIMR084PrintProc = False

        RecMeisai = Nothing

        '===受注データの読込み用SQL文作成ルーチン
        strSQL = CreateSQLText()

        '===受注データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        RecMeisai = DB.GetDatafromDB(strSQL)

        '===データ存在チェック
        If RecMeisai.Rows.Count <= 0 Then
            'データ無しメッセージ
            Call ksExpMsgBox("表示するデータが存在しません。", "I")
            GoTo Exit_URIMR084PrintProc
        End If

        '===各変数初期化
        DCount = 0                    'データカウンタ
        PrFlg = False
        BVessel = ""
        mCurRows = 0





        For Each dr As DataRow In RecMeisai.Rows
            DCount = DCount + 1


            '明細情報セット
            If sttSetMeisai(dr) = True Then
                If mMeisai(0).Vessel = "" Then
                    MsgBox(NCnvN(dr("JyuH_001")))
                End If
                '明細印刷データ存在したら
                Call sttWriteMeisai()        '明細情報出力
            End If

            Erase mMeisai


        Next




        'クローズ

        URIMR084PrintProc = True

Exit_URIMR084PrintProc:

        RecMeisai = Nothing
        Exit Function

URIMR084PrintProc_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")

        Resume Exit_URIMR084PrintProc

    End Function

    '********************************************************************
    '**  名称    : Function CreateSQLText() String
    '**  機能    : 見積データ読み込み用SQL作成処理
    '**  戻り値  : 作成されたSQL文字列
    '**  引数    :
    '**  備考    :
    '********************************************************************
    Private Function CreateSQLText() As String

        Dim strSQL As String

        '受注データ読込
        strSQL = "SELECT * FROM JyuH_Dat "

        strSQL = strSQL & " LEFT JOIN SeiD_Dat "
        strSQL = strSQL & "   ON JyuH_001 = SeiD_003 "
        If frmURIMR084_cboTanto.Text <> "" Then
            strSQL = strSQL & "INNER JOIN (SELECT * FROM Tok_Mst WHERE Tok_051 = '" & stComboGetKomoku(frmURIMR084_cboTanto.Text) & "') AS TM "
            strSQL = strSQL & "ON JyuH_0031 = TM.Tok_001 "
        End If
        If frmURIMR084_chkToday.Checked = True Then
            strSQL = strSQL & "INNER JOIN (SELECT  KnpD_001 FROM KnpD_Dat WHERE KnpD_015 BETWEEN '" & frmURIMR084_datDateRange_0.Value.ToString("yyyy/MM/dd") & "' AND '" &
                                                                                                      frmURIMR084_datDateRange_1.Value.ToString("yyyy/MM/dd") & "'" &
                                                                         "GROUP BY KnpD_001) AS KD "
            strSQL = strSQL & "ON JyuH_001 = KD.KnpD_001 "
        End If

        strSQL = strSQL & " WHERE SeiD_003 IS NULL "

        If pTCode <> 0 Then
            strSQL = strSQL & " AND JyuH_0031 = " & pTCode                  '得意先コード
        Else
            If pTName <> "" Then
                strSQL = strSQL & " AND JyuH_003 Like '%" & pTName & "%'"   '得意先名称
            End If
        End If

        If pVessel <> "" Then
            strSQL = strSQL & " AND JyuH_006 Like '%" & pVessel & "%'"  '船名
        End If
        '得意先名、船名、契約日、契約№
        strSQL = strSQL & " ORDER BY JyuH_003,JyuH_006,JyuH_002,JyuH_001"

        CreateSQLText = strSQL

    End Function

    'Private Function Format(value As Object, v As String) As String
    '    Throw New NotImplementedException()
    'End Function

    '***************************************************************
    '**  名称    : Sub sttSetMeisai
    '**  機能    : 明細データのセット
    '**  戻り値  : 1:納品書未出力あり -1:納品書未出力なし
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function sttSetMeisai(voRec As DataRow) As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim BrCaseNo As Integer
        Dim lsRemark As String
        Dim WeightF As Integer

        On Error GoTo sttSetMeisai_Err

        sttSetMeisai = False

        Erase mMeisai
        i = -1

        '-- READYデータ(梱包されていて、出荷日が入力されていないデータ)を検索する
        strSQL = "SELECT * ,JyuD_026 FROM KnpD_Dat,JyuD_Dat "
        strSQL = strSQL & " WHERE KnpD_001 ='" & NCnvN(voRec("JyuH_001")) & "'"
        strSQL = strSQL & "   AND KnpD_001 = JyuD_001"
        strSQL = strSQL & "   AND KnpD_002 = JyuD_003"
        strSQL = strSQL & "   AND KnpD_003 = JyuD_004"
        strSQL = strSQL & "   AND RIGHT(KnpD_003,1) <> 0"                         '例外行
        strSQL = strSQL & "   AND ISNULL(KnpD_010,'') <> ''"                      '単位が入っているアイテム
        strSQL = strSQL & "   AND (KnpD_018 IS NULL OR KnpD_018 = '1900/01/01' )" '出荷日が入っていないデータ
        strSQL = strSQL & " ORDER BY KnpD_006,KnpD_003"

        '読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        'データが存在しない時は、NOT READYデータを検索しにいく
        If loRec.Rows.Count <= 0 Then
            loRec = Nothing
            GoTo SKIP
        End If

        'ケース№取得
        For Each dr As DataRow In loRec.Rows
            If BrCaseNo <> NCnvZ(dr("KnpD_006")) Then
                i = i + 1
                ReDim Preserve mMeisai(i)
                BrCaseNo = NCnvZ(dr("KnpD_006"))
                mMeisai(i).Remark = "ITEM "
                Call BoldProc(i, voRec)
            End If

            With mMeisai(i)
                .Vessel = NCnvN(voRec("JyuH_006"))           '船名
                .TokNm = NCnvN(voRec("JyuH_003"))            '得意先名       'INSERT 2015/12/14 AOKI

                .KDate = CDate(NCnvN(voRec("JyuH_002"))).ToString("dd/MMM/yy")          '契約日
                .RefNo = NCnvN(voRec("JyuH_005"))            '依頼№
                .OrdNo = NCnvN(voRec("JyuH_001"))            '契約№
                .EstNo = NCnvN(voRec("JyuH_009"))            '見積№
                .TyuNo = NCnvN(voRec("JyuH_017"))            '注文№
                .SyuNo = NCnvZ(dr("KnpD_005"))            '出荷№
                .CaseNo = NCnvZ(dr("KnpD_006"))           'ケース№

                .Ready = "READY"
                .Weight = NCnvN(dr("KnpD_013")) & "kg"    '重量

                If NCnvZ(dr("KnpD_021")) > 0 Then       'サイズ
                    .Size = NCnvZ(dr("KnpD_021")) & " x " &
                            NCnvZ(dr("KnpD_022")) & " x " &
                            NCnvZ(dr("KnpD_023")) & " cm "
                End If

                .Syuka = NCnvN(voRec("JyuH_020"))            '出荷予定先（受渡場所）
                .Okiba = NCnvN(dr("KnpD_014"))            '置き場所
                '--- INSERT 2014/02/05 AOKI E N D -------------------------------------------------------------------

                lsRemark = Left(NCnvZ(dr("KnpD_003")), Len(NCnvZ(dr("KnpD_003"))) - 1)
                If NCnvZ(dr("KnpD_009")) <> NCnvZ(dr("JyuD_022")) Then
                    lsRemark = lsRemark & "(" & NCnvZ(dr("KnpD_009")) & "/" & NCnvZ(dr("JyuD_022")) & ")"
                End If
                lsRemark = lsRemark & ","
                .Remark = .Remark & lsRemark

                .Remark2 = GetRemark2(.SyuNo, .CaseNo, NCnvN(voRec("JyuH_001")), WeightF) '同時梱包時の他見積№取得
                .WeightF = WeightF                                               '重量印字フラグ

            End With
        Next


        sttSetMeisai = True


        loRec = Nothing

SKIP:

        strSQL = "Select * FROM JyuD_Dat "
        strSQL = strSQL & " WHERE JyuD_001 ='" & NCnvN(voRec("JyuH_001")) & "'"
        strSQL = strSQL & "   AND RIGHT(JyuD_004,1) <> 0"                     '例外行
        strSQL = strSQL & "   AND ISNULL(JyuD_019,'') <> ''"                  '単位が入っているデータ
        strSQL = strSQL & "   AND ISNULL(JyuD_0274,0) <> -1"                  '出荷完了されていない
        strSQL = strSQL & "   AND ( ISNULL(JyuD_037,0) = 0 OR ( ISNULL(JyuD_037,0) > 0 AND ISNULL(JyuD_022,0) <> ISNULL(JyuD_037,0) ) )"  '梱包されているが発注数≠梱包数のデータ
        strSQL = strSQL & " ORDER BY JyuD_004"

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        'データが存在しない時は、NOT READYデータを検索しにいく
        If loRec.Rows.Count <= 0 Then
            loRec = Nothing
            GoTo Exit_sttSetMeisai
        End If

        i = i + 1
      
        ReDim Preserve mMeisai(i)
        With mMeisai(i)
            .Vessel = NCnvN(voRec("JyuH_006"))       '船名
            .TokNm = NCnvN(voRec("JyuH_003"))        '得意先名           'INSERT 2015/12/14 AOKI
            .KDate = CDate(NCnvN(voRec("JyuH_002"))).ToString("dd/MMM/yy")        '契約日
            .RefNo = NCnvN(voRec("JyuH_005"))        '依頼№
            .OrdNo = NCnvN(voRec("JyuH_001"))        '契約№
            .EstNo = NCnvN(voRec("JyuH_009"))        '見積№
            .TyuNo = NCnvN(voRec("JyuH_017"))        '注文№
            '.Ready = "Not READY"
            If NCnvN(voRec("JyuH_021")) = "1900/01/01" Or NCnvN(voRec("JyuH_021")) = "" Then
                .Ready = "Not READY"
            Else
                .Ready = CDate(NCnvN(voRec("JyuH_021"))).ToString("dd/MMM/yy")         '契約日
            End If

            .Syuka = NCnvN(voRec("JyuH_020"))            '出荷予定先（受渡場所）     'INSERT 2014/02/21 AOKI
            .Remark = "ITEM: "
            .NDate = NCnvN(voRec("JyuH_018"))
        End With

        For Each dr As DataRow In loRec.Rows
            lsRemark = Left(NCnvZ(dr("JyuD_004")), Len(NCnvZ(dr("JyuD_004"))) - 1)
            If NCnvZ(dr("JyuD_037")) > 0 Then
                lsRemark = lsRemark & "(" & NCnvZ(dr("JyuD_022")) - NCnvZ(dr("JyuD_037")) & "/" & NCnvZ(dr("JyuD_022")) & ")"
            End If
            lsRemark = lsRemark & ","

            mMeisai(i).Remark = mMeisai(i).Remark & lsRemark

            mMeisai(i).HSuryo = mMeisai(i).HSuryo + NCnvZ(dr("JyuD_022"))     'INSERT 2013/03/26 AOKI
            mMeisai(i).SSuryo = mMeisai(i).SSuryo + NCnvZ(dr("JyuD_024"))     'INSERT 2013/03/26 AOKI

        Next



        loRec = Nothing

        sttSetMeisai = True

Exit_sttSetMeisai:

        Exit Function

sttSetMeisai_Err:
        On Error GoTo 0

    End Function

    '--- INSERT 2012/03/02 梱包日より2ヵ月 青木 START --------------------------------------------------------------
    '***************************************************************
    '**  名称    : Sub BoldProc()
    '**  機能    : 全てREADYのデータから最新梱包日を取得する
    '**            出荷済みのデータは対象外
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub BoldProc(i As Integer, voRec As Object)

        On Error GoTo BoldProc_ERR

        Dim strSQL As String
        Dim loRec2 As New DataTable

        'SQL作成
        strSQL = "          SELECT KD.KD015 "
        strSQL = strSQL & " FROM   ("
        strSQL = strSQL & "         SELECT   JyuD_001 "
        strSQL = strSQL & "              , SUM(JyuD_018) As JD018 "
        strSQL = strSQL & "              , SUM(JyuD_037) AS JD037 "
        strSQL = strSQL & "         FROM     JyuD_Dat "
        strSQL = strSQL & "         WHERE    JyuD_001 = '" & NCnvN(voRec![JyuH_001]) & "'"
        strSQL = strSQL & "         GROUP BY JyuD_001 "
        strSQL = strSQL & "         HAVING   SUM(JyuD_018) = SUM(JyuD_037) "
        strSQL = strSQL & "        ) AS JD "
        strSQL = strSQL & " INNER JOIN "
        strSQL = strSQL & "        ( "
        strSQL = strSQL & "         SELECT   KnpD_001 "
        strSQL = strSQL & "              ,   MAX(KnpD_015) AS KD015 "
        strSQL = strSQL & "         FROM     KnpD_Dat "
        strSQL = strSQL & "         WHERE    KnpD_001 = '" & NCnvN(voRec![JyuH_001]) & "'"
        strSQL = strSQL & "         AND      (KnpD_018 IS NULL OR KnpD_018 = '1900/01/01') "
        strSQL = strSQL & "         GROUP BY KnpD_001 "
        strSQL = strSQL & "        ) AS KD "
        strSQL = strSQL & " ON     JD.JyuD_001 = KD.KnpD_001 "

        '読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRec2 = DB.GetDatafromDB(strSQL)

        If loRec2.Rows.Count > 0 Then
            mMeisai(i).strBold = loRec2.Rows(0)("KD015")
        End If


        loRec2 = Nothing

        Exit Sub


BoldProc_ERR:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")

    End Sub
    '--- INSERT 2012/03/02 梱包日より2ヵ月 青木 E N D --------------------------------------------------------------

    '***************************************************************
    '**  名称    : Sub sttWriteMeisai
    '**  機能    : 明細情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    : voRec:見積明細データ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMeisai()

        Dim i As Integer
        Dim j As Integer
        Dim vKbn As Integer
        Dim lsWork As String
        Dim rRows As Integer

        vKbn = 0

        Try


            'スプレッド明細情報のクリア
            With frmURIMR084_spdMeisai

                For i = LBound(mMeisai) To UBound(mMeisai)
                    .Rows.Add()
                    If BVessel <> mMeisai(i).Vessel Then

                        .Rows(mCurRows).Cells(1 - 1).Value = NCnvN(mMeisai(i).Vessel)
                        .Rows(mCurRows).Cells(6 - 1).Value = NCnvN(mMeisai(i).TokNm)

                        BVessel = mMeisai(i).Vessel
                        mCurRows = mCurRows + 1
                    End If

                    If vKbn = 0 Then
                        .Rows.Add()
                        .Rows(mCurRows).Cells(2 - 1).Value = NCnvN(mMeisai(i).KDate) '契約日
                        .Rows(mCurRows).Cells(3 - 1).Value = NCnvN(mMeisai(i).EstNo) '見積№
                        .Rows(mCurRows).Cells(4 - 1).Value = NCnvN(mMeisai(i).OrdNo) '契約№
                        .Rows(mCurRows).Cells(5 - 1).Value = NCnvN(mMeisai(i).RefNo) '依頼№
                        .Rows(mCurRows).Cells(6 - 1).Value = NCnvN(mMeisai(i).TyuNo) '注文№
                        vKbn = 1
                    End If

                    .Rows(mCurRows).Cells(7 - 1).Value = NCnvN(mMeisai(i).Ready)  'Ready

                    If NCnvN(mMeisai(i).CaseNo) <> "" Then
                        .Rows(mCurRows).Cells(9 - 1).Value = NCnvN(mMeisai(i).CaseNo)     'Case No.
                    End If

                    If NCnvZ(mMeisai(i).WeightF) = 1 Then
                        .Rows(mCurRows).Cells(10 - 1).Value = NCnvN(mMeisai(i).Weight) '重量  .
                        .Rows(mCurRows).Cells(11 - 1).Value = NCnvN(mMeisai(i).Size)   'サイズ
                    End If

                    .Rows(mCurRows).Cells(12 - 1).Value = NCnvN(mMeisai(i).Syuka)  '出荷予定先  
                    .Rows(mCurRows).Cells(13 - 1).Value = NCnvN(mMeisai(i).Okiba)   '置き場所  

                    '最後のカンマを外す
                    If Right(mMeisai(i).Remark, 1) = "," Then
                        mMeisai(i).Remark = Left(mMeisai(i).Remark, Len(mMeisai(i).Remark) - 1)
                    End If

                    rRows = mCurRows
                    If Len(mMeisai(i).Remark) <= 30 Then
                        .Rows(mCurRows).Cells(14 - 1).Value = mMeisai(i).Remark
                    Else
                        lsWork = NCnvN(mMeisai(i).Remark)

                        For j = 30 To 1 Step -1
                            If Right(Mid(lsWork, 1, j), 1) = "," Then
                                .Rows(mCurRows).Cells(14 - 1).Value = Left(lsWork, j)
                                lsWork = Mid(lsWork, j + 1, Len(lsWork) - j)
                                Exit For
                            End If
                        Next j

                        Do Until lsWork = ""
                            mCurRows = mCurRows + 1
                            If Len(lsWork) <= 25 Then
                                .Rows(mCurRows).Cells(14 - 1).Value = Space(11) & lsWork
                                lsWork = ""
                            Else
                                For j = 25 To 1 Step -1
                                    If Right(Mid(lsWork, 1, j), 1) = "," Then
                                        .Rows(mCurRows).Cells(14 - 1).Value = Space(11) & Left(lsWork, j)
                                        lsWork = Mid(lsWork, j + 1, Len(lsWork) - j)
                                        Exit For
                                    End If
                                Next j
                            End If
                        Loop
                    End If

                    If NCnvN(mMeisai(i).Remark2) <> "" Then
                        mCurRows = mCurRows + 1
                        .Rows(mCurRows).Cells(14 - 1).Value = NCnvN(mMeisai(i).Remark2)  'アイテム情報     
                    End If
                    If mMeisai(i).Ready <> "READY" Then
                        '.Row = rRows
                        '.Row2 = mCurRows
                        '.Col = -1
                        '.Col2 = -1
                        '.BlockMode = True
                        If (mMeisai(i).SSuryo = 0 Or mMeisai(i).HSuryo <> mMeisai(i).SSuryo) And
                           (DateAdd("d", 1, Date.Now) >= mMeisai(i).NDate) Then
                            .Rows(mCurRows).DefaultCellStyle.ForeColor = Color.FromArgb(128, 0, 128)
                            '.Rows(rRows).DefaultCellStyle.ForeColor = Color.FromArgb(128, 0, 128)
                            '.Rows(mCurRows).DefaultCellStyle.ForeColor = Color.FromArgb(128, 0, 128)
                        Else
                            .Rows(mCurRows).DefaultCellStyle.ForeColor = Color.FromArgb(255, 0, 0)
                            '.Rows(rRows).DefaultCellStyle.ForeColor = Color.FromArgb(255, 0, 0)
                            '.Rows(mCurRows).DefaultCellStyle.ForeColor = Color.FromArgb(255, 0, 0)
                        End If
                        '--- UPDATE 2013/03/26 AOKI E N D --------------------------------------------------------------

                        .BackColor = Color.FromArgb(255, 255, 255)

                    End If

                    If mMeisai(i).strBold <> "" Or Not mMeisai(i).strBold Is Nothing Then
                        If CDate(mMeisai(i).strBold) <= Date.Now.AddMonths(-2) Then
                            '.Row = rRows
                            '.Row2 = mCurRows
                            '.Col = -1
                            '.Col2 = -1
                            '.BlockMode = True
                            ''セルのテキスト色、背景色を設定します。
                            '.ForeColor = RGB(0, 0, 255)
                            '.BlockMode = False

                            .Rows(mCurRows).DefaultCellStyle.ForeColor = Color.FromArgb(0, 0, 255)
                        End If
                    End If

                    mCurRows = mCurRows + 1

                Next

            End With
        Catch ex As Exception
            modSac_Com.ksExpMsgBox(ex.Message, "E")
        End Try

    End Sub

    '***************************************************************
    '**  名称    : Sub mMeisaiUBound()
    '**  機能    : 明細データ配列の要素数を返す
    '**  戻り値  : 配列数 -1は配列無し
    '**  引数    : 対象動的配列
    '**  備考    :
    '***************************************************************
    '    Public Function mMeisaiUBound(strAr() As URIMR084Meisai) As Long

    '        On Error GoTo mMeisaiUBound_ERR

    '        mMeisaiUBound = UBound(strAr)

    '        Exit Function


    'mMeisaiUBound_ERR:

    '        If Err.Number = 9 Then
    '            mMeisaiUBound = -1
    '        End If

    '    End Function

    '***************************************************************
    '**  名称    : Sub GetRemark2()
    '**  機能    : 同時に梱包されている他の契約№の取得
    '**  戻り値  :
    '**  引数    : viSyuNo     : 出荷№
    '**          : viCaseNo    : ケース№
    '**          : vsKeiyakuNo : 契約№
    '**          : viWeight    : 重量印字フラグ
    '**  備考    :
    '***************************************************************
    Private Function GetRemark2(viSyuNo As Integer, viCaseNo As Integer,
                                vsKeiyakuNo As String,
                                viWeight As Integer) As String

        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim lsWork As String = ""

        On Error GoTo Err_GetRemark2

        viWeight = 1

        If viCaseNo > 0 Then

            SQLtxt = "SELECT JyuD_001,JyuD_002 FROM KnpD_Dat,JyuD_Dat "
            SQLtxt = SQLtxt & " WHERE KnpD_001 <> '" & NCnvN(vsKeiyakuNo) & "'"
            SQLtxt = SQLtxt & "   AND KnpD_001 = JyuD_001"
            SQLtxt = SQLtxt & "   AND KnpD_002 = JyuD_003"
            SQLtxt = SQLtxt & "   AND KnpD_003 = JyuD_004"
            SQLtxt = SQLtxt & "   AND KnpD_005 = " & viSyuNo
            SQLtxt = SQLtxt & "   AND KnpD_006 = " & viCaseNo
            SQLtxt = SQLtxt & " GROUP BY JyuD_002,JyuD_001"
            SQLtxt = SQLtxt & " ORDER BY JyuD_002,JyuD_001"

            '受注データの読込み
            If DB Is Nothing Then DB = New clsDB.DBService
            loRSet = DB.GetDatafromDB(SQLtxt)

            If loRSet.Rows.Count <= 0 Then
                GoTo Exit_GetRemark2
            End If

            'データ設定
            For Each dr As DataRow In loRSet.Rows

                If vsKeiyakuNo >= NCnvN(dr("JyuD_001")) Then
                    viWeight = 0
                End If
                lsWork = lsWork & NCnvN(dr("JyuD_002")) & ", "

            Next

            If lsWork <> "" Then
                GetRemark2 = "Same Carton As " & Left(lsWork, Len(lsWork) - 1)
            End If



        End If

Exit_GetRemark2:
        loRSet = Nothing
        Exit Function

Err_GetRemark2:

        Call ksExpMsgBox(Err.Number & " :  " & Err.Description, "E")

    End Function

    '2013/03/19 ADD START OGAWA
    '***************************************************************
    '**  名称    : Sub GetSirSuryo()
    '**  機能    : 契約№の仕入数量・入荷予定日を取得
    '**  戻り値  :
    '**  引数    : viRow        : 行
    '**  引数    : vsKeiyakuNo  : 契約№
    '**  備考    : 入荷予定日の１日前で仕入がなかったら文字色を変更
    '***************************************************************
    Private Sub GetSirSuryo(viRow As Long, vsKeiyakuNo As String)

        Dim strSQL As String
        Dim loRSet As New DataTable
        Dim loRSet2 As New DataTable
        Dim SSuryoT As Double             '仕入数量

        On Error GoTo Err_GetSirSuryo

        '受注/発注データ読込
        strSQL = "SELECT * FROM JyuH_DAT,JyuM_Dat,JyuD_Dat"
        strSQL = strSQL & " WHERE JyuH_001 = '" & vsKeiyakuNo & "'"  '契約№
        strSQL = strSQL & "   AND JyuH_001 = JyuM_001"                          '契約№
        strSQL = strSQL & "   AND JyuM_001 = JyuD_001"                          '契約№
        strSQL = strSQL & "   AND JyuM_003 = JyuD_003"                          'SEQ
        strSQL = strSQL & " ORDER BY JyuM_003,JyuD_004"

        'データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(strSQL)

        If loRSet.Rows.Count <= 0 Then
            GoTo Exit_GetSirSuryo
        End If

        '入荷予定日
        If IsDate(NCnvN(loRSet.Rows(0)("JyuH_018"))) Then
            'システム日付が入荷予定日の１日前だったら仕入数量取得へ
            If Date.Now.AddDays(1) <> CDate(NCnvN(loRSet.Rows(0)("JyuH_018"))) Then
                GoTo Exit_GetSirSuryo
            End If
        Else
            GoTo Exit_GetSirSuryo
        End If

        '読込んだデータが無くなるまで繰返す
        For Each dr As DataRow In loRSet.Rows
            If Right(dr("JyuD_004"), 1) = "0" Then   '項目
            Else
                '通常行
                '発注/仕入データ取得
                '            strSQL = "Select * FROM HatD_Dat"
                strSQL = "Select SUM(HatD_017) AS HatD_017 "
                strSQL = strSQL & " FROM HatD_Dat"
                strSQL = strSQL & " WHERE HatD_001 ='" & vsKeiyakuNo & "'"             '契約№
                strSQL = strSQL & "   AND HatD_002 = " & NCnvZ(dr("JyuD_003"))      'SEQ
                strSQL = strSQL & "   AND HatD_003 = " & NCnvZ(dr("JyuD_004"))      '項目

                'データの読込み
                If DB Is Nothing Then DB = New clsDB.DBService
                loRSet2 = DB.GetDatafromDB(strSQL)
                If loRSet2.Rows.Count <= 0 Then
                    SSuryoT = SSuryoT + NCnvZ(loRSet2.Rows(0)("HatD_017"))   '仕入数量合計
                End If
                loRSet2 = Nothing
            End If

        Next


        '仕入数量が０だったら文字色変更
        If SSuryoT = 0 Then
            '行を選択します。
            With frmURIMR084_spdMeisai
                '.Row = viRow
                '.Row2 = viRow
                '.Col = -1
                '.Col2 = -1
                '.BlockMode = True
                ''セルのテキスト色、背景色を設定します。
                '.ForeColor = RGB(128, 0, 128)
                '.BlockMode = False
                .AlternatingRowsDefaultCellStyle.ForeColor = Color.FromArgb(128, 0, 128)
            End With
        End If


Exit_GetSirSuryo:
        loRSet = Nothing
        loRSet2 = Nothing
        Exit Sub

Err_GetSirSuryo:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")

    End Sub
#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = winApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    winApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +gstStrSEPFILE +
                     winApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next

            If xlApp IsNot Nothing Then
                xlApp.Quit()
                xlApp = Nothing
            End If
        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub

    Private Sub steSheetClear(ByVal vlSheet As Long, ByVal vsCell As String)
        xlApp.Worksheets(vlSheet).Range(vsCell).ClearContents()
    End Sub
#End Region
End Module
