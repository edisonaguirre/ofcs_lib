﻿
Imports System.IO
Imports System.Windows.Forms
Imports SAHANBAI
Imports Excel = Microsoft.Office.Interop.Excel
Public Module modChumonPrint


    Dim xlApp As Excel.Application
    Dim xlWs As Excel.Worksheet
    Dim xlRange As Excel.Range


    Dim DB As clsDB.DBService

    '***************************************************************
    '**
    '**  プログラムＩＤ :  ChumonPrint
    '**  機能           :  注文書作成(modChumonPrint)
    '**  備考           :
    '**
    '***************************************************************
    'Excel関連定数定義 ==
    Private Const psttREPORT_NAME As String = "JYUMR041_注文書"        'Report名称
    'Private Const pstPrintArea     As String = "A3:G106"               'シートクリア範囲           'DELETE 2016/04/01 AOKI
    Private Const pstPrintArea As String = "A3:G314"                'シートクリア範囲           'INSERT 2016/04/01 AOKI
    Private Const pstPrevArea As String = "A1:J107"                'プレビュー表示範囲
    Private Const pstMaxRow As Integer = 38                      '最大行数
    'Private Const pstStartRow      As Integer = 6                       'ページ内の開始行          'DELETE 2017/04/05 AOKI
    Private Const pstStartRow As Integer = 7                       'ページ内の開始行           'INSERT 2017/04/05 AOKI

    'Excel関連変数定義
    '''Private pstLastRow      As Integer                                  '最終行
    Private mCurMode As Integer                                  'カレント印刷モード
    Private mCurPage As Long                                     'カレントページ番号
    Private mCurRows As Long                                     'カレント行
    '''Private mCurCols        As Long                                     'カレント列
    '''Private PageWk(1)       As Integer                                  'ページ印字用ワーク(シートのFROM-TO)
    '''Private TotalKingaku    As Double                                 '金額合計

    '明細データ
    Private RecMeisai As New DataTable                    'レコードセット

    'ブレイク
    Private brkShiCd As Double                                 '仕入先コードブレイク
    Private brkSEQ As Integer                                  'SEQブレイク

    '項目変数
    '''Public intWk            As Integer                                  '項目
    '''Private MidHedCnt       As Integer                                  '中ヘッダ出力カウント

    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
    End Enum

    Public TotalGaku As Double                 'INSERT 2018/02/28 AOKI
    Public modChumonPrint_txtKeiyakuNo As TextBox



    '***************************************************************
    '**  名称    : Sub ChumonPrintProc()
    '**  機能    : 注文書を作成
    '**  戻り値  :
    '**  引数    :
    '**  備考    : 明細行のみプレビュー出力。
    '**　　　　　　そのまま送付する帳票ではないため、
    '**　　　　　　ヘッダ、フッタ等がありません。
    '**　　　　　　今後を考えて一応コメントアウトとしておく。
    '***************************************************************
    Public Function ChumonPrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim intNo As Integer                                  'INSERT 2014/12/03 AOKI

        On Error GoTo ChumonPrintProc_Err

        ChumonPrintProc = False

        mCurMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存

        'データの読込み
        RecMeisai = Nothing
        strSQL = CreateSQLText()   'SQL作成

        If DB Is Nothing Then DB = New clsDB.DBService
        RecMeisai = DB.GetDatafromDB(strSQL)

        If RecMeisai.Rows.Count <= 0 Then
            Call ksExpMsgBox("印刷するデータが存在しません。", "I")
            GoTo Exit_ChumonPrintProc
        End If

        If (xlApp Is Nothing) Then xlApp = CreateObject("Excel.Application")

        '===Excelファイルのオープン
        If steExcelOpen(psttREPORT_NAME & modHanbai.exXLSSuffix) = False Then
            'Excelファイル無しメッセージ
            Call ksExpMsgBox(psttREPORT_NAME & "のレポートが存在しません。", "E")
            GoTo Exit_ChumonPrintProc
        End If

        If mCurMode = enmPrintmode.Preview Then        'プレビューモード
            Call stePrevWorkDelete()  '古いワークファイルを削除する
        End If

        '===進行状況表示フォーム設定=============================================


        '===各変数初期化
        mCurPage = 1                                'シート番号
        mCurRows = pstStartRow                      '印刷開始行
        DCount = 0                                  'データカウンタ
        brkShiCd = NCnvN(RecMeisai.Rows(0)("HatD_006"))      '仕入先コードブレイク
        '--- INSERT 2011/12/13 中ﾍｯﾀﾞ追加 青木 START ---------------------------------------------------------------
        brkSEQ = 0                                  'SEQブレイク
        '--- INSERT 2011/12/13 中ﾍｯﾀﾞ追加 青木 E N D ---------------------------------------------------------------
        intNo = 1                                                   'INSERT 2014/12/03 AOKI

        'シート名を仕入先名に変更
        xlApp.Worksheets(mCurPage).NAME = NCnvN(RecMeisai.Rows(0)("HatD_007"))

        '仕入名表示
        xlApp.Worksheets(GetTargetSheet).Cells(1, 1).Value = NCnvN(RecMeisai.Rows(0)("HatD_007"))


        'クリア
        Call loSheetClear()

        TotalGaku = 0               'INSERT 2018/02/28 AOKI
        For Each dr As DataRow In RecMeisai.Rows

            '進行状況用カウンタ
            DCount = DCount + 1

            '仕入先コードブレイク
            If brkShiCd <> NCnvN(dr("HatD_006")) Then
                '改頁
                Call CheckPages()
                mCurRows = pstStartRow
                intNo = 1                                           'INSERT 2014/12/03 AOKI
            End If

            '--- INSERT 2011/12/13 中ﾍｯﾀﾞ追加 青木 START ---------------------------------------------------------------
            If brkSEQ <> NCnvN(dr("JyuM_003")) Or
             brkShiCd <> NCnvN(dr("HatD_006")) Then
                Call sttWriteMHeader()
            End If

            brkSEQ = NCnvN(dr("JyuM_003"))
            '--- INSERT 2011/12/13 中ﾍｯﾀﾞ追加 青木 E N D ---------------------------------------------------------------

            '明細情報出力
            '        Call sttWriteMeisai                                    'DELETE 2014/12/03 AOKI
            Call sttWriteMeisai(intNo, dr)                              'INSERT 2014/12/03 AOKI
            intNo = intNo + 1                                       'INSERT 2014/12/03 AOKI
            mCurRows = mCurRows + 1

        Next

        'INSERT 2018/02/28 AOKI START -----------------------------------------------------------------------------------------
        With xlApp.Worksheets(GetTargetSheet)
            .Cells(mCurRows, 6).Value = "合計　：　"
            .Cells(mCurRows, 7).Value = Format(CDbl(TotalGaku), "#,###")
        End With
        'INSERT 2018/02/28 AOKI E N D -----------------------------------------------------------------------------------------


        RecMeisai = Nothing

        'モードごとの処理
        Select Case viMode
            Case enmPrintmode.Preview      'プレビュー

                Call stePrevBookSave()       'Bookの保存

                Call steWorkbooksClose()     'Bookのクローズ

                Dim exFname As String

                If stePrevFileCheck() <> "" Then
                    exFname = steCreateWorkPrev(0)
                    xlApp.Workbooks.Open(Filename:=exFname)
                End If
                xlApp.Visible = True

        End Select

        ChumonPrintProc = True


Exit_ChumonPrintProc:
        On Error GoTo 0
        Exit Function

ChumonPrintProc_Err:
        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        Call steWorkbooksClose()        'Bookのクローズ

        Resume Exit_ChumonPrintProc

    End Function

    '********************************************************************
    '**  名称    : Function CreateSQLText() String
    '**  機能    : 受注データ読み込み用SQL作成処理
    '**  戻り値  : 作成されたSQL文字列
    '**  引数    : viMode 1:梱包リスト明細　2:梱包ラベル
    '**  備考    :
    '********************************************************************
    Private Function CreateSQLText() As String

        Dim strSQL As String

        strSQL = "          SELECT   * "
        strSQL = strSQL & " FROM     HatD_Dat, "
        strSQL = strSQL & "          JyuM_Dat "
        strSQL = strSQL & " WHERE    HatD_001 = '" & modChumonPrint_txtKeiyakuNo.Text & "'"
        '    strSQL = strSQL & " AND      HatD_003 < 1000 "                                     'DELETE 2014/05/30 AOKI
        strSQL = strSQL & " AND      HatD_003 < 10000 "                                     'INSERT 2014/05/30 AOKI
        strSQL = strSQL & " AND      HatD_001 = JyuM_001 "
        strSQL = strSQL & " AND      HatD_002 = JyuM_003 "
        strSQL = strSQL & " AND      ISNULL(HatD_012,'') <> '' "
        strSQL = strSQL & " ORDER BY HatD_006, "
        strSQL = strSQL & "          HatD_002, "
        strSQL = strSQL & "          HatD_003, "
        strSQL = strSQL & "          HatD_004 "

        CreateSQLText = strSQL

    End Function

    '***************************************************************
    '**  名称    : Sub loSheetClear()
    '**  機能    : 指定シート番号の指定レンジをクリアする。
    '**  戻り値  :
    '**  引数    : vsCell  ; レンジをあらわすセル名("A1:B2")
    '**  備考    :
    '***************************************************************
    Private Sub loSheetClear()

        '罫線クリア
        With xlApp.Worksheets(GetTargetSheet).Range(pstPrintArea)
            .Select
            .ClearContents
            .Borders.LineStyle = Excel.XlLineStyle.xlLineStyleNone
        End With

    End Sub

    '***************************************************************
    '**  名称    : Function GetTargetSheet() As Long
    '**  機能    : 出力モードとカレントページから対象シート番号を返す。
    '**  戻り値  : 対象となるExcelシート番号
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function GetTargetSheet() As Long

        'モードによりシート番号を設定
        Select Case mCurMode
            Case 0  'プレビュー
                GetTargetSheet = mCurPage
            Case 1  '印刷
                GetTargetSheet = mCurPage
        End Select

    End Function

    '***************************************************************
    '**  名称    : Sub CheckPages
    '**  機能    : 出力ページと行のチェックと改頁処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub CheckPages()

        'INSERT 2018/02/28 AOKI START -----------------------------------------------------------------------------------------
        With xlApp.Worksheets(GetTargetSheet)
            .Cells(mCurRows, 6).Value = "合計　：　"
            .Cells(mCurRows, 7).Value = Format(CDbl(TotalGaku), "#,###")
        End With

        TotalGaku = 0
        'INSERT 2018/02/28 AOKI E N D -----------------------------------------------------------------------------------------

        mCurPage = mCurPage + 1                         'ページインクリメント
        Call stePrevSheetCopy(mCurPage)                 'プレビュー・印刷用Bookのコピー

        'シート名を仕入先名に変更
        xlApp.Worksheets(mCurPage).NAME = NCnvN(RecMeisai.Rows(0)("HatD_007"))
        '仕入先名表示
        xlApp.Worksheets(GetTargetSheet).Cells(1, 1).Value = NCnvN(RecMeisai.Rows(0)("HatD_007"))

        Call loSheetClear()                               'Excelシートクリア
        mCurRows = pstStartRow                          '行をリセット

    End Sub

    '***************************************************************
    '**  名称    : Sub sttWriteMeisai
    '**  機能    : 中ヘッダ情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMHeader()

        Dim dblRowH As Double
        Dim iRow As Integer

        iRow = mCurRows

        If iRow <> 3 Then
            mCurRows = mCurRows + 1
        End If

        With xlApp.Worksheets(GetTargetSheet)
            '船名
            If JyuHDat(0).JyuH_006 <> "" Then
                .Cells(3, 2).WrapText = False
                .Cells(3, 2).Value = "船名　　： " & JyuHDat(0).JyuH_006
                .Rows(3).RowHeight = 15.75
            End If

            '造船所
            If JyuHDat(0).JyuH_037 <> "" Then
                .Cells(4, 2).WrapText = False
                .Cells(4, 2).Value = "造船所 ： " & JyuHDat(0).JyuH_037
                .Rows(4).RowHeight = 15.75
            End If

            '船番
            If JyuHDat(0).JyuH_038 <> "" Then
                .Cells(5, 2).WrapText = False
                .Cells(5, 2).Value = "船番　　： " & JyuHDat(0).JyuH_038
                .Rows(5).RowHeight = 15.75
            End If

            'INSERT 2017/04/05 AOKI START -----------------------------------------------------------------------------------------
            'IMO
            If JyuHDat(0).JyuH_IMO <> 0 Then
                .Cells(6, 2).WrapText = False
                .Cells(6, 2).Value = "IMO　　： " & JyuHDat(0).JyuH_IMO
                .Rows(5).RowHeight = 15.75
            End If
            'INSERT 2017/04/05 AOKI E N D -----------------------------------------------------------------------------------------


            'UNIT
            If NCnvN(RecMeisai.Rows(0)("JyuM_004")) <> "" Then
                .Cells(mCurRows, 2).WrapText = False
                .Cells(mCurRows, 2).Value = "UNIT: " & NCnvN(RecMeisai.Rows(0)("JyuM_004"))
                .Rows(mCurRows).RowHeight = 15.75
                mCurRows = mCurRows + 1
            End If

            'MAKER
            If NCnvN(RecMeisai.Rows(0)("JyuM_005")) <> "" Then
                .Cells(mCurRows, 2).WrapText = False
                .Cells(mCurRows, 2).Value = "MAKER: " & NCnvN(RecMeisai.Rows(0)("JyuM_005"))
                .Rows(mCurRows).RowHeight = 15.75
                mCurRows = mCurRows + 1
            End If

            'TYPE
            If NCnvN(RecMeisai.Rows(0)("JyuM_006")) <> "" Then
                .Cells(mCurRows, 2).WrapText = False
                .Cells(mCurRows, 2).Value = "TYPE: " & NCnvN(RecMeisai.Rows(0)("JyuM_006"))
                .Rows(mCurRows).RowHeight = 15.75
                mCurRows = mCurRows + 1
            End If

            'SER NO
            If NCnvN(RecMeisai.Rows(0)("JyuM_007")) <> "" Then
                .Cells(mCurRows, 2).WrapText = False
                .Cells(mCurRows, 2).Value = "SER/NO: " & NCnvN(RecMeisai.Rows(0)("JyuM_007"))
                .Rows(mCurRows).RowHeight = 15.75
                mCurRows = mCurRows + 1
            End If

            'DWG NO
            If NCnvN(RecMeisai.Rows(0)("JyuM_008")) <> "" Then
                .Cells(mCurRows, 2).WrapText = False
                .Cells(mCurRows, 2).Value = "DWG NO: " & NCnvN(RecMeisai.Rows(0)("JyuM_008"))
                .Rows(mCurRows).RowHeight = 15.75
                mCurRows = mCurRows + 1
            End If

            'ADDITIONAL DETAILS
            If NCnvN(RecMeisai.Rows(0)("JyuM_025")) <> "" Then
                .Cells(mCurRows, 2).WrapText = False
                .Cells(mCurRows, 2).Value = "ADDITIONAL DETAILS: " & NCnvN(RecMeisai.Rows(0)("JyuM_025"))
                .Rows(mCurRows).RowHeight = 15.75
                mCurRows = mCurRows + 1
            End If
        End With

        If iRow <> mCurRows Then
            mCurRows = mCurRows + 1
        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub sttWriteMeisai
    '**  機能    : 明細情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    'Private Sub sttWriteMeisai()                       'DELETE 2014/12/04 AOKI
    Private Sub sttWriteMeisai(intNo As Integer, dr As DataRow)        'INSERT 2014/12/04 AOKI

        Dim strCellWk As String
        Dim dblRowH As Double

        With xlApp.Worksheets(GetTargetSheet)
            '項目
            '        .Cells(mCurRows, 1).Value = NCnvZ(Left(NCnvZ(RecMeisai![HatD_003]), Len(NCnvZ(RecMeisai![HatD_003])) - 1))
            .Cells(mCurRows, 1).Value = intNo
            '品名
            '--- UPDATE 2011/11/11 品名品番改行 青木 START -------------------------------------------------------------
            ''--- UPDATE 2011/11/08 品名品番改行 青木 START -------------------------------------------------------------
            ''        .Cells(mCurRows, 2).Value = NCnvN(RecMeisai![HatD_009])
            '        .Cells(mCurRows, 2).Value = Replace(NCnvN(RecMeisai![HatD_009]), "*", vbCrLf)
            '        .Rows(mCurRows).RowHeight = 15.75 + 15.75 * UBound(Split(NCnvN(RecMeisai![HatD_009]), "*"))
            ''--- UPDATE 2011/11/08 品名品番改行 青木 E N D -------------------------------------------------------------
            .Cells(mCurRows, 2).WrapText = True
            .Cells(mCurRows, 2).Value = Replace(NCnvN(dr("HatD_009")), "*", Space(1))
            .Rows(mCurRows).AutoFit
            .Rows(mCurRows).RowHeight = (.Rows(mCurRows).RowHeight / 14.25) * 15.75
            dblRowH = .Rows(mCurRows).RowHeight
            '--- UPDATE 2011/11/11 品名品番改行 青木 E N D -------------------------------------------------------------
            '品番
            '--- UPDATE 2011/11/11 品名品番改行 青木 START -------------------------------------------------------------
            ''--- UPDATE 2011/11/08 品名品番改行 青木 START -------------------------------------------------------------
            ''        .Cells(mCurRows, 3).Value = NCnvN(RecMeisai![HatD_010])
            '        .Cells(mCurRows, 3).Value = Replace(NCnvN(RecMeisai![HatD_010]), "*", vbCrLf)
            '        If UBound(Split(NCnvN(RecMeisai![HatD_010]), "*")) > UBound(Split(NCnvN(RecMeisai![HatD_009]), "*")) Then
            '            .Rows(mCurRows).RowHeight = 15.75 + 15.75 * UBound(Split(NCnvN(RecMeisai![HatD_010]), "*"))
            '        End If
            ''--- UPDATE 2011/11/08 品名品番改行 青木 E N D -------------------------------------------------------------
            .Cells(mCurRows, 3).WrapText = True
            .Cells(mCurRows, 3).Value = Replace(NCnvN(dr("HatD_010")), "*", Space(1))
            .Rows(mCurRows).AutoFit
            .Rows(mCurRows).RowHeight = (.Rows(mCurRows).RowHeight / 14.25) * 15.75
            If dblRowH > .Rows(mCurRows).RowHeight Then
                .Rows(mCurRows).RowHeight = dblRowH
            End If
            '--- UPDATE 2011/11/11 品名品番改行 青木 E N D -------------------------------------------------------------
            '数量
            .Cells(mCurRows, 4).Value = Format(CDbl(NCnvZ(dr("HatD_011"))), "#,###")
            '単位
            .Cells(mCurRows, 5).Value = NCnvN(dr("HatD_012"))
            '単価
            .Cells(mCurRows, 6).Value = Format(CDbl(NCnvZ(dr("HatD_013"))), "#,###")
            '--- UPDATE 2011/11/11 合計金額計算 青木 START -------------------------------------------------------------
            '金額
            '        .Cells(mCurRows, 7).Value = Format(NCnvZ(RecMeisai![HatD_014]), "#,###")
            .Cells(mCurRows, 7).Value = Format(CDbl(NCnvZ(dr("HatD_011")) * NCnvZ(dr("HatD_013"))), "#,###")
            '--- UPDATE 2011/11/11 合計金額計算 青木 E N D -------------------------------------------------------------

            TotalGaku = TotalGaku + NCnvZ(dr("HatD_011")) * NCnvZ(dr("HatD_013"))       'INSERT 2018/02/28

        End With

        '--- DELETE 2011/11/08 注文書追加 青木 START ---------------------------------------------------------------
        '罫線は不要になりました。（2011/11/07の打合せ）
        '    '罫線
        '    strCellWk = "A" & mCurRows & ":" & "G" & mCurRows
        '    Range(strCellWk).Select
        '    Selection.Borders.LineStyle = xlContinuous
        '--- DELETE 2011/11/08 注文書追加 青木 E N D ---------------------------------------------------------------
        '
        'ブレイク
        brkShiCd = NCnvN(dr("HatD_006"))

    End Sub


#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = clswinApi.StaticWinApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = clswinApi.StaticWinApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    clswinApi.StaticWinApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = clswinApi.StaticWinApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk + gstStrSEPFILE +
                    clswinApi.StaticWinApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next


        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub
#End Region

End Module
