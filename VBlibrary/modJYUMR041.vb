﻿Imports System.Windows.Forms
Imports globalcls.modHanbai
Imports clswinApi
Imports SAHANBAI.clsGlobal
'Imports Constant = SAHANBAI.Constant
Imports winApi = clswinApi.StaticWinApi
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports SAHANBAI.TaxComp
Imports System.Transactions
''all database transaction were put here , all control related
Public Module modJYUMR041
    '***************************************************************
    '**
    '**  プログラムＩＤ :  JYUMR041
    '**  機能           :  発注入力(modJYUMR041)
    '**  　　　         :
    '**  　　　         :
    '**  備考           :
    '**
    '***************************************************************
    'ﾌﾟﾛｸﾞﾗﾑID(仕入入力)

    Private xlApp As Excel.Application
    Public Const pstmPGID As String = "JYUMR041"

    Public JyuHDat(0 To 0) As JyuHDatInfo  '受注データ(ヘッダー)

    Public JyuMDat() As JyuMDatInfo  '受注データ(中ヘッダー(SEQ))
    Public JyuDDat() As JyuDDatInfo  '受注データ(明細)
    Public HacSirD() As JDHDDatInfo
    '受注データ(JyuD_Dat)更新用
    Public Structure JyuDDat_Upd
        Public JyuD_001 As String       '契約№
        Public JyuD_003 As Integer      '契約SEQ
        Public JyuD_004 As Integer      '項目
        Public JyuD_024 As Double     '仕入数量
        Public JyuD_0241 As Double     '仕入金額
        Public JyuD_025 As String       '仕入日付
    End Structure
    Public JyuD_Upd() As JyuDDat_Upd

    Public ConMst(0 To 0) As ConMstInfo     'コントロールマスタ情報

    Public Const JYUMR050_MSG002 As String = "前SEQのデータが存在しません。"
    Public Const JYUMR050_MSG003 As String = "次SEQのデータが存在しません。"

    Public strSQL As String

    Public HSCnt As Integer       '発注数量合計カウント


    'スプレッドCOL
    Public Const spdcol_SirCd As Integer = 1      '仕入先コード
    Public Const spdcol_SiRNm As Integer = 2      '仕入先名
    Public Const spdcol_Btn1 As Integer = 3      '参照ボタン
    Public Const spdcol_Seq As Integer = 4      'SEQ
    Public Const spdcol_ItmNo1 As Integer = 5      '項目（Item No.表示用）
    Public Const spdcol_ItmNo2 As Integer = 6      '項目（Item No.DB格納用）
    Public Const spdcol_RenNo As Integer = 7      '連番
    Public Const spdcol_HinNm As Integer = 8      '品名
    Public Const spdcol_HinNo As Integer = 9      '品番
    Public Const spdcol_JSuryo As Integer = 10     '受注数量
    Public Const spdcol_JTani As Integer = 11     '受注単位
    Public Const spdcol_JTanka As Integer = 12     '受注単価
    Public Const spdcol_JKingaku As Integer = 13     '受注金額
    Public Const spdcol_HSuryo As Integer = 14     '発注数量
    Public Const spdcol_HTani As Integer = 15     '発注単位
    Public Const spdcol_HTanka As Integer = 16     '発注単価
    Public Const spdcol_HRitsu As Integer = 17     '掛率
    Public Const spdcol_HNet As Integer = 18     '発注単価NET
    Public Const spdcol_HKingaku As Integer = 19     '発注金額
    Public Const spdcol_Hantei As Integer = 20     '判定
    Public Const spdcol_NDate As Integer = 21     '入荷予定日付
    Public Const spdcol_HDate As Integer = 22     '発注日付
    Public Const spdcol_SSuryo As Integer = 23     '仕入数量
    Public Const spdcol_YmNo As Integer = 24     'Yanmar No.     'INSERT 2016/03/18 AOKI

    '□ＡＰＩ関数
    Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
    Public Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hwnd As Long, ByVal Msg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

    '□SetWindowLongで使用
    Private Const GWL_WNDPROC = -4

    '□メッセージ
    Private Const WM_CONTEXTMENU = &H7B '右クリック

    '□コレクション すべてウィンドウハンドルがキー
    Dim colDProc As Collection '現在サブクラス化されているコントロールの元のWindowsProcのアドレス


    Public modJYUMR041_txtYanmarNo As TextBox

    Dim DB As clsDB.DBService

    Private numSEQ As NumericUpDown

    '***************************************************************
    '**  名称    : Sub DataReadProc()
    '**  機能    : データ読込み処理
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo 契約№
    '**          : viSEQ       SEQ
    '**  備考    :
    '***************************************************************
    Public Function DataReadProc(vsKeiyakuNo As String,
                             viSEQ As Integer,
                             Optional viSirCd As Integer = 0) As Boolean

        Dim iCnt As Integer    'ループカウンタ

        DataReadProc = False

        '== 受注データ(ヘッダー)の読込み処理
        If JyuHDatReadProc(vsKeiyakuNo) = False Then
            Exit Function
        End If

        '== 受注データ(中ヘッダー(SEQ))の読込み処理
        '    If JyuMDatReadProc((vsKeiyakuNo), viSEQ) = False Then
        If viSEQ > 0 Then
            If JyuMDatCheckProc(vsKeiyakuNo, viSEQ) = False Then
                Call ksExpMsgBox("SEQが違います。", "E")
                Exit Function
            End If
        End If

        If JyuMDatReadProc((vsKeiyakuNo)) = False Then
        End If

        ''== 受注データ(明細)の読込み処理
        '    If JyuDDatReadProc((vsKeiyakuNo), viSEQ) = False Then
        '    End If

        '== 発注データの読込み処理
        If HatSirDatReadProc((vsKeiyakuNo), viSEQ, viSirCd) = False Then
        End If

        DataReadProc = True

    End Function

    '***************************************************************
    '**  名称    : Sub JyuHDatReadProc()
    '**  機能    : 受注データ(ヘッダー)の読込み処理
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function JyuHDatReadProc(vsKeiyakuNo As String) As Boolean


        Dim SQLtxt As String
        Dim loRSet As New DataTable

        JyuHDatReadProc = False

        '受注データ(ヘッダー)読込み用SQL文作成
        '================================================================
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT * FROM JyuH_Dat "
        'INSERT 2017/04/05 AOKI START -----------------------------------------------------------------------------------------
        SQLtxt = SQLtxt & " LEFT JOIN Ves_Tbl "
        SQLtxt = SQLtxt & " ON  JyuH_006  = Ves_002 "
        SQLtxt = SQLtxt & " AND JyuH_037  = Ves_004 "
        SQLtxt = SQLtxt & " AND JyuH_038  = Ves_005 "
        SQLtxt = SQLtxt & " AND JyuH_0031 = Ves_003 "
        'INSERT 2017/04/05 AOKI E N D -----------------------------------------------------------------------------------------
        SQLtxt = SQLtxt & " WHERE JyuH_001 ='" & vsKeiyakuNo & "'"
        '================================================================

        Erase JyuHDat '受注データ(ヘッダー)内部変数クリア
        ReDim Preserve JyuHDat(0)
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)
        DB = Nothing
        If loRSet.Rows.Count > 0 Then
            With JyuHDat(0)
                .JyuH_001 = NCnvN(loRSet.Rows(0)("JyuH_001"))      ' 契約№
                .JyuH_002 = NCnvN(loRSet.Rows(0)("JyuH_002"))      ' 契約日付
                .JyuH_003 = NCnvN(loRSet.Rows(0)("JyuH_003"))      ' 得意先名(CUSTOMER)
                .JyuH_0031 = Convert.ToDouble(NCnvZ(loRSet.Rows(0)("JyuH_0031")))   ' 得意先ｺｰﾄﾞ
                .JyuH_004 = NCnvN(loRSet.Rows(0)("JyuH_004"))      ' 得意先担当者(ATTN)
                .JyuH_005 = NCnvN(loRSet.Rows(0)("JyuH_005"))      ' 客先依頼No(YOUR NO)
                .JyuH_006 = NCnvN(loRSet.Rows(0)("JyuH_006"))      ' 船名(VESSEL NAME)
                .JyuH_007 = NCnvN(loRSet.Rows(0)("JyuH_007"))      ' 造船所
                .JyuH_008 = NCnvN(loRSet.Rows(0)("JyuH_008"))      ' 当社担当(OUR PERSON)
                .JyuH_009 = NCnvN(loRSet.Rows(0)("JyuH_009"))      ' 見積No(OUR REF NO)
                .JyuH_010 = NCnvN(loRSet.Rows(0)("JyuH_010"))      ' 見積日付
                .JyuH_011 = NCnvZ(loRSet.Rows(0)("JyuH_011"))    ' 見積時納期(DELIVERY TIME)
                .JyuH_012 = NCnvN(loRSet.Rows(0)("JyuH_012"))      ' ｿｰｽ(SOURCE)
                .JyuH_013 = NCnvN(loRSet.Rows(0)("JyuH_013"))      ' 国(CONTRY)
                .JyuH_014 = NCnvN(loRSet.Rows(0)("JyuH_014"))      ' 適用
                .JyuH_015 = Convert.ToDouble(NCnvZ(loRSet.Rows(0)("JyuH_015")))     ' 支払期間(Payment terms)
                .JyuH_016 = Convert.ToDouble(NCnvZ(loRSet.Rows(0)("JyuH_016")))     ' 見積有効期間
                .JyuH_017 = NCnvN(loRSet.Rows(0)("JyuH_017"))      ' 受)客先注文番号(YOUR REF NO)
                .JyuH_018 = NCnvN(loRSet.Rows(0)("JyuH_018"))      ' 受)入荷予定日
                .JyuH_019 = NCnvN(loRSet.Rows(0)("JyuH_019"))      ' 受)入荷場所
                .JyuH_020 = NCnvN(loRSet.Rows(0)("JyuH_020"))      ' 受)受渡場所
                .JyuH_021 = NCnvN(loRSet.Rows(0)("JyuH_021"))      ' 受)指定納期
                .JyuH_022 = NCnvN(loRSet.Rows(0)("JyuH_022"))      ' 住所関連項目) 1.オーナ
                .JyuH_023 = NCnvN(loRSet.Rows(0)("JyuH_023"))      ' 住所関連項目) 2.得意先
                .JyuH_024 = NCnvN(loRSet.Rows(0)("JyuH_024"))      ' 住所関連項目) 3.住所
                .JyuH_025 = NCnvN(loRSet.Rows(0)("JyuH_025"))      ' 住所関連項目) 4.電話他
                .JyuH_026 = NCnvN(loRSet.Rows(0)("JyuH_026"))      ' 住所関連項目) 5.備考
                .JyuH_027 = Convert.ToDouble(NCnvZ(loRSet.Rows(0)("JyuH_027")))     ' データ区分(0:見積ﾃﾞｰﾀ 1:受注ﾃﾞｰﾀ)
                .JyuH_028 = NCnvN(loRSet.Rows(0)("JyuH_028"))      ' 見積書発行日
                .JyuH_029 = NCnvN(loRSet.Rows(0)("JyuH_029"))      ' 注文請書発行日
                .JyuH_030 = NCnvN(loRSet.Rows(0)("JyuH_030"))      ' 契約台帳発行日
                .JyuH_031 = NCnvN(loRSet.Rows(0)("JyuH_031"))      ' 納品書発行日
                .JyuH_037 = NCnvN(loRSet.Rows(0)("JyuH_037"))      ' 造船所          'INSERT 2015/03/25 AOKI
                .JyuH_038 = NCnvN(loRSet.Rows(0)("JyuH_038"))      ' 船番            'INSERT 2015/03/25 AOKI
                .JyuH_IMO = Convert.ToDouble(NCnvZ(loRSet.Rows(0)("Ves_007")))      ' IMO             'INSERT 2017/04/05 AOKI
                If .JyuH_027 <> 1 Then
                    Call ksExpMsgBox("この契約№はまだ「見積」段階です。受注入力を行って下さい。", "E")
                    GoTo Exit_JyuHDatReadProc
                End If
            End With
        Else
            GoTo Exit_JyuHDatReadProc
        End If

        JyuHDatReadProc = True

Exit_JyuHDatReadProc:
        loRSet = Nothing
        Exit Function

    End Function


    Public Function JyuMDatCheckProc(vsKeiyakuNo As String, viSEQ As Integer) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        JyuMDatCheckProc = False

        On Error GoTo JyuMDatCheckProc_Err

        '受注データ(中ヘッダー(SEQ))情報読込み用SQL文作成
        '================================================================
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT * FROM JyuM_Dat "
        SQLtxt = SQLtxt & " WHERE JyuM_001 ='" & vsKeiyakuNo & "'"
        SQLtxt = SQLtxt & "   AND JyuM_003 = " & viSEQ

        If DB Is Nothing Then DB = New clsDB.DBService

        loRSet = DB.GetDatafromDB(SQLtxt)
        DB = Nothing

        If loRSet.Rows.Count <= 0 Then
            GoTo JyuMDatCheckProc_Err
        End If

        JyuMDatCheckProc = True

JyuMDatCheckProc_Err:
        loRSet = Nothing

    End Function


    '***************************************************************
    '**  名称    : Sub JyuMDatReadProc()
    '**  機能    : 受注データ(中ヘッダー(SEQ))情報の読込み処理
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo : 契約№
    '**          : viSEQ       : SEQ
    '**          : viMode      : 0 対象データ
    '**          :             : 1 前データ
    '**          :             : 2 次データ
    '**  備考    :
    '***************************************************************
    'Public Function JyuMDatReadProc(vsKeiyakuNo As String, _
    '                                viSEQ As Integer, _
    '                                Optional viMode As Integer = 0) As Boolean
    Public Function JyuMDatReadProc(vsKeiyakuNo As String) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim DCount As Integer
        Dim intSEQ As Integer


        JyuMDatReadProc = False



        Erase JyuMDat '受注データ(中ヘッダー(SEQ))内部変数クリア

        '================================================================
        SQLtxt = ""
        '    SQLtxt = SQLtxt & " SELECT TOP 1 * FROM JyuM_Dat"
        SQLtxt = SQLtxt & " SELECT * FROM JyuM_Dat"
        SQLtxt = SQLtxt & " WHERE JyuM_001 ='" & vsKeiyakuNo & "'"


        '受注データ(中ヘッダー(SEQ))の読込み
        Dim DB As New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)
        DB = Nothing

        If loRSet.Rows.Count < 1 Then
            GoTo Exit_JyuMDatReadProc
        End If

        DCount = 0

        For Each dr As DataRow In loRSet.Rows

            ReDim Preserve JyuMDat(DCount)

            With JyuMDat(DCount)
                .JyuM_001 = NCnvN(dr("JyuM_001"))    ' 契約№
                .JyuM_002 = Convert.ToDouble(NCnvZ((dr("JyuM_002"))))   ' 見積№
                .JyuM_003 = Convert.ToDouble(NCnvZ((dr("JyuM_003"))))  ' SEQ
                .JyuM_004 = NCnvN(dr("JyuM_004"))    ' UNIT
                .JyuM_005 = NCnvN(dr("JyuM_005"))    ' MAKER
                .JyuM_006 = NCnvN(dr("JyuM_006"))    ' TYPE
                .JyuM_007 = NCnvN(dr("JyuM_007"))    ' SER/NO
                .JyuM_008 = NCnvN(dr("JyuM_008"))    ' DWG/NO
                .JyuM_009 = NCnvN(dr("JyuM_009"))    ' COMMENT1
                .JyuM_010 = NCnvN(dr("JyuM_010"))    ' COMMENT2
                .JyuM_011 = NCnvN(dr("JyuM_011"))    ' COMMENT3
                .JyuM_012 = NCnvN(dr("JyuM_012"))    ' COMMENT4
                .JyuM_0121 = NCnvN(dr("JyuM_0121"))  ' COMMENT5      'INSERT 2014/01/30 AOKI
                .JyuM_013 = Convert.ToDouble(NCnvZ((dr("JyuM_013"))))  ' 見積数量
                .JyuM_014 = Convert.ToDouble(NCnvZ((dr("JyuM_014"))))  ' 見積金額
                .JyuM_015 = Convert.ToDouble(NCnvZ((dr("JyuM_015"))))  ' 受注数量
                .JyuM_016 = Convert.ToDouble(NCnvZ((dr("JyuM_016"))))  ' 受注金額
                .JyuM_017 = Convert.ToDouble(NCnvZ((dr("JyuM_017"))))  ' 発注数量
                .JyuM_018 = Convert.ToDouble(NCnvZ((dr("JyuM_018"))))  ' 発注金額
                .JyuM_019 = Convert.ToDouble(NCnvZ((dr("JyuM_019"))))  ' 仕入数量
                .JyuM_020 = Convert.ToDouble(NCnvZ((dr("JyuM_020"))))  ' 仕入金額
                .JyuM_021 = Convert.ToDouble(NCnvZ((dr("JyuM_021"))))  ' 売上数量
                .JyuM_022 = Convert.ToDouble(NCnvZ((dr("JyuM_022"))))  ' 売上金額
                .JyuM_023 = NCnvN(dr("JyuM_023"))    ' 請求日付
            End With

            DCount = DCount + 1

        Next


        JyuMDatReadProc = True

Exit_JyuMDatReadProc:
        loRSet = Nothing
        Exit Function

    End Function
    '***************************************************************
    '**  名称    : Sub HatSirDatReadProc()
    '**  機能    : 発注/仕入データ情報の読込み処理
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo : 契約№
    '**          : viSEQ       : SEQ
    '**  備考    :
    '***************************************************************
    Public Function HatSirDatReadProc(vsKeiyakuNo As String,
                                  viSEQ As Integer,
                                  Optional viSirCd As Integer = 0) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim DCount As Integer

        HatSirDatReadProc = False

        Erase HacSirD '発注/仕入データ内部変数クリア

        '発注/仕入データ情報読込み用SQL文作成
        '================================================================
        SQLtxt = "  SELECT * FROM JyuD_Dat"
        SQLtxt = SQLtxt & "  LEFT JOIN HatD_Dat"
        SQLtxt = SQLtxt & "  ON JyuD_001 = HatD_001 "
        SQLtxt = SQLtxt & "  AND JyuD_003 = HatD_002 "
        SQLtxt = SQLtxt & "  AND JyuD_004 = HatD_003 "
        SQLtxt = SQLtxt & " WHERE JyuD_001 ='" & vsKeiyakuNo & "'"

        '    SQLtxt = SQLtxt & "   AND JyuD_003 = " & viSEQ
        If viSEQ > 0 Then
            SQLtxt = SQLtxt & "   AND JyuD_003 = " & viSEQ
        End If

        If viSirCd > 0 Then
            'UPDATE 2015/10/28 AOKI START -----------------------------------------------------------------------------------------
            '        SQLtxt = SQLtxt & " AND JyuD_0071 = " & viSirCd
            SQLtxt = SQLtxt & " AND HatD_006 = " & viSirCd
            'UPDATE 2015/10/28 AOKI E N D -----------------------------------------------------------------------------------------
        End If

        SQLtxt = SQLtxt & "   AND JyuD_004 <= 10000"
        SQLtxt = SQLtxt & " ORDER BY JyuD_003,JyuD_004,HatD_004"
        '================================================================

        '発注/仕入データの読込み
        Dim DB As New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)
        DB = Nothing

        If loRSet.Rows.Count < 1 Then
            GoTo Exit_HatSirDatReadProc
        End If

        DCount = 0
        HSCnt = 0

        'データ設定
        For Each dr As DataRow In loRSet.Rows

            If Convert.ToDouble(NCnvZ(dr("JyuD_018"))) <> 0 Then

                ReDim Preserve HacSirD(DCount)
                With HacSirD(DCount)
                    .JyuD.JyuD_001 = NCnvN(dr("JyuD_001"))     ' 契約№
                    .JyuD.JyuD_002 = NCnvN(dr("JyuD_002"))     ' 見積№
                    .JyuD.JyuD_003 = Convert.ToDouble(NCnvZ(dr("JyuD_003")))    ' 契約SEQ
                    .JyuD.JyuD_004 = Convert.ToDouble(NCnvZ(dr("JyuD_004")))    ' 項目
                    .JyuD.JyuD_005 = NCnvN(dr("JyuD_005"))     ' 品名
                    .JyuD.JyuD_006 = NCnvN(dr("JyuD_006"))     ' 品番
                    .JyuD.JyuD_007 = NCnvN(dr("JyuD_007"))     ' 仕入先名
                    .JyuD.JyuD_0071 = Convert.ToDouble(NCnvZ(dr("JyuD_0071")))  ' 仕入先コード
                    .JyuD.JyuD_008 = Convert.ToDouble(NCnvZ(dr("JyuD_008")))    ' 見積数量
                    .JyuD.JyuD_009 = NCnvN(dr("JyuD_009"))     ' 見積単位
                    .JyuD.JyuD_010 = Convert.ToDouble(NCnvZ(dr("JyuD_010")))    ' 見積単価
                    .JyuD.JyuD_011 = NCnvN(dr("JyuD_011"))     ' 見積金額
                    .JyuD.JyuD_012 = Convert.ToDouble(NCnvZ(dr("JyuD_012")))    '
                    .JyuD.JyuD_013 = Convert.ToDouble(NCnvZ(dr("JyuD_013")))    ' 掛率
                    .JyuD.JyuD_014 = Convert.ToDouble(NCnvZ(dr("JyuD_014")))    '
                    .JyuD.JyuD_015 = Convert.ToDouble(NCnvZ(dr("JyuD_015")))    '
                    .JyuD.JyuD_016 = Convert.ToDouble(NCnvZ(dr("JyuD_016")))    '
                    .JyuD.JyuD_017 = NCnvN(dr("JyuD_017"))     '
                    .JyuD.JyuD_018 = Convert.ToDouble(NCnvZ(dr("JyuD_018")))    ' 受注数量
                    .JyuD.JyuD_019 = NCnvN(dr("JyuD_019"))     ' 受注単位
                    .JyuD.JyuD_020 = Convert.ToDouble(NCnvZ(dr("JyuD_020")))    ' 受注単価
                    .JyuD.JyuD_021 = Convert.ToDouble(NCnvZ(dr("JyuD_021")))    ' 受注金額
                    .JyuD.JyuD_022 = Convert.ToDouble(NCnvZ(dr("JyuD_022")))    ' 発注数量
                    .JyuD.JyuD_023 = NCnvN(dr("JyuD_023"))     ' 発注日付

                    .HatD.HatD_001 = NCnvN(dr("HatD_001"))     ' 契約№
                    .HatD.HatD_002 = Convert.ToDouble(NCnvZ(dr("HatD_002")))    ' 契約SEQ
                    .HatD.HatD_003 = Convert.ToDouble(NCnvZ(dr("HatD_003")))    ' 項目
                    .HatD.HatD_004 = Convert.ToDouble(NCnvZ(dr("HatD_004")))    ' 発注SEQ
                    .HatD.HatD_005 = NCnvN(dr("HatD_005"))     ' 発注番号
                    .HatD.HatD_006 = Convert.ToDouble(NCnvZ(dr("HatD_006")))    ' 仕入先コード
                    .HatD.HatD_007 = NCnvN(dr("HatD_007"))     ' 仕入先名称
                    .HatD.HatD_008 = NCnvN(dr("HatD_008"))     ' 発注日付
                    .HatD.HatD_009 = NCnvN(dr("HatD_009"))     ' 品名
                    .HatD.HatD_010 = NCnvN(dr("HatD_010"))     ' 品番
                    .HatD.HatD_011 = Convert.ToDouble(NCnvZ(dr("HatD_011")))   ' 発注数量
                    .HatD.HatD_012 = NCnvN(dr("HatD_012"))     ' 発注単位
                    .HatD.HatD_013 = Convert.ToDouble(NCnvZ(dr("HatD_013")))   ' 発注単価
                    .HatD.HatD_0131 = Convert.ToDouble(NCnvZ(dr("HatD_0131")))  ' 発注掛率
                    .HatD.HatD_0132 = Convert.ToDouble(NCnvZ(dr("HatD_0132")))  ' 発注単価NET
                    .HatD.HatD_014 = Convert.ToDouble(NCnvZ(dr("HatD_014")))   ' 発注金額
                    .HatD.HatD_015 = NCnvN(dr("HatD_015"))     ' 入荷予定日
                    .HatD.HatD_016 = NCnvN(dr("HatD_016"))     ' 入荷日
                    .HatD.HatD_017 = Convert.ToDouble(NCnvZ(dr("HatD_017")))    ' 仕入数量
                    .HatD.HatD_018 = NCnvN(dr("HatD_018"))     ' 仕入単位
                    .HatD.HatD_019 = Convert.ToDouble(NCnvZ(dr("HatD_019")))   ' 仕入単価
                    .HatD.HatD_020 = Convert.ToDouble(NCnvZ(dr("HatD_020")))   ' 掛率
                    .HatD.HatD_021 = Convert.ToDouble(NCnvZ(dr("HatD_021")))   ' 仕入単価NET
                    .HatD.HatD_022 = Convert.ToDouble(NCnvZ(dr("HatD_022")))    ' 仕入金額
                    .HatD.HatD_030 = Convert.ToDouble(NCnvZ(dr("HatD_030")))    ' 仕入完了チェック
                    .HatD.HatD_031 = NCnvN(dr("HatD_031"))     ' 仕入日付
                    .HatD.HatD_032 = NCnvN(dr("HatD_032"))     ' Yanmar No.          'INSERT 2016/03/18 AOKI


                    HSCnt = HSCnt + Convert.ToDouble(NCnvZ(dr("HatD_011")))
                End With
                DCount = DCount + 1
            End If
        Next
        HatSirDatReadProc = True

Exit_HatSirDatReadProc:
        loRSet = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : DataInsertProc()
    '**  機能    : データインサート処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function DataInsertProc(vsKeiyakuNo As String, viSEQ As Integer) As Integer

        Dim strSQL As String       'SQL文作成用
        Dim strMsg As String = ""     'エラーメッセージ作成用
        Dim lngErr As Long         'エラー番号
        Dim Idx As Integer
        Dim loRSet As New DataTable
        Dim flgSir As Boolean
        Dim BreakKey As Integer      '項目(ブレークキー)
        Dim wkSeq As Integer      'SEQ                    'INSERT 2015/10/28 AOKI
        Dim intJSuryo As Integer      '受注数量
        Dim intHSuryo As Integer      '発注数量

        DataInsertProc = 0


        DB = New clsDB.DBService
        'セッション開始
        flgSir = True
        Dim TransactionOptions As New TransactionOptions()
        TransactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
        TransactionOptions.Timeout = TimeSpan.MaxValue

        Using transcope As New TransactionScope(TransactionScopeOption.Required, TransactionOptions)

            Try


                BreakKey = HacSirD(LBound(HacSirD)).HatD.HatD_003       '項目
                wkSeq = HacSirD(LBound(HacSirD)).HatD.HatD_002          'SEQ        'INSERT 2015/10/28 AOKI
                intJSuryo = HacSirD(LBound(HacSirD)).JyuD.JyuD_018      '受注数量
                intHSuryo = 0

                For Idx = LBound(HacSirD) To UBound(HacSirD)

                    With HacSirD(Idx)

                        '発注データ情報読込み用SQL文作成
                        '================================================================
                        strSQL = " SELECT *  FROM HatD_Dat "
                        strSQL = strSQL & " WHERE HatD_001 ='" & modCommon.EditSQLAddSQuot(.HatD.HatD_001) & "'"
                        strSQL = strSQL & "   AND HatD_002 = " & .HatD.HatD_002
                        strSQL = strSQL & "   AND HatD_003 = " & .HatD.HatD_003
                        strSQL = strSQL & "   AND HatD_004 = " & .HatD.HatD_004
                        strSQL = strSQL & " ORDER BY HatD_003,HatD_004"
                        '================================================================

                        '発注データの読込み

                        loRSet = DB.GetDatafromDB(strSQL)



                        If loRSet.Rows.Count < 1 Then
                            '追加用SQL文作成
                            '===========================================
                            strSQL = ""
                            strSQL = strSQL & "INSERT INTO HatD_Dat("
                            strSQL = strSQL & "HatD_001,"                                   '契約№
                            strSQL = strSQL & "HatD_002,"                                   '契約SEQ
                            strSQL = strSQL & "HatD_003,"                                   '項目
                            strSQL = strSQL & "HatD_004,"                                   '発注SEQ
                            strSQL = strSQL & "HatD_005,"                                   '発注番号
                            strSQL = strSQL & "HatD_006,"                                   '仕入先コード
                            strSQL = strSQL & "HatD_007,"                                   '仕入先名称
                            strSQL = strSQL & "HatD_008,"                                   '発注日付
                            strSQL = strSQL & "HatD_009,"                                   '品名
                            strSQL = strSQL & "HatD_010,"                                   '品番
                            strSQL = strSQL & "HatD_011,"                                   '発注数量
                            strSQL = strSQL & "HatD_012,"                                   '発注単位
                            strSQL = strSQL & "HatD_013,"                                   '発注単価
                            strSQL = strSQL & "HatD_0131,"                                  '掛率
                            strSQL = strSQL & "HatD_0132,"                                  '発注単価NET
                            strSQL = strSQL & "HatD_014,"                                   '発注金額
                            strSQL = strSQL & "HatD_015,"                                   '入荷予定日
                            'INSERT 2016/03/18 AOKI START -----------------------------------------------------------------------------------------
                            If modJYUMR041_txtYanmarNo.Text <> "" Then
                                strSQL = strSQL & "HatD_032,"                               'Yanmar No.
                            End If
                            'INSERT 2016/03/18 AOKI E N D -----------------------------------------------------------------------------------------
                            strSQL = strSQL & "HatD_Insert,"                                '追加日時
                            strSQL = strSQL & "HatD_WsNo"                                   'WSNO
                            strSQL = strSQL & ") VALUES ('"
                            strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_001) & "',"        '契約№
                            strSQL = strSQL & .HatD.HatD_002 & ","                          '契約SEQ
                            strSQL = strSQL & .HatD.HatD_003 & ","                          '項目
                            strSQL = strSQL & .HatD.HatD_004 & ",'"                         '発注SEQ
                            strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_005) & "',"        '発注番号
                            strSQL = strSQL & .HatD.HatD_006 & ",'"                         '仕入先コード
                            strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_007) & "','"       '仕入先名
                            strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_008) & "','"       '発注日付
                            strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_009) & "','"       '品名
                            strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_010) & "',"        '品番
                            strSQL = strSQL & .HatD.HatD_011 & ",'"                         '発注数量
                            strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_012) & "',"        '発注単位
                            strSQL = strSQL & .HatD.HatD_013 & ","                          '発注単価
                            strSQL = strSQL & .HatD.HatD_0131 & ","                         '掛率
                            strSQL = strSQL & .HatD.HatD_0132 & ","                         '発注単価NET
                            strSQL = strSQL & .HatD.HatD_014 & ",'"                         '発注金額
                            'INSERT 2016/03/18 AOKI START -----------------------------------------------------------------------------------------
                            If modJYUMR041_txtYanmarNo.Text <> "" Then
                                strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_032) & "','"   'Yanmar No.     'INSERT 2016/03/18 AOKI
                            End If
                            'INSERT 2016/03/18 AOKI E N D -----------------------------------------------------------------------------------------
                            strSQL = strSQL & modCommon.EditSQLAddSQuot(.HatD.HatD_015) & "','"       '入荷予定日
                            strSQL = strSQL & Now & "','"                                   '追加日時
                            strSQL = strSQL & clswinApi.StaticWinApi.Wsnumber & "')"                            'WSNO
                            '===========================================

                        Else
                            'スプレッド上で仕入数量が0より大きい場合に、発注数量のみをロックにしている。
                            '請求後だとしても発注金額を訂正出来る。
                            '                IfCDbl(NCnvZ(loRSet![HatD_017]) > 0 Then
                            '                    strSQL = ""
                            '                    flgSir = False
                            '                Else
                            '更新用SQL文作成
                            '===========================================
                            strSQL = ""
                            strSQL = strSQL & "UPDATE HatD_Dat SET "
                            strSQL = strSQL & "       HatD_006    = " & .HatD.HatD_006                          '仕入先コード
                            strSQL = strSQL & "      ,HatD_007    ='" & modCommon.EditSQLAddSQuot(.HatD.HatD_007) & "'"   '仕入先名称
                            strSQL = strSQL & "      ,HatD_008    ='" & .HatD.HatD_008 & "'"                    '発注日付
                            'UPDATEする意味ない（特に画面上の空白行を更新する必要ない）
                            'strSQL = strSQL & "      ,HatD_009    ='" & modCommon.EditSQLAddSQuot(.HatD.HatD_009) & "'"   '品名
                            'strSQL = strSQL & "      ,HatD_010    ='" & modCommon.EditSQLAddSQuot(.HatD.HatD_010) & "'"   '品番
                            strSQL = strSQL & "      ,HatD_011    = " & .HatD.HatD_011                          '発注数量
                            strSQL = strSQL & "      ,HatD_012    ='" & modCommon.EditSQLAddSQuot(.HatD.HatD_012) & "'"   '発注単位
                            strSQL = strSQL & "      ,HatD_013    = " & .HatD.HatD_013                          '発注単価
                            strSQL = strSQL & "      ,HatD_0131   = " & .HatD.HatD_0131                         '掛率
                            strSQL = strSQL & "      ,HatD_0132   = " & .HatD.HatD_0132                         '発注単価NET
                            strSQL = strSQL & "      ,HatD_014    = " & .HatD.HatD_014                          '発注金額
                            strSQL = strSQL & "      ,HatD_015    ='" & .HatD.HatD_015 & "'"                    '入荷予定日
                            'INSERT 2016/03/18 AOKI START -----------------------------------------------------------------------------------------
                            If modJYUMR041_txtYanmarNo.Text <> "" Then
                                strSQL = strSQL & "      ,HatD_032    ='" & .HatD.HatD_032 & "'"                'Yanmar No.     'INSERT 2016/03/18 AOKI
                            End If
                            'INSERT 2016/03/18 AOKI E N D -----------------------------------------------------------------------------------------
                            strSQL = strSQL & "      ,HatD_Update ='" & Now & "'"                               '更新日
                            strSQL = strSQL & "      ,HatD_WsNo   ='" & clswinApi.StaticWinApi.Wsnumber & "'"                       'WsNo
                            strSQL = strSQL & " WHERE HatD_001 ='" & modCommon.EditSQLAddSQuot(.HatD.HatD_001) & "'"
                            strSQL = strSQL & "   AND HatD_002 = " & .HatD.HatD_002
                            strSQL = strSQL & "   AND HatD_003 = " & .HatD.HatD_003
                            strSQL = strSQL & "   AND HatD_004 = " & .HatD.HatD_004
                            '                End If


                        End If

                        If strSQL <> "" Then
                            'SQL文実行
                            If DB.ExecuteSql(strSQL, strMsg) = False Then
                                GoTo Error_DataInsertProc
                            End If
                        End If


                        If .HatD.HatD_011 = 0 Then
                            strMsg = "発注数量が0です。"
                            GoTo Error_DataInsertProc
                        Else
                            If BreakKey <> .HatD.HatD_003 Then
                                If intJSuryo <> intHSuryo Then
                                    If HatD_Insert(.HatD.HatD_001, wkSeq, BreakKey, .HatD.HatD_004, intJSuryo - intHSuryo) = False Then
                                        GoTo Error_DataInsertProc
                                    End If
                                End If
                                intHSuryo = .HatD.HatD_011
                                intJSuryo = .JyuD.JyuD_018
                            Else
                                intHSuryo = intHSuryo + .HatD.HatD_011
                            End If
                        End If
                        'UPDATE 2016/09/09 AOKI E N D -----------------------------------------------------------------------------------------

                        BreakKey = .HatD.HatD_003   '項目
                        wkSeq = .HatD.HatD_002      'SEQ        'INSERT 2015/10/28 AOKI

                    End With

                Next Idx

                'INSERT 2015/05/20 AOKI START -----------------------------------------------------------------------------------------
                With HacSirD(UBound(HacSirD))

                    If intJSuryo <> intHSuryo Then
                        If HatD_Insert(.HatD.HatD_001, .HatD.HatD_002, BreakKey, .HatD.HatD_004, intJSuryo - intHSuryo) = False Then
                            GoTo Error_DataInsertProc
                        End If
                    End If

                End With


                strSQL = "          SELECT     JyuD_001 "                       '契約№
                strSQL = strSQL & "          , JyuD_003 "                       'SEQ
                strSQL = strSQL & "          , SUM(HatD_011)   AS HatD_011 "    '発注数量
                strSQL = strSQL & "          , SUM(HatD_014)   AS HatD_014 "    '発注金額
                strSQL = strSQL & " FROM JyuD_Dat "
                strSQL = strSQL & " INNER JOIN HatD_Dat "
                strSQL = strSQL & " ON         JyuD_001 = HatD_001 "
                strSQL = strSQL & " AND        JyuD_003 = HatD_002 "
                strSQL = strSQL & " AND        JyuD_004 = HatD_003 "
                strSQL = strSQL & " WHERE      JyuD_001 ='" & modCommon.EditSQLAddSQuot(JyuMDat(LBound(JyuMDat)).JyuM_001) & "'"
                strSQL = strSQL & " GROUP BY   JyuD_001 "
                strSQL = strSQL & "          , JyuD_003 "
                strSQL = strSQL & " ORDER BY   JyuD_001 "
                strSQL = strSQL & "          , JyuD_003 "

                '発注/仕入データの読込み

                loRSet = DB.GetDatafromDB(strSQL)

                If loRSet.Rows.Count < 1 Then
                    GoTo Error_DataInsertProc
                End If

                For Each dr As DataRow In loRSet.Rows


                    strSQL = ""
                    strSQL = strSQL & "UPDATE JyuM_Dat SET "
                    strSQL = strSQL & "       JyuM_017 = " & CDbl(NCnvZ(dr("HatD_011")))        '発注数量
                    strSQL = strSQL & ", JyuM_018 = " & CDbl(NCnvZ(dr("HatD_014")))      '発注金額
                    strSQL = strSQL & ", JyuM_Update ='" & Now & "'"
                    strSQL = strSQL & " WHERE JyuM_001 ='" & NCnvN(dr("JyuD_001")) & "'"
                    strSQL = strSQL & "   AND JyuM_003 = " & CDbl(NCnvZ(dr("JyuD_003")))

                    'SQL文実行
                    If DB.ExecuteSql(strSQL, strMsg) = False Then
                        GoTo Error_DataInsertProc
                    End If

                Next
                'UPDATE 2015/10/28 AOKI E N D -----------------------------------------------------------------------------------------


                '受注明細データ(発注数量＆発注日付)更新処理
                If JyuDUpdateProc() = False Then
                    GoTo Error_DataInsertProc
                End If


                transcope.Complete()
                transcope.Dispose()

                DataInsertProc = 1

                If flgSir = False Then
                    DataInsertProc = 2
                End If

                DB = Nothing
                Exit Function

            Catch ex As Exception
                DB = Nothing
                transcope.Dispose()
                Call ksExpMsgBox(gstGUIDE_E002 & " ERR:  " & ex.Message, "E")
                Exit Function
            End Try

Error_DataInsertProc:
            DB = Nothing
            transcope.Dispose()
            Call ksExpMsgBox(gstGUIDE_E002 & " ERR: " & strMsg, "E")
            Exit Function
        End Using



    End Function

    '***************************************************************
    '**  名称    : JyuMUpdateProc()
    '**  機能    : 受注明細データ発注情報更新処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function JyuMUpdateProc() As Boolean

        Dim strSQL As String       'SQL分作成用ワーク
        Dim strMsg As String       'エラーメッセージ作成用
        Dim lngErr As Long         'エラー番号
        Dim Idx As Integer

        JyuMUpdateProc = False

        '受注データ発注数量/発注金額更新
        For Idx = LBound(JyuMDat) To UBound(JyuMDat)
            With JyuMDat(Idx)
                strSQL = ""
                strSQL = strSQL & "UPDATE JyuM_Dat SET "
                strSQL = strSQL & "       JyuM_017 = " & .JyuM_017
                strSQL = strSQL & "      ,JyuM_018 = " & .JyuM_018
                strSQL = strSQL & "      ,JyuM_Update ='" & Now & "'"
                strSQL = strSQL & "      ,JyuM_WsNo = '" & clswinApi.StaticWinApi.Wsnumber & "'"
                strSQL = strSQL & " WHERE JyuM_001 ='" & modCommon.EditSQLAddSQuot(.JyuM_001) & "'"
                strSQL = strSQL & "   AND JyuM_003 = " & .JyuM_003
            End With

            'SQL文実行
            If DB Is Nothing Then DB = New clsDB.DBService
            If DB.ExecuteSql(strSQL, strMsg) = False Then
                GoTo Error_JyuMUpdateProc
            End If
        Next

        JyuMUpdateProc = True

Exit_JyuMUpdateProc:

        Exit Function

Error_JyuMUpdateProc:
        Call ksExpMsgBox("受注明細" & gstGUIDE_E003 & " ERR: " & strMsg, "E")
        GoTo Exit_JyuMUpdateProc

    End Function



    '***************************************************************
    '**  名称    : JyuDUpdateProc()
    '**  機能    : 受注明細データ発注情報更新処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function JyuDUpdateProc() As Boolean

        Dim SQLtxt As String       'SQL分作成用ワーク
        Dim strMsg As String       'エラーメッセージ作成用
        Dim lngErr As Long         'エラー番号
        Dim loRSet As New DataTable

        Dim BreakKey1 As Integer      'SEQ(ブレークキー)
        Dim BreakKey2 As Integer      '項目(ブレークキー)
        Dim lcHSuryo As Double     '発注数量
        Dim lsHDate As String       '発注日付

        Dim liSirCd As Integer      '仕入先コード
        Dim lsSirNm As String       '仕入先名

        JyuDUpdateProc = False

        '発注データ情報読込み用SQL文作成
        '================================================================
        With JyuMDat(LBound(JyuMDat))
            SQLtxt = " SELECT *  FROM HatD_Dat "
            SQLtxt = SQLtxt & " WHERE HatD_001 ='" & modCommon.EditSQLAddSQuot(.JyuM_001) & "'"
            SQLtxt = SQLtxt & " ORDER BY HatD_002,HatD_003,HatD_004"
        End With
        '================================================================

        '発注データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)

        If loRSet.Rows.Count < 1 Then
            GoTo Exit_JyuDUpdateProc
        End If

        BreakKey1 = CDbl(NCnvZ(loRSet.Rows(0)("HatD_002")))    'SEQ
        BreakKey2 = CDbl(NCnvZ(loRSet.Rows(0)("HatD_003")))    '項目

        'データ設定
        For Each dr As DataRow In loRSet.Rows

            If (BreakKey1 <> CDbl(NCnvZ(dr("HatD_002")))) Or
           (BreakKey2 <> CDbl(NCnvZ(dr("HatD_003")))) Then

                '受注データ発注数量/発注金額更新
                SQLtxt = " UPDATE JyuD_Dat "
                SQLtxt = SQLtxt & "Set JyuD_022    = " & lcHSuryo                           '発注数量
                SQLtxt = SQLtxt & "   , JyuD_023 ='" & modCommon.EditSQLAddSQuot(lsHDate) & "'"     '発注日付
                SQLtxt = SQLtxt & "   ,JyuD_Update ='" & Now & "'"                          '更新日付
                SQLtxt = SQLtxt & "   ,JyuD_007    ='" & modCommon.EditSQLAddSQuot(lsSirNm) & "'"     '仕入先名
                SQLtxt = SQLtxt & "   ,JyuD_0071   = " & liSirCd                            '仕入先コード
                SQLtxt = SQLtxt & " WHERE JyuD_001 ='" & CDbl(NCnvZ(dr("HatD_001"))) & "'"
                SQLtxt = SQLtxt & "   AND JyuD_003 = " & BreakKey1
                SQLtxt = SQLtxt & "   AND JyuD_004 = " & BreakKey2

                'SQL文実行
                If DB Is Nothing Then DB = New clsDB.DBService
                If DB.ExecuteSql(strSQL, strMsg) = False Then
                    GoTo Error_JyuDUpdateProc
                End If

                lcHSuryo = 0                        '発注数量
                lsHDate = ""                        '発注日付

                BreakKey1 = CDbl(NCnvZ(dr("HatD_002")))
                BreakKey2 = CDbl(NCnvZ(dr("HatD_003")))

            End If

            lcHSuryo = lcHSuryo + CDbl(NCnvZ(dr("HatD_011"))) '発注数量
            If NCnvN(dr("HatD_008")) > lsHDate Then
                lsHDate = NCnvN(dr("HatD_008"))          '発注日付(最終発注日)
            End If

            If CDbl(NCnvZ(dr("HatD_004"))) = 1 Then
                liSirCd = CDbl(NCnvZ(dr("HatD_006")))         '仕入先コード
                lsSirNm = NCnvN(dr("HatD_007"))          '仕入先名称
            End If


        Next
        '受注データ発注数量/発注金額更新
        With JyuMDat(LBound(JyuMDat))
            SQLtxt = " UPDATE JyuD_Dat "
            SQLtxt = SQLtxt & "Set JyuD_022    = " & lcHSuryo                               '発注数量
            SQLtxt = SQLtxt & "   , JyuD_023 ='" & modCommon.EditSQLAddSQuot(lsHDate) & "'"         '発注日付
            SQLtxt = SQLtxt & "   ,JyuD_Update ='" & Now & "'"                              '更新日付
            SQLtxt = SQLtxt & "   ,JyuD_007    ='" & modCommon.EditSQLAddSQuot(lsSirNm) & "'"         '仕入先名
            SQLtxt = SQLtxt & "   ,JyuD_0071   = " & liSirCd                                '仕入先コード
            SQLtxt = SQLtxt & " WHERE JyuD_001 ='" & modCommon.EditSQLAddSQuot(.JyuM_001) & "'"
            SQLtxt = SQLtxt & "   AND JyuD_003 = " & BreakKey1
            SQLtxt = SQLtxt & "   AND JyuD_004 = " & BreakKey2
        End With

        'SQL文実行
        If DB Is Nothing Then DB = New clsDB.DBService
        If DB.ExecuteSql(strSQL, strMsg) = False Then
            GoTo Error_JyuDUpdateProc
        End If

        JyuDUpdateProc = True

Exit_JyuDUpdateProc:
        loRSet = Nothing
        Exit Function

Error_JyuDUpdateProc:
        Call ksExpMsgBox("受注明細" & gstGUIDE_E003 & " ERR: " & strMsg, "E")
        GoTo Exit_JyuDUpdateProc

    End Function



    '***************************************************************
    '**  名称    : Sub HatD_Insert()
    '**  機能    : 発注残分（受注数量－発注数量）を発注データとして追加する
    '**  戻り値  :
    '**  引数    : vKey1～4：更新キー　HSuu:発注数
    '**  備考    :
    '***************************************************************
    Private Function HatD_Insert(vKey1 As String, vKey2 As Integer, vKey3 As Integer, vKey4 As Integer,
                             HSuu As Long) As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim iSEQ As Integer
        Dim iSEQ2 As Integer

        On Error GoTo Exit_HatD_Insert

        'HatD_Dat読込み 発注SEQの最大値を取得
        strSQL = "SELECT    MAX(HatD_004) AS HD_004MAX "
        strSQL = strSQL & ",MIN(HatD_004) AS HD_004MIN "
        strSQL = strSQL & " FROM HatD_Dat "
        strSQL = strSQL & " WHERE HatD_001 ='" & vKey1 & "'"
        strSQL = strSQL & "   AND HatD_002 = " & vKey2
        strSQL = strSQL & "   AND HatD_003 = " & vKey3
        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)


        'UPDATE 2015/10/28 AOKI START -----------------------------------------------------------------------------------------
        '    If Not loRec.EOF Then
        '        iSEQ = loRec![HD_004MAX] + 1
        '        iSEQ2 = loRec![HD_004MIN]
        '    Else
        '        iSEQ = 1
        '        iSEQ2 = loRec![HD_004MIN]
        '    End If
        If CDbl(NCnvZ(loRec.Rows(0)("HD_004MIN"))) > 0 Then
            iSEQ = loRec.Rows(0)("HD_004MAX") + 1
            iSEQ2 = loRec.Rows(0)("HD_004MIN")
        Else
            GoTo Exit_HatD_Insert
        End If
        'UPDATE 2015/10/28 AOKI E N D -----------------------------------------------------------------------------------------


        loRec = Nothing

        '発注データ(HatD_Dat)読込み用SQL文作成
        strSQL = "SELECT * FROM HatD_Dat"
        strSQL = strSQL & " WHERE HatD_001 ='" & vKey1 & "'"
        strSQL = strSQL & "   AND HatD_002 = " & vKey2
        strSQL = strSQL & "   AND HatD_003 = " & vKey3
        strSQL = strSQL & "   AND HatD_004 = " & iSEQ2
        loRec = DB.GetDatafromDB(strSQL)

        If loRec.Rows.Count <= 0 Then
            Call ksExpMsgBox("発注残分データの作成に失敗しました。", "E")
            GoTo Exit_HatD_Insert
        End If

        'HatD_Dat追加
        strSQL = "INSERT INTO HatD_Dat "
        strSQL = strSQL & "(HatD_001,HatD_002,HatD_003,HatD_004,HatD_005"
        strSQL = strSQL & ",HatD_006,HatD_007,HatD_008,HatD_009,HatD_010"
        strSQL = strSQL & ",HatD_011,HatD_012,HatD_013,HatD_0131,HatD_0132,HatD_014,HatD_015"
        strSQL = strSQL & ",HatD_016,HatD_017,HatD_018,HatD_019,HatD_020"
        strSQL = strSQL & ",HatD_021,HatD_022,HatD_031"
        strSQL = strSQL & ",HatD_Insert,HatD_WsNo"
        strSQL = strSQL & ") VALUES ("
        strSQL = strSQL & "'" & loRec.Rows(0)("HatD_001") & "'"                              '1   契約№
        strSQL = strSQL & "," & loRec.Rows(0)("HatD_002")                                   '2   契約№
        strSQL = strSQL & ", " & loRec.Rows(0)("HatD_003")                                    '3   項目
        strSQL = strSQL & "," & iSEQ                                                '4   発注SEQ
        strSQL = strSQL & ",'" & NCnvN(loRec.Rows(0)("HatD_005")) & "'"                      '5   発注番号
        strSQL = strSQL & ",NULL"                                                   '6   仕入先コード
        strSQL = strSQL & ",NULL"                                                   '7   仕入先名
        strSQL = strSQL & ",NULL"                                                   '8   発注日付
        strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(loRec.Rows(0)("HatD_009")) & "'"            '9   品名
        strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(loRec.Rows(0)("HatD_010")) & "'"            '10  品番

        strSQL = strSQL & "," & 0                                                   '11  発注数量
        strSQL = strSQL & ",'" & NCnvN(loRec.Rows(0)("HatD_012")) & "'"                      '12  単位
        strSQL = strSQL & "," & CDbl(NCnvZ(loRec.Rows(0)("HatD_013")))                         '13  単価
        strSQL = strSQL & ", " & CDbl(NCnvZ(loRec.Rows(0)("HatD_0131")))                          '131 掛率
        strSQL = strSQL & "," & CDbl(NCnvZ(loRec.Rows(0)("HatD_0132")))                         '132 単価NET
        strSQL = strSQL & ", " & 0                                                   '14  販売金額       'INSERT 2016/03/16 AOKI
        strSQL = strSQL & ",'" & NCnvN(loRec.Rows(0)("HatD_015")) & "'"                      '15  入荷予定日

        strSQL = strSQL & ",NULL"                                                   '16  入荷日付
        strSQL = strSQL & ",NULL"                                                   '17  仕入数量
        strSQL = strSQL & ",NULL"                                                   '18  仕入単位
        strSQL = strSQL & ",NULL"                                                   '19  仕入単価
        strSQL = strSQL & ",NULL"                                                   '20  掛率
        strSQL = strSQL & ",NULL"                                                   '21  仕入単価NET
        strSQL = strSQL & ",NULL"                                                   '22  仕入金額
        strSQL = strSQL & ",NULL"                                                   '31  仕入日付

        strSQL = strSQL & ",'" & Now & "'"                                          '登録日
        strSQL = strSQL & ",'" & clswinApi.StaticWinApi.Wsnumber & "'"                                  'WsNo
        strSQL = strSQL & ") "

        DB.ExecuteSql(strSQL)

        HatD_Insert = True

Exit_HatD_Insert:
        On Error GoTo 0

        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub JyuDSir_Update()
    '**  機能    : 受注データ(JyuD_Dat) 仕入数量/仕入金額 更新
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function GetTanka(KEIYAKUNO As String, SEQ As Integer, Komoku As Integer) As Double

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号

        On Error GoTo Exit_JyuDSir_Update

        GetTanka = 0

        strSQL = "SELECT * FROM JyuD_Dat "
        strSQL = strSQL & " WHERE JyuD_001 ='" & KEIYAKUNO & "'"
        strSQL = strSQL & "   AND JyuD_003 = " & SEQ
        strSQL = strSQL & "   AND JyuD_004 = " & Komoku
        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        If loRec.Rows.Count > 0 Then
            GetTanka = CDbl(NCnvZ(loRec.Rows(0)("JyuD_020")))
        End If

        loRec = Nothing

Exit_JyuDSir_Update:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub CheckHatDDat()
    '**  機能    : 発注データの存在チェック
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function CheckHatDDat(Key1 As String, Key2 As Integer,
                              Key3 As Integer, Key4 As Integer) As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号

        On Error GoTo Exit_JyuDSir_Update

        CheckHatDDat = False

        strSQL = "SELECT * FROM HatD_Dat "
        strSQL = strSQL & " WHERE HatD_001 ='" & Key1 & "'"
        strSQL = strSQL & "   AND HatD_002 = " & Key2
        strSQL = strSQL & "   AND HatD_003 = " & Key3
        strSQL = strSQL & "   AND HatD_004 = " & Key4

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        If loRec.Rows.Count > 0 Then
            CheckHatDDat = True
        End If

        loRec = Nothing

Exit_JyuDSir_Update:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub JyuDSir_Update()
    '**  機能    : 受注データ(JyuD_Dat) 仕入数量/仕入金額 更新
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function JyuDSir_Update() As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim SirSuWk As Long
        Dim curWk As Long
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号

        On Error GoTo Exit_JyuDSir_Update


        For i = LBound(JyuD_Upd) To UBound(JyuD_Upd)
            With JyuD_Upd(i)
                strSQL = "SELECT * FROM JyuD_Dat "
                strSQL = strSQL & " WHERE JyuD_001='" & .JyuD_001 & "'"
                strSQL = strSQL & "   AND JyuD_003 = " & .JyuD_003
                strSQL = strSQL & "   AND LEFT(JyuD_004,LEN(JyuD_004)-1) = " & Left(.JyuD_004, Len(CStr(.JyuD_004)) - 1)
                strSQL = strSQL & " ORDER BY JyuD_001,JyuD_003,JyuD_004"

                SirSuWk = .JyuD_024   '仕入数量

                If DB Is Nothing Then DB = New clsDB.DBService
                loRec = DB.GetDatafromDB(strSQL)

                '同項目内で仕入数量を割り当てて行く
                For Each dr As DataRow In loRec.Rows

                    If CDbl(NCnvZ(dr("JyuD_022"))) > SirSuWk Then
                        curWk = SirSuWk
                    Else
                        curWk = CDbl(NCnvZ(dr("JyuD_022")))
                    End If

                    strSQL = "UPDATE JyuD_Dat "
                    strSQL = strSQL & " SET JyuD_024  = " & curWk               '仕入数量
                    strSQL = strSQL & "   , JyuD_0241 = JyuD_020 * " & curWk    '仕入金額
                    strSQL = strSQL & ", JyuD_025 = " & .JyuD_025           '仕入日付
                    strSQL = strSQL & ", JyuD_Update ='" & Now & "'"
                    strSQL = strSQL & " WHERE JyuD_001='" & dr("JyuD_001") & "'"
                    strSQL = strSQL & "   AND JyuD_003 = " & dr("JyuD_003")
                    strSQL = strSQL & "   And JyuD_004 = " & dr("JyuD_004")

                    If DB Is Nothing Then DB = New clsDB.DBService
                    If DB.ExecuteSql(strSQL, strMsg) = False Then
                        GoTo Exit_JyuDSir_Update
                    End If
                    '''                End If

                    SirSuWk = SirSuWk - CDbl(NCnvZ(dr("JyuD_024")))     '仕入数量


                Next

            End With


            loRec = Nothing

        Next i

        JyuDSir_Update = True

Exit_JyuDSir_Update:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : JyuDUri_Update()
    '**  機能    : 受注データ売上情報更新処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function JyuDUri_Update(KEIYAKUNO As String, SEQ As Integer) As Boolean

        Dim strSQL As String     'SQL文作成用
        Dim loRec As New DataTable
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号
        Dim lbKomoku As Integer    '項目ブレークキー
        Dim lcSuryo As Double   '数量ワーク
        Dim lcKingaku As Double   '金額ワーク
        Dim lsKeiyakuNo As String     '契約№
        Dim liSEQ As Integer    'SEQ
        Dim lsDate As String     '日付ワーク

        JyuDUri_Update = False

        'セッション開始
        Dim TransactionOptions As New TransactionOptions()
        TransactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
        TransactionOptions.Timeout = TimeSpan.MaxValue
        Using transcope As New TransactionScope(TransactionScopeOption.Required, TransactionOptions)

            Try



                strSQL = "SELECT * FROM HatD_Dat "
                strSQL = strSQL & " WHERE HatD_001 ='" & KEIYAKUNO & "'"
                strSQL = strSQL & "   AND HatD_002 = " & SEQ
                '    strSQL = strSQL & "   AND HatD_003 < 1000 "                        'DELETE 2014/05/30 AOKI
                strSQL = strSQL & "   AND HatD_003 < 10000 "                        'INSERT 2014/05/30 AOKI
                strSQL = strSQL & " ORDER BY HatD_003,HatD_016"

                If DB Is Nothing Then DB = New clsDB.DBService
                loRec = DB.GetDatafromDB(strSQL)


                lbKomoku = CDbl(NCnvZ(loRec("HatD_003")))
                lcSuryo = 0
                lcKingaku = 0
                For Each dr As DataRow In loRec.Rows



                    If lbKomoku <> CDbl(NCnvZ(dr("HatD_003"))) Then
                        strSQL = ""
                        strSQL = strSQL & "UPDATE JyuD_Dat Set "
                        strSQL = strSQL & "    JyuD_026 = " & lcSuryo
                        strSQL = strSQL & "   , JyuD_0262 = JyuD_020"
                        strSQL = strSQL & "   , JyuD_0261 = JyuD_020 * " & lcSuryo
                        strSQL = strSQL & ", JyuD_027 ='" & lsDate & "'"
                        strSQL = strSQL & ",JyuD_Update ='" & Now & "'"
                        strSQL = strSQL & " WHERE JyuD_001 ='" & lsKeiyakuNo & "'"
                        strSQL = strSQL & "   AND JyuD_003 = " & liSEQ
                        strSQL = strSQL & "   AND JyuD_004 = " & lbKomoku

                        'SQL文実行
                        If DB Is Nothing Then DB = New clsDB.DBService
                        DB.ExecuteSql(strSQL)
                        lbKomoku = CDbl(NCnvZ(dr("HatD_003")))
                        lcSuryo = 0
                        lcKingaku = 0
                    End If
                    '        lcSuryo = lcSuryo +CDbl(NCnvZ(loRec![HatD_023"))
                    '        lcKingaku = lcKingaku +CDbl(NCnvZ(loRec![HatD_025"))
                    '仕入数量・金額
                    lcSuryo = lcSuryo + CDbl(NCnvZ(dr("HatD_017")))
                    lcKingaku = lcKingaku + CDbl(NCnvZ(dr("HatD_022")))

                    lsKeiyakuNo = NCnvN(dr("HatD_001"))
                    liSEQ = CDbl(NCnvZ(dr("HatD_002")))
                    lsDate = NCnvN(dr("HatD_026"))
                Next

                strSQL = "UPDATE JyuD_Dat SET "
                strSQL = strSQL & "    JyuD_026 = " & lcSuryo
                strSQL = strSQL & "   , JyuD_0262 = JyuD_020"
                strSQL = strSQL & "   , JyuD_0261 = JyuD_020 * " & lcSuryo
                strSQL = strSQL & ", JyuD_027 ='" & lsDate & "'"
                strSQL = strSQL & ",JyuD_Update ='" & Now & "'"
                strSQL = strSQL & " WHERE JyuD_001 ='" & lsKeiyakuNo & "'"
                strSQL = strSQL & "   AND JyuD_003 = " & liSEQ
                strSQL = strSQL & "   AND JyuD_004 = " & lbKomoku

                'SQL文実行
                DB.ExecuteSql(strSQL)


                transcope.Complete()
                transcope.Dispose()

                loRec = Nothing

                JyuDUri_Update = True

                Exit Function

            Catch ex As Exception
                transcope.Dispose()
                Call ksExpMsgBox(gstGUIDE_E002 & " ERR: " & ex.Message, "E")
            End Try

        End Using
    End Function

    '***************************************************************
    '**  名称    : DataDeleteProc()
    '**  機能    : データデリート処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function DataDeleteProc(vsKeiyakuNo As String, viSEQ As Integer, Optional viKou As Integer = 0, Optional viRen As Integer = 0) As Boolean

        Dim strSQL As String  'SQL文作成用
        Dim strMsg As String  'エラーメッセージ作成用
        Dim lngErr As Long    'エラー番号
        Dim Idx As Integer

        DataDeleteProc = False

        Dim TransactionOptions As New TransactionOptions()
        TransactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
        TransactionOptions.Timeout = TimeSpan.MaxValue
        Using transcope As New TransactionScope(TransactionScopeOption.Required, TransactionOptions)

            Try



                '削除用SQL文作成
                '========================================================
                strSQL = ""
                strSQL = strSQL & "DELETE FROM HatD_Dat "
                strSQL = strSQL & " WHERE HatD_001 ='" & modCommon.EditSQLAddSQuot(vsKeiyakuNo) & "'"

                If CDbl(NCnvZ(numSEQ.Value)) > 0 Then
                    strSQL = strSQL & "   AND HatD_002 = " & viSEQ
                End If
                '========================================================

                'SQL文実行
                If DB Is Nothing Then DB = New clsDB.DBService
                DB.ExecuteSql(strSQL)

                '受注SEQデータ発注数量/発注金額更新
                With JyuMDat(LBound(JyuMDat))
                    strSQL = ""
                    strSQL = strSQL & "UPDATE JyuM_Dat SET "
                    strSQL = strSQL & "    JyuM_017 = " & 0#
                    strSQL = strSQL & "   ,JyuM_018 = " & 0#
                    strSQL = strSQL & "   ,JyuM_019 = " & 0#
                    strSQL = strSQL & "   ,JyuM_020 = " & 0#
                    strSQL = strSQL & "   ,JyuM_021 = " & 0#
                    strSQL = strSQL & "   ,JyuM_022 = " & 0#
                    strSQL = strSQL & ",JyuM_Update ='" & Now & "'"
                    strSQL = strSQL & " WHERE JyuM_001 ='" & modCommon.EditSQLAddSQuot(vsKeiyakuNo) & "'"
                    If CDbl(NCnvZ(numSEQ.Value)) > 0 Then
                        strSQL = strSQL & "   AND JyuM_003 = " & viSEQ
                    End If
                End With

                'SQL文実行

                DB.ExecuteSql(strSQL)

                '受注明細データ発注数量発注日クリア
                With JyuMDat(LBound(JyuMDat))
                    strSQL = ""
                    strSQL = strSQL & "UPDATE JyuD_Dat SET "
                    strSQL = strSQL & "    JyuD_022  = " & 0
                    strSQL = strSQL & "   ,JyuD_023  =''"
                    strSQL = strSQL & "   ,JyuD_024  = " & 0
                    strSQL = strSQL & "   ,JyuD_0241 = " & 0
                    strSQL = strSQL & "   ,JyuD_025  =''"
                    strSQL = strSQL & "   ,JyuD_026  = " & 0
                    strSQL = strSQL & "   ,JyuD_0261 = " & 0
                    strSQL = strSQL & "   ,JyuD_0262 = " & 0
                    strSQL = strSQL & "   ,JyuD_027  =''"
                    strSQL = strSQL & "   ,JyuD_0271 =''"
                    strSQL = strSQL & "   ,JyuD_0272 =''"
                    strSQL = strSQL & "   ,JyuD_0273 =''"
                    strSQL = strSQL & "   ,JyuD_0274 =0"
                    strSQL = strSQL & "   ,JyuD_036  =NULL"
                    strSQL = strSQL & "   ,JyuD_028  = " & 0
                    strSQL = strSQL & "   ,JyuD_029  =''"
                    strSQL = strSQL & "   ,JyuD_0291 =0"
                    strSQL = strSQL & "   ,JyuD_0292 =0"
                    strSQL = strSQL & "   ,JyuD_037  =NULL"
                    strSQL = strSQL & ",JyuD_Update ='" & Now & "'"
                    strSQL = strSQL & " WHERE JyuD_001 ='" & modCommon.EditSQLAddSQuot(vsKeiyakuNo) & "'"
                    If CDbl(NCnvZ(numSEQ.Value)) > 0 Then
                        strSQL = strSQL & "   AND JyuD_003 = " & viSEQ
                    End If
                End With

                'SQL文実行
                DB.ExecuteSql(strSQL)

                '削除用SQL文作成
                '========================================================
                strSQL = ""
                strSQL = strSQL & "DELETE FROM KnpD_Dat "
                strSQL = strSQL & " WHERE KnpD_001 ='" & modCommon.EditSQLAddSQuot(vsKeiyakuNo) & "'"
                strSQL = strSQL & "   AND KnpD_002 = " & viSEQ
                '========================================================

                'SQL文実行
                DB.ExecuteSql(strSQL)

                'コミット
                transcope.Complete()
                transcope.Dispose()

                DataDeleteProc = True

                Exit Function



            Catch ex As Exception
                transcope.Dispose()
                Call ksExpMsgBox(gstGUIDE_E002 & " ERR: " & ex.Message, "E")
            End Try
        End Using

    End Function

    '***************************************************************
    '**  名称    : Sub CheckKonpo()
    '**  機能    : 梱包数量とのチェック
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function CheckKonpo(KEIYAKUNO As String,
                            SEQ As Integer,
                            Komoku As Integer) As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号
        Dim lcKnpSu As Double   '梱包数量
        Dim lcHatSu As Double   '発注数量
        Dim lcSirSu As Double   '仕入数量

        On Error GoTo Exit_CheckKonpo

        CheckKonpo = True

        '-- 梱包数量取得
        strSQL = "SELECT SUM(KnpD_009) AS KnpD_009"
        strSQL = strSQL & "  FROM KnpD_Dat "
        strSQL = strSQL & " WHERE KnpD_001 ='" & KEIYAKUNO & "'"
        strSQL = strSQL & "   AND KnpD_002 = " & SEQ
        strSQL = strSQL & "   AND KnpD_003 = " & Komoku
        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)


        If loRec.Rows.Count > 0 Then
            lcKnpSu = CDbl(NCnvZ(loRec.Rows(0)("KnpD_009"))) '梱包数量
        Else
            GoTo Exit_CheckKonpo
        End If

        loRec = Nothing

        '-- 発注/仕入数量取得
        strSQL = "  SELECT SUM(HatD_011) AS HatD_011,"
        strSQL = strSQL & "SUM(HatD_017) AS HatD_017"
        strSQL = strSQL & " FROM HatD_Dat "
        strSQL = strSQL & "WHERE HatD_001 ='" & KEIYAKUNO & "'"
        strSQL = strSQL & "  AND HatD_002 = " & SEQ
        strSQL = strSQL & "  AND HatD_003 = " & Komoku

        loRec = DB.GetDatafromDB(strSQL)

        If loRec.Rows.Count > 0 Then
            lcHatSu = CDbl(NCnvZ(loRec.Rows(0)("HatD_011"))) '発注数量
            lcSirSu = CDbl(NCnvZ(loRec.Rows(0)("HatD_017"))) '仕入数量
        End If

        loRec = Nothing

        '既に梱包されているデータ
        If lcHatSu = lcSirSu And lcSirSu = lcKnpSu Then
            CheckKonpo = False
        End If

Exit_CheckKonpo:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub GetKonpoSu()
    '**  機能    : 梱包数量の取得
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function GetKonpoSu(KEIYAKUNO As String,
                            SEQ As Integer,
                            Komoku As Integer) As Double

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号

        On Error GoTo Exit_GetKonpoSu

        GetKonpoSu = 0

        '-- 梱包数量取得
        strSQL = "SELECT SUM(KnpD_009) AS KnpD_009"
        strSQL = strSQL & "  FROM KnpD_Dat "
        strSQL = strSQL & " WHERE KnpD_001 ='" & KEIYAKUNO & "'"
        strSQL = strSQL & "   AND KnpD_002 = " & SEQ
        strSQL = strSQL & "   AND KnpD_003 = " & Komoku

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        If loRec.Rows.Count > 0 Then
            GetKonpoSu = CDbl(NCnvZ(loRec.Rows(0)("KnpD_009"))) '梱包数量
        End If

        loRec = Nothing

Exit_GetKonpoSu:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub GetLastSEQ()
    '**  機能    : 最終SEQの取得
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo  : 契約№
    '**  備考    :
    '***************************************************************
    Public Function GetLastSEQ(vsKeiyakuNo As String) As Double

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        GetLastSEQ = 0


        '受注データ(中ヘッダー(SEQ))情報読込み用SQL文作成
        '================================================================
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT COUNT(*) AS SqlCnt FROM JyuM_Dat"
        SQLtxt = SQLtxt & " WHERE JyuM_001 ='" & vsKeiyakuNo & "'"
        '================================================================

        '受注データ(中ヘッダー(SEQ))の読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(strSQL)



        'データ設定
        If loRSet.Rows.Count > 0 Then
            GetLastSEQ = CDbl(NCnvZ(loRSet.Rows(0)("SqlCnt")))
        End If
        'UPDATE 2014/10/02 AOKI E N D ----------------------------------------------------------------------------------------

Exit_GetLastSEQ:
        loRSet = Nothing
        Exit Function

    End Function

    '--- INSERT 2013/05/21 AOKI START ---------------------------------------------------------------------------
    '***************************************************************
    '**  名称    : Sub ZaiDDatReadProc()
    '**  機能    : 在庫データ読込み処理
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function ZaiDDatReadProc(vsKeiyakuNo As String) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        ZaiDDatReadProc = False

        '在庫データ読込み用SQL文作成
        '================================================================
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT * FROM ZaiD_Dat"
        SQLtxt = SQLtxt & " WHERE ZaiD_001k ='" & vsKeiyakuNo & "'"
        '================================================================

        '在庫データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(strSQL)

        If loRSet.Rows.Count > 0 Then
            ZaiDDatReadProc = True
        End If

        loRSet = Nothing

    End Function
    '--- INSERT 2013/05/21 AOKI E N D ---------------------------------------------------------------------------


    Public Function SeqMinGet(ByVal vsKeyNo As String, ByVal viMode2 As Integer) As Integer

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        SeqMinGet = 0

        On Error GoTo Err_SeqMinGet

        SQLtxt = "SELECT MIN(JyuM_003) AS JyuM_003 FROM JyuM_Dat "

        If viMode2 = 0 Then
            '見積No.
            SQLtxt = SQLtxt & " WHERE JyuM_002 = '" & vsKeyNo & "'"
        Else
            '受注No.
            SQLtxt = SQLtxt & " WHERE JyuM_001 = '" & vsKeyNo & "'"
        End If

        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(strSQL)

        If loRSet.Rows.Count > 0 Then
            SeqMinGet = CDbl(NCnvZ(loRSet.Rows(0)("JyuM_003")))
        End If

        Exit Function

Err_SeqMinGet:

    End Function


    Public Function SeqMaxGet(ByVal vsKeyNo As String, ByVal viMode2 As Integer) As Integer

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        SeqMaxGet = 0

        On Error GoTo Err_SeqMaxGet

        SQLtxt = "Select MAX(JyuM_003) AS JyuM_003 FROM JyuM_Dat "

        If viMode2 = 0 Then
            '見積No.
            SQLtxt = SQLtxt & " WHERE JyuM_002 = '" & vsKeyNo & "'"
        Else
            '受注No.
            SQLtxt = SQLtxt & " WHERE JyuM_001 = '" & vsKeyNo & "'"
        End If

        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(strSQL)

        If loRSet.Rows.Count > 0 Then
            SeqMaxGet = CDbl(NCnvZ(loRSet.Rows(0)("JyuM_003")))
        End If

        Exit Function

Err_SeqMaxGet:

    End Function








#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = winApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    winApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk + gstStrSEPFILE +
                     winApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next

            If xlApp IsNot Nothing Then
                xlApp.Quit()
                xlApp = Nothing
            End If
        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub

    Private Sub steSheetClear(ByVal vlSheet As Long, ByVal vsCell As String)
        xlApp.Worksheets(vlSheet).Range(vsCell).ClearContents()
    End Sub
#End Region
End Module
