﻿Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmURIMR084A
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtTitle = New System.Windows.Forms.Label()
        Me.lblEstimate = New System.Windows.Forms.Label()
        Me.lblSeikyu = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtEstimate = New System.Windows.Forms.TextBox()
        Me.txtSeikyu = New System.Windows.Forms.TextBox()
        Me.lbl_015 = New System.Windows.Forms.Label()
        Me.txtCaution1 = New System.Windows.Forms.Label()
        Me.txtCaution2 = New System.Windows.Forms.Label()
        Me.txtCaution3 = New System.Windows.Forms.Label()
        Me.datKeiYMD = New System.Windows.Forms.DateTimePicker()
        Me.datNyuYMD = New System.Windows.Forms.DateTimePicker()
        Me.groupBox4 = New System.Windows.Forms.GroupBox()
        Me.optDsp_1 = New System.Windows.Forms.RadioButton()
        Me.optDsp_0 = New System.Windows.Forms.RadioButton()
        Me.cmdFunc_12 = New System.Windows.Forms.Button()
        Me.lblGuide = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.spdMeisai = New System.Windows.Forms.DataGridView()
        Me.lbl_006 = New System.Windows.Forms.Label()
        Me.lbl_004 = New System.Windows.Forms.Label()
        Me.txtVessel = New System.Windows.Forms.TextBox()
        Me.txtTokName = New System.Windows.Forms.TextBox()
        Me.groupBox4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.spdMeisai, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtTitle
        '
        Me.txtTitle.BackColor = System.Drawing.Color.Blue
        Me.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.txtTitle.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTitle.ForeColor = System.Drawing.Color.White
        Me.txtTitle.Location = New System.Drawing.Point(4, -1)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(358, 34)
        Me.txtTitle.TabIndex = 63
        Me.txtTitle.Text = "受注/発注/仕入/梱包情報"
        Me.txtTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblEstimate
        '
        Me.lblEstimate.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblEstimate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblEstimate.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstimate.Location = New System.Drawing.Point(12, 84)
        Me.lblEstimate.Name = "lblEstimate"
        Me.lblEstimate.Size = New System.Drawing.Size(115, 23)
        Me.lblEstimate.TabIndex = 64
        Me.lblEstimate.Text = " 見積No."
        Me.lblEstimate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSeikyu
        '
        Me.lblSeikyu.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lblSeikyu.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSeikyu.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeikyu.Location = New System.Drawing.Point(12, 109)
        Me.lblSeikyu.Name = "lblSeikyu"
        Me.lblSeikyu.Size = New System.Drawing.Size(115, 23)
        Me.lblSeikyu.TabIndex = 65
        Me.lblSeikyu.Text = " 契約No."
        Me.lblSeikyu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 134)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 23)
        Me.Label1.TabIndex = 66
        Me.Label1.Text = " 契約日"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEstimate
        '
        Me.txtEstimate.BackColor = System.Drawing.SystemColors.Window
        Me.txtEstimate.Location = New System.Drawing.Point(129, 84)
        Me.txtEstimate.Name = "txtEstimate"
        Me.txtEstimate.ReadOnly = True
        Me.txtEstimate.Size = New System.Drawing.Size(159, 22)
        Me.txtEstimate.TabIndex = 10
        '
        'txtSeikyu
        '
        Me.txtSeikyu.BackColor = System.Drawing.SystemColors.Window
        Me.txtSeikyu.Location = New System.Drawing.Point(129, 109)
        Me.txtSeikyu.Name = "txtSeikyu"
        Me.txtSeikyu.ReadOnly = True
        Me.txtSeikyu.Size = New System.Drawing.Size(159, 22)
        Me.txtSeikyu.TabIndex = 20
        '
        'lbl_015
        '
        Me.lbl_015.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lbl_015.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl_015.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_015.Location = New System.Drawing.Point(293, 134)
        Me.lbl_015.Name = "lbl_015"
        Me.lbl_015.Size = New System.Drawing.Size(111, 23)
        Me.lbl_015.TabIndex = 70
        Me.lbl_015.Text = " 入荷予定日"
        Me.lbl_015.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCaution1
        '
        Me.txtCaution1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtCaution1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaution1.ForeColor = System.Drawing.Color.Red
        Me.txtCaution1.Location = New System.Drawing.Point(381, 2)
        Me.txtCaution1.Name = "txtCaution1"
        Me.txtCaution1.Size = New System.Drawing.Size(522, 21)
        Me.txtCaution1.TabIndex = 72
        Me.txtCaution1.Text = " "
        Me.txtCaution1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCaution2
        '
        Me.txtCaution2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtCaution2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaution2.ForeColor = System.Drawing.Color.Red
        Me.txtCaution2.Location = New System.Drawing.Point(381, 23)
        Me.txtCaution2.Name = "txtCaution2"
        Me.txtCaution2.Size = New System.Drawing.Size(522, 21)
        Me.txtCaution2.TabIndex = 73
        Me.txtCaution2.Text = " "
        Me.txtCaution2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtCaution3
        '
        Me.txtCaution3.BackColor = System.Drawing.SystemColors.ControlLight
        Me.txtCaution3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCaution3.ForeColor = System.Drawing.Color.Red
        Me.txtCaution3.Location = New System.Drawing.Point(381, 44)
        Me.txtCaution3.Name = "txtCaution3"
        Me.txtCaution3.Size = New System.Drawing.Size(522, 21)
        Me.txtCaution3.TabIndex = 74
        Me.txtCaution3.Text = " "
        Me.txtCaution3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'datKeiYMD
        '
        Me.datKeiYMD.CustomFormat = "平成yy年MM月dd日"
        Me.datKeiYMD.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.datKeiYMD.Font = New System.Drawing.Font("Tahoma", 9.25!)
        Me.datKeiYMD.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datKeiYMD.Location = New System.Drawing.Point(129, 134)
        Me.datKeiYMD.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.datKeiYMD.Name = "datKeiYMD"
        Me.datKeiYMD.Size = New System.Drawing.Size(159, 22)
        Me.datKeiYMD.TabIndex = 30
        '
        'datNyuYMD
        '
        Me.datNyuYMD.CustomFormat = "平成yy年MM月dd日"
        Me.datNyuYMD.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.datNyuYMD.Font = New System.Drawing.Font("Tahoma", 9.25!)
        Me.datNyuYMD.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datNyuYMD.Location = New System.Drawing.Point(408, 134)
        Me.datNyuYMD.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.datNyuYMD.Name = "datNyuYMD"
        Me.datNyuYMD.Size = New System.Drawing.Size(159, 22)
        Me.datNyuYMD.TabIndex = 60
        '
        'groupBox4
        '
        Me.groupBox4.Controls.Add(Me.optDsp_1)
        Me.groupBox4.Controls.Add(Me.optDsp_0)
        Me.groupBox4.Location = New System.Drawing.Point(681, 109)
        Me.groupBox4.Name = "groupBox4"
        Me.groupBox4.Size = New System.Drawing.Size(222, 48)
        Me.groupBox4.TabIndex = 77
        Me.groupBox4.TabStop = False
        Me.groupBox4.Text = "表示内容"
        '
        'optDsp_1
        '
        Me.optDsp_1.AutoSize = True
        Me.optDsp_1.Location = New System.Drawing.Point(151, 20)
        Me.optDsp_1.Name = "optDsp_1"
        Me.optDsp_1.Size = New System.Drawing.Size(56, 20)
        Me.optDsp_1.TabIndex = 1
        Me.optDsp_1.Text = "梱包"
        Me.optDsp_1.UseVisualStyleBackColor = True
        '
        'optDsp_0
        '
        Me.optDsp_0.AutoSize = True
        Me.optDsp_0.Location = New System.Drawing.Point(13, 20)
        Me.optDsp_0.Name = "optDsp_0"
        Me.optDsp_0.Size = New System.Drawing.Size(126, 20)
        Me.optDsp_0.TabIndex = 0
        Me.optDsp_0.Text = "受注/発注/仕入"
        Me.optDsp_0.UseVisualStyleBackColor = True
        '
        'cmdFunc_12
        '
        Me.cmdFunc_12.BackColor = System.Drawing.SystemColors.ControlDark
        Me.cmdFunc_12.Location = New System.Drawing.Point(953, 3)
        Me.cmdFunc_12.Name = "cmdFunc_12"
        Me.cmdFunc_12.Size = New System.Drawing.Size(89, 41)
        Me.cmdFunc_12.TabIndex = 78
        Me.cmdFunc_12.TabStop = False
        Me.cmdFunc_12.Text = "戻る(F12)"
        Me.cmdFunc_12.UseVisualStyleBackColor = False
        '
        'lblGuide
        '
        Me.lblGuide.BackColor = System.Drawing.Color.Black
        Me.lblGuide.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblGuide.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGuide.ForeColor = System.Drawing.Color.White
        Me.lblGuide.Location = New System.Drawing.Point(12, 572)
        Me.lblGuide.Name = "lblGuide"
        Me.lblGuide.Size = New System.Drawing.Size(1031, 32)
        Me.lblGuide.TabIndex = 80
        Me.lblGuide.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.spdMeisai)
        Me.Panel1.Location = New System.Drawing.Point(12, 166)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1030, 398)
        Me.Panel1.TabIndex = 81
        '
        'spdMeisai
        '
        Me.spdMeisai.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.LightCyan
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 9.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Brown
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.spdMeisai.DefaultCellStyle = DataGridViewCellStyle1
        Me.spdMeisai.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spdMeisai.Location = New System.Drawing.Point(0, 0)
        Me.spdMeisai.Name = "spdMeisai"
        Me.spdMeisai.Size = New System.Drawing.Size(1030, 398)
        Me.spdMeisai.TabIndex = 23
        '
        'lbl_006
        '
        Me.lbl_006.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lbl_006.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl_006.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.lbl_006.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_006.Location = New System.Drawing.Point(293, 108)
        Me.lbl_006.Name = "lbl_006"
        Me.lbl_006.Size = New System.Drawing.Size(111, 23)
        Me.lbl_006.TabIndex = 85
        Me.lbl_006.Text = "船名"
        Me.lbl_006.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lbl_004
        '
        Me.lbl_004.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.lbl_004.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbl_004.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.lbl_004.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_004.Location = New System.Drawing.Point(293, 84)
        Me.lbl_004.Name = "lbl_004"
        Me.lbl_004.Size = New System.Drawing.Size(111, 23)
        Me.lbl_004.TabIndex = 84
        Me.lbl_004.Text = "得意先名称"
        Me.lbl_004.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtVessel
        '
        Me.txtVessel.BackColor = System.Drawing.SystemColors.Window
        Me.txtVessel.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtVessel.Location = New System.Drawing.Point(408, 108)
        Me.txtVessel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtVessel.Name = "txtVessel"
        Me.txtVessel.ReadOnly = True
        Me.txtVessel.Size = New System.Drawing.Size(217, 22)
        Me.txtVessel.TabIndex = 50
        Me.txtVessel.TabStop = False
        '
        'txtTokName
        '
        Me.txtTokName.BackColor = System.Drawing.SystemColors.Window
        Me.txtTokName.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.txtTokName.Location = New System.Drawing.Point(408, 84)
        Me.txtTokName.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtTokName.Name = "txtTokName"
        Me.txtTokName.ReadOnly = True
        Me.txtTokName.Size = New System.Drawing.Size(217, 22)
        Me.txtTokName.TabIndex = 40
        Me.txtTokName.TabStop = False
        '
        'frmURIMR084A
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1051, 606)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtVessel)
        Me.Controls.Add(Me.txtTokName)
        Me.Controls.Add(Me.lbl_006)
        Me.Controls.Add(Me.lbl_004)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblGuide)
        Me.Controls.Add(Me.cmdFunc_12)
        Me.Controls.Add(Me.groupBox4)
        Me.Controls.Add(Me.datNyuYMD)
        Me.Controls.Add(Me.datKeiYMD)
        Me.Controls.Add(Me.txtCaution3)
        Me.Controls.Add(Me.txtCaution2)
        Me.Controls.Add(Me.txtCaution1)
        Me.Controls.Add(Me.lbl_015)
        Me.Controls.Add(Me.txtSeikyu)
        Me.Controls.Add(Me.txtEstimate)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblSeikyu)
        Me.Controls.Add(Me.lblEstimate)
        Me.Controls.Add(Me.txtTitle)
        Me.Font = New System.Drawing.Font("Tahoma", 9.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmURIMR084A"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "受注/発注/仕入"
        Me.groupBox4.ResumeLayout(False)
        Me.groupBox4.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.spdMeisai, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents txtTitle As Label
    Private WithEvents lblEstimate As Label
    Private WithEvents lblSeikyu As Label
    Private WithEvents Label1 As Label
    Private WithEvents txtEstimate As TextBox
    Private WithEvents txtSeikyu As TextBox
    Private WithEvents lbl_015 As Label
    Private WithEvents txtCaution1 As Label
    Private WithEvents txtCaution2 As Label
    Private WithEvents txtCaution3 As Label
    Private WithEvents datKeiYMD As DateTimePicker
    Private WithEvents datNyuYMD As DateTimePicker
    Private WithEvents groupBox4 As GroupBox
    Private WithEvents optDsp_1 As RadioButton
    Private WithEvents optDsp_0 As RadioButton
    Private WithEvents cmdFunc_12 As Button
    Private WithEvents lblGuide As Label
    Friend WithEvents Panel1 As Panel
    Private WithEvents spdMeisai As DataGridView
    Public WithEvents lbl_006 As Label
    Public WithEvents lbl_004 As Label
    Public WithEvents txtVessel As TextBox
    Public WithEvents txtTokName As TextBox
End Class
