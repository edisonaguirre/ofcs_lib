﻿Imports System.Windows.Forms
Imports globalcls.modHanbai
Imports clswinApi
Imports SAHANBAI.clsGlobal
'Imports Constant = SAHANBAI.Constant
Imports winApi = clswinApi.StaticWinApi
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports SAHANBAI.TaxComp
Imports System.Transactions
''all database transaction were put here , all control related
Public Module modJYUMR050
    '***************************************************************
    '**
    '**  プログラムＩＤ :  JYUMR041
    '**  機能           :  発注入力(modJYUMR041)
    '**  　　　         :
    '**  　　　         :
    '**  備考           :
    '**
    '***************************************************************
    'ﾌﾟﾛｸﾞﾗﾑID(仕入入力)
    Private DB As clsDB.DBService
    Private xlApp As Excel.Application
    '***************************************************************
    '**
    '**  プログラムＩＤ :  JYUMR050
    '**  機能           :  仕入入力(modJYUMR050)
    '**  　　　         :
    '**  　　　         :
    '**  備考           :
    '**
    '***************************************************************
    'ﾌﾟﾛｸﾞﾗﾑID(仕入入力)
    Public Const pstmPGID As String = "JYUMR050"

    Public JYUMR050_JyuHDat(0 To 0) As JyuHDatInfo  '受注データ(ヘッダー)

    Public JyuMDat() As JyuMDatInfo  '受注データ(中ヘッダー(SEQ))               'INSERT 2015/09/15 AOKI
    Public JyuDDat() As JyuDDatInfo  '受注データ(明細)
    Public HacSirD() As HatDDatInfo
    '受注データ(JyuD_Dat)更新用
    Public Structure JyuDDat_Upd
        Public JyuD_001 As String       '契約№
        Public JyuD_003 As Integer      '契約SEQ
        Public JyuD_004 As Integer      '項目
        Public JyuD_024 As Double     '仕入数量
        Public JyuD_0241 As Double     '仕入金額
        Public JyuD_025 As String       '仕入日付
    End Structure
    Public JyuD_Upd() As JyuDDat_Upd

    Public ConMst(0 To 0) As ConMstInfo     'コントロールマスタ情報

    Public Const JYUMR050_MSG002 As String = "前SEQのデータが存在しません。"
    Public Const JYUMR050_MSG003 As String = "次SEQのデータが存在しません。"

    Public strSQL As String

    'UPDATE 2019/05/21 AOKI START -----------------------------------------------------------------------------------------
    'INSERT 2015/09/15 AOKI START -----------------------------------------------------------------------------------------
    'スプレッドCOL
    Public Const spdcol_SirCd As Integer = 1      '仕入先コード
    Public Const spdcol_SirNm As Integer = 2      '仕入先名
    Public Const spdcol_Seq As Integer = 3      'SEQ
    Public Const spdcol_ItmNo1 As Integer = 4      '項目（Item No.表示用）
    Public Const spdcol_RenNo As Integer = 5      '連番
    Public Const spdcol_HinNm As Integer = 6      '品名
    Public Const spdcol_HinNo As Integer = 7      '品番
    Public Const spdcol_HSuryo As Integer = 8      '発注数量
    Public Const spdcol_HTani As Integer = 9      '発注単位
    Public Const spdcol_DT As Integer = 10     'D/T                    'INSERT 2019/05/21 AOKI
    Public Const spdcol_JTanka As Integer = 11     '受注単価
    Public Const spdcol_SSuryo As Integer = 12     '仕入数量
    Public Const spdcol_STani As Integer = 13     '仕入単位
    Public Const spdcol_STanka As Integer = 14     '仕入単価
    Public Const spdcol_SRitsu As Integer = 15     '掛率
    Public Const spdcol_SNet As Integer = 16     '仕入単価NET
    Public Const spdcol_SKingaku As Integer = 17     '仕入金額
    Public Const spdcol_SDate As Integer = 18     '仕入日付
    Public Const spdcol_KNo As Integer = 19     '契約№
    Public Const spdcol_HNo As Integer = 20     '発注番号（不要？）
    Public Const spdcol_ItmNo2 As Integer = 21     '項目（Item No.DB格納用）
    Public Const spdcol_Hantei As Integer = 22     '判定
    Public Const spdcol_KSuryo As Integer = 23     '梱包数量
    Public Const spdcol_HDate As Integer = 24     'ﾁｪｯｸ日付

    Public frmJYUMR050_txtKeiyakuNo As TextBox
    Public frmJYUMR050_numSEQ As NumericUpDown
    Public mForm_txtKeiyakuNo As String
    Public mForm_numSEQ As Integer





    '***************************************************************
    '**  名称    : Sub DataReadProc()
    '**  機能    : データ読込み処理
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo 契約№
    '**          : viSEQ       SEQ
    '**  備考    :
    '***************************************************************
    'Public Function DataReadProc(vsKeiyakuNo As String, viSeq As Integer) As Boolean       'DELETE 2015/09/15 AOKI
    Public Function DataReadProc(vsKeiyakuNo As String,
                             viSeq As Integer,
                             Optional viSirCd As Integer = 0) As Boolean                'INSERT 2015/09/15 AOKI

        Dim iCnt As Integer    'ループカウンタ

        DataReadProc = False

        '    Call OpenBoxCheck(vsKeiyakuNo)                                                     'INSERT 2019/06/21 AOKI 'DELETE 2019/07/29 AOKI

        '== 受注データ(ヘッダー)の読込み処理
        If JyuHDatReadProc(vsKeiyakuNo) = False Then
            Exit Function
        End If

        '== 受注データ(中ヘッダー(SEQ))の読込み処理
        'UPDATE 2015/09/15 AOKI START -----------------------------------------------------------------------------------------
        '    If JyuMDatReadProc((vsKeiyakuNo), viSeq) = False Then
        '    End If
        If viSeq > 0 Then
            '        If JyuMDatReadProc(vsKeiyakuNo, viSEQ) = False Then                            'DELETE 2015/09/15 AOKI
            If JyuMDatCheckProc(vsKeiyakuNo, viSeq) = False Then                            'INSERT 2015/09/15 AOKI
                Call ksExpMsgBox("SEQが違います。", "E")
                Exit Function
            End If
        End If
        'UPDATE 2015/09/15 AOKI E N D -----------------------------------------------------------------------------------------

        If JyuMDatReadProc((vsKeiyakuNo)) = False Then
        End If

        ''== 受注データ(明細)の読込み処理
        '    If JyuDDatReadProc(vsKeiyakuNo) = False Then
        '   End If

        '== 発注データの読込み処理
        '    If HatSirDatReadProc((vsKeiyakuNo), viSeq) = False Then                            'DELETE 2015/09/15 AOKI
        If HatSirDatReadProc((vsKeiyakuNo), viSeq, viSirCd) = False Then                    'INSERT 2015/09/15 AOKI
        End If

        DataReadProc = True

    End Function

    'INSERT 2016/03/29 AOKI START -----------------------------------------------------------------------------------------
    '***************************************************************
    '**  名称    : Sub DataReadProc2()
    '**  機能    : データ読込み処理
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo 契約№
    '**          : viSEQ       SEQ
    '**  備考    :
    '***************************************************************
    Public Function DataReadProc2(vsKeiyakuNo As String,
                             viSeq As Integer,
                             Optional viSirCd As Integer = 0) As Boolean

        Dim iCnt As Integer    'ループカウンタ

        DataReadProc2 = False

        '== 発注データの読込み処理
        If HatSirDatReadProc((vsKeiyakuNo), viSeq, viSirCd) = False Then
            Exit Function
        End If

        DataReadProc2 = True

    End Function
    'INSERT 2016/03/29 AOKI E N D -----------------------------------------------------------------------------------------

    'INSERT 2019/07/18 AOKI START -----------------------------------------------------------------------------------------
    '***************************************************************
    '**  名称    : Sub DataReadProc3()
    '**  機能    : データ読込み処理
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo 契約№
    '**          : viSEQ       SEQ
    '**  備考    :
    '***************************************************************
    Public Function DataReadProc3(vsKeiyakuNo As String) As String


        Dim SQLtxt As String
        Dim loRSet As New DataTable

        DataReadProc3 = vsKeiyakuNo

        On Error GoTo DataReadProc3_Err

        '発注/仕入データ情報読込み用SQL文作成
        '==========================================================================
        SQLtxt = "          SELECT TOP 1 HatD_001 "
        SQLtxt = SQLtxt & " FROM   HatD_Dat "
        SQLtxt = SQLtxt & " WHERE  HatD_001 LIKE '%" & vsKeiyakuNo & "'"
        SQLtxt = SQLtxt & " ORDER BY HatD_001 DESC "
        '==========================================================================

        '発注/仕入データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)

        If loRSet.Rows.Count > 0 Then
            DataReadProc3 = NCnvN(loRSet.Rows(0)("HatD_001"))
        End If


DataReadProc3_Err:
        loRSet = Nothing

    End Function
    'INSERT 2019/07/18 AOKI E N D -----------------------------------------------------------------------------------------

    Public Function JyuMDatCheckProc(vsKeiyakuNo As String, viSeq As Integer) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        JyuMDatCheckProc = False

        On Error GoTo JyuMDatCheckProc_Err

        '受注データ(中ヘッダー(SEQ))情報読込み用SQL文作成
        '================================================================
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT * FROM JyuM_Dat "
        SQLtxt = SQLtxt & " WHERE JyuM_001 ='" & vsKeiyakuNo & "'"
        SQLtxt = SQLtxt & "   AND JyuM_003 = " & viSeq

        '受注データ(中ヘッダー(SEQ))の読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)


        If loRSet.Rows.Count <= 0 Then
            GoTo JyuMDatCheckProc_Err
        End If

        JyuMDatCheckProc = True

JyuMDatCheckProc_Err:

        loRSet = Nothing

    End Function

    '***************************************************************
    '**  名称    : Sub JyuHDatReadProc()
    '**  機能    : 受注データ(ヘッダー)の読込み処理
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function JyuHDatReadProc(vsKeiyakuNo As String) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        JyuHDatReadProc = False

        Erase JYUMR050_JyuHDat '受注データ(ヘッダー)内部変数クリア
        ReDim Preserve JYUMR050_JyuHDat(0)
        '受注データ(ヘッダー)読込み用SQL文作成
        '================================================================
        With JYUMR050_JyuHDat(0)
            SQLtxt = ""
            SQLtxt = SQLtxt & " SELECT * FROM JyuH_Dat"
            SQLtxt = SQLtxt & " WHERE JyuH_001 ='" & vsKeiyakuNo & "'"

        End With
        '================================================================

        '受注データ(ヘッダー)の読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)

        If loRSet.Rows.Count > 0 Then
            With JYUMR050_JyuHDat(0)
                .JyuH_001 = NCnvN(loRSet.Rows(0)("JyuH_001"))      ' 契約№
                .JyuH_002 = NCnvN(loRSet.Rows(0)("JyuH_002"))      ' 契約日付
                .JyuH_003 = NCnvN(loRSet.Rows(0)("JyuH_003"))      ' 得意先名(CUSTOMER)
                .JyuH_0031 = NCnvZ(loRSet.Rows(0)("JyuH_0031"))    ' 得意先ｺｰﾄﾞ
                .JyuH_004 = NCnvN(loRSet.Rows(0)("JyuH_004"))      ' 得意先担当者(ATTN)
                .JyuH_005 = NCnvN(loRSet.Rows(0)("JyuH_005"))      ' 客先依頼No(YOUR NO)
                .JyuH_006 = NCnvN(loRSet.Rows(0)("JyuH_006"))      ' 船名(VESSEL NAME)
                .JyuH_007 = NCnvN(loRSet.Rows(0)("JyuH_007"))      ' 造船所
                .JyuH_008 = NCnvN(loRSet.Rows(0)("JyuH_008"))      ' 当社担当(OUR PERSON)
                .JyuH_009 = NCnvN(loRSet.Rows(0)("JyuH_009"))      ' 見積No(OUR REF NO)
                .JyuH_010 = NCnvN(loRSet.Rows(0)("JyuH_010"))      ' 見積日付
                .JyuH_011 = NCnvZ(loRSet.Rows(0)("JyuH_011"))      ' 見積時納期(DELIVERY TIME)
                .JyuH_012 = NCnvN(loRSet.Rows(0)("JyuH_012"))      ' ｿｰｽ(SOURCE)
                .JyuH_013 = NCnvN(loRSet.Rows(0)("JyuH_013"))      ' 国(CONTRY)
                .JyuH_014 = NCnvN(loRSet.Rows(0)("JyuH_014"))      ' 適用
                .JyuH_015 = NCnvZ(loRSet.Rows(0)("JyuH_015"))      ' 支払期間(Payment terms)
                .JyuH_016 = NCnvZ(loRSet.Rows(0)("JyuH_016"))      ' 見積有効期間
                .JyuH_017 = NCnvN(loRSet.Rows(0)("JyuH_017"))      ' 受)客先注文番号(YOUR REF NO)
                .JyuH_018 = NCnvN(loRSet.Rows(0)("JyuH_018"))      ' 受)入荷予定日
                .JyuH_019 = NCnvN(loRSet.Rows(0)("JyuH_019"))      ' 受)入荷場所
                .JyuH_020 = NCnvN(loRSet.Rows(0)("JyuH_020"))      ' 受)受渡場所
                .JyuH_021 = NCnvN(loRSet.Rows(0)("JyuH_021"))      ' 受)指定納期
                .JyuH_022 = NCnvN(loRSet.Rows(0)("JyuH_022"))      ' 住所関連項目) 1.オーナ
                .JyuH_023 = NCnvN(loRSet.Rows(0)("JyuH_023"))      ' 住所関連項目) 2.得意先
                .JyuH_024 = NCnvN(loRSet.Rows(0)("JyuH_024"))      ' 住所関連項目) 3.住所
                .JyuH_025 = NCnvN(loRSet.Rows(0)("JyuH_025"))      ' 住所関連項目) 4.電話他
                .JyuH_026 = NCnvN(loRSet.Rows(0)("JyuH_026"))      ' 住所関連項目) 5.備考
                .JyuH_027 = NCnvZ(loRSet.Rows(0)("JyuH_027"))      ' データ区分(0:見積ﾃﾞｰﾀ 1:受注ﾃﾞｰﾀ)
                .JyuH_028 = NCnvN(loRSet.Rows(0)("JyuH_028"))      ' 見積書発行日
                .JyuH_029 = NCnvN(loRSet.Rows(0)("JyuH_029"))      ' 注文請書発行日
                .JyuH_030 = NCnvN(loRSet.Rows(0)("JyuH_030"))      ' 契約台帳発行日
                .JyuH_031 = NCnvN(loRSet.Rows(0)("JyuH_031"))      ' 納品書発行日
                If .JyuH_027 <> 1 Then
                    Call ksExpMsgBox("この契約№はまだ「見積」段階です。受注入力を行って下さい。", "E")
                    GoTo Exit_JyuHDatReadProc
                End If
            End With
        Else
            GoTo Exit_JyuHDatReadProc
        End If

        JyuHDatReadProc = True

Exit_JyuHDatReadProc:
        loRSet = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub JyuMDatReadProc()
    '**  機能    : 受注データ(中ヘッダー(SEQ))情報の読込み処理
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo : 契約№
    '**          : viSEQ       : SEQ
    '**          : viMode      : 0 対象データ
    '**          :             : 1 前データ
    '**          :             : 2 次データ
    '**  備考    :
    '***************************************************************
    'Public Function JyuMDatReadProc(vsKeiyakuNo As String, _
    '                                viSEQ As Integer, _
    '                                Optional viMode As Integer = 0) As Boolean
    Public Function JyuMDatReadProc(vsKeiyakuNo As String) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim DCount As Integer

        JyuMDatReadProc = False



        Erase JyuMDat '受注データ(中ヘッダー(SEQ))内部変数クリア

        '受注データ(中ヘッダー(SEQ))情報読込み用SQL文作成
        '================================================================
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT * FROM JyuM_Dat"
        SQLtxt = SQLtxt & " WHERE JyuM_001 ='" & vsKeiyakuNo & "'"
        '================================================================

        '受注データ(中ヘッダー(SEQ))の読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)


        If loRSet.Rows.Count <= 0 Then
            GoTo Exit_JyuMDatReadProc
        End If

        DCount = 0

        'データ設定
        For Each dr As DataRow In loRSet.Rows

            ReDim Preserve JyuMDat(DCount)
            With JyuMDat(DCount)
                .JyuM_001 = NCnvN(dr("JyuM_001"))      ' 契約№
                .JyuM_002 = NCnvZ(dr("JyuM_002"))      ' 見積№
                .JyuM_003 = NCnvZ(dr("JyuM_003"))      ' SEQ
                .JyuM_004 = NCnvN(dr("JyuM_004"))      ' UNIT
                .JyuM_005 = NCnvN(dr("JyuM_005"))      ' MAKER
                .JyuM_006 = NCnvN(dr("JyuM_006"))      ' TYPE
                .JyuM_007 = NCnvN(dr("JyuM_007"))      ' SER/NO
                .JyuM_008 = NCnvN(dr("JyuM_008"))      ' DWG/NO
                .JyuM_009 = NCnvN(dr("JyuM_009"))      ' COMMENT1
                .JyuM_010 = NCnvN(dr("JyuM_010"))      ' COMMENT2
                .JyuM_011 = NCnvN(dr("JyuM_011"))      ' COMMENT3
                .JyuM_012 = NCnvN(dr("JyuM_012"))      ' COMMENT4
                .JyuM_0121 = NCnvN(dr("JyuM_0121"))    ' COMMENT5
                .JyuM_013 = NCnvZ(dr("JyuM_013"))      ' 見積数量
                .JyuM_014 = NCnvZ(dr("JyuM_014"))      ' 見積金額
                .JyuM_015 = NCnvZ(dr("JyuM_015"))      ' 受注数量
                .JyuM_016 = NCnvZ(dr("JyuM_016"))      ' 受注金額
                .JyuM_017 = NCnvZ(dr("JyuM_017"))      ' 発注数量
                .JyuM_018 = NCnvZ(dr("JyuM_018"))      ' 発注金額
                .JyuM_019 = NCnvZ(dr("JyuM_019"))      ' 仕入数量
                .JyuM_020 = NCnvZ(dr("JyuM_020"))      ' 仕入金額
                .JyuM_021 = NCnvZ(dr("JyuM_021"))      ' 売上数量
                .JyuM_022 = NCnvZ(dr("JyuM_022"))      ' 売上金額
                .JyuM_023 = NCnvN(dr("JyuM_023"))      ' 請求日付
            End With

            DCount = DCount + 1
        Next

        JyuMDatReadProc = True

Exit_JyuMDatReadProc:
        loRSet = Nothing
        Exit Function

    End Function



    '***************************************************************
    '**  名称    : Sub HatSirDatReadProc()
    '**  機能    : 発注/仕入データ情報の読込み処理
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo : 契約№
    '**          : viSEQ       : SEQ
    '**  備考    :
    '***************************************************************
    'Public Function HatSirDatReadProc(vsKeiyakuNo As String, viSeq As Integer) As Boolean      'DELETE 2015/09/15 AOKI
    Public Function HatSirDatReadProc(vsKeiyakuNo As String,
                                  viSeq As Integer,
                                  Optional viSirCd As Integer = 0) As Boolean               'INSERT 2015/09/15 AOKI

        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim DCount As Integer

        HatSirDatReadProc = False

        Erase HacSirD '発注/仕入データ内部変数クリア

        '発注/仕入データ情報読込み用SQL文作成
        '================================================================
        'UPDATE 2019/05/21 AOKI START -----------------------------------------------------------------------------------------
        '    SQLtxt = "  SELECT * FROM HatD_Dat"

        SQLtxt = "  SELECT HatD_Dat.*, JyuD_042 FROM HatD_Dat"
        SQLtxt = SQLtxt & " LEFT JOIN JyuD_Dat "
        SQLtxt = SQLtxt & " ON  HatD_001 = JyuD_001 "
        SQLtxt = SQLtxt & " AND HatD_002 = JyuD_003 "
        SQLtxt = SQLtxt & " AND HatD_003 = JyuD_004 "

        If UCase(Mid(vsKeiyakuNo, 3, 1)) <> "K" Then                        'INSERT 2016/03/31 AOKI

            SQLtxt = SQLtxt & " WHERE HatD_032 ='" & vsKeiyakuNo & "'"
            SQLtxt = SQLtxt & " AND   (HatD_006 = 728 OR "
            '        SQLtxt = SQLtxt & "        HatD_006 = 802)   "                 'DELETE 2017/07/13 AOKI
            SQLtxt = SQLtxt & "        HatD_006 = 802 OR "                  'INSERT 2017/07/13 AOKI
            SQLtxt = SQLtxt & "        HatD_006 = 377 OR "                  'INSERT 2017/07/13 AOKI
            SQLtxt = SQLtxt & "        HatD_006 = 461)   "                  'INSERT 2017/07/13 AOKI
            SQLtxt = SQLtxt & " ORDER BY HatD_001,HatD_002,HatD_003,HatD_004"

        Else

            SQLtxt = SQLtxt & " WHERE HatD_001 ='" & vsKeiyakuNo & "'"

            If viSeq > 0 Then
                SQLtxt = SQLtxt & " AND HatD_002 = " & viSeq
            End If

            If viSirCd > 0 Then
                SQLtxt = SQLtxt & " AND HatD_006 = " & viSirCd
            End If

            SQLtxt = SQLtxt & " ORDER BY HatD_002,HatD_003,HatD_004"

        End If
        'UPDATE 2016/03/29 AOKI E N D -----------------------------------------------------------------------------------------

        '================================================================

        '発注/仕入データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)


        If loRSet.Rows.Count <= 0 Then
            GoTo Exit_HatSirDatReadProc
        End If

        DCount = 0

        'データ設定
        For Each dr As DataRow In loRSet.Rows

            ReDim Preserve HacSirD(DCount)
            With HacSirD(DCount)
                .HatD_001 = NCnvN(dr("HatD_001"))        '契約№
                .HatD_002 = NCnvZ(dr("HatD_002"))        '契約SEQ
                .HatD_003 = NCnvZ(dr("HatD_003"))        '項目
                .HatD_004 = NCnvZ(dr("HatD_004"))        '発注SEQ
                .HatD_005 = NCnvN(dr("HatD_005"))        '発注番号
                .HatD_006 = NCnvZ(dr("HatD_006"))        '仕入先コード
                .HatD_007 = NCnvN(dr("HatD_007"))        '仕入先名称
                .HatD_008 = NCnvN(dr("HatD_008"))        '発注日付
                .HatD_009 = NCnvN(dr("HatD_009"))        '品名
                .HatD_010 = NCnvN(dr("HatD_010"))        '品番
                .HatD_011 = NCnvZ(dr("HatD_011"))        '発注数量
                .HatD_012 = NCnvN(dr("HatD_012"))        '発注単位
                .HatD_013 = NCnvZ(dr("HatD_013"))        '発注単価
                .HatD_0131 = NCnvZ(dr("HatD_0131"))      '発注掛率
                .HatD_0132 = NCnvZ(dr("HatD_0132"))      '発注単価NET
                .HatD_014 = NCnvZ(dr("HatD_014"))        '発注金額
                .HatD_015 = NCnvN(dr("HatD_015"))        '入荷予定日
                .HatD_016 = NCnvN(dr("HatD_016"))        '入荷日
                .HatD_017 = NCnvZ(dr("HatD_017"))        '仕入数量
                .HatD_018 = NCnvN(dr("HatD_018"))        '仕入単位
                .HatD_019 = NCnvZ(dr("HatD_019"))        '仕入単価
                .HatD_020 = NCnvZ(dr("HatD_020"))        '掛率
                .HatD_021 = NCnvZ(dr("HatD_021"))        '仕入単価NET
                .HatD_022 = NCnvZ(dr("HatD_022"))        '仕入金額
                .HatD_030 = NCnvZ(dr("HatD_030"))        '仕入完了チェック
                .HatD_031 = NCnvN(dr("HatD_031"))        '仕入日付
                .HatD_032 = NCnvN(dr("HatD_032"))        'Yanmar No.        
                .HatD_DT = NCnvZ(dr("JyuD_042"))         'D/T    
            End With

            DCount = DCount + 1
        Next

        HatSirDatReadProc = True

Exit_HatSirDatReadProc:
        loRSet = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : DataUpdateProc()
    '**  機能    : 発注データ更新処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function DataUpdateProc() As Boolean

        Dim strSQL As String     'SQL文作成用
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号
        Dim i As Integer    'ループカウンタ
        Dim lvWork As Object    '
        Dim ldWork As Date       '
        Dim lcTanka As Double   '単価
        Dim cSuryo As Double   '数量ワーク(JyuM_Dat更新用）
        Dim cKingaku As Double   '金額ワーク(JyuM_Dat更新用）
        Dim uKingaku As Double   '金額ワーク(JyuM_Dat更新用）

        'INSERT 2015/09/15 AOKI START -----------------------------------------------------------------------------------------
        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim loRSet2 As New DataTable
        Dim loRSet3 As New DataTable          'INSERT 2016/03/29 AOKI
        Dim DCount As Integer
        'INSERT 2015/09/15 AOKI E N D -----------------------------------------------------------------------------------------

        DataUpdateProc = False

        'セッション開始
        Dim TransactionOptions As New TransactionOptions()
        TransactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
        TransactionOptions.Timeout = TimeSpan.MaxValue

        Using transcope As New TransactionScope(TransactionScopeOption.Required, TransactionOptions)


            Try

                '=================
                ' 発注データ更新
                '=================

                For i = LBound(HacSirD) To UBound(HacSirD)

                    With HacSirD(i)



                        lcTanka = 0

                        '            If .HatD_003 < 1000 Then                                       'DELETE 2014/05/30 AOKI
                        If .HatD_003 < 10000 Then                                       'INSERT 2014/05/30 AOKI
                            '発注数量＝仕入数量かどうか
                            If .HatD_017 = 0 Or .HatD_011 = .HatD_017 Then
                                '受注単価取得
                                lcTanka = GetTanka(.HatD_001, .HatD_002, .HatD_003)
                                '仕入情報更新
                                strSQL = "UPDATE HatD_Dat "
                                strSQL = strSQL & " SET HatD_016 = " & .HatD_016           '仕入日付
                                strSQL = strSQL & "   , HatD_017 = " & .HatD_017           '仕入数量
                                strSQL = strSQL & "   , HatD_018 ='" & .HatD_018 & "'"     '仕入単位
                                strSQL = strSQL & "   , HatD_019 = " & .HatD_019           '仕入単価
                                strSQL = strSQL & "   , HatD_020 = " & .HatD_020           '掛率
                                strSQL = strSQL & "   , HatD_021 = " & .HatD_021           '仕入単価NET
                                strSQL = strSQL & "   , HatD_022 = " & .HatD_022           '仕入金額
                                strSQL = strSQL & "   , HatD_023 = " & .HatD_017           '売上数量(仕入数量)
                                strSQL = strSQL & "   , HatD_024 = " & lcTanka             '売上単価(受注単価)
                                strSQL = strSQL & "   , HatD_025 = " & .HatD_017 * lcTanka '売上金額(仕入数量*受注単価)
                                strSQL = strSQL & "   , HatD_026 = " & .HatD_016           '入荷日付
                                '''                    strSQL = strSQL & "   , HatD_026 = " & .HatD_031           '入荷日を売上日に
                                strSQL = strSQL & "   , HatD_030 = " & .HatD_030           '仕入完了チェック
                                strSQL = strSQL & "   , HatD_031 = " & .HatD_031           '仕入日付
                                strSQL = strSQL & "   , HatD_Update ='" & Now & "'"
                                strSQL = strSQL & " WHERE HatD_001 ='" & .HatD_001 & "'"
                                strSQL = strSQL & "   AND HatD_002 = " & .HatD_002
                                strSQL = strSQL & "   AND HatD_003 = " & .HatD_003
                                strSQL = strSQL & "   AND HatD_004 = " & .HatD_004
                                'SQL文実行
                                If DB Is Nothing Then DB = New clsDB.DBService
                                If DB.ExecuteSql(strSQL, strMsg) = False Then
                                    GoTo Error_DataUpdateProc
                                End If

                            Else
                                '発注残分（発注数量－仕入数量）を発注データとして追加する
                                If HatD_Insert(.HatD_001, .HatD_002, .HatD_003, .HatD_004, .HatD_011 - .HatD_017) = False Then
                                    'ロールバック
                                    GoTo Error_DataUpdateProc

                                End If



                                '受注単価取得
                                lcTanka = GetTanka(.HatD_001, .HatD_002, .HatD_003)

                                '発注数量＝仕入数量とする
                                strSQL = "UPDATE HatD_Dat "
                                strSQL = strSQL & " SET HatD_011 = " & .HatD_017
                                strSQL = strSQL & "   , HatD_014 = " & .HatD_017 & "* HatD_013 "
                                strSQL = strSQL & "   , HatD_016 = " & .HatD_016
                                strSQL = strSQL & "   , HatD_017 = " & .HatD_017
                                strSQL = strSQL & "   , HatD_018 ='" & .HatD_018 & "'"
                                strSQL = strSQL & "   , HatD_019 = " & .HatD_019
                                strSQL = strSQL & "   , HatD_020 = " & .HatD_020
                                strSQL = strSQL & "   , HatD_021 = " & .HatD_021
                                strSQL = strSQL & "   , HatD_022 = " & .HatD_022
                                strSQL = strSQL & "   , HatD_023 = " & .HatD_017           '売上数量(仕入数量)
                                strSQL = strSQL & "   , HatD_024 = " & lcTanka             '売上単価(受注単価)
                                strSQL = strSQL & "   , HatD_025 = " & .HatD_017 * lcTanka '売上金額(仕入数量*受注単価)
                                strSQL = strSQL & "   , HatD_026 = " & .HatD_016           '入荷日付
                                strSQL = strSQL & "   , HatD_030 = " & .HatD_030           '仕入完了チェック
                                strSQL = strSQL & "   , HatD_031 = " & .HatD_031
                                strSQL = strSQL & "   , HatD_Update ='" & Now & "'"
                                strSQL = strSQL & " WHERE HatD_001 ='" & .HatD_001 & "'"
                                strSQL = strSQL & "   AND HatD_002 = " & .HatD_002
                                strSQL = strSQL & "   AND HatD_003 = " & .HatD_003
                                strSQL = strSQL & "   AND HatD_004 = " & .HatD_004

                                'SQL文実行
                                If DB Is Nothing Then DB = New clsDB.DBService
                                If DB.ExecuteSql(strSQL, strMsg) = False Then

                                    GoTo Error_DataUpdateProc
                                End If


                            End If

                        Else

                            'INSERT 2016/03/29 AOKI START -----------------------------------------------------------------------------------------
                            '発注/仕入データ情報読込み用SQL文作成
                            '================================================================
                            SQLtxt = "          SELECT * "
                            SQLtxt = SQLtxt & " FROM   HatD_Dat "
                            SQLtxt = SQLtxt & " WHERE  HatD_001 ='" & .HatD_001 & "'"
                            SQLtxt = SQLtxt & " AND    HatD_002 = " & .HatD_002
                            SQLtxt = SQLtxt & " AND    HatD_003 = " & .HatD_003
                            SQLtxt = SQLtxt & " AND    HatD_004 = " & .HatD_004
                            '================================================================

                            '発注/仕入データの読込み
                            If DB Is Nothing Then DB = New clsDB.DBService
                            loRSet3 = DB.GetDatafromDB(SQLtxt)


                            If loRSet3.Rows.Count < 1 Then

                                strSQL = "INSERT INTO HatD_Dat "
                                strSQL = strSQL & "(HatD_001"
                                strSQL = strSQL & ",HatD_002"
                                strSQL = strSQL & ",HatD_003"
                                strSQL = strSQL & ",HatD_004"
                                strSQL = strSQL & ",HatD_006"
                                strSQL = strSQL & ",HatD_007"
                                strSQL = strSQL & ",HatD_009"
                                strSQL = strSQL & ",HatD_016"
                                strSQL = strSQL & ",HatD_017"
                                strSQL = strSQL & ",HatD_018"
                                strSQL = strSQL & ",HatD_019"
                                strSQL = strSQL & ",HatD_022"
                                strSQL = strSQL & ",HatD_030"
                                strSQL = strSQL & ",HatD_031"
                                strSQL = strSQL & ",HatD_032"                                           'INSERT 2016/03/29 AOKI
                                strSQL = strSQL & ",HatD_Insert"
                                strSQL = strSQL & ",HatD_WsNo"
                                strSQL = strSQL & ") VALUES ("
                                strSQL = strSQL & "'" & .HatD_001 & "'"      '1 契約№
                                strSQL = strSQL & "," & .HatD_002            '2 契約SEQ
                                strSQL = strSQL & "," & .HatD_003            '3 項目
                                strSQL = strSQL & "," & .HatD_004            '4 発注SEQ
                                strSQL = strSQL & "," & .HatD_006            '6 仕入先コード

                                'UPDATE 2013/07/22 AOKI START --------------------------------------------------------------------------------------
                                '                    strSQL = strSQL & ",'" & .HatD_007 & "'"     '7 仕入先名
                                '                    strSQL = strSQL & ",'" & .HatD_009 & "'"     '9 品名
                                strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.HatD_007) & "'"     '7 仕入先名
                                strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.HatD_009) & "'"     '9 品名
                                'UPDATE 2013/07/22 AOKI E N D --------------------------------------------------------------------------------------

                                strSQL = strSQL & "," & .HatD_016            '16 入荷日付
                                '''                    strSQL = strSQL & "," & .HatD_031            '16 仕入日付(入荷日を)
                                strSQL = strSQL & "," & .HatD_017            '17 仕入数量
                                strSQL = strSQL & ",'" & .HatD_018 & "'"     '18 仕入単位
                                strSQL = strSQL & "," & .HatD_019            '19 仕入単価
                                strSQL = strSQL & "," & .HatD_022            '22 仕入金額
                                strSQL = strSQL & "," & .HatD_030            '30 仕入完了チェック
                                strSQL = strSQL & "," & .HatD_031            '31 仕入日
                                strSQL = strSQL & ",'" & .HatD_032 & "'"     '32 Yanmar No.             'INSERT 2016/03/29 AOKI

                                strSQL = strSQL & ",'" & Now & "'"           '登録日
                                strSQL = strSQL & ",'" & clswinApi.StaticWinApi.Wsnumber & "'"   'WsNo
                                strSQL = strSQL & ") "

                                'INSERT 2016/03/29 AOKI START -----------------------------------------------------------------------------------------
                            Else

                                strSQL = "UPDATE HatD_Dat "

                                If NCnvZ(.HatD_006) > 0 Then
                                    strSQL = strSQL & " SET   HatD_006 = " & .HatD_006
                                    strSQL = strSQL & "   ,   HatD_007 ='" & .HatD_007 & "'"
                                    strSQL = strSQL & "   ,   HatD_016 = " & .HatD_016
                                Else
                                    strSQL = strSQL & " SET   HatD_016 = " & .HatD_016
                                End If

                                strSQL = strSQL & "   ,   HatD_017 = " & .HatD_017
                                strSQL = strSQL & "   ,   HatD_018 ='" & .HatD_018 & "'"
                                strSQL = strSQL & "   ,   HatD_019 = " & .HatD_019
                                strSQL = strSQL & "   ,   HatD_020 = " & .HatD_020
                                strSQL = strSQL & "   ,   HatD_021 = " & .HatD_021
                                strSQL = strSQL & "   ,   HatD_022 = " & .HatD_022
                                strSQL = strSQL & "   ,   HatD_023 = " & .HatD_017              '売上数量(仕入数量)
                                strSQL = strSQL & "   ,   HatD_024 = " & lcTanka                '売上単価(受注単価)
                                strSQL = strSQL & "   ,   HatD_025 = " & .HatD_017 * lcTanka    '売上金額(仕入数量*受注単価)
                                strSQL = strSQL & "   ,   HatD_026 = " & .HatD_016              '入荷日付
                                strSQL = strSQL & "   ,   HatD_030 = " & .HatD_030              '仕入完了チェック
                                strSQL = strSQL & "   ,   HatD_031 = " & .HatD_031
                                strSQL = strSQL & "   ,   HatD_Update ='" & Now & "'"
                                strSQL = strSQL & " WHERE HatD_001 ='" & .HatD_001 & "'"
                                strSQL = strSQL & "   AND HatD_002 = " & .HatD_002
                                strSQL = strSQL & "   AND HatD_003 = " & .HatD_003
                                strSQL = strSQL & "   AND HatD_004 = " & .HatD_004

                            End If


                            'SQL文実行
                            If DB Is Nothing Then DB = New clsDB.DBService
                            If DB.ExecuteSql(strSQL, strMsg) = False Then

                                GoTo Error_DataUpdateProc
                            End If



                        End If

                        '受注データ 仕入数量・金額・売上金額更新用
                        cSuryo = cSuryo + .HatD_017
                        cKingaku = cKingaku + .HatD_022
                        uKingaku = uKingaku + (.HatD_017 * lcTanka)

                    End With

                Next i


                SQLtxt = " SELECT             HatD_001 "                                '契約№
                SQLtxt = SQLtxt & "         , HatD_002 "                                'SEQ
                SQLtxt = SQLtxt & "         , HatD_003 "                                '項目
                SQLtxt = SQLtxt & "         , MAX(HatD_016) AS HatD_016 "               '仕入日
                SQLtxt = SQLtxt & "         , SUM(HatD_017) AS HatD_017 "               '仕入数量
                SQLtxt = SQLtxt & "         , SUM(HatD_022) AS HatD_022 "               '仕入金額
                SQLtxt = SQLtxt & "         , MAX(HatD_026) AS HatD_026 "               '売上日
                SQLtxt = SQLtxt & " FROM      HatD_Dat "

                'UPDATE 2016/03/29 AOKI START -----------------------------------------------------------------------------------------
                '    SQLtxt = SQLtxt & " WHERE     HatD_001 ='" & HacSirD(0).HatD_001 & "'"
                '    If UCase(Left(frmJYUMR050.txtKeiyakuNo.Text, 2)) = "YM" Then               'DELETE 2016/03/31 AOKI
                If UCase(Mid(frmJYUMR050_txtKeiyakuNo.Text, 3, 1)) <> "K" Then              'INSERT 2016/03/31 AOKI
                    SQLtxt = SQLtxt & " WHERE     HatD_032 ='" & UCase(frmJYUMR050_txtKeiyakuNo.Text) & "'"
                Else
                    SQLtxt = SQLtxt & " WHERE     HatD_001 ='" & HacSirD(0).HatD_001 & "'"
                End If

                SQLtxt = SQLtxt & " AND       HatD_003 < 10000 "
                SQLtxt = SQLtxt & " GROUP BY  HatD_001 "
                SQLtxt = SQLtxt & "         , HatD_002 "
                SQLtxt = SQLtxt & "         , HatD_003 "
                SQLtxt = SQLtxt & " ORDER BY  HatD_001 "
                SQLtxt = SQLtxt & "         , HatD_002 "
                SQLtxt = SQLtxt & "         , HatD_003 "

                '発注/仕入データの読込み
                If DB Is Nothing Then DB = New clsDB.DBService
                loRSet2 = DB.GetDatafromDB(SQLtxt)


                If loRSet2.Rows.Count < 1 Then
                    GoTo Error_DataUpdateProc
                End If

                For Each dr As DataRow In loRSet2.Rows



                    SQLtxt = " UPDATE              JyuD_Dat "
                    SQLtxt = SQLtxt & " SET        JyuD_024    = " & NCnvZ(dr("HatD_017"))
                    SQLtxt = SQLtxt & "          , JyuD_0241   = " & NCnvZ(dr("HatD_022"))
                    SQLtxt = SQLtxt & "          , JyuD_025    ='" & NCnvN(dr("HatD_016")) & "'"
                    SQLtxt = SQLtxt & "          , JyuD_026    = " & NCnvZ(dr("HatD_017"))
                    SQLtxt = SQLtxt & "          , JyuD_0262   = JyuD_020"
                    SQLtxt = SQLtxt & "          , JyuD_0261   = JyuD_020 * " & NCnvZ(dr("HatD_017"))
                    SQLtxt = SQLtxt & "          , JyuD_027    ='" & NCnvN(dr("HatD_026")) & "'"
                    SQLtxt = SQLtxt & "          , JyuD_Update ='" & Now & "'"
                    SQLtxt = SQLtxt & "          , JyuD_WsNo   ='" & clswinApi.StaticWinApi.Wsnumber & "'"
                    SQLtxt = SQLtxt & " WHERE      JyuD_001 ='" & NCnvN(dr("HatD_001")) & "'"
                    SQLtxt = SQLtxt & " AND        JyuD_003 = " & NCnvZ(dr("HatD_002"))
                    SQLtxt = SQLtxt & " AND        JyuD_004 = " & NCnvZ(dr("HatD_003"))

                    'SQL文実行
                    If DB Is Nothing Then DB = New clsDB.DBService
                    If DB.ExecuteSql(SQLtxt, strMsg) = False Then

                        GoTo Error_DataUpdateProc
                    End If


                Next



                SQLtxt = "          SELECT     JyuD_001 "                           '契約№
                SQLtxt = SQLtxt & "          , JyuD_003 "                           'SEQ
                SQLtxt = SQLtxt & "          , SUM(JH.HatD_017)   AS HatD_017 "     '仕入数量
                SQLtxt = SQLtxt & "          , SUM(JH.HatD_022)   AS HatD_022 "     '仕入金額
                SQLtxt = SQLtxt & "          , SUM(JH.JyuD_020)   AS JyuD_020 "     '売上金額

                SQLtxt = SQLtxt & " FROM "
                SQLtxt = SQLtxt & " ( "
                SQLtxt = SQLtxt & " SELECT     HatD_001 AS JyuD_001 "               '契約№
                SQLtxt = SQLtxt & "          , HatD_002 AS JyuD_003 "               'SEQ
                SQLtxt = SQLtxt & "          , HatD_017 "                           '仕入数量
                SQLtxt = SQLtxt & "          , HatD_022 "                           '仕入金額
                SQLtxt = SQLtxt & "          , (HatD_017 * CASE JyuD_020 WHEN NULL THEN 1 ELSE JyuD_020 END) AS JyuD_020 "  '売上金額
                SQLtxt = SQLtxt & " FROM       JyuD_Dat "
                SQLtxt = SQLtxt & " RIGHT JOIN (SELECT   HatD_Dat.HatD_001 "
                SQLtxt = SQLtxt & "                    , HatD_002 "
                SQLtxt = SQLtxt & "                    , HatD_003 "
                SQLtxt = SQLtxt & "                    , SUM(HatD_017) AS HatD_017 "
                SQLtxt = SQLtxt & "                    , SUM(HatD_022) AS HatD_022 "
                SQLtxt = SQLtxt & "             FROM     HatD_Dat "


                If UCase(Mid(frmJYUMR050_txtKeiyakuNo.Text, 3, 1)) <> "K" Then              'INSERT 2016/03/31 AOKI
                    SQLtxt = SQLtxt & "         INNER JOIN ( "
                    SQLtxt = SQLtxt & "                     SELECT   HatD_001 "
                    SQLtxt = SQLtxt & "                     FROM     HatD_Dat "
                    SQLtxt = SQLtxt & "                     WHERE    HAtD_032 = '" & UCase(frmJYUMR050_txtKeiyakuNo.Text) & "'"
                    SQLtxt = SQLtxt & "                     GROUP BY HatD_001 "
                    SQLtxt = SQLtxt & "                    ) AS YM "
                    SQLtxt = SQLtxt & "                 ON HatD_Dat.HatD_001 = YM.HatD_001"
                    SQLtxt = SQLtxt & "         WHERE    1 = 1 "
                Else
                    SQLtxt = SQLtxt & "         WHERE    HatD_001 ='" & HacSirD(0).HatD_001 & "'"
                End If

                SQLtxt = SQLtxt & "             AND      HatD_017 > 0 "
                SQLtxt = SQLtxt & "             GROUP BY HatD_Dat.HatD_001 "
                SQLtxt = SQLtxt & "                    , HatD_002 "
                SQLtxt = SQLtxt & "                    , HatD_003 "
                SQLtxt = SQLtxt & "            ) AS HD "

                SQLtxt = SQLtxt & " ON         JyuD_001 = HD.HatD_001 "
                SQLtxt = SQLtxt & " AND        JyuD_003 = HD.HatD_002 "
                SQLtxt = SQLtxt & " AND        JyuD_004 = HD.HatD_003 "
                SQLtxt = SQLtxt & " ) AS JH "

                SQLtxt = SQLtxt & " GROUP BY   JyuD_001 "
                SQLtxt = SQLtxt & "          , JyuD_003 "
                SQLtxt = SQLtxt & " ORDER BY   JyuD_001 "
                SQLtxt = SQLtxt & "          , JyuD_003 "
                'UPDATE 2016/03/29 AOKI E N D -----------------------------------------------------------------------------------------


                '発注/仕入データの読込み
                If DB Is Nothing Then DB = New clsDB.DBService
                loRSet = DB.GetDatafromDB(SQLtxt)


                If loRSet.Rows.Count < 1 Then
                    GoTo Error_DataUpdateProc
                End If
                For Each dr As DataRow In loRSet.Rows



                    strSQL = ""
                    strSQL = strSQL & "UPDATE JyuM_Dat SET "
                    strSQL = strSQL & "       JyuM_019 = " & NCnvZ(dr("HatD_017"))           '仕入数量
                    strSQL = strSQL & "     , JyuM_020 = " & NCnvZ(dr("HatD_022"))           '仕入金額
                    strSQL = strSQL & "     , JyuM_021 = " & NCnvZ(dr("HatD_017"))           '売上数量
                    strSQL = strSQL & "     , JyuM_022 = " & NCnvZ(dr("JyuD_020"))           '売上金額
                    strSQL = strSQL & "     , JyuM_Update ='" & Now & "'"
                    strSQL = strSQL & " WHERE JyuM_001 ='" & NCnvN(dr("JyuD_001")) & "'"
                    strSQL = strSQL & "   AND JyuM_003 = " & NCnvZ(dr("JyuD_003"))

                    'SQL文実行
                    If DB Is Nothing Then DB = New clsDB.DBService
                    If DB.ExecuteSql(strSQL, strMsg) = False Then

                        GoTo Error_DataUpdateProc
                    End If

                Next


                'コミット
                transcope.Complete()
                transcope.Dispose()
                'UPDATE 2015/09/15 AOKI E N D -----------------------------------------------------------------------------------------

                'キー項目セット
                '    If UCase(Left(frmJYUMR050.txtKeiyakuNo.Text, 2)) <> "YM" Then          'INSERT 2016/03/29 AOKI 'DELETE 2016/03/31 AOKI
                If UCase(Mid(frmJYUMR050_txtKeiyakuNo.Text, 3, 1)) = "K" Then           'INSERT 2016/03/31 AOKI
                    mForm_txtKeiyakuNo = HacSirD(0).HatD_001
                    mForm_numSEQ = HacSirD(0).HatD_002
                End If                                                                  'INSERT 2016/03/29 AOKI

                Erase HacSirD

                loRSet = Nothing
                loRSet2 = Nothing
                loRSet3 = Nothing               'INSERT 2016/03/29 AOKI

                DataUpdateProc = True
                Return DataUpdateProc

                Exit Function

            Catch ex As Exception
                transcope.Dispose()
                ksExpMsgBox(gstGUIDE_E002 & " ERR: " & ex.Message, "E")
                loRSet = Nothing
                loRSet2 = Nothing
                loRSet3 = Nothing

            End Try

Error_DataUpdateProc:
            DataUpdateProc = False
            transcope.Dispose()
            ksExpMsgBox(gstGUIDE_E002 & " ERR: DataUpdateProc ," + strMsg, "E")
            Return DataUpdateProc
        End Using
    End Function

    '***************************************************************
    '**  名称    : Sub HatD_Insert()
    '**  機能    : 発注残分（発注数量－仕入数量）を発注データとして追加する
    '**  戻り値  :
    '**  引数    : vKey1～4：更新キー　HSuu:発注数
    '**  備考    :
    '***************************************************************
    Private Function HatD_Insert(vKey1 As String, vKey2 As Integer, vKey3 As Integer, vKey4 As Integer,
                             HSuu As Long) As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim iSEQ As Integer

        On Error GoTo Exit_HatD_Insert

        'HatD_Dat読込み 発注SEQの最大値を取得
        strSQL = "SELECT MAX(HatD_004) AS HatD_004 FROM HatD_Dat "
        strSQL = strSQL & " WHERE HatD_001 ='" & vKey1 & "'"
        strSQL = strSQL & "   AND HatD_002 = " & vKey2
        strSQL = strSQL & "   AND HatD_003 = " & vKey3

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        If loRec.Rows.Count > 0 Then
            iSEQ = loRec.Rows(0)("HatD_004") + 1
        Else
            iSEQ = 1
        End If
        loRec = Nothing

        '発注データ(HatD_Dat)読込み用SQL文作成
        strSQL = "SELECT * FROM HatD_Dat"
        strSQL = strSQL & " WHERE HatD_001 ='" & vKey1 & "'"
        strSQL = strSQL & "   AND HatD_002 = " & vKey2
        strSQL = strSQL & "   AND HatD_003 = " & vKey3
        strSQL = strSQL & "   AND HatD_004 = " & vKey4

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        If loRec.Rows.Count <= 0 Then
            Call ksExpMsgBox("発注残分データの作成に失敗しました。", "E")
            GoTo Exit_HatD_Insert
        End If

        'HatD_Dat追加
        strSQL = "INSERT INTO HatD_Dat "
        strSQL = strSQL & "(HatD_001,HatD_002,HatD_003,HatD_004,HatD_005"
        strSQL = strSQL & ",HatD_006,HatD_007,HatD_008,HatD_009,HatD_010"
        strSQL = strSQL & ",HatD_011,HatD_012,HatD_013,HatD_0131,HatD_0132,HatD_014,HatD_015"
        strSQL = strSQL & ",HatD_016,HatD_017,HatD_018,HatD_019,HatD_020"
        '    strSQL = strSQL & ",HatD_021,HatD_022,HatD_031"                    'DELETE 2016/03/29 AOKI
        strSQL = strSQL & ",HatD_021,HatD_022,HatD_031,HatD_032"            'INSERT 2016/03/29 AOKI
        strSQL = strSQL & ",HatD_Insert,HatD_WsNo"
        strSQL = strSQL & ") VALUES ("
        strSQL = strSQL & "'" & loRec.Rows(0)("HatD_001") & "'"          '1 契約№
        strSQL = strSQL & "," & loRec.Rows(0)("HatD_002")                '2 契約№
        strSQL = strSQL & "," & loRec.Rows(0)("HatD_003")                '3 項目
        strSQL = strSQL & "," & iSEQ                            '4 発注SEQ
        strSQL = strSQL & ",'" & NCnvN(loRec.Rows(0)("HatD_005")) & "'"  '5 発注番号
        strSQL = strSQL & "," & NCnvZ(loRec.Rows(0)("HatD_006"))         '6 仕入先コード
        strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(loRec.Rows(0)("HatD_007")) & "'"  '7 仕入先名
        strSQL = strSQL & ",'" & NCnvN(loRec.Rows(0)("HatD_008")) & "'"  '8 発注日付
        strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(loRec.Rows(0)("HatD_009")) & "'"  '9 品名
        strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(loRec.Rows(0)("HatD_010")) & "'"  '10 品番

        strSQL = strSQL & "," & HSuu                            '11 発注数量
        strSQL = strSQL & ",'" & NCnvN(loRec.Rows(0)("HatD_012")) & "'"  '12 単位
        strSQL = strSQL & "," & NCnvZ(loRec.Rows(0)("HatD_013"))         '13 単価
        strSQL = strSQL & "," & NCnvZ(loRec.Rows(0)("HatD_0131"))        '131 掛率
        strSQL = strSQL & "," & NCnvZ(loRec.Rows(0)("HatD_0132"))        '131 単価NET
        strSQL = strSQL & "," & HSuu * NCnvZ(loRec.Rows(0)("HatD_0132"))    '21 販売金額
        strSQL = strSQL & ",'" & NCnvN(loRec.Rows(0)("HatD_015")) & "'"  '15 入荷予定日

        strSQL = strSQL & ",NULL"                               '16 入荷日付
        strSQL = strSQL & ",NULL"                               '17 仕入数量
        strSQL = strSQL & ",NULL"                               '18 仕入単位
        strSQL = strSQL & ",NULL"                               '19 仕入単価
        strSQL = strSQL & ",NULL"                               '20 掛率
        strSQL = strSQL & ",NULL"                               '21 仕入単価NET
        strSQL = strSQL & ",NULL"                               '22 仕入金額
        strSQL = strSQL & ",NULL"                               '31 仕入日付
        strSQL = strSQL & ",'" & NCnvN(loRec.Rows(0)("HatD_032")) & "'"  '32 Yanmar No.      'INSERT 2016/03/29 AOKI

        strSQL = strSQL & ",'" & Now & "'"                      '登録日
        strSQL = strSQL & ",'" & clswinApi.StaticWinApi.Wsnumber & "'"              'WsNo
        strSQL = strSQL & ") "

        If DB Is Nothing Then DB = New clsDB.DBService
        DB.ExecuteSql(strSQL)

        HatD_Insert = True
        Return HatD_Insert
Exit_HatD_Insert:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function


    '***************************************************************
    '**  名称    : Sub JyuDSir_Update()
    '**  機能    : 受注データ(JyuD_Dat) 仕入数量/仕入金額 更新
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function GetTanka(KEIYAKUNO As String, SEQ As Integer, Komoku As Integer) As Double

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号

        On Error GoTo Exit_JyuDSir_Update

        GetTanka = 0

        strSQL = "SELECT * FROM JyuD_Dat "
        strSQL = strSQL & " WHERE JyuD_001 ='" & KEIYAKUNO & "'"
        strSQL = strSQL & "   AND JyuD_003 = " & SEQ
        strSQL = strSQL & "   AND JyuD_004 = " & Komoku

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)


        If loRec.Rows.Count > 0 Then
            GetTanka = NCnvZ(loRec.Rows(0)("JyuD_020"))
        End If

        loRec = Nothing
        Return GetTanka

Exit_JyuDSir_Update:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub CheckHatDDat()
    '**  機能    : 発注データの存在チェック
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function CheckHatDDat(Key1 As String, Key2 As Integer,
                              Key3 As Integer, Key4 As Integer) As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号

        On Error GoTo Exit_JyuDSir_Update

        CheckHatDDat = False

        strSQL = "SELECT * FROM HatD_Dat "
        strSQL = strSQL & " WHERE HatD_001 ='" & Key1 & "'"
        strSQL = strSQL & "   AND HatD_002 = " & Key2
        strSQL = strSQL & "   AND HatD_003 = " & Key3
        strSQL = strSQL & "   AND HatD_004 = " & Key4

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)


        If loRec.Rows.Count > 0 Then
            CheckHatDDat = True
        End If

        loRec = Nothing
        Return CheckHatDDat

Exit_JyuDSir_Update:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub JyuDSir_Update()
    '**  機能    : 受注データ(JyuD_Dat) 仕入数量/仕入金額 更新
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function JyuDSir_Update() As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim SirSuWk As Long
        Dim curWk As Long
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号

        On Error GoTo Exit_JyuDSir_Update


        For i = LBound(JyuD_Upd) To UBound(JyuD_Upd)
            With JyuD_Upd(i)
                strSQL = "SELECT * FROM JyuD_Dat "
                strSQL = strSQL & " WHERE JyuD_001='" & .JyuD_001 & "'"
                strSQL = strSQL & "   AND JyuD_003 = " & .JyuD_003
                strSQL = strSQL & "   AND LEFT(JyuD_004,LEN(JyuD_004)-1) = " & Left(.JyuD_004, Len(CStr(.JyuD_004)) - 1)
                strSQL = strSQL & " ORDER BY JyuD_001,JyuD_003,JyuD_004"

                SirSuWk = .JyuD_024   '仕入数量

                If DB Is Nothing Then DB = New clsDB.DBService
                loRec = DB.GetDatafromDB(strSQL)


                '同項目内で仕入数量を割り当てて行く
                For Each dr As DataRow In loRec.Rows


                    If NCnvZ(dr("JyuD_022")) > SirSuWk Then
                        curWk = SirSuWk
                    Else
                        curWk = NCnvZ(dr("JyuD_022"))
                    End If

                    strSQL = "UPDATE JyuD_Dat "
                    strSQL = strSQL & " SET JyuD_024  = " & curWk               '仕入数量
                    strSQL = strSQL & "   , JyuD_0241 = JyuD_020 * " & curWk    '仕入金額
                    strSQL = strSQL & "   , JyuD_025  = " & .JyuD_025           '仕入日付
                    strSQL = strSQL & "   , JyuD_Update ='" & Now & "'"
                    strSQL = strSQL & " WHERE JyuD_001='" & dr("JyuD_001") & "'"
                    strSQL = strSQL & "   AND JyuD_003 = " & dr("JyuD_003")
                    strSQL = strSQL & "   AND JyuD_004 = " & dr("JyuD_004")
                    'SQL文実行

                    If DB Is Nothing Then DB = New clsDB.DBService
                    If DB.ExecuteSql(strSQL, strMsg) = False Then
                        GoTo Exit_JyuDSir_Update
                    End If


                    SirSuWk = SirSuWk - NCnvZ(dr("JyuD_024"))       '仕入数量

                Next

            End With

            loRec = Nothing

        Next i

        JyuDSir_Update = True

Exit_JyuDSir_Update:

        Call ksExpMsgBox(gstGUIDE_E002 & " ERR: " & strMsg, "E")
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : JyuDUri_Update()
    '**  機能    : 受注データ売上情報更新処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function JyuDUri_Update(KEIYAKUNO As String, SEQ As Integer) As Boolean

        Dim strSQL As String     'SQL文作成用
        Dim loRec As New DataTable
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号
        Dim lbKomoku As Integer    '項目ブレークキー
        Dim lcSuryo As Double   '数量ワーク
        Dim lcKingaku As Double   '金額ワーク
        Dim lsKeiyakuNo As String     '契約№
        Dim liSeq As Integer    'SEQ
        Dim lsDate As String     '日付ワーク

        JyuDUri_Update = False

        'セッション開始
        Dim TransactionOptions As New TransactionOptions()
        TransactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
        TransactionOptions.Timeout = TimeSpan.MaxValue

        Using transcope As New TransactionScope(TransactionScopeOption.Required, TransactionOptions)
            Try

                strSQL = "SELECT * FROM HatD_Dat "
                strSQL = strSQL & " WHERE HatD_001 ='" & KEIYAKUNO & "'"
                strSQL = strSQL & "   AND HatD_002 = " & SEQ
                '    strSQL = strSQL & "   AND HatD_003 < 1000 "                        'DELETE 2014/05/30 AOKI
                strSQL = strSQL & "   AND HatD_003 < 10000 "                        'INSERT 2014/05/30 AOKI
                strSQL = strSQL & " ORDER BY HatD_003,HatD_016"

                If DB Is Nothing Then DB = New clsDB.DBService
                loRec = DB.GetDatafromDB(strSQL)

                lbKomoku = NCnvZ(loRec.Rows(0)("HatD_003"))
                lcSuryo = 0
                lcKingaku = 0


                For Each dr As DataRow In loRec.Rows


                    If lbKomoku <> NCnvZ(dr("HatD_003")) Then
                        strSQL = ""
                        strSQL = strSQL & "UPDATE JyuD_Dat Set "
                        strSQL = strSQL & "    JyuD_026 = " & lcSuryo
                        strSQL = strSQL & "   , JyuD_0262 = JyuD_020"
                        strSQL = strSQL & "   , JyuD_0261 = JyuD_020 * " & lcSuryo
                        strSQL = strSQL & ", JyuD_027 ='" & lsDate & "'"
                        strSQL = strSQL & ",JyuD_Update ='" & Now & "'"
                        strSQL = strSQL & " WHERE JyuD_001 ='" & lsKeiyakuNo & "'"
                        strSQL = strSQL & "   AND JyuD_003 = " & liSeq
                        strSQL = strSQL & "   AND JyuD_004 = " & lbKomoku

                        'SQL文実行
                        If DB Is Nothing Then DB = New clsDB.DBService
                        If DB.ExecuteSql(strSQL, strMsg) = False Then
                            GoTo Error_JyuDUri_Update
                        End If

                        lbKomoku = NCnvZ(dr("HatD_003"))
                        lcSuryo = 0
                        lcKingaku = 0
                    End If

                    lcSuryo = lcSuryo + NCnvZ(dr("HatD_017"))
                    lcKingaku = lcKingaku + NCnvZ(dr("HatD_022"))

                    lsKeiyakuNo = NCnvN(dr("HatD_001"))
                    liSeq = NCnvZ(dr("HatD_002"))
                    lsDate = NCnvN(dr("HatD_026"))

                Next

                strSQL = "UPDATE JyuD_Dat Set "
                strSQL = strSQL & "    JyuD_026 = " & lcSuryo
                strSQL = strSQL & "   , JyuD_0262 = JyuD_020"
                strSQL = strSQL & "   , JyuD_0261 = JyuD_020 * " & lcSuryo
                strSQL = strSQL & ", JyuD_027 ='" & lsDate & "'"
                strSQL = strSQL & ",JyuD_Update ='" & Now & "'"
                strSQL = strSQL & " WHERE JyuD_001 ='" & lsKeiyakuNo & "'"
                strSQL = strSQL & "   AND JyuD_003 = " & liSeq
                strSQL = strSQL & "   AND JyuD_004 = " & lbKomoku

                'SQL文実行
                If DB Is Nothing Then DB = New clsDB.DBService
                If DB.ExecuteSql(strSQL, strMsg) = False Then

                    GoTo Error_JyuDUri_Update
                End If


                'コミット
                transcope.Complete()
                transcope.Dispose()

                loRec = Nothing

                JyuDUri_Update = True
                Return JyuDUri_Update
                Exit Function

            Catch ex As Exception
                transcope.Dispose()
                Call ksExpMsgBox(gstGUIDE_E002 & " ERR: " & ex.Message, "E")
            End Try

Error_JyuDUri_Update:
            '処理を戻す（ロールバック）
            transcope.Dispose()
            'エラーメッセージ表示
            Call ksExpMsgBox(gstGUIDE_E002 & " ERR: " & strMsg, "E")


        End Using

    End Function

    '***************************************************************
    '**  名称    : DataDeleteProc()
    '**  機能    : 発注データ削除処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function DataDeleteProc() As Boolean

        Dim strSQL As String  'SQL文作成用
        Dim strMsg As String  'エラーメッセージ作成用
        Dim lngErr As Long    'エラー番号
        Dim Idx As Integer
        Dim i As Integer  'ループカウンタ

        DataDeleteProc = False

        'セッション開始
        Dim TransactionOptions As New TransactionOptions()
        TransactionOptions.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted
        TransactionOptions.Timeout = TimeSpan.MaxValue

        Using transcope As New TransactionScope(TransactionScopeOption.Required, TransactionOptions)


            Try


                '発注データ更新
                For i = LBound(HacSirD) To UBound(HacSirD)
                    With HacSirD(i)

                        If .HatD_003 >= 10000 Then
                            strSQL = "DELETE FROM HatD_Dat "
                            strSQL = strSQL & " WHERE HatD_001 ='" & .HatD_001 & "'"
                            strSQL = strSQL & "   AND HatD_002 = " & .HatD_002
                            strSQL = strSQL & "   AND HatD_003 = " & .HatD_003
                            strSQL = strSQL & "   AND HatD_004 = " & .HatD_004
                        Else
                            strSQL = "UPDATE HatD_Dat "
                            strSQL = strSQL & " SET HatD_016 = NULL"
                            strSQL = strSQL & "   , HatD_017 = 0"
                            strSQL = strSQL & "   , HatD_018 = ''"
                            strSQL = strSQL & "   , HatD_019 = 0"
                            strSQL = strSQL & "   , HatD_020 = 0"
                            strSQL = strSQL & "   , HatD_021 = 0"
                            strSQL = strSQL & "   , HatD_022 = 0"
                            strSQL = strSQL & "   , HatD_030 = 0"
                            strSQL = strSQL & "   , HatD_031 = NULL"
                            strSQL = strSQL & "   , HatD_Update ='" & Now & "'"
                            strSQL = strSQL & " WHERE HatD_001 ='" & .HatD_001 & "'"
                            strSQL = strSQL & "   AND HatD_002 = " & .HatD_002
                            strSQL = strSQL & "   AND HatD_003 = " & .HatD_003
                            strSQL = strSQL & "   AND HatD_004 = " & .HatD_004
                        End If
                        'UPDATE 2016/03/29 AOKI E N D -----------------------------------------------------------------------------------------
                    End With

                    'SQL文実行
                    If DB Is Nothing Then DB = New clsDB.DBService
                    If DB.ExecuteSql(strSQL, strMsg) = False Then

                        GoTo Error_DataDeleteProc
                    End If
                Next i

                '===============================================
                ' 受注データ(JyuM_Dat) 仕入数量/仕入金額 更新
                '===============================================
                strSQL = ""
                strSQL = strSQL & "UPDATE JyuM_Dat SET "
                strSQL = strSQL & "    JyuM_019 = " & 0#
                strSQL = strSQL & "   ,JyuM_020 = " & 0#
                strSQL = strSQL & "   ,JyuM_021 = " & 0#
                strSQL = strSQL & "   ,JyuM_022 = " & 0#
                strSQL = strSQL & ",JyuM_Update ='" & Now & "'"
                strSQL = strSQL & " WHERE JyuM_001 ='" & HacSirD(0).HatD_001 & "'"
                If frmJYUMR050_numSEQ.Value > 0 Then
                    strSQL = strSQL & "   AND JyuM_003 = " & HacSirD(0).HatD_002
                End If


                'SQL文実行
                If DB Is Nothing Then DB = New clsDB.DBService
                If DB.ExecuteSql(strSQL, strMsg) = False Then

                    GoTo Error_DataDeleteProc
                End If

                '===============================================
                ' 受注データ(JyuD_Dat) 仕入数量/仕入金額 更新
                '===============================================
                For i = LBound(JyuD_Upd) To UBound(JyuD_Upd)
                    With JyuD_Upd(i)
                        strSQL = "UPDATE JyuD_Dat "
                        strSQL = strSQL & " SET JyuD_024  = " & 0#
                        strSQL = strSQL & "   , JyuD_0241 = " & 0#
                        strSQL = strSQL & "   , JyuD_025  = NULL"
                        strSQL = strSQL & "   , JyuD_026  = " & 0#
                        strSQL = strSQL & "   , JyuD_0262 = " & 0#
                        strSQL = strSQL & "   , JyuD_0261 = " & 0#
                        strSQL = strSQL & "   , JyuD_027  = NULL"
                        strSQL = strSQL & "   , JyuD_Update ='" & Now & "'"
                        strSQL = strSQL & " WHERE JyuD_001='" & .JyuD_001 & "'"
                        strSQL = strSQL & "   AND JyuD_003 = " & .JyuD_003
                        strSQL = strSQL & "   AND JyuD_004 = " & .JyuD_004
                    End With
                    'SQL文実行
                    If DB Is Nothing Then DB = New clsDB.DBService
                    If DB.ExecuteSql(strSQL, strMsg) = False Then

                        GoTo Error_DataDeleteProc
                    End If
                Next i

                'コミット
                transcope.Complete()
                transcope.Dispose()
                'キー項目セット
                '    If UCase(Left(frmJYUMR050.txtKeiyakuNo.Text, 2)) <> "YM" Then      'INSERT 2016/03/29 AOKI 'DELETE 2016/03/31 AOKI
                If UCase(Mid(frmJYUMR050_txtKeiyakuNo.Text, 3, 1)) = "K" Then       'INSERT 2016/03/31 AOKI
                    mForm_txtKeiyakuNo = HacSirD(0).HatD_001
                    mForm_numSEQ = HacSirD(0).HatD_002
                End If                                                              'INSERT 2016/03/29 AOKI

                Erase HacSirD

                DataDeleteProc = True
                Return DataDeleteProc
                Exit Function

            Catch ex As Exception
                transcope.Dispose()
                ksExpMsgBox(gstGUIDE_E002 & " ERR: " & ex.Message, "E")
            End Try
Error_DataDeleteProc:
            '処理を戻す（ロールバック）
            transcope.Dispose()
            'エラーメッセージ表示
            ksExpMsgBox(gstGUIDE_E002 & " ERR: " & strMsg, "E")
        End Using
    End Function

    '***************************************************************
    '**  名称    : Sub CheckKonpo()
    '**  機能    : 梱包数量とのチェック
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function CheckKonpo(KEIYAKUNO As String,
                            SEQ As Integer,
                            Komoku As Integer) As Boolean

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim i As Integer
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号
        Dim lcKnpSu As Double   '梱包数量
        Dim lcHatSu As Double   '発注数量
        Dim lcSirSu As Double   '仕入数量

        On Error GoTo Exit_CheckKonpo

        CheckKonpo = True

        '-- 梱包数量取得
        strSQL = "SELECT SUM(KnpD_009) AS KnpD_009"
        strSQL = strSQL & "  FROM KnpD_Dat "
        strSQL = strSQL & " WHERE KnpD_001 ='" & KEIYAKUNO & "'"
        strSQL = strSQL & "   AND KnpD_002 = " & SEQ
        strSQL = strSQL & "   AND KnpD_003 = " & Komoku

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)



        If loRec.Rows.Count > 0 Then
            lcKnpSu = NCnvZ(loRec.Rows(0)("KnpD_009"))  '梱包数量
        Else
            GoTo Exit_CheckKonpo
        End If

        loRec = Nothing

        '-- 発注/仕入数量取得
        strSQL = "  Select SUM(HatD_011) As HatD_011, "
        strSQL = strSQL & "SUM(HatD_017) As HatD_017"
        strSQL = strSQL & " FROM HatD_Dat "
        strSQL = strSQL & "WHERE HatD_001 ='" & KEIYAKUNO & "'"
        strSQL = strSQL & "  AND HatD_002 = " & SEQ
        strSQL = strSQL & "  AND HatD_003 = " & Komoku

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)


        If loRec.Rows.Count > 0 Then
            lcHatSu = NCnvZ(loRec.Rows(0)("HatD_011"))  '発注数量
            lcSirSu = NCnvZ(loRec.Rows(0)("HatD_017"))  '仕入数量
        End If

        loRec = Nothing

        '既に梱包されているデータ
        If lcHatSu = lcSirSu And lcSirSu = lcKnpSu Then
            CheckKonpo = False
        End If

Exit_CheckKonpo:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub GetKonpoSu()
    '**  機能    : 梱包数量の取得
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function GetKonpoSu(KEIYAKUNO As String,
                            SEQ As Integer,
                            Komoku As Integer) As Double

        Dim strSQL As String
        Dim loRec As New DataTable
        Dim strMsg As String     'エラーメッセージ作成用
        Dim lngErr As Long       'エラー番号

        On Error GoTo Exit_GetKonpoSu

        GetKonpoSu = 0

        '-- 梱包数量取得
        strSQL = "SELECT SUM(KnpD_009) AS KnpD_009"
        strSQL = strSQL & "  FROM KnpD_Dat "
        strSQL = strSQL & " WHERE KnpD_001 ='" & KEIYAKUNO & "'"
        strSQL = strSQL & "   AND KnpD_002 = " & SEQ
        strSQL = strSQL & "   AND KnpD_003 = " & Komoku

        If DB Is Nothing Then DB = New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)
        
        If loRec.Rows.Count > 0 Then
            GetKonpoSu = NCnvZ(loRec.Rows(0)("KnpD_009"))  '梱包数量
        End If

        loRec = Nothing

Exit_GetKonpoSu:
        On Error GoTo 0
        loRec = Nothing
        Exit Function

    End Function

    '***************************************************************
    '**  名称    : Sub GetLastSEQ()
    '**  機能    : 最終SEQの取得
    '**  戻り値  :
    '**  引数    : vsKeiyakuNo  : 契約№
    '**  備考    :
    '***************************************************************
    Public Function GetLastSEQ(vsKeiyakuNo As String) As Double

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        GetLastSEQ = 0


        '受注データ(中ヘッダー(SEQ))情報読込み用SQL文作成
        '================================================================
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT COUNT(*) AS SqlCnt FROM JyuM_Dat"
        SQLtxt = SQLtxt & " WHERE JyuM_001 ='" & vsKeiyakuNo & "'"
        '================================================================

        '受注データ(中ヘッダー(SEQ))の読込み

        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)


        'データ設定
        If loRSet.Rows.Count > 0 Then
            GetLastSEQ = NCnvZ(loRSet.Rows(0)("SqlCnt"))
        End If

        Return GetLastSEQ

Exit_GetLastSEQ:
        loRSet = Nothing
        Exit Function

    End Function

    '--- INSERT 2013/05/21 AOKI START ---------------------------------------------------------------------------
    '***************************************************************
    '**  名称    : Sub ZaiDDatReadProc()
    '**  機能    : 在庫データ読込み処理
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function ZaiDDatReadProc(vsKeiyakuNo As String) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable

        ZaiDDatReadProc = False

        '在庫データ読込み用SQL文作成
        '================================================================
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT * FROM ZaiD_Dat"
        SQLtxt = SQLtxt & " WHERE ZaiD_001k ='" & vsKeiyakuNo & "'"
        '================================================================

        '在庫データの読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)


        If loRSet.Rows.Count <= 0 Then
            ZaiDDatReadProc = True
        End If

        loRSet = Nothing
        Return ZaiDDatReadProc
    End Function
    '--- INSERT 2013/05/21 AOKI E N D ---------------------------------------------------------------------------

    'INSERT 2019/06/21 AOKI START -----------------------------------------------------------------------------------------
    '***************************************************************
    '**  名称    : Sub OpenBoxCheck()
    '**  機能    : オーダー状況問合せ画面を表示する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Sub OpenBoxCheck(vsKeiyakuNo As String)

        On Error GoTo OpenBoxCheck_ERR

        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim Tok_Cd As String


        '得意先コード取得用SQL
        SQLtxt = ""
        SQLtxt = SQLtxt & " SELECT  HatD_001 "
        SQLtxt = SQLtxt & "       , SUM(HatD_011) as HSuryo "
        SQLtxt = SQLtxt & "       , SUM(HatD_017) as SSuryo "
        SQLtxt = SQLtxt & " FROM    HatD_Dat "
        SQLtxt = SQLtxt & " WHERE   HatD_001 = '" & vsKeiyakuNo & "'"
        SQLtxt = SQLtxt & " AND     HatD_003 < 10000"
        SQLtxt = SQLtxt & " GROUP BY  HatD_001 "

        '受注データ(ヘッダー)の読込み
        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)

        If loRSet.Rows.Count > 0 Then
            If NCnvZ(loRSet.Rows(0)("SSuryo")) > 0 And NCnvZ(loRSet.Rows(0)("HSuryo")) <> NCnvZ(loRSet.Rows(0)("SSuryo")) Then
                ksExpMsgBox("仕入入力済みのアイテムがあります。" & vbCrLf & "梱包時入れ忘れに注意して下さい！！", "W")
            End If
        End If


        GoTo OpenBoxCheck_Exit


OpenBoxCheck_ERR:
        Call ksExpMsgBox("エラーが発生しました。", "E")

OpenBoxCheck_Exit:
        loRSet = Nothing

    End Sub
    'INSERT 2019/06/21 AOKI E N D -----------------------------------------------------------------------------------------







#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = winApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    winApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk + gstStrSEPFILE +
                     winApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next

            If xlApp IsNot Nothing Then
                xlApp.Quit()
                xlApp = Nothing
            End If
        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub

    Private Sub steSheetClear(ByVal vlSheet As Long, ByVal vsCell As String)
        xlApp.Worksheets(vlSheet).Range(vsCell).ClearContents()
    End Sub
#End Region
End Module
