﻿
Imports System.IO
Imports System.Windows.Forms
Imports SAHANBAI
Imports Excel = Microsoft.Office.Interop.Excel
Public Module prtOrderConfirmation


    Dim xlApp As Excel.Application
    Dim xlWs As Excel.Worksheet
    Dim xlRange As Excel.Range

    '***************************************************************
    '**
    '**  機能      :  Order Confirmation
    '**  作成日    :  2016/06/23
    '**  更新日    :
    '**  備考      :
    '**
    '***************************************************************
    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
    End Enum


    '注文請書印刷条件
    Public ourRefNoP() As String   '契約番号指定
    Public strHakkoDate As String   '注文請書発行日付

    '注文請書ヘッダ項目
    Public Structure JYUMR030Hed
        Public Country As String      '海外国内区分(0:国内 1:海外)
        Public Messrs As String      '客先名
        Public Attn As String      '客先担当者名
        Public YourRefNo As String      '客先依頼Ｎｏ
        Public YourOrderNo As String      '客先注文Ｎｏ
        Public VesselsName As String      '船名
        Public sDate As String      '日付
        Public TantoNm As String      '当社担当者
        Public OurEstimateNo As String  '見積Ｎｏ
    End Structure
    Private printHed(0 To 0) As JYUMR030Hed

    'プレビューからの見積発行日更新用キー'
    Private KeiyakuNoWk As String      '契約№
    Private RefNoWk As String      '見積№
    Public Structure JYUMR030Upd
        Public KEIYAKUNO As String      '契約№
        Public RefNo As String      '見積№
    End Structure
    Public JyuHUPDKey() As JYUMR030Upd

    'エクセル上のセル範囲
    Private pstLastRow As Integer           '最終行
    Private TotalAmount As Double           '合計金額

    '== 注文請書のExcel定義 ==
    Private Const psttREPORT_NAME As String = "JYUMR030_注文請書(ORDER CONFIRMATION)" 'Report名称
    Private Const pstPrintArea As String = "A17:AN66"          'シートクリア範囲
    Private Const pstPrevArea As String = "A1:AT66"           'プレビュー表示範囲     'UPDATE 2017/09/27 AOKI
    Private Const pstMaxRow As Integer = 45                 '最大行数
    Private Const pstStartRow As Integer = 18                 'ページ内の開始行
    Private Const pstPrintP As Integer = 1

    '印刷共通定義
    Private mCurMode As Integer     'カレント印刷モード
    Private mCurPage As Long        'カレントページ番号
    Private mCurRows As Long        'カレント行
    Private mCurCols As Long        'カレント列
    Private PageWk(1) As Integer     '見積№毎のページ印字用ワーク(シートのFROM-TO)

    '###2010/07/26 PDF出力用Xls名
    Public PDFXlsNm As String
    Public pstrPdfNm As String   'PDF出力用ファイル

    'Excel出力用
    Public xlBookTem As Excel.Workbook   'テンプレ用ブック
    Public xlBookNew As Excel.Workbook   'ワーク用ブック

    Public spdOdrConfir()
    Public Const spdItem As Integer = 2      'Item
    Public Const spdDesc As Integer = 4      'Description
    Public Const spdPNo As Integer = 18     'Part No
    Public Const spdQty As Integer = 25     'Q'ty
    Public Const spdUnit As Integer = 27     'Unit
    Public Const spdUPrc As Integer = 33     'Unit Price
    Public Const spdAmount As Integer = 39     'Amount
    Private DB As New clsDB.DBService



    '***************************************************************
    '**  名称    : Function JYUMR030PrintProc() As Boolean
    '**  機能    : 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function JYUMR030PrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim RecHed As New DataTable  '見積ヘッダデータ
        Dim RecMeisai As New DataTable  '見積明細データ
        Dim SeqBrk As Integer              'SEQブレークチェック用
        Dim iMHedCnt As Integer              '中ヘッダ印字カウント
        Dim iCnt As Integer              'カウント

        Try



            JYUMR030PrintProc = False

            mCurMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存
            pstLastRow = pstMaxRow + pstStartRow - 1

            RecHed = Nothing

            '===受注ヘッダデータの読込み用SQL文作成ルーチン
            strSQL = CreateSQLText(0)

            '===受注ヘッダデータの読込み
            RecHed = DB.GetDatafromDB(strSQL)


            '===データ存在チェック
            If RecHed.Rows.Count <= 0 Then
                'データ無しメッセージ
                Call ksExpMsgBox("印刷するデータが存在しません。", "I")
                GoTo Exit_JYUMR030PrintProc
            End If

            xlApp = CreateObject("Excel.Application")

            '===Excelファイルのオープン
            If steExcelOpen(psttREPORT_NAME & modHanbai.exXLSSuffix) = False Then
                'Excelファイル無しメッセージ
                Call ksExpMsgBox(psttREPORT_NAME & "のレポートが存在しません。", "E")
                GoTo Exit_JYUMR030PrintProc
            End If

            If mCurMode = enmPrintmode.Preview Then        'プレビューモード
                Call stePrevWorkDelete()  '古いワークファイルを削除する
            End If

            ''===進行状況表示フォーム設定=============================================
            'With frmStProgressBar
            '    .SetCaption = "注文請書(ORDER CONFIRMATION)"
            '    .SetTitle = "【 注文請書 】・・・作成中・・・"
            '    .SetPage = "ページ = " & 0
            '    .SetMin = 0
            '    .SetMax = RecHed.RecordCount
            '    .ShowStart
            'End With

            '===各変数初期化
            mCurPage = 1                  'ページ番号
            '    mCurRows = pstStartRow - 1    '明細印刷開始行              'DELETE 2016/06/23 AOKI
            DCount = 0                    'データカウンタ
            PageWk(0) = mCurPage          '契約№毎のページ印字用ワーク(シートのFROM)
            '###2010/07/26 PDF出力用Xls名
            PDFXlsNm = ""

            'シートのクリア
            Call loSheetClear()

            '指定された契約番号を１件ずつ読む
            For iCnt = LBound(ourRefNoP) To UBound(ourRefNoP)

                '進行状況用カウンタ
                DCount = DCount + 1

                '受注ヘッダデータの読込み
                RecHed = Nothing
                'SQL作成
                strSQL = CreateSQLText(1, ourRefNoP(iCnt))
                RecHed = DB.GetDatafromDB(strSQL)

                'ヘッダー情報を変数にセット
                Call sttSetHeader(RecHed)

                '２件目以降　改ページさせる
                If DCount > 1 Then
                    mCurRows = pstLastRow
                    Call CheckPages()
                    '契約№毎のページ印字用ワーク(シートのTO)
                    PageWk(1) = mCurPage - 1
                    'ヘッダー　見積№毎のページを出力、印刷時は印刷する。
                    Call sttWriteHeaderPage()
                    PageWk(0) = mCurPage
                End If

                'プレビューからの注文請書発行日更新用キー
                KeiyakuNoWk = modHanbai.NCnvN(RecHed.Rows(0)("JyuH_001"))       '契約№
                RefNoWk = modHanbai.NCnvN(RecHed.Rows(0)("JyuH_009"))           '見積№
                '１ページ目キー
                ReDim Preserve JyuHUPDKey(mCurPage)
                JyuHUPDKey(mCurPage).KEIYAKUNO = KeiyakuNoWk      '契約№
                JyuHUPDKey(mCurPage).RefNo = RefNoWk              '見積№

                '受注明細データの読込み用SQL作成
                strSQL = CreateSQLText(2, "", RecHed)

                '受注明細データの読込み
                RecMeisai = Nothing
                RecMeisai = DB.GetDatafromDB(strSQL)

                'クリア
                SeqBrk = -9999     'SEQブレークチェック用
                iMHedCnt = 0       '中ヘッダ印字カウント
                TotalAmount = 0    '金額合計

                'データ存在チェック
                If RecMeisai.Rows.Count > 0 Then

                    'ヘッダー情報をExcelに出力
                    Call sttWriteHeader()


                    '読込んだ明細データが無くなるまで繰返す
                    For Each dr As DataRow In RecMeisai.Rows

                        'SEQブレーク
                        If SeqBrk <> dr("JyuD_003") Then
                            '中ヘッダー出力
                            Call sttWriteMidHeader(RecMeisai, iMHedCnt)
                            '明細情報出力
                            Call sttWriteMeisai(dr)
                        Else
                            '明細情報出力
                            Call sttWriteMeisai(dr)
                        End If

                        SeqBrk = dr("JyuD_003")

                    Next


                End If

                'フッター出力
                Call sttWriteFooter(RecHed)

                '＜印刷時＞注文請書発行日を更新
                If viMode = enmPrintmode.PrintOut Then
                    Call JYUMR030_JyuHUpdate(RecHed.Rows(0)("JyuH_001"), RecHed.Rows(0)("JyuH_009"))
                End If

                '###2010/07/26 PDF出力用Xls名取得
                If PDFXlsNm = "" Then
                    Dim strChuNo As String


                    PDFXlsNm = modHanbai.NCnvN(RecHed.Rows(0)("JyuH_006")) & Space(1) & modHanbai.NCnvN(RecHed.Rows(0)("JyuH_017")) & "(" & modHanbai.NCnvN(RecHed.Rows(0)("JyuH_009")) & ")"

                    'ファイル名に使えない文字を-置き換える
                    PDFXlsNm = Replace(PDFXlsNm, "/", "-")
                    PDFXlsNm = Replace(PDFXlsNm, "\", "-")
                    PDFXlsNm = Replace(PDFXlsNm, ":", "-")
                    PDFXlsNm = Replace(PDFXlsNm, "*", "-")
                    PDFXlsNm = Replace(PDFXlsNm, "?", "-")
                    PDFXlsNm = Replace(PDFXlsNm, """", "-")
                    PDFXlsNm = Replace(PDFXlsNm, "<", "-")
                    PDFXlsNm = Replace(PDFXlsNm, ">", "-")
                    PDFXlsNm = Replace(PDFXlsNm, "|", "-")

                End If
                'UPDATE 2016/08/15 AOKI E N D -----------------------------------------------------------------------------------------

            Next iCnt




            '契約№毎のページ印字用ワーク(シートのTO)
            PageWk(1) = mCurPage
            'ヘッダー　契№毎のページを出力、印刷時は印刷する。
            Call sttWriteHeaderPage()


            'モードごとの処理
            Select Case viMode

                Case enmPrintmode.Preview      'プレビュー

                    Call stePrevBookSave()       'Bookの保存
                    'Call TimeWait(1)
                    Call steWorkbooksClose()     'Bookのクローズ
                    'Call TimeWait(1)

                    '###2010/07/26 PDF出力用Xls作成================================================
                    Dim strXlsNm As String
                    Dim strPdfPath As String

                    'PDF出力用ファイル保存用フォルダ
                    strPdfPath = Trim(clswinApi.StaticWinApi.WsReportDir) & "\PDF\"
                    If Dir(strPdfPath, vbDirectory) = "" Then
                        'Report\PDFフォルダが無ければ作成する
                        Call MkDir(strPdfPath)
                    Else
                        'Report\PDFフォルダ下のファイルを削除
                        Call File_Delete(strPdfPath)
                    End If

                    strXlsNm = Trim(clswinApi.StaticWinApi.WsReportDir) & "\" & "SPW" &gstStrSEPFILE &
                           clswinApi.StaticWinApi.Wsnumber & modHanbai.exXLSSuffix
                    pstrPdfNm = strPdfPath & PDFXlsNm & modHanbai.exXLSSuffix

                    Call FileCopy(strXlsNm, pstrPdfNm)
                    '###2010/07/26 PDF出力用Xls作成================================================



                    'プレビュー画面の起動
                    'TODO REMOVE COMMENT
                    Dim frm As New frmPrintPreview

                    With frm
                        .tagJYUMR030_JyuHUpdate = True
                        .ppvActCell = pstPrevArea
                        .ShowDialog()
                    End With

                Case enmPrintmode.PrintOut     '印刷
                    Call steWorkbooksClose()     'Bookのクローズ


            End Select

            JYUMR030PrintProc = True

Exit_JYUMR030PrintProc:

            RecHed = Nothing
            RecMeisai = Nothing
            Exit Function


        Catch ex As Exception
            Call ksExpMsgBox("Error in  JYUMR030PrintProc: " & ex.Message, "E")
            Call steWorkbooksClose()        'Bookのクローズ
        End Try


    End Function

    '********************************************************************
    '**  名称    : Function CreateSQLText() String
    '**  機能    : 受注データ読み込み用SQL作成処理
    '**  戻り値  : 作成されたSQL文字列
    '**  引数    : iMode 0:印刷データ確認 iMode 1:受注ヘッダ　2：受注明細
    '**          : vKey  受注ヘッダデータKEY(受注ヘッダ読込時使用）
    '**          : voRec 受注ヘッダデータ(明細データ読込時使用）
    '**  備考    :
    '********************************************************************
    Private Function CreateSQLText(iMode As Integer, Optional vKey As String = "", Optional voRec As Object = Nothing) As String

        Dim strSQL As String
        Dim strWhere As String
        Dim iWk As Integer

        Select Case iMode
            Case 0  '印刷データ確認
                strSQL = strSQL & "SELECT * FROM JyuH_Dat"
                strSQL = strSQL & " WHERE ("

                For iWk = LBound(ourRefNoP) To UBound(ourRefNoP)
                    If strWhere = "" Then
                        strWhere = " JyuH_001 ='" & ourRefNoP(iWk) & "'"
                    Else
                        strWhere = strWhere & " OR JyuH_001 ='" & ourRefNoP(iWk) & "'"
                    End If
                Next iWk

                strSQL = strSQL & strWhere
                strSQL = strSQL & ")"

            Case 1  'ヘッダデータ
                '受注番号指定
                strSQL = strSQL & "SELECT * FROM JyuH_Dat"
                strSQL = strSQL & " WHERE JyuH_001 ='" & vKey & "'"       '受注番号指定

            Case 2  '見積明細データ
                strSQL = strSQL & "SELECT * FROM JyuM_Dat TM,JyuD_Dat TD"
                strSQL = strSQL & " WHERE TM.JyuM_001 = '" & voRec.rows(0)("JyuH_001") & "'"   '契約№
                strSQL = strSQL & "   AND TD.JyuD_001 = TM.JyuM_001"                  '契約№
                strSQL = strSQL & "   AND TD.JyuD_003 = TM.JyuM_003"                  'SEQ
                strSQL = strSQL & " ORDER BY TM.JyuM_001,TM.JyuM_003,TD.JyuD_004  "
        End Select

        CreateSQLText = strSQL

    End Function

    '***************************************************************
    '**  名称    : Function GetTargetSheet() As Long
    '**  機能    : 出力モードとカレントページから対象シート番号を返す。
    '**  戻り値  : 対象となるExcelシート番号
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function GetTargetSheet() As Long

        'モードによりシート番号を設定
        GetTargetSheet = mCurPage
        '    Select Case mCurMode
        '        Case 0  'プレビュー
        '            GetTargetSheet = mCurPage
        '        Case 1  '印刷
        '            GetTargetSheet = pstPrintP
        '    End Select

    End Function

    '***************************************************************
    '**  名称    : Sub sttSetHeader
    '**  機能    : ヘッダー情報を変数にセットする。
    '**  戻り値  :
    '**  引数    : voRec:見積ヘッダデータ
    '**  備考    :
    '***************************************************************
    Private Sub sttSetHeader(voRec As DataTable)

        Erase printHed
        ReDim Preserve printHed(0)
        With printHed(0)
            .Country = CDbl(modHanbai.NCnvZ(voRec.Rows(0)("JyuH_007")))                    '海外/国内区分

            Select Case CDbl(modHanbai.NCnvZ(voRec.Rows(0)("JyuH_0031")))
                Case 204                                            'Ost-West-Handel und Schiffahrt GmbH
                    .Messrs = modHanbai.NCnvN(voRec.Rows(0)("JyuH_022"))              'オーナー名

                Case 117                                            'Five Stars Shipping Company Pvt. Ltd
                    If modHanbai.NCnvN(voRec.Rows(0)("JyuH_006")) = "KANAK PREM" Then  'M/V:KANAK PREM
                        .Messrs = modHanbai.NCnvN(voRec.Rows(0)("JyuH_022"))           'オーナー名
                    Else
                        .Messrs = modHanbai.NCnvN(voRec.Rows(0)("JyuH_003"))           '得意先名
                    End If


                Case 138                                            'ISM Ship Management Pte Ltd
                    Select Case modHanbai.NCnvN(voRec.Rows(0)("JyuH_006"))
'                    Case "QUEEN SAPPHIRE", "AFRICAN SETO", "OCEAN DIAMOND", "HANJIN SHIKOKU", "CHORUS", "GLOBAL SYMPHONY", "MARITIME UNITY", "ANSAC WYOMING", "HL SHIKOKU"      'UPDATE 2016/12/12 AOKI    'DELETE 2017/01/24 AOKI
                        Case "QUEEN SAPPHIRE", "AFRICAN SETO", "OCEAN DIAMOND", "HL SHIKOKU", "CHORUS", "GLOBAL SYMPHONY", "MARITIME UNITY", "ANSAC WYOMING", "ANSAC MOON BEAR"     'INSERT 2017/01/24 AOKI
                            .Messrs = "ISM Pte Ltd"
                        Case "BULK GUATEMALA", "PACIFIC ENDURANCE", "AOM JULIA", "NAVIOS PROSPERITY", "IKAN SELIGI", "KING ISLAND", "JUBILANT DREAM"                                'INSERT 2017/01/24 AOKI
                            .Messrs = "KISM Pte Ltd"                                                                                                                                'INSERT 2017/01/24 AOKI
                        Case Else
                            .Messrs = modHanbai.NCnvN(voRec.Rows(0)("JyuH_003"))       '得意先名
                    End Select

                Case 398                                    'WINSON SHIPPING (TAIWAN) CO., LTD
                    If modHanbai.NCnvN(voRec.Rows(0)("JyuH_022")) = "" Then
                        .Messrs = modHanbai.NCnvN(voRec.Rows(0)("JyuH_003"))  '得意先名
                    Else
                        .Messrs = modHanbai.NCnvN(voRec.Rows(0)("JyuH_022"))   'オーナー名
                    End If
                    'UPDATE 2019/03/15 AOKI E N D -----------------------------------------------------------------------------------------
                    'INSERT 2019/02/07 AOKI E N D -----------------------------------------------------------------------------------------

                Case Else
                    .Messrs = modHanbai.NCnvN(voRec.Rows(0)("JyuH_003"))               '得意先名
            End Select

            .Attn = modHanbai.NCnvN(voRec.Rows(0)("JyuH_004"))                         '客先担当者名
            .YourRefNo = modHanbai.NCnvN(voRec.Rows(0)("JyuH_005"))                    '客先依頼№
            .YourOrderNo = modHanbai.NCnvN(voRec.Rows(0)("JyuH_017"))                  '客先注文№
            .VesselsName = modHanbai.NCnvN(voRec.Rows(0)("JyuH_006"))                  '船名
            .sDate = modHanbai.NCnvN(voRec.Rows(0)("JyuH_002"))                         '日付

            Dim KbnMst(0) As KbnMstInfo

            KbnMst(0).Kbn_001 = 0
            KbnMst(0).Kbn_002 = "担当者"
            KbnMst(0).Kbn_003 = -1
            KbnMst(0).Kbn_004 = modHanbai.NCnvN(voRec.Rows(0)("JyuH_008"))
            KbnMst(0).Kbn_005 = ""

            If GetKbnMstInfo2(KbnMst(0)) = True Then
                .TantoNm = modHanbai.NCnvN(KbnMst(0).Kbn_005)                 '当社担当者
            Else
                .TantoNm = modHanbai.NCnvN(KbnMst(0).Kbn_004)
            End If

            .OurEstimateNo = modHanbai.NCnvN(voRec.Rows(0)("JyuH_009"))                '見積№
        End With

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteHeader
    '**  機能    : ヘッダー情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeader()
        xlWs = xlApp.Worksheets(GetTargetSheet)
        With xlWs

            If printHed(0).Country = "0" Then
                '国内
                .Cells(1, 21).Value = "注文請書"
            Else
                '海外
                .Cells(1, 21).Font.NAME = "Arial Black"
                .Cells(1, 21).Value = "ORDER CONFIRMATION"
            End If

            .Cells(8, 4).Value = printHed(0).Messrs                 '客先名

            mCurRows = 9
            If Len(printHed(0).YourRefNo) > 70 Then                 '客先依頼№
                .Cells(mCurRows, 6).Value = Mid(printHed(0).YourRefNo, 1, 50)
                mCurRows = mCurRows + 1
                .Cells(mCurRows, 1).Value = ""
                .Cells(mCurRows, 6).Value = Mid(printHed(0).YourRefNo, 51)
            Else
                .Cells(mCurRows, 6).Value = printHed(0).YourRefNo
            End If

            mCurRows = mCurRows + 1
            .Cells(mCurRows, 1).Value = "Your Order No:      "
            .Cells(mCurRows, 6).Value = printHed(0).YourOrderNo     '客先注文№
            mCurRows = mCurRows + 1
            .Cells(mCurRows, 1).Value = "Vessel's Name:"
            .Cells(mCurRows, 6).Value = printHed(0).VesselsName     '船名

            '日付
            If printHed(0).Country = "0" Then
                '国内
                .Cells(6, 33).Value = printHed(0).sDate
            Else
                '海外
                .Cells(6, 33).Value = Format(CDate(printHed(0).sDate), "DD/MMM/YYYY")
            End If

            .Cells(9, 33).Value = printHed(0).TantoNm               '当社担当者

            If Len(printHed(0).OurEstimateNo) > 9 Then
                .Cells(10, 33).Value = Left(printHed(0).OurEstimateNo, 9) & "*"     '見積Ｎｏ
            Else
                .Cells(10, 33).Value = printHed(0).OurEstimateNo                    '見積Ｎｏ
            End If

        End With

        mCurRows = 17

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteHeaderPage
    '**  機能    : ヘッダー　見積№毎のページを出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeaderPage()

        Dim lWk As Long
        Dim lCnt As Long
        Dim AllPage As Long
        Dim SheeyWk As Long

        AllPage = PageWk(1) - PageWk(0) + 1  '全ページ数
        SheeyWk = PageWk(0)                  '開始シート

        For lWk = 1 To AllPage
            lCnt = lCnt + 1
            '「ページ/全ページ」印字
            xlWs = xlApp.Worksheets(SheeyWk)
            With xlWs
                .Cells(5, 33).Value = lCnt & "/" & AllPage
            End With

            '印刷時
            If mCurMode = enmPrintmode.PrintOut Then
                Call steExcelPrint(SheeyWk, modCommon.ActivePrinter.Printer) 'Excel 印刷
            End If

            SheeyWk = SheeyWk + 1
        Next lWk

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteMidHeader
    '**  機能    : 中ヘッダー情報をExcelに出力する。
    '**  戻り値  : voRec:見積明細データ   viMHedCnt:中ヘッダ印字カウント
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMidHeader(voRec As DataTable, ByRef viMHedCnt As Integer)

        '中ヘッダ印字２回目以降は１行あける
        If viMHedCnt > 0 Then
            '改行
            Call CheckPages()
        End If

        'UNIT
        If modHanbai.NCnvN(voRec.Rows(0)("JyuM_004")) <> "" Then
            Call CheckPages()
            'UNIT
            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 4).Value = "UNIT"
                .Cells(mCurRows, 7).Value = modHanbai.NCnvN(voRec.Rows(0)("JyuM_004"))
            End With
        End If

        'MAKER
        If modHanbai.NCnvN(voRec.Rows(0)("JyuM_005")) <> "" Then
            Call CheckPages()

            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 4).Value = "MAKER"
                .Cells(mCurRows, 7).Value = modHanbai.NCnvN(voRec.Rows(0)("JyuM_005"))
            End With
        End If

        'TYPE
        If modHanbai.NCnvN(voRec.Rows(0)("JyuM_006")) <> "" Then
            Call CheckPages()

            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 4).Value = "TYPE"
                .Cells(mCurRows, 7).Value = modHanbai.NCnvN(voRec.Rows(0)("JyuM_006"))
            End With
        End If

        'SER NO
        If modHanbai.NCnvN(voRec.Rows(0)("JyuM_007")) <> "" Then
            Call CheckPages()

            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 4).Value = "SER/NO"
                .Cells(mCurRows, 7).Value = modHanbai.NCnvN(voRec.Rows(0)("JyuM_007"))
            End With
        End If

        'DWG NO
        If modHanbai.NCnvN(voRec.Rows(0)("JyuM_008")) <> "" Then
            Call CheckPages()

            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 4).Value = "DWG NO"
                .Cells(mCurRows, 7).Value = modHanbai.NCnvN(voRec.Rows(0)("JyuM_008"))
            End With
        End If

        'ADDITIONAL DETAILS
        If modHanbai.NCnvN(voRec.Rows(0)("JyuM_025")) <> "" Then
            Call CheckPages()

            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 4).Value = "ADDITIONAL DETAILS " & modHanbai.NCnvN(voRec.Rows(0)("JyuM_025"))
            End With
        End If

        '改行
        Call CheckPages()
        '中ヘッダ印字カウント
        viMHedCnt = viMHedCnt + 1

    End Sub

    '***************************************************************
    '**  名称    : Sub sttWriteMeisai
    '**  機能    : 明細情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    : voRec:見積明細データ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteMeisai(voRec As DataRow)

        Dim strWk As String
        Dim strHinNmBunkatu(9) As String
        Dim strHinBanBunkatu(4) As String
        Dim Mojisu As Integer
        Dim lsWork As String
        Dim i As Integer

        Dim strHinNmWork() As String
        Dim j As Integer

        '改ページチェック
        Call CheckPages()

        '品名
        Erase strHinNmBunkatu
        ReDim Preserve strHinNmBunkatu(9)
        lsWork = modHanbai.NCnvN(voRec("JyuD_005"))

        '品名分割
        If InStr(lsWork, "*") > 0 Then

            '"*"で改行
            i = 0
            For i = LBound(strHinNmBunkatu) To UBound(strHinNmBunkatu)
                If InStr(lsWork, "*") > 0 Then
                    If InStr(lsWork, "*") > 35 Then
                        strHinNmBunkatu(i) = Mid(lsWork, 1, 34)
                        lsWork = Mid(lsWork, 35, Len(lsWork))
                    Else
                        strHinNmBunkatu(i) = Mid(lsWork, 1, InStr(lsWork, "*") - 1)
                        If InStr(lsWork, "*") + 1 <= Len(lsWork) Then
                            lsWork = Mid(lsWork, InStr(lsWork, "*") + 1, Len(lsWork))
                        Else
                            lsWork = ""
                        End If
                    End If
                Else
                    strHinNmBunkatu(i) = lsWork
                    Exit For
                End If
            Next i

        Else

            j = 0
            strHinNmWork = Split(lsWork, " ")

            If Len(lsWork) > 34 Then
                For i = 0 To UBound(strHinNmWork)
                    If Len(strHinNmBunkatu(j)) + Len(strHinNmWork(i)) > 34 Then
                        j = j + 1
                    End If

                    strHinNmBunkatu(j) = Trim(strHinNmBunkatu(j) & Space(1) & strHinNmWork(i))
                Next
            Else
                strHinNmBunkatu(0) = lsWork
            End If

        End If

        strWk = modHanbai.NCnvN(voRec("JyuD_004"))

        If Right(strWk, 1) = "0" Then
            '品名のみ
            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 4).Value = strHinNmBunkatu(0)
                .Cells(mCurRows, 4).Font.Bold = True
            End With
            '品名２
            If strHinNmBunkatu(1) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(1))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名３
            If strHinNmBunkatu(2) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(2))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名４
            If strHinNmBunkatu(3) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(3))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名５
            If strHinNmBunkatu(4) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(4))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名６
            If strHinNmBunkatu(5) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(5))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名７
            If strHinNmBunkatu(6) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(6))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名８
            If strHinNmBunkatu(7) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(7))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名９
            If strHinNmBunkatu(8) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(8))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If
            '品名１０
            If strHinNmBunkatu(9) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(9))
                    .Cells(mCurRows, 4).Font.Bold = True
                End With
            End If

        Else
            If modHanbai.NCnvN(voRec("JyuD_019")) = "" Then
                Call CheckPages()
            End If

            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                strWk = Left(strWk, Len(strWk) - 1)
                If modHanbai.NCnvN(voRec("JyuD_019")) <> "" Then
                    '項目
                    .Cells(mCurRows, 2).Value = strWk
                End If

                Call ExcelSet(True)
                '品名１
                .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(0))

                '品番
                Erase strHinBanBunkatu
                ReDim Preserve strHinBanBunkatu(4)
                lsWork = modHanbai.NCnvN(voRec("JyuD_006"))
                If InStr(lsWork, "*") > 0 Then
                    '"*"で改行
                    i = 0
                    For i = LBound(strHinBanBunkatu) To UBound(strHinBanBunkatu)
                        If InStr(lsWork, "*") > 0 Then
                            If InStr(lsWork, "*") > 17 Then
                                strHinBanBunkatu(i) = Mid(lsWork, 1, 16)
                                lsWork = Mid(lsWork, 17, Len(lsWork))
                            Else
                                strHinBanBunkatu(i) = Mid(lsWork, 1, InStr(lsWork, "*") - 1)
                                If InStr(lsWork, "*") + 1 < Len(lsWork) Then
                                    lsWork = Mid(lsWork, InStr(lsWork, "*") + 1, Len(lsWork) - InStr(lsWork, "*"))
                                Else
                                    lsWork = ""
                                End If
                            End If
                        Else
                            strHinBanBunkatu(i) = lsWork
                            Exit For
                        End If
                    Next i

                Else
                    '16文字ごとに分ける
                    Mojisu = Len(lsWork)
                    If Mojisu > 0 Then
                        strHinBanBunkatu(0) = Trim(Left(lsWork, 16))
                    End If
                    If Mojisu > 16 Then
                        strHinBanBunkatu(1) = Trim(Mid(lsWork, 17, 16))
                    End If
                    If Mojisu > 32 Then
                        strHinBanBunkatu(2) = Trim(Mid(lsWork, 33, 16))
                    End If
                    If Mojisu > 48 Then
                        strHinBanBunkatu(3) = Trim(Mid(lsWork, 49, 12))
                    End If
                End If

                '品番１
                .Cells(mCurRows, 18).Value = strHinBanBunkatu(0)
                '数量
                .Cells(mCurRows, 25).Value = Format(CDbl(modHanbai.NCnvZ(voRec("JyuD_018"))), "#,###")
                '単位
                .Cells(mCurRows, 27).Value = modHanbai.NCnvN(voRec("JyuD_019"))
                '販売単価
                .Cells(mCurRows, 33).Value = Format(CDbl(modHanbai.NCnvZ(voRec("JyuD_020"))), "#,###")
                '販売金額
                .Cells(mCurRows, 39).Value = Format(CDbl(modHanbai.NCnvZ(voRec("JyuD_021"))), "#,###")
                '金額合計
                TotalAmount = TotalAmount + CDbl(modHanbai.NCnvZ(voRec("JyuD_021")))
            End With

            '品名２ OR 品番２
            If strHinNmBunkatu(1) <> "" Or strHinBanBunkatu(1) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(1))    '品名２
                    .Cells(mCurRows, 18).Value = modHanbai.NCnvN(strHinBanBunkatu(1))  '品番２
                End With
            End If

            '品名３ OR 品番３
            If strHinNmBunkatu(2) <> "" Or strHinBanBunkatu(2) <> "" Then
                '改ページチェック
                Call CheckPages()
                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(2))    '品名３
                    .Cells(mCurRows, 18).Value = modHanbai.NCnvN(strHinBanBunkatu(2))  '品番３
                End With
            End If

            '品名４ OR 品番４
            If strHinNmBunkatu(3) <> "" Or strHinBanBunkatu(3) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(3))    '品名４
                    .Cells(mCurRows, 18).Value = modHanbai.NCnvN(strHinBanBunkatu(3))  '品番４
                End With
            End If

            '品名５ OR 品番５
            If strHinNmBunkatu(4) <> "" Or strHinBanBunkatu(4) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(4))    '品名４
                    .Cells(mCurRows, 18).Value = modHanbai.NCnvN(strHinBanBunkatu(4))  '品番４
                End With
            End If

            '品名６
            If strHinNmBunkatu(5) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(5))    '品名５
                End With
            End If

            '品名７
            If strHinNmBunkatu(6) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(6))    '品名５
                End With
            End If

            '品名８
            If strHinNmBunkatu(7) <> "" Then
                '改ページチェック
                Call CheckPages()
                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(7))    '品名５
                End With
            End If

            '品名９
            If strHinNmBunkatu(8) <> "" Then
                '改ページチェック
                Call CheckPages()
                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(8))    '品名５
                End With
            End If

            '品名１０
            If strHinNmBunkatu(9) <> "" Then
                '改ページチェック
                Call CheckPages()

                xlWs = xlApp.Worksheets(GetTargetSheet)
                With xlWs
                    Call ExcelSet(True)
                    .Cells(mCurRows, 4).Value = modHanbai.NCnvN(strHinNmBunkatu(9))    '品名５
                End With
            End If

        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub ExcelSet
    '**  機能    : 行の設定
    '**  戻り値  :
    '**  引数    : vbKbn True: 列を結合し縮小して全体を表示にする
    '**          :       False:元に戻す
    '**  備考    :
    '***************************************************************
    Private Sub ExcelSet(vbKbn As Boolean)

        Dim lsWork As String


        If vbKbn = True Then

            lsWork = "D" & mCurRows & ":Q" & mCurRows

            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs.Range(lsWork)
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .WrapText = False
                .Orientation = 0
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = True
                .MergeCells = True
            End With
            With xlWs.Range(lsWork).Font
                .Name = "Arial"
                .FontStyle = "標準"
                .Size = 9
                .Strikethrough = False
                .Superscript = False
                .Subscript = False
                .OutlineFont = False
                .Shadow = False
                .Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone
                .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
            End With

            lsWork = "R" & mCurRows & ":X" & mCurRows
            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs.Range(lsWork)
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .WrapText = False
                .Orientation = 0
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = True
                .MergeCells = True
            End With
            With xlWs.Range(lsWork).Font
                .Name = "Arial"
                .FontStyle = "標準"
                .Size = 9
                .Strikethrough = False
                .Superscript = False
                .Subscript = False
                .OutlineFont = False
                .Shadow = False
                .Underline = Excel.XlUnderlineStyle.xlUnderlineStyleNone
                .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
            End With

        Else

            lsWork = "D" & mCurRows & ":Q" & mCurRows
            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs.Range(lsWork)
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .WrapText = False
                .Orientation = 0
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
            End With

            lsWork = "R" & mCurRows & ":X" & mCurRows
            With xlApp.Worksheets(GetTargetSheet).Range(lsWork)
                .HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
                .VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
                .WrapText = False
                .Orientation = 0
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = False
                .MergeCells = False
            End With

        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub CheckPages
    '**  機能    : 出力ページと行のチェックと改頁処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub CheckPages()

        '行をインクリメント
        mCurRows = mCurRows + 1

        '行チェック
        If mCurRows > pstLastRow Then
            mCurPage = mCurPage + 1     'ページインクリメント
            '改頁処理
            Call stePrevSheetCopy(mCurPage)              'プレビュー・印刷用Bookのコピー
            Call loSheetClear()                            'Excelシートクリア

            'プレビューからの見積発行日更新用キー作成
            ReDim Preserve JyuHUPDKey(mCurPage)
            JyuHUPDKey(mCurPage).KEIYAKUNO = KeiyakuNoWk      '契約№
            JyuHUPDKey(mCurPage).RefNo = RefNoWk              '見積№

            mCurRows = pstStartRow - 1   '行をリセット
            Call sttWriteHeader()          'ヘッダー情報の出力
        End If

        Call ExcelSet(False)

    End Sub

    '***************************************************************
    '**  名称    : Sub sttWriteFooter
    '**  機能    : フッター情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :  voRec:受注ヘッダデータ
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteFooter(voRec As DataTable)

        Dim strCellWk As String
        Dim strWk As String
        Dim NoukiWk() As String
        Dim i As Integer
        Dim lbRows As Boolean


        lbRows = False

        '罫線
        Call CheckPages()
        Call CheckPages()
        strCellWk = "A" & mCurRows & ":" & "AN" & mCurRows
        xlWs = xlApp.Worksheets(GetTargetSheet)
        With xlWs.Range(strCellWk).Borders(Excel.XlBordersIndex.xlEdgeTop)
            .LineStyle = Excel.XlLineStyle.xlContinuous
            .Weight = Excel.XlBorderWeight.xlMedium
            .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
        End With


        '金額合計
        Call CheckPages()
        xlWs = xlApp.Worksheets(GetTargetSheet)
        With xlWs
            strCellWk = "Y" & mCurRows & ":" & "AM" & mCurRows
            With xlWs.Range(strCellWk).Borders(Excel.XlBordersIndex.xlEdgeBottom)
                .LineStyle = Excel.XlLineStyle.xlDouble
                .Weight = Excel.XlBorderWeight.xlThick
                .ColorIndex = Excel.XlColorIndex.xlColorIndexAutomatic
            End With
            '        .Cells(mCurRows, 26).Value = "                 Total Amount(J YEN)"            'DELETE 2017/12/26 AOKI
            .Cells(mCurRows, 26).Value = "       Total Amount(J YEN)  EX WORKS"             'INSERT 2017/12/26 AOKI
            .Cells(mCurRows, 39).Value = Format(TotalAmount, "#,###")
        End With

        'Delivery Time
        i = 0

        NoukiWk = Split(modHanbai.NCnvN(voRec.Rows(0)("JyuH_011")), "*")

        For i = LBound(NoukiWk) To UBound(NoukiWk)
            If i <> 0 Then
                Call CheckPages()
            End If
            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                If i = 0 Then
                    .Cells(mCurRows, 1).Value = "Delivery Time:"
                    lbRows = True
                End If
                .Cells(mCurRows, 6).Value = NoukiWk(i)
                .Range("A" & mCurRows).Font.Size = 9
            End With
        Next

        If CDbl(modHanbai.NCnvZ(voRec.Rows(0)("JyuH_0031"))) = 14 Then
            '文言
            If lbRows = True Then
                Call CheckPages()
                Call CheckPages()
                Call CheckPages()
            End If

            strWk = "Hereby the supplier/manufacturer declares, "
            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 1).Value = strWk
                .Range("A" & mCurRows).Font.Size = 9
            End With

            Call CheckPages()
            strWk = "that asbestos Is Not contained In the delivered products."
            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs
                .Cells(mCurRows, 1).Value = strWk
                .Range("A" & mCurRows).Font.Size = 9
            End With
        End If

        '文言
        If CDbl(modHanbai.NCnvZ(voRec.Rows(0)("JyuH_0031"))) = 14 Then
            Call CheckPages()
            Call CheckPages()
        Else
            If lbRows = True Then
                Call CheckPages()
                Call CheckPages()
                Call CheckPages()
            End If
        End If

        strWk = "We thanks you very much For this order "
        xlWs = xlApp.Worksheets(GetTargetSheet)
        With xlWs
            .Cells(mCurRows, 1).Value = strWk
            .Range("A" & mCurRows).Font.Size = 9
        End With

        Call CheckPages()
        strWk = "And may have the pleasure Of serving you again In near future."
        xlWs = xlApp.Worksheets(GetTargetSheet)
        With xlWs
            .Cells(mCurRows, 1).Value = strWk
            .Range("A" & mCurRows).Font.Size = 9
        End With


        'INSERT 2017/09/25 AOKI START -----------------------------------------------------------------------------------------
        Call CheckPages()
        Call CheckPages()
        Call CheckPages()
        strWk = "     *****Once your order Is placed With us, thereafter the order can Not be cancelled In any Case.*****"
        xlWs = xlApp.Worksheets(GetTargetSheet)
        With xlWs
            .Cells(mCurRows, 1).Value = strWk
            .Range("A" & mCurRows).Font.Size = 9
        End With

        If mCurRows < 60 Then
            strWk = "------------------------------------------------"
            strWk = strWk & vbCrLf & "3FL YAMASHIRO BLDG, 1 - 1 KANDAOGAWAMACHI, CHIYODA - KU, TOKYO 101-0052 JAPAN"
            strWk = strWk & vbCrLf & "Telephone:  81-3-5283-5252  Fax: 81-3-5283-5251"
            strWk = strWk & vbCrLf & "E-mail: sales@ones-forte.jp  Website: http://www.ones-forte.jp"
            xlWs = xlApp.Worksheets(GetTargetSheet)
            With xlWs.Range("A60:AN63")
                .Merge()
                .HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter
                .Font.Name = "Century Schoolbook"
                .Font.Size = 9
                .Value = strWk
            End With
        End If
        'INSERT 2017/09/25 AOKI E N D -----------------------------------------------------------------------------------------

    End Sub
    '***************************************************************
    '**  名称    : Sub loSheetClear
    '**  機能    : シートのクリア
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************


    Private Sub loSheetClear()

        Call steSheetClear(GetTargetSheet, pstPrintArea)
        '罫線クリア
        Dim xlWs As Excel.Worksheet = xlApp.Worksheets(GetTargetSheet)
        Dim xlRange As Excel.Range = xlWs.Range(pstPrintArea)

        With xlRange
            .RowHeight = 13.5
            .Font.Bold = False
            .Borders(Excel.XlBordersIndex.xlDiagonalDown).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            .Borders(Excel.XlBordersIndex.xlDiagonalUp).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            .Borders(Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            .Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            .Borders(Excel.XlBordersIndex.xlEdgeRight).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            .Borders(Excel.XlBordersIndex.xlInsideVertical).LineStyle = Excel.XlLineStyle.xlLineStyleNone
            .Borders(Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Excel.XlLineStyle.xlLineStyleNone
        End With

    End Sub

    Private Sub steSheetClear(ByVal vlSheet As Long, ByVal vsCell As String)
        xlApp.Worksheets(vlSheet).Range(vsCell).ClearContents()
    End Sub

    '***************************************************************
    '**  名称    : Sub JYUMR030_JyuHUpdate
    '**  機能    : 注文請書発行日を更新
    '**  戻り値  :
    '**  引数    : vKey1: 契約№　vkey2:見積№
    '**  備考    :
    '***************************************************************
    Public Sub JYUMR030_JyuHUpdate(vKey1 As String, vKey2 As String)

        Dim strWk As String

        strWk = "UPDATE JyuH_Dat "
        strWk = strWk & " SET JyuH_029 = '" & strHakkoDate & "'"
        strWk = strWk & " WHERE JyuH_001 = '" & vKey1 & "'"
        strWk = strWk & "   AND JyuH_009 = '" & vKey2 & "'"
        DB.ExecuteSql(strWk)


    End Sub

    '***************************************************************
    '**  名称    : Sub File_Delete
    '**  機能    : Report\PDFフォルダ下のファイルを削除
    '**  戻り値  :
    '**  引数    : vPath:フォルダパス
    '**  備考    :
    '***************************************************************
    Private Sub File_Delete(vPath As String)

        For Each deleteFile In Directory.GetFiles(vPath, "*.*", SearchOption.TopDirectoryOnly)
            File.Delete(deleteFile)
        Next


    End Sub


    '***************************************************************
    '**  名称    : Function JYUMR030_PrevExcelOpen() As Boolean
    '**  機能    : プレビュー用ワークシートのオープン処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : vlPage ; ページ番号(ワークシート名)
    '**  備考    :
    '***************************************************************
    Public Function JYUMR030_PrevExcelOpen(vlPage As Long, vlRange As String) As Boolean

        Dim exFname As String

        JYUMR030_PrevExcelOpen = False

        'ファイルの存在チェック
        If stePrevFileCheck() <> "" Then
            xlApp.Workbooks.Open(Filename:=pstrPdfNm)       'ブックオープン
            xlWs = xlApp.Worksheets(vlPage)                 'シートの選択
            'xlWs.Range(vlRange).Select()                           'レンジの選択
            xlWs.Range(vlRange).Copy()                                  'クリップボードにコピー

            JYUMR030_PrevExcelOpen = True
        End If

    End Function

    '***************************************************************
    '**  名称    : Function JYUMR030_PrevFileCheck() As String
    '**  機能    : プレビュー用ファイルの存在チェック処理。
    '**  戻り値  : ファイル文字列(Dir()関数の返り値)
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function JYUMR030_PrevFileCheck() As String

        JYUMR030_PrevFileCheck = Dir(pstrPdfNm, vbNormal)

    End Function


#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = clswinApi.StaticWinApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = clswinApi.StaticWinApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    clswinApi.StaticWinApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = clswinApi.StaticWinApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +gstStrSEPFILE +
                    clswinApi.StaticWinApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next

            If xlApp IsNot Nothing Then
                xlApp.Quit()
                xlApp = Nothing
            End If
        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub
#End Region

End Module
