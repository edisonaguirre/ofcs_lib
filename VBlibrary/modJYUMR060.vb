﻿Imports System.Windows.Forms
Imports globalcls.modHanbai
Imports clswinApi
Imports SAHANBAI.clsGlobal
'Imports Constant = SAHANBAI.Constant
Imports winApi = clswinApi.StaticWinApi
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports SAHANBAI.TaxComp
''all database transaction were put here , all control related
Public Module modJYUMR060

    Dim xlApp As Excel.Application
    Dim xlWs As Excel.Worksheet
    Dim xlRange As Excel.Range
    '***************************************************************
    '**
    '**  機能           :  仕入先別実績一覧表(modJYUMR060)
    '**  作成日         :
    '**  更新日         :
    '**  備考           :
    '**
    '***************************************************************
    ''各マスタ情報



    Public ConMst(0 To 0) As ConMstInfo      'コントロールマスタ情報
    Public SirMst As SirMstInfo             'INSERT 2014/03/18 AOKI
    Public psSirDate As String              'INSERT 2014/03/18 AOKI

    '印刷モード
    Public Enum enmPrintmode
        Preview = 0     'プレビュー
        PrintOut = 1    '印刷
    End Enum

    'データ抽出条件
    Public SirDate(1) As String    '仕入日付
    Public SelSirCd() As String    '仕入先コード指定
    Public chkYMD As Integer   '0:仕入日付 1:チェック日付

    'エクセル上のセル範囲
    Private pstLastRow As Integer              '最終行
    '== 請求一覧のExcel定義 ==
    Private Const psttREPORT_NAME As String = "JYUMR060_仕入先別実績一覧表"       'Report名称
    Private Const pstPrintArea As String = "B8:M73"                    'シートクリア範囲
    'Private Const pstPrevArea      As String = "A1:AM75"                   'プレビュー表示範囲     'DELETE 2014/07/18 AOKI
    Private Const pstPrevArea As String = "A1:BF75"                   'プレビュー表示範囲      'INSERT 2014/07/18 AOKI
    Private Const pstMaxRow As Integer = 66                         '最大行数
    Private Const pstStartRow As Integer = 8                          'ページ内の開始行

    '印刷共通定義
    Private mIntMode As Integer     'カレント印刷モード
    Private mCurPage As Long        'カレントページ番号
    Private mCurRows As Long        'カレント行
    Private mCurCols As Long        'カレント列


    'Excel出力用
    Private xlBookTem As Excel.Workbook
    Private xlBookNew As Excel.Workbook

    Public modJYUMR060_txtKeiyakuNo As TextBox
    Public modJYUMR060_numSCode_0 As NumericUpDown
    Public modJYUMR060_numSCode_1 As NumericUpDown
    Public modJYUMR060_optSelect_0 As RadioButton
    Public modJYUMR060_lblMstList_1 As Label
    Public modJYUMR060_datSiireYMD_0 As DateTimePicker
    Public modJYUMR060_datSiireYMD_1 As DateTimePicker

    Public TaxComp As New TaxComp
    Public TaxCalcInfo As TaxComp.TaxCalcInfo



    '***************************************************************
    '**  名称    : Function JYUMR060PrintProc() As Boolean
    '**  機能    : 仕入先別実績一覧表(明細) 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function JYUMR060PrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                     'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim loRec As New DataTable      '明細データ
        Dim befSirCd As String
        Dim befKeiNo As String
        Dim befSDate As String
        Dim lsWork As String
        Dim curSubTotal As Double
        Dim curSubTotal2 As Double

        Dim curAllTotal0 As Double
        Dim curAllTotal1 As Double
        Dim curZeiTotal As Double
        Dim curZeiTotal2 As Double
        Dim SCnt As Long
        Dim befSeiKbn As Integer
        Dim befKbn019 As Integer
        Dim befKbn020 As Integer
        Dim curZeiRitsu As Double



        'Try


        JYUMR060PrintProc = False

        mIntMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存
        pstLastRow = pstMaxRow + pstStartRow - 1

        loRec = Nothing

        '仕入データの読込み用SQL文作成ルーチン
        strSQL = CreateSQLText(0)

        'データの読込み
        Dim DB As New clsDB.DBService
        loRec = DB.GetDatafromDB(strSQL)

        '===データ存在チェック
        If loRec.Rows.Count <= 0 Then
            'データ無しメッセージ
            Call ksExpMsgBox("印刷するデータが存在しません。", "I")
            GoTo Exit_JYUMR060PrintProc
        End If

        xlApp = CreateObject("Excel.Application")

        '===Excelファイルのオープン
        If steExcelOpen(psttREPORT_NAME & modHanbai.exXLSSuffix) = False Then
            'Excelファイル無しメッセージ
            Call ksExpMsgBox(psttREPORT_NAME & "のレポートが存在しません。", "E")
            GoTo Exit_JYUMR060PrintProc
        End If

        If mIntMode = enmPrintmode.Preview Then        'プレビューモード
            Call stePrevWorkDelete()  '古いワークファイルを削除する
        End If

        Dim ConMst As DataTable
        'ConMst = GetdtConMstInfo()
        ConMst = modCommon.GetConMstInfo()

        '===各変数初期化
        mCurPage = 1                    'ページ番号
        mCurRows = pstStartRow - 1      '明細印刷開始行
        DCount = 0                      'データカウンタ
        curSubTotal = 0
        '    curAllTotal = 0                'DELETE 2014/03/18 AOKI
        curAllTotal0 = 0                'INSERT 2014/03/18 AOKI
        curAllTotal1 = 0                'INSERT 2014/03/18 AOKI
        curZeiTotal = 0                 'INSERT 2014/03/18 AOKI
        SCnt = 0

        'INSERT 2014/03/18 AOKI START -------------------------------------------------------------------------------------
        SirMst.Sir_001 = modHanbai.NCnvN(loRec.Rows(0)("HatD_006"))
        Call GetSirMstInfo(SirMst, 0)
        befSeiKbn = SirMst.Sir_021
        'INSERT 2014/03/18 AOKI E N D -------------------------------------------------------------------------------------

        'シートのクリア
        Call loSheetClear()

        'ヘッダー印刷
        Call sttWriteHeader(NCnvN(loRec.Rows(0)("HatD_006")), NCnvN(loRec.Rows(0)("HatD_007")))

        befSirCd = NCnvN(loRec.Rows(0)("HatD_006"))




        For Each dr As DataRow In loRec.Rows


            '進行状況用カウンタ
            DCount = DCount + 1


            'INSERT 2014/03/18 AOKI START -------------------------------------------------------------------------------------
            SirMst.Sir_001 = NCnvN(dr("HatD_006"))
            Call GetSirMstInfo(SirMst, 0)
            If chkYMD = 0 Then
                psSirDate = NCnvN(dr("HatD_016"))
            Else
                psSirDate = NCnvN(dr("HatD_031"))
            End If




            With TaxCalcInfo

                .ConMst = ConMst
                .ZeinukiKingaku = 0
                .Shohizei = 0
                .ZeikomiKingaku = 0
            End With

            '仕入先コードブレーク
            If befSirCd <> NCnvN(dr("HatD_006")) Then

                Call sttWriteTotal(0, curSubTotal, 0, befSeiKbn, befKbn019, befKbn020)                  'INSERT 2014/07/30 AOKI
                curAllTotal1 = curAllTotal1 + TaxCalcInfo.ZeikomiKingaku
                curZeiTotal = curZeiTotal + TaxCalcInfo.Shohizei
                '合計出力(１行あける）
                Call CheckPages()

                If befSeiKbn = 2 Then  '請求単位
                    '                Call sttWriteTotal(1, curAllTotal0, 0, befSeiKbn)                                  'DELETE 2014/07/30 AOKI
                    Call sttWriteTotal(1, curAllTotal0, 0, befSeiKbn, befKbn019, befKbn020)             'INSERT 2014/07/30 AOKI
                Else                        '上記以外（伝票単位）
                    '                Call sttWriteTotal(1, curAllTotal1, curZeiTotal, befSeiKbn)                        'DELETE 2014/07/30 AOKI
                    Call sttWriteTotal(1, curAllTotal1, curZeiTotal, befSeiKbn, befKbn019, befKbn020)   'INSERT 2014/07/30 AOKI
                End If
                curSubTotal = 0
                curAllTotal0 = 0
                curAllTotal1 = 0
                curZeiTotal = 0
                curSubTotal2 = 0            'INSERT 2017/09/25 AOKI
                curZeiTotal2 = 0            'INSERT 2017/09/25 AOKI
                'UPDATE 2014/03/18 AOKI E N D -------------------------------------------------------------------------------------
                '改ページ
                mCurRows = pstLastRow + 1
                Call CheckPages()
                mCurRows = pstStartRow - 1   '行をリセット
                'ヘッダー出力
                Call sttWriteHeader(NCnvN(dr("HatD_006")), NCnvN(dr("HatD_007")))
                befKeiNo = ""
                befSDate = ""
                befSirCd = NCnvN(dr("HatD_006"))
                befSeiKbn = SirMst.Sir_021              'INSERT 2014/03/18 AOKI
                befKbn019 = SirMst.Sir_019              'INSERT 2014/07/30 AOKI
                befKbn020 = SirMst.Sir_020              'INSERT 2014/07/30 AOKI
                SCnt = 0
            End If

            '仕入日付か契約番号がブレークしたら小計印字
            If chkYMD = 0 Then
                lsWork = Format(Convert.ToDateTime(NCnvN(dr("HatD_016"))), "yy/MM/dd")
            Else
                lsWork = Format(Convert.ToDateTime(NCnvN(dr("HatD_031"))), "yy/MM/dd")
            End If
            If SCnt > 0 And (befSDate <> lsWork Or befKeiNo <> NCnvN(dr("HatD_001"))) Then

                If SirMst.Sir_021 = 3 Then      '個別単位
                    Call sttWriteTotal(0, curSubTotal2, curZeiTotal2, befSeiKbn, befKbn019, befKbn020)

                    curSubTotal = 0
                    curAllTotal1 = curAllTotal1 + curSubTotal2
                    curZeiTotal = curZeiTotal + curZeiTotal2
                    curSubTotal2 = 0
                    curZeiTotal2 = 0
                Else
                    Call sttWriteTotal(0, curSubTotal, 0, befSeiKbn, befKbn019, befKbn020)

                    curSubTotal = 0
                    curAllTotal1 = curAllTotal1 + TaxCalcInfo.ZeikomiKingaku
                    curZeiTotal = curZeiTotal + TaxCalcInfo.Shohizei
                End If
                'UPDATE 2014/08/12 AOKI E N D -------------------------------------------------------------------------------------

                If mCurRows < pstMaxRow + pstStartRow - 1 Then
                    Call CheckPages()
                End If
                'UPDATE 2014/03/18 AOKI START -------------------------------------------------------------------------------------
            End If

            Call CheckPages()

            If befSDate <> lsWork Then
                With xlApp.Worksheets(GetTargetSheet)

                    .Cells(mCurRows, 2).Value = lsWork                                       '仕入日付
                    .Cells(mCurRows, 3).Value = NCnvN(dr("HatD_001"))                      '契約番号
                End With

                befSDate = lsWork
                befKeiNo = NCnvN(dr("HatD_001"))
            ElseIf befKeiNo <> NCnvN(dr("HatD_001")) Then
                With xlApp.Worksheets(GetTargetSheet)
                    .Cells(mCurRows, 3).Value = NCnvN(dr("HatD_001"))                      '契約番号
                End With
                befKeiNo = NCnvN(dr("HatD_001"))
            Else
                With xlApp.Worksheets(GetTargetSheet)
                    .Cells(mCurRows, 2).Value = ""   '仕入日付
                    .Cells(mCurRows, 3).Value = ""   '契約番号
                End With
            End If

            With xlApp.Worksheets(GetTargetSheet)
                .Cells(mCurRows, 5).Value = CDbl(modHanbai.NCnvZ(dr("HatD_002")))               'SEQ


                If Len(CDbl(modHanbai.NCnvZ(dr("HatD_003")))) = 1 Then
                    .Cells(mCurRows, 6).Value = CDbl(modHanbai.NCnvZ(dr("HatD_003")))                                     'Item
                Else
                    If Left(CDbl(modHanbai.NCnvZ(dr("HatD_003"))), Len(CDbl(modHanbai.NCnvZ(dr("HatD_003")))) - 1) > 999 Then
                        .Cells(mCurRows, 6).Value = ""                                                                  'Item
                    Else
                        .Cells(mCurRows, 6).Value = Left(CDbl(modHanbai.NCnvZ(dr("HatD_003"))), Len(CDbl(modHanbai.NCnvZ(dr("HatD_003")))) - 1)     'Item
                    End If
                End If
                'UPDATE 2014/12/11 AOKI START -----------------------------------------------------------------------------------------

                .Cells(mCurRows, 7).Value = NCnvN(dr("HatD_009"))                    '品名
                .Cells(mCurRows, 8).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_017"))), "#,##0")   '数量
                .Cells(mCurRows, 9).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_019"))), "#,##0")   '単価
                .Cells(mCurRows, 10).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_022"))), "#,##0")  '金額
            End With

            '合計集計
            curSubTotal = curSubTotal + CDbl(modHanbai.NCnvZ(dr("HatD_022")))

            curAllTotal0 = curAllTotal0 + CDbl(modHanbai.NCnvZ(dr("HatD_022")))   'INSERT 2014/03/18 AOKI

            'INSERT 2014/08/12 AOKI START -------------------------------------------------------------------------------------
            TaxCalcInfo.ZeiKbn = SirMst.Sir_019                         '0:未選択 1:内税 2:外税
            If SirMst.Sir_020 = 0 Then
                TaxCalcInfo.ZeiKeisan = SirMst.Sir_020                  '0:未選択
            Else
                TaxCalcInfo.ZeiKeisan = SirMst.Sir_020 - 1              '1:四捨五入 2:切上げ 3:切捨て
            End If

            TaxCalcInfo.Kingaku = CDbl(modHanbai.NCnvZ(dr("HatD_022")))              '金額
            TaxCalcInfo.DDate = modCommon.CnvDate(NCnvN(dr("HatD_016")), "MM/dd/yyyy", "2000/01/01")                 '仕入日付

            Call TaxComp.pTaxCalcCom(TaxCalcInfo)

            curSubTotal2 = curSubTotal2 + TaxCalcInfo.ZeikomiKingaku
            curZeiTotal2 = curZeiTotal2 + TaxCalcInfo.Shohizei
            'INSERT 2014/08/12 AOKI E N D -------------------------------------------------------------------------------------

            SCnt = SCnt + 1
        Next



        If SirMst.Sir_021 = 3 Then  '個別単位
            Call sttWriteTotal(0, curSubTotal2, curZeiTotal2, SirMst.Sir_021, SirMst.Sir_019, SirMst.Sir_020)   'INSERT 2014/07/30 AOKI
            curAllTotal1 = curAllTotal1 + curSubTotal2
            curZeiTotal = curZeiTotal + curZeiTotal2
        Else
            Call sttWriteTotal(0, curSubTotal, 0, SirMst.Sir_021, SirMst.Sir_019, SirMst.Sir_020)               'INSERT 2014/07/30 AOKI
            curAllTotal1 = curAllTotal1 + TaxCalcInfo.ZeikomiKingaku
            curZeiTotal = curZeiTotal + TaxCalcInfo.Shohizei
        End If
        'UPDATE 2014/08/12 AOKI E N D -------------------------------------------------------------------------------------

        '合計出力(１行あける）
        Call CheckPages()

        If SirMst.Sir_021 = 2 Then  '請求単位
            '        Call sttWriteTotal(1, curAllTotal0, 0, SirMst.Sir_021)                                             'DELETE 2014/07/30 AOKI
            Call sttWriteTotal(1, curAllTotal0, 0, SirMst.Sir_021, SirMst.Sir_019, SirMst.Sir_020)              'INSERT 2014/07/30 AOKI
        Else                        '上記以外（伝票単位）
            '        Call sttWriteTotal(1, curAllTotal1, curZeiTotal, SirMst.Sir_021)                                   'DELETE 2014/07/30 AOKI
            Call sttWriteTotal(1, curAllTotal1, curZeiTotal, SirMst.Sir_021, SirMst.Sir_019, SirMst.Sir_020)    'INSERT 2014/07/30 AOKI
        End If
        'UPDATE 2014/03/18 AOKI E N D -------------------------------------------------------------------------------------

        'クローズ

        'モードごとの処理
        Select Case viMode

            Case enmPrintmode.Preview       'プレビュー

                Call stePrevBookSave()        'Bookの保存
                Call steWorkbooksClose()      'Bookのクローズ


                gbl.ActivePrinter.Copies = 1    'INSERT 2014/02/20 AOKI

                'プレビュー画面の起動
                Dim frm As New SAHANBAI.frmPrintPreview
                With frm
                    .ppvActCell = pstPrevArea
                    .ShowDialog()
                End With

            Case enmPrintmode.PrintOut      '印刷
                mCurRows = pstLastRow
                Call CheckPages()
                Call steWorkbooksClose()      'Bookのクローズ

        End Select


        JYUMR060PrintProc = True

Exit_JYUMR060PrintProc:

        loRec = Nothing
        DB = Nothing


        Exit Function


        'Catch Err As Exception
        '    Call ksExpMsgBox("Error JYUMR060PrintProc : " & Err.Message, "E")
        '    Call steWorkbooksClose()        'Bookのクローズ
        'End Try




    End Function
    '***************************************************************
    '**  名称    : Function JYUMR060TotalPrintProc() As Boolean
    '**  機能    : 仕入先別実績一覧表(合計) 印刷・プレビュー実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    : viMode ; 印刷モード
    '**  備考    :
    '***************************************************************
    Public Function JYUMR060TotalPrintProc(viMode As Integer) As Boolean

        Dim strSQL As String
        Dim DCount As Long                 'ﾚｺｰﾄﾞｶｳﾝﾀｰ
        Dim loRec As New DataTable  '明細データ
        Dim SCnt As Long                 '仕入先毎の明細カウント
        Dim befSirCd As String
        '    Dim curAllTotal    As double             '仕入先合計             'DELETE 2014/03/18 AOKI
        Dim curAllTotal0 As Double             '仕入先合計(税抜)       'INSERT 2014/03/18 AOKI
        Dim curAllTotal1 As Double             '仕入先合計(税込)       'INSERT 2014/03/18 AOKI
        Dim curZeiTotal As Double             '消費税合計             'INSERT 2014/03/18 AOKI
        Dim befSeiKbn As Integer              '消費税計算単位         'INSERT 2014/03/18 AOKI
        Dim befKbn019 As Integer              '消費税区分             'INSERT 2014/07/30 AOKI
        Dim befKbn020 As Integer              '消費税計算区分         'INSERT 2014/07/30 AOKI

        '    On Error GoTo JYUMR060TotalPrintProc_Err

        JYUMR060TotalPrintProc = False

        Try



            mIntMode = viMode                  '印刷ﾓｰﾄﾞを変数に保存
            pstLastRow = pstMaxRow + pstStartRow - 1

            loRec = Nothing

            '仕入データの読込み用SQL文作成ルーチン
            strSQL = CreateSQLText(1)

            Dim DB As New clsDB.DBService
            'データの読込み
            loRec = DB.GetDatafromDB(strSQL)

            '===データ存在チェック
            If loRec.Rows.Count <= 0 Then
                'データ無しメッセージ
                Call ksExpMsgBox("印刷するデータが存在しません。", "I")
                GoTo Exit_JYUMR060TotalPrintProc
            End If

            xlApp = CreateObject("Excel.Application")
            '===Excelファイルのオープン
            If steExcelOpen(psttREPORT_NAME & modHanbai.exXLSSuffix) = False Then
                'Excelファイル無しメッセージ
                Call ksExpMsgBox(psttREPORT_NAME & "のレポートが存在しません。", "E")
                GoTo Exit_JYUMR060TotalPrintProc
            End If

            If mIntMode = enmPrintmode.Preview Then        'プレビューモード
                Call stePrevWorkDelete()  '古いワークファイルを削除する
            End If

            Dim ConMst As DataTable
            ' ConMst = GetdtConMstInfo()
            ConMst = modCommon.GetConMstInfo()

            '===各変数初期化
            mCurPage = 1                    'ページ番号
            mCurRows = pstStartRow - 1      '明細印刷開始行
            DCount = 0                      'データカウンタ
            '    curAllTotal = 0                'DELETE 2014/03/18 AOKI
            curAllTotal0 = 0                'INSERT 2014/03/18 AOKI
            curAllTotal1 = 0                'INSERT 2014/03/18 AOKI
            curZeiTotal = 0                 'INSERT 2014/03/18 AOKI
            SCnt = 0

            'シートのクリア
            Call loSheetClear()



            For Each dr As DataRow In loRec.Rows

                '初回ヘッダー出力
                If DCount = 0 Then
                    Call sttWriteHeader(NCnvN(dr("HatD_006")), NCnvN(dr("HatD_007")))
                End If

                '進行状況用カウンタ
                DCount = DCount + 1


                'INSERT 2014/03/18 AOKI START -------------------------------------------------------------------------------------
                SirMst.Sir_001 = NCnvN(dr("HatD_006"))
                Call GetSirMstInfo(SirMst, 0)
                If chkYMD = 0 Then
                    psSirDate = NCnvN(dr("HatD_016"))
                Else
                    psSirDate = NCnvN(dr("HatD_031"))
                End If

                '初期値
                With TaxCalcInfo
                    .ConMst = ConMst
                    .ZeinukiKingaku = 0
                    .Shohizei = 0
                    .ZeikomiKingaku = 0
                End With

                '仕入先コードブレーク
                If SCnt > 0 And befSirCd <> NCnvN(dr("HatD_006")) Then
                    '合計出力

                    Call CheckPages()

                    If befSeiKbn = 2 Then  '請求単位
                        '                Call sttWriteTotal(1, curAllTotal0, 0, befSeiKbn)                                  'DELETE 2014/07/30 AOKI
                        Call sttWriteTotal(1, curAllTotal0, 0, befSeiKbn, befKbn019, befKbn020)             'INSERT 2014/07/30 AOKI
                    Else                   '上記以外（伝票単位）
                        '                Call sttWriteTotal(1, curAllTotal1, curZeiTotal, befSeiKbn)                        'DELETE 2014/07/30 AOKI
                        Call sttWriteTotal(1, curAllTotal1, curZeiTotal, befSeiKbn, befKbn019, befKbn020)   'INSERT 2014/07/30 AOKI
                    End If
                    curAllTotal0 = 0
                    curAllTotal1 = 0
                    curZeiTotal = 0
                    'UPDATE 2014/03/18 AOKI E N D -------------------------------------------------------------------------------------

                    '改ページ
                    mCurRows = pstLastRow + 1
                    Call CheckPages()
                    mCurRows = pstStartRow - 1   '行をリセット
                    'ヘッダー出力
                    Call sttWriteHeader(NCnvN(dr("HatD_006")), NCnvN(dr("HatD_007")))
                End If

                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    If chkYMD = 0 Then
                        '仕入日付
                        .Cells(mCurRows, 2).Value = Format(Convert.ToDateTime(NCnvN(dr("HatD_016"))), "yy/MM/dd")
                    Else
                        'チェック日付
                        .Cells(mCurRows, 2).Value = Format(Convert.ToDateTime(NCnvN(dr("HatD_031"))), "yy/MM/dd")
                    End If
                    '契約番号
                    .Cells(mCurRows, 3).Value = NCnvN(dr("HatD_001"))


                    Select Case SirMst.Sir_021
                        Case 2      '請求単位
                            .Cells(mCurRows, 8).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_022"))), "#,##0")
                            .Cells(mCurRows, 9).Value = ""
                            .Cells(mCurRows, 10).Value = ""

                        Case 0, 1   '伝票単位
                            TaxCalcInfo.ZeiKbn = SirMst.Sir_019             '0:未選択 1:内税 2:外税
                            If SirMst.Sir_020 = 0 Then
                                TaxCalcInfo.ZeiKeisan = SirMst.Sir_020      '0:未選択
                            Else
                                TaxCalcInfo.ZeiKeisan = SirMst.Sir_020 - 1  '1:四捨五入 2:切上げ 3:切捨て
                            End If
                            TaxCalcInfo.Kingaku = CDbl(modHanbai.NCnvZ(dr("HatD_022"))) '金額
                            TaxCalcInfo.DDate = psSirDate                   '仕入日付

                            Call TaxComp.pTaxCalcCom(TaxCalcInfo)

                            .Cells(mCurRows, 8).Value = Format(TaxCalcInfo.ZeinukiKingaku, "#,##0")
                            .Cells(mCurRows, 9).Value = Format(TaxCalcInfo.Shohizei, "#,##0")
                            .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.ZeikomiKingaku, "#,##0")

                        Case 3      '個別単位
                            .Cells(mCurRows, 8).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_022"))), "#,##0")
                            .Cells(mCurRows, 9).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_Zei"))), "#,##0")
                            .Cells(mCurRows, 10).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_022")) + CDbl(modHanbai.NCnvZ(dr("HatD_Zei")))), "#,##0")

                    End Select

                End With

                '合計集計
                If SirMst.Sir_021 = 3 Then
                    curAllTotal0 = curAllTotal0 + CDbl(modHanbai.NCnvZ(dr("HatD_022")))
                    curAllTotal1 = curAllTotal1 + CDbl(modHanbai.NCnvZ(dr("HatD_022")) + CDbl(modHanbai.NCnvZ(dr("HatD_Zei"))))
                    curZeiTotal = curZeiTotal + CDbl(modHanbai.NCnvZ(dr("HatD_Zei")))
                Else
                    curAllTotal0 = curAllTotal0 + CDbl(modHanbai.NCnvZ(dr("HatD_022")))
                    curAllTotal1 = curAllTotal1 + TaxCalcInfo.ZeikomiKingaku
                    curZeiTotal = curZeiTotal + TaxCalcInfo.Shohizei
                End If
                'UPDATE 2014/08/12 AOKI START -------------------------------------------------------------------------------------

                befSirCd = NCnvN(dr("HatD_006"))
                befSeiKbn = SirMst.Sir_021
                befKbn019 = SirMst.Sir_019                                  'INSERT 2014/07/30 AOKI
                befKbn020 = SirMst.Sir_020                                  'INSERT 2014/07/30 AOKI

                SCnt = SCnt + 1

            Next

            '合計出力
            'UPDATE 2014/03/18 AOKI START -------------------------------------------------------------------------------------
            '    Call CheckPages: Call CheckPages
            '    Call sttWriteTotal(1, curAllTotal)
            Call CheckPages()

            If SirMst.Sir_021 = 2 Then  '請求単位
                '        Call sttWriteTotal(1, curAllTotal0, 0, SirMst.Sir_021)                                             'DELETE 2014/07/30 AOKI
                Call sttWriteTotal(1, curAllTotal0, 0, SirMst.Sir_021, SirMst.Sir_019, SirMst.Sir_020)              'INSERT 2014/07/30 AOKI
            Else                        '上記以外（伝票単位）
                '        Call sttWriteTotal(1, curAllTotal1, curZeiTotal, SirMst.Sir_021)                                   'DELETE 2014/07/30 AOKI
                Call sttWriteTotal(1, curAllTotal1, curZeiTotal, SirMst.Sir_021, SirMst.Sir_019, SirMst.Sir_020)    'INSERT 2014/07/30 AOKI
            End If
            'UPDATE 2014/03/18 AOKI E N D -------------------------------------------------------------------------------------

            'クローズ

            'モードごとの処理
            Select Case viMode

                Case enmPrintmode.Preview      'プレビュー
                    Call stePrevBookSave()       'Bookの保存
                    Call steWorkbooksClose()     'Bookのクローズ


                    gbl.ActivePrinter.Copies = 1    'INSERT 2014/02/20 AOKI

                    'プレビュー画面の起動
                    Dim frm As New SAHANBAI.frmPrintPreview
                    With frm

                        .ppvActCell = pstPrevArea
                        .ShowDialog()
                    End With


                Case enmPrintmode.PrintOut     '印刷
                    mCurRows = pstLastRow
                    Call CheckPages()
                    Call steWorkbooksClose()     'Bookのクローズ

            End Select


            JYUMR060TotalPrintProc = True

Exit_JYUMR060TotalPrintProc:

            loRec = Nothing
            Exit Function

        Catch Err As Exception
            Call ksExpMsgBox("Error JYUMR060TotalPrintProc : " & Err.Message, "E")
            Call steWorkbooksClose()        'Bookのクローズ
        End Try



    End Function

    '********************************************************************
    '**  名称    : Function CreateSQLText() String
    '**  機能    : データ読み込み用SQL作成処理
    '**  戻り値  : 作成されたSQL文字列
    '**  引数    : iSel 0:明細 1:合計
    '**  備考    :
    '********************************************************************
    Private Function CreateSQLText(iSel As Integer) As String

        Dim strSQL As String
        Dim i As Integer

        If iSel = 0 Then
            '出荷データ読込(明細)
            'UPDATE 2014/12/11 AOKI START -----------------------------------------------------------------------------------------
            '        strSQL = "SELECT *  FROM  HatD_Dat"
            strSQL = "SELECT     HatD_001 "
            strSQL = strSQL & " ,HatD_002 "
            strSQL = strSQL & " ,HatD_003 "
            strSQL = strSQL & " ,HatD_006 "
            '        strSQL = strSQL & " ,HatD_007 "                'DELETE 2015/11/26 AOKI
            strSQL = strSQL & " ,Sir_003 AS HatD_007 "      'INSERT 2015/11/26 AOKI
            strSQL = strSQL & " ,HatD_009 "
            strSQL = strSQL & " ,HatD_016 "
            strSQL = strSQL & " ,HatD_017 "
            strSQL = strSQL & " ,HatD_019 "
            strSQL = strSQL & " ,HatD_022 "
            strSQL = strSQL & " ,HatD_031 "
            strSQL = strSQL & " FROM  HatD_Dat "
            'UPDATE 2014/12/11 AOKI E N D -----------------------------------------------------------------------------------------

            strSQL = strSQL & " INNER JOIN Sir_Mst "        'INSERT 2015/11/26 AOKI
            strSQL = strSQL & " ON HatD_006 = Sir_001 "     'INSERT 2015/11/26 AOKI

            If chkYMD = 0 Then
                strSQL = strSQL & " WHERE HatD_016 >='" & SirDate(0) & "'"
                strSQL = strSQL & "   AND HatD_016 <='" & SirDate(1) & "'"
            Else
                strSQL = strSQL & " WHERE HatD_031 >='" & SirDate(0) & "'"
                strSQL = strSQL & "   AND HatD_031 <='" & SirDate(1) & "'"
            End If

            If modJYUMR060_numSCode_0.Value <> 0 Then
                strSQL = strSQL & " AND HatD_006 >= " & modJYUMR060_numSCode_0.Value
            End If

            If modJYUMR060_numSCode_1.Value <> 0 Then
                strSQL = strSQL & " AND HatD_006 <= " & modJYUMR060_numSCode_1.Value
            End If

            If modJYUMR060_numSCode_0.Value = 0 And modJYUMR060_numSCode_1.Value = 0 And SelSirCd(0) <> 0 Then
                strSQL = strSQL & "   AND ("
                For i = LBound(SelSirCd) To UBound(SelSirCd)
                    If i = LBound(SelSirCd) Then
                        strSQL = strSQL & " HatD_006 = " & SelSirCd(i)
                    Else
                        strSQL = strSQL & " OR HatD_006 = " & SelSirCd(i)
                    End If
                Next i
                strSQL = strSQL & ")"
            End If

            If modJYUMR060_txtKeiyakuNo.Text <> "" Then
                strSQL = strSQL & " AND HatD_001 = '" & modJYUMR060_txtKeiyakuNo.Text & "'"
            End If
            'UPDATE 2014/02/20 AOKI E N D -------------------------------------------------------------------------------------


            strSQL = strSQL & " AND HatD_017 > 0 "

            strSQL = strSQL & " UNION ALL "

            strSQL = strSQL & " SELECT  ZaiD_001 AS HatD_001 "
            strSQL = strSQL & "        ,ZaiD_002 AS HatD_002 "
            strSQL = strSQL & "        ,ZaiD_003 AS HatD_003 "
            strSQL = strSQL & "        ,ZaiD_007 AS HatD_006 "
            strSQL = strSQL & "        ,Sir_003 AS HatD_007 "
            strSQL = strSQL & "        ,ZaiD_005 AS HatD_009 "
            strSQL = strSQL & "        ,ZaiD_020 AS HatD_016 "
            strSQL = strSQL & "        ,SUM(ZaiD_021) AS HatD_017 "
            strSQL = strSQL & "        ,ZaiD_026 AS HatD_019 "
            strSQL = strSQL & "        ,SUM(ZaiD_021 * ZaiD_026) AS HatD_022 "
            strSQL = strSQL & "        ,ZaiD_028 AS HatD_031 "
            strSQL = strSQL & " FROM  ZaiD_Dat "

            strSQL = strSQL & " INNER JOIN Sir_Mst "                    'INSERT 2015/11/26 AOKI
            strSQL = strSQL & " ON ZaiD_007 = Sir_001 "                 'INSERT 2015/11/26 AOKI

            If chkYMD = 0 Then
                strSQL = strSQL & " WHERE ZaiD_020 >='" & SirDate(0) & "'"
                strSQL = strSQL & "   AND ZaiD_020 <='" & SirDate(1) & "'"
            Else
                strSQL = strSQL & " WHERE ZaiD_028 >='" & SirDate(0) & "'"
                strSQL = strSQL & "   AND ZaiD_028 <='" & SirDate(1) & "'"
            End If

            If modJYUMR060_numSCode_0.Value <> 0 Then
                strSQL = strSQL & " AND ZaiD_007 >= " & modJYUMR060_numSCode_0.Value
            End If

            If modJYUMR060_numSCode_1.Value <> 0 Then
                strSQL = strSQL & " AND ZaiD_007 <= " & modJYUMR060_numSCode_1.Value
            End If

            If modJYUMR060_numSCode_0.Value = 0 And modJYUMR060_numSCode_1.Value = 0 And SelSirCd(0) <> 0 Then
                strSQL = strSQL & "   AND ("
                For i = LBound(SelSirCd) To UBound(SelSirCd)
                    If i = LBound(SelSirCd) Then
                        strSQL = strSQL & " ZaiD_007 = " & SelSirCd(i)
                    Else
                        strSQL = strSQL & " OR ZaiD_007 = " & SelSirCd(i)
                    End If
                Next i
                strSQL = strSQL & ")"
            End If

            If modJYUMR060_txtKeiyakuNo.Text <> "" Then
                strSQL = strSQL & " AND ZaiD_001 = '" & modJYUMR060_txtKeiyakuNo.Text & "'"
            End If


            strSQL = strSQL & " AND ZaiD_021 > 0 "                      'INSERT 2018/02/05 AOKI


            strSQL = strSQL & "GROUP BY Zaid_001 "
            strSQL = strSQL & "        ,ZaiD_002 "
            strSQL = strSQL & "        ,ZaiD_003 "
            strSQL = strSQL & "        ,ZaiD_007 "
            '                    strSQL = strSQL & "        ,ZaiD_008 "                     'DELETE 2015/11/26 AOKI
            strSQL = strSQL & "        ,Sir_003 "                       'INSERT 2015/11/26 AOKI
            strSQL = strSQL & "        ,ZaiD_005 "
            strSQL = strSQL & "        ,ZaiD_020 "
            strSQL = strSQL & "        ,ZaiD_026 "
            strSQL = strSQL & "        ,ZaiD_028 "

            If chkYMD = 0 Then
                strSQL = strSQL & " ORDER BY HatD_006,HatD_016,HatD_001,HatD_002,HatD_003"
            Else
                strSQL = strSQL & " ORDER BY HatD_006,HatD_031,HatD_001,HatD_002,HatD_003"
            End If
            'INSERT 2014/12/11 AOKI E N D -----------------------------------------------------------------------------------------

        Else

            If chkYMD = 0 Then
                strSQL = "SELECT HatD_006,Sir_003 AS HatD_007,HatD_016,HatD_001,SUM(HatD_022) AS HatD_022,SUM(Zei) AS HatD_Zei "    'INSERT 2015/11/26 AOKI
                strSQL = strSQL & "  FROM ( "
                strSQL = strSQL & "        SELECT *, "
                strSQL = strSQL & "               (CASE Sir_020 "
                strSQL = strSQL & "                WHEN 2 THEN CEILING(HatD_022 * " & ConMst(0).Con_034 & ") "
                strSQL = strSQL & "                WHEN 3 THEN FLOOR(HatD_022 * " & ConMst(0).Con_034 & ") "
                strSQL = strSQL & "                ELSE ROUND(HatD_022 * " & ConMst(0).Con_034 & ",0) "
                strSQL = strSQL & "                END) AS Zei "
                strSQL = strSQL & "        FROM   HatD_Dat "
                strSQL = strSQL & "        INNER JOIN Sir_Mst "
                strSQL = strSQL & "        ON HatD_006 = Sir_001 "
                strSQL = strSQL & "        WHERE  HatD_016 >='" & SirDate(0) & "'"
                strSQL = strSQL & "        AND    HatD_016 <='" & SirDate(1) & "'"
            Else
                strSQL = "SELECT HatD_006,Sir_003 AS HatD_007,HatD_031,HatD_001,SUM(HatD_022) AS HatD_022,SUM(Zei) AS HatD_Zei "    'INSERT 2015/11/26 AOKI
                strSQL = strSQL & "  FROM ( "
                strSQL = strSQL & "        SELECT *, "
                strSQL = strSQL & "               (CASE Sir_020 "
                strSQL = strSQL & "                WHEN 2 THEN CEILING(HatD_022 * " & ConMst(0).Con_034 & ") "
                strSQL = strSQL & "                WHEN 3 THEN FLOOR(HatD_022 * " & ConMst(0).Con_034 & ") "
                strSQL = strSQL & "                ELSE ROUND(HatD_022 * " & ConMst(0).Con_034 & ",0) "
                strSQL = strSQL & "                END) AS Zei "
                strSQL = strSQL & "        FROM   HatD_Dat "
                strSQL = strSQL & "        INNER JOIN Sir_Mst "
                strSQL = strSQL & "        ON HatD_006 = Sir_001 "
                strSQL = strSQL & "        WHERE  HatD_031 >='" & SirDate(0) & "'"
                strSQL = strSQL & "        AND    HatD_031 <='" & SirDate(1) & "'"
            End If

            If modJYUMR060_numSCode_0.Value <> 0 Then
                strSQL = strSQL & " AND HatD_006 >= " & modJYUMR060_numSCode_0.Value
            End If

            If modJYUMR060_numSCode_1.Value <> 0 Then
                strSQL = strSQL & " AND HatD_006 <= " & modJYUMR060_numSCode_1.Value
            End If

            If modJYUMR060_numSCode_0.Value = 0 And modJYUMR060_numSCode_1.Value = 0 And SelSirCd(0) <> 0 Then
                strSQL = strSQL & "   AND ("
                For i = LBound(SelSirCd) To UBound(SelSirCd)
                    If i = LBound(SelSirCd) Then
                        strSQL = strSQL & " HatD_006 = " & SelSirCd(i)
                    Else
                        strSQL = strSQL & " OR HatD_006 = " & SelSirCd(i)
                    End If
                Next i
                strSQL = strSQL & ")"
            End If

            If modJYUMR060_txtKeiyakuNo.Text <> "" Then
                strSQL = strSQL & " AND HatD_001 = '" & modJYUMR060_txtKeiyakuNo.Text & "'"
            End If
            'UPDATE 2014/02/20 AOKI E N D -------------------------------------------------------------------------------------


            strSQL = strSQL & " AND HatD_017 > 0 "                                  'INSERT 2018/02/05 AOKI


            strSQL = strSQL & " ) AS HD "       'INSERT 2014/08/12 AOKI

            If chkYMD = 0 Then
                '            strSQL = strSQL & " GROUP BY HatD_006,HatD_007,HatD_016,HatD_001"      'DELETE 2015/11/26 AOKI
                strSQL = strSQL & " GROUP BY HatD_006,Sir_003,HatD_016,HatD_001"        'INSERT 2015/11/26 AOKI
            Else
                '            strSQL = strSQL & " GROUP BY HatD_006,HatD_007,HatD_031,HatD_001"      'DELETE 2015/11/26 AOKI
                strSQL = strSQL & " GROUP BY HatD_006,Sir_003,HatD_031,HatD_001"        'INSERT 2015/11/26 AOKI
            End If
            'UPDATE 2014/12/11 AOKI E N D -----------------------------------------------------------------------------------------

            'INSERT 2014/12/11 AOKI START -----------------------------------------------------------------------------------------
            strSQL = strSQL & " UNION ALL "

            If chkYMD = 0 Then
                strSQL = strSQL & " SELECT ZaiD_007 AS HatD_006 "
                '                        strSQL = strSQL & "       ,ZaiD_008 AS HatD_007 "          'DELETE 2015/11/26 AOKI
                strSQL = strSQL & "       ,Sir_003 AS HatD_007 "            'INSERT 2015/11/26 AOKI
                strSQL = strSQL & "       ,ZaiD_020 AS HatD_016 "
                strSQL = strSQL & "       ,ZaiD_001 AS HatD_001 "
                strSQL = strSQL & "       ,SUM(ZaiD_021 * ZaiD_026) AS HatD_022 "
                strSQL = strSQL & "       ,SUM(Zei) AS HatD_Zei "
                strSQL = strSQL & " FROM ( "
                strSQL = strSQL & "       SELECT *, "
                strSQL = strSQL & "            (CASE Sir_020 "
                strSQL = strSQL & "             WHEN 2 THEN CEILING((ZaiD_021 * ZaiD_026) * " & ConMst(0).Con_034 & ") "
                strSQL = strSQL & "             WHEN 3 THEN FLOOR((ZaiD_021 * ZaiD_026) * " & ConMst(0).Con_034 & ") "
                strSQL = strSQL & "             ELSE ROUND((ZaiD_021 * ZaiD_026) * " & ConMst(0).Con_034 & ",0) "
                strSQL = strSQL & "             END) AS Zei "
                strSQL = strSQL & "       FROM   ZaiD_Dat "
                strSQL = strSQL & "       INNER JOIN Sir_Mst "
                strSQL = strSQL & "       ON ZaiD_007 = Sir_001 "
                strSQL = strSQL & "       WHERE  ZaiD_020 >='" & SirDate(0) & "'"
                strSQL = strSQL & "       AND    ZaiD_020 <='" & SirDate(1) & "'"
            Else
                strSQL = strSQL & " SELECT ZaiD_007 AS HatD_006 "
                '                        strSQL = strSQL & "       ,ZaiD_008 AS HatD_007 "          'DELETE 2015/11/26 AOKI
                strSQL = strSQL & "       ,Sir_003 AS HatD_007 "            'INSERT 2015/11/26 AOKI
                strSQL = strSQL & "       ,ZaiD_028 AS HatD_031 "
                strSQL = strSQL & "       ,ZaiD_001 AS HatD_001 "
                strSQL = strSQL & "       ,SUM(ZaiD_021 * ZaiD_026) AS HatD_022 "
                strSQL = strSQL & "       ,SUM(Zei) AS HatD_Zei "
                strSQL = strSQL & "  FROM ( "
                strSQL = strSQL & "        SELECT *, "
                strSQL = strSQL & "               (CASE Sir_020 "
                strSQL = strSQL & "                WHEN 2 THEN CEILING((ZaiD_021 * ZaiD_026) * " & ConMst(0).Con_034 & ") "
                strSQL = strSQL & "                WHEN 3 THEN FLOOR((ZaiD_021 * ZaiD_026) * " & ConMst(0).Con_034 & ") "
                strSQL = strSQL & "                ELSE ROUND((ZaiD_021 * ZaiD_026) * " & ConMst(0).Con_034 & ",0) "
                strSQL = strSQL & "                END) AS Zei "
                strSQL = strSQL & "        FROM   ZaiD_Dat "
                strSQL = strSQL & "        INNER JOIN Sir_Mst "
                strSQL = strSQL & "        ON ZaiD_007 = Sir_001 "
                strSQL = strSQL & "        WHERE  ZaiD_028 >='" & SirDate(0) & "'"
                strSQL = strSQL & "        AND    ZaiD_028 <='" & SirDate(1) & "'"
            End If

            If modJYUMR060_numSCode_0.Value <> 0 Then
                strSQL = strSQL & " AND ZaiD_007 >= " & modJYUMR060_numSCode_0.Value
            End If

            If modJYUMR060_numSCode_1.Value <> 0 Then
                strSQL = strSQL & " AND ZaiD_007 <= " & modJYUMR060_numSCode_1.Value
            End If

            If modJYUMR060_numSCode_0.Value = 0 And modJYUMR060_numSCode_1.Value = 0 And SelSirCd(0) <> 0 Then
                strSQL = strSQL & "   AND ("
                For i = LBound(SelSirCd) To UBound(SelSirCd)
                    If i = LBound(SelSirCd) Then
                        strSQL = strSQL & " ZaiD_007 = " & SelSirCd(i)
                    Else
                        strSQL = strSQL & " OR ZaiD_007 = " & SelSirCd(i)
                    End If
                Next i
                strSQL = strSQL & ")"
            End If

            If modJYUMR060_txtKeiyakuNo.Text <> "" Then
                strSQL = strSQL & " AND ZaiD_001 = '" & modJYUMR060_txtKeiyakuNo.Text & "'"
            End If


            strSQL = strSQL & " AND ZaiD_021 > 0 "                      'INSERT 2018/02/05 AOKI


            strSQL = strSQL & " ) AS HD "

            If chkYMD = 0 Then
                '                        strSQL = strSQL & " GROUP BY ZaiD_007,ZaiD_008,ZaiD_020,ZaiD_001"          'DELETE 2015/11/26 AOKI
                strSQL = strSQL & " GROUP BY ZaiD_007,Sir_003,ZaiD_020,ZaiD_001"            'INSERT 2015/11/26 AOKI
            Else
                '                        strSQL = strSQL & " GROUP BY ZaiD_007,ZaiD_008,ZaiD_028,ZaiD_001"          'DELETE 2015/11/26 AOKI
                strSQL = strSQL & " GROUP BY ZaiD_007,Sir_003,ZaiD_028,ZaiD_001"            'INSERT 2015/11/26 AOKI
            End If

            If chkYMD = 0 Then
                '                        strSQL = strSQL & " ORDER BY HatD_006,HatD_007,HatD_016,HatD_001"          'DELETE 2015/11/26 AOKI
                strSQL = strSQL & " ORDER BY HatD_006,Sir_003,HatD_016,HatD_001"            'INSERT 2015/11/26 AOKI
            Else
                '                        strSQL = strSQL & " ORDER BY HatD_006,HatD_007,HatD_031,HatD_001"          'DELETE 2015/11/26 AOKI
                strSQL = strSQL & " ORDER BY HatD_006,Sir_003,HatD_031,HatD_001"            'INSERT 2015/11/26 AOKI
            End If
            'INSERT 2014/12/11 AOKI E N D -----------------------------------------------------------------------------------------

        End If

        CreateSQLText = strSQL

    End Function
    '***************************************************************
    '**  名称    : Function GetTargetSheet() As Long
    '**  機能    : 出力モードとカレントページから対象シート番号を返す。
    '**  戻り値  : 対象となるExcelシート番号
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Function GetTargetSheet() As Long

        'モードによりシート番号を設定
        Select Case mIntMode
            Case 0  'プレビュー
                GetTargetSheet = mCurPage
            Case 1  '印刷
                GetTargetSheet = 1
        End Select

    End Function
    '***************************************************************
    '**  名称    : Sub CheckPages
    '**  機能    : 出力ページと行のチェックと改頁処理。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub CheckPages()

        '行をインクリメント
        mCurRows = mCurRows + 1

        '行チェック
        If mCurRows > pstLastRow Then
            mCurPage = mCurPage + 1     'ページインクリメント
            '改頁処理
            Select Case mIntMode
                Case 0  'プレビュー
                    Call stePrevSheetCopy(mCurPage)              'プレビュー・印刷用Bookのコピー
                    Call loSheetClear()                            'Excelシートクリア
                Case 1  '印刷
                    Call steExcelPrint(1, gbl.ActivePrinter.Printer) 'Excel 印刷
                    Call steSheetClear(1, pstPrintArea)          'Excelシートクリア
            End Select

            '''        mCurRows = pstStartRow - 1   '行をリセット
            mCurRows = pstStartRow   '行をリセット

            xlApp.Worksheets(GetTargetSheet).Cells(1, 13).Value = "PAGE - " & mCurPage        'INSERT 2014/03/18 AOKI
        End If

    End Sub

    '***************************************************************
    '**  名称    : Sub loSheetClear
    '**  機能    : シートのクリア
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub loSheetClear()

        Call steSheetClear(GetTargetSheet, pstPrintArea)

    End Sub
    '***************************************************************
    '**  名称    : Sub sttWriteHeader
    '**  機能    : ヘッダー情報をExcelに出力する。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub sttWriteHeader(vSirCd As String, vSirNm As String)

        Dim strWk As String

        With xlApp.Worksheets(GetTargetSheet)
            .Cells(1, 13).Value = "PAGE - " & mCurPage
            .Cells(2, 13).Value = "作成日 :" & Format(DateTime.Now, "yyyy年MM月dd日")         'INSERT 2019/02/13 AOKI
            '年月日
            If chkYMD = 0 Then
                strWk = "仕入日付範囲："
                .Cells(7, 2).Value = "仕入日付"
            Else
                strWk = "チェック日付範囲："
                .Cells(7, 2).Value = "ﾁｪｯｸ日付"
            End If
            strWk = strWk & Format(CDate(SirDate(0)), "yyyy年MM月dd日") & "～"          'INSERT 2019/02/13 AOKI
            strWk = strWk & Format(CDate(SirDate(1)), "yyyy年MM月dd日")                 'INSERT 2019/02/13 AOKI
            .Cells(4, 1).Value = strWk
            '仕入先情報
            .Cells(5, 1).Value = "仕入先　情報：" & vSirCd & Space(1) & vSirNm

            'UPDATE 2014/03/18 AOKI START -------------------------------------------------------------------------------------
            If modJYUMR060_optSelect_0.Checked = True Then
                .Cells(7, 8).Value = "数量"
                .Cells(7, 9).Value = "単価"
                .Cells(7, 10).Value = "金額"
            Else
                .Cells(7, 8).Value = "税抜金額"
                .Cells(7, 9).Value = "消 費 税"
                .Cells(7, 10).Value = "税込金額"
            End If
            'INSERT 2014/03/18 AOKI E N D -------------------------------------------------------------------------------------
        End With

    End Sub
    Private Sub sttWriteTotal(viMode As Integer, vcKingaku As Double,
                              vcZei As Double,
                              viSirKbn As Integer, viKbn19 As Integer,
                              viKbn20 As Integer)     'INSERT 2014/07/30 AOKI



        TaxCalcInfo.ZeiKbn = viKbn19                    '0:未選択 1:内税 2:外税
        If viKbn20 = 0 Then
            TaxCalcInfo.ZeiKeisan = viKbn20             '0:未選択
        Else
            TaxCalcInfo.ZeiKeisan = viKbn20 - 1         '1:四捨五入 2:切上げ 3:切捨て
        End If
        'UPDATE 2014/07/30 AOKI E N D -------------------------------------------------------------------------------------
        TaxCalcInfo.Kingaku = vcKingaku                 '金額
        TaxCalcInfo.DDate = psSirDate                   '仕入日付

        Call TaxComp.pTaxCalcCom(TaxCalcInfo)

        If modJYUMR060_optSelect_0.Checked = True Then
            '明細表示
            If viMode = 0 Then          '小計

                Select Case viSirKbn
'                Case 0, 1   '伝票単位                  'DELETE 2017/09/26 AOKI
                    Case 0, 1, 3    '伝票単位               'INSERT 2017/09/26 AOKI
                        Call CheckPages()

                        With xlApp.Worksheets(GetTargetSheet)
                            .Cells(mCurRows, 7).Value = "＊＊　小計(税抜)　＊＊"
                            .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.ZeinukiKingaku, "#,##0")
                        End With

                        Call CheckPages()

                        With xlApp.Worksheets(GetTargetSheet)
                            .Cells(mCurRows, 7).Value = "＊＊　消　費　税　＊＊"
                            .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.Shohizei, "#,##0")
                        End With

                        Call CheckPages()

                        With xlApp.Worksheets(GetTargetSheet)
                            .Cells(mCurRows, 7).Value = "＊＊　小計(税込)　＊＊"
                            .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.ZeikomiKingaku, "#,##0")
                        End With

                    Case 2      '請求単位
                        Call CheckPages()

                        With xlApp.Worksheets(GetTargetSheet)
                            .Cells(mCurRows, 7).Value = "＊＊　小計(税抜)　＊＊"
                            .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.ZeinukiKingaku, "#,##0")
                        End With


                End Select
                'UPDATE 2014/08/12 AOKI E N D -------------------------------------------------------------------------------------

            Else                        '合計
                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    .Cells(mCurRows, 7).Value = "＊＊　合計(税抜)　＊＊"
                    If viSirKbn = 2 Then    '請求単位
                        .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.ZeinukiKingaku, "#,##0")
                    Else                    '伝票単位
                        .Cells(mCurRows, 10).Value = Format(vcKingaku - vcZei, "#,##0")
                    End If
                End With

                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    .Cells(mCurRows, 7).Value = "＊＊　消　費　税　＊＊"
                    If viSirKbn = 2 Then    '請求単位
                        .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.Shohizei, "#,##0")
                    Else                    '伝票単位
                        .Cells(mCurRows, 10).Value = Format(vcZei, "#,##0")
                    End If
                End With

                Call CheckPages()

                With xlApp.Worksheets(GetTargetSheet)
                    .Cells(mCurRows, 7).Value = "＊＊　合計(税込)　＊＊"
                    If viSirKbn = 2 Then    '請求単位
                        .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.ZeikomiKingaku, "#,##0")
                    Else                    '伝票単位
                        .Cells(mCurRows, 10).Value = Format(vcKingaku, "#,##0")
                    End If
                End With
            End If
        Else
            '合計表示
            Call CheckPages()

            With xlApp.Worksheets(GetTargetSheet)
                .Cells(mCurRows, 7).Value = "＊＊　合　計　＊＊"

                If viSirKbn = 2 Then    '請求単位
                    .Cells(mCurRows, 8).Value = Format(TaxCalcInfo.ZeinukiKingaku, "#,##0")
                    .Cells(mCurRows, 9).Value = Format(TaxCalcInfo.Shohizei, "#,##0")
                    .Cells(mCurRows, 10).Value = Format(TaxCalcInfo.ZeikomiKingaku, "#,##0")
                Else                    '上記以外（伝票単位）
                    .Cells(mCurRows, 8).Value = Format(vcKingaku - vcZei, "#,##0")
                    .Cells(mCurRows, 9).Value = Format(vcZei, "#,##0")
                    .Cells(mCurRows, 10).Value = Format(vcKingaku, "#,##0")
                End If
            End With
        End If

    End Sub
    'UPDATE 2014/03/18 AOKI E N D -------------------------------------------------------------------------------------

    'INSERT 2017/04/12 AOKI START -----------------------------------------------------------------------------------------
    '***************************************************************
    '**  名称    : Function GoukeiIchiran() As Boolean
    '**  機能    : 仕入先別合計一覧表出力実行処理。
    '**  戻り値  : True;正常終了 , False;エラー
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function GoukeiIchiran() As Boolean

        Dim DCount As Long
        Dim strPath As String
        Dim strSQL As String
        Dim loRSet As New DataTable

        On Error GoTo GoukeiIchiran_Err

        GoukeiIchiran = False

        If modJYUMR060_lblMstList_1.Text = "仕入日付" Then
            chkYMD = 0
        Else
            chkYMD = 1
        End If


        strSQL = " SELECT "
        strSQL = strSQL & "  SZ.Sir_001 "
        strSQL = strSQL & " ,SZ.Sir_003 "
        strSQL = strSQL & " ,SUM(SZ.HatD_Sir) AS HatD_Sir "
        strSQL = strSQL & " ,SUM(SZ.HatD_CHK_ON) AS HatD_CHK_ON "
        strSQL = strSQL & " ,SUM(SZ.HatD_CHK_OFF) AS HatD_CHK_OFF "
        strSQL = strSQL & " FROM ( "

        strSQL = strSQL & " SELECT "
        strSQL = strSQL & "  Sir_001 "
        strSQL = strSQL & " ,Sir_003 "
        strSQL = strSQL & " ,SUM(HatD_022) AS HatD_Sir "
        strSQL = strSQL & " ,SUM(CASE WHEN ISNULL(HatD_031,'1900/1/1') > '1900/1/1' THEN HatD_022 ELSE 0 END) AS HatD_CHK_ON  "
        strSQL = strSQL & " ,SUM(CASE WHEN ISNULL(HatD_031,'1900/1/1') = '1900/1/1' THEN HatD_022 ELSE 0 END) AS HatD_CHK_OFF "
        strSQL = strSQL & " FROM  HatD_Dat "

        strSQL = strSQL & " INNER JOIN Sir_Mst "
        strSQL = strSQL & " ON HatD_006 = Sir_001 "

        If chkYMD = 0 Then
            strSQL = strSQL & " WHERE HatD_016 >='" & modJYUMR060_datSiireYMD_0.Value.ToShortDateString & "'"
            strSQL = strSQL & "   AND HatD_016 <='" & modJYUMR060_datSiireYMD_1.Value.ToShortDateString & "'"
        Else
            strSQL = strSQL & " WHERE HatD_031 >='" & modJYUMR060_datSiireYMD_0.Value.ToShortDateString & "'"
            strSQL = strSQL & "   AND HatD_031 <='" & modJYUMR060_datSiireYMD_1.Value.ToShortDateString & "'"
        End If

        strSQL = strSQL & " GROUP BY Sir_001 "
        strSQL = strSQL & "         ,Sir_003 "

        strSQL = strSQL & " UNION ALL "

        strSQL = strSQL & " SELECT "
        strSQL = strSQL & "  Sir_001 "
        strSQL = strSQL & " ,Sir_003 "
        strSQL = strSQL & " ,SUM(ZaiD_021 * ZaiD_026) AS HatD_Sir "
        strSQL = strSQL & " ,SUM(CASE WHEN ISNULL(ZaiD_028,'1900/1/1') > '1900/1/1' THEN ZaiD_021 * ZaiD_026 ELSE 0 END) AS HatD_CHK_ON  "
        strSQL = strSQL & " ,SUM(CASE WHEN ISNULL(ZaiD_028,'1900/1/1') = '1900/1/1' THEN ZaiD_021 * ZaiD_026 ELSE 0 END) AS HatD_CHK_OFF "
        strSQL = strSQL & " FROM  ZaiD_Dat "
        strSQL = strSQL & " INNER JOIN Sir_Mst "
        strSQL = strSQL & " ON ZaiD_007 = Sir_001 "

        If chkYMD = 0 Then
            strSQL = strSQL & " WHERE ZaiD_020 >='" & modJYUMR060_datSiireYMD_0.Value.ToShortDateString & "'"
            strSQL = strSQL & "   AND ZaiD_020 <='" & modJYUMR060_datSiireYMD_1.Value.ToShortDateString & "'"
        Else
            strSQL = strSQL & " WHERE ZaiD_028 >='" & modJYUMR060_datSiireYMD_0.Value.ToShortDateString & "'"
            strSQL = strSQL & "   AND ZaiD_028 <='" & modJYUMR060_datSiireYMD_1.Value.ToShortDateString & "'"
        End If

        strSQL = strSQL & " GROUP BY Sir_001 "
        strSQL = strSQL & "         ,Sir_003 "

        strSQL = strSQL & " ) AS SZ"

        strSQL = strSQL & " GROUP BY Sir_001 "
        strSQL = strSQL & "         ,Sir_003 "

        strSQL = strSQL & " ORDER BY Sir_001 "
        strSQL = strSQL & "         ,Sir_003 "

        Dim DB As New clsDB.DBService
        loRSet = DB.GetDatafromDB(strSQL)

        If loRSet.Rows.Count <= 0 Then
            loRSet = Nothing
            Exit Function
        End If


        '==========================================
        '   テンプレートExcelファイルのオープン
        '==========================================
        strPath = Trim$(clswinApi.StaticWinApi.WsReportDir) & "\" & psttREPORT_NAME & modHanbai.exXLSSuffix
        xlBookTem = xlApp.Workbooks.Open(strPath)
        '新規ブックとしてオープン
        xlBookTem.Activate()
        xlBookTem.Sheets(1).Select
        xlBookTem.Sheets(1).Copy
        xlBookNew = xlApp.ActiveWorkbook



        '===各変数初期化
        mCurPage = 1
        mCurRows = 8        '明細印刷開始行
        DCount = 0          'データカウンタ


        'ヘッダー情報
        With xlBookNew.Worksheets(mCurPage)
            .Range("B8:J73").Value = ""
        End With

        For Each dr As DataRow In loRSet.Rows


            '進行状況用カウンタ
            DCount = DCount + 1



            With xlBookNew.Worksheets(mCurPage)
                .Cells(mCurRows, 6).Value = CInt(dr("Sir_001"))                              '仕入先コード
                .Cells(mCurRows, 7).Value = NCnvN(dr("Sir_003"))                             '仕入先名
                .Cells(mCurRows, 8).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_Sir"))), "#,##0")           '仕入金額
                .Cells(mCurRows, 9).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_CHK_ON"))), "#,##0")        '仕入金額・チェック済
                .Cells(mCurRows, 10).Value = Format(CDbl(modHanbai.NCnvZ(dr("HatD_CHK_OFF"))), "#,##0")      '仕入金額・チェック未
            End With

            mCurRows = mCurRows + 1
        Next

        loRSet = Nothing


        '罫線
        With xlBookNew.Worksheets(mCurPage)
            .Range("F:F").NumberFormatLocal = "G/標準"

            .Range("B9:J10").Copy
            .Range("B11:J" & mCurRows - 1).PasteSpecial(Excel.XlPasteType.xlPasteFormats)
            .Range("B8:J" & mCurRows).RowHeight = 12

            .Range("B" & mCurRows - 1 & ":J" & mCurRows - 1).Borders(Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Excel.XlLineStyle.xlContinuous
            .Range("B" & mCurRows - 1 & ":J" & mCurRows - 1).Borders(Excel.XlBordersIndex.xlEdgeBottom).Weight = Excel.XlBorderWeight.xlThin
            .Range("B" & mCurRows - 1 & ":J" & mCurRows - 1).Borders(Excel.XlBordersIndex.xlEdgeBottom).Color = RGB(0, 128, 0)

            .Range("H" & mCurRows & ":J" & mCurRows).NumberFormatLocal = "G/標準"
            .Range("H" & mCurRows & ":J" & mCurRows).ColumnWidth = 13

            .Cells(4, 1).Value = "仕入日付範囲：" & Format(modJYUMR060_datSiireYMD_0.Value, "平成ee年MM月dd日") & "～" & Format(modJYUMR060_datSiireYMD_1.Value, "平成ee年MM月dd日")
            .Cells(5, 1).Value = ""
            .Cells(1, 13).Value = ""
            .Cells(2, 13).Value = Format(DateTime.Now, "平成ee年MM月dd日")
            .Cells(7, 8).Value = "仕入金額"
            .Cells(7, 9).Value = "チェック済み金額"
            .Cells(7, 10).Value = "未チェック金額"

            .Cells(mCurRows, 8).Value = "=SUM(H8:H" & mCurRows - 1 & ")"
            .Cells(mCurRows, 9).Value = "=SUM(I8:I" & mCurRows - 1 & ")"
            .Cells(mCurRows, 10).Value = "=SUM(J8:J" & mCurRows - 1 & ")"
        End With


        xlBookTem.Close(SaveChanges:=False)
        xlBookTem = Nothing



        GoukeiIchiran = True

        xlApp.Visible = True
        xlApp.ActiveWindow.WindowState = Excel.XlWindowState.xlMaximized
        xlBookNew.Worksheets(mCurPage).Select
        'Range("A5").Select

Exit_GoukeiIchiran:

        Exit Function

GoukeiIchiran_Err:

        Call ksExpMsgBox(Err.Number & " : " & Err.Description, "E")
        xlBookTem.Close(SaveChanges:=False)
        xlBookTem = Nothing

    End Function
#Region "ADDED"



    Private Function steExcelOpen(xlsName As String) As Boolean
        xlApp.Workbooks.Open(steCreatePath(xlsName))
        steExcelOpen = True
        Return steExcelOpen
    End Function


    Private Function steCreatePath(ByVal fname As String) As String
        If fname <> "" Then
            steCreatePath = winApi.WsReportDir & "\" + fname

        Else
            steCreatePath = fname
        End If

        Return steCreatePath
    End Function

    Public Sub stePrevWorkDelete()
        If stePrevFileCheck() <> "" Then
            Dim str As String = steCreateWorkPrev(1)
            Dim sDir As String = Path.GetDirectoryName(str)
            Dim sWild As String = Path.GetFileName(str)
            Dim vDir = New DirectoryInfo(sDir)

            For Each file In vDir.EnumerateFiles(sWild)
                file.Delete()
            Next
        End If
    End Sub

    Public Function stePrevFileCheck() As String

        If File.Exists(steCreateWorkPrev(0)) Then
            stePrevFileCheck = steCreateWorkPrev(0)
        End If

        Return stePrevFileCheck
    End Function

    Public Function steCreateWorkPrev(ByVal viMode As Integer) As String

        Select Case viMode
            Case 0
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk +
                   gstStrSEPFILE +
                    winApi.Wsnumber + modHanbai.exXLSSuffix
            Case 1
                steCreateWorkPrev = winApi.WsReportDir + gstStrSEPPATH +
                    modHanbai.exXLSPrevWk + gstStrSEPFILE +
                     winApi.Wsnumber + modHanbai.exXLSWildCd
        End Select

        Return steCreateWorkPrev

    End Function

    Private Sub stePrevBookSave()
        xlApp.ActiveWorkbook.SaveAs(Filename:=steCreateWorkPrev(0),
                                    FileFormat:=Excel.XlFileFormat.xlWorkbookNormal,
                                    ReadOnlyRecommended:=False, CreateBackup:=False)
    End Sub


    Private Sub steWorkbooksClose()
        If xlApp.Workbooks.Count > 0 Then

            For Each wb As Excel.Workbook In xlApp.Workbooks
                wb.Close(SaveChanges:=False)
            Next

            If xlApp IsNot Nothing Then
                xlApp.Quit()
                xlApp = Nothing
            End If
        End If
    End Sub

    Public Sub stePrevSheetCopy(ByVal vlPage As Long)
        If vlPage > 1 Then
            xlApp.Worksheets(vlPage - 1).Copy(After:=xlApp.Sheets(vlPage - 1))
        End If
    End Sub



    Private Sub steExcelPrint(ByVal vlSheet As Long, ByVal viPrinter As Object, ByVal Optional viCopies As Integer = 1)
        Dim ws As Excel.Worksheet = New Excel.Worksheet()
        ws = xlApp.Worksheets(vlSheet)

        If viPrinter Is Nothing Then
            ws.PrintOutEx(Copies:=viCopies, Collate:=True)
        Else
            ws.PrintOutEx(Copies:=viCopies, Collate:=True, ActivePrinter:=viPrinter)
        End If
    End Sub

    Private Sub steSheetClear(ByVal vlSheet As Long, ByVal vsCell As String)
        xlApp.Worksheets(vlSheet).Range(vsCell).ClearContents()
    End Sub
#End Region
End Module
