﻿
Imports System.Windows.Forms


Public Module modZAIMR010



    Dim DB As clsDB.DBService

    '***************************************************************
    '**
    '**  プログラムＩＤ :  ZAIMR010
    '**  機能           :  在庫発注／仕入入力(modZAIMR010)
    '**  備考           :
    '**
    '***************************************************************

    '==== 定数 ==============================================================================
    'ﾌﾟﾛｸﾞﾗﾑID(在庫発注／仕入入力)
    Public Const pstmPGID As String = "ZAIMR010"

    'スプレッド列番号
    Public Const scSirCode As Integer = 1
    Public Const scSirName As Integer = 2
    Public Const scSirbtn As Integer = 3
    Public Const scNo As Integer = 4
    Public Const scRen As Integer = 5
    Public Const scSyoName As Integer = 6
    Public Const scSyobtn As Integer = 7
    Public Const scPartNo As Integer = 8
    Public Const scHSuryo As Integer = 9
    Public Const scHTanka As Integer = 10
    Public Const scHGaku As Integer = 11
    Public Const scSSuryo As Integer = 12
    Public Const scSTani As Integer = 13
    Public Const scSTanNet As Integer = 14
    Public Const scSGaku As Integer = 15
    Public Const scSDate As Integer = 16
    Public Const scChkBox As Integer = 17
    Public Const scChkDate As Integer = 18
    Public Const scKeiyaku As Integer = 19

    'メッセージ
    Public Const ZAIMR010_MSG002 As String = "前SEQのデータが存在しません。"
    Public Const ZAIMR010_MSG003 As String = "次SEQのデータが存在しません。"
    '========================================================================================

    '==== 変数 ==============================================================================
    Public strSQL As String       'SQL文作成用
    Public strMsg As String       'エラーメッセージ作成用
    Public lngErr As Long         'エラー番号

    Public ZaiHDat As ZaiHDatInfo  '在庫データ(ヘッダ)
    Public ZaiDDat() As ZaiDDatInfo  '在庫データ(明細)

    '在庫データ(ヘッダ)情報
    Public Structure ZaiHDatInfo
        Public ZaiH_001 As String       '注文№
        Public ZaiH_002 As String       '発注日
        Public ZaiH_003 As String       '当社担当者
        Public ZaiH_004 As String       'COMMENT1
        Public ZaiH_005 As Integer      'SEQ
        Public ZaiH_006 As String       'UNIT
        Public ZaiH_007 As String       'MAKER
        Public ZaiH_008 As String       'TYPE
        Public ZaiH_009 As String       'SER/NO
        Public ZaiH_010 As String       'DWG/NO
        Public ZaiH_inst As String       '登録日
        Public ZaiH_updt As String       '更新日
        Public ZaiH_WsNo As String       'WsNo
    End Structure

    '在庫データ(明細)情報
    Public Structure ZaiDDatInfo
        Public ZaiD_001 As String       '注文№
        Public ZaiD_002 As Integer      'SEQ１
        Public ZaiD_003 As Integer      '項番１
        Public ZaiD_004 As Integer      '連番１
        Public ZaiD_001k As String       '契約№
        Public ZaiD_002k As Integer      'SEQ２
        Public ZaiD_003k As Integer      '項番２
        Public ZaiD_004k As Integer      '連番２
        Public ZaiD_005 As String       '品名
        Public ZaiD_006 As String       '品番
        Public ZaiD_007 As String       '仕入先コード
        Public ZaiD_008 As String       '仕入先名
        Public ZaiD_009 As String       '
        Public ZaiD_010 As String       '発注日付
        Public ZaiD_011 As Double     '発注数量
        Public ZaiD_012 As String       '発注単位
        Public ZaiD_013 As Double     '発注単価
        Public ZaiD_014 As Double     '発注金額
        Public ZaiD_015 As Double     '発注掛率
        Public ZaiD_016 As Double     '発注単価NET
        Public ZaiD_017 As Double     '
        Public ZaiD_018 As Double     '
        Public ZaiD_019 As Double     '
        Public ZaiD_020 As String       '仕入日付
        Public ZaiD_021 As Double     '仕入数量
        Public ZaiD_022 As String       '仕入単位
        Public ZaiD_023 As Double     '仕入単価
        Public ZaiD_024 As Double     '仕入金額
        Public ZaiD_025 As Double     '仕入掛率
        Public ZaiD_026 As Double     '仕入単価NET
        Public ZaiD_027 As Double     'チェック
        Public ZaiD_028 As String       'チェック日付
        Public ZaiD_029 As Double     '
        Public ZaiD_030 As Double     '売上予定数量
        Public ZaiD_031 As Double     '売上数量
        Public ZaiD_032 As Double     '調整数量
        Public ZaiD_033 As Double     '使用数量
        Public ZaiD_034 As Double     '在庫数量
        Public ZaiD_Flg1 As Integer      '数量変更不可フラグ
        Public ZaiD_Inst As String       '登録日
        Public ZaiD_Updt As String       '更新日
        Public ZaiD_WsNo As String       'WsNo
    End Structure

    'コントロールマスタ情報
    Public ConMst(0) As ConMstInfo

    Private datHacchuYMD As DateTimePicker
    '========================================================================================


    '***************************************************************
    '**  名称    : Sub ZaiDatReadProc()
    '**  機能    : 在庫データの読込み処理
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function ZaiDatReadProc(viMode As Integer, vsChumonNo As String,
                                   Optional viSeq As Integer = 0, Optional viCnt As Integer = 0) As Boolean

        Dim SQLtxt As String
        Dim loRSet As New DataTable
        Dim i As Integer

        ZaiDatReadProc = False

        i = 0
        Erase ZaiDDat '在庫データ内部変数クリア

        '在庫データ読込み用SQL文作成
        SQLtxt = CreateSQLText(viMode, vsChumonNo, viSeq, viCnt)

        '在庫データの読込み

        If DB Is Nothing Then DB = New clsDB.DBService
        loRSet = DB.GetDatafromDB(SQLtxt)

        If loRSet.Rows.Count > 0 Then

            With ZaiHDat
                .ZaiH_001 = NCnvN(loRSet.Rows(0)("ZaiH_001"))        '注文№
                .ZaiH_002 = NCnvN(loRSet.Rows(0)("ZaiH_002"))        '発注日
                .ZaiH_003 = NCnvN(loRSet.Rows(0)("ZaiH_003"))        '当社担当者
                .ZaiH_004 = NCnvN(loRSet.Rows(0)("ZaiH_004"))        'COMMENT1
                .ZaiH_005 = NCnvZ(loRSet.Rows(0)("ZaiH_005"))        'SEQ
                .ZaiH_006 = NCnvN(loRSet.Rows(0)("ZaiH_006"))        'UNIT
                .ZaiH_007 = NCnvN(loRSet.Rows(0)("ZaiH_007"))        'MAKER
                .ZaiH_008 = NCnvN(loRSet.Rows(0)("ZaiH_008"))        'TYPE
                .ZaiH_009 = NCnvN(loRSet.Rows(0)("ZaiH_009"))        'SER/NO
                .ZaiH_010 = NCnvN(loRSet.Rows(0)("ZaiH_010"))        'DWG/NO
            End With

            For Each dr As DataRow In loRSet.Rows
                ReDim Preserve ZaiDDat(i)
                With ZaiDDat(i)
                    .ZaiD_001 = NCnvN(dr("ZaiD_001"))        '注文№
                    .ZaiD_002 = NCnvZ(dr("ZaiD_002"))        'SEQ１
                    .ZaiD_003 = NCnvZ(dr("ZaiD_003"))        '項番１
                    .ZaiD_004 = NCnvZ(dr("ZaiD_004"))        '連番１
                    .ZaiD_001k = NCnvN(dr("ZaiD_001k"))      '契約№
                    .ZaiD_005 = NCnvN(dr("ZaiD_005"))        '品名
                    .ZaiD_006 = NCnvN(dr("ZaiD_006"))        '品番
                    .ZaiD_007 = NCnvN(dr("ZaiD_007"))        '仕入先コード
                    .ZaiD_008 = NCnvN(dr("ZaiD_008"))        '仕入先名
                    .ZaiD_011 = NCnvZ(dr("ZaiD_011"))        '発注数量
                    .ZaiD_013 = NCnvZ(dr("ZaiD_013"))        '発注単価
                    .ZaiD_014 = NCnvZ(dr("ZaiD_011")) * NCnvZ(dr("ZaiD_013"))     '発注金額   'INSERT 2012/12/07 AOKI
                    .ZaiD_020 = NCnvN(dr("ZaiD_020"))        '仕入日付
                    .ZaiD_021 = NCnvZ(dr("ZaiD_021"))        '仕入数量
                    .ZaiD_022 = NCnvN(dr("ZaiD_022"))        '仕入単位
                    .ZaiD_024 = NCnvZ(dr("ZaiD_021")) * NCnvZ(dr("ZaiD_026"))     '仕入金額   'INSERT 2012/12/07 AOKI
                    .ZaiD_026 = NCnvZ(dr("ZaiD_026"))        '仕入単価NET
                    .ZaiD_027 = NCnvZ(dr("ZaiD_027"))        'チェック
                    .ZaiD_028 = NCnvN(dr("ZaiD_028"))        'チェック日付
                End With

                i = i + 1
            Next
        Else
            GoTo Exit_ZaiDatReadProc
        End If

        ZaiDatReadProc = True
        Return ZaiDatReadProc
Exit_ZaiDatReadProc:
        loRSet = Nothing

    End Function

    '********************************************************************
    '**  名称    : Function CreateSQLText() String
    '**  機能    : SQL作成処理
    '**  戻り値  : 作成されたSQL文字列
    '**  引数    :
    '**  備考    :
    '********************************************************************
    Public Function CreateSQLText(viMode As Integer, vsChumonNo As String,
                                  Optional viSeq As Integer = 0, Optional viCnt As Integer = 0) As String

        Dim lsSQL As String


        lsSQL = "         INNER JOIN "
        lsSQL = lsSQL & "           ( "
        lsSQL = lsSQL & "            SELECT   ZaiD_001 "
        lsSQL = lsSQL & "                   , ZaiD_002 "
        lsSQL = lsSQL & "                   , ZaiD_003 "
        lsSQL = lsSQL & "                   , MAX(ZaiD_004)  AS ZaiD_004 "
        lsSQL = lsSQL & "                   , MAX(ZaiD_001k) AS ZaiD_001k "
        lsSQL = lsSQL & "                   , ZaiD_005 "
        lsSQL = lsSQL & "                   , ZaiD_006 "
        lsSQL = lsSQL & "                   , ZaiD_007 "
        lsSQL = lsSQL & "                   , ZaiD_008 "
        lsSQL = lsSQL & "                   , SUM(ZaiD_011)  AS ZaiD_011 "
        lsSQL = lsSQL & "                   , ZaiD_013 "
        lsSQL = lsSQL & "                   , SUM(ZaiD_014)  AS ZaiD_014 "
        lsSQL = lsSQL & "                   , SUM(ZaiD_021)  AS ZaiD_021 "
        lsSQL = lsSQL & "                   , ZaiD_022 "
        lsSQL = lsSQL & "                   , ZaiD_026 "
        lsSQL = lsSQL & "                   , SUM(ZaiD_024)  AS ZaiD_024 "
        lsSQL = lsSQL & "                   , ZaiD_020 "
        lsSQL = lsSQL & "                   , ZaiD_027 "
        lsSQL = lsSQL & "                   , ZaiD_028 "
        lsSQL = lsSQL & "            FROM     ZaiD_Dat "

        If viMode < 6 Then
            lsSQL = lsSQL & "        WHERE    ZaiD_001 = '" & vsChumonNo & "'"
        End If

        If viMode = 1 Then
            lsSQL = lsSQL & "        AND      ZaiD_002 =  " & viSeq
        End If

        lsSQL = lsSQL & "            GROUP BY ZaiD_001 "
        lsSQL = lsSQL & "                   , ZaiD_002 "
        lsSQL = lsSQL & "                   , ZaiD_003 "
        lsSQL = lsSQL & "                   , ZaiD_005 "
        lsSQL = lsSQL & "                   , ZaiD_006 "
        lsSQL = lsSQL & "                   , ZaiD_007 "
        lsSQL = lsSQL & "                   , ZaiD_008 "
        lsSQL = lsSQL & "                   , ZaiD_013 "
        lsSQL = lsSQL & "                   , ZaiD_022 "
        lsSQL = lsSQL & "                   , ZaiD_026 "
        lsSQL = lsSQL & "                   , ZaiD_020 "
        lsSQL = lsSQL & "                   , ZaiD_027 "
        lsSQL = lsSQL & "                   , ZaiD_028 "
        lsSQL = lsSQL & "           ) AS ZD "
        lsSQL = lsSQL & " ON        ZaiH_Dat.ZaiH_001 = ZD.ZaiD_001 "
        lsSQL = lsSQL & " AND       ZaiH_Dat.ZaiH_005 = ZD.ZaiD_002 "
        lsSQL = lsSQL & " ORDER BY  ZaiD_002 "
        lsSQL = lsSQL & "        ,  ZaiD_003 "



        Select Case viMode
            Case 0  '注文№でEnterキー
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiH_Dat "
                strSQL = strSQL & " INNER  JOIN "
                strSQL = strSQL & "            ( "
                strSQL = strSQL & "             SELECT   ZaiH_001 "
                strSQL = strSQL & "                  ,   MIN(ZaiH_005) AS ZH005 "
                strSQL = strSQL & "             FROM     ZaiH_Dat "
                strSQL = strSQL & "             GROUP BY ZaiH_001 "
                strSQL = strSQL & "            ) AS ZH "
                strSQL = strSQL & " ON         ZaiH_Dat.ZaiH_001 = ZH.ZaiH_001 "
                strSQL = strSQL & " AND        ZaiH_Dat.ZaiH_005 = ZH.ZH005 "
                strSQL = strSQL & lsSQL

            Case 1  'SEQでEnterキー
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiH_Dat "
                strSQL = strSQL & lsSQL

            Case 2  'SEQ数カウント
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiH_Dat "
                strSQL = strSQL & " WHERE  ZaiH_Dat.ZaiH_001 = '" & vsChumonNo & "'"

            Case 4  '←/SEQ
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiH_Dat "
                strSQL = strSQL & " INNER  JOIN "
                strSQL = strSQL & "            ( "
                strSQL = strSQL & "             SELECT   ZaiH_001 "
                strSQL = strSQL & "                  ,   MAX(ZaiH_005) AS ZH005 "
                strSQL = strSQL & "             FROM     ZaiH_Dat "
                strSQL = strSQL & "             WHERE    ZaiH_001 = '" & vsChumonNo & "'"
                strSQL = strSQL & "             AND      ZaiH_005 <  " & viSeq
                strSQL = strSQL & "             GROUP BY ZaiH_001 "
                strSQL = strSQL & "            ) AS ZH "
                strSQL = strSQL & " ON         ZaiH_Dat.ZaiH_001 = ZH.ZaiH_001 "
                strSQL = strSQL & " AND        ZaiH_Dat.ZaiH_005 = ZH.ZH005 "
                strSQL = strSQL & lsSQL

            Case 5  '→/SEQ
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiH_Dat "
                strSQL = strSQL & " INNER  JOIN "
                strSQL = strSQL & "            ( "
                strSQL = strSQL & "             SELECT   ZaiH_001 "
                strSQL = strSQL & "                  ,   MIN(ZaiH_005) AS ZH005 "
                strSQL = strSQL & "             FROM     ZaiH_Dat "
                strSQL = strSQL & "             WHERE    ZaiH_001 = '" & vsChumonNo & "'"
                strSQL = strSQL & "             AND      ZaiH_005 >  " & viSeq
                strSQL = strSQL & "             GROUP BY ZaiH_001 "
                strSQL = strSQL & "            ) AS ZH "
                strSQL = strSQL & " ON         ZaiH_Dat.ZaiH_001 = ZH.ZaiH_001 "
                strSQL = strSQL & " AND        ZaiH_Dat.ZaiH_005 = ZH.ZH005 "
                strSQL = strSQL & lsSQL

            Case 6  '←/F6
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiH_Dat "
                strSQL = strSQL & " INNER  JOIN "
                strSQL = strSQL & "            ( "
                strSQL = strSQL & "             SELECT   TOP 1 ZaiH_001 "
                strSQL = strSQL & "                  ,   MIN(ZaiH_005) AS ZH005 "
                strSQL = strSQL & "             FROM     ZaiH_Dat "
                strSQL = strSQL & "             WHERE    ZaiH_001 < '" & vsChumonNo & "'"
                strSQL = strSQL & "             GROUP BY ZaiH_001 "
                strSQL = strSQL & "             ORDER BY ZaiH_001 DESC "
                strSQL = strSQL & "            ) AS ZH "
                strSQL = strSQL & " ON         ZaiH_Dat.ZaiH_001 = ZH.ZaiH_001 "
                strSQL = strSQL & " AND        ZaiH_Dat.ZaiH_005 = ZH.ZH005 "
                strSQL = strSQL & lsSQL

            Case 7  '→/F7
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiH_Dat "
                strSQL = strSQL & " INNER  JOIN "
                strSQL = strSQL & "            ( "
                strSQL = strSQL & "             SELECT   TOP 1 ZaiH_001 "
                strSQL = strSQL & "                  ,   MIN(ZaiH_005) AS ZH005 "
                strSQL = strSQL & "             FROM     ZaiH_Dat "
                strSQL = strSQL & "             WHERE    ZaiH_001 > '" & vsChumonNo & "'"
                strSQL = strSQL & "             GROUP BY ZaiH_001 "
                strSQL = strSQL & "             ORDER BY ZaiH_001 "
                strSQL = strSQL & "            ) AS ZH "
                strSQL = strSQL & " ON         ZaiH_Dat.ZaiH_001 = ZH.ZaiH_001 "
                strSQL = strSQL & " AND        ZaiH_Dat.ZaiH_005 = ZH.ZH005 "
                strSQL = strSQL & lsSQL

            Case 8  '発注残用SELECT
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiD_Dat "
                strSQL = strSQL & " WHERE  ZaiD_001 = '" & vsChumonNo & "'"
                strSQL = strSQL & " AND    ZaiD_002 =  " & viSeq
                strSQL = strSQL & " AND    ZaiD_011 <> ZaiD_021 "

            Case 9  'MAX項目取得
                strSQL = ""
                strSQL = strSQL & " SELECT   MAX(ZaiD_003) AS ZD003 "
                strSQL = strSQL & " FROM     ZaiD_Dat "
                strSQL = strSQL & " WHERE    ZaiD_001 = '" & vsChumonNo & "'"
                strSQL = strSQL & " AND      ZaiD_002 =  " & viSeq
                strSQL = strSQL & " GROUP BY ZaiD_001 "
                strSQL = strSQL & "        , ZaiD_002 "

            Case 10 'INSERT(ヘッダ)
                With ZaiHDat
                    strSQL = ""
                    strSQL = strSQL & " INSERT INTO ZaiH_Dat( "
                    strSQL = strSQL & "  ZaiH_001 "                 '注文№
                    strSQL = strSQL & ", ZaiH_002 "                 '発注日
                    strSQL = strSQL & ", ZaiH_003 "                 '当社担当者
                    strSQL = strSQL & ", ZaiH_004 "                 'COMMENT1
                    strSQL = strSQL & ", ZaiH_005 "                 'SEQ
                    strSQL = strSQL & ", ZaiH_006 "                 'UNIT
                    strSQL = strSQL & ", ZaiH_007 "                 'MAKER
                    strSQL = strSQL & ", ZaiH_008 "                 'TYPE
                    strSQL = strSQL & ", ZaiH_009 "                 'SER/NO
                    strSQL = strSQL & ", ZaiH_010 "                 'DWG/NO
                    strSQL = strSQL & ", ZaiH_Insert "              '登録日
                    strSQL = strSQL & ", ZaiH_WsNo "                'WsNo
                    strSQL = strSQL & " ) VALUES ( "
                    strSQL = strSQL & " '" & modCommon.EditSQLAddSQuot(.ZaiH_001) & "'"       '注文№
                    strSQL = strSQL & ", " & .ZaiH_002                              '発注日
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiH_003) & "'"       '当社担当者
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiH_004) & "'"       'COMMENT1
                    strSQL = strSQL & ", " & .ZaiH_005                              'SEQ
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiH_006) & "'"       'UNIT
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiH_007) & "'"       'MAKER
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiH_008) & "'"       'TYPE
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiH_009) & "'"       'SER/NO
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiH_010) & "'"       'DWG/NO
                    strSQL = strSQL & ",'" & Now & "'"                              '登録日
                    strSQL = strSQL & ",'" & clswinApi.StaticWinApi.Wsnumber & "'"                      'WsNo
                    strSQL = strSQL & ")"
                End With

            Case 11 'INSERT(明細)
                With ZaiDDat(viCnt)
                    strSQL = ""
                    strSQL = strSQL & " INSERT INTO ZaiD_Dat( "
                    strSQL = strSQL & "  ZaiD_001 "                 '注文№
                    strSQL = strSQL & ", ZaiD_002 "                 'SEQ１
                    strSQL = strSQL & ", ZaiD_003 "                 '項番１
                    strSQL = strSQL & ", ZaiD_004 "                 '連番１"
                    strSQL = strSQL & ", ZaiD_001k "                '契約№
                    strSQL = strSQL & ", ZaiD_002k "                'SEQ２
                    strSQL = strSQL & ", ZaiD_003k "                '項番２
                    strSQL = strSQL & ", ZaiD_004k "                '連番２
                    strSQL = strSQL & ", ZaiD_005 "                 '品名
                    strSQL = strSQL & ", ZaiD_006 "                 '品番
                    strSQL = strSQL & ", ZaiD_007 "                 '仕入先コード
                    strSQL = strSQL & ", ZaiD_008 "                 '仕入先名
                    strSQL = strSQL & ", ZaiD_009 "                 'フラグ（行削除確認用）
                    strSQL = strSQL & ", ZaiD_010 "                 '発注日付
                    strSQL = strSQL & ", ZaiD_011 "                 '発注数量
                    strSQL = strSQL & ", ZaiD_013 "                 '発注単価
                    strSQL = strSQL & ", ZaiD_014 "                 '発注金額
                    strSQL = strSQL & ", ZaiD_020 "                 '仕入日付
                    strSQL = strSQL & ", ZaiD_021 "                 '仕入数量
                    strSQL = strSQL & ", ZaiD_022 "                 '仕入単位
                    strSQL = strSQL & ", ZaiD_024 "                 '仕入金額
                    strSQL = strSQL & ", ZaiD_026 "                 '仕入単価NET
                    strSQL = strSQL & ", ZaiD_027 "                 'チェック
                    strSQL = strSQL & ", ZaiD_028 "                 'チェック日付
                    strSQL = strSQL & ", ZaiD_030 "                 '売上予定数量
                    strSQL = strSQL & ", ZaiD_031 "                 '売上数量
                    strSQL = strSQL & ", ZaiD_032 "                 '調整数量
                    strSQL = strSQL & ", ZaiD_033 "                 '使用数量
                    strSQL = strSQL & ", ZaiD_034 "                 '在庫数量
                    strSQL = strSQL & ", ZaiD_Insert "              '登録日
                    strSQL = strSQL & ", ZaiD_WsNo "                'WsNo
                    strSQL = strSQL & " ) VALUES ( "
                    strSQL = strSQL & " '" & modCommon.EditSQLAddSQuot(.ZaiD_001) & "'"       '注文№
                    strSQL = strSQL & ", " & .ZaiD_002                              'SEQ１
                    strSQL = strSQL & ", " & .ZaiD_003                              '項目１
                    strSQL = strSQL & ", " & .ZaiD_004                              '連番１
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiD_001k) & "'"      '契約№
                    strSQL = strSQL & ", " & .ZaiD_002k                             'SEQ２
                    strSQL = strSQL & ", " & .ZaiD_003k                             '項番２
                    strSQL = strSQL & ", " & .ZaiD_004k                             '連番２
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiD_005) & "'"       '品名
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiD_006) & "'"       '品番
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiD_007) & "'"       '仕入先コード
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiD_008) & "'"       '仕入先名
                    strSQL = strSQL & ",'1'"                                        'フラグ（行削除確認用）
                    strSQL = strSQL & ", " & .ZaiD_010                              '発注日付
                    strSQL = strSQL & ", " & .ZaiD_011                              '発注数量
                    strSQL = strSQL & ", " & .ZaiD_013                              '発注単価
                    strSQL = strSQL & ", " & .ZaiD_014                              '発注金額
                    strSQL = strSQL & ", " & .ZaiD_020                              '仕入日付
                    strSQL = strSQL & ", " & .ZaiD_021                              '仕入数量
                    strSQL = strSQL & ",'" & modCommon.EditSQLAddSQuot(.ZaiD_022) & "'"       '仕入単位
                    strSQL = strSQL & ", " & .ZaiD_024                              '仕入金額
                    strSQL = strSQL & ", " & .ZaiD_026                              '仕入単価NET
                    strSQL = strSQL & ", " & .ZaiD_027                              'チェック
                    strSQL = strSQL & ", " & .ZaiD_028                              'チェック日付
                    strSQL = strSQL & ", " & .ZaiD_030                              '売上予定数量
                    strSQL = strSQL & ", " & .ZaiD_031                              '売上数量
                    strSQL = strSQL & ", " & .ZaiD_032                              '調整数量
                    strSQL = strSQL & ", " & .ZaiD_033                              '使用数量
                    strSQL = strSQL & ", " & .ZaiD_034                              '在庫数量
                    strSQL = strSQL & ",'" & Now & "'"                              '登録日
                    strSQL = strSQL & ",'" & clswinApi.StaticWinApi.Wsnumber & "'"                      'WsNo
                    strSQL = strSQL & ")"
                End With

            Case 12 'DELETE
                strSQL = ""
                strSQL = strSQL & " DELETE "
                strSQL = strSQL & " FROM   ZaiH_Dat "
                strSQL = strSQL & " WHERE  ZaiH_001 = '" & vsChumonNo & "'"
                strSQL = strSQL & " AND    ZaiH_005 =  " & viSeq

            Case 13 'DELETE
                strSQL = ""
                strSQL = strSQL & " DELETE "
                strSQL = strSQL & " FROM   ZaiD_Dat "
                strSQL = strSQL & " WHERE  ZaiD_001 = '" & vsChumonNo & "'"
                strSQL = strSQL & " AND    ZaiD_002 =  " & viSeq

            Case 14 'UPDATE
                With ZaiHDat
                    strSQL = ""
                    strSQL = strSQL & " UPDATE ZaiH_Dat "
                    strSQL = strSQL & " SET    ZaiH_002    =  " & .ZaiH_002                             '発注日
                    strSQL = strSQL & "    ,   ZaiH_003    = '" & modCommon.EditSQLAddSQuot(.ZaiH_003) & "'"      '当社担当者
                    strSQL = strSQL & "    ,   ZaiH_004    = '" & modCommon.EditSQLAddSQuot(.ZaiH_004) & "'"      'COMMENT1
                    strSQL = strSQL & "    ,   ZaiH_Update = '" & Now & "'"                             '更新日
                    strSQL = strSQL & "    ,   ZaiH_WsNo   = '" & clswinApi.StaticWinApi.Wsnumber & "'"                     'WsNo
                    strSQL = strSQL & " WHERE  ZaiH_001    = '" & vsChumonNo & "'"
                End With

            Case 15 'UPDATE
                With ZaiDDat(viCnt)
                    strSQL = ""
                    strSQL = strSQL & " UPDATE ZaiD_Dat "
                    strSQL = strSQL & " SET    ZaiD_010    =  " & .ZaiD_010                             '発注日
                    strSQL = strSQL & "    ,   ZaiD_Update = '" & Now & "'"                             '更新日
                    strSQL = strSQL & "    ,   ZaiD_WsNo   = '" & clswinApi.StaticWinApi.Wsnumber & "'"                     'WsNo
                    strSQL = strSQL & " WHERE  ZaiD_001    = '" & vsChumonNo & "'"
                End With

            Case 16 'UPDATE
                With ZaiDDat(viCnt)
                    strSQL = ""
                    strSQL = strSQL & " UPDATE ZaiD_Dat "
                    strSQL = strSQL & " SET    ZaiD_011 =  " & .ZaiD_021            '発注数量
                    strSQL = strSQL & "    ,   ZaiD_014 =  " & .ZaiD_014            '発注金額       'INSERT 2012/11/27 AOKI
                    strSQL = strSQL & "    ,   ZaiD_021 =  " & .ZaiD_021            '仕入数量       'INSERT 2012/12/06 AOKI
                    strSQL = strSQL & "    ,   ZaiD_024 =  " & .ZaiD_024            '仕入金額       'INSERT 2012/12/06 AOKI
                    strSQL = strSQL & " WHERE  ZaiD_001 = '" & .ZaiD_001 & "'"
                    strSQL = strSQL & " AND    ZaiD_002 =  " & .ZaiD_002
                    strSQL = strSQL & " AND    ZaiD_003 =  " & .ZaiD_003
                    strSQL = strSQL & " AND    ZaiD_004 =  " & .ZaiD_004
                End With

            Case 17 'SELECT：登録済みか確認用
                With ZaiDDat(viCnt)
                    strSQL = ""
                    strSQL = strSQL & " SELECT   * "
                    strSQL = strSQL & " FROM     ZaiD_Dat "
                    strSQL = strSQL & " WHERE    ZaiD_001 = '" & .ZaiD_001 & "'"
                    strSQL = strSQL & " AND      ZaiD_002 =  " & .ZaiD_002
                    strSQL = strSQL & " AND      ZaiD_003 =  " & .ZaiD_003
                    strSQL = strSQL & " ORDER BY ZaiD_001 "
                    strSQL = strSQL & "        , ZaiD_002 "
                    strSQL = strSQL & "        , ZaiD_003 "
                    strSQL = strSQL & "        , ZaiD_004 "
                End With

            Case 18 'UPDATE
                With ZaiDDat(viCnt)
                    strSQL = ""
                    strSQL = strSQL & " UPDATE ZaiD_Dat "
                    strSQL = strSQL & " SET    ZaiD_005    = '" & modCommon.EditSQLAddSQuot(.ZaiD_005) & "'"      '品名
                    strSQL = strSQL & "   ,    ZaiD_006    = '" & modCommon.EditSQLAddSQuot(.ZaiD_006) & "'"      '品番
                    strSQL = strSQL & "   ,    ZaiD_007    = '" & modCommon.EditSQLAddSQuot(.ZaiD_007) & "'"      '仕入先コード
                    strSQL = strSQL & "   ,    ZaiD_008    = '" & modCommon.EditSQLAddSQuot(.ZaiD_008) & "'"      '仕入先名
                    strSQL = strSQL & "   ,    ZaiD_009    = '1'"                                       'フラグ（行削除確認用）
                    If .ZaiD_Flg1 = 0 Then
                        strSQL = strSQL & " ,  ZaiD_011    =  " & .ZaiD_011                             '発注数量
                        strSQL = strSQL & " ,  ZaiD_021    =  " & .ZaiD_021                             '仕入数量
                    End If
                    strSQL = strSQL & "   ,    ZaiD_013    =  " & .ZaiD_013                             '発注単価
                    strSQL = strSQL & "   ,    ZaiD_014    =  ZaiD_011 * " & .ZaiD_013                  '発注金額
                    strSQL = strSQL & "   ,    ZaiD_020    =  " & .ZaiD_020                             '仕入日付
                    strSQL = strSQL & "   ,    ZaiD_022    = '" & modCommon.EditSQLAddSQuot(.ZaiD_022) & "'"      '仕入単位
                    strSQL = strSQL & "   ,    ZaiD_024    =  ZaiD_021 * " & .ZaiD_026                  '仕入金額
                    strSQL = strSQL & "   ,    ZaiD_026    =  " & .ZaiD_026                             '仕入単価NET
                    strSQL = strSQL & "   ,    ZaiD_027    =  " & .ZaiD_027                             'チェック
                    strSQL = strSQL & "   ,    ZaiD_028    =  " & .ZaiD_028                             'チェック日付
                    strSQL = strSQL & "   ,    ZaiD_Update = '" & Now & "'"                             '更新日
                    strSQL = strSQL & "   ,    ZaiD_WsNo   = '" & clswinApi.StaticWinApi.Wsnumber & "'"                     'WsNo
                    strSQL = strSQL & " WHERE  ZaiD_001    = '" & .ZaiD_001 & "'"
                    strSQL = strSQL & " AND    ZaiD_002    =  " & .ZaiD_002
                    strSQL = strSQL & " AND    ZaiD_003    =  " & .ZaiD_003
                End With

            Case 19 'UPDATE
                strSQL = ""
                strSQL = strSQL & " UPDATE ZaiD_Dat "
                strSQL = strSQL & " SET    ZaiD_009 = '0'"
                strSQL = strSQL & " WHERE  ZaiD_001 = '" & vsChumonNo & "'"
                strSQL = strSQL & " AND    ZaiD_002 =  " & viSeq

            Case 20 'DELETE
                strSQL = ""
                strSQL = strSQL & " DELETE FROM ZaiD_Dat "
                strSQL = strSQL & " WHERE       ZaiD_001 = '" & vsChumonNo & "'"
                strSQL = strSQL & " AND         ZaiD_002 =  " & viSeq
                strSQL = strSQL & " AND         ZaiD_009 = '0' "

            Case 21 'SELECT
                strSQL = ""
                strSQL = strSQL & " SELECT * "
                strSQL = strSQL & " FROM   ZaiD_Dat "
                strSQL = strSQL & " WHERE  ZaiD_001  = '" & vsChumonNo & "'"
                strSQL = strSQL & " AND    ZaiD_002  =  " & viSeq
                strSQL = strSQL & " AND    ZaiD_001k <> '' "

            Case 22 'UPDATE
                With ZaiHDat
                    strSQL = ""
                    strSQL = strSQL & " UPDATE ZaiH_Dat "
                    strSQL = strSQL & " SET    ZaiH_002    =  " & .ZaiH_002                             '発注日
                    strSQL = strSQL & "    ,   ZaiH_003    = '" & modCommon.EditSQLAddSQuot(.ZaiH_003) & "'"      '当社担当者
                    strSQL = strSQL & "    ,   ZaiH_004    = '" & modCommon.EditSQLAddSQuot(.ZaiH_004) & "'"      'COMMENT1
                    strSQL = strSQL & "    ,   ZaiH_006    = '" & modCommon.EditSQLAddSQuot(.ZaiH_006) & "'"      'UNIT
                    strSQL = strSQL & "    ,   ZaiH_007    = '" & modCommon.EditSQLAddSQuot(.ZaiH_007) & "'"      'MAKER
                    strSQL = strSQL & "    ,   ZaiH_008    = '" & modCommon.EditSQLAddSQuot(.ZaiH_008) & "'"      'TYPE
                    strSQL = strSQL & "    ,   ZaiH_009    = '" & modCommon.EditSQLAddSQuot(.ZaiH_009) & "'"      'SER/NO
                    strSQL = strSQL & "    ,   ZaiH_010    = '" & modCommon.EditSQLAddSQuot(.ZaiH_010) & "'"      'DWG/NO
                    strSQL = strSQL & "    ,   ZaiH_Update = '" & Now & "'"                             '更新日
                    strSQL = strSQL & "    ,   ZaiH_WsNo   = '" & clswinApi.StaticWinApi.Wsnumber & "'"                     'WsNo
                    strSQL = strSQL & " WHERE  ZaiH_001    = '" & vsChumonNo & "'"
                    strSQL = strSQL & " AND    ZaiH_005    =  " & viSeq
                End With

        End Select

        CreateSQLText = strSQL

    End Function

    '***************************************************************
    '**  名称    : Function GetNewOrderNo()
    '**  機能    : データ追加ＳＱＬを作成
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function GetNewOrderNo() As String

        Dim lsYY As String
        Dim lsSEQ As String

        GetNewOrderNo = ""

        '== コントロールマスタより最終注文番号取得 ========================
        If modHanbai.GetConMstInfo(ConMst(1)) = True Then
            '--- UPDATE 2012/11/26 AOKI START --------------------------------------------------------------
            '        If ConMst(1).Con_024 = 9999 Then
            '            lsSEQ = "0001"
            '        Else
            '            lsSEQ = Format(ConMst(1).Con_024 + 1, "0000")
            '        End If

            If ConMst(1).Con_024 = 99999 Then
                lsSEQ = "00001"
            Else
                lsSEQ = Format(ConMst(1).Con_024 + 1, "00000")
            End If
            '--- UPDATE 2012/11/26 AOKI E N D --------------------------------------------------------------
        End If

        lsYY = Mid(Format(datHacchuYMD.Value, "00000000"), 3, 2)

        GetNewOrderNo = "ST" & lsYY & "-" & lsSEQ

    End Function

    '***************************************************************
    '**  名称    : Function UpdateOrderNo()
    '**  機能    : コントロールマスタの最終見積№を更新する
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Function UpdateOrderNo(vsOrderNo As String) As Boolean

        Dim lcOrder As Double '注文№

        UpdateOrderNo = False

        '注文№算出
        lcOrder = CDbl(Right(vsOrderNo, Len(vsOrderNo) - InStr(1, vsOrderNo, "-")))

        '更新用SQL文作成
        '=====================================================================
        strSQL = "UPDATE Con_Mst SET "
        strSQL = strSQL & " Con_024 = " & lcOrder
        strSQL = strSQL & " WHERE Con_001 = 1"
        '=====================================================================

        'SQL文実行
        If DB Is Nothing Then DB = New clsDB.DBService
        If DB.ExecuteSql(strSQL, strMsg) = False Then
            GoTo Error_UpdateOrderNo
        End If

        UpdateOrderNo = True

        Exit Function

Error_UpdateOrderNo:

        Call ksExpMsgBox(gstGUIDE_E003 & " ERR: " & strMsg, "E") 'エラーメッセージ表示

    End Function


End Module
