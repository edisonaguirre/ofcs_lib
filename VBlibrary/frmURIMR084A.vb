﻿Imports System.Drawing
Imports System.Windows.Forms
Public Class frmURIMR084A
    Private L_OURREFNO As String
    Private RetMode As Boolean
    Private DB As clsDB.DBService

    Public Sub SET_OURREFNO(ByVal Value As String)
        L_OURREFNO = Value
    End Sub
    Public Function STATUS() As Boolean
        Return RetMode
    End Function
    Private Sub frmURIMR084A_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim i As Integer
        Dim lvWork As Object
        Dim ConMst1(0 To 0) As ConMstInfo

        Call EditClear()      'フォームクリア
        Call modHanbai.GetConMstInfo(ConMst1(0))
        txtSeikyu.Text = L_OURREFNO

        Call modCommon.SetCtrlDate(datNyuYMD, ConMst1(0).Con_016, "YMD")   '西暦和暦設定
        Call modCommon.SetCtrlDate(datKeiYMD, ConMst1(0).Con_016, "YMD")   '西暦和暦設定     'INSERT 2016/05/16 AOKI

        grid_spdHed_SetupGrid()
        grid_spdHed_GridFormat()
        optDsp_1.Checked = True
        txtSeikyu.Select()

    End Sub

    Private Sub EditClear(Optional p001 As String = "D")

        RetMode = False

        txtSeikyu.Text = ""

        txtCaution1.Visible = False  '警告ラベル非表示          'INSERT 2014/02/10 AOKI
        txtCaution2.Visible = False  '警告ラベル非表示          'INSERT 2014/02/10 AOKI
        txtCaution3.Visible = False  '警告ラベル非表示          'INSERT 2014/02/10 AOKI

        spdMeisai.Rows.Clear()

    End Sub



    Private Sub DataLoadSec()

        Dim strSQL As String
        Dim loRSet As New DataTable
        Dim loRSet2 As New DataTable
        Dim iRow As Integer
        Dim BrkSeq As Integer              'SEQブレークチェック用
        Dim BrkKomoku As Integer              '項目ブレークチェック用
        Dim JSuryoT As Double             '受注数量
        Dim HSuryoT As Double             '発注数量
        Dim SSuryoT As Double             '仕入数量
        Dim KSuryoT As Double             '梱包数量

        Dim ssTXT As Object


        Try


            Dim dgvr As DataGridViewRow

            'スプレッド明細情報のクリア
            With spdMeisai
                '.Row = -1
                '.Col = -1
                '.Action = 12
                '.MaxRows = 0

                ''セルのテキスト色、背景色を設定します。
                '.ForeColor = RGB(0, 0, 0)
                '.BackColor = RGB(255, 255, 255)
                .Columns(2 - 1).Visible = True
                .Columns(2 - 1).HeaderText = "仕入先"
                .Columns(2 - 1).Width = 200
                .Columns(3 - 1).Width = 250
                .Columns(3 - 1).HeaderText = "品名"
                .Columns(4 - 1).Width = 150
                .Columns(4 - 1).HeaderText = "品番"
                .Columns(5 - 1).Width = 60
                .Columns(5 - 1).HeaderText = "受注" & vbCrLf & "数量"
                .Columns(6 - 1).Width = 60
                .Columns(6 - 1).HeaderText = "発注" & vbCrLf & "数量"
                .Columns(7 - 1).Width = 60
                .Columns(7 - 1).HeaderText = "仕入" & vbCrLf & "数量"
                .Columns(8 - 1).Width = 60
                .Columns(8 - 1).HeaderText = "梱包" & vbCrLf & "数量"
                .Columns(9 - 1).Visible = False
                .Columns(9 - 1).Width = 60
                .Columns(9 - 1).HeaderText = "ｹｰｽ" & vbCrLf & "№"
                .Columns(10 - 1).Visible = False
                .Columns(10 - 1).Width = 70
                .Columns(10 - 1).HeaderText = "重量"
                .Columns(11 - 1).Visible = False
                .Columns(11 - 1).Width = 70
                .Columns(11 - 1).HeaderText = "サイズ"
                .Columns(12 - 1).Visible = False
                .Columns(12 - 1).Width = 70
                .Columns(12 - 1).HeaderText = "梱包日"
                .Columns(13 - 1).Visible = False
                .Columns(13 - 1).Width = 70
                .Columns(13 - 1).HeaderText = "出荷日"
                .Columns(14 - 1).Visible = False
                .Columns(14 - 1).Width = 100
                .Columns(14 - 1).HeaderText = "置き場所"
                .Columns(15 - 1).Visible = True
                .Columns(15 - 1).Width = 70
                .Columns(15 - 1).HeaderText = "出荷No."



            End With

            '受注/発注データ読込
            strSQL = "SELECT * FROM JyuH_DAT,JyuM_Dat,JyuD_Dat"
            strSQL = strSQL & " WHERE JyuH_001 = '" & modHanbai.NCnvN(txtSeikyu.Text) & "'"   '契約№
            strSQL = strSQL & "   AND JyuH_001 = JyuM_001"                          '契約№
            strSQL = strSQL & "   AND JyuM_001 = JyuD_001"                          '契約№
            strSQL = strSQL & "   AND JyuM_003 = JyuD_003"                          'SEQ
            strSQL = strSQL & " ORDER BY JyuM_003,JyuD_004"

            'データの読込み
            If DB Is Nothing Then DB = New clsDB.DBService
            loRSet = DB.GetDatafromDB(strSQL)

            If loRSet.Rows.Count = 0 Then

                GoTo DataLoadSec_Exit
            End If

            Call txtCaution_Disp(loRSet.Rows(0))

            txtEstimate.Text = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_009")) '見積No.        
            datNyuYMD.Value = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_018"))  '入荷予定日
            datKeiYMD.Value = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_002"))  '契約日     

            txtTokName.Text = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_003"))
            txtVessel.Text = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_006"))


            With spdMeisai
                spdMeisai.Rows.Clear()
                spdMeisai.AllowUserToAddRows = True
                JSuryoT = 0        '受注数量合計クリア
                HSuryoT = 0        '発注数量合計クリア
                SSuryoT = 0        '仕入数量合計クリア
                KSuryoT = 0        '梱包数量合計クリア
                BrkSeq = -9999     'SEQブレークチェック用
                BrkKomoku = -9999  '項目№ブレークチェック用
                iRow = 0

                If loRSet.Rows.Count > 0 Then
                    dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                    spdMeisai.Rows.Add(dgvr)
                End If



                For Each dr In loRSet.Rows
                    iRow = iRow + 1

                    'SEQ
                    If BrkSeq <> dr("JyuM_003") Then


                        iRow = iRow + 1
                        '中ヘッダー表示

                        If modHanbai.NCnvN(dr("JyuM_004")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "UNIT:    " & modHanbai.NCnvN(dr("JyuM_004"))
                            spdMeisai.Rows.Add(dgvr)
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_005")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "MAKER:   " & modHanbai.NCnvN(dr("JyuM_005"))
                            spdMeisai.Rows.Add(dgvr)
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_006")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "TYPE:    " & modHanbai.NCnvN(dr("JyuM_006"))
                            spdMeisai.Rows.Add(dgvr)
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_007")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "SER/NO:  " & modHanbai.NCnvN(dr("JyuM_007"))
                            spdMeisai.Rows.Add(dgvr)
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_008")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "DWG NO:  " & modHanbai.NCnvN(dr("JyuM_008"))
                            spdMeisai.Rows.Add(dgvr)
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_025")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "ADDITIONAL DETAILS " & modHanbai.NCnvN(dr("JyuM_025"))
                            spdMeisai.Rows.Add(dgvr)
                            iRow = iRow + 1
                        End If
                        BrkSeq = CDbl(modHanbai.NCnvZ(dr("JyuM_003")))
                        BrkKomoku = -9999
                        iRow = iRow + 1
                    End If

                    '項目
                    dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                    If Strings.Right(dr("JyuD_004"), 1) = "0" Then
                        'コメント行

                        dgvr.Cells(1 - 1).Value = ""
                        dgvr.Cells(3 - 1).Value = modHanbai.NCnvN(dr("JyuD_005"))

                    Else
                        If BrkKomoku <> CDbl(modHanbai.NCnvZ(dr("JyuD_004"))) Then
                            '通常行
                            If modHanbai.NCnvN(dr("JyuD_019")) <> "" Then

                                dgvr.Cells(1 - 1).Value = Strings.Left(dr("JyuD_004").ToString().Trim(), Len(dr("JyuD_004").ToString().Trim()) - 1)

                            End If
                            dgvr.Cells(5 - 1).Value = CDbl(modHanbai.NCnvZ(dr("JyuD_018")))
                            BrkKomoku = CDbl(modHanbai.NCnvZ(dr("JyuD_004")))
                            JSuryoT = JSuryoT + CDbl(modHanbai.NCnvZ(dr("JyuD_018")))     '受注数量合計

                        End If

                        dgvr.Cells(3 - 1).Value = modHanbai.NCnvN(dr("JyuD_005"))
                        dgvr.Cells(4 - 1).Value = modHanbai.NCnvN(dr("JyuD_006"))


                        '梱包数量
                        If CDbl(modHanbai.NCnvZ(dr("JyuD_0274"))) = -1 Then
                            dgvr.Cells(8 - 1).Value = "入完"


                        Else
                            dgvr.Cells(8 - 1).Value = Format(CDbl(modHanbai.NCnvZ(dr("JyuD_037"))), "#,##0")

                            KSuryoT = KSuryoT + CDbl(modHanbai.NCnvZ(dr("JyuD_037")))   '梱包数量合計
                        End If


                        '発注/仕入データ取得
                        strSQL = "SELECT * FROM HatD_Dat"
                        strSQL = strSQL & " WHERE HatD_001 ='" & modHanbai.NCnvN(txtSeikyu.Text) & "'"   '契約№
                        strSQL = strSQL & "   AND HatD_002 = " & CDbl(modHanbai.NCnvZ(dr("JyuD_003")))      'SEQ
                        strSQL = strSQL & "   AND HatD_003 = " & CDbl(modHanbai.NCnvZ(dr("JyuD_004")))    'SEQ
                        strSQL = strSQL & " ORDER BY HatD_004"

                        'データの読込み
                        loRSet2 = DB.GetDatafromDB(strSQL)

                        If loRSet2.Rows.Count > 0 Then

                            For Each dr2 As DataRow In loRSet2.Rows
                                If modHanbai.NCnvN(dr2("HatD_004")) > 1 Then
                                    iRow = iRow + 1
                                    dgvr.Cells(3 - 1).Value = modHanbai.NCnvN(dr("JyuD_005"))
                                    dgvr.Cells(4 - 1).Value = modHanbai.NCnvN(dr("JyuD_006"))
                                End If

                                dgvr.Cells(2 - 1).Value = modHanbai.NCnvN(dr2("HatD_007"))
                                dgvr.Cells(6 - 1).Value = CDbl(modHanbai.NCnvZ(dr2("HatD_011")))
                                dgvr.Cells(7 - 1).Value = CDbl(modHanbai.NCnvZ(dr2("HatD_017")))
                                HSuryoT = HSuryoT + CDbl(modHanbai.NCnvZ(dr2("HatD_011")))  '発注数量合計
                                SSuryoT = SSuryoT + CDbl(modHanbai.NCnvZ(dr2("HatD_017")))   '仕入数量合計


                                spdMeisai.Rows.Add(dgvr)
                            Next


                        Else
                            spdMeisai.Rows.Add(dgvr)

                        End If


                    End If


                Next

                dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                spdMeisai.Rows.Add(dgvr)
                iRow = iRow + 2
                dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone

                dgvr.Cells(3 - 1).Value = "＊＊ 合 計 ＊＊"
                dgvr.Cells(5 - 1).Value = CDbl(modHanbai.NCnvZ(JSuryoT))
                dgvr.Cells(6 - 1).Value = CDbl(modHanbai.NCnvZ(HSuryoT))
                dgvr.Cells(7 - 1).Value = CDbl(modHanbai.NCnvZ(SSuryoT))
                dgvr.Cells(8 - 1).Value = Format(CDbl(modHanbai.NCnvZ(KSuryoT)), "#,###")

                spdMeisai.Rows.Add(dgvr)

            End With

            'クローズ


            spdMeisai.AllowUserToAddRows = False
DataLoadSec_Exit:

            loRSet = Nothing
            Exit Sub

        Catch Err As Exception

            Call ksExpMsgBox("Error in DataLoadSec. " & Err.Message, "E")


        End Try



    End Sub


    Private Sub grid_spdHed_SetupGrid()

        Dim oGrd As DataGridView = Me.spdMeisai

        Dim chkcol As DataGridViewCheckBoxColumn
        Dim txtcol As DataGridViewTextBoxColumn
        Dim btncol As DataGridViewButtonColumn

        oGrd.AllowUserToAddRows = True
        oGrd.AllowUserToDeleteRows = False
        oGrd.AllowUserToResizeColumns = True
        oGrd.AllowUserToResizeRows = False
        oGrd.AllowUserToOrderColumns = True
        oGrd.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect
        oGrd.RowHeadersWidth = 55
        oGrd.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing
        oGrd.ShowEditingIcon = False
        oGrd.AutoGenerateColumns = False
        oGrd.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        oGrd.DefaultCellStyle.BackColor = System.Drawing.Color.LightCyan
        oGrd.DefaultCellStyle.Font = New System.Drawing.Font("Tahoma", 8.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CType(0, Byte)))
        oGrd.DefaultCellStyle.ForeColor = System.Drawing.SystemColors.ControlText
        oGrd.ColumnHeadersDefaultCellStyle.Font = New System.Drawing.Font("Tahoma", 9.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CType(0, Byte)))
        oGrd.RowHeadersDefaultCellStyle.Font = New System.Drawing.Font("Tahoma", 8.0F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (CType(0, Byte)))

        'oGrd.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.Highlight
        'oGrd.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.Control

        oGrd.Columns.Clear()


        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "col_01"
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "HatD_007"
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "JyuD_005"
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "JyuD_006"
        oGrd.Columns.Add(txtcol)
        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "JyuD_018"
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "HatD_011_JyuD_022" '6
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "HatD_017_JyuD_024" '7
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "JyuD_037_KnpD_009" '8
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "KnpD_006" '9
        oGrd.Columns.Add(txtcol)


        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "KnpD_013" '10
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "KnpD_021+KnpD_022+KnpD_023" '11
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "KnpD_015" '12
        txtcol.ValueType = GetType(String)
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "KnpD_018" '13
        txtcol.ValueType = GetType(String)
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "KnpD_014" '14
        oGrd.Columns.Add(txtcol)

        txtcol = New DataGridViewTextBoxColumn()
        txtcol.Name = "KnpD_005" '14
        oGrd.Columns.Add(txtcol)



    End Sub
    Private Sub grid_spdHed_GridFormat()
        Try

            Dim ColCaption As String
            Dim oGrd As DataGridView = Me.spdMeisai

            For Each col As DataGridViewColumn In oGrd.Columns
                ColCaption = col.HeaderText
                col.SortMode = DataGridViewColumnSortMode.NotSortable
                col.ReadOnly = True
                Select Case ColCaption
                    Case "col_01" '
                        col.HeaderText = "項" & vbCrLf & "目"
                        col.Visible = True
                        col.Width = 40
                        col.DefaultCellStyle.Format = "#,##0"
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan
                    Case "HatD_007" '

                        'col.HeaderText = "仕入先"
                        col.Width = 220
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan
                    Case "JyuD_005" '
                        'col.HeaderText = "発注数量"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                    Case "JyuD_006" '
                        'col.HeaderText = "発注単価"
                        col.Width = 80
                        col.DefaultCellStyle.Format = "#,##0"
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan
                    Case "JyuD_018"
                        'col.HeaderText = "発注金額"
                        col.Width = 80
                        col.DefaultCellStyle.Format = "#,##0"
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan
                    Case "HatD_011_JyuD_022"

                        'col.HeaderText = "仕入数量"
                        col.Width = 80
                        'col.DefaultCellStyle.Format = "#,##0"
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan
                    Case "HatD_017_JyuD_024"
                        'col.HeaderText = "仕入単価"
                        col.Width = 80
                        col.DefaultCellStyle.Format = "#,##0"
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan
                    Case "JyuD_037_KnpD_009"
                        'col.HeaderText = "仕入金額"
                        col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan

                    Case "JyuD_037_KnpD_009"
                        col.HeaderText = "仕入金額"
                        col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan

                    Case "KnpD_006"
                        col.HeaderText = "仕入金額"
                        col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan

                    Case "KnpD_013" '10
                        col.HeaderText = "仕入金額"
                        col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan

                    Case "KnpD_021+KnpD_022+KnpD_023" '11
                        col.HeaderText = "仕入金額"
                        col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan
                    Case "KnpD_015" '12
                        col.HeaderText = "仕入金額"
                        'col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan

                    Case "KnpD_018" '13
                        col.HeaderText = "仕入金額"
                        'col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan

                    Case "KnpD_014" '14
                        col.HeaderText = "仕入金額"
                        col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan

                    Case "KnpD_005" '15
                        col.HeaderText = "仕入金額"
                        col.DefaultCellStyle.Format = "#,##0"
                        col.Width = 80
                        col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter
                        col.DefaultCellStyle.BackColor = Color.LightCyan

                    Case Else
                        col.Visible = False
                        col.ReadOnly = True
                End Select



            Next



        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub txtCaution_Disp(ByVal loRSet As DataRow)

        If modHanbai.NCnvN(loRSet("JyuM_0121")) <> "" Or modHanbai.NCnvN(loRSet("JyuM_012")) <> "" Or modHanbai.NCnvN(loRSet("JyuM_011")) <> "" Then
            txtCaution1.Text = modHanbai.NCnvN(loRSet("JyuM_011"))
            txtCaution2.Text = modHanbai.NCnvN(loRSet("JyuM_012"))
            txtCaution3.Text = modHanbai.NCnvN(loRSet("JyuM_0121"))
            txtCaution1.Visible = True       '警告ラベル表示
            txtCaution2.Visible = True       '警告ラベル表示
            txtCaution3.Visible = True       '警告ラベル表示
        Else
            txtCaution1.Text = ""
            txtCaution2.Text = ""
            txtCaution3.Text = ""
            txtCaution1.Visible = False      '警告ラベル非表示
            txtCaution2.Visible = False      '警告ラベル非表示
            txtCaution3.Visible = False      '警告ラベル非表示
        End If

    End Sub


    Private Sub optDsp_0_CheckedChanged(sender As Object, e As EventArgs) Handles optDsp_0.CheckedChanged

        Option_CheckChanged()
    End Sub

    Private Sub optDsp_1_CheckedChanged(sender As Object, e As EventArgs) Handles optDsp_1.CheckedChanged
        Option_CheckChanged()
    End Sub

    Private Sub Option_CheckChanged()
        If Me.optDsp_0.Checked = True Then
            DataLoadSec()
        ElseIf Me.optDsp_1.Checked = True Then
            DataLoadSec2()
        End If
    End Sub

    Private Sub DataLoadSec2()

        Dim strSQL As String
        Dim loRSet As New DataTable
        Dim loRSet2 As New DataTable
        Dim iRow As Integer
        Dim BrkSeq As Integer              'SEQƒuƒŒ[ƒNƒ`ƒFƒbƒN—p
        Dim BrkKomoku As Integer              '€–ÚƒuƒŒ[ƒNƒ`ƒFƒbƒN—p
        Dim JSuryoT As Double             'Žó’”—Ê
        Dim HSuryoT As Double             '”­’”—Ê
        Dim SSuryoT As Double             'Žd“ü”—Ê
        Dim KSuryoT As Double             '«•ï”—Ê

        Dim ssTXT As Object



        Try



            Dim dgvr As DataGridViewRow

            'ƒXƒvƒŒƒbƒh–¾×î•ñ‚ÌƒNƒŠƒA
            With spdMeisai
                '.Row = -1
                '.Col = -1
                '.Action = 12
                '.MaxRows = 0

                ''ƒZƒ‹‚ÌƒeƒLƒXƒgFA”wŒiF‚ðÝ’è‚µ‚Ü‚·B
                '.ForeColor = RGB(0, 0, 0)
                '.BackColor = RGB(255, 255, 255)
                .Columns(2 - 1).Visible = False
                .Columns(2 - 1).HeaderText = "仕入先"
                .Columns(2 - 1).Width = 200
                .Columns(3 - 1).Width = 250
                .Columns(3 - 1).HeaderText = "品名"
                .Columns(4 - 1).Width = 150
                .Columns(4 - 1).HeaderText = "品番"
                .Columns(5 - 1).Width = 60
                .Columns(5 - 1).HeaderText = "受注" & vbCrLf & "数量"
                .Columns(6 - 1).Width = 60
                .Columns(6 - 1).HeaderText = "発注" & vbCrLf & "数量"
                .Columns(7 - 1).Width = 60
                .Columns(7 - 1).HeaderText = "仕入" & vbCrLf & "数量"
                .Columns(8 - 1).Width = 60
                .Columns(8 - 1).HeaderText = "梱包" & vbCrLf & "数量"
                .Columns(9 - 1).Visible = True
                .Columns(9 - 1).Width = 60
                .Columns(9 - 1).HeaderText = "ｹｰｽ" & vbCrLf & "№"
                .Columns(10 - 1).Visible = True
                .Columns(10 - 1).Width = 70
                .Columns(10 - 1).HeaderText = "重量"
                .Columns(11 - 1).Visible = True
                .Columns(11 - 1).Width = 70
                .Columns(11 - 1).HeaderText = "サイズ"
                .Columns(12 - 1).Visible = True
                .Columns(12 - 1).Width = 70
                .Columns(12 - 1).HeaderText = "梱包日"
                .Columns(13 - 1).Visible = True
                .Columns(13 - 1).Width = 70
                .Columns(13 - 1).HeaderText = "出荷日"
                .Columns(14 - 1).Visible = True
                .Columns(14 - 1).Width = 100
                .Columns(14 - 1).HeaderText = "置き場所"
                .Columns(15 - 1).Visible = True
                .Columns(15 - 1).Width = 70
                .Columns(15 - 1).HeaderText = "出荷No."

            End With


            '受注/発注データ読込
            strSQL = "SELECT * FROM JyuH_DAT,JyuM_Dat,JyuD_Dat"
            strSQL = strSQL & " WHERE JyuH_001 = '" & modHanbai.NCnvN(txtSeikyu.Text) & "'"   '契約№
            strSQL = strSQL & "   AND JyuH_001 = JyuM_001"                          '契約№
            strSQL = strSQL & "   AND JyuM_001 = JyuD_001"                          '契約№
            strSQL = strSQL & "   AND JyuM_003 = JyuD_003"                          'SEQ
            strSQL = strSQL & " ORDER BY JyuM_003,JyuD_004"

            'データの読込み
            If DB Is Nothing Then DB = New clsDB.DBService

            loRSet = DB.GetDatafromDB(strSQL)

            If loRSet.Rows.Count = 0 Then
                GoTo DataLoadSec_Exit
            End If

            Call txtCaution_Disp(loRSet.Rows(0))                'INSERT 2014/02/10 AOKI

            txtEstimate.Text = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_009")) '見積No.        'INSERT 2016/02/09 AOKI
            datNyuYMD.Value = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_018"))  '入荷予定日
            datKeiYMD.Value = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_002"))  '契約日         'INSERT 2016/05/16 AOKI
            txtTokName.Text = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_003"))
            txtVessel.Text = modHanbai.NCnvN(loRSet.Rows(0)("JyuH_006"))

            With spdMeisai
                spdMeisai.Rows.Clear()
                spdMeisai.AllowUserToAddRows = True
                JSuryoT = 0
                HSuryoT = 0
                SSuryoT = 0
                KSuryoT = 0
                BrkSeq = -9999
                BrkKomoku = -9999
                iRow = 0

                'If loRSet.Rows.Count > 0 Then
                '    dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                '    spdMeisai.Rows.Add(dgvr)
                'End If

                For Each dr In loRSet.Rows
                    iRow = iRow + 1

                    'SEQ
                    If BrkSeq <> dr("JyuM_003") Then

                        iRow = iRow + 1

                        'If loRSet.Rows.Count > 0 Then
                        dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                        spdMeisai.Rows.Add(dgvr)
                        'End If
                        '中ヘッダー表示

                        If modHanbai.NCnvN(dr("JyuM_004")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "UNIT:    " & modHanbai.NCnvN(dr("JyuM_004"))
                            spdMeisai.Rows.Add(dgvr)
                            dgvr = Nothing
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_005")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "MAKER:   " & modHanbai.NCnvN(dr("JyuM_005"))
                            spdMeisai.Rows.Add(dgvr)
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_006")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "TYPE:    " & modHanbai.NCnvN(dr("JyuM_006"))
                            spdMeisai.Rows.Add(dgvr)
                            dgvr = Nothing
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_007")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "SER/NO:  " & modHanbai.NCnvN(dr("JyuM_007"))
                            spdMeisai.Rows.Add(dgvr)
                            dgvr = Nothing
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_008")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "DWG NO:  " & modHanbai.NCnvN(dr("JyuM_008"))
                            spdMeisai.Rows.Add(dgvr)
                            dgvr = Nothing
                            iRow = iRow + 1
                        End If
                        If modHanbai.NCnvN(dr("JyuM_025")) <> "" Then
                            dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                            dgvr.Cells(3 - 1).Value = "ADDITIONAL DETAILS " & modHanbai.NCnvN(dr("JyuM_025"))
                            spdMeisai.Rows.Add(dgvr)
                            dgvr = Nothing
                            iRow = iRow + 1
                        End If
                        BrkSeq = CDbl(modHanbai.NCnvZ(dr("JyuM_003")))
                        BrkKomoku = -9999
                        iRow = iRow + 1

                        dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                        spdMeisai.Rows.Add(dgvr)
                    End If

                    '項目

                    dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone

                    If Strings.Right(dr("JyuD_004"), 1) = "0" Then
                        'コメント行
                        dgvr.Cells(1 - 1).Value = ""
                        dgvr.Cells(3 - 1).Value = modHanbai.NCnvN(dr("JyuD_005"))

                    Else
                        If BrkKomoku <> CDbl(modHanbai.NCnvZ(dr("JyuD_004"))) Then
                            '通常行
                            If modHanbai.NCnvN(dr("JyuD_019")) <> "" Then
                                dgvr.Cells(1 - 1).Value = Strings.Left(dr("JyuD_004").ToString().Trim(), Len(dr("JyuD_004").ToString().Trim()) - 1)
                            End If
                            dgvr.Cells(5 - 1).Value = CDbl(modHanbai.NCnvZ(dr("JyuD_018")))
                            BrkKomoku = CDbl(modHanbai.NCnvZ(dr("JyuD_004")))
                            JSuryoT = JSuryoT + CDbl(modHanbai.NCnvZ(dr("JyuD_018")))   '受注数量合計
                        End If
                        dgvr.Cells(3 - 1).Value = modHanbai.NCnvN(dr("JyuD_005"))
                        dgvr.Cells(4 - 1).Value = modHanbai.NCnvN(dr("JyuD_006"))
                        dgvr.Cells(6 - 1).Value = CDbl(modHanbai.NCnvZ(dr("JyuD_022")))
                        dgvr.Cells(7 - 1).Value = CDbl(modHanbai.NCnvZ(dr("JyuD_024")))
                        HSuryoT = HSuryoT + CDbl(modHanbai.NCnvZ(dr("JyuD_022")))
                        SSuryoT = SSuryoT + CDbl(modHanbai.NCnvZ(dr("JyuD_024")))

                        '梱包数量
                        If CDbl(modHanbai.NCnvZ(dr("JyuD_0274"))) = -1 Then
                            dgvr.Cells(8 - 1).Value = "入完"
                        Else
                            If CDbl(modHanbai.NCnvZ(dr("JyuD_037"))) = 0 Then

                                dgvr.DefaultCellStyle.ForeColor = Color.FromArgb(255, 0, 0)


                                '梱包数量合計
                            End If
                            dgvr.Cells(8 - 1).Value = Format(CDbl(modHanbai.NCnvZ(dr("JyuD_037"))), "#,##0")
                            KSuryoT = KSuryoT + CDbl(modHanbai.NCnvZ(dr("JyuD_037")))
                        End If

                        If CDbl(modHanbai.NCnvZ(dr("JyuD_037"))) > 0 Then

                            '«•ïƒf[ƒ^Žæ“¾
                            strSQL = "SELECT * FROM KnpD_Dat"
                            strSQL = strSQL & " WHERE KnpD_001 ='" & modHanbai.NCnvN(txtSeikyu.Text) & "'"
                            strSQL = strSQL & "   AND KnpD_002 = " & CDbl(modHanbai.NCnvZ(dr("JyuD_003")))
                            strSQL = strSQL & " And KnpD_003 = " & CDbl(modHanbai.NCnvZ(dr("JyuD_004")))
                            strSQL = strSQL & " ORDER BY KnpD_004"


                            loRSet2 = DB.GetDatafromDB(strSQL)

                            If loRSet2.Rows.Count > 0 Then
                                iRow = iRow - 1
                                For Each dr2 As DataRow In loRSet2.Rows
                                    iRow = iRow + 1

                                    dgvr.Cells(8 - 1).Value = modHanbai.NCnvN(dr2("KnpD_009"))
                                    dgvr.Cells(9 - 1).Value = modHanbai.NCnvN(dr2("KnpD_006"))
                                    dgvr.Cells(10 - 1).Value = Format(modHanbai.NCnvN(dr2("KnpD_013")), "0.0")


                                    If CDbl(modHanbai.NCnvZ(dr2("KnpD_021"))) > 0 Then
                                        dgvr.Cells(11 - 1).Value = NCnvZ(dr2("KnpD_021")) & "x" &
                                                               NCnvZ(dr2("KnpD_022")) & "x" &
                                                               NCnvZ(dr2("KnpD_023"))
                                    End If
                                    If (modHanbai.NCnvN(dr2("KnpD_015")) = "") Then
                                        dgvr.Cells(12 - 1).Value = ""
                                    Else
                                        dgvr.Cells(12 - 1).Value = Format(CDate(modHanbai.NCnvN(dr2("KnpD_015"))), "yy.MM.dd")
                                    End If
                                    If (modHanbai.NCnvN(dr2("KnpD_018")) = "") Then
                                        dgvr.Cells(13 - 1).Value = ""
                                    Else
                                        dgvr.Cells(13 - 1).Value = Format(CDate(modHanbai.NCnvN(dr2("KnpD_018"))), "yy.MM.dd")
                                    End If

                                    'dgvr.Cells(12 - 1).Value = IIf(modHanbai.NCnvN(dr2("KnpD_015")) = "", "", (Format(CDate(modHanbai.NCnvN(dr2("KnpD_015"))), "yy.MM.dd")))
                                    'dgvr.Cells(13 - 1).Value = IIf(modHanbai.NCnvN(dr2("KnpD_018")) = "", "", (Format(CDate(modHanbai.NCnvN(dr2("KnpD_018"))), "yy.MM.dd")))
                                    dgvr.Cells(14 - 1).Value = modHanbai.NCnvN(dr2("KnpD_014"))
                                    dgvr.Cells(15 - 1).Value = CDbl(modHanbai.NCnvZ(dr2("KnpD_005")))

                                    spdMeisai.Rows.Add(dgvr)

                                Next

                            Else
                                spdMeisai.Rows.Add(dgvr)
                            End If
                        Else
                            spdMeisai.Rows.Add(dgvr)
                        End If


                    End If


                Next


                dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone
                spdMeisai.Rows.Add(dgvr)
                iRow = iRow + 2
                dgvr = spdMeisai.Rows(spdMeisai.NewRowIndex).Clone

                dgvr.Cells(3 - 1).Value = "＊＊ 合 計 ＊＊"
                dgvr.Cells(5 - 1).Value = CDbl(modHanbai.NCnvZ(JSuryoT))
                dgvr.Cells(6 - 1).Value = CDbl(modHanbai.NCnvZ(HSuryoT))
                dgvr.Cells(7 - 1).Value = CDbl(modHanbai.NCnvZ(SSuryoT))
                dgvr.Cells(8 - 1).Value = Format(CDbl(modHanbai.NCnvZ(KSuryoT)), "#,###")

                spdMeisai.Rows.Add(dgvr)


            End With


            spdMeisai.AllowUserToAddRows = False
DataLoadSec_Exit:

            loRSet = Nothing
            Exit Sub

        Catch ex As Exception

            Call ksExpMsgBox("Error in DataLoadSec2. " & ex.Message, "E")


        End Try



    End Sub

    Private Sub cmdFunc_12_Click(sender As Object, e As EventArgs) Handles cmdFunc_12.Click
        RetMode = True
        Me.Close()
    End Sub

    Private Sub frmURIMR084A_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyData
            Case Keys.Enter
                cmdFunc_12_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Sub txtSeikyu_TextChanged(sender As Object, e As EventArgs) Handles txtSeikyu.TextChanged

    End Sub

    Private Sub groupBox4_Enter(sender As Object, e As EventArgs) Handles groupBox4.Enter

    End Sub

    Private Sub lbl_004_Click(sender As Object, e As EventArgs) Handles lbl_004.Click

    End Sub

    Private Sub txtVessel_TextChanged(sender As Object, e As EventArgs) Handles txtVessel.TextChanged

    End Sub

    Private Sub datKeiYMD_ValueChanged(sender As Object, e As EventArgs) Handles datKeiYMD.ValueChanged

    End Sub

    Private Sub lbl_006_Click(sender As Object, e As EventArgs) Handles lbl_006.Click

    End Sub

    Private Sub txtEstimate_TextChanged(sender As Object, e As EventArgs) Handles txtEstimate.TextChanged

    End Sub

    Private Sub txtTokName_TextChanged(sender As Object, e As EventArgs) Handles txtTokName.TextChanged

    End Sub

    Private Sub lbl_015_Click(sender As Object, e As EventArgs) Handles lbl_015.Click

    End Sub

    Private Sub datNyuYMD_ValueChanged(sender As Object, e As EventArgs) Handles datNyuYMD.ValueChanged

    End Sub

    Private Sub lblEstimate_Click(sender As Object, e As EventArgs) Handles lblEstimate.Click

    End Sub

    Private Sub lblSeikyu_Click(sender As Object, e As EventArgs) Handles lblSeikyu.Click

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub
End Class