﻿
Imports System.Windows.Forms


Public Module modCommon




    Public gbl As New SAHANBAI.clsGlobal()




    Public Sub CenterForm(vcForm As Form)

        With vcForm
            .StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        End With

    End Sub

    Public Sub ShowVersion()

        MessageBox.Show("Version " & stvVersionNumber)
    End Sub


    'Public Function GetdtConMstInfo() As DataTable
    '    Dim SQLtxt As String
    '    SQLtxt = ""
    '    SQLtxt = SQLtxt & "SELECT * FROM Con_Mst"
    '    SQLtxt = SQLtxt & " WHERE Con_001 = 1"
    '    Dim DB As New clsDB.DBService()
    '    Dim dt As DataTable = DB.GetDatafromDB(SQLtxt)
    '    DB = Nothing

    '    Return dt

    'End Function


    Public Function EditSQLAddSQuot(ByVal pItem As String) As String
        ''------error
        'value = value.Replace("'", "''")
        'value = value.Replace(Environment.NewLine, "")
        'Return value

        Dim wWork As String
        Dim i As Integer

        If Val(InStr(pItem, "'")) = 0 Then
            EditSQLAddSQuot = pItem
        Else
            wWork = Left(pItem, Val(InStr(pItem, "'"))) & "'"
            For i = Val(InStr(pItem, "'")) + 1 To Len(pItem)
                If Mid(pItem, i, 1) = "'" Then
                    wWork = wWork & "'"
                End If
                wWork = wWork & Mid(pItem, i, 1)
            Next i
            EditSQLAddSQuot = wWork
        End If

        EditSQLAddSQuot = Replace(EditSQLAddSQuot, vbCr, "")        'INSERT 2014/01/30 AOKI
        EditSQLAddSQuot = Replace(EditSQLAddSQuot, vbLf, "")        'INSERT 2014/01/30 AOKI
    End Function


    Public Function GetConMstInfo() As DataTable
        Dim SQLtxt As String
        SQLtxt = ""
        SQLtxt = SQLtxt & "SELECT * FROM Con_Mst"
        SQLtxt = SQLtxt & " WHERE Con_001 = 1"
        Dim DB = New clsDB.DBService
        Dim dt As DataTable = DB.GetDatafromDB(SQLtxt)
        Return dt
    End Function

    Public Function SetCtrlDate(ByVal obj As DateTimePicker, ByVal viSWKbn As Integer, ByVal viYMDKbn As String) As Boolean
        Select Case viSWKbn
            Case 0

                Select Case viYMDKbn
                    Case "Y"
                        obj.CustomFormat = "gggee年"
                    Case "YM"
                        obj.CustomFormat = "gggee年MM月"
                    Case "YMD"
                        obj.CustomFormat = "gggee年MM月dd日"
                End Select

            Case 1

                Select Case viYMDKbn
                    Case "Y"
                        obj.CustomFormat = "yyyy年"
                    Case "YM"
                        obj.CustomFormat = "yyyy年MM月"
                    Case "YMD"
                        obj.CustomFormat = "yyyy年MM月dd日"
                End Select
        End Select

        Return True
    End Function

    Public Function CnvDate(ByVal sDate As String, ByVal sFormat As String, ByVal Optional sDefault As String = "") As String
        If IsDateTime(NCnvN(sDate)) = True Then
            Return Convert.ToDateTime(sDate).ToString(sFormat)
        Else
            Return Convert.ToDateTime(sDefault).ToString(sFormat)
        End If
    End Function

    Public Function IsDateTime(ByVal txtDate As String) As Boolean
        Dim tempDate As DateTime
        Return DateTime.TryParse(txtDate, tempDate)
    End Function

    Public Function IsNumeric(ByVal value As String) As Boolean
        Return Information.IsNumeric(value)
    End Function


    Public ActivePrinter As PrinterInfo = New PrinterInfo()

    Public Structure PrinterInfo
        Public Printer As String
        Public Copies As Integer
        Public hDC As ULong
    End Structure


    Public Function CurRound2(ByVal vdValue As Double, ByVal viDigit As Integer, ByVal Optional viType As Integer = 0) As Double
        Dim vdAns As Double = 0
        Dim vlDig As Long
        vlDig = 10 Xor Math.Abs(viDigit)

        Select Case viType
            Case 0

                If viDigit >= 0 Then
                    vdAns = Convert.ToInt64(Convert.ToDouble(vdValue * vlDig + 0.5)) / vlDig
                Else
                    vdAns = Convert.ToInt64(Convert.ToDouble(vdValue / vlDig + 0.5)) * vlDig
                End If

            Case 1

                If viDigit >= 0 Then

                    If vdValue <> (Convert.ToInt64(Convert.ToDouble((vdValue * vlDig))) / vlDig) Then
                        vdAns = Convert.ToInt64(Convert.ToDouble((vdValue * vlDig + 1))) / vlDig
                    End If
                Else

                    If vdValue <> (Convert.ToInt64(Convert.ToDouble((vdValue / vlDig))) * vlDig) Then
                        vdAns = Convert.ToInt64(Convert.ToDouble((vdValue / vlDig + 1))) * vlDig
                    End If
                End If

            Case 2

                If viDigit >= 0 Then
                    vdAns = Convert.ToInt64(Convert.ToDouble(vdValue * vlDig)) / vlDig
                Else
                    vdAns = Convert.ToInt64(Convert.ToDouble(vdValue / vlDig)) * vlDig
                End If
        End Select

        Return vdAns
    End Function


End Module
