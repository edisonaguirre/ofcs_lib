﻿
Imports System.Windows.Forms


Public Module modZAIMR020



    '***************************************************************
    '**
    '**  プログラムＩＤ :  ZAIMR020
    '**  機能           :  在庫選択(modZAIMR020)
    '**  作成日         :  2012/04/19   青木
    '**  更新日         :
    '**  備考           :
    '**
    '***************************************************************
    'ﾌﾟﾛｸﾞﾗﾑID(出荷入力)


    'スプレッド列番号
    Public Const scChumonNo As Integer = 1
    Public Const scKeiyakuNo As Integer = 2
    Public Const scSeq As Integer = 3
    Public Const scNo As Integer = 4
    Public Const scRen As Integer = 5
    Public Const scSyoName As Integer = 6
    Public Const scPartNo As Integer = 7
    Public Const scSTanNet As Integer = 8
    Public Const scSSuryo As Integer = 9
    Public Const scUPlan As Integer = 10
    Public Const scUSuryo As Integer = 11
    Public Const scCSuryo As Integer = 12
    Public Const scZSuryo As Integer = 13
    Public Const scChkBox As Integer = 14


    '==== 変数 ==============================================================================
    '''Public strSQL       As String
    Public lngErr As Long
    Public strMsg As String

    Public ZaiHDat As ZaiHDatInfo  '在庫データ(ヘッダ)
    Public ZaiDDat() As ZaiDDatInfo  '在庫データ(明細)

    '在庫データ(ヘッダ)情報
    Public Structure ZaiHDatInfo
        Public ZaiH_001 As String       '注文№
        Public ZaiH_002 As String       '発注日
        Public ZaiH_003 As String       '当社担当者
        Public ZaiH_004 As String       'COMMENT1
        Public ZaiH_005 As Integer      'SEQ
        Public ZaiH_006 As String       'UNIT
        Public ZaiH_007 As String       'MAKER
        Public ZaiH_008 As String       'TYPE
        Public ZaiH_009 As String       'SER/NO
        Public ZaiH_010 As String       'DWG/NO
        Public ZaiH_inst As String       '登録日
        Public ZaiH_updt As String       '更新日
        Public ZaiH_WsNo As String       'WsNo
    End Structure

    '在庫データ(明細)情報
    Public Structure ZaiDDatInfo
        Public ZaiD_001 As String       '注文№
        Public ZaiD_002 As Integer      'SEQ１
        Public ZaiD_003 As Integer      '項番１
        Public ZaiD_004 As Integer      '連番１
        Public ZaiD_001k As String       '契約№
        Public ZaiD_002k As Integer      'SEQ２
        Public ZaiD_003k As Integer      '項番２
        Public ZaiD_004k As Integer      '連番２
        Public ZaiD_005 As String       '品名
        Public ZaiD_006 As String       '品番
        Public ZaiD_007 As String       '仕入先コード
        Public ZaiD_008 As String       '仕入先名
        Public ZaiD_009 As String       '
        Public ZaiD_010 As String       '発注日付
        Public ZaiD_011 As Double     '発注数量
        Public ZaiD_012 As String       '発注単位
        Public ZaiD_013 As Double     '発注単価
        Public ZaiD_014 As Double     '発注金額
        Public ZaiD_015 As Double     '発注掛率
        Public ZaiD_016 As Double     '発注単価NET
        Public ZaiD_017 As Double     '
        Public ZaiD_018 As Double     '
        Public ZaiD_019 As Double     '
        Public ZaiD_020 As String       '仕入日付
        Public ZaiD_021 As Double     '仕入数量
        Public ZaiD_022 As String       '仕入単位
        Public ZaiD_023 As Double     '仕入単価
        Public ZaiD_024 As Double     '仕入金額
        Public ZaiD_025 As Double     '仕入掛率
        Public ZaiD_026 As Double     '仕入単価NET
        Public ZaiD_027 As Double     'チェック
        Public ZaiD_028 As String       'チェック日付
        Public ZaiD_029 As Double     '
        Public ZaiD_030 As Double     '売上予定数量
        Public ZaiD_031 As Double     '売上数量
        Public ZaiD_032 As Double     '調整数量
        Public ZaiD_033 As Double     '使用数量
        Public ZaiD_Inst As String       '登録日
        Public ZaiD_Updt As String       '更新日
        Public ZaiD_WsNo As String       'WsNo
    End Structure

    ''''コントロールマスタ情報
    '''Public ConMst(1)    As ConMstInfo
    '========================================================================================

    Public psChumonNo As String
    Public piSeqNo As Integer
    Public piNoNo As Integer
    Public piRenNo As Integer
    Public psSyoName As String
    Public psPartNo As String

    Public pcSTanNet As Double

    Public piSSuryo As Integer
    Public piUplan As Integer
    Public piUSuryo As Integer
    Public piCSuryo As Integer
    Public piZSuryo As Integer


End Module
