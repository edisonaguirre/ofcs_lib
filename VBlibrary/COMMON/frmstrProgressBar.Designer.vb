﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmstProgressBar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.frame1 = New System.Windows.Forms.GroupBox()
        Me.lblTitleMessage = New System.Windows.Forms.Label()
        Me.lblPageMessage = New System.Windows.Forms.Label()
        Me.prgCount = New System.Windows.Forms.ProgressBar()
        Me.frame1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.Location = New System.Drawing.Point(142, 118)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(110, 32)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "中止"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'frame1
        '
        Me.frame1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.frame1.Controls.Add(Me.prgCount)
        Me.frame1.Controls.Add(Me.lblPageMessage)
        Me.frame1.Controls.Add(Me.lblTitleMessage)
        Me.frame1.Location = New System.Drawing.Point(6, 4)
        Me.frame1.Name = "frame1"
        Me.frame1.Size = New System.Drawing.Size(406, 108)
        Me.frame1.TabIndex = 2
        Me.frame1.TabStop = False
        '
        'lblTitleMessage
        '
        Me.lblTitleMessage.AutoSize = True
        Me.lblTitleMessage.Location = New System.Drawing.Point(6, 31)
        Me.lblTitleMessage.Name = "lblTitleMessage"
        Me.lblTitleMessage.Size = New System.Drawing.Size(142, 13)
        Me.lblTitleMessage.TabIndex = 4
        Me.lblTitleMessage.Text = "                                             "
        '
        'lblPageMessage
        '
        Me.lblPageMessage.AutoSize = True
        Me.lblPageMessage.Location = New System.Drawing.Point(6, 73)
        Me.lblPageMessage.Name = "lblPageMessage"
        Me.lblPageMessage.Size = New System.Drawing.Size(103, 13)
        Me.lblPageMessage.TabIndex = 5
        Me.lblPageMessage.Text = "                                "
        '
        'prgCount
        '
        Me.prgCount.Location = New System.Drawing.Point(9, 47)
        Me.prgCount.Name = "prgCount"
        Me.prgCount.Size = New System.Drawing.Size(385, 23)
        Me.prgCount.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.prgCount.TabIndex = 6
        '
        'frmstProgressBar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(419, 152)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.frame1)
        Me.Name = "frmstProgressBar"
        Me.Text = "処理中"
        Me.frame1.ResumeLayout(False)
        Me.frame1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Public WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents frame1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPageMessage As System.Windows.Forms.Label
    Friend WithEvents lblTitleMessage As System.Windows.Forms.Label
    Friend WithEvents prgCount As System.Windows.Forms.ProgressBar
End Class
