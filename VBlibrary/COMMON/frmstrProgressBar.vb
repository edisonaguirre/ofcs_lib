﻿Imports System.Windows.Forms

Public Class frmstProgressBar

    '各デフォルト値
    Private Const stpFrmTitle As String = "処理中"
    Private Const stpProgMin As Integer = 1

    '各プロパティ
    Private spButton As Boolean      'キャンセルボタン表示(False)／非表示(True)
    Private spCancel As Boolean      'キャンセルボタンステータス

    Private spFORM_MSG As String       'フォームタイトル
    Private spTTLE_MSG As String       'タイトルメッセージ
    Private spPAGE_MSG As String       'ページ用メッセージ

    Private spPROG_MIN As Single       'プログレスバー最小値
    Private spPROG_MAX As Single       'プログレスバー最大値
    Private spPROG_VAL As Single       'プログレスバーカレント値(?)
    Private Sub frmstProgressBar_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub



    Public Sub ShowStart()

        '初期設定
        Call SetupForm()
        spCancel = False        'キャンセルボタンステータス

        '中央に表示
        Call Me.CenterToScreen()
        '表示
        Call Me.Show()
        Application.DoEvents()
        Application.DoEvents()
        Application.DoEvents()

    End Sub

    '***************************************************************
    '**  名称    : Sub ShowStop()
    '**  機能    : プログレスバーの終了。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public Sub ShowStop()

        Call InitForm()
        Call Me.Close()        '終了

    End Sub

    '***************************************************************
    '**  名称    : Sub SetupForm()
    '**  機能    : フォームの初期設定。
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub SetupForm()

        On Error Resume Next

        'フォームキャプション
        If spFORM_MSG <> "" Then
            Me.Text = spFORM_MSG
        Else
            Me.Text = stpFrmTitle
        End If
        'タイトルメッセージ
        lblTitleMessage.Text = spTTLE_MSG
        'ページメッセージ
        lblPageMessage.Text = spPAGE_MSG

        'プログレスバー
        With prgCount
            .Minimum = spPROG_MIN
            .Maximum = spPROG_MAX
        End With

        'ボタンのステータス
        If spButton Then
            cmdCancel.Visible = False
        End If

        On Error GoTo 0

    End Sub

    '***************************************************************
    '**  名称    : Sub SetForm()
    '**  機能    : フォームの設定
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub SetForm()

        'タイトルメッセージ
        lblTitleMessage.Text = spTTLE_MSG
        'ページメッセージ
        lblPageMessage.Text = spPAGE_MSG

        'プログレスバー
        With prgCount
            .Value = spPROG_VAL
        End With

    End Sub

    '***************************************************************
    '**  名称    : Sub InitForm()
    '**  機能    : 各情報の初期化
    '**  戻り値  :
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Private Sub InitForm()

        '初期化
        spFORM_MSG = ""
        spTTLE_MSG = ""
        spPAGE_MSG = ""
        spPROG_MIN = 0
        spPROG_MAX = 0
        spPROG_VAL = 0

    End Sub

    '***************************************************************
    '**  名称    : Property Let SetCaption()
    '**  機能    : フォームキャプションの設定。
    '**  戻り値  :
    '**  引数    : vNewValue ; フォームキャプション名称
    '**  備考    :
    '***************************************************************



    Public WriteOnly Property SetCaption() As Object

        Set(ByVal value As Object)
            spFORM_MSG = value.ToString()
        End Set
    End Property





    '***************************************************************
    '**  名称    : Property Let SetTitle()
    '**  機能    : タイトルメッセージの設定。
    '**  戻り値  :
    '**  引数    : vNewValue ; タイトルメッセージ
    '**  備考    :
    '***************************************************************


    Public Property SetTitle() As Object
        Get
            Return spTTLE_MSG
        End Get
        Set(ByVal value As Object)
            spTTLE_MSG = value.ToString()
        End Set
    End Property


    '***************************************************************
    '**  名称    : Property Let SetPageMsg()
    '**  機能    : ページメッセージの設定。
    '**  戻り値  :
    '**  引数    : vNewValue ; ページメッセージ名称
    '**  備考    :
    '***************************************************************


    Public Property SetPage() As Object
        Get
            Return spPAGE_MSG
        End Get
        Set(ByVal value As Object)
            spPAGE_MSG = value.ToString()
        End Set
    End Property

    '***************************************************************
    '**  名称    : Property Let SetMax()
    '**  機能    : プログレスバー最大値設定。
    '**  戻り値  :
    '**  引数    : vNewValue ; 最大値
    '**  備考    :
    '***************************************************************

    Public Property SetMax() As Object
        Get
            Return spPROG_MAX
        End Get
        Set(ByVal value As Object)
            spPROG_MAX = value.ToString()
        End Set
    End Property
    '***************************************************************
    '**  名称    : Property Let SetMin()
    '**  機能    : プログレスバー最小値設定。
    '**  戻り値  :
    '**  引数    : vNewValue ; 最小値
    '**  備考    :
    '***************************************************************

    Public Property SetMin() As Object
        Get
            Return spPROG_MIN
        End Get
        Set(ByVal value As Object)
            spPROG_MIN = value.ToString()
        End Set
    End Property

    '***************************************************************
    '**  名称    : Property Let SetVal()
    '**  機能    : プログレスバーカレント値設定。
    '**  戻り値  :
    '**  引数    : vNewValue ; カレント値
    '**  備考    :
    '***************************************************************



    Public Property SetVal() As Object
        Get
            Return spPROG_VAL
        End Get
        Set(ByVal value As Object)

            If Not (spPROG_MIN > value Or spPROG_MAX < value) Then
                spPROG_VAL = value
                Call SetForm()
                Application.DoEvents()
            End If

        End Set
    End Property

    '***************************************************************
    '**  名称    : Property Get Cancel() As Variant
    '**  機能    : キャンセルステータスを返す。
    '**  戻り値  : True;Cancel , False;OK
    '**  引数    :
    '**  備考    :
    '***************************************************************
    Public  Property Cancel() As Object
        Get
            Return spCancel
        End Get
        Set(ByVal value As Object)
            spCancel = value.ToString()
        End Set
    End Property



    '***************************************************************
    '**  名称    : Property Let Cancel()
    '**  機能    : キャンセルステータスを設定。
    '**  戻り値  :
    '**  引数    : vNewValue ; キャンセルステータス
    '**  備考    :

    '***************************************************************
    '**  名称    : Property Let ButtonCancel()
    '**  機能    : キャンセルステータスを設定。
    '**  戻り値  :
    '**  引数    : vNewValue ; ボタンステータス
    '**  備考    :
    '***************************************************************

    Public WriteOnly Property ButtonCancel() As Object

        Set(ByVal value As Object)
            spButton = value.ToString()
        End Set
    End Property

End Class