﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace SAMenu3
{
    public partial class frmSAMenu3 : Form
    {
        public frmSAMenu3()
        {
            InitializeComponent();
        }

        private void Frame6_Enter(object sender, EventArgs e)
        {

        }

        private void cmdUriage_1_Click(object sender, EventArgs e)
        {

        }

        private void cmdUriage_7_Click(object sender, EventArgs e)
        {

        }

        private void cmdSeikyu_3_Click(object sender, EventArgs e)
        {

        }

        private void cmdZuiji_1_Click(object sender, EventArgs e)
        {

        }

        private void frmSAMenu3_Load(object sender, EventArgs e)
        {
            lblCompany.Text = "ONE'S FORTE CO., LTD.";
            var c = GetAll(this, typeof(Button));
            foreach (Button btn in c)
            {
                btn.MouseEnter += new EventHandler(ShowTooltip);
                if (!File.Exists(Application.StartupPath + @"\" + btn.Tag + ".exe"))
                {
                    
                    btn.BackColor = Color.Red;
                    btn.ForeColor = Color.White;
    
                }
                else
                {
                    btn.Click += new EventHandler(menuClick);
                }
            }
        }

        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }



        private void menuClick(object sender, EventArgs e)
        {
            Button button = sender as Button;
            gksExecuteModule(Application.StartupPath + @"\" + button.Tag + ".exe");
            
        }

        private void ShowTooltip(object sender, EventArgs e)
        {
            Button button = sender as Button;
            try
            {
                toolTip1.SetToolTip(button, button.Tag.ToString());
            }
            catch (Exception)
            {

            }


        }

        private void gksExecuteModule(string vsModule)
        {
            Process.Start(vsModule);

        }

        private void cmdMaster_1_Click(object sender, EventArgs e)
        {

        }

        private void cmdSeikyu_2_Click(object sender, EventArgs e)
        {

        }
    }
}
