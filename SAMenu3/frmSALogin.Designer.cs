﻿namespace SAMenu3
{
    partial class frmSALogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblUserNM = new System.Windows.Forms.Label();
            this.lblPassWD = new System.Windows.Forms.Label();
            this.txtPassWD = new System.Windows.Forms.TextBox();
            this.txtUserNM = new System.Windows.Forms.TextBox();
            this.cmdFunc_0 = new System.Windows.Forms.Button();
            this.cmdFunc_1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("MS Gothic", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-1, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(466, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "ユーザ名とパスワードを入力して下さい";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblUserNM
            // 
            this.lblUserNM.BackColor = System.Drawing.Color.Transparent;
            this.lblUserNM.Font = new System.Drawing.Font("MS Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblUserNM.ForeColor = System.Drawing.Color.White;
            this.lblUserNM.Location = new System.Drawing.Point(51, 44);
            this.lblUserNM.Name = "lblUserNM";
            this.lblUserNM.Size = new System.Drawing.Size(151, 23);
            this.lblUserNM.TabIndex = 1;
            this.lblUserNM.Text = "ユーザ名：";
            this.lblUserNM.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblPassWD
            // 
            this.lblPassWD.BackColor = System.Drawing.Color.Transparent;
            this.lblPassWD.Font = new System.Drawing.Font("MS Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblPassWD.ForeColor = System.Drawing.Color.White;
            this.lblPassWD.Location = new System.Drawing.Point(36, 77);
            this.lblPassWD.Name = "lblPassWD";
            this.lblPassWD.Size = new System.Drawing.Size(165, 22);
            this.lblPassWD.TabIndex = 2;
            this.lblPassWD.Text = "パスワード：";
            this.lblPassWD.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPassWD
            // 
            this.txtPassWD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassWD.Location = new System.Drawing.Point(203, 74);
            this.txtPassWD.Name = "txtPassWD";
            this.txtPassWD.PasswordChar = '*';
            this.txtPassWD.Size = new System.Drawing.Size(179, 22);
            this.txtPassWD.TabIndex = 1;
            this.txtPassWD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtUserNM
            // 
            this.txtUserNM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserNM.Location = new System.Drawing.Point(203, 42);
            this.txtUserNM.Name = "txtUserNM";
            this.txtUserNM.Size = new System.Drawing.Size(179, 22);
            this.txtUserNM.TabIndex = 0;
            this.txtUserNM.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmdFunc_0
            // 
            this.cmdFunc_0.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFunc_0.Location = new System.Drawing.Point(167, 133);
            this.cmdFunc_0.Name = "cmdFunc_0";
            this.cmdFunc_0.Size = new System.Drawing.Size(101, 41);
            this.cmdFunc_0.TabIndex = 2;
            this.cmdFunc_0.Text = "OK";
            this.cmdFunc_0.UseVisualStyleBackColor = true;
            this.cmdFunc_0.Click += new System.EventHandler(this.cmdFunc_0_Click);
            // 
            // cmdFunc_1
            // 
            this.cmdFunc_1.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFunc_1.Location = new System.Drawing.Point(290, 133);
            this.cmdFunc_1.Name = "cmdFunc_1";
            this.cmdFunc_1.Size = new System.Drawing.Size(101, 41);
            this.cmdFunc_1.TabIndex = 3;
            this.cmdFunc_1.Text = "ｷｬﾝｾﾙ";
            this.cmdFunc_1.UseVisualStyleBackColor = true;
            // 
            // frmSALogin
            // 
            this.AcceptButton = this.cmdFunc_0;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Navy;
            this.BackgroundImage = global::SAMenu3.Properties.Resources._427362_windows_7_aqua_logon_screen_by_zedzna_on_deviantart_1280x1024_h;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(468, 204);
            this.Controls.Add(this.cmdFunc_1);
            this.Controls.Add(this.cmdFunc_0);
            this.Controls.Add(this.txtUserNM);
            this.Controls.Add(this.txtPassWD);
            this.Controls.Add(this.lblPassWD);
            this.Controls.Add(this.lblUserNM);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmSALogin";
            this.Text = "ログイン";
            this.Load += new System.EventHandler(this.frmSALogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblUserNM;
        private System.Windows.Forms.Label lblPassWD;
        private System.Windows.Forms.TextBox txtPassWD;
        private System.Windows.Forms.TextBox txtUserNM;
        private System.Windows.Forms.Button cmdFunc_0;
        private System.Windows.Forms.Button cmdFunc_1;
    }
}

