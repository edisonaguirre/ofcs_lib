﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SAMenu3
{
    public partial class frmMain : Form
    {
        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);



        public frmMain()
        {
            InitializeComponent();
        }

        private void toolStripContainer1_ContentPanel_Load(object sender, EventArgs e)
        {

        }

        private void tvMenu_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void 見積処理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
      
        }

        private void gksExecuteModule(string vsModule)
        {
            //var clientApplication = Process.Start(vsModule);
            //SetParent(clientApplication.MainWindowHandle, this.Handle);
            //Native.LoadProcessInControl(vsModule, this.splitContainer1.Panel2);
            //Process p = new Process();
            //p.StartInfo.FileName = "calc.exe";
            //p.Start();
            //p.WaitForInputIdle();
            //SetParent(p.MainWindowHandle, this.Handle);

            Process p = Process.Start(vsModule);
            int tries = 0;
            while (p.MainWindowHandle == IntPtr.Zero && tries < 3)
            {
                tries++;
                Thread.Sleep(1000);
            }

        SetParent(p.MainWindowHandle, this.splitContainer1.Panel2.Handle);
;

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            gksExecuteModule(Application.StartupPath + @"\MITMR010.exe");

        }
    }
}
