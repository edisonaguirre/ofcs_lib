﻿namespace SAMenu3
{
    partial class frmSAMenu3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.P = new System.Windows.Forms.MenuStrip();
            this.mnuVer = new System.Windows.Forms.ToolStripMenuItem();
            this.Hard_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.txtTitle = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.cmdMitumori_1 = new System.Windows.Forms.Button();
            this.cmdMitumori_0 = new System.Windows.Forms.Button();
            this.Frame5 = new System.Windows.Forms.GroupBox();
            this.cmdJyutyu_5 = new System.Windows.Forms.Button();
            this.cmdJyutyu_0 = new System.Windows.Forms.Button();
            this.cmdJyutyu_1 = new System.Windows.Forms.Button();
            this.cmdJyutyu_2 = new System.Windows.Forms.Button();
            this.cmdJyutyu_3 = new System.Windows.Forms.Button();
            this.cmdJyutyu_4 = new System.Windows.Forms.Button();
            this.cmdJyutyu_6 = new System.Windows.Forms.Button();
            this.Frame2 = new System.Windows.Forms.GroupBox();
            this.cmdUriage_0 = new System.Windows.Forms.Button();
            this.cmdUriage_10 = new System.Windows.Forms.Button();
            this.cmdUriage_4 = new System.Windows.Forms.Button();
            this.cmdUriage_7 = new System.Windows.Forms.Button();
            this.cmdUriage_8 = new System.Windows.Forms.Button();
            this.cmdUriage_5 = new System.Windows.Forms.Button();
            this.cmdUriage_1 = new System.Windows.Forms.Button();
            this.cmdUriage_9 = new System.Windows.Forms.Button();
            this.cmdUriage_11 = new System.Windows.Forms.Button();
            this.cmdUriage_2 = new System.Windows.Forms.Button();
            this.cmdUriage_3 = new System.Windows.Forms.Button();
            this.cmdUriage_6 = new System.Windows.Forms.Button();
            this.Frame3 = new System.Windows.Forms.GroupBox();
            this.cmdSeikyu_0 = new System.Windows.Forms.Button();
            this.cmdSeikyu_4 = new System.Windows.Forms.Button();
            this.cmdSeikyu_3 = new System.Windows.Forms.Button();
            this.cmdSeikyu_2 = new System.Windows.Forms.Button();
            this.cmdSeikyu_1 = new System.Windows.Forms.Button();
            this.Frame7 = new System.Windows.Forms.GroupBox();
            this.cmdZuiji_6 = new System.Windows.Forms.Button();
            this.cmdZuiji_7 = new System.Windows.Forms.Button();
            this.cmdZuiji_0 = new System.Windows.Forms.Button();
            this.cmdZuiji_5 = new System.Windows.Forms.Button();
            this.cmdZuiji_1 = new System.Windows.Forms.Button();
            this.cmdZuiji_2 = new System.Windows.Forms.Button();
            this.cmdZuiji_3 = new System.Windows.Forms.Button();
            this.cmdZuiji_4 = new System.Windows.Forms.Button();
            this.cmdGetuji_3 = new System.Windows.Forms.Button();
            this.Frame6 = new System.Windows.Forms.GroupBox();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.Frame8 = new System.Windows.Forms.GroupBox();
            this.cmdMaster_0 = new System.Windows.Forms.Button();
            this.cmdMaster_1 = new System.Windows.Forms.Button();
            this.cmdMaster_5 = new System.Windows.Forms.Button();
            this.cmdMaster_4 = new System.Windows.Forms.Button();
            this.cmdMaster_2 = new System.Windows.Forms.Button();
            this.cmdMaster_6 = new System.Windows.Forms.Button();
            this.cmdMaster_3 = new System.Windows.Forms.Button();
            this.Frame9 = new System.Windows.Forms.GroupBox();
            this.cmdTable_7 = new System.Windows.Forms.Button();
            this.cmdTable_8 = new System.Windows.Forms.Button();
            this.cmdTable_0 = new System.Windows.Forms.Button();
            this.cmdTable_2 = new System.Windows.Forms.Button();
            this.cmdTable_3 = new System.Windows.Forms.Button();
            this.cmdTable_4 = new System.Windows.Forms.Button();
            this.cmdTable_1 = new System.Windows.Forms.Button();
            this.cmdTable_5 = new System.Windows.Forms.Button();
            this.cmdTable_6 = new System.Windows.Forms.Button();
            this.Frame10 = new System.Windows.Forms.GroupBox();
            this.cmdZaiko_0 = new System.Windows.Forms.Button();
            this.Frame4 = new System.Windows.Forms.GroupBox();
            this.cmdSonota_0 = new System.Windows.Forms.Button();
            this.cmdSonota_1 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.P.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.Frame5.SuspendLayout();
            this.Frame2.SuspendLayout();
            this.Frame3.SuspendLayout();
            this.Frame7.SuspendLayout();
            this.Frame6.SuspendLayout();
            this.Frame8.SuspendLayout();
            this.Frame9.SuspendLayout();
            this.Frame10.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.SuspendLayout();
            // 
            // P
            // 
            this.P.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.P.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuVer,
            this.Hard_Copy});
            this.P.Location = new System.Drawing.Point(0, 0);
            this.P.Name = "P";
            this.P.Size = new System.Drawing.Size(889, 24);
            this.P.TabIndex = 0;
            this.P.Text = "バージョン情報(&V)";
            // 
            // mnuVer
            // 
            this.mnuVer.Name = "mnuVer";
            this.mnuVer.Size = new System.Drawing.Size(110, 20);
            this.mnuVer.Text = "バージョン情報(&V)";
            // 
            // Hard_Copy
            // 
            this.Hard_Copy.Name = "Hard_Copy";
            this.Hard_Copy.Size = new System.Drawing.Size(86, 20);
            this.Hard_Copy.Text = "画面印刷(&P)";
            // 
            // txtTitle
            // 
            this.txtTitle.BackColor = System.Drawing.Color.Blue;
            this.txtTitle.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.txtTitle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtTitle.Font = new System.Drawing.Font("MS Gothic", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.ForeColor = System.Drawing.Color.Aqua;
            this.txtTitle.Location = new System.Drawing.Point(9, 27);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(403, 45);
            this.txtTitle.TabIndex = 1;
            this.txtTitle.Text = "販売管理システム メニュー";
            this.txtTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCompany
            // 
            this.lblCompany.BackColor = System.Drawing.Color.Navy;
            this.lblCompany.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCompany.Font = new System.Drawing.Font("MS Gothic", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.ForeColor = System.Drawing.Color.White;
            this.lblCompany.Location = new System.Drawing.Point(611, 27);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(274, 32);
            this.lblCompany.TabIndex = 2;
            this.lblCompany.Text = "どこかの会社";
            this.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.cmdMitumori_1);
            this.Frame1.Controls.Add(this.cmdMitumori_0);
            this.Frame1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame1.ForeColor = System.Drawing.Color.Blue;
            this.Frame1.Location = new System.Drawing.Point(20, 75);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(273, 102);
            this.Frame1.TabIndex = 4;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "見積処理";
            // 
            // cmdMitumori_1
            // 
            this.cmdMitumori_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMitumori_1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMitumori_1.ForeColor = System.Drawing.Color.Black;
            this.cmdMitumori_1.Location = new System.Drawing.Point(23, 58);
            this.cmdMitumori_1.Name = "cmdMitumori_1";
            this.cmdMitumori_1.Size = new System.Drawing.Size(223, 32);
            this.cmdMitumori_1.TabIndex = 1;
            this.cmdMitumori_1.Tag = "MITMR020";
            this.cmdMitumori_1.Text = "見積書発行";
            this.cmdMitumori_1.UseVisualStyleBackColor = true;
            // 
            // cmdMitumori_0
            // 
            this.cmdMitumori_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdMitumori_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMitumori_0.ForeColor = System.Drawing.Color.Black;
            this.cmdMitumori_0.Location = new System.Drawing.Point(23, 29);
            this.cmdMitumori_0.Name = "cmdMitumori_0";
            this.cmdMitumori_0.Size = new System.Drawing.Size(223, 31);
            this.cmdMitumori_0.TabIndex = 0;
            this.cmdMitumori_0.Tag = "MITMR010";
            this.cmdMitumori_0.Text = "見積書入力";
            this.cmdMitumori_0.UseVisualStyleBackColor = true;
            // 
            // Frame5
            // 
            this.Frame5.BackColor = System.Drawing.Color.Transparent;
            this.Frame5.Controls.Add(this.cmdJyutyu_5);
            this.Frame5.Controls.Add(this.cmdJyutyu_0);
            this.Frame5.Controls.Add(this.cmdJyutyu_1);
            this.Frame5.Controls.Add(this.cmdJyutyu_2);
            this.Frame5.Controls.Add(this.cmdJyutyu_3);
            this.Frame5.Controls.Add(this.cmdJyutyu_4);
            this.Frame5.Controls.Add(this.cmdJyutyu_6);
            this.Frame5.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame5.ForeColor = System.Drawing.Color.Blue;
            this.Frame5.Location = new System.Drawing.Point(20, 178);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(273, 232);
            this.Frame5.TabIndex = 5;
            this.Frame5.TabStop = false;
            this.Frame5.Text = "受注処理";
            // 
            // cmdJyutyu_5
            // 
            this.cmdJyutyu_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdJyutyu_5.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdJyutyu_5.ForeColor = System.Drawing.Color.Black;
            this.cmdJyutyu_5.Location = new System.Drawing.Point(23, 25);
            this.cmdJyutyu_5.Name = "cmdJyutyu_5";
            this.cmdJyutyu_5.Size = new System.Drawing.Size(223, 28);
            this.cmdJyutyu_5.TabIndex = 2;
            this.cmdJyutyu_5.Tag = "JYUMR000";
            this.cmdJyutyu_5.Text = "見積データ確定";
            this.cmdJyutyu_5.UseVisualStyleBackColor = true;
            // 
            // cmdJyutyu_0
            // 
            this.cmdJyutyu_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdJyutyu_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdJyutyu_0.ForeColor = System.Drawing.Color.Black;
            this.cmdJyutyu_0.Location = new System.Drawing.Point(23, 52);
            this.cmdJyutyu_0.Name = "cmdJyutyu_0";
            this.cmdJyutyu_0.Size = new System.Drawing.Size(223, 27);
            this.cmdJyutyu_0.TabIndex = 3;
            this.cmdJyutyu_0.Tag = "JYUMR010";
            this.cmdJyutyu_0.Text = "受注入力 ";
            this.cmdJyutyu_0.UseVisualStyleBackColor = true;
            // 
            // cmdJyutyu_1
            // 
            this.cmdJyutyu_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdJyutyu_1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdJyutyu_1.ForeColor = System.Drawing.Color.Black;
            this.cmdJyutyu_1.Location = new System.Drawing.Point(23, 78);
            this.cmdJyutyu_1.Name = "cmdJyutyu_1";
            this.cmdJyutyu_1.Size = new System.Drawing.Size(223, 32);
            this.cmdJyutyu_1.TabIndex = 4;
            this.cmdJyutyu_1.Tag = "JYUMR020";
            this.cmdJyutyu_1.Text = "契約台帳";
            this.cmdJyutyu_1.UseVisualStyleBackColor = true;
            // 
            // cmdJyutyu_2
            // 
            this.cmdJyutyu_2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdJyutyu_2.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdJyutyu_2.ForeColor = System.Drawing.Color.Black;
            this.cmdJyutyu_2.Location = new System.Drawing.Point(23, 109);
            this.cmdJyutyu_2.Name = "cmdJyutyu_2";
            this.cmdJyutyu_2.Size = new System.Drawing.Size(223, 28);
            this.cmdJyutyu_2.TabIndex = 5;
            this.cmdJyutyu_2.Tag = "JYUMR030";
            this.cmdJyutyu_2.Text = "注文請書";
            this.cmdJyutyu_2.UseVisualStyleBackColor = true;
            // 
            // cmdJyutyu_3
            // 
            this.cmdJyutyu_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdJyutyu_3.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdJyutyu_3.ForeColor = System.Drawing.Color.Black;
            this.cmdJyutyu_3.Location = new System.Drawing.Point(23, 136);
            this.cmdJyutyu_3.Name = "cmdJyutyu_3";
            this.cmdJyutyu_3.Size = new System.Drawing.Size(223, 28);
            this.cmdJyutyu_3.TabIndex = 6;
            this.cmdJyutyu_3.Tag = "JYUMR041";
            this.cmdJyutyu_3.Text = "発注入力";
            this.cmdJyutyu_3.UseVisualStyleBackColor = true;
            // 
            // cmdJyutyu_4
            // 
            this.cmdJyutyu_4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdJyutyu_4.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdJyutyu_4.ForeColor = System.Drawing.Color.Black;
            this.cmdJyutyu_4.Location = new System.Drawing.Point(23, 163);
            this.cmdJyutyu_4.Name = "cmdJyutyu_4";
            this.cmdJyutyu_4.Size = new System.Drawing.Size(223, 27);
            this.cmdJyutyu_4.TabIndex = 7;
            this.cmdJyutyu_4.Tag = "JYUMR050";
            this.cmdJyutyu_4.Text = "仕入入力";
            this.cmdJyutyu_4.UseVisualStyleBackColor = true;
            // 
            // cmdJyutyu_6
            // 
            this.cmdJyutyu_6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdJyutyu_6.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdJyutyu_6.ForeColor = System.Drawing.Color.Black;
            this.cmdJyutyu_6.Location = new System.Drawing.Point(23, 189);
            this.cmdJyutyu_6.Name = "cmdJyutyu_6";
            this.cmdJyutyu_6.Size = new System.Drawing.Size(223, 30);
            this.cmdJyutyu_6.TabIndex = 8;
            this.cmdJyutyu_6.Tag = "JYUMR060";
            this.cmdJyutyu_6.Text = "仕入先別実績一覧表";
            this.cmdJyutyu_6.UseVisualStyleBackColor = true;
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.Transparent;
            this.Frame2.Controls.Add(this.cmdUriage_0);
            this.Frame2.Controls.Add(this.cmdUriage_10);
            this.Frame2.Controls.Add(this.cmdUriage_4);
            this.Frame2.Controls.Add(this.cmdUriage_7);
            this.Frame2.Controls.Add(this.cmdUriage_8);
            this.Frame2.Controls.Add(this.cmdUriage_5);
            this.Frame2.Controls.Add(this.cmdUriage_1);
            this.Frame2.Controls.Add(this.cmdUriage_9);
            this.Frame2.Controls.Add(this.cmdUriage_11);
            this.Frame2.Controls.Add(this.cmdUriage_2);
            this.Frame2.Controls.Add(this.cmdUriage_3);
            this.Frame2.Controls.Add(this.cmdUriage_6);
            this.Frame2.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame2.ForeColor = System.Drawing.Color.Blue;
            this.Frame2.Location = new System.Drawing.Point(20, 412);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(273, 395);
            this.Frame2.TabIndex = 5;
            this.Frame2.TabStop = false;
            this.Frame2.Text = "入荷処理";
            // 
            // cmdUriage_0
            // 
            this.cmdUriage_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_0.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_0.Location = new System.Drawing.Point(23, 229);
            this.cmdUriage_0.Name = "cmdUriage_0";
            this.cmdUriage_0.Size = new System.Drawing.Size(223, 33);
            this.cmdUriage_0.TabIndex = 16;
            this.cmdUriage_0.Tag = "URIMR051";
            this.cmdUriage_0.Text = "ｶｽﾀﾑｲﾝﾎﾞｲｽ";
            this.cmdUriage_0.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_10
            // 
            this.cmdUriage_10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_10.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_10.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_10.Location = new System.Drawing.Point(23, 261);
            this.cmdUriage_10.Name = "cmdUriage_10";
            this.cmdUriage_10.Size = new System.Drawing.Size(223, 36);
            this.cmdUriage_10.TabIndex = 17;
            this.cmdUriage_10.Tag = "URIMR062";
            this.cmdUriage_10.Text = "COMMERCIAL INVOICE";
            this.cmdUriage_10.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_4
            // 
            this.cmdUriage_4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_4.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_4.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_4.Location = new System.Drawing.Point(23, 296);
            this.cmdUriage_4.Name = "cmdUriage_4";
            this.cmdUriage_4.Size = new System.Drawing.Size(223, 29);
            this.cmdUriage_4.TabIndex = 18;
            this.cmdUriage_4.Tag = "GETMR060";
            this.cmdUriage_4.Text = "売上実績表";
            this.cmdUriage_4.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_7
            // 
            this.cmdUriage_7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_7.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_7.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_7.Location = new System.Drawing.Point(23, 324);
            this.cmdUriage_7.Name = "cmdUriage_7";
            this.cmdUriage_7.Size = new System.Drawing.Size(223, 32);
            this.cmdUriage_7.TabIndex = 19;
            this.cmdUriage_7.Tag = "JYUMR070";
            this.cmdUriage_7.Text = "仮仕入リスト";
            this.cmdUriage_7.UseVisualStyleBackColor = true;
            this.cmdUriage_7.Click += new System.EventHandler(this.cmdUriage_7_Click);
            // 
            // cmdUriage_8
            // 
            this.cmdUriage_8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_8.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_8.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_8.Location = new System.Drawing.Point(23, 355);
            this.cmdUriage_8.Name = "cmdUriage_8";
            this.cmdUriage_8.Size = new System.Drawing.Size(223, 28);
            this.cmdUriage_8.TabIndex = 20;
            this.cmdUriage_8.Tag = "JYUMR080";
            this.cmdUriage_8.Text = "在庫リスト";
            this.cmdUriage_8.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_5
            // 
            this.cmdUriage_5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_5.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_5.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_5.Location = new System.Drawing.Point(23, 25);
            this.cmdUriage_5.Name = "cmdUriage_5";
            this.cmdUriage_5.Size = new System.Drawing.Size(223, 28);
            this.cmdUriage_5.TabIndex = 9;
            this.cmdUriage_5.Tag = "URIMR091";
            this.cmdUriage_5.Text = "入荷完了入力";
            this.cmdUriage_5.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_1
            // 
            this.cmdUriage_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_1.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_1.Location = new System.Drawing.Point(23, 52);
            this.cmdUriage_1.Name = "cmdUriage_1";
            this.cmdUriage_1.Size = new System.Drawing.Size(223, 36);
            this.cmdUriage_1.TabIndex = 10;
            this.cmdUriage_1.Tag = "URIMR050";
            this.cmdUriage_1.Text = "納品書/受領書";
            this.cmdUriage_1.UseVisualStyleBackColor = true;
            this.cmdUriage_1.Click += new System.EventHandler(this.cmdUriage_1_Click);
            // 
            // cmdUriage_9
            // 
            this.cmdUriage_9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_9.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_9.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_9.Location = new System.Drawing.Point(23, 87);
            this.cmdUriage_9.Name = "cmdUriage_9";
            this.cmdUriage_9.Size = new System.Drawing.Size(223, 27);
            this.cmdUriage_9.TabIndex = 11;
            this.cmdUriage_9.Tag = "TTLABEL";
            this.cmdUriage_9.Text = "ラベル印刷";
            this.cmdUriage_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdUriage_9.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_11
            // 
            this.cmdUriage_11.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_11.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_11.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_11.Location = new System.Drawing.Point(23, 113);
            this.cmdUriage_11.Name = "cmdUriage_11";
            this.cmdUriage_11.Size = new System.Drawing.Size(223, 31);
            this.cmdUriage_11.TabIndex = 12;
            this.cmdUriage_11.Tag = "URIMR063";
            this.cmdUriage_11.Text = "出荷処理";
            this.cmdUriage_11.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_2
            // 
            this.cmdUriage_2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_2.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_2.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_2.Location = new System.Drawing.Point(23, 143);
            this.cmdUriage_2.Name = "cmdUriage_2";
            this.cmdUriage_2.Size = new System.Drawing.Size(223, 29);
            this.cmdUriage_2.TabIndex = 13;
            this.cmdUriage_2.Tag = "URIMR061";
            this.cmdUriage_2.Text = "梱包入力";
            this.cmdUriage_2.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_3
            // 
            this.cmdUriage_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_3.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_3.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_3.Location = new System.Drawing.Point(23, 171);
            this.cmdUriage_3.Name = "cmdUriage_3";
            this.cmdUriage_3.Size = new System.Drawing.Size(223, 30);
            this.cmdUriage_3.TabIndex = 14;
            this.cmdUriage_3.Tag = "URIMR071";
            this.cmdUriage_3.Text = "出荷台帳印刷";
            this.cmdUriage_3.UseVisualStyleBackColor = true;
            // 
            // cmdUriage_6
            // 
            this.cmdUriage_6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdUriage_6.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdUriage_6.ForeColor = System.Drawing.Color.Black;
            this.cmdUriage_6.Location = new System.Drawing.Point(23, 200);
            this.cmdUriage_6.Name = "cmdUriage_6";
            this.cmdUriage_6.Size = new System.Drawing.Size(223, 30);
            this.cmdUriage_6.TabIndex = 15;
            this.cmdUriage_6.Tag = "URIMR075";
            this.cmdUriage_6.Text = "出荷台帳問合せ";
            this.cmdUriage_6.UseVisualStyleBackColor = true;
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.Transparent;
            this.Frame3.Controls.Add(this.cmdSeikyu_0);
            this.Frame3.Controls.Add(this.cmdSeikyu_4);
            this.Frame3.Controls.Add(this.cmdSeikyu_3);
            this.Frame3.Controls.Add(this.cmdSeikyu_2);
            this.Frame3.Controls.Add(this.cmdSeikyu_1);
            this.Frame3.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame3.ForeColor = System.Drawing.Color.Blue;
            this.Frame3.Location = new System.Drawing.Point(310, 74);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(271, 190);
            this.Frame3.TabIndex = 5;
            this.Frame3.TabStop = false;
            this.Frame3.Text = "請求処理";
            // 
            // cmdSeikyu_0
            // 
            this.cmdSeikyu_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdSeikyu_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdSeikyu_0.ForeColor = System.Drawing.Color.Black;
            this.cmdSeikyu_0.Location = new System.Drawing.Point(25, 25);
            this.cmdSeikyu_0.Name = "cmdSeikyu_0";
            this.cmdSeikyu_0.Size = new System.Drawing.Size(223, 31);
            this.cmdSeikyu_0.TabIndex = 16;
            this.cmdSeikyu_0.Tag = "SEIMR011";
            this.cmdSeikyu_0.Text = "請求書発行準備";
            this.cmdSeikyu_0.UseVisualStyleBackColor = true;
            // 
            // cmdSeikyu_4
            // 
            this.cmdSeikyu_4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdSeikyu_4.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdSeikyu_4.ForeColor = System.Drawing.Color.Black;
            this.cmdSeikyu_4.Location = new System.Drawing.Point(25, 55);
            this.cmdSeikyu_4.Name = "cmdSeikyu_4";
            this.cmdSeikyu_4.Size = new System.Drawing.Size(223, 32);
            this.cmdSeikyu_4.TabIndex = 17;
            this.cmdSeikyu_4.Tag = "SEIMR015";
            this.cmdSeikyu_4.Text = "請求ﾃﾞｰﾀﾒﾝﾃﾅﾝｽ";
            this.cmdSeikyu_4.UseVisualStyleBackColor = true;
            // 
            // cmdSeikyu_3
            // 
            this.cmdSeikyu_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdSeikyu_3.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdSeikyu_3.ForeColor = System.Drawing.Color.Black;
            this.cmdSeikyu_3.Location = new System.Drawing.Point(25, 142);
            this.cmdSeikyu_3.Name = "cmdSeikyu_3";
            this.cmdSeikyu_3.Size = new System.Drawing.Size(223, 33);
            this.cmdSeikyu_3.TabIndex = 20;
            this.cmdSeikyu_3.Tag = "SEIMR040";
            this.cmdSeikyu_3.Text = "請求取消し";
            this.cmdSeikyu_3.UseVisualStyleBackColor = true;
            this.cmdSeikyu_3.Click += new System.EventHandler(this.cmdSeikyu_3_Click);
            // 
            // cmdSeikyu_2
            // 
            this.cmdSeikyu_2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdSeikyu_2.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdSeikyu_2.ForeColor = System.Drawing.Color.Black;
            this.cmdSeikyu_2.Location = new System.Drawing.Point(25, 112);
            this.cmdSeikyu_2.Name = "cmdSeikyu_2";
            this.cmdSeikyu_2.Size = new System.Drawing.Size(223, 31);
            this.cmdSeikyu_2.TabIndex = 19;
            this.cmdSeikyu_2.Tag = "SEIMR030";
            this.cmdSeikyu_2.Text = "請求一覧表";
            this.cmdSeikyu_2.UseVisualStyleBackColor = true;
            this.cmdSeikyu_2.Click += new System.EventHandler(this.cmdSeikyu_2_Click);
            // 
            // cmdSeikyu_1
            // 
            this.cmdSeikyu_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdSeikyu_1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdSeikyu_1.ForeColor = System.Drawing.Color.Black;
            this.cmdSeikyu_1.Location = new System.Drawing.Point(25, 86);
            this.cmdSeikyu_1.Name = "cmdSeikyu_1";
            this.cmdSeikyu_1.Size = new System.Drawing.Size(223, 27);
            this.cmdSeikyu_1.TabIndex = 18;
            this.cmdSeikyu_1.Tag = "SEIMR020";
            this.cmdSeikyu_1.Text = "請求ﾃﾞｰﾀﾒﾝﾃﾅﾝｽ";
            this.cmdSeikyu_1.UseVisualStyleBackColor = true;
            // 
            // Frame7
            // 
            this.Frame7.BackColor = System.Drawing.Color.Transparent;
            this.Frame7.Controls.Add(this.cmdZuiji_6);
            this.Frame7.Controls.Add(this.cmdZuiji_7);
            this.Frame7.Controls.Add(this.cmdZuiji_0);
            this.Frame7.Controls.Add(this.cmdZuiji_5);
            this.Frame7.Controls.Add(this.cmdZuiji_1);
            this.Frame7.Controls.Add(this.cmdZuiji_2);
            this.Frame7.Controls.Add(this.cmdZuiji_3);
            this.Frame7.Controls.Add(this.cmdZuiji_4);
            this.Frame7.Controls.Add(this.cmdGetuji_3);
            this.Frame7.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame7.ForeColor = System.Drawing.Color.Blue;
            this.Frame7.Location = new System.Drawing.Point(310, 270);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(271, 306);
            this.Frame7.TabIndex = 5;
            this.Frame7.TabStop = false;
            this.Frame7.Text = "随時処理";
            // 
            // cmdZuiji_6
            // 
            this.cmdZuiji_6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZuiji_6.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZuiji_6.ForeColor = System.Drawing.Color.Black;
            this.cmdZuiji_6.Location = new System.Drawing.Point(23, 233);
            this.cmdZuiji_6.Name = "cmdZuiji_6";
            this.cmdZuiji_6.Size = new System.Drawing.Size(223, 26);
            this.cmdZuiji_6.TabIndex = 25;
            this.cmdZuiji_6.Tag = "GETMR070";
            this.cmdZuiji_6.Text = "仕入ﾃﾞｰﾀTXT出力";
            this.cmdZuiji_6.UseVisualStyleBackColor = true;
            // 
            // cmdZuiji_7
            // 
            this.cmdZuiji_7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZuiji_7.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZuiji_7.ForeColor = System.Drawing.Color.Black;
            this.cmdZuiji_7.Location = new System.Drawing.Point(23, 258);
            this.cmdZuiji_7.Name = "cmdZuiji_7";
            this.cmdZuiji_7.Size = new System.Drawing.Size(223, 29);
            this.cmdZuiji_7.TabIndex = 26;
            this.cmdZuiji_7.Tag = "ZUIMR071";
            this.cmdZuiji_7.Text = "倉庫リスト";
            this.cmdZuiji_7.UseVisualStyleBackColor = true;
            // 
            // cmdZuiji_0
            // 
            this.cmdZuiji_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZuiji_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZuiji_0.ForeColor = System.Drawing.Color.Black;
            this.cmdZuiji_0.Location = new System.Drawing.Point(23, 29);
            this.cmdZuiji_0.Name = "cmdZuiji_0";
            this.cmdZuiji_0.Size = new System.Drawing.Size(223, 28);
            this.cmdZuiji_0.TabIndex = 18;
            this.cmdZuiji_0.Tag = "URIMR085";
            this.cmdZuiji_0.Text = "STATUS OF ORDER";
            this.cmdZuiji_0.UseVisualStyleBackColor = true;
            // 
            // cmdZuiji_5
            // 
            this.cmdZuiji_5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZuiji_5.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZuiji_5.ForeColor = System.Drawing.Color.Black;
            this.cmdZuiji_5.Location = new System.Drawing.Point(23, 56);
            this.cmdZuiji_5.Name = "cmdZuiji_5";
            this.cmdZuiji_5.Size = new System.Drawing.Size(223, 31);
            this.cmdZuiji_5.TabIndex = 19;
            this.cmdZuiji_5.Tag = "URIMR084";
            this.cmdZuiji_5.Text = "オーダー状況問合せ";
            this.cmdZuiji_5.UseVisualStyleBackColor = true;
            // 
            // cmdZuiji_1
            // 
            this.cmdZuiji_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZuiji_1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZuiji_1.ForeColor = System.Drawing.Color.Black;
            this.cmdZuiji_1.Location = new System.Drawing.Point(23, 86);
            this.cmdZuiji_1.Name = "cmdZuiji_1";
            this.cmdZuiji_1.Size = new System.Drawing.Size(223, 31);
            this.cmdZuiji_1.TabIndex = 20;
            this.cmdZuiji_1.Tag = "GETMR050";
            this.cmdZuiji_1.Text = "CHECK OPEN BOX";
            this.cmdZuiji_1.UseVisualStyleBackColor = true;
            this.cmdZuiji_1.Click += new System.EventHandler(this.cmdZuiji_1_Click);
            // 
            // cmdZuiji_2
            // 
            this.cmdZuiji_2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZuiji_2.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZuiji_2.ForeColor = System.Drawing.Color.Black;
            this.cmdZuiji_2.Location = new System.Drawing.Point(23, 116);
            this.cmdZuiji_2.Name = "cmdZuiji_2";
            this.cmdZuiji_2.Size = new System.Drawing.Size(223, 31);
            this.cmdZuiji_2.TabIndex = 21;
            this.cmdZuiji_2.Tag = "ZUIMR030";
            this.cmdZuiji_2.Text = "データ問合せ(詳細)";
            this.cmdZuiji_2.UseVisualStyleBackColor = true;
            // 
            // cmdZuiji_3
            // 
            this.cmdZuiji_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZuiji_3.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZuiji_3.ForeColor = System.Drawing.Color.Black;
            this.cmdZuiji_3.Location = new System.Drawing.Point(23, 146);
            this.cmdZuiji_3.Name = "cmdZuiji_3";
            this.cmdZuiji_3.Size = new System.Drawing.Size(223, 30);
            this.cmdZuiji_3.TabIndex = 22;
            this.cmdZuiji_3.Tag = "ZUIMR031";
            this.cmdZuiji_3.Text = "データ問合せ(ｵｰﾀﾞｰ)";
            this.cmdZuiji_3.UseVisualStyleBackColor = true;
            // 
            // cmdZuiji_4
            // 
            this.cmdZuiji_4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZuiji_4.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZuiji_4.ForeColor = System.Drawing.Color.Black;
            this.cmdZuiji_4.Location = new System.Drawing.Point(23, 175);
            this.cmdZuiji_4.Name = "cmdZuiji_4";
            this.cmdZuiji_4.Size = new System.Drawing.Size(223, 31);
            this.cmdZuiji_4.TabIndex = 23;
            this.cmdZuiji_4.Tag = "ZUIMR051";
            this.cmdZuiji_4.Text = "入荷予定日遅延問合せ";
            this.cmdZuiji_4.UseVisualStyleBackColor = true;
            // 
            // cmdGetuji_3
            // 
            this.cmdGetuji_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdGetuji_3.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdGetuji_3.ForeColor = System.Drawing.Color.Black;
            this.cmdGetuji_3.Location = new System.Drawing.Point(23, 205);
            this.cmdGetuji_3.Name = "cmdGetuji_3";
            this.cmdGetuji_3.Size = new System.Drawing.Size(223, 29);
            this.cmdGetuji_3.TabIndex = 24;
            this.cmdGetuji_3.Tag = "GETMR040";
            this.cmdGetuji_3.Text = "売上ﾃﾞｰﾀTXT出力";
            this.cmdGetuji_3.UseVisualStyleBackColor = true;
            // 
            // Frame6
            // 
            this.Frame6.BackColor = System.Drawing.Color.Transparent;
            this.Frame6.Controls.Add(this.button36);
            this.Frame6.Controls.Add(this.button37);
            this.Frame6.Controls.Add(this.button38);
            this.Frame6.Controls.Add(this.button39);
            this.Frame6.Controls.Add(this.button40);
            this.Frame6.Controls.Add(this.button41);
            this.Frame6.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame6.ForeColor = System.Drawing.Color.Blue;
            this.Frame6.Location = new System.Drawing.Point(310, 583);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(271, 224);
            this.Frame6.TabIndex = 5;
            this.Frame6.TabStop = false;
            this.Frame6.Text = "入金処理";
            this.Frame6.Enter += new System.EventHandler(this.Frame6_Enter);
            // 
            // button36
            // 
            this.button36.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button36.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button36.ForeColor = System.Drawing.Color.Black;
            this.button36.Location = new System.Drawing.Point(23, 33);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(223, 32);
            this.button36.TabIndex = 15;
            this.button36.Tag = "NYUMR010";
            this.button36.Text = "入金予定一覧表";
            this.button36.UseVisualStyleBackColor = true;
            // 
            // button37
            // 
            this.button37.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button37.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button37.ForeColor = System.Drawing.Color.Black;
            this.button37.Location = new System.Drawing.Point(23, 64);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(223, 29);
            this.button37.TabIndex = 16;
            this.button37.Tag = "NYUMR020";
            this.button37.Text = "入金依頼書";
            this.button37.UseVisualStyleBackColor = true;
            // 
            // button38
            // 
            this.button38.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button38.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button38.ForeColor = System.Drawing.Color.Black;
            this.button38.Location = new System.Drawing.Point(23, 92);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(223, 25);
            this.button38.TabIndex = 17;
            this.button38.Tag = "NYUMR030";
            this.button38.Text = "入金入力";
            this.button38.UseVisualStyleBackColor = true;
            // 
            // button39
            // 
            this.button39.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button39.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button39.ForeColor = System.Drawing.Color.Black;
            this.button39.Location = new System.Drawing.Point(23, 116);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(223, 32);
            this.button39.TabIndex = 18;
            this.button39.Tag = "NYUMR040";
            this.button39.Text = "入金結果リスト";
            this.button39.UseVisualStyleBackColor = true;
            // 
            // button40
            // 
            this.button40.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button40.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button40.ForeColor = System.Drawing.Color.Black;
            this.button40.Location = new System.Drawing.Point(23, 147);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(223, 30);
            this.button40.TabIndex = 19;
            this.button40.Tag = "NYUMR050";
            this.button40.Text = "入金遅延一覧表";
            this.button40.UseVisualStyleBackColor = true;
            // 
            // button41
            // 
            this.button41.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button41.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button41.ForeColor = System.Drawing.Color.Black;
            this.button41.Location = new System.Drawing.Point(23, 176);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(223, 26);
            this.button41.TabIndex = 20;
            this.button41.Tag = "NYUMR060";
            this.button41.Text = "入金状況問合せ";
            this.button41.UseVisualStyleBackColor = true;
            // 
            // Frame8
            // 
            this.Frame8.BackColor = System.Drawing.Color.Transparent;
            this.Frame8.Controls.Add(this.cmdMaster_0);
            this.Frame8.Controls.Add(this.cmdMaster_1);
            this.Frame8.Controls.Add(this.cmdMaster_5);
            this.Frame8.Controls.Add(this.cmdMaster_4);
            this.Frame8.Controls.Add(this.cmdMaster_2);
            this.Frame8.Controls.Add(this.cmdMaster_6);
            this.Frame8.Controls.Add(this.cmdMaster_3);
            this.Frame8.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame8.ForeColor = System.Drawing.Color.Blue;
            this.Frame8.Location = new System.Drawing.Point(609, 72);
            this.Frame8.Name = "Frame8";
            this.Frame8.Size = new System.Drawing.Size(265, 244);
            this.Frame8.TabIndex = 5;
            this.Frame8.TabStop = false;
            this.Frame8.Text = "各種ﾏｽﾀﾒﾝﾃ";
            // 
            // cmdMaster_0
            // 
            this.cmdMaster_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdMaster_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMaster_0.ForeColor = System.Drawing.Color.Black;
            this.cmdMaster_0.Location = new System.Drawing.Point(21, 25);
            this.cmdMaster_0.Name = "cmdMaster_0";
            this.cmdMaster_0.Size = new System.Drawing.Size(223, 31);
            this.cmdMaster_0.TabIndex = 27;
            this.cmdMaster_0.Tag = "MSTMR010";
            this.cmdMaster_0.Text = "ｺﾝﾄﾛｰﾙﾏｽﾀﾒﾝﾃ";
            this.cmdMaster_0.UseVisualStyleBackColor = true;
            // 
            // cmdMaster_1
            // 
            this.cmdMaster_1.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdMaster_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdMaster_1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMaster_1.ForeColor = System.Drawing.Color.Black;
            this.cmdMaster_1.Location = new System.Drawing.Point(21, 55);
            this.cmdMaster_1.Name = "cmdMaster_1";
            this.cmdMaster_1.Size = new System.Drawing.Size(223, 32);
            this.cmdMaster_1.TabIndex = 28;
            this.cmdMaster_1.Tag = "MSTMR020";
            this.cmdMaster_1.Text = "得意先ﾏｽﾀﾒﾝﾃ";
            this.cmdMaster_1.UseVisualStyleBackColor = true;
            this.cmdMaster_1.Click += new System.EventHandler(this.cmdMaster_1_Click);
            // 
            // cmdMaster_5
            // 
            this.cmdMaster_5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdMaster_5.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMaster_5.ForeColor = System.Drawing.Color.Black;
            this.cmdMaster_5.Location = new System.Drawing.Point(21, 178);
            this.cmdMaster_5.Name = "cmdMaster_5";
            this.cmdMaster_5.Size = new System.Drawing.Size(223, 26);
            this.cmdMaster_5.TabIndex = 32;
            this.cmdMaster_5.Tag = "MSTAA080";
            this.cmdMaster_5.Text = "銀行ﾏｽﾀﾒﾝﾃ";
            this.cmdMaster_5.UseVisualStyleBackColor = true;
            // 
            // cmdMaster_4
            // 
            this.cmdMaster_4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdMaster_4.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMaster_4.ForeColor = System.Drawing.Color.Black;
            this.cmdMaster_4.Location = new System.Drawing.Point(21, 146);
            this.cmdMaster_4.Name = "cmdMaster_4";
            this.cmdMaster_4.Size = new System.Drawing.Size(223, 33);
            this.cmdMaster_4.TabIndex = 31;
            this.cmdMaster_4.Tag = "MSTMR031";
            this.cmdMaster_4.Text = "仕入先ﾏｽﾀﾘｽﾄ";
            this.cmdMaster_4.UseVisualStyleBackColor = true;
            // 
            // cmdMaster_2
            // 
            this.cmdMaster_2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdMaster_2.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMaster_2.ForeColor = System.Drawing.Color.Black;
            this.cmdMaster_2.Location = new System.Drawing.Point(21, 86);
            this.cmdMaster_2.Name = "cmdMaster_2";
            this.cmdMaster_2.Size = new System.Drawing.Size(223, 28);
            this.cmdMaster_2.TabIndex = 29;
            this.cmdMaster_2.Tag = "MSTMR021";
            this.cmdMaster_2.Text = "得意先ﾏｽﾀﾘｽﾄ";
            this.cmdMaster_2.UseVisualStyleBackColor = true;
            // 
            // cmdMaster_6
            // 
            this.cmdMaster_6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdMaster_6.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMaster_6.ForeColor = System.Drawing.Color.Black;
            this.cmdMaster_6.Location = new System.Drawing.Point(21, 203);
            this.cmdMaster_6.Name = "cmdMaster_6";
            this.cmdMaster_6.Size = new System.Drawing.Size(223, 27);
            this.cmdMaster_6.TabIndex = 33;
            this.cmdMaster_6.Tag = "MSTAA200";
            this.cmdMaster_6.Text = "区分名称ﾏｽﾀﾒﾝﾃ";
            this.cmdMaster_6.UseVisualStyleBackColor = true;
            // 
            // cmdMaster_3
            // 
            this.cmdMaster_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdMaster_3.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdMaster_3.ForeColor = System.Drawing.Color.Black;
            this.cmdMaster_3.Location = new System.Drawing.Point(21, 113);
            this.cmdMaster_3.Name = "cmdMaster_3";
            this.cmdMaster_3.Size = new System.Drawing.Size(223, 34);
            this.cmdMaster_3.TabIndex = 30;
            this.cmdMaster_3.Tag = "MSTMR030";
            this.cmdMaster_3.Text = "仕入先ﾏｽﾀﾒﾝﾃ";
            this.cmdMaster_3.UseVisualStyleBackColor = true;
            // 
            // Frame9
            // 
            this.Frame9.BackColor = System.Drawing.Color.Transparent;
            this.Frame9.Controls.Add(this.cmdTable_7);
            this.Frame9.Controls.Add(this.cmdTable_8);
            this.Frame9.Controls.Add(this.cmdTable_0);
            this.Frame9.Controls.Add(this.cmdTable_2);
            this.Frame9.Controls.Add(this.cmdTable_3);
            this.Frame9.Controls.Add(this.cmdTable_4);
            this.Frame9.Controls.Add(this.cmdTable_1);
            this.Frame9.Controls.Add(this.cmdTable_5);
            this.Frame9.Controls.Add(this.cmdTable_6);
            this.Frame9.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame9.ForeColor = System.Drawing.Color.Blue;
            this.Frame9.Location = new System.Drawing.Point(609, 322);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(265, 305);
            this.Frame9.TabIndex = 5;
            this.Frame9.TabStop = false;
            this.Frame9.Text = "各種ﾃｰﾌﾞﾙﾒﾝﾃ";
            // 
            // cmdTable_7
            // 
            this.cmdTable_7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_7.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_7.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_7.Location = new System.Drawing.Point(21, 234);
            this.cmdTable_7.Name = "cmdTable_7";
            this.cmdTable_7.Size = new System.Drawing.Size(223, 26);
            this.cmdTable_7.TabIndex = 34;
            this.cmdTable_7.Tag = "TBLMR080";
            this.cmdTable_7.Text = "ｵｰﾅｰﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_7.UseVisualStyleBackColor = true;
            // 
            // cmdTable_8
            // 
            this.cmdTable_8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_8.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_8.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_8.Location = new System.Drawing.Point(21, 259);
            this.cmdTable_8.Name = "cmdTable_8";
            this.cmdTable_8.Size = new System.Drawing.Size(223, 27);
            this.cmdTable_8.TabIndex = 35;
            this.cmdTable_8.Tag = "TBLMR090";
            this.cmdTable_8.Text = "請求住所ﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_8.UseVisualStyleBackColor = true;
            // 
            // cmdTable_0
            // 
            this.cmdTable_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_0.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_0.Location = new System.Drawing.Point(21, 28);
            this.cmdTable_0.Name = "cmdTable_0";
            this.cmdTable_0.Size = new System.Drawing.Size(223, 33);
            this.cmdTable_0.TabIndex = 27;
            this.cmdTable_0.Tag = "TBLMR010";
            this.cmdTable_0.Text = "船名ﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_0.UseVisualStyleBackColor = true;
            // 
            // cmdTable_2
            // 
            this.cmdTable_2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_2.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_2.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_2.Location = new System.Drawing.Point(21, 60);
            this.cmdTable_2.Name = "cmdTable_2";
            this.cmdTable_2.Size = new System.Drawing.Size(223, 31);
            this.cmdTable_2.TabIndex = 28;
            this.cmdTable_2.Tag = "TBLMR030";
            this.cmdTable_2.Text = "入荷場所ﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_2.UseVisualStyleBackColor = true;
            // 
            // cmdTable_3
            // 
            this.cmdTable_3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_3.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_3.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_3.Location = new System.Drawing.Point(21, 90);
            this.cmdTable_3.Name = "cmdTable_3";
            this.cmdTable_3.Size = new System.Drawing.Size(223, 32);
            this.cmdTable_3.TabIndex = 29;
            this.cmdTable_3.Tag = "TBLMR040";
            this.cmdTable_3.Text = "発送先ﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_3.UseVisualStyleBackColor = true;
            // 
            // cmdTable_4
            // 
            this.cmdTable_4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_4.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_4.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_4.Location = new System.Drawing.Point(21, 121);
            this.cmdTable_4.Name = "cmdTable_4";
            this.cmdTable_4.Size = new System.Drawing.Size(223, 31);
            this.cmdTable_4.TabIndex = 30;
            this.cmdTable_4.Tag = "TBLMR050";
            this.cmdTable_4.Text = "UNITﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_4.UseVisualStyleBackColor = true;
            // 
            // cmdTable_1
            // 
            this.cmdTable_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_1.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_1.Location = new System.Drawing.Point(21, 151);
            this.cmdTable_1.Name = "cmdTable_1";
            this.cmdTable_1.Size = new System.Drawing.Size(223, 32);
            this.cmdTable_1.TabIndex = 31;
            this.cmdTable_1.Tag = "TBLMR020";
            this.cmdTable_1.Text = "MAKERﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_1.UseVisualStyleBackColor = true;
            // 
            // cmdTable_5
            // 
            this.cmdTable_5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_5.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_5.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_5.Location = new System.Drawing.Point(21, 182);
            this.cmdTable_5.Name = "cmdTable_5";
            this.cmdTable_5.Size = new System.Drawing.Size(223, 27);
            this.cmdTable_5.TabIndex = 32;
            this.cmdTable_5.Tag = "TBLMR060";
            this.cmdTable_5.Text = "TYPEﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_5.UseVisualStyleBackColor = true;
            // 
            // cmdTable_6
            // 
            this.cmdTable_6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdTable_6.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdTable_6.ForeColor = System.Drawing.Color.Black;
            this.cmdTable_6.Location = new System.Drawing.Point(21, 208);
            this.cmdTable_6.Name = "cmdTable_6";
            this.cmdTable_6.Size = new System.Drawing.Size(223, 27);
            this.cmdTable_6.TabIndex = 33;
            this.cmdTable_6.Tag = "TBLMR070";
            this.cmdTable_6.Text = "品名ﾃｰﾌﾞﾙﾒﾝﾃ";
            this.cmdTable_6.UseVisualStyleBackColor = true;
            // 
            // Frame10
            // 
            this.Frame10.BackColor = System.Drawing.Color.Transparent;
            this.Frame10.Controls.Add(this.cmdZaiko_0);
            this.Frame10.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame10.ForeColor = System.Drawing.Color.Blue;
            this.Frame10.Location = new System.Drawing.Point(609, 633);
            this.Frame10.Name = "Frame10";
            this.Frame10.Size = new System.Drawing.Size(265, 69);
            this.Frame10.TabIndex = 5;
            this.Frame10.TabStop = false;
            this.Frame10.Text = "在庫処理";
            // 
            // cmdZaiko_0
            // 
            this.cmdZaiko_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdZaiko_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdZaiko_0.ForeColor = System.Drawing.Color.Black;
            this.cmdZaiko_0.Location = new System.Drawing.Point(21, 26);
            this.cmdZaiko_0.Name = "cmdZaiko_0";
            this.cmdZaiko_0.Size = new System.Drawing.Size(223, 29);
            this.cmdZaiko_0.TabIndex = 16;
            this.cmdZaiko_0.Tag = "ZAIMenu";
            this.cmdZaiko_0.Text = "在庫メニュー";
            this.cmdZaiko_0.UseVisualStyleBackColor = true;
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.Transparent;
            this.Frame4.Controls.Add(this.cmdSonota_0);
            this.Frame4.Controls.Add(this.cmdSonota_1);
            this.Frame4.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame4.ForeColor = System.Drawing.Color.Blue;
            this.Frame4.Location = new System.Drawing.Point(609, 708);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(265, 99);
            this.Frame4.TabIndex = 5;
            this.Frame4.TabStop = false;
            this.Frame4.Text = "その他";
            // 
            // cmdSonota_0
            // 
            this.cmdSonota_0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdSonota_0.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdSonota_0.ForeColor = System.Drawing.Color.Black;
            this.cmdSonota_0.Location = new System.Drawing.Point(18, 26);
            this.cmdSonota_0.Name = "cmdSonota_0";
            this.cmdSonota_0.Size = new System.Drawing.Size(226, 30);
            this.cmdSonota_0.TabIndex = 17;
            this.cmdSonota_0.Tag = "SABackUp";
            this.cmdSonota_0.Text = "ﾊﾞｯｸｱｯﾌﾟ処理";
            this.cmdSonota_0.UseVisualStyleBackColor = true;
            // 
            // cmdSonota_1
            // 
            this.cmdSonota_1.BackColor = System.Drawing.Color.Transparent;
            this.cmdSonota_1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cmdSonota_1.Font = new System.Drawing.Font("MS Mincho", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdSonota_1.ForeColor = System.Drawing.Color.Black;
            this.cmdSonota_1.Location = new System.Drawing.Point(18, 51);
            this.cmdSonota_1.Name = "cmdSonota_1";
            this.cmdSonota_1.Size = new System.Drawing.Size(226, 37);
            this.cmdSonota_1.TabIndex = 18;
            this.cmdSonota_1.Text = "ﾒﾆｭｰの終了";
            this.cmdSonota_1.UseVisualStyleBackColor = false;
            // 
            // frmSAMenu3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.BackgroundImage = global::SAMenu3.Properties.Resources._427362_windows_7_aqua_logon_screen_by_zedzna_on_deviantart_1280x1024_h;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(889, 807);
            this.Controls.Add(this.Frame5);
            this.Controls.Add(this.Frame2);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.Frame7);
            this.Controls.Add(this.Frame6);
            this.Controls.Add(this.Frame8);
            this.Controls.Add(this.Frame9);
            this.Controls.Add(this.Frame10);
            this.Controls.Add(this.Frame4);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.P);
            this.ForeColor = System.Drawing.Color.Blue;
            this.MainMenuStrip = this.P;
            this.Name = "frmSAMenu3";
            this.Text = "販売管理システム メニュー";
            this.Load += new System.EventHandler(this.frmSAMenu3_Load);
            this.P.ResumeLayout(false);
            this.P.PerformLayout();
            this.Frame1.ResumeLayout(false);
            this.Frame5.ResumeLayout(false);
            this.Frame2.ResumeLayout(false);
            this.Frame3.ResumeLayout(false);
            this.Frame7.ResumeLayout(false);
            this.Frame6.ResumeLayout(false);
            this.Frame8.ResumeLayout(false);
            this.Frame9.ResumeLayout(false);
            this.Frame10.ResumeLayout(false);
            this.Frame4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip P;
        private System.Windows.Forms.ToolStripMenuItem mnuVer;
        private System.Windows.Forms.ToolStripMenuItem Hard_Copy;
        private System.Windows.Forms.Label txtTitle;
        private System.Windows.Forms.Label lblCompany;
        private System.Windows.Forms.GroupBox Frame1;
        private System.Windows.Forms.GroupBox Frame5;
        private System.Windows.Forms.GroupBox Frame2;
        private System.Windows.Forms.GroupBox Frame3;
        private System.Windows.Forms.GroupBox Frame7;
        private System.Windows.Forms.GroupBox Frame6;
        private System.Windows.Forms.GroupBox Frame8;
        private System.Windows.Forms.GroupBox Frame9;
        private System.Windows.Forms.GroupBox Frame10;
        private System.Windows.Forms.GroupBox Frame4;
        private System.Windows.Forms.Button cmdMitumori_1;
        private System.Windows.Forms.Button cmdMitumori_0;
        private System.Windows.Forms.Button cmdJyutyu_5;
        private System.Windows.Forms.Button cmdJyutyu_0;
        private System.Windows.Forms.Button cmdJyutyu_1;
        private System.Windows.Forms.Button cmdJyutyu_2;
        private System.Windows.Forms.Button cmdJyutyu_3;
        private System.Windows.Forms.Button cmdJyutyu_4;
        private System.Windows.Forms.Button cmdJyutyu_6;
        private System.Windows.Forms.Button cmdUriage_0;
        private System.Windows.Forms.Button cmdUriage_10;
        private System.Windows.Forms.Button cmdUriage_4;
        private System.Windows.Forms.Button cmdUriage_7;
        private System.Windows.Forms.Button cmdUriage_8;
        private System.Windows.Forms.Button cmdUriage_5;
        private System.Windows.Forms.Button cmdUriage_1;
        private System.Windows.Forms.Button cmdUriage_9;
        private System.Windows.Forms.Button cmdUriage_11;
        private System.Windows.Forms.Button cmdUriage_2;
        private System.Windows.Forms.Button cmdUriage_3;
        private System.Windows.Forms.Button cmdUriage_6;
        private System.Windows.Forms.Button cmdSeikyu_0;
        private System.Windows.Forms.Button cmdSeikyu_4;
        private System.Windows.Forms.Button cmdSeikyu_1;
        private System.Windows.Forms.Button cmdSeikyu_2;
        private System.Windows.Forms.Button cmdSeikyu_3;
        private System.Windows.Forms.Button cmdZuiji_6;
        private System.Windows.Forms.Button cmdZuiji_7;
        private System.Windows.Forms.Button cmdZuiji_0;
        private System.Windows.Forms.Button cmdZuiji_5;
        private System.Windows.Forms.Button cmdZuiji_1;
        private System.Windows.Forms.Button cmdZuiji_2;
        private System.Windows.Forms.Button cmdZuiji_3;
        private System.Windows.Forms.Button cmdZuiji_4;
        private System.Windows.Forms.Button cmdGetuji_3;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button cmdMaster_0;
        private System.Windows.Forms.Button cmdMaster_1;
        private System.Windows.Forms.Button cmdMaster_2;
        private System.Windows.Forms.Button cmdMaster_3;
        private System.Windows.Forms.Button cmdMaster_4;
        private System.Windows.Forms.Button cmdMaster_5;
        private System.Windows.Forms.Button cmdMaster_6;
        private System.Windows.Forms.Button cmdTable_7;
        private System.Windows.Forms.Button cmdTable_8;
        private System.Windows.Forms.Button cmdTable_0;
        private System.Windows.Forms.Button cmdTable_2;
        private System.Windows.Forms.Button cmdTable_3;
        private System.Windows.Forms.Button cmdTable_4;
        private System.Windows.Forms.Button cmdTable_1;
        private System.Windows.Forms.Button cmdTable_5;
        private System.Windows.Forms.Button cmdTable_6;
        private System.Windows.Forms.Button cmdZaiko_0;
        private System.Windows.Forms.Button cmdSonota_0;
        private System.Windows.Forms.Button cmdSonota_1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}