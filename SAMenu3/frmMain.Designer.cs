﻿namespace SAMenu3
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode421 = new System.Windows.Forms.TreeNode("見積書入力");
            System.Windows.Forms.TreeNode treeNode422 = new System.Windows.Forms.TreeNode("見積書発行");
            System.Windows.Forms.TreeNode treeNode423 = new System.Windows.Forms.TreeNode("見積処理", new System.Windows.Forms.TreeNode[] {
            treeNode421,
            treeNode422});
            System.Windows.Forms.TreeNode treeNode424 = new System.Windows.Forms.TreeNode("見積データ確定");
            System.Windows.Forms.TreeNode treeNode425 = new System.Windows.Forms.TreeNode("見積データ確定");
            System.Windows.Forms.TreeNode treeNode426 = new System.Windows.Forms.TreeNode("契約台帳");
            System.Windows.Forms.TreeNode treeNode427 = new System.Windows.Forms.TreeNode("注文請書");
            System.Windows.Forms.TreeNode treeNode428 = new System.Windows.Forms.TreeNode("発注入力");
            System.Windows.Forms.TreeNode treeNode429 = new System.Windows.Forms.TreeNode("仕入入力");
            System.Windows.Forms.TreeNode treeNode430 = new System.Windows.Forms.TreeNode("仕入先別実績一覧表");
            System.Windows.Forms.TreeNode treeNode431 = new System.Windows.Forms.TreeNode("受注処理", new System.Windows.Forms.TreeNode[] {
            treeNode424,
            treeNode425,
            treeNode426,
            treeNode427,
            treeNode428,
            treeNode429,
            treeNode430});
            System.Windows.Forms.TreeNode treeNode432 = new System.Windows.Forms.TreeNode("入荷完了入力");
            System.Windows.Forms.TreeNode treeNode433 = new System.Windows.Forms.TreeNode("納品書/受領書");
            System.Windows.Forms.TreeNode treeNode434 = new System.Windows.Forms.TreeNode("ラベル印刷");
            System.Windows.Forms.TreeNode treeNode435 = new System.Windows.Forms.TreeNode("出荷処理");
            System.Windows.Forms.TreeNode treeNode436 = new System.Windows.Forms.TreeNode("梱包入力");
            System.Windows.Forms.TreeNode treeNode437 = new System.Windows.Forms.TreeNode("出荷台帳印刷");
            System.Windows.Forms.TreeNode treeNode438 = new System.Windows.Forms.TreeNode("出荷台帳問合せ");
            System.Windows.Forms.TreeNode treeNode439 = new System.Windows.Forms.TreeNode("ｶｽﾀﾑｲﾝﾎﾞｲｽ");
            System.Windows.Forms.TreeNode treeNode440 = new System.Windows.Forms.TreeNode("COMMERCIAL INVOICE");
            System.Windows.Forms.TreeNode treeNode441 = new System.Windows.Forms.TreeNode("売上実績表");
            System.Windows.Forms.TreeNode treeNode442 = new System.Windows.Forms.TreeNode("仮仕入リスト");
            System.Windows.Forms.TreeNode treeNode443 = new System.Windows.Forms.TreeNode("在庫リスト");
            System.Windows.Forms.TreeNode treeNode444 = new System.Windows.Forms.TreeNode("入荷処理", new System.Windows.Forms.TreeNode[] {
            treeNode432,
            treeNode433,
            treeNode434,
            treeNode435,
            treeNode436,
            treeNode437,
            treeNode438,
            treeNode439,
            treeNode440,
            treeNode441,
            treeNode442,
            treeNode443});
            System.Windows.Forms.TreeNode treeNode445 = new System.Windows.Forms.TreeNode("請求書発行準備");
            System.Windows.Forms.TreeNode treeNode446 = new System.Windows.Forms.TreeNode("請求ﾃﾞｰﾀﾒﾝﾃﾅﾝｽ");
            System.Windows.Forms.TreeNode treeNode447 = new System.Windows.Forms.TreeNode("請求ﾃﾞｰﾀﾒﾝﾃﾅﾝｽ");
            System.Windows.Forms.TreeNode treeNode448 = new System.Windows.Forms.TreeNode("請求一覧表");
            System.Windows.Forms.TreeNode treeNode449 = new System.Windows.Forms.TreeNode("請求取消し");
            System.Windows.Forms.TreeNode treeNode450 = new System.Windows.Forms.TreeNode("請求処理", new System.Windows.Forms.TreeNode[] {
            treeNode445,
            treeNode446,
            treeNode447,
            treeNode448,
            treeNode449});
            System.Windows.Forms.TreeNode treeNode451 = new System.Windows.Forms.TreeNode("STATUS OF ORDER");
            System.Windows.Forms.TreeNode treeNode452 = new System.Windows.Forms.TreeNode("オーダー状況問合せ");
            System.Windows.Forms.TreeNode treeNode453 = new System.Windows.Forms.TreeNode("CHECK OPEN BOX");
            System.Windows.Forms.TreeNode treeNode454 = new System.Windows.Forms.TreeNode("データ問合せ(詳細)");
            System.Windows.Forms.TreeNode treeNode455 = new System.Windows.Forms.TreeNode("データ問合せ(ｵｰﾀﾞｰ)");
            System.Windows.Forms.TreeNode treeNode456 = new System.Windows.Forms.TreeNode("入荷予定日遅延問合せ");
            System.Windows.Forms.TreeNode treeNode457 = new System.Windows.Forms.TreeNode("売上ﾃﾞｰﾀTXT出力");
            System.Windows.Forms.TreeNode treeNode458 = new System.Windows.Forms.TreeNode("仕入ﾃﾞｰﾀTXT出力");
            System.Windows.Forms.TreeNode treeNode459 = new System.Windows.Forms.TreeNode("倉庫リスト");
            System.Windows.Forms.TreeNode treeNode460 = new System.Windows.Forms.TreeNode("随時処理", new System.Windows.Forms.TreeNode[] {
            treeNode451,
            treeNode452,
            treeNode453,
            treeNode454,
            treeNode455,
            treeNode456,
            treeNode457,
            treeNode458,
            treeNode459});
            System.Windows.Forms.TreeNode treeNode461 = new System.Windows.Forms.TreeNode("入金予定一覧表");
            System.Windows.Forms.TreeNode treeNode462 = new System.Windows.Forms.TreeNode("入金依頼書");
            System.Windows.Forms.TreeNode treeNode463 = new System.Windows.Forms.TreeNode("入金入力");
            System.Windows.Forms.TreeNode treeNode464 = new System.Windows.Forms.TreeNode("入金結果リスト");
            System.Windows.Forms.TreeNode treeNode465 = new System.Windows.Forms.TreeNode("入金遅延一覧表");
            System.Windows.Forms.TreeNode treeNode466 = new System.Windows.Forms.TreeNode("入金状況問合せ");
            System.Windows.Forms.TreeNode treeNode467 = new System.Windows.Forms.TreeNode("入金処理", new System.Windows.Forms.TreeNode[] {
            treeNode461,
            treeNode462,
            treeNode463,
            treeNode464,
            treeNode465,
            treeNode466});
            System.Windows.Forms.TreeNode treeNode468 = new System.Windows.Forms.TreeNode("ｺﾝﾄﾛｰﾙﾏｽﾀﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode469 = new System.Windows.Forms.TreeNode("得意先ﾏｽﾀﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode470 = new System.Windows.Forms.TreeNode("得意先ﾏｽﾀﾘｽﾄ");
            System.Windows.Forms.TreeNode treeNode471 = new System.Windows.Forms.TreeNode("仕入先ﾏｽﾀﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode472 = new System.Windows.Forms.TreeNode("仕入先ﾏｽﾀﾘｽﾄ");
            System.Windows.Forms.TreeNode treeNode473 = new System.Windows.Forms.TreeNode("銀行ﾏｽﾀﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode474 = new System.Windows.Forms.TreeNode("区分名称ﾏｽﾀﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode475 = new System.Windows.Forms.TreeNode("各種ﾏｽﾀﾒﾝﾃ", new System.Windows.Forms.TreeNode[] {
            treeNode468,
            treeNode469,
            treeNode470,
            treeNode471,
            treeNode472,
            treeNode473,
            treeNode474});
            System.Windows.Forms.TreeNode treeNode476 = new System.Windows.Forms.TreeNode("船名ﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode477 = new System.Windows.Forms.TreeNode("入荷場所ﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode478 = new System.Windows.Forms.TreeNode("発送先ﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode479 = new System.Windows.Forms.TreeNode("UNITﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode480 = new System.Windows.Forms.TreeNode("MAKERﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode481 = new System.Windows.Forms.TreeNode("TYPEﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode482 = new System.Windows.Forms.TreeNode("品名ﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode483 = new System.Windows.Forms.TreeNode("ｵｰﾅｰﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode484 = new System.Windows.Forms.TreeNode("請求住所ﾃｰﾌﾞﾙﾒﾝﾃ");
            System.Windows.Forms.TreeNode treeNode485 = new System.Windows.Forms.TreeNode("各種ﾃｰﾌﾞﾙﾒﾝﾃ", new System.Windows.Forms.TreeNode[] {
            treeNode476,
            treeNode477,
            treeNode478,
            treeNode479,
            treeNode480,
            treeNode481,
            treeNode482,
            treeNode483,
            treeNode484});
            System.Windows.Forms.TreeNode treeNode486 = new System.Windows.Forms.TreeNode("在庫メニュー");
            System.Windows.Forms.TreeNode treeNode487 = new System.Windows.Forms.TreeNode("在庫処理", new System.Windows.Forms.TreeNode[] {
            treeNode486});
            System.Windows.Forms.TreeNode treeNode488 = new System.Windows.Forms.TreeNode("ﾊﾞｯｸｱｯﾌﾟ処理");
            System.Windows.Forms.TreeNode treeNode489 = new System.Windows.Forms.TreeNode("ﾒﾆｭｰの終了");
            System.Windows.Forms.TreeNode treeNode490 = new System.Windows.Forms.TreeNode("その他", new System.Windows.Forms.TreeNode[] {
            treeNode488,
            treeNode489});
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.見積処理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.受注処理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.入荷処理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.請求処理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.随時処理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.入金処理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.各種ﾏｽﾀﾒﾝﾃToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.各種ﾃｰﾌﾞﾙﾒﾝﾃToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.在庫処理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.その他ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tvMenu = new System.Windows.Forms.TreeView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1508, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.見積処理ToolStripMenuItem,
            this.受注処理ToolStripMenuItem,
            this.入荷処理ToolStripMenuItem,
            this.請求処理ToolStripMenuItem,
            this.随時処理ToolStripMenuItem,
            this.入金処理ToolStripMenuItem,
            this.各種ﾏｽﾀﾒﾝﾃToolStripMenuItem,
            this.各種ﾃｰﾌﾞﾙﾒﾝﾃToolStripMenuItem,
            this.在庫処理ToolStripMenuItem,
            this.その他ToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // 見積処理ToolStripMenuItem
            // 
            this.見積処理ToolStripMenuItem.Name = "見積処理ToolStripMenuItem";
            this.見積処理ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.見積処理ToolStripMenuItem.Text = "見積処理";
            this.見積処理ToolStripMenuItem.Click += new System.EventHandler(this.見積処理ToolStripMenuItem_Click);
            // 
            // 受注処理ToolStripMenuItem
            // 
            this.受注処理ToolStripMenuItem.Name = "受注処理ToolStripMenuItem";
            this.受注処理ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.受注処理ToolStripMenuItem.Text = "受注処理";
            // 
            // 入荷処理ToolStripMenuItem
            // 
            this.入荷処理ToolStripMenuItem.Name = "入荷処理ToolStripMenuItem";
            this.入荷処理ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.入荷処理ToolStripMenuItem.Text = "入荷処理";
            // 
            // 請求処理ToolStripMenuItem
            // 
            this.請求処理ToolStripMenuItem.Name = "請求処理ToolStripMenuItem";
            this.請求処理ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.請求処理ToolStripMenuItem.Text = "請求処理";
            // 
            // 随時処理ToolStripMenuItem
            // 
            this.随時処理ToolStripMenuItem.Name = "随時処理ToolStripMenuItem";
            this.随時処理ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.随時処理ToolStripMenuItem.Text = "随時処理";
            // 
            // 入金処理ToolStripMenuItem
            // 
            this.入金処理ToolStripMenuItem.Name = "入金処理ToolStripMenuItem";
            this.入金処理ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.入金処理ToolStripMenuItem.Text = "入金処理";
            // 
            // 各種ﾏｽﾀﾒﾝﾃToolStripMenuItem
            // 
            this.各種ﾏｽﾀﾒﾝﾃToolStripMenuItem.Name = "各種ﾏｽﾀﾒﾝﾃToolStripMenuItem";
            this.各種ﾏｽﾀﾒﾝﾃToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.各種ﾏｽﾀﾒﾝﾃToolStripMenuItem.Text = "各種ﾏｽﾀﾒﾝﾃ";
            // 
            // 各種ﾃｰﾌﾞﾙﾒﾝﾃToolStripMenuItem
            // 
            this.各種ﾃｰﾌﾞﾙﾒﾝﾃToolStripMenuItem.Name = "各種ﾃｰﾌﾞﾙﾒﾝﾃToolStripMenuItem";
            this.各種ﾃｰﾌﾞﾙﾒﾝﾃToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.各種ﾃｰﾌﾞﾙﾒﾝﾃToolStripMenuItem.Text = "各種ﾃｰﾌﾞﾙﾒﾝﾃ";
            // 
            // 在庫処理ToolStripMenuItem
            // 
            this.在庫処理ToolStripMenuItem.Name = "在庫処理ToolStripMenuItem";
            this.在庫処理ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.在庫処理ToolStripMenuItem.Text = "在庫処理";
            // 
            // その他ToolStripMenuItem
            // 
            this.その他ToolStripMenuItem.Name = "その他ToolStripMenuItem";
            this.その他ToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.その他ToolStripMenuItem.Text = "その他";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 726);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1508, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.splitter1.Location = new System.Drawing.Point(0, 24);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(10, 702);
            this.splitter1.TabIndex = 4;
            this.splitter1.TabStop = false;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(10, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tvMenu);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackgroundImage = global::SAMenu3.Properties.Resources.Untitled_2;
            this.splitContainer1.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.splitContainer1.Size = new System.Drawing.Size(1498, 702);
            this.splitContainer1.SplitterDistance = 191;
            this.splitContainer1.TabIndex = 5;
            // 
            // tvMenu
            // 
            this.tvMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvMenu.Font = new System.Drawing.Font("Modern No. 20", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tvMenu.Location = new System.Drawing.Point(0, 0);
            this.tvMenu.Name = "tvMenu";
            treeNode421.Name = "Node10";
            treeNode421.Text = "見積書入力";
            treeNode422.Name = "Node12";
            treeNode422.Text = "見積書発行";
            treeNode423.Name = "Node0";
            treeNode423.Text = "見積処理";
            treeNode424.Name = "Node13";
            treeNode424.Text = "見積データ確定";
            treeNode425.Name = "Node15";
            treeNode425.Text = "見積データ確定";
            treeNode426.Name = "Node16";
            treeNode426.Text = "契約台帳";
            treeNode427.Name = "Node17";
            treeNode427.Text = "注文請書";
            treeNode428.Name = "Node18";
            treeNode428.Text = "発注入力";
            treeNode429.Name = "Node19";
            treeNode429.Text = "仕入入力";
            treeNode430.Name = "Node20";
            treeNode430.Text = "仕入先別実績一覧表";
            treeNode431.Name = "Node1";
            treeNode431.Text = "受注処理";
            treeNode432.Name = "Node21";
            treeNode432.Text = "入荷完了入力";
            treeNode433.Name = "Node22";
            treeNode433.Text = "納品書/受領書";
            treeNode434.Name = "Node23";
            treeNode434.Text = "ラベル印刷";
            treeNode435.Name = "Node24";
            treeNode435.Text = "出荷処理";
            treeNode436.Name = "Node25";
            treeNode436.Text = "梱包入力";
            treeNode437.Name = "Node26";
            treeNode437.Text = "出荷台帳印刷";
            treeNode438.Name = "Node27";
            treeNode438.Text = "出荷台帳問合せ";
            treeNode439.Name = "Node28";
            treeNode439.Text = "ｶｽﾀﾑｲﾝﾎﾞｲｽ";
            treeNode440.Name = "Node29";
            treeNode440.Text = "COMMERCIAL INVOICE";
            treeNode441.Name = "Node30";
            treeNode441.Text = "売上実績表";
            treeNode442.Name = "Node31";
            treeNode442.Text = "仮仕入リスト";
            treeNode443.Name = "Node32";
            treeNode443.Text = "在庫リスト";
            treeNode444.Name = "Node2";
            treeNode444.Text = "入荷処理";
            treeNode445.Name = "Node34";
            treeNode445.Text = "請求書発行準備";
            treeNode446.Name = "Node35";
            treeNode446.Text = "請求ﾃﾞｰﾀﾒﾝﾃﾅﾝｽ";
            treeNode447.Name = "Node36";
            treeNode447.Text = "請求ﾃﾞｰﾀﾒﾝﾃﾅﾝｽ";
            treeNode448.Name = "Node37";
            treeNode448.Text = "請求一覧表";
            treeNode449.Name = "Node38";
            treeNode449.Text = "請求取消し";
            treeNode450.Name = "Node3";
            treeNode450.Text = "請求処理";
            treeNode451.Name = "Node39";
            treeNode451.Text = "STATUS OF ORDER";
            treeNode452.Name = "Node40";
            treeNode452.Text = "オーダー状況問合せ";
            treeNode453.Name = "Node41";
            treeNode453.Text = "CHECK OPEN BOX";
            treeNode454.Name = "Node42";
            treeNode454.Text = "データ問合せ(詳細)";
            treeNode455.Name = "Node43";
            treeNode455.Text = "データ問合せ(ｵｰﾀﾞｰ)";
            treeNode456.Name = "Node44";
            treeNode456.Text = "入荷予定日遅延問合せ";
            treeNode457.Name = "Node45";
            treeNode457.Text = "売上ﾃﾞｰﾀTXT出力";
            treeNode458.Name = "Node46";
            treeNode458.Text = "仕入ﾃﾞｰﾀTXT出力";
            treeNode459.Name = "Node47";
            treeNode459.Text = "倉庫リスト";
            treeNode460.Name = "Node4";
            treeNode460.Text = "随時処理";
            treeNode461.Name = "Node48";
            treeNode461.Text = "入金予定一覧表";
            treeNode462.Name = "Node49";
            treeNode462.Text = "入金依頼書";
            treeNode463.Name = "Node50";
            treeNode463.Text = "入金入力";
            treeNode464.Name = "Node51";
            treeNode464.Text = "入金結果リスト";
            treeNode465.Name = "Node52";
            treeNode465.Text = "入金遅延一覧表";
            treeNode466.Name = "Node53";
            treeNode466.Text = "入金状況問合せ";
            treeNode467.Name = "Node5";
            treeNode467.Text = "入金処理";
            treeNode468.Name = "Node54";
            treeNode468.Text = "ｺﾝﾄﾛｰﾙﾏｽﾀﾒﾝﾃ";
            treeNode469.Name = "Node55";
            treeNode469.Text = "得意先ﾏｽﾀﾒﾝﾃ";
            treeNode470.Name = "Node56";
            treeNode470.Text = "得意先ﾏｽﾀﾘｽﾄ";
            treeNode471.Name = "Node57";
            treeNode471.Text = "仕入先ﾏｽﾀﾒﾝﾃ";
            treeNode472.Name = "Node58";
            treeNode472.Text = "仕入先ﾏｽﾀﾘｽﾄ";
            treeNode473.Name = "Node59";
            treeNode473.Text = "銀行ﾏｽﾀﾒﾝﾃ";
            treeNode474.Name = "Node60";
            treeNode474.Text = "区分名称ﾏｽﾀﾒﾝﾃ";
            treeNode475.Name = "Node6";
            treeNode475.Text = "各種ﾏｽﾀﾒﾝﾃ";
            treeNode476.Name = "Node61";
            treeNode476.Text = "船名ﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode477.Name = "Node62";
            treeNode477.Text = "入荷場所ﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode478.Name = "Node63";
            treeNode478.Text = "発送先ﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode479.Name = "Node64";
            treeNode479.Text = "UNITﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode480.Name = "Node65";
            treeNode480.Text = "MAKERﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode481.Name = "Node66";
            treeNode481.Text = "TYPEﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode482.Name = "Node67";
            treeNode482.Text = "品名ﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode483.Name = "Node68";
            treeNode483.Text = "ｵｰﾅｰﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode484.Name = "Node69";
            treeNode484.Text = "請求住所ﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode485.Name = "Node7";
            treeNode485.Text = "各種ﾃｰﾌﾞﾙﾒﾝﾃ";
            treeNode486.Name = "Node70";
            treeNode486.Text = "在庫メニュー";
            treeNode487.Name = "Node8";
            treeNode487.Text = "在庫処理";
            treeNode488.Name = "Node71";
            treeNode488.Text = "ﾊﾞｯｸｱｯﾌﾟ処理";
            treeNode489.Name = "Node72";
            treeNode489.Text = "ﾒﾆｭｰの終了";
            treeNode490.Name = "Node9";
            treeNode490.Text = "その他";
            this.tvMenu.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode423,
            treeNode431,
            treeNode444,
            treeNode450,
            treeNode460,
            treeNode467,
            treeNode475,
            treeNode485,
            treeNode487,
            treeNode490});
            this.tvMenu.Size = new System.Drawing.Size(191, 702);
            this.tvMenu.TabIndex = 2;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SAMenu3.Properties.Resources.Untitled_2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1508, 748);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMaincs";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 見積処理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 受注処理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 入荷処理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 請求処理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 随時処理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 入金処理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 各種ﾏｽﾀﾒﾝﾃToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 各種ﾃｰﾌﾞﾙﾒﾝﾃToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 在庫処理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem その他ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tvMenu;
    }
}