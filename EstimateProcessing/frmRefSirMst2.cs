﻿using clsDB;
using clswinApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBlibrary;
using static VBlibrary.modHanbai;

namespace EstimateProcessing
{
    public partial class frmRefSirMst2 : Form
    {
        string Sir_Mst_Code;
        string Sir_Mst_Name1;
        string Sir_Mst_Name2;
        string Sir_Mst_Kana ;
        Boolean Sir_Mst_Status;
        SirMstInfo sirMst;

        public frmRefSirMst2()
        {
            InitializeComponent();
            spdList.DoubleClick += new EventHandler(cmdFunc_12_Click);
            numSerch.KeyUp += new KeyEventHandler(searchevent);
            txtSerch_0.KeyUp += new KeyEventHandler(searchevent);
            txtSerch_1.KeyUp += new KeyEventHandler(searchevent);
            txtSerch_2.KeyUp += new KeyEventHandler(searchevent);
            spdList.KeyDown += new KeyEventHandler(gridkeyup);
        }

        private void gridkeyup(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        cmdFunc_12_Click(sender, null);
                        break;
                    }
                case Keys.Back:
                    {
                        numSerch.Focus();
                        break;
                    }
            }
        }

        private void searchevent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                spdList.Focus();
                return;
            }
            SetTblInfo();
        }


        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void frmRefSirMst2_Load(object sender, EventArgs e)
        {
            EditClear();
            SetTblInfo();
            cmdFunc_10.Text = "(F10)" + Environment.NewLine + "ENTER";
            cmdFunc_11.Text = "(F11)" + Environment.NewLine + "BACK";
            cmdFunc_12.Text = "(F12)" + Environment.NewLine + "SET";
            numSerch.Focus();
            Application.DoEvents();
        }

        private void cmdFunc_10_Click(object sender, EventArgs e)
        {
            if (DatainsertProc())
            {
                EditClear();
                SetTblInfo();
            }
        }

        private void cmdFunc_11_Click(object sender, EventArgs e)
        {
            modSac_Com.RetMode = 0;
            this.Close();
        }

        private void cmdFunc_12_Click(object sender, EventArgs e)
        {
            
                modSac_Com.RetMode = 1;
                int selectedrowindex = spdList.SelectedCells[0].RowIndex;
            setActiveData(selectedrowindex);
            Sir_Mst_Status = true;
                this.Close();
            }
        private void EditClear()
        {

            Sir_Mst_Code = "";
            Sir_Mst_Name1 = "";
            Sir_Mst_Name2 = "";
            Sir_Mst_Kana = "";
            Sir_Mst_Status = false;

            txtSerch_0.Clear();
            txtSerch_1.Clear();
            txtSerch_2.Clear();
        }

        private void setActiveData(int row)
        {
            Sir_Mst_Code = spdList.Rows[row].Cells[0].Value.ToString();
            Sir_Mst_Name1 = spdList.Rows[row].Cells[1].Value.ToString();
            Sir_Mst_Name2 = spdList.Rows[row].Cells[2].Value.ToString();
            Sir_Mst_Kana = spdList.Rows[row].Cells[3].Value.ToString();

        }


        private void SetTblInfo()
        {

            string SQLtxt = "SELECT Sir_001,Sir_003,Sir_004,Sir_006 FROM Sir_Mst";

            if (txtSerch_0.Text.Trim() != "")
                SQLtxt += String.Format(" Where Sir_003 like '%{0}%'", txtSerch_0.Text.Trim());

            if (txtSerch_1.Text.Trim() != "")
            {


                if (!SQLtxt.ToUpper().Contains("WHERE"))
                    SQLtxt += String.Format(" Where Sir_004 like '%{0}%'", txtSerch_1.Text.Trim());
                else
                    SQLtxt += String.Format(" And Sir_004 like '%{0}%'", txtSerch_1.Text.Trim());
            }
            if (txtSerch_2.Text.Trim() != "")
            {
                if (!SQLtxt.ToUpper().Contains("WHERE"))
                    SQLtxt += String.Format(" Where Sir_006 like '%{0}%'", txtSerch_2.Text.Trim());
                else
                    SQLtxt += String.Format(" And Sir_006 like '%{0}%'", txtSerch_2.Text.Trim());
            }
            if (numSerch.Text.Trim() != "")
            {
                if (!SQLtxt.ToUpper().Contains("WHERE"))
                    SQLtxt += String.Format(" Where Sir_001 >=  {0}", numSerch.Text.Trim());
                else
                    SQLtxt += String.Format(" And Sir_001 >=  {0}", numSerch.Text.Trim());
            }


            SQLtxt += " ORDER BY Sir_001,Sir_006,Sir_003,Sir_004";


            DBService db = new DBService();
            DataTable dt = new DataTable();
            dt = db.GetDatafromDB(SQLtxt);
            spdList.DataSource = dt;
        }
        private Boolean DatainsertProc()
        {

            

            string SQLtxt = " SELECT * FROM Sir_Mst ";
            SQLtxt = SQLtxt + "WHERE Sir_003 ='" + modCommon.EditSQLAddSQuot(modHanbai.NCnvN(txtSerch_0.Text)) + "'"; // •i–¼
            SQLtxt = SQLtxt + "  AND Sir_004 ='" + modCommon.EditSQLAddSQuot(modHanbai.NCnvN(txtSerch_1.Text)) + "'"; // •i”Ô
            DBService db = new DBService();
            DataTable dt = new DataTable();
            dt = db.GetDatafromDB(SQLtxt);
            spdList.DataSource = dt;
            if(dt.Rows.Count>=1)
            {
                return false;
            }


            if (!(numSerch.Text.Trim() == "" && txtSerch_0.Text.Trim() == "" && txtSerch_1.Text.Trim() == "" && txtSerch_2.Text.Trim() == ""))
            {
                if (SirInsertProc())
                {
                    SetTblInfo();
                    return true;
                }

            }
            return false;
        }
        private bool SirInsertProc()
        {
            SirMstInfo sirMst = new SirMstInfo();

            sirMst.Sir_001 = getNewSirCode();
            sirMst.Sir_002 = 0;
            sirMst.Sir_003 = modHanbai.NCnvN(txtSerch_0.Text.Trim());
            sirMst.Sir_004 = modHanbai.NCnvN(txtSerch_1.Text.Trim());
            sirMst.Sir_006 = modHanbai.NCnvN(txtSerch_2.Text.Trim());

            string strSQL = "";
            strSQL = strSQL + "INSERT INTO Sir_Mst(";
            strSQL = strSQL + "Sir_001,";
            strSQL = strSQL + "Sir_002,";
            strSQL = strSQL + "Sir_003,";
            strSQL = strSQL + "Sir_004,";
            strSQL = strSQL + "Sir_006,";
            strSQL = strSQL + "Sir_Insert,";
            strSQL = strSQL + "Uni_WsNo";
            strSQL = strSQL + ") VALUES (";
            strSQL = strSQL + sirMst.Sir_001 + ",";
            strSQL = strSQL + sirMst.Sir_002 + ",'";
            strSQL = strSQL + modCommon.EditSQLAddSQuot(sirMst.Sir_003) + "','";
            strSQL = strSQL + modCommon.EditSQLAddSQuot(sirMst.Sir_004) + "','";
            strSQL = strSQL + modCommon.EditSQLAddSQuot(sirMst.Sir_006) + "','";
            strSQL = strSQL + DateTime.Today.ToShortDateString() + "','";
            strSQL = strSQL + StaticWinApi.Wsnumber + "')";
            DBService db = new DBService();
            string errormsg;

            db.ExecuteSql(strSQL, out errormsg);
            if (errormsg != "")
            {
                MessageBox.Show(@"Error has been encountered: 
                    " + errormsg);
                return false;
            }
            else
            {
                return true;
            }
        }

        private long getNewSirCode()
        {
            int retint;

            string SQLtxt = "";
            SQLtxt += "SELECT Sir_001 As LastCode FROM Sir_Mst";
            SQLtxt += " ORDER BY Sir_001 DESC";

            DBService db = new DBService();
            DataTable dt = new DataTable();
            dt = db.GetDatafromDB(SQLtxt);
            spdList.DataSource = dt;

            if(dt.Rows.Count>=1)
            {
                return Convert.ToInt64(dt.Rows[0][0].ToString()) + 1;
            }
            else
            {
                throw new Exception("Error while getting new Code from Sir_Mst");
            }

        }

        public bool Status()
        {
            return Sir_Mst_Status;
        }

        public string STM_CODE()
        {
            return Sir_Mst_Code;
        }
        public string STM_NAME1()
        {
           return Sir_Mst_Name1;
        }
        public string STM_NAME2()
        {
            return Sir_Mst_Name2;
        }
        public string STM_KANA()
        {
            return Sir_Mst_Kana;
        }

        private void numSerch_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }




        private void numSerch_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
