﻿namespace EstimateProcessing
{
    partial class saFrm_Sansyo901
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmdFunc_9 = new System.Windows.Forms.Button();
            this.cmdFunc_10 = new System.Windows.Forms.Button();
            this.cmdFunc_11 = new System.Windows.Forms.Button();
            this.cmdFunc_12 = new System.Windows.Forms.Button();
            this.lbl_001 = new System.Windows.Forms.Label();
            this.lbl_002 = new System.Windows.Forms.Label();
            this.lbl_003 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_004 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.spdTokMstInfo = new System.Windows.Forms.DataGridView();
            this.Tok_001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tok_003 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tok_004 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tok_006 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tok_051 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTokSerch2 = new System.Windows.Forms.TextBox();
            this.txtTokSerch0 = new System.Windows.Forms.TextBox();
            this.txtTokSerch1 = new System.Windows.Forms.TextBox();
            this.optCountry_1 = new System.Windows.Forms.RadioButton();
            this.optCountry_0 = new System.Windows.Forms.RadioButton();
            this.txtDoc = new System.Windows.Forms.TextBox();
            this.txtPerson = new System.Windows.Forms.TextBox();
            this.txtShipNo = new System.Windows.Forms.TextBox();
            this.txtTName = new System.Windows.Forms.TextBox();
            this.txtTTanto = new System.Windows.Forms.TextBox();
            this.txtVessel = new System.Windows.Forms.TextBox();
            this.txtOwner = new System.Windows.Forms.TextBox();
            this.numTCode = new System.Windows.Forms.TextBox();
            this.numOCode = new System.Windows.Forms.TextBox();
            this.frame4 = new System.Windows.Forms.GroupBox();
            this.spdVesTblInfo = new System.Windows.Forms.DataGridView();
            this.Ves_002 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ves_004 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ves_005 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtSVessel = new System.Windows.Forms.TextBox();
            this.optVessel_1 = new System.Windows.Forms.RadioButton();
            this.optVessel_0 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.spdTokTMstInfo = new System.Windows.Forms.DataGridView();
            this.Attn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.spdOwnTblInfo = new System.Windows.Forms.DataGridView();
            this.Own_001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Own_002 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdTokMstInfo)).BeginInit();
            this.frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdVesTblInfo)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdTokTMstInfo)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdOwnTblInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("MS Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "得意先マスタ参照";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(272, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "造船所";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(355, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "船番";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(430, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "当社担当者";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label4.Visible = false;
            // 
            // cmdFunc_9
            // 
            this.cmdFunc_9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_9.Location = new System.Drawing.Point(583, 19);
            this.cmdFunc_9.Name = "cmdFunc_9";
            this.cmdFunc_9.Size = new System.Drawing.Size(106, 43);
            this.cmdFunc_9.TabIndex = 10;
            this.cmdFunc_9.Text = "船名ﾃｰﾌﾞﾙﾒﾝﾃ(F9)";
            this.cmdFunc_9.UseVisualStyleBackColor = false;
            // 
            // cmdFunc_10
            // 
            this.cmdFunc_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_10.Location = new System.Drawing.Point(688, 19);
            this.cmdFunc_10.Name = "cmdFunc_10";
            this.cmdFunc_10.Size = new System.Drawing.Size(97, 43);
            this.cmdFunc_10.TabIndex = 11;
            this.cmdFunc_10.Text = "登録(F10)";
            this.cmdFunc_10.UseVisualStyleBackColor = false;
            this.cmdFunc_10.Visible = false;
            // 
            // cmdFunc_11
            // 
            this.cmdFunc_11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_11.Location = new System.Drawing.Point(784, 19);
            this.cmdFunc_11.Name = "cmdFunc_11";
            this.cmdFunc_11.Size = new System.Drawing.Size(97, 43);
            this.cmdFunc_11.TabIndex = 12;
            this.cmdFunc_11.Text = "戻る(F11)";
            this.cmdFunc_11.UseVisualStyleBackColor = false;
            this.cmdFunc_11.Click += new System.EventHandler(this.cmdFunc_11_Click);
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_12.Location = new System.Drawing.Point(880, 19);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(97, 43);
            this.cmdFunc_12.TabIndex = 13;
            this.cmdFunc_12.Text = "確定(F12)";
            this.cmdFunc_12.UseVisualStyleBackColor = false;
            this.cmdFunc_12.Click += new System.EventHandler(this.cmdFunc_12_Click);
            // 
            // lbl_001
            // 
            this.lbl_001.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lbl_001.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_001.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_001.Location = new System.Drawing.Point(12, 70);
            this.lbl_001.Name = "lbl_001";
            this.lbl_001.Size = new System.Drawing.Size(225, 23);
            this.lbl_001.TabIndex = 11;
            this.lbl_001.Text = "Messrs";
            this.lbl_001.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_001.Click += new System.EventHandler(this.label5_Click);
            // 
            // lbl_002
            // 
            this.lbl_002.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lbl_002.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_002.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_002.Location = new System.Drawing.Point(237, 70);
            this.lbl_002.Name = "lbl_002";
            this.lbl_002.Size = new System.Drawing.Size(166, 23);
            this.lbl_002.TabIndex = 12;
            this.lbl_002.Text = "Attn";
            this.lbl_002.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_002.Click += new System.EventHandler(this.label6_Click);
            // 
            // lbl_003
            // 
            this.lbl_003.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lbl_003.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_003.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_003.Location = new System.Drawing.Point(403, 70);
            this.lbl_003.Name = "lbl_003";
            this.lbl_003.Size = new System.Drawing.Size(222, 23);
            this.lbl_003.TabIndex = 13;
            this.lbl_003.Text = "Vessel\'s Name";
            this.lbl_003.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_003.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(830, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 23);
            this.label8.TabIndex = 14;
            this.label8.Text = "得意先ｺｰﾄﾞ";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label8.Visible = false;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // lbl_004
            // 
            this.lbl_004.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lbl_004.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl_004.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_004.Location = new System.Drawing.Point(626, 70);
            this.lbl_004.Name = "lbl_004";
            this.lbl_004.Size = new System.Drawing.Size(203, 23);
            this.lbl_004.TabIndex = 15;
            this.lbl_004.Text = "OWNER NAME";
            this.lbl_004.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_004.Click += new System.EventHandler(this.label9_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(920, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 23);
            this.label5.TabIndex = 16;
            this.label5.Text = "ｵｰﾅｰｺｰﾄﾞ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label5.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.spdTokMstInfo);
            this.groupBox1.Controls.Add(this.txtTokSerch2);
            this.groupBox1.Controls.Add(this.txtTokSerch0);
            this.groupBox1.Controls.Add(this.txtTokSerch1);
            this.groupBox1.Controls.Add(this.optCountry_1);
            this.groupBox1.Controls.Add(this.optCountry_0);
            this.groupBox1.Location = new System.Drawing.Point(17, 129);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(425, 630);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SEARCH CUSTOMER";
            // 
            // spdTokMstInfo
            // 
            this.spdTokMstInfo.AllowUserToAddRows = false;
            this.spdTokMstInfo.AllowUserToDeleteRows = false;
            this.spdTokMstInfo.AllowUserToResizeColumns = false;
            this.spdTokMstInfo.AllowUserToResizeRows = false;
            this.spdTokMstInfo.BackgroundColor = System.Drawing.Color.White;
            this.spdTokMstInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spdTokMstInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tok_001,
            this.Tok_003,
            this.Tok_004,
            this.Tok_006,
            this.Tok_051});
            this.spdTokMstInfo.Location = new System.Drawing.Point(9, 76);
            this.spdTokMstInfo.MultiSelect = false;
            this.spdTokMstInfo.Name = "spdTokMstInfo";
            this.spdTokMstInfo.ReadOnly = true;
            this.spdTokMstInfo.RowHeadersVisible = false;
            this.spdTokMstInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.spdTokMstInfo.Size = new System.Drawing.Size(405, 533);
            this.spdTokMstInfo.TabIndex = 1;
            this.spdTokMstInfo.DoubleClick += new System.EventHandler(this.spdTokMstInfo_DoubleClick);
            this.spdTokMstInfo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.spdTokMstInfo_KeyDown);
            // 
            // Tok_001
            // 
            this.Tok_001.DataPropertyName = "Tok_001";
            this.Tok_001.HeaderText = "Tok_001";
            this.Tok_001.Name = "Tok_001";
            this.Tok_001.ReadOnly = true;
            this.Tok_001.Visible = false;
            // 
            // Tok_003
            // 
            this.Tok_003.DataPropertyName = "Tok_003";
            this.Tok_003.HeaderText = "Messrs";
            this.Tok_003.Name = "Tok_003";
            this.Tok_003.ReadOnly = true;
            this.Tok_003.Width = 500;
            // 
            // Tok_004
            // 
            this.Tok_004.DataPropertyName = "Tok_004";
            this.Tok_004.HeaderText = "Tok_004";
            this.Tok_004.Name = "Tok_004";
            this.Tok_004.ReadOnly = true;
            this.Tok_004.Visible = false;
            // 
            // Tok_006
            // 
            this.Tok_006.DataPropertyName = "Tok_006";
            this.Tok_006.HeaderText = "Tok_006";
            this.Tok_006.Name = "Tok_006";
            this.Tok_006.ReadOnly = true;
            this.Tok_006.Visible = false;
            // 
            // Tok_051
            // 
            this.Tok_051.DataPropertyName = "Tok_051";
            this.Tok_051.HeaderText = "Tok_051";
            this.Tok_051.Name = "Tok_051";
            this.Tok_051.ReadOnly = true;
            this.Tok_051.Visible = false;
            // 
            // txtTokSerch2
            // 
            this.txtTokSerch2.Location = new System.Drawing.Point(244, 50);
            this.txtTokSerch2.Name = "txtTokSerch2";
            this.txtTokSerch2.Size = new System.Drawing.Size(168, 20);
            this.txtTokSerch2.TabIndex = 24;
            this.txtTokSerch2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.on_key);
            // 
            // txtTokSerch0
            // 
            this.txtTokSerch0.Location = new System.Drawing.Point(9, 50);
            this.txtTokSerch0.Name = "txtTokSerch0";
            this.txtTokSerch0.Size = new System.Drawing.Size(229, 20);
            this.txtTokSerch0.TabIndex = 0;
            this.txtTokSerch0.TextChanged += new System.EventHandler(this.txtTokSerch0_TextChanged);
            this.txtTokSerch0.KeyDown += new System.Windows.Forms.KeyEventHandler(this.on_key);
            // 
            // txtTokSerch1
            // 
            this.txtTokSerch1.Location = new System.Drawing.Point(194, 19);
            this.txtTokSerch1.Name = "txtTokSerch1";
            this.txtTokSerch1.Size = new System.Drawing.Size(81, 20);
            this.txtTokSerch1.TabIndex = 23;
            this.txtTokSerch1.TabStop = false;
            this.txtTokSerch1.Visible = false;
            // 
            // optCountry_1
            // 
            this.optCountry_1.AutoSize = true;
            this.optCountry_1.Checked = true;
            this.optCountry_1.Location = new System.Drawing.Point(105, 19);
            this.optCountry_1.Name = "optCountry_1";
            this.optCountry_1.Size = new System.Drawing.Size(83, 17);
            this.optCountry_1.TabIndex = 19;
            this.optCountry_1.TabStop = true;
            this.optCountry_1.Text = "OVERSEAS";
            this.optCountry_1.UseVisualStyleBackColor = true;
            this.optCountry_1.Click += new System.EventHandler(this.on_Click);
            // 
            // optCountry_0
            // 
            this.optCountry_0.AutoSize = true;
            this.optCountry_0.Location = new System.Drawing.Point(23, 19);
            this.optCountry_0.Name = "optCountry_0";
            this.optCountry_0.Size = new System.Drawing.Size(59, 17);
            this.optCountry_0.TabIndex = 18;
            this.optCountry_0.Text = "JAPAN";
            this.optCountry_0.UseVisualStyleBackColor = true;
            this.optCountry_0.Click += new System.EventHandler(this.on_Click);
            // 
            // txtDoc
            // 
            this.txtDoc.Location = new System.Drawing.Point(272, 40);
            this.txtDoc.Name = "txtDoc";
            this.txtDoc.ReadOnly = true;
            this.txtDoc.Size = new System.Drawing.Size(81, 20);
            this.txtDoc.TabIndex = 38;
            this.txtDoc.TabStop = false;
            this.txtDoc.Visible = false;
            this.txtDoc.TextChanged += new System.EventHandler(this.txtDoc_TextChanged);
            // 
            // txtPerson
            // 
            this.txtPerson.Location = new System.Drawing.Point(430, 40);
            this.txtPerson.Name = "txtPerson";
            this.txtPerson.ReadOnly = true;
            this.txtPerson.Size = new System.Drawing.Size(100, 20);
            this.txtPerson.TabIndex = 40;
            this.txtPerson.TabStop = false;
            this.txtPerson.Visible = false;
            // 
            // txtShipNo
            // 
            this.txtShipNo.Location = new System.Drawing.Point(355, 40);
            this.txtShipNo.Name = "txtShipNo";
            this.txtShipNo.ReadOnly = true;
            this.txtShipNo.Size = new System.Drawing.Size(73, 20);
            this.txtShipNo.TabIndex = 39;
            this.txtShipNo.TabStop = false;
            this.txtShipNo.Visible = false;
            // 
            // txtTName
            // 
            this.txtTName.Location = new System.Drawing.Point(11, 96);
            this.txtTName.Name = "txtTName";
            this.txtTName.ReadOnly = true;
            this.txtTName.Size = new System.Drawing.Size(225, 20);
            this.txtTName.TabIndex = 6;
            this.txtTName.TabStop = false;
            // 
            // txtTTanto
            // 
            this.txtTTanto.Location = new System.Drawing.Point(237, 96);
            this.txtTTanto.Name = "txtTTanto";
            this.txtTTanto.ReadOnly = true;
            this.txtTTanto.Size = new System.Drawing.Size(166, 20);
            this.txtTTanto.TabIndex = 7;
            this.txtTTanto.TabStop = false;
            // 
            // txtVessel
            // 
            this.txtVessel.Location = new System.Drawing.Point(404, 96);
            this.txtVessel.Name = "txtVessel";
            this.txtVessel.ReadOnly = true;
            this.txtVessel.Size = new System.Drawing.Size(222, 20);
            this.txtVessel.TabIndex = 8;
            this.txtVessel.TabStop = false;
            // 
            // txtOwner
            // 
            this.txtOwner.Location = new System.Drawing.Point(627, 96);
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.ReadOnly = true;
            this.txtOwner.Size = new System.Drawing.Size(203, 20);
            this.txtOwner.TabIndex = 9;
            this.txtOwner.TabStop = false;
            // 
            // numTCode
            // 
            this.numTCode.Location = new System.Drawing.Point(834, 96);
            this.numTCode.Name = "numTCode";
            this.numTCode.ReadOnly = true;
            this.numTCode.Size = new System.Drawing.Size(81, 20);
            this.numTCode.TabIndex = 30;
            this.numTCode.TabStop = false;
            this.numTCode.Visible = false;
            this.numTCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numOCode_KeyPress);
            // 
            // numOCode
            // 
            this.numOCode.Location = new System.Drawing.Point(919, 96);
            this.numOCode.Name = "numOCode";
            this.numOCode.ReadOnly = true;
            this.numOCode.Size = new System.Drawing.Size(81, 20);
            this.numOCode.TabIndex = 33;
            this.numOCode.TabStop = false;
            this.numOCode.Visible = false;
            this.numOCode.TextChanged += new System.EventHandler(this.numOCode_TextChanged);
            this.numOCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numOCode_KeyPress);
            // 
            // frame4
            // 
            this.frame4.Controls.Add(this.spdVesTblInfo);
            this.frame4.Controls.Add(this.txtSVessel);
            this.frame4.Controls.Add(this.optVessel_1);
            this.frame4.Controls.Add(this.optVessel_0);
            this.frame4.Location = new System.Drawing.Point(458, 129);
            this.frame4.Name = "frame4";
            this.frame4.Size = new System.Drawing.Size(539, 366);
            this.frame4.TabIndex = 33;
            this.frame4.TabStop = false;
            this.frame4.Text = "VESSEL NAME";
            // 
            // spdVesTblInfo
            // 
            this.spdVesTblInfo.AllowUserToAddRows = false;
            this.spdVesTblInfo.AllowUserToDeleteRows = false;
            this.spdVesTblInfo.AllowUserToResizeColumns = false;
            this.spdVesTblInfo.AllowUserToResizeRows = false;
            this.spdVesTblInfo.BackgroundColor = System.Drawing.Color.White;
            this.spdVesTblInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spdVesTblInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Ves_002,
            this.Ves_004,
            this.Ves_005});
            this.spdVesTblInfo.Location = new System.Drawing.Point(15, 50);
            this.spdVesTblInfo.MultiSelect = false;
            this.spdVesTblInfo.Name = "spdVesTblInfo";
            this.spdVesTblInfo.ReadOnly = true;
            this.spdVesTblInfo.RowHeadersVisible = false;
            this.spdVesTblInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.spdVesTblInfo.Size = new System.Drawing.Size(513, 303);
            this.spdVesTblInfo.TabIndex = 3;
            this.spdVesTblInfo.DoubleClick += new System.EventHandler(this.spdVesTblInfo_DoubleClick);
            this.spdVesTblInfo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.spdVesTblInfo_KeyDown);
            // 
            // Ves_002
            // 
            this.Ves_002.DataPropertyName = "Ves_002";
            this.Ves_002.HeaderText = "Vessel Name";
            this.Ves_002.Name = "Ves_002";
            this.Ves_002.ReadOnly = true;
            this.Ves_002.Width = 200;
            // 
            // Ves_004
            // 
            this.Ves_004.DataPropertyName = "Ves_004";
            this.Ves_004.HeaderText = "Builder No";
            this.Ves_004.Name = "Ves_004";
            this.Ves_004.ReadOnly = true;
            this.Ves_004.Width = 150;
            // 
            // Ves_005
            // 
            this.Ves_005.DataPropertyName = "Ves_005";
            this.Ves_005.HeaderText = "Number";
            this.Ves_005.Name = "Ves_005";
            this.Ves_005.ReadOnly = true;
            this.Ves_005.Width = 150;
            // 
            // txtSVessel
            // 
            this.txtSVessel.Location = new System.Drawing.Point(230, 21);
            this.txtSVessel.Name = "txtSVessel";
            this.txtSVessel.Size = new System.Drawing.Size(255, 20);
            this.txtSVessel.TabIndex = 2;
            this.txtSVessel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSVessel_KeyUp);
            // 
            // optVessel_1
            // 
            this.optVessel_1.AutoSize = true;
            this.optVessel_1.Location = new System.Drawing.Point(152, 25);
            this.optVessel_1.Name = "optVessel_1";
            this.optVessel_1.Size = new System.Drawing.Size(44, 17);
            this.optVessel_1.TabIndex = 26;
            this.optVessel_1.Text = "ALL";
            this.optVessel_1.UseVisualStyleBackColor = true;
            // 
            // optVessel_0
            // 
            this.optVessel_0.AutoSize = true;
            this.optVessel_0.Checked = true;
            this.optVessel_0.Location = new System.Drawing.Point(31, 24);
            this.optVessel_0.Name = "optVessel_0";
            this.optVessel_0.Size = new System.Drawing.Size(118, 17);
            this.optVessel_0.TabIndex = 25;
            this.optVessel_0.TabStop = true;
            this.optVessel_0.Text = "EACH CUSTOMER";
            this.optVessel_0.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.spdTokTMstInfo);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox2.Location = new System.Drawing.Point(458, 497);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(249, 262);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Attn";
            // 
            // spdTokTMstInfo
            // 
            this.spdTokTMstInfo.AllowUserToAddRows = false;
            this.spdTokTMstInfo.AllowUserToDeleteRows = false;
            this.spdTokTMstInfo.AllowUserToResizeColumns = false;
            this.spdTokTMstInfo.AllowUserToResizeRows = false;
            this.spdTokTMstInfo.BackgroundColor = System.Drawing.Color.White;
            this.spdTokTMstInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spdTokTMstInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Attn});
            this.spdTokTMstInfo.Location = new System.Drawing.Point(15, 22);
            this.spdTokTMstInfo.MultiSelect = false;
            this.spdTokTMstInfo.Name = "spdTokTMstInfo";
            this.spdTokTMstInfo.ReadOnly = true;
            this.spdTokTMstInfo.RowHeadersVisible = false;
            this.spdTokTMstInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.spdTokTMstInfo.Size = new System.Drawing.Size(219, 230);
            this.spdTokTMstInfo.TabIndex = 4;
            this.spdTokTMstInfo.DoubleClick += new System.EventHandler(this.spdTokTMstInfo_DoubleClick);
            this.spdTokTMstInfo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.spdTokTMstInfo_KeyDown);
            // 
            // Attn
            // 
            this.Attn.HeaderText = "Attn";
            this.Attn.Name = "Attn";
            this.Attn.ReadOnly = true;
            this.Attn.Width = 210;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.spdOwnTblInfo);
            this.groupBox3.Location = new System.Drawing.Point(734, 498);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(252, 262);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "OWNER NAME";
            // 
            // spdOwnTblInfo
            // 
            this.spdOwnTblInfo.AllowUserToAddRows = false;
            this.spdOwnTblInfo.AllowUserToDeleteRows = false;
            this.spdOwnTblInfo.AllowUserToResizeColumns = false;
            this.spdOwnTblInfo.AllowUserToResizeRows = false;
            this.spdOwnTblInfo.BackgroundColor = System.Drawing.Color.White;
            this.spdOwnTblInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spdOwnTblInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Own_001,
            this.Own_002});
            this.spdOwnTblInfo.Location = new System.Drawing.Point(18, 23);
            this.spdOwnTblInfo.MultiSelect = false;
            this.spdOwnTblInfo.Name = "spdOwnTblInfo";
            this.spdOwnTblInfo.ReadOnly = true;
            this.spdOwnTblInfo.RowHeadersVisible = false;
            this.spdOwnTblInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.spdOwnTblInfo.Size = new System.Drawing.Size(214, 229);
            this.spdOwnTblInfo.TabIndex = 5;
            this.spdOwnTblInfo.DoubleClick += new System.EventHandler(this.spdOwnTblInfo_DoubleClick);
            this.spdOwnTblInfo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.spdOwnTblInfo_KeyDown);
            // 
            // Own_001
            // 
            this.Own_001.DataPropertyName = "Own_001";
            this.Own_001.HeaderText = "Own_001";
            this.Own_001.Name = "Own_001";
            this.Own_001.ReadOnly = true;
            this.Own_001.Visible = false;
            // 
            // Own_002
            // 
            this.Own_002.DataPropertyName = "Own_002";
            this.Own_002.HeaderText = "Owner Name";
            this.Own_002.Name = "Own_002";
            this.Own_002.ReadOnly = true;
            this.Own_002.Width = 210;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(19, 772);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(967, 24);
            this.label6.TabIndex = 36;
            this.label6.Text = "ｺｰﾄﾞを入力して下さい。";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // saFrm_Sansyo901
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(1011, 825);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.frame4);
            this.Controls.Add(this.numOCode);
            this.Controls.Add(this.numTCode);
            this.Controls.Add(this.txtOwner);
            this.Controls.Add(this.txtVessel);
            this.Controls.Add(this.txtTTanto);
            this.Controls.Add(this.txtTName);
            this.Controls.Add(this.txtShipNo);
            this.Controls.Add(this.txtPerson);
            this.Controls.Add(this.txtDoc);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbl_004);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbl_003);
            this.Controls.Add(this.lbl_002);
            this.Controls.Add(this.lbl_001);
            this.Controls.Add(this.cmdFunc_12);
            this.Controls.Add(this.cmdFunc_11);
            this.Controls.Add(this.cmdFunc_10);
            this.Controls.Add(this.cmdFunc_9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "saFrm_Sansyo901";
            this.Text = "得意先マスタ参照  (saFrm_Sansyo901)";
            this.Load += new System.EventHandler(this.saFrm_Sansyo901_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdTokMstInfo)).EndInit();
            this.frame4.ResumeLayout(false);
            this.frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdVesTblInfo)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spdTokTMstInfo)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spdOwnTblInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox txtDoc;
        public System.Windows.Forms.TextBox txtPerson;
        public System.Windows.Forms.TextBox txtShipNo;
        public System.Windows.Forms.TextBox txtTName;
        public System.Windows.Forms.TextBox txtTTanto;
        public System.Windows.Forms.TextBox txtVessel;
        public System.Windows.Forms.TextBox txtOwner;
        public System.Windows.Forms.TextBox numTCode;
        public System.Windows.Forms.TextBox numOCode;
        public System.Windows.Forms.TextBox txtTokSerch2;
        public System.Windows.Forms.TextBox txtTokSerch0;
        public System.Windows.Forms.TextBox txtTokSerch1;
        public System.Windows.Forms.TextBox txtSVessel;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button cmdFunc_9;
        public System.Windows.Forms.Button cmdFunc_10;
        public System.Windows.Forms.Button cmdFunc_11;
        public System.Windows.Forms.Button cmdFunc_12;
        public System.Windows.Forms.Label lbl_001;
        public System.Windows.Forms.Label lbl_002;
        public System.Windows.Forms.Label lbl_003;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label lbl_004;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.RadioButton optCountry_1;
        public System.Windows.Forms.RadioButton optCountry_0;
        public System.Windows.Forms.GroupBox frame4;
        public System.Windows.Forms.RadioButton optVessel_1;
        public System.Windows.Forms.RadioButton optVessel_0;
        public System.Windows.Forms.DataGridView spdTokMstInfo;
        public System.Windows.Forms.DataGridView spdVesTblInfo;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.DataGridView spdTokTMstInfo;
        public System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.DataGridView spdOwnTblInfo;
        public System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tok_001;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tok_003;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tok_004;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tok_006;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tok_051;
        private System.Windows.Forms.DataGridViewTextBoxColumn Attn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Own_001;
        private System.Windows.Forms.DataGridViewTextBoxColumn Own_002;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ves_002;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ves_004;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ves_005;
    }
}