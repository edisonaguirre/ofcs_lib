﻿namespace EstimateProcessing
{
    partial class frmSiireTanka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.numTanka = new System.Windows.Forms.TextBox();
            this.txtHinban = new System.Windows.Forms.TextBox();
            this.txtHinmei = new System.Windows.Forms.TextBox();
            this.txtShiireNm = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.frame2 = new System.Windows.Forms.GroupBox();
            this.pictnumSTankaureBox4 = new System.Windows.Forms.TextBox();
            this.datSDate = new System.Windows.Forms.DateTimePicker();
            this.txtSName = new System.Windows.Forms.TextBox();
            this.numSCode = new System.Windows.Forms.TextBox();
            this.cmdFunc_10 = new System.Windows.Forms.Button();
            this.cmdFunc_12 = new System.Windows.Forms.Button();
            this.txtMitumoriNo = new System.Windows.Forms.TextBox();
            this.numSEQ = new System.Windows.Forms.TextBox();
            this.numItem = new System.Windows.Forms.TextBox();
            this.Frame1.SuspendLayout();
            this.frame2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("MS Mincho", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-3, -4);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 47);
            this.label1.TabIndex = 0;
            this.label1.Text = "仕入先見積単価入力";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(494, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(224, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "見積№";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label2.Visible = false;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(719, 1);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 33);
            this.label3.TabIndex = 2;
            this.label3.Text = "SEQ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label3.Visible = false;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(800, 1);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 32);
            this.label4.TabIndex = 3;
            this.label4.Text = "Item";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label4.Visible = false;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(30, 24);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(387, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = " 仕入先";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(436, 24);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(362, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "品名";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(819, 24);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(246, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "品番";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(31, 25);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 24);
            this.label9.TabIndex = 8;
            this.label9.Text = "コード";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(159, 25);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(492, 24);
            this.label10.TabIndex = 9;
            this.label10.Text = " 仕入先";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.Location = new System.Drawing.Point(671, 25);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 24);
            this.label11.TabIndex = 10;
            this.label11.Text = "見積単価";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(827, 25);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(189, 24);
            this.label12.TabIndex = 11;
            this.label12.Text = "入力日付";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(13, 254);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(1226, 23);
            this.label13.TabIndex = 12;
            this.label13.Text = "ｺｰﾄﾞを入力して下さい。";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.numTanka);
            this.Frame1.Controls.Add(this.txtHinban);
            this.Frame1.Controls.Add(this.txtHinmei);
            this.Frame1.Controls.Add(this.txtShiireNm);
            this.Frame1.Controls.Add(this.label5);
            this.Frame1.Controls.Add(this.label6);
            this.Frame1.Controls.Add(this.label7);
            this.Frame1.Controls.Add(this.label14);
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame1.Location = new System.Drawing.Point(12, 73);
            this.Frame1.Margin = new System.Windows.Forms.Padding(4);
            this.Frame1.Name = "Frame1";
            this.Frame1.Padding = new System.Windows.Forms.Padding(4);
            this.Frame1.Size = new System.Drawing.Size(1227, 83);
            this.Frame1.TabIndex = 13;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "見積情報";
            this.Frame1.Enter += new System.EventHandler(this.Frame1_Enter);
            // 
            // numTanka
            // 
            this.numTanka.Location = new System.Drawing.Point(1073, 47);
            this.numTanka.Name = "numTanka";
            this.numTanka.ReadOnly = true;
            this.numTanka.Size = new System.Drawing.Size(105, 23);
            this.numTanka.TabIndex = 13;
            this.numTanka.TabStop = false;
            this.numTanka.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numSEQ_KeyPress);
            // 
            // txtHinban
            // 
            this.txtHinban.Location = new System.Drawing.Point(819, 47);
            this.txtHinban.Name = "txtHinban";
            this.txtHinban.Size = new System.Drawing.Size(246, 23);
            this.txtHinban.TabIndex = 12;
            // 
            // txtHinmei
            // 
            this.txtHinmei.Location = new System.Drawing.Point(436, 48);
            this.txtHinmei.Name = "txtHinmei";
            this.txtHinmei.Size = new System.Drawing.Size(362, 23);
            this.txtHinmei.TabIndex = 11;
            this.txtHinmei.TextChanged += new System.EventHandler(this.txtHinmei_TextChanged);
            // 
            // txtShiireNm
            // 
            this.txtShiireNm.Location = new System.Drawing.Point(30, 47);
            this.txtShiireNm.Name = "txtShiireNm";
            this.txtShiireNm.Size = new System.Drawing.Size(387, 23);
            this.txtShiireNm.TabIndex = 10;
            this.txtShiireNm.TabStop = false;
            // 
            // label14
            // 
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(1073, 24);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 21);
            this.label14.TabIndex = 7;
            this.label14.Text = "見積単価";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(621, 52);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(30, 18);
            this.button1.TabIndex = 2;
            this.button1.TabStop = false;
            this.button1.Text = "...";
            this.toolTip1.SetToolTip(this.button1, "仕入先の一覧を表示します。");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // frame2
            // 
            this.frame2.Controls.Add(this.pictnumSTankaureBox4);
            this.frame2.Controls.Add(this.datSDate);
            this.frame2.Controls.Add(this.txtSName);
            this.frame2.Controls.Add(this.button1);
            this.frame2.Controls.Add(this.numSCode);
            this.frame2.Controls.Add(this.label9);
            this.frame2.Controls.Add(this.label10);
            this.frame2.Controls.Add(this.label11);
            this.frame2.Controls.Add(this.label12);
            this.frame2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.frame2.Location = new System.Drawing.Point(12, 164);
            this.frame2.Margin = new System.Windows.Forms.Padding(4);
            this.frame2.Name = "frame2";
            this.frame2.Padding = new System.Windows.Forms.Padding(4);
            this.frame2.Size = new System.Drawing.Size(1227, 86);
            this.frame2.TabIndex = 18;
            this.frame2.TabStop = false;
            this.frame2.Text = "仕入先見積単価";
            this.frame2.Enter += new System.EventHandler(this.frame2_Enter);
            // 
            // pictnumSTankaureBox4
            // 
            this.pictnumSTankaureBox4.Location = new System.Drawing.Point(671, 53);
            this.pictnumSTankaureBox4.Name = "pictnumSTankaureBox4";
            this.pictnumSTankaureBox4.ReadOnly = true;
            this.pictnumSTankaureBox4.Size = new System.Drawing.Size(135, 23);
            this.pictnumSTankaureBox4.TabIndex = 3;
            this.pictnumSTankaureBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numSEQ_KeyPress);
            // 
            // datSDate
            // 
            this.datSDate.Location = new System.Drawing.Point(827, 52);
            this.datSDate.Name = "datSDate";
            this.datSDate.Size = new System.Drawing.Size(189, 23);
            this.datSDate.TabIndex = 4;
            // 
            // txtSName
            // 
            this.txtSName.Location = new System.Drawing.Point(159, 52);
            this.txtSName.Name = "txtSName";
            this.txtSName.Size = new System.Drawing.Size(455, 23);
            this.txtSName.TabIndex = 1;
            // 
            // numSCode
            // 
            this.numSCode.Location = new System.Drawing.Point(31, 52);
            this.numSCode.Name = "numSCode";
            this.numSCode.Size = new System.Drawing.Size(118, 23);
            this.numSCode.TabIndex = 0;
            this.numSCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numSEQ_KeyPress);
            // 
            // cmdFunc_10
            // 
            this.cmdFunc_10.Location = new System.Drawing.Point(1061, 307);
            this.cmdFunc_10.Name = "cmdFunc_10";
            this.cmdFunc_10.Size = new System.Drawing.Size(83, 31);
            this.cmdFunc_10.TabIndex = 5;
            this.cmdFunc_10.Text = "F10:更新";
            this.cmdFunc_10.UseVisualStyleBackColor = true;
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.Location = new System.Drawing.Point(1150, 307);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(89, 31);
            this.cmdFunc_12.TabIndex = 6;
            this.cmdFunc_12.Text = "F12:戻る";
            this.cmdFunc_12.UseVisualStyleBackColor = true;
            // 
            // txtMitumoriNo
            // 
            this.txtMitumoriNo.Location = new System.Drawing.Point(494, 36);
            this.txtMitumoriNo.Name = "txtMitumoriNo";
            this.txtMitumoriNo.Size = new System.Drawing.Size(224, 24);
            this.txtMitumoriNo.TabIndex = 7;
            this.txtMitumoriNo.TabStop = false;
            this.txtMitumoriNo.Visible = false;
            // 
            // numSEQ
            // 
            this.numSEQ.Location = new System.Drawing.Point(719, 36);
            this.numSEQ.Name = "numSEQ";
            this.numSEQ.ReadOnly = true;
            this.numSEQ.Size = new System.Drawing.Size(80, 24);
            this.numSEQ.TabIndex = 8;
            this.numSEQ.TabStop = false;
            this.numSEQ.Visible = false;
            this.numSEQ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numSEQ_KeyPress);
            // 
            // numItem
            // 
            this.numItem.Location = new System.Drawing.Point(800, 36);
            this.numItem.Name = "numItem";
            this.numItem.ReadOnly = true;
            this.numItem.Size = new System.Drawing.Size(57, 24);
            this.numItem.TabIndex = 9;
            this.numItem.TabStop = false;
            this.numItem.Visible = false;
            this.numItem.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numSEQ_KeyPress);
            // 
            // frmSiireTanka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1259, 381);
            this.Controls.Add(this.numItem);
            this.Controls.Add(this.numSEQ);
            this.Controls.Add(this.txtMitumoriNo);
            this.Controls.Add(this.cmdFunc_12);
            this.Controls.Add(this.cmdFunc_10);
            this.Controls.Add(this.frame2);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmSiireTanka";
            this.Text = "frmSiireTanka";
            this.Load += new System.EventHandler(this.frmSiireTanka_Load);
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            this.frame2.ResumeLayout(false);
            this.frame2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox txtMitumoriNo;
        public System.Windows.Forms.TextBox numSEQ;
        public System.Windows.Forms.TextBox numItem;
        public System.Windows.Forms.TextBox txtShiireNm;
        public System.Windows.Forms.TextBox txtHinmei;
        public System.Windows.Forms.TextBox txtHinban;
        public System.Windows.Forms.TextBox numTanka;
        public System.Windows.Forms.TextBox pictnumSTankaureBox4;
        public System.Windows.Forms.DateTimePicker datSDate;
        public System.Windows.Forms.TextBox txtSName;
        public System.Windows.Forms.TextBox numSCode;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.GroupBox Frame1;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.GroupBox frame2;
        public System.Windows.Forms.Button cmdFunc_10;
        public System.Windows.Forms.Button cmdFunc_12;
    }
}