﻿using clsDB;
using clswinApi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBlibrary;
using static VBlibrary.modSinseiMarin;

namespace EstimateProcessing
{
    public partial class frmRefSyoTbl : Form
    {
        string Syo_Code;
        string Syo_Hinmei;
        string Syo_Hinban;
        string Syo_Tani;
        Boolean Syo_Status;
        SyoTblInfo SyoTbl;

        public frmRefSyoTbl()
        {
            InitializeComponent();
            spdList.DoubleClick += new EventHandler(cmdFunc_12_Click);
            txtSerch_0.KeyUp += new KeyEventHandler(searchevent);
            txtSerch_1.KeyUp += new KeyEventHandler(searchevent);
            txtSerch_2.KeyUp += new KeyEventHandler(searchevent);
            spdList.KeyDown += new KeyEventHandler(gridkeyup);
        }
        private void gridkeyup(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        cmdFunc_12_Click(sender, null);
                        break;
                    }
                case Keys.Back:
                    {
                        txtSerch_0.Focus();
                        break;
                    }
            }
        }

        private void searchevent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                spdList.Focus();
                return;
            }
            SetTblInfo();
        }


        private void cmdFunc_10_Click(object sender, EventArgs e)
        {
            if (DatainsertProc())
            {
                EditClear();
                SetTblInfo();
            }
        }

        private void cmdFunc_11_Click(object sender, EventArgs e)
        {
            modSac_Com.RetMode = 0;
            this.Close();
        }

        private void cmdFunc_12_Click(object sender, EventArgs e)
        {
            modSac_Com.RetMode = 1;
            int selectedrowindex = spdList.SelectedCells[0].RowIndex;
            setActiveData(selectedrowindex);
            Syo_Status = true;
            this.Close();
        }

        private void frmRefSyoTbl_Load(object sender, EventArgs e)
        {
            EditClear();
            SetTblInfo();
            cmdFunc_10.Text = "(F10)" + Environment.NewLine + "ENTER";
            cmdFunc_11.Text = "(F11)" + Environment.NewLine + "BACK";
            cmdFunc_12.Text = "(F12)" + Environment.NewLine + "SET";
            txtSerch_0.Focus();
            Application.DoEvents();
        }


        private void EditClear()
        {

            Syo_Code = "";
            Syo_Hinmei = "";
            Syo_Hinban = "";
            Syo_Tani = "";
            Syo_Status = false;

            txtSerch_0.Clear();
            txtSerch_1.Clear();
            txtSerch_2.Clear();
        }

        private void setActiveData(int row)
        {
            Syo_Code = spdList.Rows[row].Cells[0].Value.ToString();
            Syo_Hinmei = spdList.Rows[row].Cells[1].Value.ToString();
            Syo_Hinban = spdList.Rows[row].Cells[2].Value.ToString();
            Syo_Tani = spdList.Rows[row].Cells[3].Value.ToString();

        }


        private void SetTblInfo()
        {

            string SQLtxt = "SELECT Syo_001, Syo_002, Syo_003, Syo_005 FROM Syo_Tbl";

            if (txtSerch_0.Text.Trim() != "")
                SQLtxt += String.Format(" Where Syo_002 like '%{0}%'", txtSerch_0.Text.Trim());

            if (txtSerch_1.Text.Trim() != "")
            {


                if (!SQLtxt.ToUpper().Contains("WHERE"))
                    SQLtxt += String.Format(" Where Syo_003 like '%{0}%'", txtSerch_1.Text.Trim());
                else
                    SQLtxt += String.Format(" And Syo_003 like '%{0}%'", txtSerch_1.Text.Trim());
            }
            if (txtSerch_2.Text.Trim() != "")
            {
                if (!SQLtxt.ToUpper().Contains("WHERE"))
                    SQLtxt += String.Format(" Where Syo_005 like '%{0}%'", txtSerch_2.Text.Trim());
                else
                    SQLtxt += String.Format(" And Syo_005 like '%{0}%'", txtSerch_2.Text.Trim());
            }



            SQLtxt += " ORDER BY 1,2,3,4";


            DBService db = new DBService();
            DataTable dt = new DataTable();
            dt = db.GetDatafromDB(SQLtxt);
            spdList.DataSource = dt;
        }
        private Boolean DatainsertProc()
        {



            string SQLtxt = " SELECT * FROM Syo_Tbl ";
            SQLtxt = SQLtxt + "WHERE Syo_002 ='" + modCommon.EditSQLAddSQuot(modHanbai.NCnvN(txtSerch_0.Text)) + "'"; // •i–¼
            SQLtxt = SQLtxt + "  AND Syo_003 ='" + modCommon.EditSQLAddSQuot(modHanbai.NCnvN(txtSerch_1.Text)) + "'"; // •i”Ô
            SQLtxt = SQLtxt + "  AND Syo_005 ='" + modCommon.EditSQLAddSQuot(modHanbai.NCnvN(txtSerch_2.Text)) + "'"; // •i”Ô
            DBService db = new DBService();
            DataTable dt = new DataTable();
            dt = db.GetDatafromDB(SQLtxt);
            spdList.DataSource = dt;
            if (dt.Rows.Count >= 1)
            {
                return false;
            }


            if (!( txtSerch_0.Text.Trim() == "" && txtSerch_1.Text.Trim() == "" && txtSerch_2.Text.Trim() == ""))
            {
                if (SyoInsertProc())
                {
                    SetTblInfo();
                    return true;
                }

            }
            return false;
        }
        private bool SyoInsertProc()
        {
            SyoTbl = new SyoTblInfo();

            SyoTbl.Syo_001 = modSinseiMarin.GetNewCode(6);
            SyoTbl.Syo_002 = modHanbai.NCnvN(txtSerch_0.Text.Trim());
            SyoTbl.Syo_003 = modHanbai.NCnvN(txtSerch_1.Text.Trim());
            SyoTbl.Syo_005 = modHanbai.NCnvN(txtSerch_2.Text.Trim());

            string strSQL = "";
            strSQL = strSQL + "INSERT INTO Syo_Tbl(";
            strSQL = strSQL + "Syo_001,";
            strSQL = strSQL + "Syo_002,";
            strSQL = strSQL + "Syo_003,";
            strSQL = strSQL + "Sir_005,";
            strSQL = strSQL + "Sir_Insert,";
            strSQL = strSQL + "Uni_WsNo";
            strSQL = strSQL + ") VALUES (";
            strSQL = strSQL + SyoTbl.Syo_001 + ",'"; 
            strSQL = strSQL + modCommon.EditSQLAddSQuot(SyoTbl.Syo_002) + "','";
            strSQL = strSQL + modCommon.EditSQLAddSQuot(SyoTbl.Syo_003) + "','";
            strSQL = strSQL + modCommon.EditSQLAddSQuot(SyoTbl.Syo_005) + "','";
            strSQL = strSQL + DateTime.Today.ToShortDateString() + "','";
            strSQL = strSQL + StaticWinApi.Wsnumber + "')";
            DBService db = new DBService();
            string errormsg;

            db.ExecuteSql(strSQL, out errormsg);
            if (errormsg != "")
            {
                MessageBox.Show(@"Error has been encountered: 
                    " + errormsg);
                return false;
            }
            else
            {
                return true;
            }
        }
        

        public bool Status()
        {
            return Syo_Status;
        }

        public string STM_CODE()
        {
            return Syo_Code;
        }
        public string STM_HINMEI()
        {
            return Syo_Hinmei;
        }
        public string STM_HINBAN()
        {
            return Syo_Hinban;
        }
        public string STM_TANI()
        {
            return Syo_Tani;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }






    }
}
