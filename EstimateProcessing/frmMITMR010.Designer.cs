﻿namespace EstimateProcessing
{
    partial class frmMITMR010
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMITMR010));
            this.cboCountry = new System.Windows.Forms.ComboBox();
            this.spdData = new System.Windows.Forms.DataGridView();
            this.index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colActual = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCodeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRef1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colCase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSeq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDesc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRef2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colPartNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUnitPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOrigin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colddtDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPPNet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPurchAmt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colMargin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOrderDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPurchDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSalesDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInvoiceDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVendor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colVendorCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colInputDAte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.menustrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.cboManila = new System.Windows.Forms.ComboBox();
            this.timTimer = new System.Windows.Forms.Timer(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.txtTekiyo = new System.Windows.Forms.TextBox();
            this.cboMitsumori = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmdNewNo = new System.Windows.Forms.Button();
            this.datMtHakko = new System.Windows.Forms.DateTimePicker();
            this.datMitsuYMD = new System.Windows.Forms.DateTimePicker();
            this.txtShipNo = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.optCountry_1 = new System.Windows.Forms.RadioButton();
            this.optCountry_0 = new System.Windows.Forms.RadioButton();
            this.cmdSansyo_1 = new System.Windows.Forms.Button();
            this.numOCode = new System.Windows.Forms.TextBox();
            this.txtOName = new System.Windows.Forms.TextBox();
            this.txtDoc = new System.Windows.Forms.TextBox();
            this.txtVessel = new System.Windows.Forms.TextBox();
            this.txtTokTanto = new System.Windows.Forms.TextBox();
            this.txtTokName = new System.Windows.Forms.TextBox();
            this.numTokCode = new System.Windows.Forms.TextBox();
            this.txtIraiNo = new System.Windows.Forms.TextBox();
            this.txtMitsuNo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCompany = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtComment4 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtComment1 = new System.Windows.Forms.TextBox();
            this.txtSerNo = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtComment2 = new System.Windows.Forms.TextBox();
            this.txtDwgNo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtComment3 = new System.Windows.Forms.TextBox();
            this.txtAdditional = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.cmdSansyo_4 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.cmdSansyo_3 = new System.Windows.Forms.Button();
            this.numSEQ = new System.Windows.Forms.TextBox();
            this.numLastSEQ = new System.Windows.Forms.TextBox();
            this.txtUnit = new System.Windows.Forms.TextBox();
            this.cmdSansyo_2 = new System.Windows.Forms.Button();
            this.cmdSEQ_0 = new System.Windows.Forms.Button();
            this.cmdSEQ_1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboTanto = new System.Windows.Forms.ComboBox();
            this.txtMitsuNouki = new System.Windows.Forms.TextBox();
            this.txtOrigin = new System.Windows.Forms.TextBox();
            this.numMitsuYuko = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.cmdSansyo_0 = new System.Windows.Forms.Button();
            this.lblJoutai = new System.Windows.Forms.Label();
            this.lblModeTitle = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.numUKingaku = new System.Windows.Forms.TextBox();
            this.optMeisai_1 = new System.Windows.Forms.RadioButton();
            this.optMeisai_0 = new System.Windows.Forms.RadioButton();
            this.numUSuryo = new System.Windows.Forms.TextBox();
            this.numSKingaku = new System.Windows.Forms.TextBox();
            this.numSSuryo = new System.Windows.Forms.TextBox();
            this.numHKingaku = new System.Windows.Forms.TextBox();
            this.numHSuryo = new System.Windows.Forms.TextBox();
            this.numJKingaku = new System.Windows.Forms.TextBox();
            this.numJSuryo = new System.Windows.Forms.TextBox();
            this.numMKingaku = new System.Windows.Forms.TextBox();
            this.numMSuryo = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.cmdFunc_1 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_2 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_3 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_4 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_5 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_6 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_7 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_8 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_9 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_10 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_11 = new System.Windows.Forms.ToolStripButton();
            this.cmdFunc_12 = new System.Windows.Forms.ToolStripButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.datPIHakko = new System.Windows.Forms.DateTimePicker();
            this.imDay = new System.Windows.Forms.TextBox();
            this.Command1_0 = new System.Windows.Forms.Button();
            this.Command1_2 = new System.Windows.Forms.Button();
            this.Command1_1 = new System.Windows.Forms.Button();
            this.txtOrderNo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.spdData)).BeginInit();
            this.menustrip.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.toolBar.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // cboCountry
            // 
            this.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCountry.FormattingEnabled = true;
            this.cboCountry.Location = new System.Drawing.Point(187, 82);
            this.cboCountry.Name = "cboCountry";
            this.cboCountry.Size = new System.Drawing.Size(207, 21);
            this.cboCountry.TabIndex = 19;
            this.cboCountry.TabStop = false;
            // 
            // spdData
            // 
            this.spdData.AllowUserToAddRows = false;
            this.spdData.AllowUserToDeleteRows = false;
            this.spdData.AllowUserToResizeColumns = false;
            this.spdData.AllowUserToResizeRows = false;
            this.spdData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spdData.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.spdData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spdData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.index,
            this.colActual,
            this.colCodeNo,
            this.colNos,
            this.colRef1,
            this.colCase,
            this.colSeq,
            this.colItem,
            this.colDesc,
            this.colRef2,
            this.colPartNo,
            this.colQty,
            this.colUnit,
            this.colUnitPrice,
            this.colOrigin,
            this.colddtDay,
            this.colAmt,
            this.colPup,
            this.colRate,
            this.colPPNet,
            this.colPurchAmt,
            this.colMargin,
            this.colOrderDate,
            this.colPurchDate,
            this.colSalesDate,
            this.colInvoiceDate,
            this.colVendor,
            this.colVendorCost,
            this.colInputDAte,
            this.colCode,
            this.Delete});
            this.spdData.ContextMenuStrip = this.menustrip;
            this.spdData.Location = new System.Drawing.Point(5, 392);
            this.spdData.Name = "spdData";
            this.spdData.RowHeadersVisible = false;
            this.spdData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.spdData.Size = new System.Drawing.Size(1366, 215);
            this.spdData.TabIndex = 140;
            this.spdData.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.spdData_CellContentClick);
            this.spdData.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.spdData_CellEndEdit);
            this.spdData.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.spdData_CellValueChanged);
            this.spdData.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.spdData_EditingControlShowing);
            this.spdData.Enter += new System.EventHandler(this.spdData_Enter);
            this.spdData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.spdData_KeyDown);
            this.spdData.KeyUp += new System.Windows.Forms.KeyEventHandler(this.spdData_KeyUp);
            // 
            // index
            // 
            this.index.HeaderText = "idx";
            this.index.Name = "index";
            this.index.Visible = false;
            // 
            // colActual
            // 
            this.colActual.HeaderText = "実績";
            this.colActual.Name = "colActual";
            this.colActual.ReadOnly = true;
            this.colActual.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colActual.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colActual.Text = "実績";
            this.colActual.ToolTipText = "...";
            this.colActual.Visible = false;
            this.colActual.Width = 30;
            // 
            // colCodeNo
            // 
            this.colCodeNo.HeaderText = "Code No.";
            this.colCodeNo.Name = "colCodeNo";
            this.colCodeNo.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colCodeNo.Width = 70;
            // 
            // colNos
            // 
            this.colNos.HeaderText = "Name of Supplier";
            this.colNos.Name = "colNos";
            this.colNos.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colNos.Width = 200;
            // 
            // colRef1
            // 
            this.colRef1.HeaderText = "参照１";
            this.colRef1.Name = "colRef1";
            this.colRef1.ReadOnly = true;
            this.colRef1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colRef1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colRef1.Width = 30;
            // 
            // colCase
            // 
            this.colCase.HeaderText = "Case";
            this.colCase.Name = "colCase";
            this.colCase.Visible = false;
            // 
            // colSeq
            // 
            this.colSeq.HeaderText = "Seq";
            this.colSeq.Name = "colSeq";
            this.colSeq.Visible = false;
            // 
            // colItem
            // 
            this.colItem.HeaderText = "項目";
            this.colItem.Name = "colItem";
            this.colItem.Visible = false;
            // 
            // colDesc
            // 
            this.colDesc.HeaderText = "Description";
            this.colDesc.Name = "colDesc";
            this.colDesc.Width = 250;
            // 
            // colRef2
            // 
            this.colRef2.HeaderText = "参照 2";
            this.colRef2.Name = "colRef2";
            this.colRef2.ReadOnly = true;
            this.colRef2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colRef2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colRef2.Width = 30;
            // 
            // colPartNo
            // 
            this.colPartNo.HeaderText = "Part No.";
            this.colPartNo.Name = "colPartNo";
            this.colPartNo.Width = 150;
            // 
            // colQty
            // 
            this.colQty.HeaderText = "QTY";
            this.colQty.Name = "colQty";
            // 
            // colUnit
            // 
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = "0";
            this.colUnit.DefaultCellStyle = dataGridViewCellStyle4;
            this.colUnit.HeaderText = "Unit";
            this.colUnit.Name = "colUnit";
            // 
            // colUnitPrice
            // 
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = "0";
            this.colUnitPrice.DefaultCellStyle = dataGridViewCellStyle5;
            this.colUnitPrice.HeaderText = "Unit Price";
            this.colUnitPrice.Name = "colUnitPrice";
            // 
            // colOrigin
            // 
            this.colOrigin.HeaderText = "Origin";
            this.colOrigin.Name = "colOrigin";
            this.colOrigin.Width = 50;
            // 
            // colddtDay
            // 
            this.colddtDay.HeaderText = "D/T Day";
            this.colddtDay.Name = "colddtDay";
            this.colddtDay.Width = 50;
            // 
            // colAmt
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = "0";
            this.colAmt.DefaultCellStyle = dataGridViewCellStyle6;
            this.colAmt.HeaderText = "Amount";
            this.colAmt.Name = "colAmt";
            this.colAmt.ReadOnly = true;
            // 
            // colPup
            // 
            this.colPup.HeaderText = "仕入単価";
            this.colPup.Name = "colPup";
            this.colPup.Visible = false;
            // 
            // colRate
            // 
            this.colRate.HeaderText = "掛率";
            this.colRate.Name = "colRate";
            this.colRate.Visible = false;
            // 
            // colPPNet
            // 
            this.colPPNet.HeaderText = "仕入単価NET ";
            this.colPPNet.Name = "colPPNet";
            this.colPPNet.Visible = false;
            // 
            // colPurchAmt
            // 
            this.colPurchAmt.HeaderText = "仕入金額";
            this.colPurchAmt.Name = "colPurchAmt";
            this.colPurchAmt.Visible = false;
            // 
            // colMargin
            // 
            this.colMargin.HeaderText = "粗利";
            this.colMargin.Name = "colMargin";
            this.colMargin.Visible = false;
            // 
            // colOrderDate
            // 
            this.colOrderDate.HeaderText = "発注日";
            this.colOrderDate.Name = "colOrderDate";
            this.colOrderDate.Visible = false;
            // 
            // colPurchDate
            // 
            this.colPurchDate.HeaderText = "仕入日";
            this.colPurchDate.Name = "colPurchDate";
            this.colPurchDate.Visible = false;
            // 
            // colSalesDate
            // 
            this.colSalesDate.HeaderText = "売上日";
            this.colSalesDate.Name = "colSalesDate";
            this.colSalesDate.Visible = false;
            // 
            // colInvoiceDate
            // 
            this.colInvoiceDate.HeaderText = "請求日";
            this.colInvoiceDate.Name = "colInvoiceDate";
            this.colInvoiceDate.Visible = false;
            // 
            // colVendor
            // 
            this.colVendor.HeaderText = "仕入先名";
            this.colVendor.Name = "colVendor";
            this.colVendor.Visible = false;
            // 
            // colVendorCost
            // 
            this.colVendorCost.HeaderText = "仕入先見積原価";
            this.colVendorCost.Name = "colVendorCost";
            this.colVendorCost.Visible = false;
            // 
            // colInputDAte
            // 
            this.colInputDAte.HeaderText = "入力日付  ";
            this.colInputDAte.Name = "colInputDAte";
            this.colInputDAte.Visible = false;
            // 
            // colCode
            // 
            this.colCode.HeaderText = "ｺｰﾄﾞ";
            this.colCode.Name = "colCode";
            this.colCode.Visible = false;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "";
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Text = "Delete";
            this.Delete.ToolTipText = "Delete";
            this.Delete.UseColumnTextForButtonValue = true;
            // 
            // menustrip
            // 
            this.menustrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.mnuCopy,
            this.mnuPaste});
            this.menustrip.Name = "menustrip";
            this.menustrip.Size = new System.Drawing.Size(130, 70);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(129, 22);
            this.toolStripMenuItem1.Text = "Insert Row";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // mnuCopy
            // 
            this.mnuCopy.Name = "mnuCopy";
            this.mnuCopy.Size = new System.Drawing.Size(129, 22);
            this.mnuCopy.Text = "Copy";
            this.mnuCopy.Click += new System.EventHandler(this.mnuCopy_Click_1);
            // 
            // mnuPaste
            // 
            this.mnuPaste.Name = "mnuPaste";
            this.mnuPaste.Size = new System.Drawing.Size(129, 22);
            this.mnuPaste.Text = "Paste";
            this.mnuPaste.Click += new System.EventHandler(this.mnuPaste_Click_1);
            // 
            // cboManila
            // 
            this.cboManila.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboManila.FormattingEnabled = true;
            this.cboManila.ItemHeight = 13;
            this.cboManila.Location = new System.Drawing.Point(335, 133);
            this.cboManila.Name = "cboManila";
            this.cboManila.Size = new System.Drawing.Size(144, 21);
            this.cboManila.TabIndex = 22;
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label25.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(10, 135);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(170, 21);
            this.label25.TabIndex = 99;
            this.label25.Text = "PERSON IN CHARGE ";
            // 
            // txtTekiyo
            // 
            this.txtTekiyo.Location = new System.Drawing.Point(186, 161);
            this.txtTekiyo.Name = "txtTekiyo";
            this.txtTekiyo.Size = new System.Drawing.Size(444, 20);
            this.txtTekiyo.TabIndex = 24;
            this.txtTekiyo.Enter += new System.EventHandler(this.txtTekiyo_Enter);
            // 
            // cboMitsumori
            // 
            this.cboMitsumori.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMitsumori.FormattingEnabled = true;
            this.cboMitsumori.Location = new System.Drawing.Point(485, 133);
            this.cboMitsumori.Name = "cboMitsumori";
            this.cboMitsumori.Size = new System.Drawing.Size(144, 21);
            this.cboMitsumori.TabIndex = 23;
            // 
            // label24
            // 
            this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label24.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(10, 159);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(170, 21);
            this.label24.TabIndex = 99;
            this.label24.Text = "NOTE ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmdNewNo);
            this.groupBox1.Controls.Add(this.datMtHakko);
            this.groupBox1.Controls.Add(this.datMitsuYMD);
            this.groupBox1.Controls.Add(this.txtShipNo);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.cmdSansyo_1);
            this.groupBox1.Controls.Add(this.numOCode);
            this.groupBox1.Controls.Add(this.txtOName);
            this.groupBox1.Controls.Add(this.txtDoc);
            this.groupBox1.Controls.Add(this.txtVessel);
            this.groupBox1.Controls.Add(this.txtTokTanto);
            this.groupBox1.Controls.Add(this.txtTokName);
            this.groupBox1.Controls.Add(this.numTokCode);
            this.groupBox1.Controls.Add(this.txtIraiNo);
            this.groupBox1.Controls.Add(this.txtMitsuNo);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 98);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(534, 289);
            this.groupBox1.TabIndex = 138;
            this.groupBox1.TabStop = false;
            // 
            // cmdNewNo
            // 
            this.cmdNewNo.Location = new System.Drawing.Point(424, 43);
            this.cmdNewNo.Name = "cmdNewNo";
            this.cmdNewNo.Size = new System.Drawing.Size(87, 21);
            this.cmdNewNo.TabIndex = 121;
            this.cmdNewNo.TabStop = false;
            this.cmdNewNo.Text = "見積№";
            this.cmdNewNo.UseVisualStyleBackColor = true;
            this.cmdNewNo.Click += new System.EventHandler(this.cmdNewNo_Click);
            // 
            // datMtHakko
            // 
            this.datMtHakko.CalendarMonthBackground = System.Drawing.Color.White;
            this.datMtHakko.CustomFormat = " ";
            this.datMtHakko.Enabled = false;
            this.datMtHakko.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datMtHakko.Location = new System.Drawing.Point(197, 243);
            this.datMtHakko.Name = "datMtHakko";
            this.datMtHakko.Size = new System.Drawing.Size(84, 20);
            this.datMtHakko.TabIndex = 117;
            this.datMtHakko.TabStop = false;
            // 
            // datMitsuYMD
            // 
            this.datMitsuYMD.CustomFormat = "";
            this.datMitsuYMD.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datMitsuYMD.Location = new System.Drawing.Point(197, 16);
            this.datMitsuYMD.Name = "datMitsuYMD";
            this.datMitsuYMD.Size = new System.Drawing.Size(84, 20);
            this.datMitsuYMD.TabIndex = 113;
            // 
            // txtShipNo
            // 
            this.txtShipNo.Location = new System.Drawing.Point(338, 194);
            this.txtShipNo.Name = "txtShipNo";
            this.txtShipNo.ReadOnly = true;
            this.txtShipNo.Size = new System.Drawing.Size(171, 20);
            this.txtShipNo.TabIndex = 137;
            this.txtShipNo.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.optCountry_1);
            this.groupBox3.Controls.Add(this.optCountry_0);
            this.groupBox3.Location = new System.Drawing.Point(371, 87);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(140, 31);
            this.groupBox3.TabIndex = 132;
            this.groupBox3.TabStop = false;
            // 
            // optCountry_1
            // 
            this.optCountry_1.Enabled = false;
            this.optCountry_1.Location = new System.Drawing.Point(63, 7);
            this.optCountry_1.Name = "optCountry_1";
            this.optCountry_1.Size = new System.Drawing.Size(73, 17);
            this.optCountry_1.TabIndex = 96;
            this.optCountry_1.Text = "OverSeas";
            this.optCountry_1.UseVisualStyleBackColor = true;
            // 
            // optCountry_0
            // 
            this.optCountry_0.AutoSize = true;
            this.optCountry_0.Enabled = false;
            this.optCountry_0.Location = new System.Drawing.Point(7, 7);
            this.optCountry_0.Name = "optCountry_0";
            this.optCountry_0.Size = new System.Drawing.Size(54, 17);
            this.optCountry_0.TabIndex = 97;
            this.optCountry_0.Text = "Japan";
            this.optCountry_0.UseVisualStyleBackColor = true;
            // 
            // cmdSansyo_1
            // 
            this.cmdSansyo_1.Location = new System.Drawing.Point(314, 89);
            this.cmdSansyo_1.Name = "cmdSansyo_1";
            this.cmdSansyo_1.Size = new System.Drawing.Size(51, 24);
            this.cmdSansyo_1.TabIndex = 116;
            this.cmdSansyo_1.Text = "...";
            this.cmdSansyo_1.UseVisualStyleBackColor = true;
            this.cmdSansyo_1.Click += new System.EventHandler(this.cmdSansyo_1_Click);
            // 
            // numOCode
            // 
            this.numOCode.Location = new System.Drawing.Point(256, 92);
            this.numOCode.Name = "numOCode";
            this.numOCode.ReadOnly = true;
            this.numOCode.Size = new System.Drawing.Size(52, 20);
            this.numOCode.TabIndex = 131;
            this.numOCode.TabStop = false;
            // 
            // txtOName
            // 
            this.txtOName.Location = new System.Drawing.Point(197, 219);
            this.txtOName.Name = "txtOName";
            this.txtOName.ReadOnly = true;
            this.txtOName.Size = new System.Drawing.Size(312, 20);
            this.txtOName.TabIndex = 135;
            this.txtOName.TabStop = false;
            // 
            // txtDoc
            // 
            this.txtDoc.Location = new System.Drawing.Point(197, 194);
            this.txtDoc.Name = "txtDoc";
            this.txtDoc.ReadOnly = true;
            this.txtDoc.Size = new System.Drawing.Size(135, 20);
            this.txtDoc.TabIndex = 136;
            this.txtDoc.TabStop = false;
            // 
            // txtVessel
            // 
            this.txtVessel.Location = new System.Drawing.Point(197, 169);
            this.txtVessel.Name = "txtVessel";
            this.txtVessel.ReadOnly = true;
            this.txtVessel.Size = new System.Drawing.Size(312, 20);
            this.txtVessel.TabIndex = 120;
            this.txtVessel.TabStop = false;
            // 
            // txtTokTanto
            // 
            this.txtTokTanto.Location = new System.Drawing.Point(197, 144);
            this.txtTokTanto.Name = "txtTokTanto";
            this.txtTokTanto.ReadOnly = true;
            this.txtTokTanto.Size = new System.Drawing.Size(312, 20);
            this.txtTokTanto.TabIndex = 119;
            this.txtTokTanto.TabStop = false;
            // 
            // txtTokName
            // 
            this.txtTokName.Location = new System.Drawing.Point(197, 121);
            this.txtTokName.Name = "txtTokName";
            this.txtTokName.ReadOnly = true;
            this.txtTokName.Size = new System.Drawing.Size(312, 20);
            this.txtTokName.TabIndex = 118;
            this.txtTokName.TabStop = false;
            // 
            // numTokCode
            // 
            this.numTokCode.Location = new System.Drawing.Point(197, 92);
            this.numTokCode.Name = "numTokCode";
            this.numTokCode.ReadOnly = true;
            this.numTokCode.Size = new System.Drawing.Size(53, 20);
            this.numTokCode.TabIndex = 129;
            this.numTokCode.TabStop = false;
            // 
            // txtIraiNo
            // 
            this.txtIraiNo.Location = new System.Drawing.Point(197, 67);
            this.txtIraiNo.Name = "txtIraiNo";
            this.txtIraiNo.Size = new System.Drawing.Size(314, 20);
            this.txtIraiNo.TabIndex = 115;
            // 
            // txtMitsuNo
            // 
            this.txtMitsuNo.Location = new System.Drawing.Point(197, 43);
            this.txtMitsuNo.Name = "txtMitsuNo";
            this.txtMitsuNo.Size = new System.Drawing.Size(221, 20);
            this.txtMitsuNo.TabIndex = 114;
            this.txtMitsuNo.TextChanged += new System.EventHandler(this.txtMitsuNo_TextChanged);
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label10.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 244);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(174, 18);
            this.label10.TabIndex = 133;
            this.label10.Text = "DATE OF ISSUE QUOTE ";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(17, 220);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(174, 18);
            this.label9.TabIndex = 128;
            this.label9.Text = "OWNER NAME ";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label8.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 195);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(174, 18);
            this.label8.TabIndex = 127;
            this.label8.Text = "SHIP BUILDER /NO :";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 18);
            this.label7.TabIndex = 126;
            this.label7.Text = "Vessel\'s Name ";
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(17, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(174, 18);
            this.label6.TabIndex = 125;
            this.label6.Text = "Attn ";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 18);
            this.label5.TabIndex = 124;
            this.label5.Text = "MESSRS ";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 18);
            this.label4.TabIndex = 123;
            this.label4.Text = "CUSTOMER CODE ";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 18);
            this.label3.TabIndex = 122;
            this.label3.Text = "Your Ref No.";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(174, 18);
            this.label2.TabIndex = 130;
            this.label2.Text = "Our Estimate No";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 18);
            this.label1.TabIndex = 134;
            this.label1.Text = "QUOTED DATE ";
            // 
            // lblCompany
            // 
            this.lblCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCompany.AutoSize = true;
            this.lblCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompany.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblCompany.Location = new System.Drawing.Point(836, 52);
            this.lblCompany.Name = "lblCompany";
            this.lblCompany.Size = new System.Drawing.Size(472, 55);
            this.lblCompany.TabIndex = 147;
            this.lblCompany.Text = "KKKKKKKKKKKKKK";
            this.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txtComment4);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.txtType);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.txtComment1);
            this.groupBox4.Controls.Add(this.txtSerNo);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.txtComment2);
            this.groupBox4.Controls.Add(this.txtDwgNo);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.txtComment3);
            this.groupBox4.Controls.Add(this.txtAdditional);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.cmdSansyo_4);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.txtMaker);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.cmdSansyo_3);
            this.groupBox4.Controls.Add(this.numSEQ);
            this.groupBox4.Controls.Add(this.numLastSEQ);
            this.groupBox4.Controls.Add(this.txtUnit);
            this.groupBox4.Controls.Add(this.cmdSansyo_2);
            this.groupBox4.Controls.Add(this.cmdSEQ_0);
            this.groupBox4.Controls.Add(this.cmdSEQ_1);
            this.groupBox4.Location = new System.Drawing.Point(548, 98);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(807, 193);
            this.groupBox4.TabIndex = 139;
            this.groupBox4.TabStop = false;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(354, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 15);
            this.label11.TabIndex = 128;
            this.label11.Text = "SEQ NO:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtComment4
            // 
            this.txtComment4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment4.Location = new System.Drawing.Point(611, 123);
            this.txtComment4.Name = "txtComment4";
            this.txtComment4.Size = new System.Drawing.Size(163, 20);
            this.txtComment4.TabIndex = 110;
            this.txtComment4.TabStop = false;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label14.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(32, 153);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(145, 20);
            this.label14.TabIndex = 126;
            this.label14.Text = "ADD\'L DETAILS ";
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label26.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(515, 123);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(90, 20);
            this.label26.TabIndex = 125;
            this.label26.Text = "COMMENT 4";
            // 
            // txtType
            // 
            this.txtType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtType.Location = new System.Drawing.Point(183, 85);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(242, 20);
            this.txtType.TabIndex = 103;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label15.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(32, 131);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(145, 20);
            this.label15.TabIndex = 124;
            this.label15.Text = "DWG NO ";
            // 
            // txtComment1
            // 
            this.txtComment1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment1.Location = new System.Drawing.Point(611, 52);
            this.txtComment1.Name = "txtComment1";
            this.txtComment1.Size = new System.Drawing.Size(163, 20);
            this.txtComment1.TabIndex = 107;
            this.txtComment1.TabStop = false;
            // 
            // txtSerNo
            // 
            this.txtSerNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSerNo.Location = new System.Drawing.Point(183, 107);
            this.txtSerNo.Name = "txtSerNo";
            this.txtSerNo.Size = new System.Drawing.Size(295, 20);
            this.txtSerNo.TabIndex = 104;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label16.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(32, 109);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(145, 20);
            this.label16.TabIndex = 123;
            this.label16.Text = "SERIAL NO ";
            // 
            // txtComment2
            // 
            this.txtComment2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment2.Location = new System.Drawing.Point(611, 75);
            this.txtComment2.Name = "txtComment2";
            this.txtComment2.Size = new System.Drawing.Size(163, 20);
            this.txtComment2.TabIndex = 108;
            this.txtComment2.TabStop = false;
            // 
            // txtDwgNo
            // 
            this.txtDwgNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDwgNo.Location = new System.Drawing.Point(183, 130);
            this.txtDwgNo.Name = "txtDwgNo";
            this.txtDwgNo.Size = new System.Drawing.Size(295, 20);
            this.txtDwgNo.TabIndex = 105;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label17.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(32, 87);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(145, 20);
            this.label17.TabIndex = 122;
            this.label17.Text = "TYPE ";
            // 
            // txtComment3
            // 
            this.txtComment3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComment3.Location = new System.Drawing.Point(611, 99);
            this.txtComment3.Name = "txtComment3";
            this.txtComment3.Size = new System.Drawing.Size(163, 20);
            this.txtComment3.TabIndex = 109;
            this.txtComment3.TabStop = false;
            // 
            // txtAdditional
            // 
            this.txtAdditional.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdditional.Location = new System.Drawing.Point(183, 153);
            this.txtAdditional.Name = "txtAdditional";
            this.txtAdditional.Size = new System.Drawing.Size(295, 20);
            this.txtAdditional.TabIndex = 106;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label18.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(32, 65);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(145, 20);
            this.label18.TabIndex = 119;
            this.label18.Text = "MAKER ";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label21.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(515, 100);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(90, 20);
            this.label21.TabIndex = 121;
            this.label21.Text = "COMMENT 3";
            // 
            // cmdSansyo_4
            // 
            this.cmdSansyo_4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSansyo_4.Location = new System.Drawing.Point(431, 84);
            this.cmdSansyo_4.Name = "cmdSansyo_4";
            this.cmdSansyo_4.Size = new System.Drawing.Size(47, 21);
            this.cmdSansyo_4.TabIndex = 113;
            this.cmdSansyo_4.TabStop = false;
            this.cmdSansyo_4.Text = "...";
            this.cmdSansyo_4.UseVisualStyleBackColor = true;
            this.cmdSansyo_4.Click += new System.EventHandler(this.cmdSansyo_4_Click);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label19.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(32, 43);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(145, 20);
            this.label19.TabIndex = 120;
            this.label19.Text = "UNIT ";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label22.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(515, 77);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(90, 20);
            this.label22.TabIndex = 127;
            this.label22.Text = "COMMENT 2";
            // 
            // txtMaker
            // 
            this.txtMaker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtMaker.Location = new System.Drawing.Point(183, 63);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.ReadOnly = true;
            this.txtMaker.Size = new System.Drawing.Size(242, 20);
            this.txtMaker.TabIndex = 102;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label20.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(32, 21);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(145, 20);
            this.label20.TabIndex = 117;
            this.label20.Text = "SEQ ";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label23.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(515, 53);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(90, 20);
            this.label23.TabIndex = 118;
            this.label23.Text = "COMMENT 1";
            // 
            // cmdSansyo_3
            // 
            this.cmdSansyo_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSansyo_3.Location = new System.Drawing.Point(431, 62);
            this.cmdSansyo_3.Name = "cmdSansyo_3";
            this.cmdSansyo_3.Size = new System.Drawing.Size(47, 21);
            this.cmdSansyo_3.TabIndex = 114;
            this.cmdSansyo_3.TabStop = false;
            this.cmdSansyo_3.Text = "...";
            this.cmdSansyo_3.UseVisualStyleBackColor = true;
            this.cmdSansyo_3.Click += new System.EventHandler(this.cmdSansyo_3_Click);
            // 
            // numSEQ
            // 
            this.numSEQ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numSEQ.Location = new System.Drawing.Point(183, 20);
            this.numSEQ.Name = "numSEQ";
            this.numSEQ.Size = new System.Drawing.Size(70, 20);
            this.numSEQ.TabIndex = 100;
            this.numSEQ.TextChanged += new System.EventHandler(this.numSEQ_TextChanged);
            this.numSEQ.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numSEQ_KeyUp);
            // 
            // numLastSEQ
            // 
            this.numLastSEQ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numLastSEQ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.numLastSEQ.Location = new System.Drawing.Point(431, 19);
            this.numLastSEQ.Name = "numLastSEQ";
            this.numLastSEQ.ReadOnly = true;
            this.numLastSEQ.Size = new System.Drawing.Size(44, 20);
            this.numLastSEQ.TabIndex = 116;
            this.numLastSEQ.TabStop = false;
            this.numLastSEQ.Text = "0";
            // 
            // txtUnit
            // 
            this.txtUnit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUnit.Location = new System.Drawing.Point(183, 41);
            this.txtUnit.Name = "txtUnit";
            this.txtUnit.Size = new System.Drawing.Size(242, 20);
            this.txtUnit.TabIndex = 101;
            // 
            // cmdSansyo_2
            // 
            this.cmdSansyo_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSansyo_2.Location = new System.Drawing.Point(431, 40);
            this.cmdSansyo_2.Name = "cmdSansyo_2";
            this.cmdSansyo_2.Size = new System.Drawing.Size(47, 21);
            this.cmdSansyo_2.TabIndex = 115;
            this.cmdSansyo_2.TabStop = false;
            this.cmdSansyo_2.Text = "...";
            this.cmdSansyo_2.UseVisualStyleBackColor = true;
            this.cmdSansyo_2.Click += new System.EventHandler(this.cmdSansyo_2_Click);
            // 
            // cmdSEQ_0
            // 
            this.cmdSEQ_0.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSEQ_0.Location = new System.Drawing.Point(259, 19);
            this.cmdSEQ_0.Name = "cmdSEQ_0";
            this.cmdSEQ_0.Size = new System.Drawing.Size(33, 21);
            this.cmdSEQ_0.TabIndex = 111;
            this.cmdSEQ_0.TabStop = false;
            this.cmdSEQ_0.Text = "←";
            this.cmdSEQ_0.UseVisualStyleBackColor = true;
            this.cmdSEQ_0.Click += new System.EventHandler(this.cmdSEQClick);
            // 
            // cmdSEQ_1
            // 
            this.cmdSEQ_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdSEQ_1.Location = new System.Drawing.Point(298, 19);
            this.cmdSEQ_1.Name = "cmdSEQ_1";
            this.cmdSEQ_1.Size = new System.Drawing.Size(30, 21);
            this.cmdSEQ_1.TabIndex = 112;
            this.cmdSEQ_1.TabStop = false;
            this.cmdSEQ_1.Text = "→";
            this.cmdSEQ_1.UseVisualStyleBackColor = true;
            this.cmdSEQ_1.Click += new System.EventHandler(this.cmdSEQClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTekiyo);
            this.groupBox2.Controls.Add(this.cboMitsumori);
            this.groupBox2.Controls.Add(this.cboManila);
            this.groupBox2.Controls.Add(this.cboTanto);
            this.groupBox2.Controls.Add(this.txtMitsuNouki);
            this.groupBox2.Controls.Add(this.cboCountry);
            this.groupBox2.Controls.Add(this.txtOrigin);
            this.groupBox2.Controls.Add(this.numMitsuYuko);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Location = new System.Drawing.Point(8, 610);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(647, 207);
            this.groupBox2.TabIndex = 141;
            this.groupBox2.TabStop = false;
            // 
            // cboTanto
            // 
            this.cboTanto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTanto.FormattingEnabled = true;
            this.cboTanto.ItemHeight = 13;
            this.cboTanto.Location = new System.Drawing.Point(186, 133);
            this.cboTanto.Name = "cboTanto";
            this.cboTanto.Size = new System.Drawing.Size(144, 21);
            this.cboTanto.TabIndex = 21;
            // 
            // txtMitsuNouki
            // 
            this.txtMitsuNouki.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtMitsuNouki.Location = new System.Drawing.Point(186, 107);
            this.txtMitsuNouki.Name = "txtMitsuNouki";
            this.txtMitsuNouki.ReadOnly = true;
            this.txtMitsuNouki.Size = new System.Drawing.Size(444, 20);
            this.txtMitsuNouki.TabIndex = 20;
            this.txtMitsuNouki.TabStop = false;
            this.txtMitsuNouki.Enter += new System.EventHandler(this.txtMitsuNouki_Enter);
            // 
            // txtOrigin
            // 
            this.txtOrigin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtOrigin.Location = new System.Drawing.Point(186, 58);
            this.txtOrigin.Name = "txtOrigin";
            this.txtOrigin.ReadOnly = true;
            this.txtOrigin.Size = new System.Drawing.Size(444, 20);
            this.txtOrigin.TabIndex = 18;
            this.txtOrigin.TabStop = false;
            this.txtOrigin.Enter += new System.EventHandler(this.txtOrigin_Enter);
            // 
            // numMitsuYuko
            // 
            this.numMitsuYuko.Location = new System.Drawing.Point(186, 34);
            this.numMitsuYuko.Name = "numMitsuYuko";
            this.numMitsuYuko.Size = new System.Drawing.Size(100, 20);
            this.numMitsuYuko.TabIndex = 17;
            // 
            // label27
            // 
            this.label27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label27.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(10, 108);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(170, 21);
            this.label27.TabIndex = 99;
            this.label27.Text = "DELIVERY TIME ";
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(10, 84);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(170, 21);
            this.label28.TabIndex = 99;
            this.label28.Text = "COUNTRY OF ORIGIN ";
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label29.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(10, 60);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(170, 21);
            this.label29.TabIndex = 99;
            this.label29.Text = "ORIGIN ";
            // 
            // label30
            // 
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label30.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(10, 36);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(170, 21);
            this.label30.TabIndex = 99;
            this.label30.Text = "VALIDITY ";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.LightSkyBlue;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label31.Location = new System.Drawing.Point(12, 70);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(177, 31);
            this.label31.TabIndex = 142;
            this.label31.Text = "QUOTATION";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmdSansyo_0
            // 
            this.cmdSansyo_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdSansyo_0.Font = new System.Drawing.Font("MS Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdSansyo_0.Location = new System.Drawing.Point(358, 66);
            this.cmdSansyo_0.Margin = new System.Windows.Forms.Padding(1);
            this.cmdSansyo_0.Name = "cmdSansyo_0";
            this.cmdSansyo_0.Size = new System.Drawing.Size(161, 36);
            this.cmdSansyo_0.TabIndex = 148;
            this.cmdSansyo_0.TabStop = false;
            this.cmdSansyo_0.Text = "SEARCH";
            this.cmdSansyo_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdSansyo_0.UseVisualStyleBackColor = false;
            // 
            // lblJoutai
            // 
            this.lblJoutai.AutoSize = true;
            this.lblJoutai.BackColor = System.Drawing.SystemColors.Control;
            this.lblJoutai.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJoutai.ForeColor = System.Drawing.Color.Red;
            this.lblJoutai.Location = new System.Drawing.Point(592, 65);
            this.lblJoutai.Name = "lblJoutai";
            this.lblJoutai.Size = new System.Drawing.Size(234, 31);
            this.lblJoutai.TabIndex = 144;
            this.lblJoutai.Text = "受注処理済みです!!";
            this.lblJoutai.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblJoutai.Visible = false;
            // 
            // lblModeTitle
            // 
            this.lblModeTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.lblModeTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.lblModeTitle.Location = new System.Drawing.Point(188, 70);
            this.lblModeTitle.Name = "lblModeTitle";
            this.lblModeTitle.Size = new System.Drawing.Size(167, 31);
            this.lblModeTitle.TabIndex = 143;
            this.lblModeTitle.Text = "追加/訂正";
            this.lblModeTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.numUKingaku);
            this.groupBox6.Controls.Add(this.optMeisai_1);
            this.groupBox6.Controls.Add(this.optMeisai_0);
            this.groupBox6.Controls.Add(this.numUSuryo);
            this.groupBox6.Controls.Add(this.numSKingaku);
            this.groupBox6.Controls.Add(this.numSSuryo);
            this.groupBox6.Controls.Add(this.numHKingaku);
            this.groupBox6.Controls.Add(this.numHSuryo);
            this.groupBox6.Controls.Add(this.numJKingaku);
            this.groupBox6.Controls.Add(this.numJSuryo);
            this.groupBox6.Controls.Add(this.numMKingaku);
            this.groupBox6.Controls.Add(this.numMSuryo);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.label37);
            this.groupBox6.Controls.Add(this.label32);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Enabled = false;
            this.groupBox6.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.Blue;
            this.groupBox6.Location = new System.Drawing.Point(849, 614);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(514, 207);
            this.groupBox6.TabIndex = 146;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "STATUS (EACH PROCESSED QTY/AMOUNT)";
            // 
            // numUKingaku
            // 
            this.numUKingaku.Location = new System.Drawing.Point(324, 150);
            this.numUKingaku.Name = "numUKingaku";
            this.numUKingaku.ReadOnly = true;
            this.numUKingaku.Size = new System.Drawing.Size(128, 22);
            this.numUKingaku.TabIndex = 101;
            this.numUKingaku.TabStop = false;
            this.numUKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // optMeisai_1
            // 
            this.optMeisai_1.AutoSize = true;
            this.optMeisai_1.ForeColor = System.Drawing.Color.Black;
            this.optMeisai_1.Location = new System.Drawing.Point(437, 177);
            this.optMeisai_1.Name = "optMeisai_1";
            this.optMeisai_1.Size = new System.Drawing.Size(57, 19);
            this.optMeisai_1.TabIndex = 86;
            this.optMeisai_1.Text = "状態";
            this.optMeisai_1.UseVisualStyleBackColor = true;
            // 
            // optMeisai_0
            // 
            this.optMeisai_0.AutoSize = true;
            this.optMeisai_0.ForeColor = System.Drawing.Color.Black;
            this.optMeisai_0.Location = new System.Drawing.Point(352, 179);
            this.optMeisai_0.Name = "optMeisai_0";
            this.optMeisai_0.Size = new System.Drawing.Size(89, 19);
            this.optMeisai_0.TabIndex = 85;
            this.optMeisai_0.TabStop = true;
            this.optMeisai_0.Text = "見積原価";
            this.optMeisai_0.UseVisualStyleBackColor = true;
            // 
            // numUSuryo
            // 
            this.numUSuryo.Location = new System.Drawing.Point(200, 149);
            this.numUSuryo.Name = "numUSuryo";
            this.numUSuryo.ReadOnly = true;
            this.numUSuryo.Size = new System.Drawing.Size(117, 22);
            this.numUSuryo.TabIndex = 100;
            this.numUSuryo.TabStop = false;
            // 
            // numSKingaku
            // 
            this.numSKingaku.Location = new System.Drawing.Point(324, 125);
            this.numSKingaku.Name = "numSKingaku";
            this.numSKingaku.ReadOnly = true;
            this.numSKingaku.Size = new System.Drawing.Size(128, 22);
            this.numSKingaku.TabIndex = 93;
            this.numSKingaku.TabStop = false;
            this.numSKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numSSuryo
            // 
            this.numSSuryo.Location = new System.Drawing.Point(200, 125);
            this.numSSuryo.Name = "numSSuryo";
            this.numSSuryo.ReadOnly = true;
            this.numSSuryo.Size = new System.Drawing.Size(117, 22);
            this.numSSuryo.TabIndex = 92;
            this.numSSuryo.TabStop = false;
            // 
            // numHKingaku
            // 
            this.numHKingaku.Location = new System.Drawing.Point(324, 102);
            this.numHKingaku.Name = "numHKingaku";
            this.numHKingaku.ReadOnly = true;
            this.numHKingaku.Size = new System.Drawing.Size(128, 22);
            this.numHKingaku.TabIndex = 80;
            this.numHKingaku.TabStop = false;
            this.numHKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numHSuryo
            // 
            this.numHSuryo.Location = new System.Drawing.Point(200, 102);
            this.numHSuryo.Name = "numHSuryo";
            this.numHSuryo.ReadOnly = true;
            this.numHSuryo.Size = new System.Drawing.Size(117, 22);
            this.numHSuryo.TabIndex = 79;
            this.numHSuryo.TabStop = false;
            // 
            // numJKingaku
            // 
            this.numJKingaku.Location = new System.Drawing.Point(324, 79);
            this.numJKingaku.Name = "numJKingaku";
            this.numJKingaku.ReadOnly = true;
            this.numJKingaku.Size = new System.Drawing.Size(128, 22);
            this.numJKingaku.TabIndex = 78;
            this.numJKingaku.TabStop = false;
            this.numJKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numJSuryo
            // 
            this.numJSuryo.Location = new System.Drawing.Point(200, 79);
            this.numJSuryo.Name = "numJSuryo";
            this.numJSuryo.ReadOnly = true;
            this.numJSuryo.Size = new System.Drawing.Size(117, 22);
            this.numJSuryo.TabIndex = 76;
            this.numJSuryo.TabStop = false;
            // 
            // numMKingaku
            // 
            this.numMKingaku.Location = new System.Drawing.Point(324, 57);
            this.numMKingaku.Name = "numMKingaku";
            this.numMKingaku.ReadOnly = true;
            this.numMKingaku.Size = new System.Drawing.Size(128, 22);
            this.numMKingaku.TabIndex = 77;
            this.numMKingaku.TabStop = false;
            this.numMKingaku.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // numMSuryo
            // 
            this.numMSuryo.Location = new System.Drawing.Point(200, 57);
            this.numMSuryo.Name = "numMSuryo";
            this.numMSuryo.ReadOnly = true;
            this.numMSuryo.Size = new System.Drawing.Size(117, 22);
            this.numMSuryo.TabIndex = 75;
            this.numMSuryo.TabStop = false;
            // 
            // label38
            // 
            this.label38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label38.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(324, 25);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(128, 29);
            this.label38.TabIndex = 99;
            this.label38.Text = "PRICE";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label37
            // 
            this.label37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label37.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(201, 25);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(116, 29);
            this.label37.TabIndex = 99;
            this.label37.Text = "QUANTITY";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label32
            // 
            this.label32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label32.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label32.Location = new System.Drawing.Point(42, 148);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(150, 22);
            this.label32.TabIndex = 99;
            this.label32.Text = "SALES ";
            // 
            // label33
            // 
            this.label33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label33.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label33.Location = new System.Drawing.Point(42, 126);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(150, 22);
            this.label33.TabIndex = 99;
            this.label33.Text = "PURCHASED ";
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label34.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label34.Location = new System.Drawing.Point(42, 104);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(150, 22);
            this.label34.TabIndex = 99;
            this.label34.Text = "ORDER PLACED ";
            // 
            // label35
            // 
            this.label35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label35.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label35.Location = new System.Drawing.Point(42, 81);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(150, 22);
            this.label35.TabIndex = 99;
            this.label35.Text = "ORDER RECEIVED ";
            // 
            // label36
            // 
            this.label36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label36.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Black;
            this.label36.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label36.Location = new System.Drawing.Point(42, 59);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(150, 22);
            this.label36.TabIndex = 99;
            this.label36.Text = "QUOTED ";
            // 
            // toolBar
            // 
            this.toolBar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.toolBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdFunc_1,
            this.cmdFunc_2,
            this.cmdFunc_3,
            this.cmdFunc_4,
            this.cmdFunc_5,
            this.cmdFunc_6,
            this.cmdFunc_7,
            this.cmdFunc_8,
            this.cmdFunc_9,
            this.cmdFunc_10,
            this.cmdFunc_11,
            this.cmdFunc_12});
            this.toolBar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(1379, 52);
            this.toolBar.TabIndex = 145;
            this.toolBar.Text = "toolStrip1";
            // 
            // cmdFunc_1
            // 
            this.cmdFunc_1.AutoSize = false;
            this.cmdFunc_1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_1.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_1.Image")));
            this.cmdFunc_1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_1.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_1.Name = "cmdFunc_1";
            this.cmdFunc_1.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_1.Text = "追加/訂正(F1)";
            this.cmdFunc_1.Click += new System.EventHandler(this.cmdFunc_1_Click);
            // 
            // cmdFunc_2
            // 
            this.cmdFunc_2.AutoSize = false;
            this.cmdFunc_2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_2.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_2.Image")));
            this.cmdFunc_2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_2.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_2.Name = "cmdFunc_2";
            this.cmdFunc_2.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_2.Text = "　   (F2)";
            this.cmdFunc_2.Click += new System.EventHandler(this.cmdFunc_2_Click);
            // 
            // cmdFunc_3
            // 
            this.cmdFunc_3.AutoSize = false;
            this.cmdFunc_3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_3.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_3.Image")));
            this.cmdFunc_3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_3.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_3.Name = "cmdFunc_3";
            this.cmdFunc_3.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_3.Text = " 削除 (F3)";
            this.cmdFunc_3.Click += new System.EventHandler(this.cmdFunc_3_Click);
            // 
            // cmdFunc_4
            // 
            this.cmdFunc_4.AutoSize = false;
            this.cmdFunc_4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_4.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_4.Image")));
            this.cmdFunc_4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_4.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_4.Name = "cmdFunc_4";
            this.cmdFunc_4.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_4.Text = "      (F4)";
            this.cmdFunc_4.Click += new System.EventHandler(this.cmdFunc_4_Click);
            // 
            // cmdFunc_5
            // 
            this.cmdFunc_5.AutoSize = false;
            this.cmdFunc_5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_5.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_5.Image")));
            this.cmdFunc_5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_5.Margin = new System.Windows.Forms.Padding(50, 1, 1, 1);
            this.cmdFunc_5.Name = "cmdFunc_5";
            this.cmdFunc_5.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_5.Text = "見積書(F5)";
            this.cmdFunc_5.Click += new System.EventHandler(this.cmdFunc_5_Click);
            // 
            // cmdFunc_6
            // 
            this.cmdFunc_6.AutoSize = false;
            this.cmdFunc_6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_6.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_6.Image")));
            this.cmdFunc_6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_6.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_6.Name = "cmdFunc_6";
            this.cmdFunc_6.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_6.Text = "　  　(F6)";
            this.cmdFunc_6.Click += new System.EventHandler(this.cmdFunc_6_Click);
            // 
            // cmdFunc_7
            // 
            this.cmdFunc_7.AutoSize = false;
            this.cmdFunc_7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_7.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_7.Image")));
            this.cmdFunc_7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_7.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_7.Name = "cmdFunc_7";
            this.cmdFunc_7.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_7.Text = "　  　(F7)";
            this.cmdFunc_7.Click += new System.EventHandler(this.cmdFunc_7_Click);
            // 
            // cmdFunc_8
            // 
            this.cmdFunc_8.AutoSize = false;
            this.cmdFunc_8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_8.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_8.Image")));
            this.cmdFunc_8.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_8.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_8.Name = "cmdFunc_8";
            this.cmdFunc_8.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_8.Text = "改行制御(F8)";
            this.cmdFunc_8.Click += new System.EventHandler(this.cmdFunc_8_Click);
            // 
            // cmdFunc_9
            // 
            this.cmdFunc_9.AutoSize = false;
            this.cmdFunc_9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_9.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_9.Image")));
            this.cmdFunc_9.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_9.Margin = new System.Windows.Forms.Padding(50, 1, 1, 1);
            this.cmdFunc_9.Name = "cmdFunc_9";
            this.cmdFunc_9.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_9.Text = " 行ｺﾋﾟｰ (F9)";
            this.cmdFunc_9.Click += new System.EventHandler(this.cmdFunc_9_Click);
            // 
            // cmdFunc_10
            // 
            this.cmdFunc_10.AutoSize = false;
            this.cmdFunc_10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_10.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_10.Image")));
            this.cmdFunc_10.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_10.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_10.Name = "cmdFunc_10";
            this.cmdFunc_10.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_10.Text = "　実行　(F10)";
            this.cmdFunc_10.Click += new System.EventHandler(this.cmdFunc_10_Click);
            // 
            // cmdFunc_11
            // 
            this.cmdFunc_11.AutoSize = false;
            this.cmdFunc_11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_11.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_11.Image")));
            this.cmdFunc_11.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_11.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_11.Name = "cmdFunc_11";
            this.cmdFunc_11.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_11.Text = "　取消　(F11)";
            this.cmdFunc_11.Click += new System.EventHandler(this.cmdFunc_11_Click);
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.AutoSize = false;
            this.cmdFunc_12.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cmdFunc_12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.cmdFunc_12.Image = ((System.Drawing.Image)(resources.GetObject("cmdFunc_12.Image")));
            this.cmdFunc_12.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdFunc_12.Margin = new System.Windows.Forms.Padding(1);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(100, 50);
            this.cmdFunc_12.Text = "　終了　(F12)";
            this.cmdFunc_12.Click += new System.EventHandler(this.cmdFunc_12_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.datPIHakko);
            this.groupBox5.Controls.Add(this.imDay);
            this.groupBox5.Controls.Add(this.Command1_0);
            this.groupBox5.Controls.Add(this.Command1_2);
            this.groupBox5.Controls.Add(this.Command1_1);
            this.groupBox5.Controls.Add(this.txtOrderNo);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Location = new System.Drawing.Point(548, 291);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(807, 94);
            this.groupBox5.TabIndex = 149;
            this.groupBox5.TabStop = false;
            // 
            // datPIHakko
            // 
            this.datPIHakko.CalendarForeColor = System.Drawing.Color.Black;
            this.datPIHakko.CalendarMonthBackground = System.Drawing.Color.White;
            this.datPIHakko.CustomFormat = " ";
            this.datPIHakko.Enabled = false;
            this.datPIHakko.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datPIHakko.Location = new System.Drawing.Point(251, 38);
            this.datPIHakko.Name = "datPIHakko";
            this.datPIHakko.Size = new System.Drawing.Size(84, 20);
            this.datPIHakko.TabIndex = 129;
            this.datPIHakko.TabStop = false;
            // 
            // imDay
            // 
            this.imDay.Location = new System.Drawing.Point(647, 59);
            this.imDay.Name = "imDay";
            this.imDay.Size = new System.Drawing.Size(44, 20);
            this.imDay.TabIndex = 135;
            // 
            // Command1_0
            // 
            this.Command1_0.Location = new System.Drawing.Point(515, 59);
            this.Command1_0.Name = "Command1_0";
            this.Command1_0.Size = new System.Drawing.Size(126, 21);
            this.Command1_0.TabIndex = 130;
            this.Command1_0.TabStop = false;
            this.Command1_0.Text = "OEM(0)";
            this.Command1_0.UseVisualStyleBackColor = true;
            this.Command1_0.Click += new System.EventHandler(this.Command1_2_Click);
            // 
            // Command1_2
            // 
            this.Command1_2.Location = new System.Drawing.Point(515, 12);
            this.Command1_2.Name = "Command1_2";
            this.Command1_2.Size = new System.Drawing.Size(126, 21);
            this.Command1_2.TabIndex = 131;
            this.Command1_2.TabStop = false;
            this.Command1_2.Text = "DELETE";
            this.Command1_2.UseVisualStyleBackColor = true;
            this.Command1_2.Click += new System.EventHandler(this.Command1_2_Click);
            // 
            // Command1_1
            // 
            this.Command1_1.Location = new System.Drawing.Point(515, 35);
            this.Command1_1.Name = "Command1_1";
            this.Command1_1.Size = new System.Drawing.Size(126, 21);
            this.Command1_1.TabIndex = 132;
            this.Command1_1.TabStop = false;
            this.Command1_1.Text = "GENUINE(1)";
            this.Command1_1.UseVisualStyleBackColor = true;
            this.Command1_1.Click += new System.EventHandler(this.Command1_2_Click);
            // 
            // txtOrderNo
            // 
            this.txtOrderNo.Location = new System.Drawing.Point(251, 15);
            this.txtOrderNo.Name = "txtOrderNo";
            this.txtOrderNo.Size = new System.Drawing.Size(160, 20);
            this.txtOrderNo.TabIndex = 128;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label12.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(29, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(214, 18);
            this.label12.TabIndex = 133;
            this.label12.Text = "DATE OF PROFORMA INVOICE ";
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(29, 17);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(214, 18);
            this.label13.TabIndex = 134;
            this.label13.Text = "Your Order No.";
            // 
            // frmMITMR010
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1379, 832);
            this.Controls.Add(this.spdData);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblCompany);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.cmdSansyo_0);
            this.Controls.Add(this.lblJoutai);
            this.Controls.Add(this.lblModeTitle);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.groupBox5);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.Name = "frmMITMR010";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmMITMR010_Load_1);
            this.Enter += new System.EventHandler(this.frmMITMR010_Enter);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMITMR010_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.spdData)).EndInit();
            this.menustrip.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ComboBox cboCountry;
        public System.Windows.Forms.DataGridView spdData;
        private System.Windows.Forms.ContextMenuStrip menustrip;
        private System.Windows.Forms.ToolStripMenuItem mnuCopy;
        private System.Windows.Forms.ToolStripMenuItem mnuPaste;
        public System.Windows.Forms.ComboBox cboManila;
        public System.Windows.Forms.Timer timTimer;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.TextBox txtTekiyo;
        public System.Windows.Forms.ComboBox cboMitsumori;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Button cmdNewNo;
        public System.Windows.Forms.DateTimePicker datMtHakko;
        public System.Windows.Forms.DateTimePicker datMitsuYMD;
        public System.Windows.Forms.TextBox txtShipNo;
        public System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.RadioButton optCountry_1;
        public System.Windows.Forms.RadioButton optCountry_0;
        public System.Windows.Forms.Button cmdSansyo_1;
        public System.Windows.Forms.TextBox numOCode;
        public System.Windows.Forms.TextBox txtOName;
        public System.Windows.Forms.TextBox txtDoc;
        public System.Windows.Forms.TextBox txtVessel;
        public System.Windows.Forms.TextBox txtTokTanto;
        public System.Windows.Forms.TextBox txtTokName;
        public System.Windows.Forms.TextBox numTokCode;
        public System.Windows.Forms.TextBox txtIraiNo;
        public System.Windows.Forms.TextBox txtMitsuNo;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblCompany;
        public System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox txtComment4;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.TextBox txtType;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox txtComment1;
        public System.Windows.Forms.TextBox txtSerNo;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox txtComment2;
        public System.Windows.Forms.TextBox txtDwgNo;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox txtComment3;
        public System.Windows.Forms.TextBox txtAdditional;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.Button cmdSansyo_4;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.TextBox txtMaker;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Button cmdSansyo_3;
        public System.Windows.Forms.TextBox numSEQ;
        public System.Windows.Forms.TextBox numLastSEQ;
        public System.Windows.Forms.TextBox txtUnit;
        public System.Windows.Forms.Button cmdSansyo_2;
        public System.Windows.Forms.Button cmdSEQ_0;
        public System.Windows.Forms.Button cmdSEQ_1;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.ComboBox cboTanto;
        public System.Windows.Forms.TextBox txtMitsuNouki;
        public System.Windows.Forms.TextBox txtOrigin;
        public System.Windows.Forms.TextBox numMitsuYuko;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Button cmdSansyo_0;
        public System.Windows.Forms.Label lblJoutai;
        public System.Windows.Forms.Label lblModeTitle;
        public System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.TextBox numUKingaku;
        public System.Windows.Forms.RadioButton optMeisai_1;
        public System.Windows.Forms.RadioButton optMeisai_0;
        public System.Windows.Forms.TextBox numUSuryo;
        public System.Windows.Forms.TextBox numSKingaku;
        public System.Windows.Forms.TextBox numSSuryo;
        public System.Windows.Forms.TextBox numHKingaku;
        public System.Windows.Forms.TextBox numHSuryo;
        public System.Windows.Forms.TextBox numJKingaku;
        public System.Windows.Forms.TextBox numJSuryo;
        public System.Windows.Forms.TextBox numMKingaku;
        public System.Windows.Forms.TextBox numMSuryo;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.ToolStrip toolBar;
        public System.Windows.Forms.ToolStripButton cmdFunc_1;
        public System.Windows.Forms.ToolStripButton cmdFunc_2;
        public System.Windows.Forms.ToolStripButton cmdFunc_3;
        public System.Windows.Forms.ToolStripButton cmdFunc_4;
        public System.Windows.Forms.ToolStripButton cmdFunc_5;
        public System.Windows.Forms.ToolStripButton cmdFunc_6;
        public System.Windows.Forms.ToolStripButton cmdFunc_7;
        public System.Windows.Forms.ToolStripButton cmdFunc_8;
        public System.Windows.Forms.ToolStripButton cmdFunc_9;
        public System.Windows.Forms.ToolStripButton cmdFunc_10;
        public System.Windows.Forms.ToolStripButton cmdFunc_11;
        public System.Windows.Forms.ToolStripButton cmdFunc_12;
        public System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.DateTimePicker datPIHakko;
        public System.Windows.Forms.TextBox imDay;
        public System.Windows.Forms.Button Command1_0;
        public System.Windows.Forms.Button Command1_2;
        public System.Windows.Forms.Button Command1_1;
        public System.Windows.Forms.TextBox txtOrderNo;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label13;
        private System.Windows.Forms.DataGridViewTextBoxColumn index;
        private System.Windows.Forms.DataGridViewButtonColumn colActual;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCodeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNos;
        private System.Windows.Forms.DataGridViewButtonColumn colRef1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCase;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSeq;
        private System.Windows.Forms.DataGridViewTextBoxColumn colItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDesc;
        private System.Windows.Forms.DataGridViewButtonColumn colRef2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPartNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUnitPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOrigin;
        private System.Windows.Forms.DataGridViewTextBoxColumn colddtDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPup;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPPNet;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPurchAmt;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMargin;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOrderDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPurchDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSalesDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInvoiceDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVendor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colVendorCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn colInputDAte;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCode;
        private System.Windows.Forms.DataGridViewButtonColumn Delete;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

