﻿using clsDB;
using clswinApi;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBlibrary;
namespace EstimateProcessing
{
    public partial class frmBreakRow60 : Form
    {
        public String L_TYPE { get; set; }
        public String L_VALUE { get; set; }
        public String L_KNO { get; set; }
        public String L_SEQ { get; set; }
        public String L_NO { get; set; }
        public String G_VALUE { get; set; }
        public Boolean G_STATUS { get; set; }
        public frmBreakRow60()
        {
            InitializeComponent();
            txtVALUE_0.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_1.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_2.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_3.Enter += new EventHandler(txtValue_GotFocus);


            txtVALUE_0.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_1.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_2.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_3.Leave += new EventHandler(txtValue_LostFocus);

            txtMoto.Enter += new EventHandler(txtMoto_GotFocus);
            txtMoto.Leave += new EventHandler(txtMoto_LostFocus);

        }


        private void txtMoto_GotFocus(Object sender, EventArgs e)
        {

            TextBox txt = sender as TextBox;
            GotFocusSec(txt, "入力した内容です" );
        }
        private void txtMoto_LostFocus(Object sender, EventArgs e)
        {

            TextBox txt = sender as TextBox;
            LostFocusSec(txt);
        }

        private void txtValue_GotFocus(Object sender, EventArgs e)
        {

            TextBox txt = sender as TextBox;
            GotFocusSec(txt, "文字" + modHanbai.gstGUIDE_M001);
        }
        private void txtValue_LostFocus(Object sender, EventArgs e)
        {

            TextBox txt = sender as TextBox;
            LostFocusSec(txt);
        }
        private void GotFocusSec(Control vcCtrl, String vsMsg)
        {
            modSac_Com.stSetActiveControlB(vcCtrl, true);
            lblGuide.Text = vsMsg.Replace(" ", "");
        }

        private void LostFocusSec(Control vcCtrl)
        {
            modSac_Com.stSetActiveControlB(vcCtrl, false);
            lblGuide.Text = "";
        }
        private void EditClear(string p001 = "D")
        {
            G_STATUS = false;
            txtVALUE_0.Text = "";
            txtVALUE_1.Text = "";
            txtVALUE_2.Text = "";
            txtVALUE_3.Text = "";
        }

        private void frmBreakRow60_Load(object sender, EventArgs e)
        {

            string lsWork;
            int i;
            int j;
            List<string> strArray = new List<string>();
            lsWork = L_VALUE;
            txtMoto.Text = L_VALUE.Replace("*", "");
            if (Strings.InStr(L_VALUE, "*") > 0)
            {
                i = 0;
                lsWork = L_VALUE;
                var tmp = lsWork.Split('*');


                foreach (var item in tmp)
                {
                    if (item.ToString().Length > 60)
                    {
                        strArray.AddRange(Utils.SplitString(item, 60).ToList());
                    }
                    else
                    {
                        strArray.Add(item);
                    }
                }

            }
            else
                strArray = Utils.SplitString(lsWork, 60).ToList();



            try
            {
                txtVALUE_0.Text = strArray[0].ToString();
                txtVALUE_1.Text = strArray[1].ToString();
                txtVALUE_2.Text = strArray[2].ToString();
                txtVALUE_3.Text = strArray[3].ToString();

            }
            catch
            {
                //possibly there will be error due to length of the  strarray.
                //bypass this error
            }



        }

        private string MojiRenketu()
        {

            string lsWork = "";

            lsWork += txtVALUE_0.Text.Length > 0 ? txtVALUE_0.Text + "*" : "";
            lsWork += txtVALUE_1.Text.Length > 0 ? txtVALUE_1.Text + "*" : "";
            lsWork += txtVALUE_2.Text.Length > 0 ? txtVALUE_2.Text + "*" : "";
            lsWork += txtVALUE_3.Text.Length > 0 ? txtVALUE_3.Text + "*" : "";

            if (lsWork.Length > 0)
            {
                lsWork = lsWork.Substring(0, lsWork.Length - 1);
            }

            string strSQL = "";
            string strMsg = "";

            DBService db = new DBService();

   

                strSQL = "UPDATE OrdH_Dat ";

                switch (L_TYPE)
                {
                    case "ORIGIN":
                        {
                            strSQL += "  SET OrdH_012    ='" + lsWork + "'";
                            break;
                        }
                    case "Delivery Time":
                        {
                            strSQL += "  SET OrdH_011    ='" + lsWork + "'";
                            break;
                        }
                    case "NOTE":
                        {
                            strSQL += "  SET OrdH_014    ='" + lsWork + "'";
                            break;
                        }
                    default:
                        break;
                }

                strSQL += "  ,OrdH_Update    ='" + DateTime.Now + "'";
                strSQL += "  ,OrdH_WsNo    ='" + StaticWinApi.Wsnumber + "'";

                if (db.ExecuteSql(strSQL, out strMsg) == false)
                {
                    db.RollbackTran();
                    return "";
                }
       

            return lsWork;


        }

        private void cmdFunc_11_Click(object sender, EventArgs e)
        {
            G_STATUS = false;
            this.Close();
        }

        private void cmdFunc_12_Click(object sender, EventArgs e)
        {
            G_STATUS = true;
            G_VALUE = MojiRenketu();
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}
