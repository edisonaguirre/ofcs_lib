﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBlibrary;

namespace EstimateProcessing
{
    public partial class frmMITMR010S : Form
    {
        const int spdcol_IraiNo = 1;      // 客先依頼番号
        const int spdcol_MNo = 2;      // 見積№
        const int spdcol_MDate = 3;      // 見積日付
        const int spdcol_TName = 4;      // 得意先名
        const int spdcol_Vessel = 5;      // 船名

        private string L_MNO;           // 見積№
        private string L_IRAINO;           // 客先依頼番号
        private string getKNo;           // 契約№

        public frmMITMR010S()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmMITMR010S_Load(object sender, EventArgs e)
        {

        }
        private void EditClear()
        {

            txtIraiNo.Clear();
            txtMNo.Clear();
            datMDate.Format = DateTimePickerFormat.Custom;
            datMDate.CustomFormat = " ";
            datMDate.Text = " ";
            txtTName.Clear();
        }

        private void Label0_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void cmdFunc_1_Click(object sender, EventArgs e)
        {

        }

        private void cmdFunc_0_Click(object sender, EventArgs e)
        {

        }

        private void cmdFunc_10_Click(object sender, EventArgs e)
        {

        }

        private void cmdFunc_12_Click(object sender, EventArgs e)
        {

        }

        private void datMDate_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
