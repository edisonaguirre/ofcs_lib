﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBlibrary;
using clsDB;
using clswinApi;
using Microsoft.VisualBasic;
namespace EstimateProcessing
{
    public  partial class frmBreakRow34 : Form
    {
        public String L_VALUE { get; set; }
        public String L_KNO { get; set; }
        public String L_SEQ { get; set; }
        public String L_NO { get; set; }
        public String G_VALUE { get; set; }
        public Boolean G_STATUS { get; set; }
        public frmBreakRow34()
        {
      

            InitializeComponent();
            txtVALUE_0.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_1.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_2.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_3.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_4.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_5.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_6.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_7.Enter += new EventHandler(txtValue_GotFocus);
            txtVALUE_8.Enter += new EventHandler(txtValue_GotFocus);

            txtVALUE_0.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_1.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_2.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_3.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_4.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_5.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_6.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_7.Leave += new EventHandler(txtValue_LostFocus);
            txtVALUE_8.Leave += new EventHandler(txtValue_LostFocus);


        }

        private void txtValue_GotFocus(Object sender,EventArgs e)
        {

            TextBox txt = sender as TextBox;
            GotFocusSec(txt, "文字" + modHanbai.gstGUIDE_M001);
        }
        private void txtValue_LostFocus(Object sender, EventArgs e)
        {

            TextBox txt = sender as TextBox;
            LostFocusSec(txt);
        }



        public void frmBreakRow34_Load(object sender, EventArgs e)
        {

            
            string lsWork;
            int i;
            int j;
            List<string> strArray = new List<string>();
            lsWork = L_VALUE;
            txtMoto.Text = L_VALUE.Replace("*", "");
            if (Strings.InStr(L_VALUE, "*") > 0)
            {
                i = 0;
                lsWork = L_VALUE;
                var tmp = lsWork.Split('*');


                foreach (var item in tmp)
                {
                    if(item.ToString().Length>34)
                    {
                        strArray.AddRange(Utils.SplitString(item, 34).ToList());                        
                    }
                    else
                    {
                        strArray.Add(item);
                    }
                }

            }
            else
                strArray = Utils.SplitString(lsWork, 34).ToList();

          

            try
            {
                txtVALUE_0.Text = strArray[0].ToString();
                txtVALUE_1.Text = strArray[1].ToString();
                txtVALUE_2.Text = strArray[2].ToString();
                txtVALUE_3.Text = strArray[3].ToString();
                txtVALUE_4.Text = strArray[4].ToString();
                txtVALUE_5.Text = strArray[5].ToString();
                txtVALUE_6.Text = strArray[6].ToString();
                txtVALUE_7.Text = strArray[7].ToString();
                txtVALUE_8.Text = strArray[8].ToString();
            }
            catch
            {
                //possibly there will be error due to length of the  strarray.
                //bypass this error
            }

        }

        private void GotFocusSec(Control vcCtrl ,String vsMsg)
        {
            modSac_Com.stSetActiveControlB(vcCtrl, true);
            lblGuide.Text = vsMsg.Replace(" ", "");
        }

        private void LostFocusSec(Control vcCtrl)
        {
            modSac_Com.stSetActiveControlB(vcCtrl, false);
            lblGuide.Text= "";
        }

        private string MojiRenketu()
        {

            string lsWork = "";

            lsWork += txtVALUE_0.Text.Length > 0 ? txtVALUE_0.Text + "*": "";
            lsWork += txtVALUE_1.Text.Length > 0 ? txtVALUE_1.Text + "*" : "";
            lsWork += txtVALUE_2.Text.Length > 0 ? txtVALUE_2.Text + "*" : "";
            lsWork += txtVALUE_3.Text.Length > 0 ? txtVALUE_3.Text + "*" : "";
            lsWork += txtVALUE_4.Text.Length > 0 ? txtVALUE_4.Text + "*" : "";
            lsWork += txtVALUE_5.Text.Length > 0 ? txtVALUE_5.Text + "*" : "";
            lsWork += txtVALUE_6.Text.Length > 0 ? txtVALUE_6.Text + "*" : "";
            lsWork += txtVALUE_7.Text.Length > 0 ? txtVALUE_7.Text + "*" : "";
            lsWork += txtVALUE_8.Text.Length > 0 ? txtVALUE_8.Text + "*" : "";
            
            if(lsWork.Length>0)
            {
                lsWork= lsWork.Substring(0, lsWork.Length - 1);
            }

            string strSQL = "";
            string strMsg= "";

            DBService db = new DBService();

            db.BeginTrans();
            if(Utils.Mid(L_KNO,3,1)=="E")
            {
                strSQL = "UPDATE OrdD_Dat SET ";
                strSQL = strSQL + " OrdD_005 = '" + modCommon.EditSQLAddSQuot(lsWork) + "'";
                strSQL = strSQL + " WHERE OrdD_002 = '" + modCommon.EditSQLAddSQuot(L_KNO) + "'";
                strSQL = strSQL + " AND   OrdD_003 = " + L_SEQ;
                strSQL = strSQL + " AND   OrdD_004 = " + L_NO;
                if (db.ExecuteSql(strSQL, out strMsg)== false)
                {
                    db.RollbackTran();
                    return "";
                }
            }
            else
            {
                strSQL = "UPDATE JyuD_Dat SET ";
                strSQL = strSQL + " JyuD_005 = '" + modCommon.EditSQLAddSQuot(lsWork) + "'";
                strSQL = strSQL + " WHERE JyuD_001 = '" + modCommon.EditSQLAddSQuot(L_KNO) + "'";
                strSQL = strSQL + " AND   JyuD_003 = " + L_SEQ;
                strSQL = strSQL + " AND   JyuD_004 = " + L_NO;
                if (db.ExecuteSql(strSQL, out strMsg) == false)
                {
                    db.RollbackTran();
                    return "";
                }


                strSQL = "UPDATE HatD_Dat SET ";
                strSQL = strSQL + " HatD_009 = '" + modCommon.EditSQLAddSQuot(lsWork) + "'";
                strSQL = strSQL + " WHERE HatD_001 = '" + modCommon.EditSQLAddSQuot(L_KNO) + "'";
                strSQL = strSQL + " AND   HatD_002 = " + L_SEQ;
                strSQL = strSQL + " AND   HatD_003 = " + L_NO;
                if (db.ExecuteSql(strSQL, out strMsg) == false)
                {
                    db.RollbackTran();
                    return "";
                }

                strSQL = "UPDATE KnpD_Dat SET ";
                strSQL = strSQL + " KnpD_007 = '" + modCommon.EditSQLAddSQuot(lsWork) + "'";
                strSQL = strSQL + " WHERE KnpD_001 = '" + modCommon.EditSQLAddSQuot(L_KNO) + "'";
                strSQL = strSQL + " AND   KnpD_002 = " + L_SEQ;
                strSQL = strSQL + " AND   KnpD_003 = " + L_NO;
                if (db.ExecuteSql(strSQL, out strMsg) == false)
                {
                    db.RollbackTran();
                    return "";
                }


      

            }
            db.CommitTrans();
            return lsWork;


        }

        private void EditClear(string p001 = "D")
        {
            G_STATUS = false;
            txtVALUE_0.Text = "";
            txtVALUE_1.Text =   "";
            txtVALUE_2.Text =   "";
            txtVALUE_3.Text =   "";
            txtVALUE_4.Text =   "";
            txtVALUE_5.Text =   "";
            txtVALUE_6.Text =   "";
            txtVALUE_7.Text =   "";
            txtVALUE_8.Text =   "";
        }

        private void cmdFunc_11_Click(object sender, EventArgs e)
        {
            G_STATUS = false;
            this.Close();
        }

        private void cmdFunc_12_Click(object sender, EventArgs e)
        {
            G_STATUS = true;
            G_VALUE = MojiRenketu();
            this.Close();
        }

        private void Frame__Jyouken_Enter(object sender, EventArgs e)
        {

        }

        private void txtVALUE_0_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMoto_Enter(object sender, EventArgs e)
        {
            GotFocusSec(txtMoto, "入力した内容です");
        }

        private void txtMoto_Leave(object sender, EventArgs e)
        {
            LostFocusSec(txtMoto);
        }
    }
}

