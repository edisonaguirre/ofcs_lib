﻿namespace EstimateProcessing
{
    partial class frmBreakRow60
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmdFunc_12 = new System.Windows.Forms.Button();
            this.cmdFunc_11 = new System.Windows.Forms.Button();
            this.txtVALUE_3 = new System.Windows.Forms.TextBox();
            this.txtVALUE_2 = new System.Windows.Forms.TextBox();
            this.txtVALUE_1 = new System.Windows.Forms.TextBox();
            this.txtVALUE_0 = new System.Windows.Forms.TextBox();
            this.txtMemori = new System.Windows.Forms.TextBox();
            this.txtMoto = new System.Windows.Forms.TextBox();
            this.imText1 = new System.Windows.Forms.TextBox();
            this.lblGuide = new System.Windows.Forms.Label();
            this.Frame__Jyouken = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Frame__Jyouken.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("MS Mincho", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(22, -1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(401, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "改行・文字数制御";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_12.Font = new System.Drawing.Font("MS Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFunc_12.Location = new System.Drawing.Point(526, 12);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(89, 36);
            this.cmdFunc_12.TabIndex = 4;
            this.cmdFunc_12.Text = "確定(F12)";
            this.cmdFunc_12.UseVisualStyleBackColor = false;
            this.cmdFunc_12.Click += new System.EventHandler(this.cmdFunc_12_Click);
            // 
            // cmdFunc_11
            // 
            this.cmdFunc_11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_11.Font = new System.Drawing.Font("MS Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFunc_11.Location = new System.Drawing.Point(437, 12);
            this.cmdFunc_11.Name = "cmdFunc_11";
            this.cmdFunc_11.Size = new System.Drawing.Size(87, 36);
            this.cmdFunc_11.TabIndex = 3;
            this.cmdFunc_11.Text = "戻る(F11)";
            this.cmdFunc_11.UseVisualStyleBackColor = false;
            this.cmdFunc_11.Click += new System.EventHandler(this.cmdFunc_11_Click);
            // 
            // txtVALUE_3
            // 
            this.txtVALUE_3.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_3.Location = new System.Drawing.Point(71, 254);
            this.txtVALUE_3.MaxLength = 60;
            this.txtVALUE_3.Name = "txtVALUE_3";
            this.txtVALUE_3.Size = new System.Drawing.Size(546, 24);
            this.txtVALUE_3.TabIndex = 6;
            // 
            // txtVALUE_2
            // 
            this.txtVALUE_2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_2.Location = new System.Drawing.Point(71, 228);
            this.txtVALUE_2.MaxLength = 60;
            this.txtVALUE_2.Name = "txtVALUE_2";
            this.txtVALUE_2.Size = new System.Drawing.Size(546, 24);
            this.txtVALUE_2.TabIndex = 5;
            // 
            // txtVALUE_1
            // 
            this.txtVALUE_1.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_1.Location = new System.Drawing.Point(71, 202);
            this.txtVALUE_1.MaxLength = 60;
            this.txtVALUE_1.Name = "txtVALUE_1";
            this.txtVALUE_1.Size = new System.Drawing.Size(546, 24);
            this.txtVALUE_1.TabIndex = 4;
            // 
            // txtVALUE_0
            // 
            this.txtVALUE_0.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_0.Location = new System.Drawing.Point(71, 176);
            this.txtVALUE_0.MaxLength = 60;
            this.txtVALUE_0.Name = "txtVALUE_0";
            this.txtVALUE_0.Size = new System.Drawing.Size(546, 24);
            this.txtVALUE_0.TabIndex = 3;
            // 
            // txtMemori
            // 
            this.txtMemori.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtMemori.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMemori.Location = new System.Drawing.Point(71, 150);
            this.txtMemori.Name = "txtMemori";
            this.txtMemori.Size = new System.Drawing.Size(546, 24);
            this.txtMemori.TabIndex = 2;
            this.txtMemori.Text = "---------------1---------------2---------------3---------------4---------------5-" +
    "--------------6--------";
            // 
            // txtMoto
            // 
            this.txtMoto.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoto.Location = new System.Drawing.Point(71, 60);
            this.txtMoto.Name = "txtMoto";
            this.txtMoto.Size = new System.Drawing.Size(546, 24);
            this.txtMoto.TabIndex = 1;
            // 
            // imText1
            // 
            this.imText1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.imText1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.imText1.Location = new System.Drawing.Point(71, 30);
            this.imText1.Name = "imText1";
            this.imText1.Size = new System.Drawing.Size(546, 24);
            this.imText1.TabIndex = 0;
            this.imText1.Text = "---------------1---------------2---------------3---------------4---------------5-" +
    "--------------6--------";
            // 
            // lblGuide
            // 
            this.lblGuide.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblGuide.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblGuide.Font = new System.Drawing.Font("MS Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGuide.ForeColor = System.Drawing.Color.White;
            this.lblGuide.Location = new System.Drawing.Point(26, 369);
            this.lblGuide.Name = "lblGuide";
            this.lblGuide.Size = new System.Drawing.Size(640, 23);
            this.lblGuide.TabIndex = 6;
            this.lblGuide.Text = "ｺｰﾄﾞを入力して下さい。";
            this.lblGuide.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame__Jyouken
            // 
            this.Frame__Jyouken.Controls.Add(this.label5);
            this.Frame__Jyouken.Controls.Add(this.label4);
            this.Frame__Jyouken.Controls.Add(this.label3);
            this.Frame__Jyouken.Controls.Add(this.label2);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_3);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_2);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_1);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_0);
            this.Frame__Jyouken.Controls.Add(this.txtMemori);
            this.Frame__Jyouken.Controls.Add(this.txtMoto);
            this.Frame__Jyouken.Controls.Add(this.imText1);
            this.Frame__Jyouken.Location = new System.Drawing.Point(26, 65);
            this.Frame__Jyouken.Name = "Frame__Jyouken";
            this.Frame__Jyouken.Size = new System.Drawing.Size(640, 301);
            this.Frame__Jyouken.TabIndex = 5;
            this.Frame__Jyouken.TabStop = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(-10, 255);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 24);
            this.label5.TabIndex = 10;
            this.label5.Text = "4行目";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(-19, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "3行目";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(-16, 203);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "2行目";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(-13, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 23);
            this.label2.TabIndex = 7;
            this.label2.Text = "1行目";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // frmBreakRow60
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 416);
            this.Controls.Add(this.lblGuide);
            this.Controls.Add(this.Frame__Jyouken);
            this.Controls.Add(this.cmdFunc_12);
            this.Controls.Add(this.cmdFunc_11);
            this.Controls.Add(this.label1);
            this.Name = "frmBreakRow60";
            this.Text = "改行・文字数制御 (frmBreakRow60)";
            this.Load += new System.EventHandler(this.frmBreakRow60_Load);
            this.Frame__Jyouken.ResumeLayout(false);
            this.Frame__Jyouken.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdFunc_12;
        public System.Windows.Forms.Button cmdFunc_11;
        public System.Windows.Forms.TextBox txtVALUE_3;
        public System.Windows.Forms.TextBox txtVALUE_2;
        public System.Windows.Forms.TextBox txtVALUE_1;
        public System.Windows.Forms.TextBox txtVALUE_0;
        public System.Windows.Forms.TextBox txtMemori;
        public System.Windows.Forms.TextBox txtMoto;
        public System.Windows.Forms.TextBox imText1;
        public System.Windows.Forms.Label lblGuide;
        public System.Windows.Forms.GroupBox Frame__Jyouken;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
    }
}