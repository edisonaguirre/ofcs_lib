﻿namespace EstimateProcessing
{
    partial class frmRefSirMst2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdFunc_10 = new System.Windows.Forms.Button();
            this.cmdFunc_11 = new System.Windows.Forms.Button();
            this.cmdFunc_12 = new System.Windows.Forms.Button();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.txtSerch_2 = new System.Windows.Forms.TextBox();
            this.txtSerch_1 = new System.Windows.Forms.TextBox();
            this.txtSerch_0 = new System.Windows.Forms.TextBox();
            this.numSerch = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblGuide = new System.Windows.Forms.Label();
            this.spdList = new System.Windows.Forms.DataGridView();
            this.Sir_001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sir_003 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sir_004 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sir_006 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdList)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(18, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(226, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "仕入先マスタ参照";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdFunc_10
            // 
            this.cmdFunc_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_10.Location = new System.Drawing.Point(471, 35);
            this.cmdFunc_10.Name = "cmdFunc_10";
            this.cmdFunc_10.Size = new System.Drawing.Size(96, 37);
            this.cmdFunc_10.TabIndex = 14;
            this.cmdFunc_10.TabStop = false;
            this.cmdFunc_10.Text = "登録(F10)";
            this.cmdFunc_10.UseVisualStyleBackColor = false;
            this.cmdFunc_10.Click += new System.EventHandler(this.cmdFunc_10_Click);
            // 
            // cmdFunc_11
            // 
            this.cmdFunc_11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_11.Location = new System.Drawing.Point(573, 35);
            this.cmdFunc_11.Name = "cmdFunc_11";
            this.cmdFunc_11.Size = new System.Drawing.Size(96, 37);
            this.cmdFunc_11.TabIndex = 7;
            this.cmdFunc_11.TabStop = false;
            this.cmdFunc_11.Text = "取消(F11)";
            this.cmdFunc_11.UseVisualStyleBackColor = false;
            this.cmdFunc_11.Click += new System.EventHandler(this.cmdFunc_11_Click);
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_12.Location = new System.Drawing.Point(675, 35);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(96, 37);
            this.cmdFunc_12.TabIndex = 6;
            this.cmdFunc_12.TabStop = false;
            this.cmdFunc_12.Text = "確定(F12)";
            this.cmdFunc_12.UseVisualStyleBackColor = false;
            this.cmdFunc_12.Click += new System.EventHandler(this.cmdFunc_12_Click);
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Frame1.Controls.Add(this.txtSerch_2);
            this.Frame1.Controls.Add(this.txtSerch_1);
            this.Frame1.Controls.Add(this.txtSerch_0);
            this.Frame1.Controls.Add(this.numSerch);
            this.Frame1.Controls.Add(this.label5);
            this.Frame1.Controls.Add(this.label4);
            this.Frame1.Controls.Add(this.label3);
            this.Frame1.Controls.Add(this.label2);
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Frame1.Location = new System.Drawing.Point(12, 78);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(759, 99);
            this.Frame1.TabIndex = 0;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "曖昧検索";
            // 
            // txtSerch_2
            // 
            this.txtSerch_2.Location = new System.Drawing.Point(541, 52);
            this.txtSerch_2.Name = "txtSerch_2";
            this.txtSerch_2.Size = new System.Drawing.Size(209, 24);
            this.txtSerch_2.TabIndex = 3;
            // 
            // txtSerch_1
            // 
            this.txtSerch_1.Location = new System.Drawing.Point(352, 52);
            this.txtSerch_1.Name = "txtSerch_1";
            this.txtSerch_1.Size = new System.Drawing.Size(183, 24);
            this.txtSerch_1.TabIndex = 2;
            // 
            // txtSerch_0
            // 
            this.txtSerch_0.Location = new System.Drawing.Point(85, 52);
            this.txtSerch_0.Name = "txtSerch_0";
            this.txtSerch_0.Size = new System.Drawing.Size(261, 24);
            this.txtSerch_0.TabIndex = 1;
            // 
            // numSerch
            // 
            this.numSerch.Location = new System.Drawing.Point(11, 51);
            this.numSerch.Name = "numSerch";
            this.numSerch.ReadOnly = true;
            this.numSerch.Size = new System.Drawing.Size(68, 24);
            this.numSerch.TabIndex = 0;
            this.numSerch.TextChanged += new System.EventHandler(this.numSerch_TextChanged);
            this.numSerch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numSerch_KeyPress);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(542, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(208, 26);
            this.label5.TabIndex = 3;
            this.label5.Text = "仕入先名称カナ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(352, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(183, 26);
            this.label4.TabIndex = 2;
            this.label4.Text = "NAME OF SUPPLIER2";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(85, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(261, 27);
            this.label3.TabIndex = 1;
            this.label3.Text = "NAME OF SUPPLIER1";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(11, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 26);
            this.label2.TabIndex = 0;
            this.label2.Text = "CODE NO.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(12, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(428, 31);
            this.label6.TabIndex = 5;
            this.label6.Text = "※ ヘッダー行をクリックすると並び替えをします(昇順・降順)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGuide
            // 
            this.lblGuide.BackColor = System.Drawing.Color.Blue;
            this.lblGuide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGuide.ForeColor = System.Drawing.Color.White;
            this.lblGuide.Location = new System.Drawing.Point(14, 580);
            this.lblGuide.Name = "lblGuide";
            this.lblGuide.Size = new System.Drawing.Size(757, 29);
            this.lblGuide.TabIndex = 7;
            this.lblGuide.Text = "ｺｰﾄﾞを入力して下さい。";
            this.lblGuide.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // spdList
            // 
            this.spdList.AllowUserToAddRows = false;
            this.spdList.AllowUserToDeleteRows = false;
            this.spdList.AllowUserToResizeColumns = false;
            this.spdList.AllowUserToResizeRows = false;
            this.spdList.BackgroundColor = System.Drawing.Color.White;
            this.spdList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spdList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Sir_001,
            this.Sir_003,
            this.Sir_004,
            this.Sir_006});
            this.spdList.Location = new System.Drawing.Point(12, 224);
            this.spdList.MultiSelect = false;
            this.spdList.Name = "spdList";
            this.spdList.ReadOnly = true;
            this.spdList.RowHeadersVisible = false;
            this.spdList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.spdList.Size = new System.Drawing.Size(759, 341);
            this.spdList.TabIndex = 4;
            // 
            // Sir_001
            // 
            this.Sir_001.DataPropertyName = "Sir_001";
            dataGridViewCellStyle1.Format = "#";
            dataGridViewCellStyle1.NullValue = null;
            this.Sir_001.DefaultCellStyle = dataGridViewCellStyle1;
            this.Sir_001.HeaderText = "Code No.";
            this.Sir_001.Name = "Sir_001";
            this.Sir_001.ReadOnly = true;
            // 
            // Sir_003
            // 
            this.Sir_003.DataPropertyName = "Sir_003";
            this.Sir_003.HeaderText = "Name of Supplier1";
            this.Sir_003.Name = "Sir_003";
            this.Sir_003.ReadOnly = true;
            this.Sir_003.Width = 250;
            // 
            // Sir_004
            // 
            this.Sir_004.DataPropertyName = "Sir_004";
            this.Sir_004.HeaderText = "Name of Supplier2";
            this.Sir_004.Name = "Sir_004";
            this.Sir_004.ReadOnly = true;
            this.Sir_004.Width = 200;
            // 
            // Sir_006
            // 
            this.Sir_006.DataPropertyName = "Sir_006";
            this.Sir_006.HeaderText = "Supplier Name Kana";
            this.Sir_006.Name = "Sir_006";
            this.Sir_006.ReadOnly = true;
            this.Sir_006.Width = 200;
            // 
            // frmRefSirMst2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(783, 675);
            this.Controls.Add(this.spdList);
            this.Controls.Add(this.lblGuide);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cmdFunc_12);
            this.Controls.Add(this.cmdFunc_11);
            this.Controls.Add(this.cmdFunc_10);
            this.Controls.Add(this.label1);
            this.Name = "frmRefSirMst2";
            this.Text = "仕入先マスタ参照";
            this.Load += new System.EventHandler(this.frmRefSirMst2_Load);
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.TextBox txtSerch_1;
        public System.Windows.Forms.TextBox txtSerch_0;
        public System.Windows.Forms.TextBox numSerch;
        public System.Windows.Forms.TextBox txtSerch_2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdFunc_10;
        public System.Windows.Forms.Button cmdFunc_11;
        public System.Windows.Forms.Button cmdFunc_12;
        public System.Windows.Forms.GroupBox Frame1;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label lblGuide;
        public System.Windows.Forms.DataGridView spdList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sir_001;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sir_003;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sir_004;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sir_006;
    }
}