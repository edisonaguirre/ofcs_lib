﻿using clsDB;
using clswinApi;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBlibrary;

namespace EstimateProcessing
{
    public partial class frmMITMR010 : Form
    {

        private int EditMode;
        private Boolean IfEditFlag;
        private Control BefCtl;
        private Boolean newrowcreated;
        bool notlastColumn = true; //class level variable--- to check either last column is reached or not

        public struct TypeArray
        {
           public int Deltime;
           public string ItemNo;
        }
        private string[] strArrData;

        public frmMITMR010()
        {
            InitializeComponent();



            numSEQ.KeyPress += new KeyPressEventHandler(numSEQ_KeyPress);
            txtMitsuNo.KeyDown += new KeyEventHandler(txtMitsuNo_KeyUp);
            txtIraiNo.KeyDown += new KeyEventHandler(txtIraiNo_KeyUp);
            var txtbx = GetAll(this, typeof(TextBox));
            foreach (TextBox tb in txtbx)
            {
                tb.KeyDown += new KeyEventHandler(TxtBoxOnEnter);
            }

            


            spdData.Columns[modMITMR010.spdcol_MKinG].DefaultCellStyle.Format = "#,##0.00";


        }
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        private void TxtBoxOnEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {

                TextBox tb = sender as TextBox;
                switch (tb.Name)
                {
                    case "txtUnit":
                        {
                            cmdSansyo_2_Click(sender, null);
                            this.SelectNextControl((TextBox)sender, true, true, true, true);
                            break;
                        }
                    case "txtMaker":
                        {
                            cmdSansyo_3_Click(sender, null);
                            this.SelectNextControl((TextBox)sender, true, true, true, true);
                            break;
                        }
                    case "txtType":
                        {
                            cmdSansyo_4_Click(sender, null);
                            this.SelectNextControl((TextBox)sender, true, true, true, true);
                            break;
                        }
                    case "imDay":
                        {
                            string delivery = imDay.Text;
                            for (int i = 0; i < spdData.Rows.Count - 1; i++)
                            {
                                if (spdData.Rows[i].Cells[modMITMR010.spdcol_MTani].Value?.ToString() != "")
                                {
                                    spdData.Rows[i].Cells[modMITMR010.spdcol_DelTime].Value = delivery;
                                }
                            }
                            this.SelectNextControl((TextBox)sender, true, true, true, true);
                            break;

                        }


                    default:
                        {
                            this.SelectNextControl((TextBox)sender, true, true, true, true);
                            break;
                        }

                }
            }
        }




        #region Functiontions

        private void loadThis()
        {
            string sqltext;
            DspMopdeProc();

            clsMITMR010.stmSetItemDivid(this);
            cmdFunc_1.Text = "(F1)" + Environment.NewLine + "ADD.";//            'INSERT 2014/02/04 AOKI
            cmdFunc_2.Text = "(F2)" + Environment.NewLine + "REVISE";  //   'INSERT 2014/02/04 AOKI
            cmdFunc_3.Text = "(F3)" + Environment.NewLine + "DELETE";

            cmdFunc_4.Text = "(F4)" + Environment.NewLine + "COPY";//        'INSERT 2017/03/29 AOKI

            cmdFunc_5.Text = "(F5)ISSUE" + Environment.NewLine + "QUOTATION";//

            cmdFunc_6.Text = "(F6)INVOICE" + Environment.NewLine + "PROFORMA";//   'INSERT 2014/06/12 AOKI

            cmdFunc_7.Text = "(F7)PAC ++" + Environment.NewLine + "DEL CHARGE";//    'INSERT 2016/05/10 AOKI
            cmdFunc_8.Text = "(F8)CONTROL" + Environment.NewLine + "LINE FEED";

            cmdFunc_9.Text = "(F9)" + Environment.NewLine + "LINE COPY";

            cmdFunc_10.Text = "(F10)" + Environment.NewLine + "SAVE";

            cmdFunc_11.Text = "(F11)" + Environment.NewLine + "DELETE ALL";

            cmdFunc_12.Text = "(F12)" + Environment.NewLine + "QUIT";

            modHanbai.GetCompanyInfo();

            lblCompany.Text = modHanbai.Company.NAME;
            statclsMITMR010.FlgF6 = false;
            EditClear();
            datMitsuYMD.Focus();
            

        }
        private void EditClear(int viMode = 0)
        {
            if (viMode == 0)
            {
                // todo : add timer events

                timTimer.Enabled = false;
                lblJoutai.Visible = false;
                setDateTimePickerBlank(datMitsuYMD, DateTime.Now);
                setDateTimePickerBlank(datMtHakko, DateTime.MinValue); //means to set this as blank
                setDateTimePickerBlank(datPIHakko, DateTime.MinValue); //means to set this as blank
                txtIraiNo.Clear();
                txtOrderNo.Clear();
                txtMitsuNo.Text = "";
                numTokCode.Text = "0";
                numOCode.Text = "0";
                optCountry_1.Checked = true;
                txtTokName.Text = "";
                txtTokTanto.Text = "";
                txtVessel.Text = "";
                txtDoc.Text = "";
                txtShipNo.Text = "";
                txtOName.Text = "";
                txtOrigin.Text = "";
                cboCountry.SelectedIndex = -1;
                cboManila.SelectedIndex = -1;
                txtTekiyo.Clear();
                numMitsuYuko.Clear();
                numLastSEQ.Text = "0";
                numSEQ.Clear();
            }
            txtUnit.Text = ""; //       'UNIT
            txtMaker.Text = "";//'MAKER
            txtType.Text = "";//'TYPE
            txtSerNo.Clear();//'SER/NO
            txtDwgNo.Clear();// 'DWG NO
            txtAdditional.Clear(); //'ADDITIONAL DETAILS
            txtComment1.Clear(); //'COMMENT1
            txtComment2.Clear();// 'COMMENT2
            txtComment3.Clear();// 'COMMENT3
            txtComment4.Clear();// 'COMMENT4

            numMSuryo.Text = "0";//  'Œ©Ï”—Ê
            numMKingaku.Text = "0";//'Œ©Ï‹àŠz
            numJSuryo.Text = "0";//'Žó’”—Ê
            numJKingaku.Text = "0";//'Žó’‹àŠz
            numHSuryo.Text = "0";//'”­’”—Ê
            numHKingaku.Text = "0";//'”­’‹àŠz
            numSSuryo.Text = "0";//'Žd“ü”—Ê
            numSKingaku.Text = "0";//'Žd“ü‹àŠz
            numUSuryo.Text = "0";//'”„ã”—Ê
            numUKingaku.Text = "0";//'”„ã‹àŠz
            optMeisai_0.Checked = true;

            spdData.DataSource = null;
            spdData.Refresh();
            cmdFunc_1.Enabled = true;//            'INSERT 2014/02/24 AOKI
            cmdFunc_2.Enabled = true;//            'INSERT 2014/02/24 AOKI
            cmdFunc_3.Enabled = true;//            'INSERT 2014/02/24 AOKI
            cmdFunc_4.Enabled = true;//            'INSERT 2017/07/19 AOKI
            cmdFunc_5.Enabled = true;//            'INSERT 2014/02/24 AOKI
            cmdFunc_6.Enabled = true;
            clearGridView();

            //          'INSERT 2014/06/12 AOKI



        }
        private void setDateTimePickerBlank(DateTimePicker dtpicker, DateTime value)
        {
            if (value == DateTime.MinValue)
            {
                dtpicker.Format = DateTimePickerFormat.Custom;
                dtpicker.CustomFormat = " ";
            }
            else
            {
                dtpicker.Format = DateTimePickerFormat.Short;
                dtpicker.Value = value;
            }
        }

        private void DspMopdeProc()
        {
            EditMode = 0;
            lblModeTitle.Text = modSac_Com.stStrDSPNM;

            txtMitsuNo.ReadOnly = false;
            cmdNewNo.Visible = false;

        }

        #endregion

        private void frmMITMR010_Load_1(object sender, EventArgs e)
        {

            loadThis();

        }

        private void clearGridView()
        {
            spdData.Rows.Clear();
            spdData.Rows.Add();
        }


        private void spdData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if ((spdData.Columns[spdData.CurrentCell.ColumnIndex].Index == modMITMR010.spdcol_MSuryo) ||
                (spdData.Columns[spdData.CurrentCell.ColumnIndex].Index == modMITMR010.spdcol_MTanka))
            {
                /// Workarround para que estando editando en las columnas del grid Clave y Nombre
                /// podamos detectar cuando se dio F4 para lanzar el dialogo de busqueda del
                /// articulo.
                e.Control.KeyPress += new KeyPressEventHandler(spddata_Cells_keypress_acceptsnumericonly);
            }
        }

        private void spddata_Cells_keypress_acceptsnumericonly(object sender, KeyPressEventArgs e)
        {
            if ((spdData.Columns[spdData.CurrentCell.ColumnIndex].Index == modMITMR010.spdcol_MSuryo) ||
    (spdData.Columns[spdData.CurrentCell.ColumnIndex].Index == modMITMR010.spdcol_MTanka))
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }

                // only allow one decimal point
                if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
                {
                    e.Handled = true;
                }
            }
        }


        private void spdData_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0 && (e.ColumnIndex == 9 || e.ColumnIndex == 4 || e.ColumnIndex==30))
            {
                switch(e.ColumnIndex)
                {
                    case 4://name of supplier
                        {

                            frmRefSirMst2 frm = new frmRefSirMst2();
                            frm.ShowDialog(this);
                            if (frm.Status() == true)
                            {
                                spdData.Rows[e.RowIndex].Cells[e.ColumnIndex-1].Value = frm.STM_NAME1();
                                spdData.Rows[e.RowIndex].Cells[e.ColumnIndex - 2].Value = frm.STM_CODE();
                            }                        
                            break;
                        }
                    case 9://description
                        {
                            frmRefSyoTbl frm = new frmRefSyoTbl();
                            frm.ShowDialog(this);
                            if (frm.Status() == true)
                            {
                                spdData.Rows[e.RowIndex].Cells[e.ColumnIndex - 1].Value = frm.STM_HINMEI();
                                spdData.Rows[e.RowIndex].Cells[e.ColumnIndex +1 ].Value = frm.STM_HINBAN();
                                spdData.Rows[e.RowIndex].Cells[e.ColumnIndex + 3].Value = frm.STM_TANI();
                            }
                            break;

                        }
                    case 30:
                        {
                            if(spdData.Rows[e.RowIndex].Cells[e.ColumnIndex].Value!=null)
                            {
                                spdData.Rows.RemoveAt(e.RowIndex);                                
                            }
                            break;
                        }
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            saFrm_Sansyo906 frm = new saFrm_Sansyo906();
            frm.SETMODE(0);
            frm.ShowDialog(this);
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void cmdNewNo_Click(object sender, EventArgs e)
        {
            txtMitsuNo.Text = modMITMR010.GetNewEstimateNo(long.Parse(datMitsuYMD.Value.ToString("yyyyMMddHHmmss")));

            if (modMITMR010.UpdateEstimateNo(txtMitsuNo.Text) == false)
            {
                modSac_Com.ksExpMsgBox("Update failed", "E");
            }
            numSEQ.Text = "1";
            cmdFunc_1.Enabled = false;
            cmdFunc_2.Enabled = false;
            cmdFunc_3.Enabled = false;
            cmdFunc_4.Enabled = false;
            cmdFunc_5.Enabled = false;
            cmdFunc_6.Enabled = false;
            txtIraiNo.Focus();
        }

        private void cmdFunc_1_Click(object sender, EventArgs e)
        {
            AddModeProc();

        }

        private void AddModeProc()
        {
            EditMode = 1;
            lblModeTitle.Text = modSac_Com.stStrADDNM;
            lblModeTitle.BackColor = Color.LightGreen;
            txtMitsuNo.ReadOnly = true;
            txtMitsuNo.Text = "";
            cmdNewNo.Visible = true;
            cmdNewNo.Focus();



        }

        private void ModModeProc()
        {
            EditMode = 2;
            lblModeTitle.Text = modSac_Com.stStrMODNM;
            lblModeTitle.BackColor = Color.LightYellow;
            txtMitsuNo.ReadOnly = false;
            cmdNewNo.Visible = false;
            if (BefCtl != null)
            BefCtl.Focus();
        }
        
        private void DelModeProc()
        {
            EditMode = 3;
            lblModeTitle.Text = modSac_Com.stStrDELNM;
            lblModeTitle.BackColor = Color.Red;
            txtMitsuNo.ReadOnly = false;
            cmdNewNo.Visible = false;
            if (BefCtl != null)
                BefCtl.Focus();

        }
        private void DispModeProc()
        {
            EditMode = 0;
            lblModeTitle.Text = modSac_Com.stStrDSPNM;
            lblModeTitle.BackColor = Color.LightCyan;
            txtMitsuNo.ReadOnly = false;
            cmdNewNo.Visible = false;

        }



        private void CopyDataProc()
        {
            string SQLtxt;
            string ssText;
            int liCnt;

            string strMsg="";
            long lngErr;
            if(modCommon.EditSQLAddSQuot(txtMitsuNo.Text).Substring(2,1)!="E" ||  modCommon.EditSQLAddSQuot(txtMitsuNo.Text).Length!=9) 
            {
                modSac_Com.ksExpMsgBox(@"""Our Estimate No."" is invalid.", "W");
                txtMitsuNo.Focus();
                return;
            }

            if (modSac_Com.ksExpMsgBox(@"表示中の Our Estimate No. を全てコピーし、" + Environment.NewLine + "新しく見積データを作成します" + Environment.NewLine + Environment.NewLine + @"よろしいですか？", "Q") == (int)MsgBoxResult.Yes)
            {
                DBService db = new DBService();
                db.BeginTrans();
                if(modMITMR010.OrdHDatReadProc(modCommon.EditSQLAddSQuot(txtMitsuNo.Text),0)==false)
                {
                    db.RollbackTran();
                    modSac_Com.ksExpMsgBox(modHanbai.gstGUIDE_E002 + " ERR: " + strMsg, "E");
                    return;                    
                }
                SQLtxt = "";
                SQLtxt = SQLtxt + " SELECT COUNT(*) AS OM_CNT";
                SQLtxt = SQLtxt + " FROM   OrdM_Dat";
                SQLtxt = SQLtxt + " WHERE  OrdM_002 = '" + modCommon.EditSQLAddSQuot(txtMitsuNo.Text) + "'";
                DataTable dt = new DataTable();
                dt=db.GetDatafromDB(SQLtxt);

                if(dt.Rows.Count==0||Convert.ToUInt64(modHanbai.NCnvZ(dt.Rows[0][0].ToString()))<1 )
                {
                    db.RollbackTran();
                    modSac_Com.ksExpMsgBox(modHanbai.gstGUIDE_E002 + " ERR: " + strMsg, "E");
                    return;
                }
                else
                {
                    liCnt = Convert.ToInt32(modHanbai.NCnvZ(dt.Rows[0][0].ToString()));
                }

                datMitsuYMD.Value = DateTime.Now;
                datMitsuYMD.Format = DateTimePickerFormat.Custom;
                datMitsuYMD.CustomFormat = "yyyymmdd";
                ssText = modMITMR010.GetNewEstimateNo(long.Parse(datMitsuYMD.Value.ToString("yyyyMMddHHmmss")));

                if (modMITMR010.UpdateEstimateNo(ssText)==false)
                {
                    db.RollbackTran();
                    modSac_Com.ksExpMsgBox("Update failed.", "E");
                    return;
                }


                SQLtxt = "";
                SQLtxt = SQLtxt + " INSERT INTO OrdH_Dat ";
                SQLtxt = SQLtxt + " VALUES( NULL";
                SQLtxt = SQLtxt + "        ,NULL";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_003) + "'";
                SQLtxt = SQLtxt + "        ," + modMITMR010.OrdHDat[0].OrdH_0031;
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_0032) + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_004) + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_005) + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_006) + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_007) + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_008) + "'";
                SQLtxt = SQLtxt + "        ,NULL";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_0082) + "'";     // 'INSERT 2019/05/09 AOKI
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(ssText) + "'";
                
                SQLtxt = SQLtxt + "        ,'" + DateTime.Now.ToString("yyyy/MM/dd") + "'";    // 'INSERT 2018/07/31 AOKI
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_011) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_012) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_013) + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_014) + "'" ;
                SQLtxt = SQLtxt + "        ," + modMITMR010.OrdHDat[0].OrdH_015                                                         ;
                SQLtxt = SQLtxt + "        ," + modMITMR010.OrdHDat[0].OrdH_016                                                         ;
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_017) + "'" ;
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_018) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_019) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_020) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_021) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_022) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_023) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_024) + "'";
                SQLtxt = SQLtxt + "        ,'" +  modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_025) + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_026) + "'";
               
                SQLtxt = SQLtxt + "        ,0"                   ;   //'INSERT 2018/02/26 AOKI
                SQLtxt = SQLtxt + "        ,NULL"                ;
                SQLtxt = SQLtxt + "        ,NULL"                ;
                SQLtxt = SQLtxt + "        ,NULL"                ;
                SQLtxt = SQLtxt + "        ,NULL"                ;
                SQLtxt = SQLtxt + "        ,NULL"                ;
                SQLtxt = SQLtxt + "        ,NULL"                ;
                SQLtxt = SQLtxt + "        ,NULL"                ;
                SQLtxt = SQLtxt + "        ,NULL"                ;
                SQLtxt = SQLtxt + "        ," + modMITMR010.OrdHDat[0].OrdH_035;
                SQLtxt = SQLtxt + "        ,'" + DateTime.Now.ToShortDateString() + "'";
                SQLtxt = SQLtxt + "        ,NULL"                                      ;
                SQLtxt = SQLtxt + "        ,'" + clswinApi.StaticWinApi.Wsnumber + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_037) + "'";
                SQLtxt = SQLtxt + "        ,'" + modCommon.EditSQLAddSQuot(modMITMR010.OrdHDat[0].OrdH_038) + "'";
                SQLtxt = SQLtxt + "       )";


                if (!db.ExecuteSql(SQLtxt, out strMsg))
                {
                    db.RollbackTran();
                    modSac_Com.ksExpMsgBox(modHanbai.gstGUIDE_E002 + " ERR: " + strMsg, "E");
                    return;

                }





                SQLtxt = "";
                SQLtxt = SQLtxt + " INSERT INTO OrdM_Dat ";
                SQLtxt = SQLtxt + " SELECT NULL";
                SQLtxt = SQLtxt + "       ,'" + ssText + "'";
                SQLtxt = SQLtxt + "       ,OrdM_003";
                SQLtxt = SQLtxt + "       ,OrdM_004";
                SQLtxt = SQLtxt + "       ,OrdM_005";
                SQLtxt = SQLtxt + "       ,OrdM_006";
                SQLtxt = SQLtxt + "       ,OrdM_007";
                SQLtxt = SQLtxt + "       ,OrdM_008";
                SQLtxt = SQLtxt + "       ,OrdM_009";
                SQLtxt = SQLtxt + "       ,OrdM_010";
                SQLtxt = SQLtxt + "       ,OrdM_011";
                SQLtxt = SQLtxt + "       ,OrdM_012";
                SQLtxt = SQLtxt + "       ,OrdM_013";
                SQLtxt = SQLtxt + "       ,OrdM_014";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,'" + DateTime.Now + "'";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,'" + clswinApi.StaticWinApi.Wsnumber + "'";
                SQLtxt = SQLtxt + "       ,OrdM_025";
                SQLtxt = SQLtxt + " FROM   OrdM_Dat";
                SQLtxt = SQLtxt + " WHERE  OrdM_002 = '" + modCommon.EditSQLAddSQuot(txtMitsuNo.Text) + "'";

          
                if (!db.ExecuteSql(SQLtxt,out strMsg))
                {
                    db.RollbackTran();
                    modSac_Com.ksExpMsgBox(modHanbai.gstGUIDE_E002 + " ERR: " + strMsg, "E");
                    return;

                }



                SQLtxt = "";
                SQLtxt = SQLtxt + " INSERT INTO OrdD_Dat ";
                SQLtxt = SQLtxt + " SELECT NULL";
                SQLtxt = SQLtxt + "       ,'" + ssText + "'";
                SQLtxt = SQLtxt + "       ,OrdD_003";
                SQLtxt = SQLtxt + "       ,OrdD_004";
                SQLtxt = SQLtxt + "       ,OrdD_005";
                SQLtxt = SQLtxt + "       ,OrdD_006";
                SQLtxt = SQLtxt + "       ,OrdD_007";
                SQLtxt = SQLtxt + "       ,OrdD_0071";
                SQLtxt = SQLtxt + "       ,OrdD_008";
                SQLtxt = SQLtxt + "       ,OrdD_009";
                SQLtxt = SQLtxt + "       ,OrdD_010";
                SQLtxt = SQLtxt + "       ,OrdD_011";
                SQLtxt = SQLtxt + "       ,OrdD_012";
                SQLtxt = SQLtxt + "       ,OrdD_013";
                SQLtxt = SQLtxt + "       ,OrdD_014";
                SQLtxt = SQLtxt + "       ,OrdD_015";
                SQLtxt = SQLtxt + "       ,OrdD_016";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,'" + DateTime.Now + "'";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,'" + StaticWinApi.Wsnumber + "'";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,NULL";
                SQLtxt = SQLtxt + "       ,OrdD_041";
                SQLtxt = SQLtxt + "       ,OrdD_042";
                SQLtxt = SQLtxt + " FROM   OrdD_Dat";
                SQLtxt = SQLtxt + " WHERE  OrdD_002 = '" + modCommon.EditSQLAddSQuot(txtMitsuNo.Text) + "'";
                SQLtxt = SQLtxt + " ORDER BY OrdD_003";
                SQLtxt = SQLtxt + "         ,OrdD_004";


                if (!db.ExecuteSql(SQLtxt, out strMsg))
                {
                    db.RollbackTran();
                    modSac_Com.ksExpMsgBox(modHanbai.gstGUIDE_E002 + " ERR: " + strMsg, "E");
                    return;

                }
                db.CommitTrans();

                modSac_Com.ksExpMsgBox("『" + modCommon.EditSQLAddSQuot(txtMitsuNo.Text) + "』のデータをコピーし、" + Environment.NewLine + "『" + ssText + "』として追加しました。" + Environment.NewLine + Environment.NewLine + "SEQ数： " + liCnt, "I");
                txtMitsuNo.Text = ssText;
                txtMitsuNo_KeyUp(this, new KeyEventArgs(Keys.Return));


            }



        }       


        private void cmdSansyo_1_Click(object sender, EventArgs e)
        {
            modHanbai.TokMstInfo TokMst = new modHanbai.TokMstInfo();
            
            saFrm_Sansyo901 frm = new saFrm_Sansyo901();
            frm.SET_TNAME(modHanbai.NCnvN(txtTokName.Text));
            frm.SET_TCODE(modHanbai.NCnvZ(numTokCode.Text));
            frm.SET_TANTO(modHanbai.NCnvN(txtTokTanto.Text));
            frm.SET_VESSEL(modHanbai.NCnvN(txtVessel.Text));
            frm.SET_OWNER(modHanbai.NCnvN(txtOName.Text));
            frm.SET_OCODE(modHanbai.NCnvZ(numOCode.Text));
            frm.SET_DOC(modHanbai.NCnvN(txtDoc.Text));
            frm.SET_SNO(modHanbai.NCnvN(txtShipNo.Text));
            frm.SET_PERSON(modSac_Com.stComboGetKomoku(cboTanto.SelectedIndex > 0 ? cboTanto.Text : ""));
            if (optCountry_0.Checked == true)
                frm.SET_COUNTRY("0");
            else
                frm.SET_COUNTRY("1");
            frm.ShowDialog(this);
            if (frm.Status() == 1)
            {
          
                numTokCode.Text = Convert.ToInt64(Convert.ToDouble(frm.STM_TCODE())).ToString();
                txtTokName.Text = frm.STM_TNAME();
                txtTokTanto.Text = frm.STM_TANTO();
                txtVessel.Text = frm.STM_VESSEL();
                numOCode.Text = Convert.ToInt64(Convert.ToDouble(frm.STM_OCODE())).ToString();
                txtOName.Text = frm.STM_OWNER();
                if (frm.STM_VESSEL() == "")
                {
                    txtDoc.Text = "";
                    txtShipNo.Text = "";
                    }
                else
                    {
                    txtDoc.Text = frm.STM_DOC();
                    txtShipNo.Text = frm.STM_SHIPNO();
                    }
                if (frm.STM_COUNTRY() == 0)
                    optCountry_0.Checked = true;
                else
                    optCountry_1.Checked = true;
                modSac_Com.stComboDispDataKomoku(cboTanto, frm.STM_PERSON());
            }
            TokMst.Tok_001 = Convert.ToDecimal(numTokCode.Text.Trim() == "" ? "0": numTokCode.Text);
            if(modHanbai.GetTokMstInfo(ref TokMst, 0)==true)
            {
                numMitsuYuko.Text=TokMst.Tok_038.ToString();
            }
            txtUnit.Focus();


        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void cmdSansyo_0_Click(object sender, EventArgs e)
        {
            saFrm_Sansyo906 frm = new saFrm_Sansyo906();
            frm.ShowDialog(this);
            if (frm.Status() == 0)
                return;
            txtMitsuNo.Text = frm.MITNO();    //    '見積№
            datMitsuYMD.Value = Convert.ToDateTime(frm.MITDATE());    //   '見積日
            txtMitsuNo_KeyUp(sender, null);//
            txtIraiNo.Focus();
        }

        private void timTimer_Tick(object sender, EventArgs e)
        {
            if (lblJoutai.Visible)
                lblJoutai.Visible = false;
            else
                lblJoutai.Visible = true;
        }


        private void OrdHInfoSetProc()
        {
            var tmpHeaderOrder = modMITMR010.OrdHDat[0];
            //var tmpMasterOrder = modMITMR010.OrdMDat;
            //var tmpDetailOrder = modMITMR010.OrdDDat;

            if (tmpHeaderOrder.OrdH_027 != 0)
            {
                lblJoutai.Visible = true;
                timTimer.Enabled = true;
                timTimer.Interval = 600;
            }

            datMitsuYMD.Value = Convert.ToDateTime(modHanbai.NCnvN(tmpHeaderOrder.OrdH_010));
            txtMitsuNo.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_009).ToString().ToString();
            txtIraiNo.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_005).ToString();
            txtOrderNo.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_017).ToString();
            txtTokName.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_003).ToString();
            numTokCode.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_0031).ToString();
            numOCode.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_035).ToString();
            txtTokTanto.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_004).ToString();
            txtVessel.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_006).ToString();
            txtOName.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_022).ToString();
            txtOrigin.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_012).ToString();
            modSac_Com.stComboDispDataKomoku(cboCountry, modHanbai.NCnvN(tmpHeaderOrder.OrdH_013)).ToString();
            txtMitsuNouki.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_011).ToString();
            modSac_Com.stComboDispDataKomoku(cboTanto, modHanbai.NCnvN(tmpHeaderOrder.OrdH_008)).ToString();
            modSac_Com.stComboDispDataKomoku(cboManila, modHanbai.NCnvN(tmpHeaderOrder.OrdH_0081)).ToString();
            modSac_Com.stComboDispDataKomoku(cboMitsumori, modHanbai.NCnvN(tmpHeaderOrder.OrdH_0082)).ToString();
            txtTekiyo.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_014).ToString();
            numMitsuYuko.Text = modHanbai.NCnvZ(tmpHeaderOrder.OrdH_016).ToString();

            if (Convert.ToDecimal(modHanbai.NCnvZ(tmpHeaderOrder.OrdH_007)) == 0)
                optCountry_0.Checked = true;
            else
                optCountry_1.Checked = true;

            numLastSEQ.Text = modMITMR010.GetLastSEQ(modHanbai.NCnvN(tmpHeaderOrder.OrdH_009)).ToString();
            SetGoukeiArea(modHanbai.NCnvN(tmpHeaderOrder.OrdH_009).ToString());

            if (modHanbai.NCnvN(tmpHeaderOrder.OrdH_028) != "" && Convert.ToDateTime(modHanbai.NCnvN(tmpHeaderOrder.OrdH_028)) > Convert.ToDateTime("01/01/1900"))
            {
                datMtHakko.Value = Convert.ToDateTime(modHanbai.NCnvN(tmpHeaderOrder.OrdH_028).ToString());
            }

            if (modHanbai.NCnvN(tmpHeaderOrder.OrdH_0281) != "" && Convert.ToDateTime(modHanbai.NCnvN(tmpHeaderOrder.OrdH_0281)) > Convert.ToDateTime("01/01/1900"))
            {
                datPIHakko.Value = Convert.ToDateTime(modHanbai.NCnvN(tmpHeaderOrder.OrdH_0281).ToString());
            }

            txtDoc.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_037).ToString();
            txtShipNo.Text = modHanbai.NCnvN(tmpHeaderOrder.OrdH_038).ToString();
        }


        private void OrdMInfoSetProc()
        {
            //var tmpHeaderOrder = modMITMR010.OrdHDat[0];
            var tmpMasterOrder = modMITMR010.OrdMDat[0];
            //var tmpDetailOrder = modMITMR010.OrdDDat;




            numSEQ.Text = modHanbai.NCnvZ(tmpMasterOrder.OrdM_003).ToString();
            txtUnit.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_004).ToString();
            txtMaker.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_005).ToString();
            txtType.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_006).ToString();
            txtSerNo.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_007).ToString();
            txtDwgNo.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_008).ToString();
            txtAdditional.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_025).ToString();
            txtComment1.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_009).ToString();
            txtComment2.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_010).ToString();
            txtComment3.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_011).ToString();
            txtComment4.Text = modHanbai.NCnvN(tmpMasterOrder.OrdM_012).ToString();


        }

        private void GotFocusSec(Control vcCtrl, string vsMsg, int viMode)
        {
            if (viMode == 0)
            {
                modSac_Com.stSetActiveControlB(vcCtrl, true);
            }
            BefCtl = vcCtrl;
        }



        private void LostFocusSec(Control vcCtrl,  int viMode)
        {
            if (viMode == 0)
            {
                modSac_Com.stSetActiveControlB(vcCtrl, false   );
            }
            BefCtl = vcCtrl;
        }



        private void OrdDInfoSetProc()
        {
            clearGridView();
            //var tmpHeaderOrder = modMITMR010.OrdHDat[0];
            //var tmpMasterOrder = modMITMR010.OrdMDat[0];
            var tmpDetailOrder = modMITMR010.OrdDDat;
            int rowctr = 0;
            for (int i = 0; i < tmpDetailOrder.Count(); i++)
            {
                spdData.Rows.Add();
                spdData.Rows[rowctr].Cells[modMITMR010.spdcol_Seq].Value = modHanbai.NCnvZ(tmpDetailOrder[i].OrdD_003).ToString();
                spdData.Rows[rowctr].Cells[modMITMR010.spdcol_ItemNo].Value = modHanbai.NCnvZ(tmpDetailOrder[i].OrdD_004).ToString();
                spdData.Rows[rowctr].Cells[modMITMR010.spdcol_HinNm].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_005).ToString();
                spdData.Rows[rowctr].Cells[modMITMR010.spdcol_HinNo].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_006).ToString();

                if (Convert.ToDecimal(modHanbai.NCnvZ(tmpDetailOrder[i].OrdD_008)) != 0)
                {

                    if (Convert.ToDecimal(modHanbai.NCnvZ(tmpDetailOrder[i].OrdD_0071)) > 0)
                    {
                        spdData.Rows[rowctr].Cells[modMITMR010.spdcol_CdNo].Value = modHanbai.NCnvZ(tmpDetailOrder[i].OrdD_0071).ToString();
                    }

                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_NoS].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_007).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_MSuryo].Value = Double.Parse(modHanbai.NCnvN(tmpDetailOrder[i].OrdD_008).ToString(), CultureInfo.InvariantCulture).ToString("N2", CultureInfo.InvariantCulture);




                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_MTani].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_009).ToString();

                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_MTanka].Value = Double.Parse(modHanbai.NCnvN(tmpDetailOrder[i].OrdD_010).ToString(), CultureInfo.InvariantCulture).ToString("N2", CultureInfo.InvariantCulture);

                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_Origin].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_041).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_DelTime].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_042).ToString();

                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_MKinG].Value = Double.Parse(modHanbai.NCnvN(tmpDetailOrder[i].OrdD_011).ToString(), CultureInfo.InvariantCulture).ToString("N2", CultureInfo.InvariantCulture);

                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_STanka].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_012).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_SRitsu].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_013).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_SNet].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_014).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_SKinG].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_015).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_Arari].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_016).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_HDate].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_023).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_SDate].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_025).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_UDate].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_027).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_SeDate].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_029).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_SirNm].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_038).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_SMTanka].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_039).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_InDate].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_040).ToString();
                    spdData.Rows[rowctr].Cells[modMITMR010.spdcol_Code].Value = modHanbai.NCnvN(tmpDetailOrder[i].OrdD_0381).ToString();
                }
                rowctr++;
            }
        }



        private Boolean SetGoukeiArea(string vsMitsumoriNo)
        {
            DBService db = new DBService();
            Boolean retbool;
            string SQLtxt;

            SQLtxt = "    SELECT SUM(OrdM_013) AS OrdM_013";
            SQLtxt = SQLtxt + " ,SUM(OrdM_014) AS OrdM_014";
            SQLtxt = SQLtxt + "  FROM OrdM_Dat";
            SQLtxt = SQLtxt + " WHERE OrdM_002 ='" + vsMitsumoriNo + "'";
            SQLtxt = SQLtxt + " GROUP BY OrdM_002";




            DataTable dt = new DataTable();
            dt = db.GetDatafromDB(SQLtxt);
            if (dt.Rows.Count >= 1)
            {
                DataRow dr = dt.Rows[0];
                numMSuryo.Text = modHanbai.NCnvZ(dr["OrdM_013"].ToString());
                numMKingaku.Text = modHanbai.NCnvZ(dr["OrdM_014"].ToString());
            }

            SQLtxt = "    SELECT SUM(OrdM_015) AS OrdM_015";
            SQLtxt = SQLtxt + " ,SUM(OrdM_016) AS OrdM_016";
            SQLtxt = SQLtxt + " ,SUM(OrdM_017) AS OrdM_017";
            SQLtxt = SQLtxt + " ,SUM(OrdM_018) AS OrdM_018";
            SQLtxt = SQLtxt + " ,SUM(OrdM_019) AS OrdM_019";
            SQLtxt = SQLtxt + " ,SUM(OrdM_020) AS OrdM_020";
            SQLtxt = SQLtxt + " ,SUM(OrdM_021) AS OrdM_021";
            SQLtxt = SQLtxt + " ,SUM(OrdM_022) AS OrdM_022";
            SQLtxt = SQLtxt + "  FROM OrdM_Dat";
            SQLtxt = SQLtxt + " WHERE OrdM_002 ='" + vsMitsumoriNo + "'";
            SQLtxt = SQLtxt + " AND   OrdM_001 <> ''";
            SQLtxt = SQLtxt + " GROUP BY OrdM_001";

            dt = db.GetDatafromDB(SQLtxt);
            if (dt.Rows.Count >= 1)
            {
                DataRow dr = dt.Rows[0];
                numJSuryo.Text = modHanbai.NCnvZ(dr["OrdM_015"].ToString());
                numJKingaku.Text = modHanbai.NCnvZ(dr["OrdM_016"].ToString());

                numHSuryo.Text = modHanbai.NCnvZ(dr["OrdM_017"].ToString());
                numHKingaku.Text = modHanbai.NCnvZ(dr["OrdM_018"].ToString());

                numSSuryo.Text = modHanbai.NCnvZ(dr["OrdM_019"].ToString());
                numSKingaku.Text = modHanbai.NCnvZ(dr["OrdM_020"].ToString());

                numUSuryo.Text = modHanbai.NCnvZ(dr["OrdM_021"].ToString());
                numUKingaku.Text = modHanbai.NCnvZ(dr["OrdM_022"].ToString());
            }
            retbool = true;
            return retbool;
        }

        private void cmdFunc_2_Click(object sender, EventArgs e)
        {
            ModModeProc();
        }

        private void cmdSansyo_2_Click(object sender, EventArgs e)
        {
            frmRefUniTbl frm = new frmRefUniTbl();
            frm.ShowDialog(this);

            if (frm.Status() == 0)
                return;
            txtUnit.Text = frm.STM_TNAME();
        }

        private void cmdSansyo_3_Click(object sender, EventArgs e)
        {
            frmRefMakTbl frm = new frmRefMakTbl();
            frm.ShowDialog(this);
            if (frm.Status() == 0)
                return;
            txtMaker.Text = frm.STM_TNAME();
        }

        private void cmdSansyo_4_Click(object sender, EventArgs e)
        {
            frmRefTypTbl frm = new frmRefTypTbl();
            frm.ShowDialog(this);
            if (frm.Status() == 0)
                return;
            txtType.Text = frm.STM_TNAME();
        }

        private void txtMitsuNo_KeyUp(object sender, KeyEventArgs e)
        {
            string lsMitsuNo = modHanbai.NCnvN(txtMitsuNo.Text);
            ;

            if (e != null && e.KeyCode != Keys.Enter)
            {
                return;
            }

            lsMitsuNo = modHanbai.NCnvN(txtMitsuNo.Text);

            if (lsMitsuNo != "")
            {
                if (modMITMR010.OrdHDatReadProc(lsMitsuNo, 0) == false)
                {
                    timTimer.Interval = 100;
                    timTimer.Enabled = false;
                    lblJoutai.Visible = false;
                    return;
                }
                if (modMITMR010.OrdMDatReadProc(lsMitsuNo, 1, 0) == false)
                {
                }
                if (modMITMR010.OrdDDatReadProc(lsMitsuNo, 1) == false)
                {
                }

                OrdHInfoSetProc();
                OrdMInfoSetProc();
                OrdDInfoSetProc();
            }
        }

        private void mnuPaste_Click(object sender, EventArgs e)
        {
            pastefromClipboard();
        }

        private void pastefromClipboard()
        {
            string s = Clipboard.GetText();
            string line = s.Split('\n')[0]; //1 line only
            int isFail = 0;
            int currRow = spdData.SelectedCells[0].RowIndex;
            int currCol = spdData.SelectedCells[0].ColumnIndex;
            DataGridViewCell ocell;
            string[] texts = line.Split('\t');
            if (spdData.Rows.Count - 1 == currRow)//paste in lastrows
            {
                spdData.Rows.Add();
            }
            for (int i = 0; i < texts.GetLength(0); i++)
            {
                ocell = spdData[currCol + i, currRow];
                ocell.Value = Convert.ChangeType(texts[i], ocell.ValueType);

            }


        }

        private void mnuCopy_Click(object sender, EventArgs e)
        {

            if(spdData.GetClipboardContent()==null)
            {
                MessageBox.Show("Nothing was selected", "Information", MessageBoxButtons.OK);
            }
            else
            {
                Clipboard.SetDataObject(this.spdData.GetClipboardContent());
            }

          
        }

        private void cmdSEQClick(object sender, EventArgs e) 
        {
            Button btn = sender as Button;
            int indx= 0;

            switch(btn.Name.ToString())
            {
                case "cmdSEQ_0":
                    {
                        indx = 0;
                        break;
                    }
                case "cmdSEQ_1":
                    {
                        indx = 1;
                        break;
                    }
            }

            if(modHanbai.NCnvN(txtMitsuNo.Text)=="")
            {
                modSac_Com.ksExpMsgBox("Please input '" + label2.Text + "'", "E");
                txtMitsuNo.Focus();
                return;

            }
            if (modMITMR010.OrdMDatReadProc(modHanbai.NCnvN(txtMitsuNo.Text), Convert.ToInt16( numSEQ.Text), indx +1 ) == true)
            {
                OrdMInfoSetProc();
                if(modMITMR010.OrdDDatReadProc(modHanbai.NCnvN(txtMitsuNo.Text), Convert.ToInt16(numSEQ.Text))==true)
                {
                    OrdDInfoSetProc();
                }

            }
            else
            {
                if (indx == 0)
                {
                    modSac_Com.ksExpMsgBox(modMITMR010.MITMR010_MSG002, "I");
                }
                else
                {
                    modSac_Com.ksExpMsgBox(modMITMR010.MITMR010_MSG003, "I");
                }
            }



        }

        private void txtMitsuNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmdFunc_3_Click(object sender, EventArgs e)
        {
            DelModeProc();
        }

        private void cmdFunc_4_Click(object sender, EventArgs e)
        {
            CopyDataProc();
        }

        private void txtIraiNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtIraiNo_KeyUp(object sender, KeyEventArgs e)
        {
            string lsIriNo = modHanbai.NCnvN(txtIraiNo.Text);
            ;

            if (e != null && (e.KeyCode != Keys.Enter || e.KeyCode != Keys.Return))
            {
                return;
            }



            if (lsIriNo != "")
            {
                if (OrdHDatReadProcIraiNo(lsIriNo, 0) == false)
                {
                    timTimer.Interval = 100;
                    timTimer.Enabled = false;
                    lblJoutai.Visible = false;
                    return;
                }
                if (modMITMR010.OrdMDatReadProc(lsIriNo, 1, 0) == false)
                {
                }
                if (modMITMR010.OrdDDatReadProc(lsIriNo, 1) == false)
                {
                }

                OrdHInfoSetProc();
                OrdMInfoSetProc();
                OrdDInfoSetProc();
            }
        }

        private bool OrdHDatReadProcIraiNo(string vsIraiNo, int viMode)
        {
            string SQLtxt;
            bool retbool = false;
            if(viMode==0)
            {
                modMITMR010.OrdHDat[0] = new modSinseiMarin.OrdHDatInfo();
            }

            SQLtxt = "";
            SQLtxt = SQLtxt + " SELECT * FROM OrdH_Dat";
            SQLtxt = SQLtxt + " WHERE OrdH_005 ='" + vsIraiNo + "'";

            DBService db = new DBService();

            DataTable dt = new DataTable();
            dt = db.GetDatafromDB(SQLtxt);

            if(dt.Rows.Count>0 )
            {
                if(viMode==0)
                {
                    if(dt.Rows.Count > 1)
                    {
                        frmMITMR010S frm = new frmMITMR010S();
                        
                        frm.ShowDialog(this);

                    }
                }
            }
            return retbool;

        }

        private void label31_Click(object sender, EventArgs e)
        {
            frmMITMR010S frm = new frmMITMR010S();
            frm.Show();
        }

        private void numSEQ_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void numMitsuYuko_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }


        private void cmdFunc_10_Click(object sender, EventArgs e)
        {
            Boolean retbool = false;
            int Idx;
            string lsEstimateNo;
            string lsMsg1="";      // INSERT 2014/02/04 AOKI
            string lsMsg2 = "";      // INSERT 2014/02/04 AOKI
            string SQLtxt;
            string strMsg;
            long lngErr;
            string lsGenuine;
            string lsOem;
            string lsNote;
            int i;
            TypeArray[] DTArray = new TypeArray[0];
            string lsDelTime="";
            int liDelCnt;
            string lsNA;
            string lsNote2;
            string lsNote3;

            DataTable dt = new DataTable();
            DBService db = new DBService();


            switch(EditMode)
            {
                case 0:
                    {
                        modSac_Com.ksExpMsgBox(modHanbai.gstGUIDE_E001, "E");
                        break;
                    }
                case 1:
                case 2:
                    {
                        if (ValidateEntry(0))
                        {
                            if (EditMode == 1)
                            {
                                lsMsg1 = "見積データを追加します。よろしいですか？";
                                lsMsg2 = "正常に追加されました。";
                            }
                            else
                            {
                                lsMsg1 = "見積データを更新します。よろしいですか？";
                                lsMsg2 = "正常に更新されました。";
                            }

                            if (modSac_Com.ksExpMsgBox(lsMsg1, "Q") == (int)Microsoft.VisualBasic.MsgBoxResult.Yes)
                            {
                                if (modMITMR010.DataKousinProc() == true)
                                {
                                    SQLtxt = "";
                                    SQLtxt = SQLtxt + " SELECT   OrdD_042 ";
                                    SQLtxt = SQLtxt + " FROM     OrdD_Dat ";
                                    SQLtxt = SQLtxt + " WHERE    OrdD_002 = '" + modHanbai.NCnvN(txtMitsuNo.Text) + "'";
                                    SQLtxt = SQLtxt + " AND      RIGHT(OrdD_004,1) = 1 ";
                                    SQLtxt = SQLtxt + " AND      ISNULL(OrdD_042,0) > 0 ";
                                    SQLtxt = SQLtxt + " AND      OrdD_010 <> 0 ";    // INSERT 2017/09/20 AOKI
                                    SQLtxt = SQLtxt + " GROUP BY OrdD_042 ";
                                    SQLtxt = SQLtxt + " ORDER BY OrdD_042 DESC";
                                    dt = new DataTable();
                                    dt = db.GetDatafromDB(SQLtxt);
                                    liDelCnt = dt.Rows.Count;


                                    if (liDelCnt > 0)
                                    {
                                        Array.Resize(ref DTArray, liDelCnt );
                                        for (int ctr = 0; ctr <= liDelCnt - 1; ctr++)
                                        {
                                            DTArray[ctr].Deltime = Convert.ToInt32(dt.Rows[ctr]["OrdD_042"].ToString());
                                        }
                                    }


                                    SQLtxt = "";
                                    SQLtxt = SQLtxt + " SELECT   OrdD_004 ";
                                    SQLtxt = SQLtxt + "      ,   OrdD_009 ";         // INSERT 2016/04/20 AOKI
                                    SQLtxt = SQLtxt + "      ,   OrdD_041 ";
                                    SQLtxt = SQLtxt + "      ,   OrdD_042 ";
                                    SQLtxt = SQLtxt + " FROM     OrdD_Dat ";
                                    SQLtxt = SQLtxt + " WHERE    OrdD_002 = '" + modHanbai.NCnvN(txtMitsuNo.Text) + "'";
                                    SQLtxt = SQLtxt + " AND      RIGHT(OrdD_004,1) = 1 ";
                                    SQLtxt = SQLtxt + " AND      OrdD_010 <> 0 ";    // INSERT 2017/09/20 AOKI
                                    SQLtxt = SQLtxt + " ORDER BY OrdD_004";

                                    dt = new DataTable();
                                    dt = db.GetDatafromDB(SQLtxt);

                                    if (dt.Rows.Count < 1)
                                    {
                                        return;
                                    }

                                    lsGenuine = "";
                                    lsOem = "";
                                    lsNA = "";

                                    for (int ctr = 0; ctr < dt.Rows.Count - 1; ctr++)
                                    {
                                        if (modHanbai.NCnvN(dt.Rows[ctr]["OrdD_009"].ToString()) != "")
                                        {
                                            switch (dt.Rows[ctr]["OrdD_041"].ToString())
                                            {
                                                case "1":
                                                    {
                                                        lsGenuine = Utils.Left(dt.Rows[ctr]["OrdD_004"].ToString(), dt.Rows[ctr]["OrdD_004"].ToString().Length - 1) + ",";
                                                        break;
                                                    }
                                                case "0":
                                                    {
                                                        lsOem = Utils.Left(dt.Rows[ctr]["OrdD_004"].ToString(), dt.Rows[ctr]["OrdD_004"].ToString().Length - 1) + ",";
                                                        break;
                                                    }
                                                default:
                                                    {
                                                        lsNA = Utils.Left(dt.Rows[ctr]["OrdD_004"].ToString(), dt.Rows[ctr]["OrdD_004"].ToString().Length - 1) + ",";
                                                        break;
                                                    }
                                            }

                                        }

                                        if (liDelCnt > 0)
                                        {
                                            for (int ictr = 0; ictr <= DTArray.GetUpperBound(0); ictr++)
                                            {
                                                if (DTArray[ictr].Deltime == Convert.ToInt32(modHanbai.NCnvZ(dt.Rows[ctr]["OrdD_042"].ToString())))
                                                {
                                                    DTArray[ictr].ItemNo = DTArray[ictr].ItemNo + Utils.Left(modHanbai.NCnvZ(dt.Rows[ctr]["OrdD_004"]).ToString(),
                                                        dt.Rows[ctr]["OrdD_004"].ToString().Length - 1) + ",";
                                                }
                                            }
                                        }


                                        //DTArray[ctr].Deltime = Convert.ToInt32(dt.Rows[ctr]["OrdD_042"].ToString());
                                    }
                                    lsNote = modMITMR010.OriginEdit(lsGenuine)??"";
                                    lsNote2 = modMITMR010.OriginEdit(lsOem) ?? "";
                                    lsNote3 = modMITMR010.OriginEdit(lsNA) ?? "";
                                    if (liDelCnt > 1)
                                    {
                                        for (int x = 0; x <= DTArray.GetUpperBound(0); x++)
                                        {
                                            lsDelTime = lsDelTime + "ITEM " + modMITMR010.OriginEdit(DTArray[x].ItemNo.ToString()) +
                                                " = " + DTArray[x].Deltime.ToString() + " DAYS*";

                                        }
                                    }
                                    else
                                    {
                                        switch (liDelCnt)
                                        {
                                            case 0:
                                                {
                                                    lsDelTime = "";
                                                    break;
                                                }
                                            case 1:
                                                {
                                                    lsDelTime = DTArray[0].Deltime + " DAYS";
                                                    break;
                                                }

                                            default:
                                                break;
                                        }
                                    }
                                    //
                                    SQLtxt = "";
                                    SQLtxt += "UPDATE OrdH_Dat ";
                                    SQLtxt += "SET ";

                                    if (lsDelTime.Length > 0)

                                    {
                                        if (txtMitsuNo.Text.Replace("*", "").Replace(" ", "") != lsDelTime.Replace(" ", ""))
                                        {
                                            SQLtxt += " OrdH_011 = '" + lsDelTime + "',";
                                        }

                                    }
                                    else
                                    {
                                        SQLtxt += " OrdH_011 = '', ";
                                    }

                                    if (txtOrigin.Text.Replace("*", "").Replace(" ", "") != lsNote.Replace(" ", "")
                                        || txtOrigin.Text.Replace("*", "").Replace(" ", "") != lsNote2.Replace(" ", "")
                                        )
                                    {
                                        if (lsGenuine.Length > 0 && lsOem.Length > 0 && lsNA.Length > 0)
                                        {
                                            SQLtxt = SQLtxt + "OrdH_012 = 'ITEM " + lsNote + " = GENUINE*ITEM " + lsNote2 + " = OEM*ITEM " + lsNote3 + " = NA'";
                                        }
                                        else if (lsGenuine.Length > 0 && lsOem.Length > 0 && lsNA.Length <= 0)
                                        {
                                            SQLtxt = SQLtxt + "OrdH_012 = 'ITEM " + lsNote + " = GENUINE*ITEM " + lsNote2 + " = OEM'";
                                        }
                                        else if (lsGenuine.Length > 0 && lsOem.Length <= 0 && lsNA.Length > 0)
                                        {
                                            SQLtxt = SQLtxt + "OrdH_012 = 'ITEM " + lsNote + " = GENUINE*ITEM " + lsNote3 + " = NA'";
                                        }
                                        else if (lsGenuine.Length > 0 && lsOem.Length <= 0 && lsNA.Length > 0)
                                        {
                                            SQLtxt = SQLtxt + "OrdH_012 = 'ITEM " + lsNote + " = GENUINE*ITEM " + lsNote3 + " = NA'";
                                        }
                                        else if (lsGenuine.Length > 0 && lsOem.Length <= 0 && lsNA.Length <= 0)
                                        {
                                            SQLtxt = SQLtxt + "OrdH_012 = 'GENUINE'";
                                        }
                                        else if (lsGenuine.Length <= 0 && lsOem.Length > 0 && lsNA.Length > 0)
                                        {
                                            SQLtxt = SQLtxt + "OrdH_012 = 'ITEM " + lsNote2 + " = OEM*ITEM " + lsNote3 + " = NA'";
                                        }
                                        else if (lsGenuine.Length <= 0 && lsOem.Length > 0 && lsNA.Length <= 0)
                                        {
                                            SQLtxt = SQLtxt + "OrdH_012 = 'OEM'";
                                        }

                                        else if (lsGenuine.Length <= 0 && lsOem.Length <= 0)
                                        {
                                            SQLtxt = SQLtxt + "OrdH_012 = ''";
                                        }
                                    }
                                    else
                                    {
                                        SQLtxt = Utils.Left(SQLtxt, SQLtxt.Length - 1);
                                    }

                                    SQLtxt += "WHERE  OrdH_009 = '" + modHanbai.NCnvN(txtMitsuNo.Text) + "'";


                                    db.ExecuteSql(SQLtxt, out strMsg);

                                    modSac_Com.ksExpMsgBox(lsMsg2, "I");
                                    lsEstimateNo = txtMitsuNo.Text;
                                    EditClear();
                                    txtMitsuNo.Text = lsEstimateNo;
                                    txtMitsuNo.Focus();
                                    spdData.Rows.Clear();
                                    //


                                }
                            }

                        }
                        break;
                    }
                case 3:
                    {
                        if(ValidateEntry(1) == true)
                        {
                            if(modSac_Com.ksExpMsgBox("見積№：" + modHanbai.NCnvN(txtMitsuNo.Text) + " SEQ: "
                                + modHanbai.NCnvZ(numSEQ.Text) + " の見積データを取り消します。よろしいですか？","Q" )
                                == (int)Microsoft.VisualBasic.MsgBoxResult.Yes)
                            {
                                if(modMITMR010.DataDeleteProc(modHanbai.NCnvN(txtMitsuNo.Text), Convert.ToInt16(modHanbai.NCnvZ(numSEQ.Text))) == true)
                                {
                                    modSac_Com.ksExpMsgBox("正常に取り消されました。", "I");
                                    EditClear();
                                    datMitsuYMD.Focus();
                                }
                            }
                        }
                        break;
                    }
            }
            
        }

       

        private Boolean ValidateEntry(int viMode)
        {
            int iKoumoku;
            int k1;
            int liBefF;
            int JCnt;
            decimal WK_MSuryo =0;
            decimal WK_MKingaku = 0;

            string lsGenuine = "";
            string lsOem = "";
            string[] stArrayData1  ;
            string[] stArrayData2  ;
            int ii=0;
            int liStart=0;
            int liBefore=0;
            string lsNote = "";
            modHanbai.TokMstInfo TokMst ;
            
            Boolean retbool = false;
            if(!checkIfhasOrder())
            {
                modSac_Com.ksExpMsgBox("Data was empty please input order ", "E");
                spdData.Focus();
                return retbool;
            }



            if (modHanbai.NCnvN(txtMitsuNo.Text)=="")
            {
                modSac_Com.ksExpMsgBox("Please input '" + label2.Text + "'", "E");
                txtMitsuNo.Focus();
                return retbool;
            }
            if(modMITMR010.OrdDDatReadProc(modHanbai.NCnvN(txtMitsuNo.Text),0)==true)
            {
                if(modMITMR010.OrdHDat[0].OrdH_027 != 0)
                {
                    modSac_Com.ksExpMsgBox(modMITMR010.MITMR010_MSG001, "E");
                    txtMitsuNo.Focus();
                    return retbool;
                }
            }
            if(modHanbai.NCnvZ(numSEQ.Text)=="0")
            {
                modSac_Com.ksExpMsgBox(label20.Text + modHanbai.gstGUIDE_E011, "E");
                numSEQ.Focus();
                return retbool;
            }
            if (viMode==1)
            {
                if(modMITMR010.OrdMDatReadProc(modHanbai.NCnvN(txtMitsuNo.Text),
                    Convert.ToInt16(modHanbai.NCnvZ(numSEQ.Text)),3)==false)
                {
                    modSac_Com.ksExpMsgBox("入力されたSEQのデータは存在しません", "E");
                    numSEQ.Focus();
                    return retbool;
                }
            }
            else
            {
                if(modMITMR010.OrdHDatReadProc(modHanbai.NCnvN(txtMitsuNo.Text),1)==true)
                {
                    if(EditMode==1)
                    {
                        MessageBox.Show("この見積№は既に登録されているデータです。" + Environment.NewLine +
                            "追加処理は行えません。",  "エラー",MessageBoxButtons.OK);
                        txtMitsuNo.Focus();
                        return retbool;
                    }
                    else
                    {
                        if(modSac_Com.ksExpMsgBox("この見積№は登録されていないデータです。" + Environment.NewLine + "このまま訂正処理を行いますか？","Q")== (int)(MsgBoxResult.No))
                        {
                            txtMitsuNo.Focus();
                            return retbool;
                        }
                    }
                }
                else
                {
                    if(EditMode==2)
                    {
                        MessageBox.Show("この見積№は登録されていないデータです" + Environment.NewLine +
                            "訂正処理は行えません。","", MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                        txtMitsuNo.Focus();
                        return retbool;
                    }
                }
            }
            if (viMode == 0)
            {
                if (modHanbai.NCnvN(txtTokName.Text) == "")
                {
                    modSac_Com.ksExpMsgBox(label5.Text + modHanbai.gstGUIDE_E011, "E");
                    txtIraiNo.Focus();
                    return retbool;
                }

                if (modHanbai.NCnvN(txtVessel.Text) == "")
                {
                    if (modSac_Com.ksExpMsgBox("No Vessel's Name Entered." + Environment.NewLine + "Do you want to continue?", "Q") == (int)(MsgBoxResult.No))
                        txtIraiNo.Focus();
                    return retbool;
                }
                //todo: check if this works



                int j = 0;
                k1 = 1;
                liBefF = 0;

                for (int i = 0; i < spdData.Rows.Count-1; i++)
                {
                    string lvWork = spdData.Rows[i].Cells[modMITMR010.spdcol_HinNm].Value.ToString();

                    if (modHanbai.NCnvN(lvWork) != "")
                    {
                        Array.Resize(ref modMITMR010.OrdDDat,j+1);
                           
                        modMITMR010.OrdDDat[j].OrdD_001 = "";
                        modMITMR010.OrdDDat[j].OrdD_002 = modHanbai.NCnvN((txtMitsuNo.Text));
                        modMITMR010.OrdDDat[j].OrdD_003 = Convert.ToInt32(modHanbai.NCnvZ(numSEQ.Text));
                        modMITMR010.OrdDDat[j].OrdD_005 =
                        modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_HinNm].Value.ToString());
                        modMITMR010.OrdDDat[j].OrdD_006 =
                        modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_HinNo].Value);
                        modMITMR010.OrdDDat[j].OrdD_0071 =0;
//                        modMITMR010.OrdDDat[j].OrdD_0071 =
//Convert.ToDecimal(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_CdNo].Value.ToString()));
                        modMITMR010.OrdDDat[j].OrdD_007 ="";
                        //modMITMR010.OrdDDat[j].OrdD_007 =
                        //modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_NoS].Value.ToString());

                        modMITMR010.OrdDDat[j].OrdD_008 =
                        Convert.ToString(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_MSuryo].Value.ToString()));

                        WK_MSuryo += Convert.ToDecimal(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_MSuryo].Value.ToString()));

                        modMITMR010.OrdDDat[j].OrdD_009 =
                        modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_MTani].Value.ToString());
                        modMITMR010.OrdDDat[j].OrdD_010 =
                        Convert.ToString(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_MTanka].Value));


                        if (Convert.ToDecimal(modHanbai.NCnvZ(modMITMR010.OrdDDat[j].OrdD_008)) > 0)
                        {
                            if (modHanbai.NCnvN(modMITMR010.OrdDDat[j].OrdD_008) == "")
                            {
                                modSac_Com.ksExpMsgBox("Please input 'Unit Price'", "E");
                                //spdData.Rows[i].Cells[modMITMR010.spdcol_MTanka]
                                spdData.CurrentCell = spdData.Rows[i].Cells[modMITMR010.spdcol_MTanka];
                                return retbool;
                            }
                        }

                        modMITMR010.OrdDDat[j].OrdD_011 =
                        Convert.ToString(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_MKinG].Value.ToString()));

                        WK_MKingaku += Convert.ToDecimal(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_MKinG].Value.ToString()));

                        modMITMR010.OrdDDat[j].OrdD_012 =
                        Convert.ToString(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_MTanka].Value));

                        modMITMR010.OrdDDat[j].OrdD_013 =
                        Convert.ToDecimal(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_SRitsu].Value));

                        modMITMR010.OrdDDat[j].OrdD_014 =
                        Convert.ToDecimal(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_SNet].Value));

                        modMITMR010.OrdDDat[j].OrdD_015 =
                        Convert.ToDecimal(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_SKinG].Value));

                        modMITMR010.OrdDDat[j].OrdD_016 =
                        Convert.ToDecimal(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_Arari].Value));

                        modMITMR010.OrdDDat[j].OrdD_038 =
                        modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_SirNm].Value);

                        modMITMR010.OrdDDat[j].OrdD_039 =
                        Convert.ToDecimal(modHanbai.NCnvZ(spdData.Rows[i].Cells[modMITMR010.spdcol_SMTanka].Value));

                        modMITMR010.OrdDDat[j].OrdD_040 =
                        (modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_SMTanka].Value));


                        modMITMR010.OrdDDat[j].OrdD_041 =
                        (modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_Origin].Value));



                        modMITMR010.OrdDDat[j].OrdD_042 =
                        (modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_DelTime].Value));

                        modMITMR010.OrdDDat[j].OrdD_0381 =
                        (modHanbai.NCnvN(spdData.Rows[i].Cells[modMITMR010.spdcol_Code].Value));

                        if (Convert.ToDecimal(modHanbai.NCnvZ(modMITMR010.OrdDDat[j].OrdD_008)) > 0
                            || modMITMR010.OrdDDat[j].OrdD_009 != ""
                            || (Convert.ToDecimal(modHanbai.NCnvZ(modMITMR010.OrdDDat[j].OrdD_011)) > 0
                            ))

                        {
                            if (modMITMR010.OrdDDat[j].OrdD_009 != "")
                            {
                                switch (modMITMR010.OrdDDat[j].OrdD_041)
                                {
                                    case ("1"):
                                        {
                                            lsGenuine = lsGenuine + "1,";
                                            break;
                                        }
                                    case ("2"):
                                        {
                                            lsOem = lsOem + k1.ToString() + ",";
                                            break;
                                        }
                                    default:
                                        break;
                                }
                            }
                            iKoumoku = Convert.ToInt32(k1.ToString("0000") + "1");
                            modMITMR010.OrdDDat[j].OrdD_004 = Convert.ToString(modHanbai.NCnvZ(iKoumoku));
                            k1++;
                            liBefF = 0;

                        }
                        else
                        {
                            if (liBefF == 1)
                            {
                                k1++;
                            }
                            iKoumoku = Convert.ToInt32(k1.ToString("0000") + "0");
                            modMITMR010.OrdDDat[j].OrdD_004 = Convert.ToString(modHanbai.NCnvZ(iKoumoku));
                            liBefF = 1;

                        }

                    }
                    j++;
                }


                modMITMR010.OrdHDat[0] = new modSinseiMarin.OrdHDatInfo()
                {
                    OrdH_001 = "",
                    OrdH_002 = "",
                    OrdH_003 = modHanbai.NCnvN(txtTokName.Text),
                    OrdH_0031 = Convert.ToInt32(modHanbai.NCnvZ(numTokCode.Text)),
                    OrdH_004 = modHanbai.NCnvN(txtTokTanto.Text),
                    OrdH_005 = modHanbai.NCnvN(txtIraiNo.Text),
                    OrdH_017 = modHanbai.NCnvN(txtOrderNo.Text),
                    OrdH_006 = modHanbai.NCnvN(txtVessel.Text),
                    OrdH_007 = optCountry_0.Checked ? "0" : "1",
                    OrdH_008 = modSac_Com.stComboGetKomoku(cboTanto.Text),
                    OrdH_0081 = modSac_Com.stComboGetKomoku(cboManila.Text),
                    OrdH_0082 = modSac_Com.stComboGetKomoku(cboMitsumori.Text),
                    OrdH_009 = modHanbai.NCnvN(txtMitsuNo.Text),
                    OrdH_010 = modHanbai.NCnvN(datMitsuYMD.Value),
                    OrdH_011 = modHanbai.NCnvN(txtMitsuNouki.Text),
                    OrdH_012 = modHanbai.NCnvN(txtOrigin.Text),
                    OrdH_013 = modSac_Com.stComboGetKomoku(cboCountry.Text),
                    OrdH_014 = modHanbai.NCnvN(txtTekiyo.Text),
                    OrdH_016 = Convert.ToInt32(numMitsuYuko.Text),
                    OrdH_027 = 0,
                    OrdH_022 = modHanbai.NCnvN(txtOName.Text),
                    OrdH_035 = Convert.ToInt32(modHanbai.NCnvZ(numOCode.Text)),
                    OrdH_037 = modHanbai.NCnvN(txtDoc.Text),
                    OrdH_038 = modHanbai.NCnvN(txtShipNo.Text)
                };
                TokMst = new modHanbai.TokMstInfo();
                TokMst.Tok_001 = Convert.ToDecimal( modMITMR010.OrdHDat[0].OrdH_031);
                if(modHanbai.GetTokMstInfo(ref TokMst,0))
                {
                    modMITMR010.OrdHDat[0].OrdH_015 = TokMst.Tok_037;
                    modMITMR010.OrdHDat[0].OrdH_022 = TokMst.Tok_016;
                    modMITMR010.OrdHDat[0].OrdH_023 = TokMst.Tok_003;
                    modMITMR010.OrdHDat[0].OrdH_024 = TokMst.Tok_008;
                    modMITMR010.OrdHDat[0].OrdH_025 = TokMst.Tok_010;
                    modMITMR010.OrdHDat[0].OrdH_026 = TokMst.Tok_049;
                    modMITMR010.OrdHDat[0].OrdH_0032 = TokMst.Tok_004;
                }

                modMITMR010.OrdMDat[0] = new modSinseiMarin.OrdMDatInfo()
                {
                    OrdM_001 = "",
                    OrdM_002 = modHanbai.NCnvN(txtMitsuNo.Text),
                    OrdM_003 = Convert.ToInt32(modHanbai.NCnvZ(numSEQ.Text)),
                    OrdM_004 = modHanbai.NCnvN(txtUnit.Text),
                    OrdM_005 = modHanbai.NCnvN(txtMaker.Text),
                    OrdM_006 = modHanbai.NCnvN(txtType.Text),
                    OrdM_007 = modHanbai.NCnvN(txtSerNo.Text),
                    OrdM_008 = modHanbai.NCnvN(txtDwgNo.Text),
                    OrdM_025 = modHanbai.NCnvN(txtAdditional.Text),
                    OrdM_009 = modHanbai.NCnvN(txtComment1.Text),
                    OrdM_010 = modHanbai.NCnvN(txtComment2.Text),
                    OrdM_011 = modHanbai.NCnvN(txtComment3.Text),
                    OrdM_012 = modHanbai.NCnvN(txtComment4.Text),
                    OrdM_013 = Convert.ToDecimal(modHanbai.NCnvZ(WK_MSuryo)),
                    OrdM_014 = Convert.ToDecimal(modHanbai.NCnvZ(WK_MKingaku))
                };

            }
            retbool = true;
            return retbool;
        }



        private bool checkIfhasOrder() //check if spddata is empty
        {

            foreach (DataGridViewRow row in spdData.Rows)
            {
                if (string.IsNullOrEmpty(row.Cells[modMITMR010.spdcol_MKinG].Value?.ToString()) == false)
                {
                    return true;
                }
            }
            return false;

        }
        private void cmdFunc_7_Click(object sender, EventArgs e)
        {
            int iRow;

            if(spdData.Rows.Count==0)
            {
                return;
            }
            
            iRow = spdData.Rows.Add();
            spdData.Rows[iRow].Cells[modMITMR010.spdcol_HinNm].Value = "PACKING AND DELIVERY CHARGE*BY MAKER";
            spdData.Rows[iRow].Cells[modMITMR010.spdcol_MSuryo].Value = "1";
            this.spdData.Focus();
            this.spdData.CurrentCell = this.spdData[modMITMR010.spdcol_MTanka, iRow];

        }


        private void cmdFunc_8_Click(object sender, EventArgs e)
        {
            BreakSeigyo(BefCtl);
        }

        private void cmdFunc_9_Click(object sender, EventArgs e)
        {
            CopyProc();
        }

        private void CopyProc()
        {
            int Idx;
            int IRow;
            int ICol;
            String lvWork;
            if(spdData.SelectedCells.Count<=0)            
               return;
            
            IRow = spdData.SelectedCells[0].RowIndex;
            ICol = spdData.SelectedCells[0].ColumnIndex;
            if (IRow < 1)
                return;
            if (IRow == spdData.Rows.Count - 1)
                spdData.Rows.Add();
            switch (ICol)
            {
                case modMITMR010.spdcol_CdNo:
                case modMITMR010.spdcol_NoS:
                    {
                        lvWork = modHanbai.NCnvN(spdData.Rows[IRow].Cells[modMITMR010.spdcol_NoS].Value?.ToString());
                        if (lvWork == "")
                        {
                            string prevcdno = spdData.Rows[IRow - 1].Cells[modMITMR010.spdcol_CdNo].Value?.ToString();
                            string prevcdnos = spdData.Rows[IRow - 1].Cells[modMITMR010.spdcol_NoS].Value?.ToString();
                            spdData.Rows[IRow].Cells[modMITMR010.spdcol_CdNo].Value = prevcdno;
                            spdData.Rows[IRow].Cells[modMITMR010.spdcol_NoS].Value = prevcdnos;
                            spdData.CurrentCell = spdData.Rows [IRow].Cells[modMITMR010.spdcol_HinNm];
                        }

                        break;
                    }
                case modMITMR010.spdcol_HinNm:
                    {
                        lvWork = modHanbai.NCnvN(spdData.Rows[IRow].Cells[modMITMR010.spdcol_HinNm].Value?.ToString());
                        if (lvWork == "")
                        {
                            string prevHinm = modHanbai.NCnvN(spdData.Rows[IRow - 1].Cells[modMITMR010.spdcol_HinNm].Value?.ToString());
                            spdData.Rows[IRow].Cells[modMITMR010.spdcol_HinNm].Value = prevHinm;
                            spdData.CurrentCell = spdData.Rows[IRow].Cells[modMITMR010.spdcol_HinNo];
                        }
                            break;
                    }

                default:
                    break;
            }


        }

        private void cmdFunc_11_Click(object sender, EventArgs e)
        {
            if (modSac_Com.ksExpMsgBox("見積書 " + modHanbai.gstGUIDE_Q010, "Q") == (int)Microsoft.VisualBasic.MsgBoxResult.Yes)
            {
                EditClear();
                DispModeProc();
                datMitsuYMD.Focus();
            }


        }

        private void BreakSeigyo(Control voCtrl)
        {
            string SQLtxt;
            String col_Seq;
            String col_No;

            SQLtxt = "";
            SQLtxt = SQLtxt + " SELECT * FROM OrdH_Dat";
            SQLtxt = SQLtxt + " WHERE OrdH_009 ='" + txtMitsuNo.Text + "'";

            DBService db= new DBService();
            DataTable dt = new DataTable();

            dt = db.GetDatafromDB(SQLtxt);
            if (dt.Rows.Count <= 0) 
            {
                return;
            }

            if(voCtrl == spdData)
            {
                if(spdData.SelectedCells.Count>0)
                {
                    switch (spdData.SelectedCells[0].ColumnIndex)
                    {
                        case (modMITMR010.spdcol_HinNm):
                        {
                                col_Seq = modHanbai.NCnvN(spdData.Rows[spdData.SelectedCells[0].RowIndex].Cells[6].Value);
                                col_No =  modHanbai.NCnvN(spdData.Rows[spdData.SelectedCells[0].RowIndex].Cells[7].Value);
                                frmBreakRow34 frm = new frmBreakRow34();
                                frm.L_SEQ = col_Seq;
                                frm.L_NO = col_No;
                                frm.L_KNO = txtMitsuNo.Text;
                                frm.L_VALUE = modHanbai.NCnvN(spdData.SelectedCells[0].Value.ToString());
                                frm.ShowDialog(this);
                                if(frm.G_STATUS==true)
                                {
                                    spdData.SelectedCells[0].Value = frm.G_VALUE;
                                }

                                break;
                        }
                        case (modMITMR010.spdcol_HinNo):
                            {
                                frmBreakRow16 frm = new frmBreakRow16();
                                frm.L_VALUE = modHanbai.NCnvN(spdData.SelectedCells[0].Value.ToString());
                                frm.ShowDialog(this);
                                if (frm.G_STATUS == true)
                                {
                                    spdData.SelectedCells[0].Value = frm.G_VALUE;
                                }
                                break;
                            }

                        default:
                            break;
                    }

                }

            }
            if(voCtrl == txtOrigin)
            {
                frmBreakRow60 frm = new frmBreakRow60();
                frm.L_VALUE = modHanbai.NCnvN(txtOrigin.Text.Trim());
                frm.L_NO= modHanbai.NCnvN(txtMitsuNo.Text.Trim());
                frm.L_TYPE = "ORIGIN";
                frm.ShowDialog(this);
                if (frm.G_STATUS == true)
                {
                    txtOrigin.Text  = frm.G_VALUE;
                }
                txtOrigin.Focus();
            }
            if (voCtrl == txtMitsuNouki)
            {
                frmBreakRow60 frm = new frmBreakRow60();
                frm.L_VALUE = modHanbai.NCnvN(txtMitsuNouki.Text.Trim());
                frm.L_NO = modHanbai.NCnvN(txtMitsuNo.Text.Trim());
                frm.L_TYPE = "Delivery Time";
                frm.ShowDialog(this);
                if (frm.G_STATUS == true)
                {
                    txtMitsuNouki.Text = frm.G_VALUE;
                }
                txtMitsuNouki.Focus();
            }
            if (voCtrl == txtTekiyo)
            {
                frmBreakRow60 frm = new frmBreakRow60();
                frm.L_VALUE = modHanbai.NCnvN(txtTekiyo.Text.Trim());
                frm.L_NO = modHanbai.NCnvN(txtMitsuNo.Text.Trim());
                frm.L_TYPE = "NOTE";
                frm.ShowDialog(this);
                if (frm.G_STATUS == true)
                {
                    txtTekiyo.Text = frm.G_VALUE;
                }
                txtTekiyo.Focus();
            }

        }

        private void frmMITMR010_Enter(object sender, EventArgs e)
        {

        }

        private void spdData_Enter(object sender, EventArgs e)
        {
            GotFocusSec(spdData, "明細情報" + modHanbai.gstGUIDE_M001, 1);
        }

        private void txtOrigin_Enter(object sender, EventArgs e)
        {
            BefCtl = txtOrigin;
        }

        private void txtMitsuNouki_Enter(object sender, EventArgs e)
        {
            BefCtl = txtMitsuNouki;
        }

        private void txtTekiyo_Enter(object sender, EventArgs e)
        {

        }

        private void cmdFunc_5_Click(object sender, EventArgs e)
        {
            modMITMR010.FlgF6 = false;
            PrintOutProc();
        }

        private void PrintOutProc()
        {

            Array.Resize(ref modMITMR010P.ourRefNoP, 1);
            modMITMR010P.ourRefNoP[0] = "";
            if(modHanbai.NCnvN(txtMitsuNo.Text) == "")
            {
                modSac_Com.ksExpMsgBox("Please input " + label2.Text +"'", "E");
                txtMitsuNo.Focus();
                return;
            }
            else
            {
                modMITMR010P.ourRefNoP[0] = txtMitsuNo.Text;
            }
            if(modSac_Com.ksExpMsgBox("見積書 " + modHanbai.gstGUIDE_Q010 , "Q") == (int)Microsoft.VisualBasic.MsgBoxResult.Yes)
            {
                
                modMITMR010P.numtok = numTokCode.Text;
                modMITMR010P.maker = txtMaker.Text;
                modMITMR010P.origin = txtOrigin.Text;

                if (modMITMR010P.MITMR020PrintProc(Convert.ToInt16(modMITMR010P.enmPrintmode.Preview))==true)
                {
                    EditClear();
                }

            }


        }


        private void PrintOutProc2()
        {

            Array.Resize(ref modMITMR010P.ourRefNoP, 1);
            modMITMR010P.ourRefNoP[0] = "";
            if (modHanbai.NCnvN(txtMitsuNo.Text) == "")
            {
                modSac_Com.ksExpMsgBox("Please input " + label2.Text + "'", "E");
                txtMitsuNo.Focus();
                return;
            }
            else
            {
                modMITMR010P.ourRefNoP[0] = txtMitsuNo.Text;
            }
            if (modSac_Com.ksExpMsgBox("見積書 " + modHanbai.gstGUIDE_Q010, "Q") == (int)Microsoft.VisualBasic.MsgBoxResult.Yes)
            {

                modMITMR010P.numtok = numTokCode.Text;
                modMITMR010P.maker = txtMaker.Text;
                modMITMR010P.origin = txtOrigin.Text;

                if (modMITMR010P.MITMR020PrintProc(Convert.ToInt16(modMITMR010P.enmPrintmode.Preview)) == true)
                {
                    EditClear();
                }

            }


        }


        private void cmdFunc_6_Click(object sender, EventArgs e)
        {
            modMITMR010.FlgF6 = true;
            PrintOutProc2();
        }

       

        private void spdData_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void spdData_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;
            if (e.ColumnIndex == modMITMR010.spdcol_MSuryo || e.ColumnIndex == modMITMR010.spdcol_MTanka)
            {
                DataGridViewTextBoxCell cellAmount = (DataGridViewTextBoxCell)spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_MKinG];
                DataGridViewTextBoxCell cellQty = (DataGridViewTextBoxCell)spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_MSuryo];
                DataGridViewTextBoxCell cellUnit = (DataGridViewTextBoxCell)spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_MTanka];


                DataGridViewTextBoxCell cellUnitType = (DataGridViewTextBoxCell)spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_MTani];

                if (!string.IsNullOrEmpty(cellQty.Value?.ToString()))
                    cellUnitType.Value = cellQty.Value.ToString() == "1" ? "PC" : "PCS";
                else
                    cellUnitType.Value = "";
                try
                {
                    cellAmount.Value = Convert.ToDecimal(cellQty.Value) * Convert.ToDecimal(cellUnit.Value);
                }
                catch(Exception ex)
                {

                }
               
       
            }

            if (e.ColumnIndex == modMITMR010.spdcol_HinNm )
            {
                string strdesc = spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_HinNm].Value.ToString();

                Boolean isDiscount = false;
                Boolean isAddtl = false;

                isDiscount = strdesc.Contains("% DISCOUNT");
                isAddtl = strdesc.Contains("% ON");

                if (spdData.Rows.Count > 1 && (isDiscount || isAddtl))
                {
                    Int32 totalamt = 0;

                    for (int i = 0; i < spdData.Rows.Count; i++)
                    {
                        if(i!= e.RowIndex)
                        {
                            Int32 amounttoadd = 0;

                            try
                            {
                                amounttoadd = Convert.ToInt32(Math.Floor(Convert.ToDouble(spdData.Rows[i].Cells[modMITMR010.spdcol_MKinG].Value.ToString())));
                            }
                            catch (Exception)
                            {
                            }
            

                            totalamt += amounttoadd;
                             


                        }

                    }

                    string strbeforepercent = strdesc.Split('%')[0];
                    Int32 percent = 0;
                    if (Int32.TryParse(strbeforepercent, out percent))
                    {
                        spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_MSuryo].Value = 1;
                        spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_MTani].Value = "";
                        Int32 finalamt = (totalamt * percent * (isDiscount ? (-1) : 1)) / 100;
                        spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_MTanka].Value = finalamt;
                        spdData.Rows[e.RowIndex].Cells[modMITMR010.spdcol_MKinG].Value = finalamt;
                    }
                }
            }

        }

        private void numSEQ_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (modMITMR010.OrdMDatReadProc(modHanbai.NCnvN(txtMitsuNo.Text), Convert.ToInt16(numSEQ.Text)) == true)
                {
                    OrdMInfoSetProc();
                    if(modMITMR010.OrdDDatReadProc(modHanbai.NCnvN(txtMitsuNo.Text), Convert.ToInt16(numSEQ.Text)) == true)
                    {
                        OrdDInfoSetProc();
                    }
                }
                else
                    EditClear(1);


            }
        }

        private void numSEQ_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmMITMR010_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    this.cmdFunc_1_Click(null, null);
                    break;
                case Keys.F2:
                    this.cmdFunc_2_Click(null, null);
                    break;
                case Keys.F3:
                    this.cmdFunc_3_Click(null, null);
                    break;
                case Keys.F4:
                    this.cmdFunc_4_Click(null, null);
                    break;
                case Keys.F5:
                    this.cmdFunc_5_Click(null, null);
                    break;
                case Keys.F6:
                    this.cmdFunc_6_Click(null, null);
                    break;
                case Keys.F7:
                    this.cmdFunc_7_Click(null, null);
                    break;
                case Keys.F8:
                    this.cmdFunc_8_Click(null, null);
                    break;

                case Keys.F9:
                    this.cmdFunc_9_Click(null, null);
                    break;

                case Keys.F10:
                    this.cmdFunc_10_Click(null, null);
                    break;

                case Keys.F11:
                    this.cmdFunc_11_Click(null, null);
                    break;
                case Keys.F12:
                    this.cmdFunc_12_Click(null, null);
                    break;
            }
        }

        private void cmdFunc_12_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Are You sure you want to exit?","EXIT APPLICATION",MessageBoxButtons.YesNo)==DialogResult.Yes)
            this.Close();
        }

        private void mnuCopy_Click_1(object sender, EventArgs e)
        {
            if (spdData.GetClipboardContent() == null)
            {
                MessageBox.Show("Nothing was selected", "Information", MessageBoxButtons.OK);
            }
            else
            {
                Clipboard.SetDataObject(this.spdData.GetClipboardContent());
            }


        }

        private void mnuPaste_Click_1(object sender, EventArgs e)
        {
            pastefromClipboard();
        }

        private void spdData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter)
                return;
            int icolumn = spdData.CurrentCell.ColumnIndex;
            int iRow = spdData.CurrentCell.RowIndex;
            switch (icolumn)
            {

                case 8://description
                    {
                        if (!String.IsNullOrEmpty(spdData.Rows[iRow].Cells[icolumn].Value?.ToString()))
                            break;
                        frmRefSyoTbl frm = new frmRefSyoTbl();
                        frm.ShowDialog(this);
                        if (frm.Status() == true)
                        {
                            spdData.Rows[iRow].Cells[icolumn].Value = frm.STM_HINMEI();
                        }
                        if (spdData.CurrentCell.RowIndex == spdData.Rows.Count - 1)
                        {
                            spdData.Rows.Add();
                            spdData.Refresh();
                        }
                        break;
                    }

            }

            Console.WriteLine(icolumn.ToString() + "," +notlastColumn.ToString() + "," + (spdData.CurrentCell.RowIndex == spdData.Rows.Count - 1).ToString());
            if (e.KeyCode == Keys.Enter && (notlastColumn && icolumn<15)) //if not last column move to nex
            {
                if(spdData.CurrentCell.RowIndex != spdData.Rows.Count -1 )
                SendKeys.Send("{up}");
                SendKeys.Send("{right}");
            }
            else if (e.KeyCode == Keys.Enter)
            {

                SendKeys.Send("{home}");//go to first column
                notlastColumn = true;
            }
            

        }

        private void spdData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
                            if(e.RowIndex == spdData.Rows.Count -1)
                {
                    spdData.Rows.Add();
                    return;
                }


            if ( e.ColumnIndex>=15)  //if last column
            {

                KeyEventArgs forKeyDown = new KeyEventArgs(Keys.Enter);
                notlastColumn = false;
                spdData_KeyDown(spdData, forKeyDown);
            }
            else
            {

                SendKeys.Send("{up}");
                SendKeys.Send("{right}");
            }

        }

        private void spdData_SelectionChanged(object sender, EventArgs e)
        {
       
        }

        private void Command1_2_Click(object sender, EventArgs e)
        {

            string origin = "";
            if (((Button)sender).Text.ToString() == "GENUINE(1)")
                origin = "1";
            else if (((Button)sender).Text.ToString() == "OEM(0)")
                origin = "0";
            else
                origin = "";

            for (int i = 0; i < spdData.Rows.Count-1; i++)
            {
                if (spdData.Rows[i].Cells[modMITMR010.spdcol_MTani].Value?.ToString()!="")
                {
                    spdData.Rows[i].Cells[modMITMR010.spdcol_Origin].Value = origin;
                }
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            int rowindex = spdData.CurrentCell.RowIndex;
                spdData.Rows.Insert(rowindex , 1);


        }
    }
}


