﻿namespace EstimateProcessing
{
    partial class frmBreakRow34
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmdFunc_11 = new System.Windows.Forms.Button();
            this.cmdFunc_12 = new System.Windows.Forms.Button();
            this.Frame__Jyouken = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVALUE_8 = new System.Windows.Forms.TextBox();
            this.txtVALUE_7 = new System.Windows.Forms.TextBox();
            this.txtVALUE_6 = new System.Windows.Forms.TextBox();
            this.txtVALUE_5 = new System.Windows.Forms.TextBox();
            this.txtVALUE_4 = new System.Windows.Forms.TextBox();
            this.txtVALUE_3 = new System.Windows.Forms.TextBox();
            this.txtVALUE_2 = new System.Windows.Forms.TextBox();
            this.txtVALUE_1 = new System.Windows.Forms.TextBox();
            this.txtVALUE_0 = new System.Windows.Forms.TextBox();
            this.txtMemori = new System.Windows.Forms.TextBox();
            this.txtMoto = new System.Windows.Forms.TextBox();
            this.imText1 = new System.Windows.Forms.TextBox();
            this.lblGuide = new System.Windows.Forms.Label();
            this.Frame__Jyouken.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("MS Mincho", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(348, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "改行・文字数制御";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdFunc_11
            // 
            this.cmdFunc_11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_11.Font = new System.Drawing.Font("MS Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFunc_11.Location = new System.Drawing.Point(394, 0);
            this.cmdFunc_11.Name = "cmdFunc_11";
            this.cmdFunc_11.Size = new System.Drawing.Size(87, 36);
            this.cmdFunc_11.TabIndex = 1;
            this.cmdFunc_11.Text = "戻る(F11)";
            this.cmdFunc_11.UseVisualStyleBackColor = false;
            this.cmdFunc_11.Click += new System.EventHandler(this.cmdFunc_11_Click);
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_12.Font = new System.Drawing.Font("MS Gothic", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFunc_12.Location = new System.Drawing.Point(483, 0);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(89, 36);
            this.cmdFunc_12.TabIndex = 2;
            this.cmdFunc_12.Text = "確定(F12)";
            this.cmdFunc_12.UseVisualStyleBackColor = false;
            this.cmdFunc_12.Click += new System.EventHandler(this.cmdFunc_12_Click);
            // 
            // Frame__Jyouken
            // 
            this.Frame__Jyouken.Controls.Add(this.label10);
            this.Frame__Jyouken.Controls.Add(this.label9);
            this.Frame__Jyouken.Controls.Add(this.label8);
            this.Frame__Jyouken.Controls.Add(this.label7);
            this.Frame__Jyouken.Controls.Add(this.label6);
            this.Frame__Jyouken.Controls.Add(this.label5);
            this.Frame__Jyouken.Controls.Add(this.label4);
            this.Frame__Jyouken.Controls.Add(this.label3);
            this.Frame__Jyouken.Controls.Add(this.label2);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_8);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_7);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_6);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_5);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_4);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_3);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_2);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_1);
            this.Frame__Jyouken.Controls.Add(this.txtVALUE_0);
            this.Frame__Jyouken.Controls.Add(this.txtMemori);
            this.Frame__Jyouken.Controls.Add(this.txtMoto);
            this.Frame__Jyouken.Controls.Add(this.imText1);
            this.Frame__Jyouken.Location = new System.Drawing.Point(16, 58);
            this.Frame__Jyouken.Name = "Frame__Jyouken";
            this.Frame__Jyouken.Size = new System.Drawing.Size(544, 460);
            this.Frame__Jyouken.TabIndex = 3;
            this.Frame__Jyouken.TabStop = false;
            this.Frame__Jyouken.Enter += new System.EventHandler(this.Frame__Jyouken_Enter);
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(23, 410);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 24);
            this.label10.TabIndex = 20;
            this.label10.Text = "9行目";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(23, 379);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 24);
            this.label9.TabIndex = 19;
            this.label9.Text = "8行目";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(23, 349);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 24);
            this.label8.TabIndex = 18;
            this.label8.Text = "7行目";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 319);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 24);
            this.label7.TabIndex = 17;
            this.label7.Text = "6行目";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 24);
            this.label6.TabIndex = 16;
            this.label6.Text = "5行目";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 19);
            this.label5.TabIndex = 15;
            this.label5.Text = "4行目";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 19);
            this.label4.TabIndex = 14;
            this.label4.Text = "3行目";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 199);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 21);
            this.label3.TabIndex = 13;
            this.label3.Text = "2行目";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("MS Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 24);
            this.label2.TabIndex = 12;
            this.label2.Text = "1行目bel2";
            // 
            // txtVALUE_8
            // 
            this.txtVALUE_8.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_8.Location = new System.Drawing.Point(76, 406);
            this.txtVALUE_8.MaxLength = 34;
            this.txtVALUE_8.Name = "txtVALUE_8";
            this.txtVALUE_8.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_8.TabIndex = 11;
            // 
            // txtVALUE_7
            // 
            this.txtVALUE_7.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_7.Location = new System.Drawing.Point(76, 376);
            this.txtVALUE_7.MaxLength = 34;
            this.txtVALUE_7.Name = "txtVALUE_7";
            this.txtVALUE_7.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_7.TabIndex = 10;
            // 
            // txtVALUE_6
            // 
            this.txtVALUE_6.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_6.Location = new System.Drawing.Point(76, 346);
            this.txtVALUE_6.MaxLength = 34;
            this.txtVALUE_6.Name = "txtVALUE_6";
            this.txtVALUE_6.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_6.TabIndex = 9;
            // 
            // txtVALUE_5
            // 
            this.txtVALUE_5.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_5.Location = new System.Drawing.Point(76, 316);
            this.txtVALUE_5.MaxLength = 34;
            this.txtVALUE_5.Name = "txtVALUE_5";
            this.txtVALUE_5.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_5.TabIndex = 8;
            // 
            // txtVALUE_4
            // 
            this.txtVALUE_4.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_4.Location = new System.Drawing.Point(76, 286);
            this.txtVALUE_4.MaxLength = 34;
            this.txtVALUE_4.Name = "txtVALUE_4";
            this.txtVALUE_4.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_4.TabIndex = 7;
            // 
            // txtVALUE_3
            // 
            this.txtVALUE_3.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_3.Location = new System.Drawing.Point(76, 256);
            this.txtVALUE_3.MaxLength = 34;
            this.txtVALUE_3.Name = "txtVALUE_3";
            this.txtVALUE_3.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_3.TabIndex = 6;
            // 
            // txtVALUE_2
            // 
            this.txtVALUE_2.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_2.Location = new System.Drawing.Point(76, 226);
            this.txtVALUE_2.MaxLength = 34;
            this.txtVALUE_2.Name = "txtVALUE_2";
            this.txtVALUE_2.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_2.TabIndex = 5;
            // 
            // txtVALUE_1
            // 
            this.txtVALUE_1.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_1.Location = new System.Drawing.Point(76, 196);
            this.txtVALUE_1.MaxLength = 34;
            this.txtVALUE_1.Name = "txtVALUE_1";
            this.txtVALUE_1.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_1.TabIndex = 4;
            // 
            // txtVALUE_0
            // 
            this.txtVALUE_0.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVALUE_0.Location = new System.Drawing.Point(76, 166);
            this.txtVALUE_0.MaxLength = 34;
            this.txtVALUE_0.Name = "txtVALUE_0";
            this.txtVALUE_0.Size = new System.Drawing.Size(317, 24);
            this.txtVALUE_0.TabIndex = 3;
            this.txtVALUE_0.TextChanged += new System.EventHandler(this.txtVALUE_0_TextChanged);
            this.txtVALUE_0.Enter += new System.EventHandler(this.txtValue_GotFocus);
            // 
            // txtMemori
            // 
            this.txtMemori.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtMemori.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtMemori.Location = new System.Drawing.Point(76, 136);
            this.txtMemori.Name = "txtMemori";
            this.txtMemori.Size = new System.Drawing.Size(317, 24);
            this.txtMemori.TabIndex = 2;
            this.txtMemori.Text = "---------------1---------------2---------------3---------------4---------------5-" +
    "--------------6";
            // 
            // txtMoto
            // 
            this.txtMoto.Font = new System.Drawing.Font("Courier New", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMoto.Location = new System.Drawing.Point(21, 48);
            this.txtMoto.Multiline = true;
            this.txtMoto.Name = "txtMoto";
            this.txtMoto.Size = new System.Drawing.Size(505, 73);
            this.txtMoto.TabIndex = 1;
            this.txtMoto.Enter += new System.EventHandler(this.txtMoto_Enter);
            this.txtMoto.Leave += new System.EventHandler(this.txtMoto_Leave);
            // 
            // imText1
            // 
            this.imText1.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.imText1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.imText1.Location = new System.Drawing.Point(21, 19);
            this.imText1.Name = "imText1";
            this.imText1.Size = new System.Drawing.Size(505, 24);
            this.imText1.TabIndex = 0;
            this.imText1.Text = "---------------1---------------2---------------3---------------4---------------5-" +
    "--------------6";
            // 
            // lblGuide
            // 
            this.lblGuide.BackColor = System.Drawing.Color.Black;
            this.lblGuide.Font = new System.Drawing.Font("MS Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGuide.ForeColor = System.Drawing.Color.White;
            this.lblGuide.Location = new System.Drawing.Point(16, 521);
            this.lblGuide.Name = "lblGuide";
            this.lblGuide.Size = new System.Drawing.Size(544, 23);
            this.lblGuide.TabIndex = 4;
            this.lblGuide.Text = "ｺｰﾄﾞを入力して下さい。";
            this.lblGuide.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmBreakRow34
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 569);
            this.Controls.Add(this.lblGuide);
            this.Controls.Add(this.Frame__Jyouken);
            this.Controls.Add(this.cmdFunc_12);
            this.Controls.Add(this.cmdFunc_11);
            this.Controls.Add(this.label1);
            this.Name = "frmBreakRow34";
            this.Text = "改行・文字数制御 (frmBreakRow34)";
            this.Load += new System.EventHandler(this.frmBreakRow34_Load);
            this.Frame__Jyouken.ResumeLayout(false);
            this.Frame__Jyouken.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdFunc_11;
        private System.Windows.Forms.Button cmdFunc_12;
        private System.Windows.Forms.GroupBox Frame__Jyouken;
        private System.Windows.Forms.TextBox txtVALUE_8;
        private System.Windows.Forms.TextBox txtVALUE_7;
        private System.Windows.Forms.TextBox txtVALUE_6;
        private System.Windows.Forms.TextBox txtVALUE_5;
        private System.Windows.Forms.TextBox txtVALUE_4;
        private System.Windows.Forms.TextBox txtVALUE_3;
        private System.Windows.Forms.TextBox txtVALUE_2;
        private System.Windows.Forms.TextBox txtVALUE_1;
        private System.Windows.Forms.TextBox txtVALUE_0;
        private System.Windows.Forms.TextBox txtMemori;
        private System.Windows.Forms.TextBox txtMoto;
        private System.Windows.Forms.TextBox imText1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblGuide;
    }
}