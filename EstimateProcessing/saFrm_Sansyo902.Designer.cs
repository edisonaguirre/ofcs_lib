﻿namespace EstimateProcessing
{
    partial class saFrm_Sansyo902
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmdFunc_10 = new System.Windows.Forms.Button();
            this.cmdFunc_11 = new System.Windows.Forms.Button();
            this.cmdFunc_12 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmdSansyo = new System.Windows.Forms.Button();
            this.txtHinban = new System.Windows.Forms.TextBox();
            this.txtHinmei = new System.Windows.Forms.TextBox();
            this.txtSerch_6 = new System.Windows.Forms.TextBox();
            this.txtSerch_5 = new System.Windows.Forms.TextBox();
            this.txtSerch_4 = new System.Windows.Forms.TextBox();
            this.txtSerch_3 = new System.Windows.Forms.TextBox();
            this.txtSerch_2 = new System.Windows.Forms.TextBox();
            this.txtSerch_1 = new System.Windows.Forms.TextBox();
            this.txtSerch_0 = new System.Windows.Forms.TextBox();
            this.lblPartNo = new System.Windows.Forms.Label();
            this.lblSName = new System.Windows.Forms.Label();
            this.lblSerch_6 = new System.Windows.Forms.Label();
            this.lblSerch_5 = new System.Windows.Forms.Label();
            this.lblSerch_4 = new System.Windows.Forms.Label();
            this.lblSerch_3 = new System.Windows.Forms.Label();
            this.lblSerch_2 = new System.Windows.Forms.Label();
            this.lblSerch_1 = new System.Windows.Forms.Label();
            this.lblSerch_0 = new System.Windows.Forms.Label();
            this.spdJisseki = new System.Windows.Forms.DataGridView();
            this.lblGuide = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdJisseki)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Blue;
            this.label1.Font = new System.Drawing.Font("MS Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(-9, -3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "受注実績データ参照";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdFunc_10
            // 
            this.cmdFunc_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_10.Location = new System.Drawing.Point(726, 12);
            this.cmdFunc_10.Name = "cmdFunc_10";
            this.cmdFunc_10.Size = new System.Drawing.Size(82, 37);
            this.cmdFunc_10.TabIndex = 5;
            this.cmdFunc_10.TabStop = false;
            this.cmdFunc_10.Text = "登録(F10)";
            this.cmdFunc_10.UseVisualStyleBackColor = false;
            this.cmdFunc_10.Click += new System.EventHandler(this.cmdFunc_10_Click);
            // 
            // cmdFunc_11
            // 
            this.cmdFunc_11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_11.Location = new System.Drawing.Point(807, 12);
            this.cmdFunc_11.Name = "cmdFunc_11";
            this.cmdFunc_11.Size = new System.Drawing.Size(75, 37);
            this.cmdFunc_11.TabIndex = 4;
            this.cmdFunc_11.TabStop = false;
            this.cmdFunc_11.Text = "戻る(F11)";
            this.cmdFunc_11.UseVisualStyleBackColor = false;
            this.cmdFunc_11.Click += new System.EventHandler(this.cmdFunc_11_Click);
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_12.Location = new System.Drawing.Point(881, 12);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(83, 37);
            this.cmdFunc_12.TabIndex = 3;
            this.cmdFunc_12.TabStop = false;
            this.cmdFunc_12.Text = "確定(F12)";
            this.cmdFunc_12.UseVisualStyleBackColor = false;
            this.cmdFunc_12.Click += new System.EventHandler(this.cmdFunc_12_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmdSansyo);
            this.groupBox1.Controls.Add(this.txtHinban);
            this.groupBox1.Controls.Add(this.txtHinmei);
            this.groupBox1.Controls.Add(this.txtSerch_6);
            this.groupBox1.Controls.Add(this.txtSerch_5);
            this.groupBox1.Controls.Add(this.txtSerch_4);
            this.groupBox1.Controls.Add(this.txtSerch_3);
            this.groupBox1.Controls.Add(this.txtSerch_2);
            this.groupBox1.Controls.Add(this.txtSerch_1);
            this.groupBox1.Controls.Add(this.txtSerch_0);
            this.groupBox1.Controls.Add(this.lblPartNo);
            this.groupBox1.Controls.Add(this.lblSName);
            this.groupBox1.Controls.Add(this.lblSerch_6);
            this.groupBox1.Controls.Add(this.lblSerch_5);
            this.groupBox1.Controls.Add(this.lblSerch_4);
            this.groupBox1.Controls.Add(this.lblSerch_3);
            this.groupBox1.Controls.Add(this.lblSerch_2);
            this.groupBox1.Controls.Add(this.lblSerch_1);
            this.groupBox1.Controls.Add(this.lblSerch_0);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox1.Location = new System.Drawing.Point(12, 55);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(952, 244);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "検索条件";
            // 
            // cmdSansyo
            // 
            this.cmdSansyo.Location = new System.Drawing.Point(795, 171);
            this.cmdSansyo.Name = "cmdSansyo";
            this.cmdSansyo.Size = new System.Drawing.Size(75, 48);
            this.cmdSansyo.TabIndex = 7;
            this.cmdSansyo.TabStop = false;
            this.cmdSansyo.Text = "検索";
            this.cmdSansyo.UseVisualStyleBackColor = true;
            // 
            // txtHinban
            // 
            this.txtHinban.Location = new System.Drawing.Point(243, 197);
            this.txtHinban.Name = "txtHinban";
            this.txtHinban.Size = new System.Drawing.Size(531, 23);
            this.txtHinban.TabIndex = 15;
            // 
            // txtHinmei
            // 
            this.txtHinmei.Location = new System.Drawing.Point(243, 171);
            this.txtHinmei.Name = "txtHinmei";
            this.txtHinmei.Size = new System.Drawing.Size(531, 23);
            this.txtHinmei.TabIndex = 14;
            // 
            // txtSerch_6
            // 
            this.txtSerch_6.Location = new System.Drawing.Point(592, 137);
            this.txtSerch_6.Name = "txtSerch_6";
            this.txtSerch_6.Size = new System.Drawing.Size(182, 23);
            this.txtSerch_6.TabIndex = 16;
            this.txtSerch_6.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // txtSerch_5
            // 
            this.txtSerch_5.Location = new System.Drawing.Point(592, 110);
            this.txtSerch_5.Name = "txtSerch_5";
            this.txtSerch_5.Size = new System.Drawing.Size(182, 23);
            this.txtSerch_5.TabIndex = 13;
            // 
            // txtSerch_4
            // 
            this.txtSerch_4.Location = new System.Drawing.Point(592, 84);
            this.txtSerch_4.Name = "txtSerch_4";
            this.txtSerch_4.Size = new System.Drawing.Size(182, 23);
            this.txtSerch_4.TabIndex = 12;
            // 
            // txtSerch_3
            // 
            this.txtSerch_3.Location = new System.Drawing.Point(592, 58);
            this.txtSerch_3.Name = "txtSerch_3";
            this.txtSerch_3.Size = new System.Drawing.Size(182, 23);
            this.txtSerch_3.TabIndex = 11;
            // 
            // txtSerch_2
            // 
            this.txtSerch_2.Location = new System.Drawing.Point(592, 32);
            this.txtSerch_2.Name = "txtSerch_2";
            this.txtSerch_2.Size = new System.Drawing.Size(182, 23);
            this.txtSerch_2.TabIndex = 10;
            // 
            // txtSerch_1
            // 
            this.txtSerch_1.Location = new System.Drawing.Point(243, 60);
            this.txtSerch_1.Name = "txtSerch_1";
            this.txtSerch_1.Size = new System.Drawing.Size(182, 23);
            this.txtSerch_1.TabIndex = 9;
            // 
            // txtSerch_0
            // 
            this.txtSerch_0.Location = new System.Drawing.Point(243, 33);
            this.txtSerch_0.Name = "txtSerch_0";
            this.txtSerch_0.Size = new System.Drawing.Size(182, 23);
            this.txtSerch_0.TabIndex = 8;
            this.txtSerch_0.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblPartNo
            // 
            this.lblPartNo.BackColor = System.Drawing.Color.Blue;
            this.lblPartNo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblPartNo.ForeColor = System.Drawing.Color.White;
            this.lblPartNo.Location = new System.Drawing.Point(67, 194);
            this.lblPartNo.Name = "lblPartNo";
            this.lblPartNo.Size = new System.Drawing.Size(171, 25);
            this.lblPartNo.TabIndex = 8;
            this.lblPartNo.Text = "　品番(Part No)";
            // 
            // lblSName
            // 
            this.lblSName.BackColor = System.Drawing.Color.Blue;
            this.lblSName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSName.ForeColor = System.Drawing.Color.White;
            this.lblSName.Location = new System.Drawing.Point(67, 169);
            this.lblSName.Name = "lblSName";
            this.lblSName.Size = new System.Drawing.Size(171, 25);
            this.lblSName.TabIndex = 7;
            this.lblSName.Text = "　品名(Description)";
            // 
            // lblSerch_6
            // 
            this.lblSerch_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblSerch_6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSerch_6.Location = new System.Drawing.Point(475, 136);
            this.lblSerch_6.Name = "lblSerch_6";
            this.lblSerch_6.Size = new System.Drawing.Size(111, 25);
            this.lblSerch_6.TabIndex = 6;
            this.lblSerch_6.Text = "DWG/NO";
            // 
            // lblSerch_5
            // 
            this.lblSerch_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblSerch_5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSerch_5.Location = new System.Drawing.Point(475, 110);
            this.lblSerch_5.Name = "lblSerch_5";
            this.lblSerch_5.Size = new System.Drawing.Size(111, 25);
            this.lblSerch_5.TabIndex = 5;
            this.lblSerch_5.Text = "SER/NO";
            // 
            // lblSerch_4
            // 
            this.lblSerch_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblSerch_4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSerch_4.Location = new System.Drawing.Point(475, 84);
            this.lblSerch_4.Name = "lblSerch_4";
            this.lblSerch_4.Size = new System.Drawing.Size(111, 25);
            this.lblSerch_4.TabIndex = 4;
            this.lblSerch_4.Text = "Type";
            // 
            // lblSerch_3
            // 
            this.lblSerch_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblSerch_3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSerch_3.Location = new System.Drawing.Point(475, 58);
            this.lblSerch_3.Name = "lblSerch_3";
            this.lblSerch_3.Size = new System.Drawing.Size(111, 25);
            this.lblSerch_3.TabIndex = 3;
            this.lblSerch_3.Text = "Maker";
            // 
            // lblSerch_2
            // 
            this.lblSerch_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblSerch_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSerch_2.Location = new System.Drawing.Point(475, 32);
            this.lblSerch_2.Name = "lblSerch_2";
            this.lblSerch_2.Size = new System.Drawing.Size(111, 25);
            this.lblSerch_2.TabIndex = 2;
            this.lblSerch_2.Text = "Unit";
            // 
            // lblSerch_1
            // 
            this.lblSerch_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblSerch_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSerch_1.Location = new System.Drawing.Point(67, 59);
            this.lblSerch_1.Name = "lblSerch_1";
            this.lblSerch_1.Size = new System.Drawing.Size(171, 25);
            this.lblSerch_1.TabIndex = 1;
            this.lblSerch_1.Text = " 船名(Vessel\'s Name)";
            // 
            // lblSerch_0
            // 
            this.lblSerch_0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.lblSerch_0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSerch_0.Location = new System.Drawing.Point(67, 33);
            this.lblSerch_0.Name = "lblSerch_0";
            this.lblSerch_0.Size = new System.Drawing.Size(171, 25);
            this.lblSerch_0.TabIndex = 0;
            this.lblSerch_0.Text = " 得意先名(Messrs)";
            // 
            // spdJisseki
            // 
            this.spdJisseki.AllowUserToAddRows = false;
            this.spdJisseki.AllowUserToDeleteRows = false;
            this.spdJisseki.AllowUserToResizeColumns = false;
            this.spdJisseki.AllowUserToResizeRows = false;
            this.spdJisseki.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.spdJisseki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spdJisseki.Location = new System.Drawing.Point(17, 312);
            this.spdJisseki.MultiSelect = false;
            this.spdJisseki.Name = "spdJisseki";
            this.spdJisseki.ReadOnly = true;
            this.spdJisseki.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.spdJisseki.Size = new System.Drawing.Size(946, 421);
            this.spdJisseki.TabIndex = 2;
            this.spdJisseki.TabStop = false;
            // 
            // lblGuide
            // 
            this.lblGuide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblGuide.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblGuide.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGuide.ForeColor = System.Drawing.Color.White;
            this.lblGuide.Location = new System.Drawing.Point(16, 736);
            this.lblGuide.Name = "lblGuide";
            this.lblGuide.Size = new System.Drawing.Size(948, 30);
            this.lblGuide.TabIndex = 6;
            this.lblGuide.Text = "ｺｰﾄﾞを入力して下さい。";
            this.lblGuide.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // saFrm_Sansyo902
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(989, 786);
            this.Controls.Add(this.lblGuide);
            this.Controls.Add(this.spdJisseki);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmdFunc_12);
            this.Controls.Add(this.cmdFunc_11);
            this.Controls.Add(this.cmdFunc_10);
            this.Controls.Add(this.label1);
            this.Name = "saFrm_Sansyo902";
            this.Text = "受注実績データ参照  (saFrm_Sansyo902)";
            this.Load += new System.EventHandler(this.saFrm_Sansyo902_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.saFrm_Sansyo902_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdJisseki)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.TextBox txtHinban;
        public System.Windows.Forms.TextBox txtHinmei;
        public System.Windows.Forms.TextBox txtSerch_6;
        public System.Windows.Forms.TextBox txtSerch_5;
        public System.Windows.Forms.TextBox txtSerch_4;
        public System.Windows.Forms.TextBox txtSerch_3;
        public System.Windows.Forms.TextBox txtSerch_2;
        public System.Windows.Forms.TextBox txtSerch_1;
        public System.Windows.Forms.TextBox txtSerch_0;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdFunc_10;
        public System.Windows.Forms.Button cmdFunc_11;
        public System.Windows.Forms.Button cmdFunc_12;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label lblPartNo;
        public System.Windows.Forms.Label lblSName;
        public System.Windows.Forms.Label lblSerch_6;
        public System.Windows.Forms.Label lblSerch_5;
        public System.Windows.Forms.Label lblSerch_4;
        public System.Windows.Forms.Label lblSerch_3;
        public System.Windows.Forms.Label lblSerch_2;
        public System.Windows.Forms.Label lblSerch_1;
        public System.Windows.Forms.Label lblSerch_0;
        public System.Windows.Forms.Button cmdSansyo;
        public System.Windows.Forms.DataGridView spdJisseki;
        public System.Windows.Forms.Label lblGuide;
    }
}