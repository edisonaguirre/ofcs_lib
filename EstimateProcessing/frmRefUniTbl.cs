﻿using clsDB;
using clswinApi;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VBlibrary;
namespace EstimateProcessing
{
    public partial class frmRefUniTbl : Form
    {
        public frmRefUniTbl()
        {
            InitializeComponent();
            spdTblInfo.DoubleClick += new EventHandler(cmdFunc_12_Click);
            txtSerch.KeyUp += new KeyEventHandler(searchevent);
            spdTblInfo.KeyDown += new KeyEventHandler(gridkeyup);
            txtSerch.Focus();
        }

        private void gridkeyup(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        cmdFunc_12_Click(sender, null);
                        break;
                    }
                case Keys.Back:
                    {
                        txtSerch.Focus();
                        break;
                    }
            }
        }

        private void searchevent(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                spdTblInfo.Focus();
                return;
            }
            SetTblInfo();
        }


        private void cmdFunc_10_Click(object sender, EventArgs e)
        {
            if(DatainsertProc())
            {
                EditClear();
                SetTblInfo();
            }
        }

        private void cmdFunc_11_Click(object sender, EventArgs e)
        {
            modSac_Com.RetMode = 0;
            this.Close();
        }

        private void cmdFunc_12_Click(object sender, EventArgs e)
        {
            modSac_Com.RetMode = 1;
            int selectedrowindex = spdTblInfo.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = spdTblInfo.Rows[selectedrowindex];
            modSac_Com.RetName1 = modHanbai.NCnvN(Convert.ToString(selectedRow.Cells[1].Value));
            this.Close();
        }
        private void menu1_Click(object sender, EventArgs e)
        {
            if (spdTblInfo.SelectedCells.Count >= 1)
                DataDeleteProc(spdTblInfo.SelectedCells[0].RowIndex);
            EditClear();
            SetTblInfo();

        }

        private void frmRefUniTbl_Load(object sender, EventArgs e)
        {
            Application.DoEvents();
            EditClear();
            SetTblInfo();
            cmdFunc_10.Text = "(F10)" + Environment.NewLine + "ENTER";
            cmdFunc_11.Text = "(F11)" + Environment.NewLine + "BACK";
            cmdFunc_12.Text= "(F12)" + Environment.NewLine + "SET";
            Thread.Sleep(500);
            txtSerch.Focus();
            Application.DoEvents();

        }



        private void EditClear(string p001 = "D")
        {
            modSac_Com.RetName1 = "";
            modSac_Com.RetMode = 0;


        }
        private void SetTblInfo()
        {

            string SQLtxt = "SELECT Uni_001,Uni_002 FROM Uni_Tbl ";
            // UNIT(曖昧検索)

            SQLtxt = Convert.ToString(SQLtxt + "WHERE Uni_002 Like '%") + modCommon.EditSQLAddSQuot(modHanbai.NCnvN(txtSerch.Text)) + "%'";
            SQLtxt = SQLtxt + " ORDER BY Uni_002";

            DBService db = new DBService();
            DataTable dt = new DataTable();
            dt = db.GetDatafromDB(SQLtxt);
            spdTblInfo.DataSource = dt;
        }
        private Boolean DatainsertProc()
        {

            if(txtSerch.Text.Trim()!= "")
            {
                if(UniInsertProc())
                {
                    SetTblInfo();
                    return true;
                }

            }
            return false;
        }

        private bool UniInsertProc()
        {
            modSinseiMarin.UniTblInfo uni = new modSinseiMarin.UniTblInfo();

            uni.Uni_001 = modSinseiMarin.GetNewCode(4);
            uni.Uni_002 = modHanbai.NCnvN(txtSerch.Text);

            string strSQL = "";
            strSQL = strSQL + "INSERT INTO Uni_Tbl(";
            strSQL = strSQL + "Uni_001,";
            strSQL = strSQL + "Uni_002,";
            strSQL = strSQL + "Uni_003,";
            strSQL = strSQL + "Uni_004,";
            strSQL = strSQL + "Uni_005,";
            strSQL = strSQL + "Uni_Insert,";
            strSQL = strSQL + "Uni_WsNo";
            strSQL = strSQL + ") VALUES (";
            strSQL = strSQL + uni.Uni_001 + ",'";
            strSQL = strSQL + modCommon.EditSQLAddSQuot(uni.Uni_002) + "',";
            strSQL = strSQL + uni.Uni_003 + ",'";
            strSQL = strSQL + modCommon.EditSQLAddSQuot(uni.Uni_004) + "','";
            strSQL = strSQL + modCommon.EditSQLAddSQuot(uni.Uni_005) + "','";
            strSQL = strSQL + DateTime.Today.ToShortDateString() + "','";
            strSQL = strSQL + StaticWinApi.Wsnumber + "')";
            DBService db = new DBService();
            string errormsg;
     
            db.ExecuteSql(strSQL,out errormsg);
            if (errormsg != "")
            {
                MessageBox.Show(@"Error has been encountered: 
                    " + errormsg);
                return false;
            }
            else
            {
                return true;
            }
        }

        private Boolean DataDeleteProc(int iRow)
        {
            Boolean retbool = false;

            string ssTXT = "";



            ssTXT = spdTblInfo.Rows[iRow].Cells[1].Value.ToString();

            if(modSac_Com.ksExpMsgBox("「" + modHanbai.NCnvN(ssTXT) + "」" + "を削除します。よろしいですか？", "Q")==(int)MsgBoxResult.No)
            {
                return retbool;
            }

            string strSQL = "DELETE FROM Uni_Tbl";
            strSQL = Convert.ToString(strSQL + " WHERE Uni_002 ='") + modCommon.EditSQLAddSQuot(modHanbai.NCnvN(ssTXT)) + "'";

            string errormsg;
            DBService db = new DBService();
            db.ExecuteSql(strSQL, out errormsg);
            if (errormsg != "")
            {
                MessageBox.Show(@"Error has been encountered: 
                    " + errormsg);
                return false;
            }
            else
            {
                return true;
            }
        }

        public byte Status()
        {
            return VBlibrary.modSac_Com.RetMode;
        }

        public string STM_TNAME()
        {
            return VBlibrary.modSac_Com.RetName1;
        }



    }
}
