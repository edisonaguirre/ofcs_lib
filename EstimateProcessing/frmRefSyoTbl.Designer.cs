﻿namespace EstimateProcessing
{
    partial class frmRefSyoTbl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdFunc_10 = new System.Windows.Forms.Button();
            this.cmdFunc_11 = new System.Windows.Forms.Button();
            this.cmdFunc_12 = new System.Windows.Forms.Button();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.txtSerch_2 = new System.Windows.Forms.TextBox();
            this.txtSerch_1 = new System.Windows.Forms.TextBox();
            this.txtSerch_0 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_3 = new System.Windows.Forms.Label();
            this.lblGuide = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.spdList = new System.Windows.Forms.DataGridView();
            this.Syo_001 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Syo_002 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Syo_003 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Syo_005 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Frame1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spdList)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(26, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(212, 35);
            this.label1.TabIndex = 0;
            this.label1.Text = "商品テーブル参照";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdFunc_10
            // 
            this.cmdFunc_10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_10.Location = new System.Drawing.Point(396, 44);
            this.cmdFunc_10.Margin = new System.Windows.Forms.Padding(4);
            this.cmdFunc_10.Name = "cmdFunc_10";
            this.cmdFunc_10.Size = new System.Drawing.Size(88, 40);
            this.cmdFunc_10.TabIndex = 13;
            this.cmdFunc_10.TabStop = false;
            this.cmdFunc_10.Text = "登録(F10)";
            this.cmdFunc_10.UseVisualStyleBackColor = false;
            this.cmdFunc_10.Click += new System.EventHandler(this.cmdFunc_10_Click);
            // 
            // cmdFunc_11
            // 
            this.cmdFunc_11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_11.Location = new System.Drawing.Point(492, 44);
            this.cmdFunc_11.Margin = new System.Windows.Forms.Padding(4);
            this.cmdFunc_11.Name = "cmdFunc_11";
            this.cmdFunc_11.Size = new System.Drawing.Size(88, 40);
            this.cmdFunc_11.TabIndex = 6;
            this.cmdFunc_11.TabStop = false;
            this.cmdFunc_11.Text = "取消(F11)";
            this.cmdFunc_11.UseVisualStyleBackColor = false;
            this.cmdFunc_11.Click += new System.EventHandler(this.cmdFunc_11_Click);
            // 
            // cmdFunc_12
            // 
            this.cmdFunc_12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cmdFunc_12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmdFunc_12.Location = new System.Drawing.Point(588, 44);
            this.cmdFunc_12.Margin = new System.Windows.Forms.Padding(4);
            this.cmdFunc_12.Name = "cmdFunc_12";
            this.cmdFunc_12.Size = new System.Drawing.Size(88, 40);
            this.cmdFunc_12.TabIndex = 5;
            this.cmdFunc_12.TabStop = false;
            this.cmdFunc_12.Text = "確定(F12)";
            this.cmdFunc_12.UseVisualStyleBackColor = false;
            this.cmdFunc_12.Click += new System.EventHandler(this.cmdFunc_12_Click);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtSerch_2);
            this.Frame1.Controls.Add(this.txtSerch_1);
            this.Frame1.Controls.Add(this.txtSerch_0);
            this.Frame1.Controls.Add(this.label2);
            this.Frame1.Controls.Add(this.label4);
            this.Frame1.Controls.Add(this.label3);
            this.Frame1.Location = new System.Drawing.Point(22, 90);
            this.Frame1.Margin = new System.Windows.Forms.Padding(4);
            this.Frame1.Name = "Frame1";
            this.Frame1.Padding = new System.Windows.Forms.Padding(4);
            this.Frame1.Size = new System.Drawing.Size(664, 100);
            this.Frame1.TabIndex = 4;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "曖昧検索";
            // 
            // txtSerch_2
            // 
            this.txtSerch_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSerch_2.Location = new System.Drawing.Point(494, 60);
            this.txtSerch_2.Margin = new System.Windows.Forms.Padding(4);
            this.txtSerch_2.Name = "txtSerch_2";
            this.txtSerch_2.Size = new System.Drawing.Size(160, 24);
            this.txtSerch_2.TabIndex = 2;
            // 
            // txtSerch_1
            // 
            this.txtSerch_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSerch_1.Location = new System.Drawing.Point(308, 60);
            this.txtSerch_1.Margin = new System.Windows.Forms.Padding(4);
            this.txtSerch_1.Name = "txtSerch_1";
            this.txtSerch_1.Size = new System.Drawing.Size(180, 24);
            this.txtSerch_1.TabIndex = 1;
            // 
            // txtSerch_0
            // 
            this.txtSerch_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtSerch_0.Location = new System.Drawing.Point(24, 60);
            this.txtSerch_0.Margin = new System.Windows.Forms.Padding(4);
            this.txtSerch_0.Name = "txtSerch_0";
            this.txtSerch_0.Size = new System.Drawing.Size(280, 24);
            this.txtSerch_0.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(24, 37);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(280, 19);
            this.label2.TabIndex = 5;
            this.label2.Text = "Description";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(494, 37);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 19);
            this.label4.TabIndex = 7;
            this.label4.Text = "UNIT";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(308, 37);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(180, 19);
            this.label3.TabIndex = 6;
            this.label3.Text = "Part No.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label_3
            // 
            this.label_3.AutoSize = true;
            this.label_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_3.ForeColor = System.Drawing.Color.Red;
            this.label_3.Location = new System.Drawing.Point(24, 206);
            this.label_3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_3.Name = "label_3";
            this.label_3.Size = new System.Drawing.Size(395, 20);
            this.label_3.TabIndex = 5;
            this.label_3.Text = "※ ヘッダー行をクリックすると並び替えをします(昇順・降順)";
            // 
            // lblGuide
            // 
            this.lblGuide.BackColor = System.Drawing.Color.Blue;
            this.lblGuide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblGuide.ForeColor = System.Drawing.Color.White;
            this.lblGuide.Location = new System.Drawing.Point(28, 667);
            this.lblGuide.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGuide.Name = "lblGuide";
            this.lblGuide.Size = new System.Drawing.Size(656, 25);
            this.lblGuide.TabIndex = 7;
            this.lblGuide.Text = "ｺｰﾄﾞを入力して下さい。";
            this.lblGuide.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.spdList);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(22, 246);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(654, 399);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // spdList
            // 
            this.spdList.AllowUserToAddRows = false;
            this.spdList.AllowUserToDeleteRows = false;
            this.spdList.AllowUserToResizeColumns = false;
            this.spdList.AllowUserToResizeRows = false;
            this.spdList.BackgroundColor = System.Drawing.Color.White;
            this.spdList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.spdList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Syo_001,
            this.Syo_002,
            this.Syo_003,
            this.Syo_005});
            this.spdList.Location = new System.Drawing.Point(2, 9);
            this.spdList.Margin = new System.Windows.Forms.Padding(4);
            this.spdList.MultiSelect = false;
            this.spdList.Name = "spdList";
            this.spdList.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.spdList.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.spdList.RowHeadersVisible = false;
            this.spdList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.spdList.Size = new System.Drawing.Size(652, 408);
            this.spdList.TabIndex = 3;
            this.spdList.TabStop = false;
            // 
            // Syo_001
            // 
            this.Syo_001.DataPropertyName = "Syo_001";
            dataGridViewCellStyle1.Format = "#";
            dataGridViewCellStyle1.NullValue = null;
            this.Syo_001.DefaultCellStyle = dataGridViewCellStyle1;
            this.Syo_001.HeaderText = "Code No";
            this.Syo_001.Name = "Syo_001";
            this.Syo_001.ReadOnly = true;
            // 
            // Syo_002
            // 
            this.Syo_002.DataPropertyName = "Syo_002";
            this.Syo_002.HeaderText = "Description";
            this.Syo_002.Name = "Syo_002";
            this.Syo_002.ReadOnly = true;
            this.Syo_002.Width = 250;
            // 
            // Syo_003
            // 
            this.Syo_003.DataPropertyName = "Syo_003";
            this.Syo_003.HeaderText = "Part No";
            this.Syo_003.Name = "Syo_003";
            this.Syo_003.ReadOnly = true;
            this.Syo_003.Width = 180;
            // 
            // Syo_005
            // 
            this.Syo_005.DataPropertyName = "Syo_005";
            this.Syo_005.HeaderText = "Unit";
            this.Syo_005.Name = "Syo_005";
            this.Syo_005.ReadOnly = true;
            // 
            // frmRefSyoTbl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(704, 777);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblGuide);
            this.Controls.Add(this.label_3);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.cmdFunc_12);
            this.Controls.Add(this.cmdFunc_11);
            this.Controls.Add(this.cmdFunc_10);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "frmRefSyoTbl";
            this.Text = "商品テーブル参照";
            this.Load += new System.EventHandler(this.frmRefSyoTbl_Load);
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spdList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.TextBox txtSerch_1;
        public System.Windows.Forms.TextBox txtSerch_0;
        public System.Windows.Forms.TextBox txtSerch_2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdFunc_10;
        public System.Windows.Forms.Button cmdFunc_11;
        public System.Windows.Forms.Button cmdFunc_12;
        public System.Windows.Forms.GroupBox Frame1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label_3;
        public System.Windows.Forms.Label lblGuide;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.DataGridView spdList;
        private System.Windows.Forms.DataGridViewTextBoxColumn Syo_001;
        private System.Windows.Forms.DataGridViewTextBoxColumn Syo_002;
        private System.Windows.Forms.DataGridViewTextBoxColumn Syo_003;
        private System.Windows.Forms.DataGridViewTextBoxColumn Syo_005;
    }
}