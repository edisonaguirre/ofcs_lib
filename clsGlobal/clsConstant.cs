﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace SAHANBAI
{

    public enum enmPrintmode
    {
        Preview = 0,   //'プレビュー
        PrintOut = 1,    //'印刷     
        Excel = 2,
        File = 3
    }



    public static class Constant
    {
        public const string exXLSSuffix  = ".xls";             //      'エクセル拡張子
        public const string exXLSWildCd  = "*";                //         'ワークファイル用ワイルドカード
        public const string exXLSPrevWk  = "SPW";              //


        public const string gstStrSEPSTR = ",";             //     '文字区切り文字定数
        public const string gstStrSEPARG = @"/";            //     '内部パラメータ用セパレータ定数
        public const string gstStrSEPCL = "-";              //     'リスト／コンボボックス区切り文字定数
        public const string gstStrKEYSEP = ":";             //     'キー文字列区切り文字(時間の区切り)
        public const string gstStrSEPPATH = @"\";           //     'パスの区切り
        public const string gstStrSEPFILE = "_";            //     'ファイルの区切り
        public const string gstStrDBLKT = "\"";             //    '文字タイプ(CSV出力用)
        public const string gstStrEXECSUF = ".EXE";         //  '実行時拡張子   

        public const string gstGUIDE_M001 = "を入力して下さい。";
        public const string gstGUIDE_M002 = "を選択して下さい。";
        public const string gstGUIDE_Q001 = "を終了します。よろしいですか？";
        public const string gstGUIDE_Q002 = "データの追加を行います。よろしいですか？";
        public const string gstGUIDE_Q003 = "データの訂正を行います。よろしいですか？";
        public const string gstGUIDE_Q004 = "データの削除を行います。よろしいですか？";
        public const string gstGUIDE_Q010 = "のプレビューを行います。よろしいですか？";
        public const string gstGUIDE_Q011 = "の印刷を行います。よろしいですか？";
        public const string gstGUIDE_Q012 = "取消しを行います。よろしいですか？";
        public const string gstGUIDE_Q013 = "画面をクリアします。よろしいですか？";
        public const string gstGUIDE_E001 = "モード選択されていません。";
        public const string gstGUIDE_E002 = "データの追加に失敗しました。";
        public const string gstGUIDE_E003 = "データの訂正に失敗しました。";
        public const string gstGUIDE_E004 = "データの削除に失敗しました。";
        public const string gstGUIDE_E005 = "対象データが存在しません。";
        public const string gstGUIDE_E006 = "既にデータが存在します。";
        public const string gstGUIDE_E011 = "が入力されていません。";
        public const string gstGUIDE_E012 = "が選択されていません。";
        public const string gstGUIDE_E013 = "の入力に誤りがあります。";
        public const string gstGUIDE_E014 = "の大小関係に誤りがあります。";





    }
}
