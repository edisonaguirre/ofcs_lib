﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAHANBAI;

namespace SAHANBAI
{


    public class TaxComp
    {
        private clsGlobal gbl = new clsGlobal();

        public struct TaxCalcInfo
        {
            public int ZeiKbn;                   //'消費税区分(Tok_019,Sir_019)        1:内税 2:外税
            public int ZeiKeisan;                //'消費税計算区分(Tok_020,Sir_020)    1:四捨五入 2:切上げ 3:切捨て
            public double Kingaku;                //'金額
            public string DDate;                  //'伝票日付

            public double ZeinukiKingaku;         //'税抜金額
            public double Shohizei;               //'消費税
            public double ZeikomiKingaku;         //'税込金額

            public DataTable ConMst;
        }

        public void pTaxCalcCom(ref TaxCalcInfo myTaxCalcInfo)
        {
            double decWk;
            double decZei;


            myTaxCalcInfo.ZeinukiKingaku = 0;
            myTaxCalcInfo.Shohizei = 0;
            myTaxCalcInfo.ZeikomiKingaku = 0;

            if (myTaxCalcInfo.ZeiKbn == 1)
            {
                decWk = Convert.ToDouble(myTaxCalcInfo.Kingaku / ((1 + GetZeiritsu(ref myTaxCalcInfo)) * 100) * (100 * GetZeiritsu(ref myTaxCalcInfo)));
                decZei = gbl.CurRound2(decWk, 0, myTaxCalcInfo.ZeiKeisan);
                myTaxCalcInfo.ZeinukiKingaku = myTaxCalcInfo.Kingaku - decZei;      // '税抜金額
                myTaxCalcInfo.Shohizei = decZei;                                    //'消費税
                myTaxCalcInfo.ZeikomiKingaku = myTaxCalcInfo.Kingaku;               //'税込金額
            }
            else
            {
                decWk = Convert.ToDouble(myTaxCalcInfo.Kingaku * GetZeiritsu(ref myTaxCalcInfo));        
                decZei = gbl.CurRound2(decWk, 0, myTaxCalcInfo.ZeiKeisan);
                myTaxCalcInfo.ZeinukiKingaku = myTaxCalcInfo.Kingaku;           //'税抜金額
                myTaxCalcInfo.Shohizei = decZei;                                //'消費税
                myTaxCalcInfo.ZeikomiKingaku = myTaxCalcInfo.Kingaku + decZei;  //'税込金額
            }

        }

        public double GetZeiritsu(ref TaxCalcInfo myTaxCalcInfo)
        {

            double SngLet;
            DateTime dt1;
            DateTime.TryParseExact(myTaxCalcInfo.DDate, "yy/MM/dd", CultureInfo.CurrentCulture, DateTimeStyles.None, out dt1);
            if (dt1 <= Convert.ToDateTime(myTaxCalcInfo.ConMst.Rows[0]["Con_038"].ToString()))
            {
                SngLet = Convert.ToDouble(myTaxCalcInfo.ConMst.Rows[0]["Con_033"].ToString());
            }
            else
            {
                SngLet = Convert.ToDouble(myTaxCalcInfo.ConMst.Rows[0]["Con_034"].ToString());
      
            }

            return SngLet;
        }
     
    }
}
