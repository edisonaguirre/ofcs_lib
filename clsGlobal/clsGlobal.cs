﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Data;
using System.Drawing;
using System.Threading;
//using Spire.Pdf;
//using Spire.Pdf.Graphics;
using System.Drawing.Imaging;
//using Spire.Pdf.Print;
using System.Globalization;
using SAHANBAI.Properties;

namespace SAHANBAI
{

    public class clsGlobal
    {
        public clsDB.DBService DB;

        public PrinterInfo ActivePrinter = new PrinterInfo();

     
        public bool gstGetPrinterInfoFunc(frmStPrinterSelect frm)
        {
            bool result = false;
            frm.spPRINTERNAME = ActivePrinter.Printer;
            frm.spPRINTCOPIES = ActivePrinter.Copies;
            frm.ShowDialog();
            if (frm.STATUS == true)
            {
                ActivePrinter.Printer = frm.spPRINTERNAME;
                ActivePrinter.Copies = frm.spPRINTCOPIES;
                ActivePrinter.hDC = frm.spPRINThDC;
                result = true;
                return result;
            }
            return result;
        }




        public struct PrinterInfo
        {
            public string Printer;
            public int Copies;
            public ulong hDC;

        }
        public string NCnvN(object buf)
        {
            if (buf == null)
            {
                buf = "";
            }
            return buf.ToString();
        }

        public double NCnvZ(object buf)
        {

            if (buf == null || buf.ToString() == "")
            {
                buf = "0";
            }
            return Convert.ToDouble(buf.ToString());
        }


        public DateTime NCnvD(string buf)
        {
            if (buf == null || buf == "")
            {
                buf = "2000/01/01";
            }
            return Convert.ToDateTime(buf);
        }


        public string NCnvS(string buf)
        {
            if (buf == null || buf == "")
            {
                buf = " ";
            }
            return buf;
        }




        public string MyConnectionString { get; set; }
        public string MyProviderName { get; set; }
        public DbConnection MyDBConn { get; set; }
        public object TaxCalcInfo { get; set; }

        public CompanyStruct Company = new CompanyStruct();

        public bool ConnectToDB()
        {
            bool bOut = false;
            try

            {
                MyConnectionString = this.BuildConnectionString();
                MyProviderName = this.GetProviderName(); ;

                if (string.IsNullOrEmpty(MyConnectionString))
                {
                    throw new ArgumentNullException("providerName");
                }
                if (string.IsNullOrEmpty(MyProviderName))
                {
                    throw new ArgumentNullException("connectionString");
                }
                System.Data.Common.DbProviderFactory factory;
                factory = System.Data.Common.DbProviderFactories.GetFactory(MyProviderName);
                if (factory != null)
                {
                    this.MyDBConn = factory.CreateConnection();
                    this.MyDBConn.ConnectionString = MyConnectionString;
                    this.MyDBConn.Open();
                    return bOut;
                }
                else
                {
                    throw new InvalidOperationException("Failed creating connection.");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Connect", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return bOut;
            }

        }

        public string GetIniPath()
        {
            

            return Settings.Default.IniPath;
        }

        public string GetProviderName()
        {
            return Settings.Default.ProviderName;
        }
        public string GetIniValue(string sKey)
        {
            string sOutput = "";

            string IniPath = this.GetIniPath();
            string[] sColLine = System.IO.File.ReadAllLines(IniPath);
            string sFilter = Array.Find(sColLine,
              element => element.Contains(sKey));

            string[] sSelect = sFilter.Split('=');
            sOutput = sSelect[1].Trim();

            return sOutput;


        }


        public string BuildConnectionString()
        {


            string CurrentUserDBName = this.GetIniValue("DB USER ID");
            string CurrentUserDBPass = this.GetIniValue("DB USER PW");
            string CurrentServer = this.GetIniValue("DB SERVER");
            string CurrentDB = this.GetIniValue("DB NAME");

            string ConnectionString = "persist security info=True;User ID = " + CurrentUserDBName +
                      ";Password= " + CurrentUserDBPass +
                      ";Data Source=" + CurrentServer +
                      ";Initial Catalog= " + CurrentDB;


            return ConnectionString;
        }



        public DialogResult ksExpMsgBox(Form fParent = null, string vsMsg = "", string vsCode = "")
        {
            string Title = "";
            MessageBoxButtons iMessageButtons = MessageBoxButtons.OK;
            MessageBoxIcon iMessageIcon = MessageBoxIcon.Information;

            switch (vsCode)
            {
                case "E":
                    iMessageButtons = MessageBoxButtons.OK;
                    iMessageIcon = MessageBoxIcon.Exclamation;
                    Title = "エラー";
                    break;
                case "Q":
                    iMessageButtons = MessageBoxButtons.YesNo;
                    iMessageIcon = MessageBoxIcon.Question;
                    Title = "確認";
                    break;
                case "I":
                    iMessageButtons = MessageBoxButtons.OK;
                    iMessageIcon = MessageBoxIcon.Information;
                    Title = "案内";
                    break;
                case "W":
                    iMessageButtons = MessageBoxButtons.YesNoCancel;
                    iMessageIcon = MessageBoxIcon.Exclamation;
                    Title = "警告";
                    break;
            }
            if (fParent != null)
            {
                return MessageBox.Show(fParent, vsMsg, Title, iMessageButtons, iMessageIcon);
            }
            else
            {
                return MessageBox.Show(vsMsg, Title, iMessageButtons, iMessageIcon);
            }
        }


        public bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            return DateTime.TryParse(txtDate, out tempDate);
        }

        public bool IsNumeric(string value)
        {
            return value.All(Char.IsDigit);
        }

        public double CurRound2(double vdValue, int viDigit, int viType = 0)
        {
            double vdAns = 0;
            long vlDig;

            vlDig = 10 ^ Math.Abs(viDigit);

            switch (viType)
            {
                case 0:
                    if (viDigit >= 0)
                    {
                        vdAns = Convert.ToInt64(Convert.ToDouble(vdValue * vlDig + 0.5)) / vlDig;
                    }
                    else
                    {
                        vdAns = Convert.ToInt64(Convert.ToDouble(vdValue / vlDig + 0.5)) * vlDig;
                    }


                    break;

                case 1:

                    if (viDigit >= 0)
                    {
                        if (vdValue != (Convert.ToInt64(Convert.ToDouble((vdValue * vlDig))) / vlDig))
                        {
                            vdAns = Convert.ToInt64(Convert.ToDouble((vdValue * vlDig + 1))) / vlDig;
                        }


                    }
                    else
                    {
                        if (vdValue != (Convert.ToInt64(Convert.ToDouble((vdValue / vlDig))) * vlDig))
                        {
                            vdAns = Convert.ToInt64(Convert.ToDouble((vdValue / vlDig + 1))) * vlDig;
                        }
                    }
                    break;

                case 2:

                    if (viDigit >= 0)
                    {
                        vdAns = Convert.ToInt64(Convert.ToDouble(vdValue * vlDig)) / vlDig;
                    }
                    else
                    {
                        vdAns = Convert.ToInt64(Convert.ToDouble(vdValue / vlDig)) * vlDig;
                    }
                    break;



            }

            return vdAns;


        }

        public DataTable GetConMstInfo()
        {
            string SQLtxt;
            SQLtxt = "";
            SQLtxt = SQLtxt + "SELECT * FROM Con_Mst";
            SQLtxt = SQLtxt + " WHERE Con_001 = 1";

            DataTable dt = DB.GetDatafromDB(SQLtxt);
            return dt;
        }



        public bool SetCtrlDate(DateTimePicker obj, int viSWKbn, string viYMDKbn)
        {

            switch (viSWKbn)
            {
                case 0:
                    switch (viYMDKbn)
                    {
                        case "Y":
                            obj.CustomFormat = "gggee年";
                            break;

                        case "YM":
                            obj.CustomFormat = "gggee年MM月";
                            break;
                        case "YMD":
                            obj.CustomFormat = "gggee年MM月dd日";
                            break;
                    }
                    break;
                case 1:
                    switch (viYMDKbn)
                    {
                        case "Y":
                            obj.CustomFormat = "yyyy年";
                            break;

                        case "YM":
                            obj.CustomFormat = "yyyy年MM月";
                            break;
                        case "YMD":
                            obj.CustomFormat = "yyyy年MM月dd日";
                            break;
                    }

                    break;

            }

            return true;
        }

        public struct CompanyStruct
        {
            public string NAME;
            public string POST;
            public string ADDRESS1;
            public string ADDRESS2;
            public string TEL;
            public string FAX;
            public string MAIL;
            public string HP;
            public string LEADER;

        }

        public void GetCompanyInfo()
        {
            String SQLtxt;

            SQLtxt = "";
            SQLtxt = SQLtxt + "SELECT * FROM Con_Mst";
            SQLtxt = SQLtxt + " WHERE Con_001 = 1";

            DataTable dt = DB.GetDatafromDB(SQLtxt);

            if (dt.Rows.Count >= 0)
            {
                Company.NAME = NCnvN(dt.Rows[0]["Con_002"].ToString());
                Company.POST = NCnvN(dt.Rows[0]["Con_003"].ToString());
                Company.ADDRESS1 = NCnvN(dt.Rows[0]["Con_004"].ToString());
                Company.ADDRESS2 = NCnvN(dt.Rows[0]["Con_005"].ToString());
                Company.TEL = NCnvN(dt.Rows[0]["Con_006"].ToString());
                Company.FAX = NCnvN(dt.Rows[0]["Con_007"].ToString());
                Company.MAIL = NCnvN(dt.Rows[0]["Con_008"].ToString());
                Company.HP = NCnvN(dt.Rows[0]["Con_009"].ToString());
                Company.LEADER = NCnvN(dt.Rows[0]["Con_010"].ToString());
            }




        }

        public bool setRangeDate(DateTimePicker datYM
                              , DateTimePicker datFrom
                              , DateTimePicker datTo)
        {
            bool setRangeDate = false;
            string strDate;

            strDate = datYM.Value.Month.ToString() + "/01/" + datYM.Value.Year.ToString();
            setRangeDate = true;

            datFrom.Value = Convert.ToDateTime(strDate);
            datFrom.Enabled = true;


            datTo.Value = datFrom.Value.AddMonths(1).AddDays(-1);
            datFrom.Enabled = true;

            setRangeDate = true;
            return setRangeDate;

        }
        public DateTime getMonthEndDate(DateTime dat)
        {
            int daysInMo = DateTime.DaysInMonth(dat.Year, dat.Month);
            return new DateTime(dat.Year, dat.Month, daysInMo);
        }

        public string EditSQLAddSQuot(string value)
        {
            value = value.Replace("'", "''");
            value = value.Replace(Environment.NewLine, "");
            return value;
        }


        
        private string SnapShot(string DestinationPath, string FileName)
        {


            string str = string.Format(Path.Combine(DestinationPath, FileName));

            Rectangle bounds = Screen.PrimaryScreen.Bounds;
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);
                bitmap.Save(str, ImageFormat.Jpeg);
            }
            return str;


        }



        public void GotFocusSec(Control vcCtrl, String vsMsg, int viMode, Control ShowControl)
        {
            if (viMode == 0)
            {
                //if (vcCtrl is DateTimePicker)
                //{

                //    DateTimePicker dtpCtrl = (DateTimePicker)vcCtrl;
                //    dtpCtrl.Region.
                //}
                //else
                //{
                vcCtrl.BackColor = Color.LightCyan;
                //}
            }
            ShowControl.Text = vsMsg.Replace(" ", "");
        }

        public void LostFocusSec(Control vcCtrl, int viMode, Control ShowControl)
        {
            if (viMode == 0)
            {
                vcCtrl.BackColor = System.Drawing.SystemColors.Window;
            }

            ShowControl.Text = "";
        }

        public DataTable GetTokMstInfo(string Tok_001, int viMode)
        {
            string SQLtxt = "";
            SQLtxt = SQLtxt + "SELECT * FROM Tok_Mst";
            switch (viMode)
            {
                case 0: //対象データ
                    SQLtxt = SQLtxt + " WHERE Tok_001 = " + Tok_001;
                    break;
                case 1: //次データ
                    SQLtxt = SQLtxt + " WHERE Tok_001 = ";
                    SQLtxt = SQLtxt + " ( SELECT MIN(Tok_001) FROM Tok_Mst";
                    SQLtxt = SQLtxt + "    WHERE Tok_001 > " + Tok_001 + ")";
                    break;
                case 2: //前データ
                    SQLtxt = SQLtxt + " WHERE Tok_001 = ";
                    SQLtxt = SQLtxt + " ( SELECT MAX(Tok_001) FROM Tok_Mst";
                    SQLtxt = SQLtxt + "    WHERE Tok_001 < " + Tok_001 + ")";
                    break;
            }

            return DB.GetDatafromDB(SQLtxt);

        }


        public string CnvDate(string sDate, string sFormat, string sDefault = "")
        {
            if (IsDateTime(NCnvN(sDate)) == true)
            {
                return Convert.ToDateTime(sDate).ToString(sFormat);
            }
            else
            {
                //return sDefault;
                return Convert.ToDateTime(sDefault).ToString(sFormat) ;
            }

        }
        public string stvVersionNumber()
        {
            return GetIniValue("SYSTEM VERSION");
        }

        public void ShowVersion(Form frm)
        {
            MessageBox.Show(frm, "Version " + stvVersionNumber(),frm.Text,  MessageBoxButtons.OK, MessageBoxIcon.Information);
         }
       


    }




}
